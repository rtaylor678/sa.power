﻿CREATE view [Val].[DCGrade] as
Select Refnum = t.Refnum, t.CoLoc, t.StudyYear, s.Version, w.Section, w.Category, w.DivMethod, RefineryType = 'OLE'
,Score = Weighting * (3 * (7*SUM(IsNull(RedH, 0)) + 3*SUM(IsNull(RedM ,0)) + 1*SUM(IsNull(RedL, 0))) 
	                 +2 * (7*SUM(IsNull(BlueH,0)) + 3*SUM(IsNull(BlueM,0)) + 1*SUM(IsNull(BlueL,0)))
	                 +1 * (7*SUM(IsNull(TealH,0)) + 3*SUM(IsNull(TealM,0)) + 1*SUM(IsNull(TealL,0))))
	/ CASE DivMethod
		WHEN 'NumUnit' THEN MIN(d.NumUnits)
		WHEN 'EDC' THEN MIN(d.EDC)
		WHEN 'PartUnit' THEN MIN(d.PartUnits)
		ELSE 1
	END
,NumReds = 	SUM(IsNull(RedH,0) + IsNull(RedM,0) + IsNull(RedL,0))
,NumBlues = SUM(IsNull(BlueH,0) + IsNull(BlueM,0) + IsNull(BlueL,0))
,NumTeals = SUM(IsNull(TealH,0) + IsNull(TealM,0) + IsNull(TealL,0))
,NumGreens = SUM(IsNull(GreenH,0) + IsNull(GreenM,0) + IsNull(GreenL,0))
,PercentRed = CASE WHEN SUM(IsNull(RedH,0) + IsNull(RedM,0) + IsNull(RedL,0) + IsNull(BlueH,0) + IsNull(BlueM,0) + IsNull(BlueL,0)+ IsNull(TealH,0) + IsNull(TealM,0) + IsNull(TealL,0)+ IsNull(GreenH,0) + IsNull(GreenM,0) + IsNull(GreenL,0)) = 0 THEN
	0 ELSE
	SUM(IsNull(RedH,0) + IsNull(RedM,0) + IsNull(RedL,0)) / SUM(IsNull(RedH,0) + IsNull(RedM,0) + IsNull(RedL,0) + IsNull(BlueH,0) + IsNull(BlueM,0) + IsNull(BlueL,0)+ IsNull(TealH,0) + IsNull(TealM,0) + IsNull(TealL,0) + IsNull(GreenH,0) + IsNull(GreenM,0) + IsNull(GreenL,0)) * 100 END
From TSort t JOIN Val.DCScores s on s.refnum=t.Refnum
Join Val.Weighting w on w.Category=s.Category 
JOIN Val.DivFactors d on d.refnum = s.refnum
Group by t.Refnum, t.CoLoc, t.StudyYear, s.Version, w.Section, w.Category, w.Weighting, w.DivMethod
