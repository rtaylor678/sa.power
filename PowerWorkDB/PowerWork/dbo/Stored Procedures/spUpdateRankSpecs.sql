﻿CREATE PROCEDURE spUpdateRankSpecs 
	@RefListNo RefListNo,
	@BreakID DD_ID, 
	@RankVariableID DD_ID
AS
UPDATE RankSpecs_LU
SET LastTime = GetDate()
WHERE RefListNo = @RefListNo AND BreakID = @BreakID 
AND RankVariableID = @RankVariableID
UPDATE RankSpecs_LU
SET NumTiles = (SELECT MAX(Tile) 
	FROM Rank 
	WHERE RankSpecsID = RankSpecs_LU.RankSpecsID)
WHERE RefListNo = @RefListNo AND BreakID = @BreakID
AND RankVariableID = @RankVariableID 
UPDATE RankSpecs_LU
SET DataCount = (SELECT Count(Refnum) 
	FROM Rank 
	WHERE RankSpecsID = RankSpecs_LU.RankSpecsID)
WHERE RefListNo = @RefListNo AND BreakID = @BreakID
AND RankVariableID = @RankVariableID
