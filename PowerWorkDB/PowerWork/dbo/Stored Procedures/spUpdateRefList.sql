﻿CREATE PROCEDURE spUpdateRefList
	@RefListNo RefListNo = -2 OUT, @ListName RefListName = NULL,
	@Owner varchar(5) = NULL,
	@JobNo varchar(10) = NULL,
	@Description varchar(50) = NULL,
	@Replace bit = 0
AS
DECLARE @Result integer
IF ((@RefListNo < 0) OR (@RefListNo IS NULL)) AND (@ListName IS NULL)
	RAISERROR ('No refinery list supplied.', 16, -1)
IF @ListName IS NULL
	SELECT @ListName = ListName FROM RefList_LU
	WHERE RefListNo = @RefListNo
IF (@RefListNo > 0) AND EXISTS (
		SELECT * FROM RefList_LU 
		WHERE ListName = @ListName AND RefListNo <> @RefListNo)
	RAISERROR ('Another list named %s already exists', 11, -1, @ListName)
IF EXISTS (SELECT * FROM RefList_LU 
	WHERE ListName = @ListName AND RefListNo <> @RefListNo) 
	AND (@Replace = 0)
	RAISERROR ('Another list named %s already exists', 11, -1, @ListName)
EXEC @Result = spCheckListOwner @ListNo = @RefListNo
IF @Result < 0
	RAISERROR ('Insufficient rights to modify list number %d', 11, -1, @RefListNo)
IF (@RefListNo IS NULL) OR (@RefListNo < 0)
BEGIN
	INSERT INTO RefList_LU (ListName, Description, Owner, JobNo, CreateDate)
	VALUES (@ListName, @Description, @Owner, @JobNo, DEFAULT)
	SELECT @RefListNo = RefListNo FROM RefList_LU
	WHERE ListName = @ListName
END
ELSE
	UPDATE RefList_LU
	SET ListName = @ListName, Description = @Description, 
	    Owner = @Owner, JobNo = @JobNo, CreateDate = Default
	WHERE RefListNo = @RefListNo
