﻿CREATE PROCEDURE procLoadDDColumns AS
INSERT INTO DD_Columns (ObjectID, ColumnName)
SELECT o.ObjectID, c.Name
FROM syscolumns c, DD_Objects o, sysObjects s
WHERE s.Name = o.ObjectName AND s.id = c.id
 AND NOT EXISTS (SELECT * FROM DD_Columns
	 WHERE DD_Columns.ObjectID = o.ObjectID AND DD_Columns.ColumnName = c.Name)
DELETE FROM DD_Columns
WHERE NOT EXISTS 
	(SELECT *
	FROM syscolumns c, sysobjects o, DD_Objects do
	WHERE o.id = c.id AND o.Name = do.ObjectName 
	AND do.ObjectID = DD_Columns.ObjectID
	AND c.Name = DD_Columns.ColumnName)
