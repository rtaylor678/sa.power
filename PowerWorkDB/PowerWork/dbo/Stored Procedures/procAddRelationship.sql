﻿CREATE PROC procAddRelationship @ObjA sysname, @ObjB sysname, @ColumnA sysname, @ColumnB sysname AS
DECLARE @ObjIDA integer, @ObjIDB integer, @TempObj integer, @TempColumn sysname
EXEC procGetObjectID @ObjA, @ObjIDA OUTPUT
EXEC procGetObjectID @ObjB, @ObjIDB OUTPUT
IF @ObjIDA > @ObjIDB
BEGIN
	SELECT @TempObj = @ObjIDB, @TempColumn = @ColumnB
	SELECT @ObjIDB = @ObjIDA, @ColumnB = @ColumnA
	SELECT @ObjIDA = @TempObj, @ColumnA = @TempColumn
END
INSERT INTO DD_Relationships
SELECT @ObjIDA, @ObjIDB, a.ColumnID, b.ColumnID
FROM DD_Columns a, DD_Columns B
WHERE a.ObjectID = @ObjIDA AND a.ColumnName = @ColumnA
AND b.ObjectID = @ObjIDB AND b.ColumnName = @ColumnB
