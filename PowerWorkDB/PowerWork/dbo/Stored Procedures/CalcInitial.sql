﻿
CREATE PROC [dbo].[CalcInitial](@SiteID SiteID)
AS

----SW 12/20/12 why is this being done?
--UPDATE OHMaint
--SET Component = 'SITETRAN', ProjectID = CASE WHEN ID = 600110 THEN 'OVHL' ELSE 'OTHER' END
--FROM OHMaint INNER JOIN Equipment e ON e.Refnum = OHMaint.Refnum AND e.TurbineID = OHMaint.TurbineID AND e.EquipID = OHMaint.EquipID
--WHERE OHMaint.Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND e.EquipType = 'SITETRAN' AND OHMaint.Component = 'BOIL'

----SW 12/20/12 why is this being done? couldn't it be done in tables?
--UPDATE OHMaint
--SET ProjectID = 'OTHER'
--WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND Component = 'SITETRAN' AND ProjectID = 'FWHEAT'

--SW 12/20/12 this doesn't do anything now, last time it did was 2003
--UPDATE CTGData
--SET FiringTemp = CTGSupplement.FiringTemp
--FROM CTGData INNER JOIN CTGSupplement
--	ON CTGData.Refnum = CTGSupplement.Refnum AND CTGData.TurbineID = CTGSupplement.TurbineID
--WHERE CTGData.FiringTemp IS NULL AND CTGData.Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE PowerGeneration
SET NetMWH = ISNULL(GrossMWH, 0) - ISNULL(StationSvcElec, 0), 
	ElecMBTU = (ISNULL(GrossMWH, 0) - ISNULL(StationSvcElec, 0))*3.41214 -- MBTU = MWh * 3.41214
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID AND StudyYear >= 1999)

INSERT DesignData (UtilityUnitCode, Utility, Unit)
SELECT DISTINCT UtilityUnitCode, Util_Code, Unit_Code 
FROM NERCTurbine
WHERE UtilityUnitCode IN (SELECT UtilityUnitCode FROM NERCTurbine WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID))
AND UtilityUnitCode NOT IN (SELECT UtilityUnitCode FROM DesignData)

UPDATE DesignData 
SET BlrPSIG = BoilerPressure 
FROM EquipCount ec
LEFT JOIN NERCTurbine nt ON nt.Refnum = ec.Refnum
where ec.TurbineID <> 'Site'
and DesignData.UtilityUnitCode = nt.UtilityUnitCode
AND ec.Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND ec.BoilerPressure IS NOT NULL


--SW 7/12/16
--Tony decided that steam was not calculating correctly, leading to heat rates that are too high for any cogens.
--after way too much discussion, the following is intended to fix the problem
--ask Tony for the details if you want them. basically the original value was only calculating halfway, it wasn't getting the final conversion from steam to hot water
--by adding that in (by putting in 69.1227 as the final enthalpy) we get much closer to a theoretical steam turbine value, and thus better steam heat rates

UPDATE SteamSales 
	SET StmSalesMWH = (StmSales * 1000)/((3412.14 / (Enthalpy - 69.1227))/0.4)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)


--SW 7/12/16
--updating the way that equipment is assigned.
--previously it was done in the upload spreadsheet, in a fairly haphazard way
--this is intended to do it in the database, using lookup tables

DECLARE cUnits CURSOR LOCAL FAST_FORWARD
FOR	SELECT Refnum FROM TSort WHERE SiteID = @SiteID
DECLARE @Refnum Refnum
OPEN cUnits
FETCH NEXT FROM cUnits INTO @Refnum
WHILE @@FETCH_STATUS = 0
BEGIN


	--first we make sure the equipment is there: this adds all the STG equipment if they have an STG, and all the CTG equipment if they have a CTG

	IF NOT EXISTS (SELECT * FROM Equipment WHERE Refnum = @Refnum AND EquipID = 210 AND EquipType = 'STG-TURB') --if we haven't already inserted STGs
	BEGIN
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 210, EquipType = 'STG-TURB' FROM Equipment where Refnum = @refnum and EquipID = 200
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 220, EquipType = 'STG-GEN' FROM Equipment where Refnum = @refnum and EquipID = 200
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 230, EquipType = 'STG-VC' FROM Equipment where Refnum = @refnum and EquipID = 200
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 240, EquipType = 'STG-CA' FROM Equipment where Refnum = @refnum and EquipID = 200
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 250, EquipType = 'STG-HPS' FROM Equipment where Refnum = @refnum and EquipID = 200
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 260, EquipType = 'STG-IPS' FROM Equipment where Refnum = @refnum and EquipID = 200
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 270, EquipType = 'STG-LPS' FROM Equipment where Refnum = @refnum and EquipID = 200
	END

	IF NOT EXISTS (SELECT * FROM Equipment WHERE Refnum = @Refnum AND EquipID = 800 AND EquipType = 'CTG-TURB') --if we haven't already inserted CTGs
	BEGIN
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 800, EquipType = 'CTG-TURB' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 810, EquipType = 'CTG-GEN' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 820, EquipType = 'CTG-TRAN' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 830, EquipType = 'CTG-SCR' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 840, EquipType = 'CTG-COMB' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 850, EquipType = 'CTG-COMP' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
		insert Equipment (Refnum, TurbineID, EquipID, EquipType) SELECT distinct Refnum, TurbineID, EquipID = 860, EquipType = 'CTG-OTH' FROM NERCTurbine where Refnum = @refnum and TurbineType IN ('CTG','CC')
	END


	--then we update some tables
	EXEC spUpdateOHMaintComponents @Refnum
	FETCH NEXT FROM cUnits INTO @Refnum
END
CLOSE cUnits
DEALLOCATE cUnits



