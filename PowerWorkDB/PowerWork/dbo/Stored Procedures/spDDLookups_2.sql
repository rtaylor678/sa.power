﻿CREATE PROCEDURE spDDLookups;2
	@LookupID DD_ID
AS
	SELECT *
	FROM DD_Lookups
	WHERE LookupID = @LookupID
