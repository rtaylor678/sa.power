﻿CREATE  PROCEDURE  dbo.sp_HourlyPrices(@Refnum As Varchar(15) ) AS


DELETE FROM AvailableHours WHERE Refnum = @Refnum

INSERT INTO Offline(refnum,turbineid,util_code,unit_code,evnt_type,event_start,event_end,nac,gac)
SELECT  refnum,turbineid,util_code,unit_code,evnt_type,event_start,event_end,nac,gac
FROM _NERCOffline
WHERE Refnum = @Refnum





INSERT INTO AvailableHours (Refnum, Period, Price, Unavail)



SELECT t.Refnum, 
       StartTime = DATEADD(mi, -DATEPART(mi, r.StartTime), 
       DATEADD(hh, h.Hour-DATEPART(hh, r.StartTime), r.StartTime)), 
       r.Price,
       (
         select COUNT(n.Refnum)
         from Offline n
        where 
       (dateadd(hh,h.hour, r.starttime) between dateadd(mi,-1,n.event_start) and dateadd(mi,1,n.event_end)) and
       Refnum =t.Refnum 
     ) As NotAvailable
FROM PricingHub_LU l,
     RevenuePrices r, 
     HoursInDay h, 
     TSort t
     
WHERE 
t.Refnum = @Refnum
 AND
(
 (r.Pricinghub = t.PricingHub AND l.PricingHub=t.PricingHub) OR 
 ( l.[Legacy Name] in( t.PricingHub) AND r.PricingHub=l.PricingHub) OR 
 ( r.PricingHub=l.[Legacy Name] AND l.PricingHub = t.PricingHub)  OR
 (charindex(','+convert(nvarchar,RTrim(t.PricingHub))+',',','+l.[Legacy Name]+',')>0 and r.PricingHub=l.PricingHub)
)  
AND DATEPART(yy, r.StartTime) = t.StudyYear
AND h.Hour BETWEEN DATEPART(hh, r.StartTime) AND DATEPART(hh, DATEADD(mi, -1, r.EndTime))
order by DATEADD(hh, h.Hour-DATEPART(hh, r.StartTime), r.StartTime)

DELETE FROM Offline WHERE Refnum = @Refnum