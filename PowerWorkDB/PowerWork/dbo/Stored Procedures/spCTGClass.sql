﻿
CREATE            PROC [dbo].[spCTGClass] (@Refnum Refnum)
AS

	-- Created by SW 6/11/14
	-- Purpose is to fill in the class of the CTG (which is used starting in 2013 EGC model)
	-- We are trying to determine if the CTG is E (or lower), F (or higher), or Aero
	-- We don't want to change the class if it is already populated (we're assuming it is correct)
	-- We're trying to use some standard abbreviations of Frame Model to catch the unit. What if it is not caught?

	-- In general you will see pairs of updates below, one defining the E and one defining the F of a particular type
	-- if you are adding to this, see if there is a way to define the pairs for each type

	-- this is a brute force way of updating the class. Is there a better way?

	-- we only do CC units
	IF @Refnum NOT LIKE '%C[1-9][1-9]%'
		RETURN

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%11N2%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%501[B-D]%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%501[F-G]%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%GT[0-1]%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%GT2[4-6]%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%GT-2[4-6]%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel = '6B' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel = '6FA' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel = '6101 FA' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel = '6101 TA' AND Refnum LIKE @Refnum AND Class IS NULL --hoping this is a typo
	UPDATE CTGData SET Class = 'F' WHERE FrameModel = '6111 FA' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%701%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%GT8C%' AND Refnum LIKE @Refnum AND Class IS NULL -- Alstom unit

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%7[B-E]%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%7F%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%9[B-E]%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%9F%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%V%.2%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%V%.3%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%SGT-%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%SGT5%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '%PG[5-6]%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '%PG9%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE 'MS%' AND FrameModel NOT LIKE 'MS%F%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE 'MS%F%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'E' WHERE FrameModel LIKE '6541%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE '7241%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'Aero' WHERE FrameModel LIKE '%LM%' AND Refnum LIKE @Refnum AND Class IS NULL
	UPDATE CTGData SET Class = 'Aero' WHERE FrameModel LIKE '%Trent%' AND Refnum LIKE @Refnum AND Class IS NULL

	UPDATE CTGData SET Class = 'F' WHERE FrameModel LIKE 'AE94.3A4%' AND Refnum LIKE @Refnum AND Class IS NULL
	
	UPDATE CTGData SET Manufacturer = 'SIEMENS' WHERE Manufacturer like '%Siemens%' AND Refnum LIKE @Refnum --standardize manufacturer name
	UPDATE CTGData SET Manufacturer = 'SIEMENS' WHERE Manufacturer like '%Siemems%' AND Refnum LIKE @Refnum --standardize manufacturer name, based on a typo
	
	UPDATE CTGData SET Manufacturer = 'GENERAL ELECTRIC' WHERE Manufacturer like '%General Electric%' AND Refnum LIKE @Refnum --standardize manufacturer name
	UPDATE CTGData SET Manufacturer = 'GENERAL ELECTRIC' WHERE Manufacturer = 'GE' AND Refnum LIKE @Refnum --standardize manufacturer name
	UPDATE CTGData SET Manufacturer = 'GENERAL ELECTRIC' WHERE Manufacturer like '%General Elektric%' AND Refnum LIKE @Refnum --standardize manufacturer name, based on a typo
	
	UPDATE CTGData SET Manufacturer = 'ALSTOM' WHERE Manufacturer like '%Alstom%' AND Refnum LIKE @Refnum --standardize manufacturer name
	UPDATE CTGData SET Manufacturer = 'ALSTOM' WHERE Manufacturer like '%ABB%' AND Refnum LIKE @Refnum --standardize manufacturer name

	UPDATE CTGData SET Manufacturer = 'MHI' WHERE Manufacturer like '%MHI%' AND Refnum LIKE @Refnum --standardize manufacturer name
	UPDATE CTGData SET Manufacturer = 'MHI' WHERE Manufacturer like '%Mitsubishi%' AND Refnum LIKE @Refnum --standardize manufacturer name

	UPDATE CTGData SET Manufacturer = 'PRATT WHITNEY', Class = 'Aero' WHERE Manufacturer = 'Pratt Whitney Power System' AND Refnum LIKE @Refnum
