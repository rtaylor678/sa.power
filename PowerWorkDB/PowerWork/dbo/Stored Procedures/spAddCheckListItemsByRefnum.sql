﻿-- =============================================
-- Author:		Dennis Burdett
-- Create date: 11/25/2008
-- Description:	Add Check List Items used in PwrConsole by Refnum
-- =============================================
create PROCEDURE [DBo].[spAddCheckListItemsByRefnum] 
	-- Add the parameters for the stored procedure here
	@Refnum Char(12)
AS
BEGIN
	SET NOCOUNT ON;

	-- grab master records
	SELECT space(12) AS Refnum, IssueID, IssueTitle, IssueText, PostedBy, PostedTime, 
		Completed, SetBy, SetTime
	INTO #tmp 
	FROM dbo.ValStatSource

	UPDATE #tmp SET Refnum = @Refnum, PostedTime = getdate()

	INSERT INTO ValStat
	SELECT * FROM #tmp

END
