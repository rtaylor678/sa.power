﻿CREATE PROCEDURE dbo.sp_CalcEstRevenue(@Refnum Varchar(15) /*, @Year Int*/)  AS


DELETE FROM Revenue WHERE Refnum=@Refnum /*and Year(Period)=@Year*/

INSERT INTO  Revenue ( Refnum,Period,AdjNetMWH,AvgPrice,RevenueKUS)
SELECT a.Refnum,
       p.Period,
       AVG(p.NetMWH) As AdjNetMWH,
       AVG(a.Price) As AvgPrice,
       CAST(   (AVG(p.NetMWH)*AVG(a.Price)) /1000    As MONEY) As RevenueKUS
FROM PowerGeneration p, AvailableHours a 
WHERE p.refnum=a.refnum and 
              p.refnum=@Refnum and
              Year(p.Period)=Year(a.Period) and  
              Month(p.Period)=Month(a.Period) /*and
              Year(a.Period)=@Year*/
GROUP BY Month(p.Period),a.Refnum,p.Period