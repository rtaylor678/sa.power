﻿
CREATE PROCEDURE [dbo].[CalcSummary](@SiteID SiteID)
AS

BEGIN TRY

	DECLARE @NumSitePers TABLE (
		Refnum varchar(12) NOT NULL,
		OCCSitePers real NULL,
		MPSSitePers real NULL)
	INSERT @NumSitePers (Refnum, OCCSitePers, MPSSitePers)
	SELECT Refnum, SUM(CASE WHEN PersCat Like 'OCC%' THEN SiteNumPers ELSE 0 END), SUM(CASE WHEN PersCat Like 'MPS%' THEN SiteNumPers ELSE 0 END)
	FROM Pers WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND PersCat NOT LIKE '%Exc%' 
	GROUP BY Refnum

	DELETE FROM GenSum WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	INSERT INTO GenSum(Refnum, EUF, EFOR, EAF2Yr, EFOR2Yr, EUF2Yr, NCF2Yr, NOF2Yr
		, HeatRate, HeatRateLHV, NetMWH, AdjNetMWH
		, MaintNonOHInd, MaintOHInd, MaintInd, MaintOHProjInd
		, OSHARate, TotCash, GrandTot, ActCashLessFuelMWH, TotCashwoFuelCost
		, TotEffPersMWH, TotEffPersMW, TotEffPersGJ
		, OCCAnnComp, OCCBenPcnt, MPSAnnComp, MPSBenPcnt
		, AvgFuelCostMBTU, AvgFuelCostMBTULHV)
	SELECT t.Refnum, EUF = CASE WHEN (ISNULL(nf.EUF,0) + ISNULL(nf.EFOR,0)) > 0 THEN ISNULL(nf.EUF,0) END
		, EFOR = CASE WHEN (ISNULL(nf.EUF,0) + ISNULL(nf.EFOR,0)) > 0 THEN ISNULL(nf.EFOR,0) END
		, nf.EAF2Yr, nf.EFOR2Yr, nf.EUF2Yr, nf.NCF2Yr, nf.NOF2Yr
		, gtc.HeatRate, gtc.HeatRateLHV, gtc.NetMWH, gtc.AdjNetMWH
		, AnnNonOHCostMWH, AnnOHCostMWH, AnnMaintCostMWH, AnnOHProjCostMWH
		, OSHARate = ISNULL(s.OSHAIncRate, ISNULL(s.CompOSHAIncRate,0))
		, oc.TotCash, oc.GrandTot, oc.ActCashLessFuel, TotCashwoFuelCost = oc.TotCash - oc.STFuelCost
		, TotEffPersMWH = CASE WHEN TotEffPersMWH > 0 THEN TotEffPersMWH END, TotEffPersMW = CASE WHEN TotEffPersMW > 0 THEN TotEffPersMW END, TotEffPersGJ = CASE WHEN TotEffPersGJ > 0 THEN TotEffPersGJ END
		, CASE WHEN sp.OCCSitePers > 0 THEN oa.OCCCompensation/sp.OCCSitePers*1000 END, CASE WHEN oa.OCCWages > 0 THEN oa.OCCBenefits*100/oa.OCCWages END
		, CASE WHEN sp.MPSSitePers > 0 THEN oa.MPSCompensation/sp.MPSSitePers*1000 END, CASE WHEN oa.MPSWages > 0 THEN oa.MPSBenefits*100/oa.MPSWages END
		, CASE WHEN ftc.TotMBTU > 0 THEN 1000*oa.STFuelCost/ftc.TotMBTU END
		, CASE WHEN ftc.TotMBTULHV > 0 THEN 1000*oa.STFuelCost/ftc.TotMBTULHV END
	FROM TSort t LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
		LEFT JOIN Safety s ON s.Refnum = t.Refnum
		LEFT JOIN OpExCalc oc ON oc.Refnum = t.Refnum AND oc.DataType = 'MWH'
		LEFT JOIN PersSTCalc pst ON pst.Refnum = t.Refnum AND pst.SectionID = 'TP'
		LEFT JOIN OpExCalc oa ON oa.Refnum = t.Refnum AND oa.DataType = 'ADJ'
		LEFT JOIN @NumSitePers sp ON sp.Refnum = t.Refnum
		LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum
	WHERE t.SiteID = @SiteID

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcSummary'
	
	EXEC sp_InsertMessage @siteid, 35, 'CalcSummary', 'CalcSummary'

END TRY
BEGIN CATCH

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcSummary'

	EXEC sp_InsertMessage @siteid, 36, 'CalcSummary', 'CalcSummary'

END CATCH