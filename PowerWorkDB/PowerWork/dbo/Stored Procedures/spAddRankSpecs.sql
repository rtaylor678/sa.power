﻿CREATE PROCEDURE spAddRankSpecs @RefListNo RefListNo,
			 @BreakID DD_ID,
			 @BreakValue varchar(12),
			 @RankVariableID DD_ID,
			 @RankSpecsID DD_ID OUTPUT
AS
IF NOT EXISTS (SELECT * FROM RankSpecs_LU 
	WHERE RefListNo = @RefListNo AND BreakID = @BreakID
	AND BreakValue = @BreakValue AND RankVariableID = @RankVariableID)
	INSERT INTO RankSpecs_LU (RefListNo, BreakID, BreakValue, RankVariableID, LastTime)
	VALUES (@RefListNo, @BreakID, @BreakValue, @RankVariableID, GetDate())
	
SELECT @RankSpecsID = RankSpecsID
FROM RankSpecs_LU 
WHERE RefListNo = @RefListNo 
AND BreakID = @BreakID AND BreakValue = @BreakValue
AND RankVariableID = @RankVariableID
