﻿-- =============================================
-- Author:		Dennis Burdett
-- Create date: Aug 5, 2008
-- Description:	Updates all the tables in Power, PowerWork, and PowerGlobal to change a RefNum
-- Update: 3/26/14 SW changed this stored proc to be dynamic, and to change both Refnum and SiteIDs
-- =============================================
-- CAUTION: You must pass the four variables. They will all be changed. If you do not want to change some, pass the same variable to both.
-- For example, when changing a Refnum but not a SiteID, pass the old and new refnums, and for the old and new SiteIDs pass the old one twice.
-- This will cause it to replace itself with itself, leaving the same value.

CREATE PROCEDURE [dbo].[spChangeRefnumToNewRefnum] 
	@OrgRefnum CHAR(12), 
	@NewRefnum CHAR(12),
	@OrgSiteID CHAR(10),
	@NewSiteID CHAR(10)
	
AS
BEGIN

	CREATE TABLE #dbtmp (db CHAR(50)) --these are the databases it is going to look in
	INSERT #dbtmp (db) VALUES ('Power')
	INSERT #dbtmp (db) VALUES ('PowerGlobal')
	INSERT #dbtmp (db) VALUES ('PowerWork')

	CREATE TABLE #tbltmp (tb CHAR(50)) --these are the columns it is going to update
	INSERT #tbltmp (tb) VALUES ('Refnum')
	INSERT #tbltmp (tb) VALUES ('SiteID')
	--Caution: the UPDATE query below has Refnum hardcoded in it, to populate the right fields with the right data.
	--If you change the columns here, you will need to change the query too.

	DECLARE @dbname CHAR(50)
	DECLARE @tblname CHAR(50)
	DECLARE @table CHAR(100)
	DECLARE @sql CHAR(500)

	--we loop through each of the databases in #dbtemp
	DECLARE db_cursor CURSOR FOR SELECT db FROM #dbtmp
	OPEN db_cursor
	FETCH NEXT FROM db_cursor INTO @dbname
	WHILE @@FETCH_STATUS = 0
		BEGIN

			--we loop through the field names we want to replace (from #tbltmp)
			DECLARE tbl_cursor CURSOR FOR SELECT tb FROM #tbltmp
			OPEN tbl_cursor
			FETCH NEXT FROM tbl_cursor INTO @tblname
			WHILE @@FETCH_STATUS = 0
				BEGIN

					--we find all the tables in the chosen database that have a field with the chosen name
					EXEC ('DECLARE Refnum_Cursor CURSOR FOR
						SELECT o.name
						FROM ' + @dbname + '.sys.columns c
							LEFT JOIN ' + @dbname + '.sys.objects o ON o.object_id = c.object_id
						WHERE c.name = ''' + @tblname + ''' AND o.Type=''U'' AND o.schema_id = 1
						ORDER BY o.name')

					--for each table found, we do the replace on the appropriate field with the appropriate value
					OPEN Refnum_Cursor
					FETCH NEXT FROM Refnum_Cursor INTO @table
					WHILE @@FETCH_STATUS = 0
						BEGIN

							SET @sql = ('UPDATE ' + RTRIM(@dbname) + '.dbo.' + RTRIM(@table) + ' SET ' + RTRIM(@tblname) + ' = ''' + RTRIM(CASE WHEN @tblname = 'Refnum' THEN @NewRefnum ELSE @NewSiteID END) + ''' WHERE ' + RTRIM(@tblname) + ' = ''' + RTRIM(CASE WHEN @tblname = 'Refnum' THEN @OrgRefnum ELSE @OrgSiteID END) + '''')
							EXEC (@sql)
							FETCH NEXT FROM Refnum_Cursor INTO @table
						END

					CLOSE Refnum_Cursor
					DEALLOCATE Refnum_Cursor

					FETCH NEXT FROM tbl_Cursor INTO @tblname
				END
			CLOSE tbl_Cursor
			DEALLOCATE tbl_Cursor
			
			FETCH NEXT FROM db_Cursor INTO @dbname
		END

	CLOSE db_Cursor
	DEALLOCATE db_Cursor

	DROP TABLE #dbtmp
	DROP TABLE #tbltmp


	--if ((substring(@OrgRefNum, len(@OrgRefNum)-2, 3)) = (substring(@NewRefNum, len(@NewRefNum)-2, 3)))
	--BEGIN
	--	-- Update all the PowerWork tables
	--	UPDATE PowerWork.dbo.AlternateHubs SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Ancillary SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.AssetCosts SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Auxiliaries SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.AvailableHours SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Breaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CarryoverParents SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Coal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CoalTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CogenCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CommUnavail SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.ComponentEUF SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.ComponentGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CptlMaintExp SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CptlMaintExpLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CRVCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CRVGap SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CTGData SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CTGMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.CTGSupplement SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.EGCCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Equipment SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.EventLRO SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Events SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Fuel SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.FuelProperties SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.FuelTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.GenerationAnn SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.GenerationTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.GenSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.HRVariance SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.LTSA SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.MaintEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.MaintEquipSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.MaintTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.MessageLog SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Misc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.MiscCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.NERCFactors SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.NERCTurbine SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.NonOHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Offline SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.OHEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.OHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.OHMaintCPHM SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.OpEx SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.OpExCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.OpexLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Pers SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.PersSTCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.PowerGeneration SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Rank SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.Revenue SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.RevenuePeaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.ROODs SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.StartsAnalysis SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.SteamSales SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.TSort SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.UnitGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.ValidationNotes SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.ValStat SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE PowerWork.dbo.VarCostCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum

	--	-- Update all the PowerWork tables
	--	UPDATE Power.dbo.AlternateHubs SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Ancillary SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.AssetCosts SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Auxiliaries SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Breaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Coal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CoalTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CogenCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CommUnavail SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.ComponentEUF SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.ComponentGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CptlMaintExp SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CptlMaintExpLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CRVCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CRVGap SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CTGData SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.CTGMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.EGCCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.EmissionsTons SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Equipment SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.EventLRO SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Events SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Fuel SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.FuelProperties SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.FuelTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.GenerationAnn SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.GenerationTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.GenSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.HRVariance SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.LTSA SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.MaintEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.MaintEquipSum SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.MaintTotCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.MessageLog SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Misc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.MiscCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.NERCFactors SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.NERCTurbine SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.NERCTurbine SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.NonOHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.OHEquipCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.OHMaint SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.OHMaintCPHM SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.OpEx SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.OpExCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.OpexLocal SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Pers SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.PersSTCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.PowerGeneration SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.Rank SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.RevenuePeaks SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.SteamSales SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.TSort SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.UnitGaps SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.ValidationNotes SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.ValStat SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.ValStatRestore SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--	UPDATE Power.dbo.VarCostCalc SET refnum=@NewRefNum WHERE refnum = @OrgRefNum

	--	-- Update all the PowerGlobal tables
	--	UPDATE PowerGlobal.dbo.RefList SET refnum=@NewRefNum WHERE refnum = @OrgRefNum
	--END
	--ELSE
	--	Print 'last 3 CHARs of refnums do NOT match'
END
