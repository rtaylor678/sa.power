﻿CREATE PROCEDURE [dbo].[spInflateCarryover](@SiteID varchar(10), @InflFactor real)
AS

--SW 10/23/12 - the Current value from last year should be the Prev value for this year, so just copy it and don't inflate
--and we only inflate the Current value if Curr and Prev don't match 
--(if they do match the client may be keeping them that way, and we get a slightly more accurate result by not inflating)

--UPDATE PlantGenData
--SET PlantPrevMMOACostLocal=PlantPrevMMOACostLocal*@InflFactor,
--PlantCurrMMOACostLocal=PlantCurrMMOACostLocal*@InflFactor
--WHERE SiteID = @SiteID

UPDATE PlantGenData
SET PlantPrevMMOACostLocal=PlantCurrMMOACostLocal, --copy last year's curr to this year's prev
PlantCurrMMOACostLocal=PlantCurrMMOACostLocal*@InflFactor --inflate this year's curr from last year's curr
WHERE SiteID = @SiteID AND PlantPrevMMOACostLocal <> PlantCurrMMOACostLocal --when they don't match



UPDATE CptlMaintExpLocal
SET TotCptl = TotCptl*@InflFactor,
CptlExcl = CptlExcl*@InflFactor,
Energy = Energy*@InflFactor,
RegEnv = RegEnv*@InflFactor,
Admin = Admin*@InflFactor,
ConstrRmvl = ConstrRmvl*@InflFactor,
STMaintCptl = STMaintCptl*@InflFactor,
TotMaintExp = TotMaintExp*@InflFactor,
MaintExpExcl = MaintExpExcl*@InflFactor,
STMaintExp = STMaintExp*@InflFactor,
TotMMO = TotMMO*@InflFactor,
AllocMMO = AllocMMO*@InflFactor,
MMOExcl = MMOExcl*@InflFactor,
STMMO = STMMO*@InflFactor,
OtherCptl = OtherCptl*@InflFactor
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE Misc
SET ScrCostMaintLocal=ScrCostMaintLocal*@InflFactor,
ScrCostNMaintLocal=ScrCostNMaintLocal*@InflFactor,
AshCostDispLocal=AshCostDispLocal*@InflFactor,
AshCostOpLocal=AshCostOpLocal*@InflFactor
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE OHMaint
SET DirectCostLocal=DirectCostLocal*@InflFactor,
LumpSumCostLocal=LumpSumCostLocal*@InflFactor
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE NonOHMaint
SET PrevExpLocal=PrevExpLocal*@InflFactor,
PrevCptlLocal=PrevCptlLocal*@InflFactor,
PrevLaborCostLocal=PrevLaborCostLocal*@InflFactor,
PrevMMOCostLocal=PrevMMOCostLocal*@InflFactor,
CurrExpLocal=CurrExpLocal*@InflFactor,
CurrCptlLocal=CurrCptlLocal*@InflFactor,
CurrLaborCostLocal=CurrLaborCostLocal*@InflFactor,
CurrMMOCostLocal=CurrMMOCostLocal*@InflFactor
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE OpexLocal
SET OCCWages=OCCWages*@InflFactor,
MPSWages=MPSWages*@InflFactor,
OCCBenefits=OCCBenefits*@InflFactor,
MPSBenefits=MPSBenefits*@InflFactor,
CentralOCCWagesBen=CentralOCCWagesBen*@InflFactor,
CentralMPSWagesBen=CentralMPSWagesBen*@InflFactor,
ContMaintMatl=ContMaintMatl*@InflFactor,
ContMaintLumpSum=ContMaintLumpSum*@InflFactor,
OthContSvc=OthContSvc*@InflFactor,
MaintMatl=MaintMatl*@InflFactor,
ContMaintLabor=ContMaintLabor*@InflFactor,
Envir=Envir*@InflFactor,
AGPers=AGPers*@InflFactor,
Supply=Supply*@InflFactor,
OthFixed=OthFixed*@InflFactor,
Chemicals=Chemicals*@InflFactor
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

IF EXISTS (SELECT * FROM StudySites WHERE SiteID = @SiteID AND StudyYear >= 2010)
BEGIN
	UPDATE OpexLocal
	SET PropTax=PropTax*@InflFactor,
	OthTax=OthTax*@InflFactor,
	Insurance=Insurance*@InflFactor,
	OthVar=OthVar*@InflFactor,
	AGNonPers=AGNonPers*@InflFactor,
	Water=Water*@InflFactor
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	UPDATE LTSA
	SET PaymentCurrKLocal = PaymentCurrKLocal*@InflFactor,
	PaymentPrevKLocal = PaymentPrevKLocal*@InflFactor,
	TotCostKLocal = TotCostKLocal*@InflFactor
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
END

