﻿CREATE  PROCEDURE spQuarterlyHeatRates (@Refnum Refnum)
AS
DECLARE @AnnAdjNetMWH float
SELECT @AnnAdjNetMWH = AdjNetMWH
FROM GenerationTotCalc WHERE Refnum = @Refnum
SELECT Quarter = DATEPART(qq, Period), HeatRate = CONVERT(real, SUM(TotMBTU)/SUM(AdjNetMWH)*1000),
HeatRateLHV = CONVERT(real, SUM(TotMBTULHV)/SUM(AdjNetMWH)*1000),
AvgFuelCostMBTU = SUM(FuelCostMBTU*TotMBTU)/SUM(TotMBTU)
FROM VarCostCalc
WHERE Refnum = @Refnum AND AdjNetMWH > 0 
GROUP BY DATEPART(qq, Period)
HAVING SUM(AdjNetMWH/@AnnAdjNetMWH) > 0.05
