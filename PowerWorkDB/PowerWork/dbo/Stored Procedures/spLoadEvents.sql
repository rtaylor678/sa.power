﻿


CREATE  PROC spLoadEvents ( @Refnum Refnum )
AS

DELETE FROM Events WHERE Refnum = @Refnum

INSERT INTO Events (Refnum, TurbineID, Evnt_No, Phase, Evnt_Type, Start_Time, End_Time, Avail_Cap, LostMW, Duration, Cause_Code)
SELECT t.Refnum, n.TurbineID, tl.Evnt_No, tl.Phase, tl.EVNT_TYPE, tl.Start_Time, tl.End_Time, tl.Avail_Cap, tl.LostMW, tl.Duration, e1.CauseCode
FROM TSort t INNER JOIN NERCTurbine n ON n.Refnum = t.Refnum
INNER JOIN EvntTL tl ON tl.UtilityUnitCode = n.UtilityUnitCode
LEFT JOIN Event01 e1 ON e1.UtilityUnitCode = n.UtilityUnitCode AND e1.Year = tl.EVNT_YEAR AND e1.EventNumber = tl.EVNT_NO 
--AND e2.CARDNO = '02' -- THIS PORTION REMOVED OF THE JOIN CLAUSE DELETED
--                        AND EVNT02 CHANGED TO EVENT01 WHEN WE WENT TO 
--                        THE NEW GADS SYSTEM.
WHERE tl.Evnt_Year = t.EvntYear
AND t.Refnum = @Refnum

UPDATE Events
SET EVNT_Category = EVNT_LU.EVNT_Category, EVNT_GROUP = EVNT_LU.EVNT_GROUP
FROM Events INNER JOIN EVNT_LU ON Events.EVNT_TYPE = EVNT_LU.EVNT_TYPE
WHERE Events.Refnum = @Refnum

UPDATE Events
SET EVNT_Category = e.EVNT_Category
FROM Events INNER JOIN Events e
ON e.Refnum = Events.Refnum AND e.TurbineID = Events.TurbineID
AND e.EVNT_GROUP = Events.EVNT_GROUP AND e.END_TIME = Events.START_TIME
WHERE Events.EVNT_TYPE IN ('SE', 'DE')
AND Events.Refnum = @Refnum

UPDATE Events
SET EVNT_Category = 'P'
WHERE EVNT_TYPE IN ('SE', 'DE') AND EVNT_Category IS NULL
AND Refnum = @Refnum




