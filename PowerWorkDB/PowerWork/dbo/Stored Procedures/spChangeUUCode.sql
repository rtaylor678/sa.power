﻿






CREATE  PROC spChangeUUCode (@OldCode varchar(6), @NewCode varchar(6))
AS
	IF EXISTS (SELECT * FROM NERCTurbine WHERE UtilityUnitCode = @NewCode)
	 	PRINT @NewCode + ' already exists'
	ELSE
		UPDATE NERCTurbine
		Set UtilityUnitCode = @NewCode,
			Util_Code = LEFT(@NewCode,3),
			Unit_Code = RIGHT(@NewCode,3)
		WHERE UtilityUnitCode = @OldCode
	


