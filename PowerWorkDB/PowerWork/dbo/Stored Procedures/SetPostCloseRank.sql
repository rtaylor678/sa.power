﻿

CREATE PROC [dbo].[SetPostCloseRank](@Refnum varchar(10), @Listname varchar(20), @BreakCondition varchar(20), @BreakValue varchar(10), @RankVariableID INT, @Value REAL)
AS


	INSERT PostCloseRank (Refnum, ListName, BreakCondition, BreakValue, Percentile, RankVariableID)
	SELECT Refnum = @Refnum, 
		ListName = @Listname, 
		BreakCondition = @BreakCondition, 
		BreakValue = @BreakValue,
		Percentile = (MIN(case when Value < @Value then Percentile else 100 end) + MAX(case when Value >= @Value then Percentile else 0 end)) /2, 
		@RankVariableID
	FROM _RankView r
	WHERE ListName = @Listname
		AND BreakCondition = @BreakCondition
		AND BreakValue = @BreakValue
		AND RankVariableID = @RankVariableID



