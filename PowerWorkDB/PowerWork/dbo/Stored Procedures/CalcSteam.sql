﻿CREATE PROC [dbo].[CalcSteam] (@SiteID SiteID)
AS

BEGIN TRY

	DECLARE @Units TABLE(Refnum char(12) NOT NULL, StmGenCount smallint NULL)
	INSERT @Units (Refnum, StmGenCount)
	SELECT Refnum, StmGenCount = (SELECT COUNT(*) FROM NERCTurbine WHERE NERCTurbine.Refnum = TSort.Refnum AND TurbineType IN ('BLR', 'HRSG'))
	FROM TSort 
	WHERE SiteID = @SiteID

	DECLARE @SvcHrs real, @Starts real
	SELECT @SvcHrs = 0, @Starts = 0
	DECLARE @StmHeatRate real; SET @StmHeatRate = 8500 /*Heat Rate used to convert MBTU to MWH*/
	DECLARE @BoilerEff real; SET @BoilerEff = 0.85;  /* Boiler Efficiency*/
	DECLARE @StudyYear StudyYear; SELECT @StudyYear = StudyYear FROM StudySites WHERE SiteID = @SiteID

	DELETE FROM FuelTotCalc WHERE Refnum IN (SELECT Refnum FROM @Units)
	DELETE FROM GenerationTotCalc WHERE Refnum IN (SELECT Refnum FROM @Units)
	DELETE FROM VarCostCalc WHERE Refnum IN (SELECT Refnum FROM @Units)

	INSERT FuelTotCalc (Refnum, NatGasMBTU, NatGasMBTULHV, OffGasMBTU, OffGasMBTULHV, H2GasMBTU, H2GasMBTULHV, JetTurbMBTU, JetTurbMBTULHV, JetTurbKGal, DieselMBTU, DieselMBTULHV, DieselKGal, FuelOilMBTU, FuelOilMBTULHV, FuelOilKGal, CoalMBTU, CoalMBTULHV)
	SELECT TSort.Refnum, NatGasMBTU = SUM(CASE WHEN Fuel.FuelType = 'NatGas' THEN MBTU ELSE 0 END), NatGasMBTULHV = SUM(CASE WHEN Fuel.FuelType = 'NatGas' THEN MBTULHV ELSE 0 END),
	OffGasMBTU = SUM(CASE WHEN Fuel.FuelType = 'OffGas' THEN MBTU ELSE 0 END), OffGasMBTULHV = SUM(CASE WHEN Fuel.FuelType = 'OffGas' THEN MBTULHV ELSE 0 END),
	H2GasMBTU = SUM(CASE WHEN Fuel.FuelType = 'H2Gas' THEN MBTU ELSE 0 END), H2GasMBTULHV = SUM(CASE WHEN Fuel.FuelType = 'H2Gas' THEN MBTULHV ELSE 0 END),
	JetTurbMBTU = SUM(CASE WHEN Fuel.FuelType = 'Jet' THEN MBTU ELSE 0 END), JetTurbMBTULHV = SUM(CASE WHEN Fuel.FuelType = 'Jet' THEN MBTULHV ELSE 0 END), JetTurbKGal = SUM(CASE WHEN Fuel.FuelType = 'Jet' THEN KGal ELSE 0 END),
	DieselMBTU = SUM(CASE WHEN Fuel.FuelType = 'Diesel' THEN MBTU ELSE 0 END), DieselMBTULHV = SUM(CASE WHEN Fuel.FuelType = 'Diesel' THEN MBTULHV ELSE 0 END), DieselKGal = SUM(CASE WHEN Fuel.FuelType = 'Diesel' THEN KGal ELSE 0 END),
	FuelOilMBTU = SUM(CASE WHEN Fuel.FuelType = 'FOil' THEN MBTU ELSE 0 END), FuelOilMBTULHV = SUM(CASE WHEN Fuel.FuelType = 'FOil' THEN MBTULHV ELSE 0 END), FuelOilKGal = SUM(CASE WHEN Fuel.FuelType = 'FOil' THEN KGal ELSE 0 END),
	CoalMBTU = ISNULL((SELECT MBTU FROM CoalTotCalc WHERE CoalTotCalc.Refnum = TSort.Refnum),0),
	CoalMBTULHV = ISNULL((SELECT MBTULHV FROM CoalTotCalc WHERE CoalTotCalc.Refnum = TSort.Refnum),0)
	FROM TSort LEFT JOIN Fuel ON Fuel.Refnum =TSort.Refnum 
	WHERE TSort.SiteID = @SiteID
	GROUP BY TSort.Refnum

	UPDATE FuelTotCalc
	SET JetTurbMBTU = ISNULL(JetTurbMBTU,0), DieselMBTU = ISNULL(DieselMBTU,0), FuelOilMBTU = ISNULL(FuelOilMBTU,0),
		JetTurbMBTULHV = ISNULL(JetTurbMBTULHV,0), DieselMBTULHV = ISNULL(DieselMBTULHV,0), FuelOilMBTULHV = ISNULL(FuelOilMBTULHV,0),
		NatGasMBTU = ISNULL(NatGasMBTU,0), OffGasMBTU = ISNULL(OffGasMBTU,0), H2GasMBTU = ISNULL(H2GasMBTU,0),
		NatGasMBTULHV = ISNULL(NatGasMBTULHV,0), OffGasMBTULHV = ISNULL(OffGasMBTULHV,0), H2GasMBTULHV = ISNULL(H2GasMBTULHV,0),    
		LiquidsMBTU = ISNULL(JetTurbMBTU,0) + ISNULL(DieselMBTU,0) + ISNULL(FuelOilMBTU,0),
		LiquidsMBTULHV = ISNULL(JetTurbMBTULHV,0) + ISNULL(DieselMBTULHV,0) + ISNULL(FuelOilMBTULHV,0),
		TotGasMBTU = ISNULL(NatGasMBTU,0) + ISNULL(OffGasMBTU,0) + ISNULL(H2GasMBTU,0),
		TotGasMBTULHV = ISNULL(NatGasMBTULHV,0) + ISNULL(OffGasMBTULHV,0) + ISNULL(H2GasMBTULHV,0),
		JetTurbHeatValue = CASE WHEN JetTurbKGal > 0 THEN JetTurbMBTU/JetTurbKGal END,
		JetTurbLHV = CASE WHEN JetTurbKGal > 0 THEN JetTurbMBTULHV/JetTurbKGal END,
		DieselHeatValue = CASE WHEN DieselKGal > 0 THEN DieselMBTU/DieselKGal END,
		DieselLHV = CASE WHEN DieselKGal > 0 THEN DieselMBTULHV/DieselKGal END,
		FuelOilHeatValue = CASE WHEN FuelOilKGal > 0 THEN FuelOilMBTU/FuelOilKGal END,
		FuelOilLHV = CASE WHEN FuelOilKGal > 0 THEN FuelOilMBTULHV/FuelOilKGal END
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	UPDATE FuelTotCalc
	SET TotMBTU = ISNULL(LiquidsMBTU,0) + ISNULL(TotGasMBTU,0) + ISNULL(CoalMBTU,0),
		TotMBTULHV = ISNULL(LiquidsMBTULHV,0) + ISNULL(TotGasMBTULHV,0) + ISNULL(CoalMBTULHV,0),
		FuelType = CASE WHEN CoalMBTU > TotGasMBTU THEN CASE WHEN CoalMBTU > LiquidsMBTU THEN 'Coal' ELSE 'Oil' END
						WHEN TotGasMBTU > LiquidsMBTU THEN 'Gas'
						WHEN LiquidsMBTU > 0 THEN 'Oil'
						ELSE '' END
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	/* Update NERCTurbine */
	UPDATE NERCTurbine SET
		ServiceFactor = CASE WHEN PeriodHrs>0 THEN ServiceHrs/PeriodHrs*100 ELSE NULL END,
		ServiceFactor2Yr = CASE WHEN PeriodHrs2Yr>0 THEN ServiceHrs2Yr/PeriodHrs2Yr*100 ELSE NULL END,
		WPH = PeriodHrs * NMC, SHMW = ServiceHrs * NMC, NOFWtFactor = PeriodHrs * NMC,
		WPH2Yr = PeriodHrs2Yr * NMC2Yr, NOF2YrWtFactor = PeriodHrs2Yr * NMC2Yr,
		MSTPO = CASE WHEN NumPO > 0 THEN ServiceHrs/NumPO ELSE PeriodHrs END,
		MSTUO = CASE WHEN NumUO > 0 THEN ServiceHrs/NumUO ELSE PeriodHrs END,
		MSTFO = CASE WHEN NumFO > 0 THEN ServiceHrs/NumFO ELSE PeriodHrs END,
		MSTMO = CASE WHEN NumMO > 0 THEN ServiceHrs/NumMO ELSE PeriodHrs END
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	--select * from @Units
	/* DB 01-28-08 changed the case statements to exclude CST & blank & null turbinetypes 
		 original code TurbineType = 'CST' */
	SELECT Refnum, NMC=SUM(NMC*PeriodHrs*IsTurbine)/AVG(PeriodHrs*IsTurbine),
	NDC=SUM(NDC*PeriodHrs*IsTurbine)/AVG(PeriodHrs*IsTurbine),
	NMC_MBTU = 0,
	NDC_MBTU = 0,
	--SW 7/26/13 NMC_MBTU and NDC_MBTU have been NULL since 2006, so set them to 0 here just to remove the calc and the error message they give
	--NMC_MBTU=SUM(NMC_MBTU*PeriodHrs*IsTurbine)/AVG(PeriodHrs*IsTurbine),
	--NDC_MBTU=SUM(NDC_MBTU*PeriodHrs*IsTurbine)/AVG(PeriodHrs*IsTurbine),
	PeriodHrs=MAX(PeriodHrs*IsTurbine),
	ServiceHrs=MAX(ServiceHrs*IsTurbine),
	ATMStarts = SUM(ATMStarts), ACTStarts=SUM(ACTStarts),
	EUF=SUM(EUF*WPH)/SUM(WPH), EAF=SUM(EAF*WPH)/SUM(WPH),
	EFOR=SUM(EFOR*EFORWtFactor)/SUM(EFORWtFactor), EFORWtFactor=SUM(EFORWtFactor),
	NumPO=SUM(NumPO), NumUO=SUM(NumUO), NumFO=SUM(NumFO), NumMO=SUM(NumMO),
		  /*  DB 11-12-08 Next line, changed AVG to MAX for PeriodHrs2Yr */
	PeriodHrs2Yr = MAX(PeriodHrs2Yr*IsTurbine),
	ServiceHrs2Yr=MAX(ServiceHrs2Yr*IsTurbine),
	ServiceHrs2YrAvg=MAX(ServiceHrs2YrAvg*IsTurbine),
	NMC2Yr=SUM(NMC2Yr*PeriodHrs2Yr*IsTurbine)/AVG(PeriodHrs2Yr*IsTurbine),
	NDC2Yr=SUM(NDC2Yr*PeriodHrs2Yr*IsTurbine)/AVG(PeriodHrs2Yr*IsTurbine),
	NMC_MBTU2Yr=0, --SUM(NMC_MBTU2Yr*PeriodHrs2Yr*IsTurbine)/AVG(PeriodHrs2Yr*IsTurbine), --see above NMC_MBTU for why
	NDC_MBTU2Yr=0, --SUM(NDC_MBTU2Yr*PeriodHrs2Yr*IsTurbine)/AVG(PeriodHrs2Yr*IsTurbine),
	EUF2Yr=GlobalDB.dbo.WtAvg(EUF2Yr, WPH2Yr), EAF2Yr=GlobalDB.dbo.WtAvg(EAF2Yr,WPH2Yr),
	EFOR2Yr=GlobalDB.dbo.WtAvg(EFOR2Yr,EFOR2YrWtFactor),
	EFOR2YrWtFactor=SUM(EFOR2YrWtFactor), Age=MAX(Age),
	EUOF=GlobalDB.dbo.WtAvg(EUOF, WPH), EPOF=GlobalDB.dbo.WtAvg(EPOF, WPH),
	EUOF2Yr=GlobalDB.dbo.WtAvg(EUOF2Yr, WPH2Yr), EPOF2Yr=GlobalDB.dbo.WtAvg(EPOF2Yr, WPH2Yr),
	EOR=GlobalDB.dbo.WtAvg(EOR, EORWtFactor), EORWtFactor=SUM(EORWtFactor),
	EOR2Yr=GlobalDB.dbo.WtAvg(EOR2Yr, EOR2YrWtFactor), EOR2YrWtFactor=SUM(EOR2YrWtFactor),
		   /* 01/21/09 DB fixed error in next line's calculation - typo in WtFactor name */
	EUOR=SUM(EUOR*EUORWtFactor)/SUM(EUORWtFactor), EUORWtFactor=SUM(EUORWtFactor),
	EUOR2Yr=SUM(EUOR2Yr*EUOR2YrWtFactor)/SUM(EUOR2YrWtFactor),
	EUOR2YrWtFactor=SUM(EUOR2YrWtFactor),
		   /* 01/21/09 DB fixed error in next line's calculation - typo in WtFactor name*/
	EPOR=GlobalDB.dbo.WtAvg(EPOR, EPORWtFactor), EPORWtFactor=SUM(EPORWtFactor),
	EPOR2Yr=GlobalDB.dbo.WtAvg(EPOR2Yr, EPOR2YrWtFactor), EPOR2YrWtFactor=SUM(EPOR2YrWtFactor),
		   /* begin lines added by DB 1/21/09 for Rod */
	WEFOF=GlobalDB.dbo.WtAvg(WEFOF, WPH), WEFOF2Yr=GlobalDB.dbo.WtAvg(WEFOF2Yr, WPH2Yr),
	WUOR=GlobalDB.dbo.WtAvg(WUOR, UORWtFactor), UORWtFactor=SUM(UORWtFactor),
	WUOR2Yr=GlobalDB.dbo.WtAvg(WUOR2Yr, UOR2YrWtFactor), UOR2YrWtFactor=SUM(UOR2YrWtFactor),
	WFOR=GlobalDB.dbo.WtAvg(WFOR, FORWtFactor), FORWtFactor=SUM(FORWtFactor),
	WFOR2Yr=GlobalDB.dbo.WtAvg(WFOR2Yr, FOR2YrWtFactor), FOR2YrWtFactor=SUM(FOR2YrWtFactor),
	WPOR=GlobalDB.dbo.WtAvg(WPOR, PORWtFactor), PORWtFactor=SUM(PORWtFactor),
	WPOR2Yr=GlobalDB.dbo.WtAvg(WPOR2Yr, POR2YrWtFactor), POR2YrWtFactor=SUM(POR2YrWtFactor),
		   /* end lines added by DB 1/21/09 for Rod */
	--4/2/13 SW added RS for Tony
	RSHours = MAX(RSHours * IsTurbine),
	RSEvents = SUM(ISNULL(RSEvents,0)),
	AF = SUM(AF*WPH)/SUM(WPH)
	INTO #NERCFactors
	FROM (SELECT NERCTurbine.*, IsTurbine = CAST(CASE WHEN TurbineType = 'CST' or ISNULL(TurbineType, '') = '' THEN NULL ELSE 1 END as real), u.StmGenCount
		FROM  NERCTurbine INNER JOIN @Units u ON u.Refnum = NERCTurbine.Refnum) x 
	WHERE StmGenCount = 0 OR TurbineType <> 'SS'
	GROUP BY Refnum

	INSERT INTO NERCFactors(Refnum)
	SELECT Refnum FROM @Units u
	WHERE NOT EXISTS (SELECT * FROM NERCFactors WHERE NERCFactors.Refnum = u.Refnum)

	UPDATE NERCFactors
	SET NMC = x.NMC, NDC = x.NDC, PeriodHrs = x.PeriodHrs, ServiceHrs = x.ServiceHrs,
	ATMStarts = x.ATMStarts, ACTStarts = x.ACTStarts,
	EUF = x.EUF, EAF = x.EAF, EFOR = x.EFOR, EFORWtFactor = x.EFORWtFactor,
	NumPO = x.NumPO, NumUO = x.NumUO, NumFO = x.NumFO, NumMO = x.NumMO,
	PeriodHrs2Yr = x.PeriodHrs2Yr, ServiceHrs2Yr = x.ServiceHrs2Yr,
	ServiceHrs2YrAvg = x.ServiceHrs2YrAvg, NMC2Yr = x.NMC2Yr, NDC2Yr = x.NDC2Yr,
	EUF2Yr = x.EUF2Yr, EAF2Yr = x.EAF2Yr, EFOR2Yr = x.EFOR2Yr,
	EFOR2YrWtFactor = x.EFOR2YrWtFactor, Age = x.Age,
	NMC_MBTU = x.NMC_MBTU, NDC_MBTU = x.NDC_MBTU,
	NMC_MBTU2Yr = x.NMC_MBTU2Yr, NDC_MBTU2Yr = x.NDC_MBTU2Yr,
	EUOF = x.EUOF, EPOF = x.EPOF,
	EUOF2Yr = x.EUOF2Yr, EPOF2Yr = x.EPOF2Yr,
	EOR = x.EOR, EORWtFactor = x.EORWtFactor,
	EOR2Yr = x.EOR2Yr, EOR2YrWtFactor = x.EOR2YrWtFactor,
	EUOR = x.EUOR, EUORWtFactor = x.EUORWtFactor,
	EUOR2Yr = x.EUOR2Yr, EUOR2YrWtFactor = x.EUOR2YrWtFactor,
	EPOR = x.EPOR, EPORWtFactor = x.EPORWtFactor,
	EPOR2Yr = x.EPOR2Yr, EPOR2YrWtFactor = x.EPOR2YrWtFactor,
	WEFOF = x.WEFOF, WEFOF2Yr = x.WEFOF2Yr, 
	WUOR = x.WUOR, UORWtFactor = x.UORWtFactor,
	WUOR2Yr = x.WUOR2Yr, UOR2YrWtFactor = x.UOR2YrWtFactor,
	WFOR = x.WFOR, FORWtFactor = x.FORWtFactor,
	WFOR2Yr = x.WFOR2Yr, FOR2YrWtFactor = x.FOR2YrWtFactor,
	WPOR = x.WPOR, PORWtFactor = x.PORWtFactor,
	WPOR2Yr = x.WPOR2Yr, POR2YrWtFactor = x.POR2YrWtFactor,
	RSHours = x.RSHours, RSEvents = x.RSEvents,
	AF = x.AF
	FROM NERCFactors LEFT JOIN #NERCFactors x ON x.Refnum = NERCFactors.Refnum
	WHERE NERCFactors.Refnum IN (SELECT Refnum FROM @Units)

	UPDATE NERCFactors
	SET MSTPO = CASE WHEN NumPO > 0 THEN ServiceHrs/NumPO ELSE PeriodHrs END,
	MSTUO = CASE WHEN NumUO > 0 THEN ServiceHrs/NumUO ELSE PeriodHrs END,
	MSTFO = CASE WHEN NumFO > 0 THEN ServiceHrs/NumFO ELSE PeriodHrs END,
	MSTMO = CASE WHEN NumMO > 0 THEN ServiceHrs/NumMO ELSE PeriodHrs END
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	SELECT Refnum, TurbineID, SUM(CASE WHEN DATEPART(YY, Period) = @StudyYear THEN NetMWH ELSE 0 END) AS NetMWH 
	, SUM(CASE WHEN DATEPART(YY, Period) = (@StudyYear-1) THEN NetMWH ELSE 0 END) AS PriorNetMWH 
	INTO #TurbineNetMWH
	FROM PowerGeneration
	WHERE Refnum IN (SELECT Refnum FROM @Units)
	GROUP BY Refnum, TurbineID

	UPDATE NERCTurbine SET 
		NCF = CASE WHEN (NMC*PeriodHrs) = 0 THEN NULL WHEN t.NetMWH > 0 THEN t.NetMWH/(NMC*PeriodHrs)*100 END,
		NOF = CASE WHEN (NMC*ServiceHrs) = 0 THEN NULL WHEN t.NetMWH > 0 THEN t.NetMWH/(NMC*ServiceHrs)*100 END,
		--NCF2Yr = CASE WHEN (NMC2Yr*PeriodHrs2Yr) = 0 THEN NULL WHEN (t.NetMWH+t.PriorNetMWH) > 0 THEN (t.NetMWH+t.PriorNetMWH)/(NMC2Yr*PeriodHrs2Yr)*100 END,
		--NOF2Yr = CASE WHEN (NMC2Yr*ServiceHrs2Yr) = 0 THEN NULL WHEN (t.NetMWH+t.PriorNetMWH) > 0 THEN (t.NetMWH+t.PriorNetMWH)/(NMC2Yr*ServiceHrs2Yr)*100 END
		--SW 9/18/13 replaced the two lines above with the two lines below
		--this deals with the rare case (~20 units 2010-12) where they have Prior Year MWH but not Prior Year event data
		--this would cause their generation to double while period hours remains the same, which in turn causes NCF and NOF to double
		--leading to cases of NCF > 100%, which is obviously wrong
		--fix was to check if the Period Hours matched the Period Hours 2Yr, if they do then only use the current year MWH, not Prior Year as well
		NCF2Yr = CASE WHEN (NMC2Yr*PeriodHrs2Yr) = 0 THEN NULL WHEN (t.NetMWH+(case when PeriodHrs= PeriodHrs2Yr then 0 else t.PriorNetMWH end)) > 0 THEN (t.NetMWH+(case when PeriodHrs= PeriodHrs2Yr then 0 else t.PriorNetMWH end))/(NMC2Yr*PeriodHrs2Yr)*100 END,
		NOF2Yr = CASE WHEN (NMC2Yr*ServiceHrs2Yr) = 0 THEN NULL WHEN (t.NetMWH+(case when PeriodHrs= PeriodHrs2Yr then 0 else t.PriorNetMWH end)) > 0 THEN (t.NetMWH+(case when PeriodHrs= PeriodHrs2Yr then 0 else t.PriorNetMWH end))/(NMC2Yr*ServiceHrs2Yr)*100 END
	FROM NERCTurbine LEFT JOIN #TurbineNetMWH t ON t.Refnum = NERCTurbine.Refnum AND t.TurbineID = NERCTurbine.TurbineID
	WHERE NERCTurbine.Refnum In (SELECT Refnum FROM @Units)

	UPDATE NERCFactors SET
		ServiceFactor = CASE WHEN PeriodHrs>0 THEN ServiceHrs/PeriodHrs*100 ELSE NULL END,
		ServiceFactor2Yr = CASE WHEN PeriodHrs2Yr>0 THEN ServiceHrs2Yr/PeriodHrs2Yr*100 ELSE NULL END,
		WPH = PeriodHrs * NMC, SHMW = ServiceHrs * NMC, NOFWtFactor = PeriodHrs * NMC,
		WPH2Yr = PeriodHrs2Yr * NMC2Yr, NOF2YrWtFactor = PeriodHrs2Yr * NMC2Yr,
		MSTPO = CASE WHEN NumPO > 0 THEN ServiceHrs/NumPO ELSE PeriodHrs END,
		MSTUO = CASE WHEN NumUO > 0 THEN ServiceHrs/NumUO ELSE PeriodHrs END,
		MSTFO = CASE WHEN NumFO > 0 THEN ServiceHrs/NumFO ELSE PeriodHrs END,
		MSTMO = CASE WHEN NumMO > 0 THEN ServiceHrs/NumMO ELSE PeriodHrs END
	WHERE Refnum IN (SELECT Refnum FROM @Units)

	DROP TABLE #NERCFactors
	DROP TABLE #TurbineNetMWH

	--  VarCostCalc
	SELECT g.Refnum, g.Period, NetMWH = SUM(g.NetMWH),
		StmMWH = (SELECT SUM(s1.StmSalesMWH) FROM SteamSales s1 WHERE s1.Refnum = g.Refnum AND s1.Period = g.Period),
		StmMBTU = (SELECT SUM(s2.StmSalesMBTU) FROM SteamSales s2 WHERE s2.Refnum = g.Refnum AND s2.Period = g.Period),
		AdjNetMWH = CAST(NULL as float)
	INTO #VarCostCalc
	FROM PowerGeneration g
	WHERE g.Refnum In (SELECT Refnum FROM @Units) AND DATEPART(YY, g.Period) = @StudyYear
	GROUP BY g.Refnum, g.Period

	UPDATE #VarCostCalc
	SET NetMWH = ISNULL(NetMWH, 0), StmMWH = ISNULL(StmMWH, 0), StmMBTU = ISNULL(StmMBTU, 0),
		AdjNetMWH = ISNULL(NetMWH, 0) + ISNULL(StmMWH, 0)
	INSERT INTO VarCostCalc (Refnum, Period, StmSalesMBTU, StmSalesMWH, AdjNetMWH)
	SELECT Refnum, Period, StmMBTU, StmMWH, AdjNetMWH
	FROM #VarCostCalc

	DROP TABLE #VarCostCalc

	-- GenerationTotals
	INSERT INTO GenerationTotCalc (Refnum, NetMWH, StationSvcElec,
	ColdStarts, HotStarts, SROperHrs, HSOperHrs, AutoGenCtrlPcnt)
	SELECT Refnum, SUM(NetMWH), SUM(ISNULL(StationSvcElec,0)),
	SUM(ISNULL(ColdStarts,0)), SUM(ISNULL(HotStarts,0)), SUM(ISNULL(SROperHrs,0)), SUM(ISNULL(HSOperHrs,0)),
	GlobalDB.dbo.WtAvg(AutoGenCtrlPcnt, NetMWH)
	FROM PowerGeneration
	WHERE Refnum In (SELECT Refnum FROM @Units) AND DATEPART(YY, Period) = @StudyYear
	GROUP BY Refnum

	SELECT Refnum, StmSales = SUM(StmSales), StmMWH = SUM(StmSalesMWH), StmMBTU = SUM(StmSalesMBTU),
		StmPress = GlobalDB.dbo.WtAvgNN(StmPress, CASE WHEN StmSales > 0 THEN StmSales ELSE 0 END),
		StmTemp = GlobalDB.dbo.WtAvgNN(StmTemp, CASE WHEN StmSales > 0 THEN StmSales ELSE 0 END)
	INTO #AnnSteam
	FROM SteamSales
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND DATEPART(YY, Period) = @StudyYear
	GROUP BY Refnum

	UPDATE GenerationTotCalc
	SET StmSales = ISNULL(a.StmSales, 0), StmSalesMWH = ISNULL(a.StmMWH, 0),
		StmPress = a.StmPress, StmTemp = a.StmTemp,
		AdjNetMWH = NetMWH + ISNULL(a.StmMWH, 0)
	FROM GenerationTotCalc LEFT JOIN #AnnSteam a ON a.Refnum = GenerationTotCalc.Refnum
	WHERE GenerationTotCalc.Refnum IN (SELECT Refnum FROM @Units)

	IF @StudyYear >= 1999
		UPDATE GenerationTotCalc 
		SET AutoGenCtrlPcnt = a.AutoGenCtrlPcnt,
			ColdStarts = a.ColdStarts, HotStarts = a.HotStarts,
			TotStarts = ISNULL(a.STGStarts, a.TotStarts)
		FROM GenerationTotCalc INNER JOIN GenerationAnn a
		ON GenerationTotCalc.Refnum = a.Refnum
		WHERE GenerationTotCalc.Refnum IN (SELECT Refnum FROM @Units)

	IF @StudyYear >= 2014
		UPDATE GenerationTotCalc 
		SET TotStarts = ISNULL(a.ACTStarts,0)
		FROM GenerationTotCalc INNER JOIN NERCFactors a
		ON GenerationTotCalc.Refnum = a.Refnum
		WHERE GenerationTotCalc.Refnum IN (SELECT Refnum FROM @Units)	

	SELECT u.Refnum, MBTUs = ftc.TotMBTU, MBTULHVs = ftc.TotMBTULHV, nf.ServiceHrs
		, gtc.HotStarts, gtc.ColdStarts, gtc.TotStarts, gtc.NetMWH, gtc.AdjNetMWH, SSE = gtc.StationSvcElec, Starts = ISNULL(gtc.TotStarts, ISNULL(gtc.HotStarts,0)+ISNULL(gtc.ColdStarts,0))
		, HeatRate = CAST(NULL as real), HeatRateLHV = CAST(NULL as real)
		, PriorStmSalesMWH = (SELECT SUM(StmSalesMWH) FROM SteamSales WHERE SteamSales.Refnum = u.Refnum AND DATEPART(YY, Period) = @StudyYear - 1)
		, PriorStmSales = (SELECT SUM(StmSales) FROM SteamSales WHERE SteamSales.Refnum = u.Refnum AND DATEPART(YY, Period) = @StudyYear - 1)
		, PriorNetMWH = (SELECT SUM(NetMWH) FROM PowerGeneration WHERE PowerGeneration.Refnum = u.Refnum AND DATEPART(YY, Period) = @StudyYear - 1)
	INTO #GenTot
	FROM @Units u LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = u.Refnum
	LEFT JOIN NERCFactors nf ON nf.Refnum = u.Refnum
	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = u.Refnum

	UPDATE GenerationTotCalc
	SET StationSvcElec = gt.SSE, 
		PriorStmSales = ISNULL(gt.PriorStmSales,0), 
		PriorStmSalesMWH = ISNULL(gt.PriorStmSalesMWH,0),
		PriorNetMWH = gt.PriorNetMWH,
		PriorAdjNetMWH = ISNULL(gt.PriorNetMWH,0)+ISNULL(gt.PriorStmSalesMWH,0),
		TotStarts = ISNULL(gt.TotStarts, gt.Starts), 
		MeanRunTime = CASE WHEN gt.Starts > 0 THEN gt.ServiceHrs/gt.Starts END,
		HeatRate = CASE WHEN gt.AdjNetMWH > 0 THEN gt.MBTUs / gt.AdjNetMWH * 1000 END, 
		HeatRateLHV = CASE WHEN gt.AdjNetMWH > 0 THEN gt.MBTULHVs / gt.AdjNetMWH * 1000 END,
		AdjNetMWH2Yr = (gt.AdjNetMWH + ISNULL(gt.PriorNetMWH,0)+ISNULL(gt.PriorStmSalesMWH,0)) / 2
	FROM GenerationTotCalc INNER JOIN #GenTot gt ON gt.Refnum = GenerationTotCalc.Refnum

	UPDATE GenerationTotCalc
	SET PriorAdjNetMWH = NULL, AdjNetMWH2Yr = AdjNetMWH
	WHERE Refnum IN (SELECT Refnum FROM @Units) AND ISNULL(PriorNetMWH,0) = 0

	-- Update NERC Capacity Factors for the unit
	UPDATE NERCFactors SET
		NCF = CASE WHEN ISNULL(NMC*PeriodHrs,0) > 0 AND gtc.AdjNetMWH > 0 THEN gtc.AdjNetMWH/(NMC*PeriodHrs)*100 END,
		NOF = CASE WHEN ISNULL(NMC*ServiceHrs,0) > 0 AND gtc.AdjNetMWH > 0 THEN gtc.AdjNetMWH/(NMC*ServiceHrs)*100 END,
		--SW 9/18/13 changed the two lines below, see note above about NERCTurbine, cases where there's no prior year event data
		NCF2Yr = CASE WHEN ISNULL(NMC2Yr*PeriodHrs2Yr,0) > 0 AND gtc.PriorAdjNetMWH > 0 THEN (gtc.AdjNetMWH+(CASE WHEN PeriodHrs = PeriodHrs2Yr THEN 0 ELSE gtc.PriorAdjNetMWH END))/(NMC2Yr*PeriodHrs2Yr)*100 END, 
		NOF2Yr = CASE WHEN ISNULL(NMC2Yr*ServiceHrs2Yr,0) > 0 AND gtc.PriorAdjNetMWH > 0 THEN (gtc.AdjNetMWH+(CASE WHEN PeriodHrs = PeriodHrs2Yr THEN 0 ELSE gtc.PriorAdjNetMWH END))/(NMC2Yr*ServiceHrs2Yr)*100 END
	FROM NERCFactors LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = NERCFactors.Refnum
	WHERE NERCFactors.Refnum In (SELECT Refnum FROM @Units)

	DROP TABLE #AnnSteam
	DROP TABLE #GenTot


	-- CalcAuxiliaries
	UPDATE Auxiliaries
	SET AuxLvlMWH = CASE Auxiliaries.LoadVary
			WHEN 'H' THEN nf.NCF/100*GlobalDB.dbo.HoursInYear(@StudyYear)*DesignStmFlow*0.7457
			WHEN 'N' THEN nf.ServiceHrs*DesignStmFlow*(EnthalpyIn-EnthalpyOut)*0.000293
			WHEN 'Y' THEN nf.NCF/100*GlobalDB.dbo.HoursInYear(@StudyYear)*DesignStmFlow*(EnthalpyIn-EnthalpyOut)*0.000293
			END
	FROM Auxiliaries LEFT JOIN NERCFactors nf ON nf.Refnum = Auxiliaries.Refnum
	WHERE Auxiliaries.Refnum IN (SELECT Refnum FROM @Units)


	--     GenerationPcnt
	SELECT gtc.Refnum, gtc.NetMWH, gtc.AdjNetMWH, SSE = gtc.StationSvcElec, StmMWH = gtc.StmSalesMWH,
		ga.PurElec, ScrubberMWH = ISNULL(ScrConsMWH,0),
		AuxMWH = (SELECT SUM(AuxLvlMWH) FROM Auxiliaries WHERE Auxiliaries.Refnum = gtc.Refnum),
		PotGross = CAST(NULL as float)
	INTO #GenPcnt
	FROM GenerationTotCalc gtc LEFT JOIN GenerationAnn ga ON ga.Refnum = gtc.Refnum
	LEFT JOIN Misc ON Misc.Refnum = gtc.Refnum
	WHERE gtc.Refnum IN (SELECT Refnum FROM @Units)

	UPDATE #GenPcnt
	SET PotGross = ISNULL(NetMWH,0) + ISNULL(StmMWH,0) + ISNULL(AuxMWH,0) + ISNULL(SSE,0)

	UPDATE #GenPcnt
	SET SSE = ISNULL(SSE,0) - ISNULL(ScrubberMWH,0)

	UPDATE GenerationTotCalc
	SET AuxiliaryMWH = ISNULL(g.AuxMWH,0), PotentialGrossMWH = g.PotGross,
		ScrubberMWH = CASE WHEN g.ScrubberMWH > 0 THEN g.ScrubberMWH END, AdjNetMWHwoScrubber = g.AdjNetMWH + ISNULL(g.ScrubberMWH,0),
		StationSvcPcnt = NULL, AuxiliaryPcnt = NULL, StmSalesPcnt = NULL, ScrubberPcnt = NULL, NetPcnt = NULL, 
		InternalUsagePcnt = NULL, InternalLessScrubberPcnt = NULL
	FROM GenerationTotCalc INNER JOIN #GenPcnt g ON g.Refnum = GenerationTotCalc.Refnum

	UPDATE GenerationTotCalc SET
		StationSvcPcnt = (ISNULL(g.SSE,0)+ISNULL(g.PurElec,0))/g.PotGross*100, 
		AuxiliaryPcnt = ISNULL(AuxMWH,0)/g.PotGross*100,
		StmSalesPcnt = ISNULL(g.StmMWH,0)/g.PotGross*100, 
		ScrubberPcnt = CASE WHEN g.ScrubberMWH > 0 THEN g.ScrubberMWH/g.PotGross*100 END,
		NetPcnt = g.NetMWH/g.PotGross*100, 
		InternalUsagePcnt = (ISNULL(g.SSE,0)+ISNULL(g.PurElec,0)+ISNULL(AuxMWH,0)+ISNULL(g.ScrubberMWH,0))/g.PotGross*100,
		InternalLessScrubberPcnt = (ISNULL(g.SSE,0)+ISNULL(g.PurElec,0)+ISNULL(AuxMWH,0))/g.PotGross*100
	FROM GenerationTotCalc INNER JOIN #GenPcnt g ON g.Refnum = GenerationTotCalc.Refnum
	WHERE g.PotGross > 0



	UPDATE GenerationTotCalc SET 
		TotalOutputMBTU = b.TotalMBTU, 
		TotalOutputGJ = b.TotalGJ,
		TotalOutputMBTU2Yr = CASE WHEN b.TotalMBTUPriorYr > 0 THEN (b.TotalMBTU + b.TotalMBTUPriorYr)/2 ELSE b.TotalMBTU END, 
		TotalOutputGJ2Yr = CASE WHEN b.TotalGJPriorYr > 0 THEN (b.TotalGJ + b.TotalGJPriorYr)/2 ELSE b.TotalGJ END
		FROM (
			SELECT gtc.Refnum, 
				TotalMBTU =  ISNULL(s.StmSalesMBTU,0) +  ISNULL(p.ElecMBTU,0),
				TotalGJ = 1.055056 * (ISNULL(s.StmSalesMBTU,0) +  ISNULL(p.ElecMBTU,0)),
				TotalMBTUPriorYr =  ISNULL(s2.StmSalesMBTU,0) +  ISNULL(p2.ElecMBTU,0),
				TotalGJPriorYr = 1.055056 * (ISNULL(s2.StmSalesMBTU,0) + ISNULL(p2.ElecMBTU,0))

			FROM GenerationTotCalc gtc
				LEFT JOIN (SELECT Refnum, StmSalesMBTU = SUM(ISNULL(StmSalesMBTU,0)) FROM SteamSales WHERE YEAR(Period) = @studyyear GROUP BY Refnum) s ON s.Refnum = gtc.Refnum
				LEFT JOIN (SELECT Refnum, ElecMBTU = SUM(ISNULL(ElecMBTU,0)) FROM PowerGeneration WHERE YEAR(Period) = @studyyear GROUP BY Refnum) p on p.Refnum = gtc.Refnum
				LEFT JOIN (SELECT Refnum, StmSalesMBTU = SUM(ISNULL(StmSalesMBTU,0)) FROM SteamSales WHERE YEAR(Period) = @studyyear - 1 GROUP BY Refnum) s2 ON s2.Refnum = gtc.Refnum
				LEFT JOIN (SELECT Refnum, ElecMBTU = SUM(ISNULL(ElecMBTU,0)) FROM PowerGeneration WHERE YEAR(Period) = @studyyear - 1 GROUP BY Refnum) p2 on p2.Refnum = gtc.Refnum

			WHERE gtc.Refnum IN (SELECT Refnum FROM @Units)
			--GROUP BY gtc.Refnum
			) b
		WHERE b.Refnum = GenerationTotCalc.Refnum

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcSteam'
	
	EXEC sp_InsertMessage @siteid, 35, 'CalcSteam', 'CalcSteam'


END TRY
BEGIN CATCH

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcSteam'

	EXEC sp_InsertMessage @siteid, 36, 'CalcSteam', 'CalcSteam'


END CATCH