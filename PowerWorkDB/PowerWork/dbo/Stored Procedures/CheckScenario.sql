﻿CREATE PROCEDURE CheckScenario @TableName char (30), @RecCount tinyint OUTPUT
AS
SELECT @RecCount = (SELECT count(*) FROM syscolumns
	WHERE Name = 'Scenario'
	AND id = (SELECT id FROM sysobjects WHERE Name = @TableName))
