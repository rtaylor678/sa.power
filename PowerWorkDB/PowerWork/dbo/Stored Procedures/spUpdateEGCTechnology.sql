﻿-- =================================================================
-- Author:		Dennis Burdett
-- Create date: July 15, 2008
-- Description:	Updates the EGC Technology field in TSort table
-- Modified: 02/19/09 Dennis - changed the name of the Technology field and now looks to see if
--   the EGCManualTech is True or False
-- =================================================================
CREATE PROCEDURE [dbo].[spUpdateEGCTechnology] 
AS
BEGIN
	SET NOCOUNT ON;

	-- All Coal: 'COAL'
	UPDATE dbo.TSort
	SET EGCTechnology='COAL'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE fuelgroup LIKE 'coal') AND EGCManualTech=0
	 
	-- CC Electric: 'CCGT-E'
	UPDATE dbo.TSort
	SET EGCTechnology='CCGT-E'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE crvsizegroup LIKE 'ce') AND EGCManualTech=0

	-- CC Cogen: 'COGEN'
	UPDATE dbo.TSort
	SET EGCTechnology='COGEN'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE (crvsizegroup LIKE 'cg1' or crvsizegroup LIKE 'cg2' or crvsizegroup LIKE 'cg3'))
		 AND EGCManualTech=0
	 
	-- SGO: 'SGO GAS&OIL'
	UPDATE dbo.TSort
	SET EGCTechnology='SGO GAS&OIL'
	WHERE refnum IN ( SELECT refnum FROM breaks
						WHERE crvgroup LIKE 'gas') AND EGCManualTech=0

	-- Multi: 'MULTI'
	UPDATE dbo.TSort
	SET EGCTechnology='MULTI'
	WHERE ((Coal_NDC > 0 AND Oil_NDC > 0) 
		OR (Coal_NDC > 0 AND Gas_NDC > 0) 
		OR (Coal_NDC > 0 AND CTG_NDC > 0)
		OR (Oil_NDC > 0 AND CTG_NDC > 0)
		OR (Gas_NDC > 0 AND Oil_NDC > 0) 
		OR (Gas_NDC > 0 AND CTG_NDC >0)) AND EGCManualTech=0

	SELECT DISTINCT EGCTechnology FROM tsort
END
