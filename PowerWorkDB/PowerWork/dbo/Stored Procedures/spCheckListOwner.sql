﻿
CREATE  PROCEDURE spCheckListOwner
	@ListName RefListName = NULL,
	@ListNo RefListNo = NULL,
	@CurrUser varchar(30) = NULL
AS
DECLARE @UName varchar(30), @UGroup varchar(30)
IF @ListNo Is NULL
	SELECT @ListNo = RefListNo FROM RefList_LU
	WHERE ListName = @ListName
IF @ListNo IS NOT NULL
BEGIN
	IF @CurrUser = 'sa'
		SELECT @CurrUser = 'dbo'
	IF @CurrUser IS NULL
		SELECT @UName = User_Name(), @UGroup = Name 
		FROM sysusers
		WHERE uid = (SELECT gid from sysusers
				WHERE name = User_Name())
	ELSE
		SELECT @UName = @CurrUser, @UGroup = Name 
		FROM sysusers
		WHERE uid = (SELECT gid from sysusers
			WHERE name = @CurrUser)
	IF @UGroup IN ('developer', 'Datagrp') OR @UName = 'dbo'
		RETURN 2
	ELSE
	BEGIN
		IF EXISTS (SELECT * FROM RefList_LU
			WHERE RefListNo = @ListNo AND
			(Owner <> @UName AND Owner IS NOT NULL))
			RETURN -101
		ELSE
			RETURN 1
	END
END

