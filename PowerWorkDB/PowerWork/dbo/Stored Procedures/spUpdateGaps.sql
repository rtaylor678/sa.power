﻿
CREATE      PROCEDURE [dbo].[spUpdateGaps](@Refnum Refnum) AS
DECLARE @StudyYear smallint, @TargetGroup Char(12), @MaintCompTargetGroup char(12), @Scrubber char(1),
	@tSiteWagesBen real, @tCentralWagesBen real, @tContract real,
	@tOverhaulAdj real, @tMaterials real, @tTaxInsurEnvir real,
	@tAGPers real, @tOthFixed real, @tOtherVar real,
	@tOCCCompensation real, @tMPSCompensation real,
	@tOCCOVTPcnt real, @tMPSOVTPcnt real,
	@tInternalEnergyUsage real, @tHeatRateVariance real

DECLARE @gSiteWagesBen real, @gCentralWagesBen real,
	@gContract real, @gOverhaulAdj real, @gMaterials real, 
	@gTaxInsurEnvir real, @gAGPers real, @gOthFixed real, @gOtherVar real, 
	@gOCCCompensation real, @gMPSCompensation real,
	@gOCCOVTPcnt real, @gMPSOVTPcnt real, @gInternalEnergyUsage real, @gHeatRateVariance real

DECLARE @HeatRateVar real, @InternalEnergy real

SELECT @StudyYear = dbo.StudyYear(@Refnum)

SELECT @TargetGroup = CASE 
		WHEN b.CombinedCycle = 'Y' THEN 'CC'
		WHEN b.FuelGroup = 'Gas & Oil' THEN 'Gas & Oil'
		WHEN b.CoalNMCGroup2 IS NOT NULL THEN 'Coal' + CONVERT(varchar(1), b.CoalNMCGroup2)
		ELSE '' -- not sure if this should be '' or NULL (was NULL prior to 10/3/13 but gave errors on TransAlta Sundance because of units 1&2 - SW
		END,
	@Scrubber = Scrubbers
	FROM Breaks b WHERE Refnum = @Refnum

SELECT @MaintCompTargetGroup = @TargetGroup

IF @StudyYear = 1999 AND @TargetGroup = 'Coal3'
	SELECT @TargetGroup = 'Coal3' + CASE WHEN @Scrubber = 'Y' THEN 'S' ELSE 'U' END

IF @StudyYear = 2000 AND @TargetGroup IN ('CC', 'Gas & Oil')
	SELECT @MaintCompTargetGroup = 'SteamGO'

IF @StudyYear>=2001 AND @TargetGroup = 'Gas & Oil'
	SELECT @MaintCompTargetGroup = 'SteamGO' 

IF @TargetGroup = 'CC' AND NOT EXISTS (SELECT * FROM Targets WHERE StudyYear = @StudyYear AND TargetGroup = @TargetGroup)
	SELECT @TargetGroup = 'Gas & Oil'
	
SELECT	@tSiteWagesBen = SiteWagesBen , 
	@tCentralWagesBen = CentralWagesBen , 
	@tContract = Contract ,
	@tOverhaulAdj = OverhaulAdj , 
	@tMaterials = Materials , 
	@tTaxInsurEnvir = TaxInsurEnvir ,
	@tAGPers = AGPers , 
	@tOthFixed = OthFixed , 
	@tOtherVar = OtherVar ,
	@tOCCCompensation = OCCCompensation , 
	@tMPSCompensation = MPSCompensation ,
	@tOCCOVTPcnt = OCCOVTPcnt , 
	@tMPSOVTPcnt = MPSOVTPcnt ,
	@tInternalEnergyUsage = InternalEnergyLessScrub , 
	@tHeatRateVariance = HeatRateVariance
FROM Targets WHERE StudyYear = @StudyYear AND TargetGroup = @TargetGroup
--IF @Scrubber = 'Y'
--	SELECT @tInternalEnergyUsage = InternalEnergyUsage
--	FROM Targets WHERE StudyYear = @StudyYear AND TargetGroup = @TargetGroup
SELECT @gHeatRateVariance = Variance - @tHeatRateVariance,
	@HeatRateVar = Variance
FROM HRVariance 
WHERE Refnum = @Refnum
SELECT @gInternalEnergyUsage = ISNULL(InternalLessScrubberPcnt, InternalUsagePcnt) - @tInternalEnergyUsage,
	@InternalEnergy = ISNULL(InternalLessScrubberPcnt, InternalUsagePcnt)
FROM GenerationTotCalc
WHERE Refnum = @Refnum
 
SELECT 	@gSiteWagesBen = o.SiteWagesBen - @tSiteWagesBen,
	@gCentralWagesBen = o.CentralWagesBen - @tCentralWagesBen,
	@gContract = o.Contract - @tContract,
	@gOverhaulAdj = o.OverhaulAdj - @tOverhaulAdj,
	@gMaterials = o.Materials - @tMaterials,
	@gTaxInsurEnvir = o.TaxInsurEnvir - @tTaxInsurEnvir,
	@gAGPers = o.AGPers - @tAGPers,
	@gOthFixed = o.OthFixed - @tOthFixed,
	@gOtherVar = o.OtherVar - @tOtherVar
FROM PresOpex o WHERE Refnum = @Refnum AND DataType = 'MWH'

SELECT 	@gOCCCompensation = g.OCCAnnComp - @tOCCCompensation
FROM Gensum g 
WHERE g.Refnum = @Refnum 

SELECT 	@gMPSCompensation = g.MPSAnnComp - @tMPSCompensation
FROM Gensum g
WHERE g.Refnum = @Refnum 

SELECT 	@gOCCOVTPcnt = o.SiteOVTPcnt - @tOCCOVTPcnt
FROM PersSTCalc o WHERE o.Refnum = @Refnum AND o.SectionID = 'TOCC'

SELECT 	@gMPSOVTPcnt = m.SiteOVTPcnt - @tMPSOVTPcnt
FROM PersSTCalc m WHERE m.Refnum = @Refnum AND m.SectionID = 'TMPS'

DELETE FROM UnitGaps WHERE Refnum = @Refnum

INSERT INTO UnitGaps (Refnum, TargetGroup, SiteWagesBenGap, CentralWagesBenGap,
	ContractGap, OverhaulAdjGap, MaterialsGap, TaxInsurEnvirGap, AGPersGap,
	OthFixedGap, OtherVarGap, OCCCompensationGap, MPSCompensationGap,
	OCCOVTPcntGap, MPSOVTPcntGap, InternalEnergyUsageGap, HeatRateVarianceGap,
	HeatRateVariance, InternalEnergyUsage)
VALUES (@Refnum, @TargetGroup, @gSiteWagesBen, @gCentralWagesBen,
	@gContract, @gOverhaulAdj, @gMaterials, @gTaxInsurEnvir, @gAGPers,
	@gOthFixed, @gOtherVar, @gOCCCompensation, @gMPSCompensation,
	@gOCCOVTPcnt, @gMPSOVTPcnt, @gInternalEnergyUsage, @gHeatRateVariance,
	@HeatRateVar, @InternalEnergy)

DELETE FROM ComponentGaps WHERE Refnum = @Refnum

INSERT INTO ComponentGaps (Refnum, EquipGroup, TargetGroup, 
	AnnMaintCostMWHGap, AnnMaintCostMWH, Target)
SELECT 	m.Refnum, m.EquipGroup, @MaintCompTargetGroup, 
	m.AnnMaintCostMWH - x.AnnMaintCostMWH,
	m.AnnMaintCostMWH, x.AnnMaintCostMWH
FROM MaintEquipCalc m 
	INNER JOIN ComponentTargets x ON x.StudyYear = @StudyYear
	AND x.EquipGroup = m.EquipGroup
	AND x.TargetGroup = @MaintCompTargetGroup
WHERE m.Refnum = @Refnum

EXEC spCRVCalcs @Refnum

EXEC spCRVGap @Refnum

