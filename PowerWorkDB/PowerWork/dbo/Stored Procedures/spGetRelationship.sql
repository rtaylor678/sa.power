﻿CREATE PROCEDURE spGetRelationship @ObjNameA sysname, @ObjNameB sysname
AS
DECLARE @ObjIDA DD_ID, @ObjIDB DD_ID, @TempObj DD_ID
EXEC procGetObjectID @ObjNameA, @ObjIDA OUTPUT
EXEC procGetObjectID @ObjNameB, @ObjIDB OUTPUT
IF @ObjIDB < @ObjIDA 
BEGIN
	SELECT @TempObj = @ObjIDA
	SELECT @ObjIDA = @ObjIDB
	SELECT @ObjIDB = @TempObj
END
SELECT a.ObjectName ObjectA, b.ObjectName ObjectB, c.ColumnName ColumnA, d.ColumnName ColumnB
FROM DD_Objects a, DD_Objects b, DD_Columns c, DD_Columns d, DD_Relationships r
WHERE a.ObjectID = c.ObjectID AND b.ObjectID = d.ObjectID
AND ((r.ObjectA = a.ObjectID AND r.ColumnA = c.ColumnID)
AND (r.ObjectB = b.ObjectID AND r.ColumnB = d.ColumnID))
AND r.ObjectA = @ObjIDA AND r.ObjectB = @ObjIDB
