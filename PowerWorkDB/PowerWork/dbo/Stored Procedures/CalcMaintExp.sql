﻿CREATE PROC [dbo].[CalcMaintExp](@SiteID SiteID)
AS

BEGIN TRY

	--RAISERROR ('Error raised in TRY block.',11,1)

	DECLARE @CurrMMO real, @PrevMMO real, @MaintCptl real, @MaintExp real, @MMO real, @AllocMMO real, 
		@CurrTotMaint real, @PrevTotMaint real, @CurrAMMOFraction real, @PrevAMMOFraction real

	/* Get MMO costs to be allocated */
	SELECT @CurrMMO = PlantCurrMMOACost, @PrevMMO = PlantPrevMMOACost 
	FROM PlantGenData WHERE SiteID = @SiteID

	/* Calculate subtotals */
	UPDATE CptlMaintExp SET
		STMaintCptl = ISNULL(TotCptl, 0) - (ISNULL(CptlExcl,0) + ISNULL(Energy,0) + ISNULL(RegEnv,0) + ISNULL(Admin,0) + ISNULL(ConstrRmvl,0)),
		STMaintExp = ISNULL(TotMaintExp,0) - ISNULL(MaintExpExcl,0),
		STMMO = ISNULL(TotMMO,0) - ISNULL(MMOExcl,0),
		InvestCptl = ISNULL(Energy,0) + ISNULL(RegEnv,0) + ISNULL(Admin,0) + ISNULL(ConstrRmvl,0)
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	SELECT CptlCode, PlantDirMaint = SUM(STMaintCptl + STMaintExp + STMMO), 
		PlantAMMO = CASE CptlCode WHEN 'CURR' THEN ISNULL(@CurrMMO,0) WHEN 'P1' THEN ISNULL(@PrevMMO,0) ELSE 0 END,
		AMMOFraction = CAST(0.0 as real)
	INTO #PlantMaint
	FROM CptlMaintExp
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND CptlCode IN ('CURR','P1')
	GROUP BY CptlCode

	UPDATE #PlantMaint
	SET AMMOFraction = PlantAMMO / PlantDirMaint
	WHERE PlantAMMO > 0 AND PlantDirMaint > 0

	UPDATE CptlMaintExp SET
		AllocMMO = (STMaintCptl + STMaintExp + STMMO) * ISNULL((SELECT AMMOFraction FROM #PlantMaint x WHERE x.CptlCode = CptlMaintExp.CptlCode), 0)
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	UPDATE CptlMaintExp SET
		TotMaint = STMaintCptl + STMaintExp + STMMO + AllocMMO,
		STMMO = STMMO + AllocMMO
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
	
	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcMaintExp'
	
	--INSERT PowerWork.dbo.MessageLog (Refnum, Source, Severity, MessageID, MessageText, SysAdmin, Consultant, Pricing, Audience4, Audience5, Audience6, Audience7, MessageTime)
		--VALUES (@SiteID, 'CalcMaintExp', 'I', 35, 'CalcMaintExp was completed successfully.', 0, 0, 0, 0, 0, 0, 0, GETDATE())
	EXEC sp_InsertMessage @siteid, 35, 'CalcMaintExp', 'CalcMaintExp'

END TRY
BEGIN CATCH

	DELETE FROM PowerWork.dbo.MessageLog WHERE Refnum = @SiteID AND Source = 'CalcMaintExp'

	--INSERT PowerWork.dbo.MessageLog (Refnum, Source, Severity, MessageID, MessageText, SysAdmin, Consultant, Pricing, Audience4, Audience5, Audience6, Audience7, MessageTime)
	--	VALUES (@SiteID, 'CalcMaintExp', 'E', 36, 'CalcMaintExp failed.', 0, 0, 0, 0, 0, 0, 0, GETDATE())

	EXEC sp_InsertMessage @siteid, 36, 'CalcMaintExp', 'CalcMaintExp'

	RAISERROR ('Error raised in CATCH block.',11,1)

END CATCH
