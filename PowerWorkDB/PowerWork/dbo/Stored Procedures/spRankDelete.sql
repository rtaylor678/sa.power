﻿/****** Object:  Stored Procedure dbo.spRankDelete    Script Date: 4/18/2003 4:39:01 PM ******/
CREATE PROCEDURE spRankDelete
@RefListNo RefListNo,
@BreakID DD_ID,
@RankVariableID DD_ID
AS
DELETE FROM Rank
WHERE RankSpecsID IN (
	SELECT RankSpecsID 
	FROM RankSpecs_LU
	WHERE RefListNo = @RefListNo 
	AND BreakID = @BreakID AND RankVariableID = @RankVariableID)
