﻿CREATE PROCEDURE [dbo].[util_search]
@atext varchar(96)
AS
BEGIN /* ---PROCEDURE--- */
/*
$Header: $
Description: this is a utility search procedure for searching MS SQL SERVER objects
Instructions: the wildcard character '%' can be used within a supplied search string
*/
declare @ltext varchar(128)
/* prepare search string for use in 'LIKE' operation; see help on LIKE re _ and [_] */
set @ltext = '%' + replace(@atext,'_','[_]') + '%'
select distinct @@servername as svr, o.type + ' ' + o.name as ref
from sysobjects o with (nolock)
where o.name like @ltext
union
select distinct @@servername as svr, o.type + ' ' + o.name as ref
from syscomments c with (nolock), sysobjects o with (nolock)
where o.id = c.id and c.text like @ltext
union
select distinct @@servername, o.type + ' ' + o.name +'.' + c.name + 
 ' ' + s.name + 
 ' ' + convert(varchar,c.length) + 
case when c.length <> c.prec then ',' + convert(varchar,c.prec) else '' end
from sysobjects o with (nolock), syscolumns c with (nolock)
,
sys.type  s with (nolock)
where o.id = c.id and c.name like @ltext and
c.xtype = s.system_type_id
union
select distinct @@servername, 'JOB:STEP: ' + j.name + ':' + convert(varchar,s.step_id) + ':' + 
 s.step_name + ':' + isnull(s.command,'')
from msdb.dbo.sysjobs j with (nolock), msdb.dbo.sysjobsteps s with (nolock)
where j.job_id = s.job_id and (s.command like @ltext or s.step_name like @ltext or s.subsystem like @ltext)
union
select distinct @@servername, 'JOB: ' + j.name + ':' + isnull(j.description,'')
from msdb.dbo.sysjobs j with (nolock)
where j.name like @ltext or j.description like @ltext
union
/*select distinct @@servername, 'JOB:SCHEDULE: ' + j.name + ':' + s.name
from msdb.dbo.sysjobs j with (nolock), msdb.dbo.sysjobschedules s with (nolock)
where j.job_id = s.job_id and s.name like @ltext
union*/
select distinct @@servername, 'DTS: ' + d.name + ':' + d.description
from msdb.dbo.sysdtspackages d with (nolock)
where (d.name like @ltext or d.description like @ltext or d.owner like @ltext) and d.createdate = (
select max(createdate) from msdb.dbo.sysdtspackages d2 with (nolock) where d.id = d2.id )
order by ref, svr
/* $History: $ 
*/
END /* ---PROCEDURE--- */
