﻿CREATE PROC GetMedian(@ListName varchar(30), @RankVariableID smallint, @BreakCondition varchar(50), @BreakValue varchar(15))
AS
SET NOCOUNT ON
CREATE TABLE #Data (VarValue real NOT NULL)
insert into #Data
select TOP 1 Value
from _rankview
where listname = @ListName and rankvariableid = @RankVariableID and Breakcondition = @BreakCondition AND BreakValue = @BreakValue 
and Percentile >= 50
order by Percentile ASC
insert into #Data
select TOP 1 Value
from _rankview
where listname = @ListName and rankvariableid = @RankVariableID and Breakcondition = @BreakCondition AND BreakValue = @BreakValue 
and Percentile < 50
order by Percentile DESC
SET NOCOUNT OFF
select AVG(VarValue) AS AvgValue FROM #Data
DROP TABLE #Data
