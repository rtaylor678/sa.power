﻿
CREATE  PROC [dbo].[spCRVGap](@Refnum Refnum) AS
DECLARE @StudyYear smallint, @PricingHub varchar(12), 
	@CRVSizeGroup varchar(6), @CRVSizeGroup2 varchar(6)
DECLARE @AvgLRO_Unp real
DECLARE @tPeakUnavail_Unp real, @tHeatRate real, @tTotCashLessFuel real, @tTotCashLessFuelEmm real,
		@tTotCashLessFuelEmmMW real, @tTotCashLessFuelEmmEGC real,
		@tPlantManageableMWH real
DECLARE @LostPeakMWH_Unp real, @PeakUnavail_Unp real, @TotPeakMWH real, 
	@HeatRate real, @TotCashLessFuel real, @TotCashLessFuelEmm real, @AvgFuelCost real, @AdjNetMWH real,
	@TotCashLessFuelEmmMW real, @TotCashLessFuelEmmEGC real,
	@ActCashLessFuel real
DECLARE @ScrubberCost real, @ScrubberMWH real
DECLARE @RevenueGap real, @FuelOperGap real, @OpexGap real, @OpexEmmGap real, @TotGap real, @PlantManageableGap real
DECLARE @NMC real, @OpexEmmGapMW real, @OpexEmmGapEGC real, @EGC real


SELECT @StudyYear = dbo.StudyYear(@Refnum), @PricingHub = dbo.PricingHubByRefnum(@Refnum)

SELECT @CRVSizeGroup = ISNULL(CRVSizeGroup,''), @CRVSizeGroup2 = CRVSizeGroup2
FROM Breaks
WHERE Refnum = @Refnum
--SW 10/3/13 not sure about the ISNULL above but I inserted it so that TransAlta Sundance units 1&2 would calc (they had no data but were attached to the rest)

SELECT @NMC = NMC FROM NERCFactors WHERE Refnum = @Refnum
SELECT @EGC = EGC FROM GenSum WHERE Refnum = @Refnum

DELETE FROM CRVGap WHERE Refnum = @Refnum

--SELECT @AvgLRO_Unp = AVG(Price)
--FROM Prices
--WHERE DATEPART(yy, StartTime) = @StudyYear
--AND PricingHub = @PricingHub

SELECT @AvgLRO_Unp = LRO_Unp*1000/PeakMWHLost_Unp 
FROM CommUnavail
WHERE Refnum = @Refnum and PeakMWHLost_Unp > 0

IF @AvgLRO_Unp IS NULL
	SELECT @AvgLRO_Unp = 0

SELECT @PeakUnavail_Unp = PeakUnavail_Unp, @TotPeakMWH = TotPeakMWH
FROM CommUnavail WHERE Refnum = @Refnum

--SELECT @LostPeakMWH_Unp = SUM(tl.PeakHrs*tl.LostMW)
--FROM TSort INNER JOIN NERCTurbine n ON n.Refnum = TSort.Refnum
--INNER JOIN EVNTTL tl ON  tl.UtilityUnitCode = n.UtilityUnitCode
--	AND tl.Evnt_Year = TSort.EvntYear
--INNER JOIN EvntRpts e2 ON e2.UNITABBR = n.UNITABBR
--	AND DATEPART(yy, e2.Start_Date) = tl.EVNT_YEAR AND e2.EVNT_NO = tl.EVNT_NO
--INNER JOIN CauseCodes c ON c.Cause_Code = e2.Cause_Code
--WHERE tl.EVNT_Category IN ('F','M') AND c.SAIMajorEquip IN ('Boiler','Turbine','Balance Of Plant', 'Combustion Turbine')
--AND c.SAIMinorEquip <> 'Coal Pulverizers/Hammer Mills (CPHM)'
--AND tl.PeakHrs*tl.LostMW>0 AND e2.EVNT_NO <> 7777
--and TSort.Refnum = @Refnum

--SW 2/3/16 change because old EvntRpts view was removed during change of DB to DBS1
SELECT @LostPeakMWH_Unp = SUM(lro.PeakHrs*tl.LostMW)
FROM TSort t
LEFT JOIN NERCTurbine n ON n.Refnum = t.Refnum
LEFT JOIN EVNTTL tl ON  tl.UtilityUnitCode = n.UtilityUnitCode AND tl.Evnt_Year = t.EvntYear
--INNER JOIN EvntRpts e2 ON e2.UNITABBR = n.UNITABBR AND DATEPART(yy, e2.Start_Date) = tl.EVNT_YEAR AND e2.EVNT_NO = tl.EVNT_NO
LEFT JOIN Events e2 ON e2.refnum = n.refnum and e2.evnt_no = tl.evnt_no
LEFT JOIN CauseCodes c ON c.Cause_Code = e2.Cause_Code
left join eventlro lro on lro.refnum = t.refnum and lro.evnt_no = tl.evnt_no
WHERE e2.EVNT_Category IN ('F','M') 
AND c.SAIMajorEquip IN ('Boiler','Turbine','Balance Of Plant', 'Combustion Turbine')
AND c.SAIMinorEquip <> 'Coal Pulverizers/Hammer Mills (CPHM)'
AND lro.PeakHrs*tl.LostMW>0 --AND e2.EVNT_NO <> 7777
 and t.Refnum = @Refnum


IF @TotPeakMWH > 0 
BEGIN
	IF @LostPeakMWH_Unp*100/@TotPeakMWH < @PeakUnavail_Unp
		SELECT @PeakUnavail_Unp = @LostPeakMWH_Unp*100/@TotPeakMWH
END

SELECT @TotCashLessFuel = TotCashLessFuel, @TotCashLessFuelEmm = TotCashLessFuelEm,
	@ActCashLessFuel = ActCashLessFuel
	FROM OpexCalc 
	WHERE Refnum = @Refnum AND DataType = 'MWH'

SELECT @TotCashLessFuelEmmMW = TotCashLessFuelEm
	FROM OpexCalc 
	WHERE Refnum = @Refnum AND DataType = 'KW'

SELECT @TotCashLessFuelEmmEGC = TotCashLessFuelEm
	FROM OpexCalc 
	WHERE Refnum = @Refnum AND DataType = 'EGC'


SELECT @HeatRate = HeatRate, @AvgFuelCost = AvgFuelCostMBTU, @AdjNetMWH = AdjNetMWH
	FROM GenSum WHERE Refnum = @Refnum

SELECT @ScrubberCost = c.TotScrubber-c.ScrubberPowerCost, @ScrubberMWH = ISNULL(ScrConsMWH,0)
	FROM MiscCalc c INNER JOIN Misc m ON m.Refnum = c.Refnum
	WHERE c.Refnum = @Refnum

-- DB 11/12/08 slight modification to make both of these adjustments in the same statement instead of 3 "if"
IF @ScrubberCost > 0
BEGIN
	SELECT @TotCashLessFuel = @TotCashLessFuel - @ScrubberCost*1000/@AdjNetMWH
	SELECT @TotCashLessFuelEmm = @TotCashLessFuelEmm - @ScrubberCost*1000/@AdjNetMWH
	SELECT @TotCashLessFuelEmmMW = @TotCashLessFuelEmmMW - @ScrubberCost/@NMC
	SELECT @ActCashLessFuel = @ActCashLessFuel - @ScrubberCost*1000/@AdjNetMWH
END

IF @ScrubberMWH > 0 
	SELECT @HeatRate = @HeatRate*(@AdjNetMWH/(@AdjNetMWH+@ScrubberMWH))

-- Calculate gaps
SELECT 	@tPeakUnavail_Unp = PeakUnavail_Unp, @tHeatRate = HeatRate, 
	@tTotCashLessFuel = TotCashLessFuelMWH, @tTotCashLessFuelEmm = TotCashLessFuelEmmMWH,
	@tTotCashLessFuelEmmMW = TotCashLessFuelEmmMW, @tTotCashLessFuelEmmEGC = TotCashLessFuelEmmEGC,
	@tPlantManageableMWH = PlantManageableMWH
FROM CRVGapFactors f
WHERE StudyYear = @StudyYear AND CRVSizeGroup = @CRVSizeGroup

SELECT @RevenueGap = (@PeakUnavail_Unp - @tPeakUnavail_Unp)/100 * @AvgLRO_Unp * @TotPeakMWH / 1000
SELECT @FuelOperGap = (@HeatRate - @tHeatRate) * @AvgFuelCost * @AdjNetMWH / 1000000
SELECT @OpexGap = (@TotCashLessFuel - @tTotCashLessFuel) * @AdjNetMWH / 1000
SELECT @OpexEmmGap = (@TotCashLessFuelEmm - @tTotCashLessFuelEmm) * @AdjNetMWH / 1000
SELECT @OpexEmmGapMW = (@TotCashLessFuelEmmMW - @tTotCashLessFuelEmmMW) * @NMC
SELECT @OpexEmmGapEGC = (@TotCashLessFuelEmmEGC - @tTotCashLessFuelEmmEGC) * @EGC
SELECT @PlantManageableGap = (@ActCashLessFuel - @tPlantManageableMWH) * @AdjNetMWH / 1000


SELECT @TotGap = CASE WHEN @RevenueGap>0 THEN @RevenueGap ELSE 0 END 
		+ CASE WHEN @FuelOperGap>0 THEN @FuelOperGap ELSE 0 END 
		+ CASE WHEN @OpexEmmGap>0 THEN @OpexEmmGap ELSE 0 END
		
INSERT INTO CRVGap (Refnum, CRVSizeGroup, AdjNetMWH,
	PeakUnavail_Unp, TargetPeakUnavail_Unp, MktAvgPrice, RevenueGapKUS,
	HeatRate, TargetHeatRate, AvgFuelCostMBTU, FuelOperGapKUS,
	TotCashLessFuelMWH, TargetTotCashLessFuelMWH, TotCashLessFuelEmmMWH, TargetTotCashLessFuelEmmMWH, 
	OpexGapKUS, OpexEmmGapKUS, PlantManGapKUS, TotGapKUS, OpexEmmGapKUSMW, OpexEmmGapKUSEGC)
VALUES (@Refnum, @CRVSizeGroup, @AdjNetMWH,
	@PeakUnavail_Unp, @tPeakUnavail_Unp, @AvgLRO_Unp, @RevenueGap,
	@HeatRate, @tHeatRate, @AvgFuelCost, @FuelOperGap,
	@TotCashLessFuel, @tTotCashLessFuel, @TotCashLessFuelEmm, @tTotCashLessFuelEmm, 
	@OpexGap, @OpexEmmGap, @PlantManageableGap, @TotGap, @OpexEmmGapMW, @OpexEmmGapEGC)

-- Calculate gaps for overlapping CRV Size Group
SELECT @tPeakUnavail_Unp = NULL, @tHeatRate = NULL, @tTotCashLessFuel = NULL

SELECT 	@tPeakUnavail_Unp = PeakUnavail_Unp, @tHeatRate = HeatRate, 
	@tTotCashLessFuel = TotCashLessFuelMWH,	@tTotCashLessFuelEmm = TotCashLessFuelEmmMWH,	
	@tPlantManageableMWH = PlantManageableMWH
FROM CRVGapFactors f
WHERE StudyYear = @StudyYear AND CRVSizeGroup = @CRVSizeGroup2

IF @tPeakUnavail_Unp IS NOT NULL
BEGIN
	SELECT @RevenueGap = (@PeakUnavail_Unp - @tPeakUnavail_Unp)/100 * @AvgLRO_Unp * @TotPeakMWH / 1000
	SELECT @FuelOperGap = (@HeatRate - @tHeatRate) * @AvgFuelCost * @AdjNetMWH / 1000000
	SELECT @OpexGap = (@TotCashLessFuel - @tTotCashLessFuel) * @AdjNetMWH / 1000
	SELECT @OpexEmmGap = (@TotCashLessFuelEmm - @tTotCashLessFuelEmm) * @AdjNetMWH / 1000
	SELECT @PlantManageableGap = (@ActCashLessFuel - @tPlantManageableMWH) * @AdjNetMWH / 1000

	SELECT @TotGap = CASE WHEN @RevenueGap>0 THEN @RevenueGap ELSE 0 END 
			+ CASE WHEN @FuelOperGap>0 THEN @FuelOperGap ELSE 0 END 
			+ CASE WHEN @OpexEmmGap>0 THEN @OpexEmmGap ELSE 0 END
	INSERT INTO CRVGap (Refnum, CRVSizeGroup, AdjNetMWH,
		PeakUnavail_Unp, TargetPeakUnavail_Unp, MktAvgPrice, RevenueGapKUS,
		HeatRate, TargetHeatRate, AvgFuelCostMBTU, FuelOperGapKUS,
		TotCashLessFuelMWH, TargetTotCashLessFuelMWH, TotCashLessFuelEmmMWH, TargetTotCashLessFuelEmmMWH,
		OpexGapKUS, OpexEmmGapKUS, PlantManGapKUS, TotGapKUS)
	VALUES (@Refnum, @CRVSizeGroup2, @AdjNetMWH,
		@PeakUnavail_Unp, @tPeakUnavail_Unp, @AvgLRO_Unp, @RevenueGap,
		@HeatRate, @tHeatRate, @AvgFuelCost, @FuelOperGap,
		@TotCashLessFuel, @tTotCashLessFuel, @TotCashLessFuelEmm, @tTotCashLessFuelEmm, 
		@OpexGap, @OpexEmmGap, @PlantManageableGap, @TotGap)
END




