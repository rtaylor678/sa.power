﻿CREATE FUNCTION [dbo].[GetSANumber](@Refnum dbo.Refnum)
RETURNS int
AS
BEGIN
	DECLARE @SANumber int
	
	SELECT @SANumber = SANumber
	FROM dbo.CoRef WHERE Refnum = dbo.FormatRefnum(@Refnum, 0)
		
	RETURN @SANumber
END
