﻿
CREATE  FUNCTION [dbo].[RefnumBase](@Refnum Refnum)
RETURNS varchar(12)
AS
BEGIN 
	DECLARE @RefnumBase varchar(12)

    SELECT @RefnumBase = 
		CASE 
			WHEN SUBSTRING(@Refnum, LEN(RTRIM(@Refnum)),1)='P' THEN 
				 SUBSTRING(@Refnum, 1, LEN(RTRIM(@Refnum))-3) 
		ELSE 
			SUBSTRING(@Refnum, 1, LEN(RTRIM(@Refnum))-2) 
		END

    RETURN @RefnumBase
END

