﻿CREATE Function dbo.CsvToVarchar( @Array varchar(1000)) 
returns @CharTable table 
	(CharValue  varchar(18))
AS
begin

	declare @separator char(1)
	set @separator = ','

	declare @separator_position int 
	declare @array_value varchar(1000) 
	
	set @array = @array + ','
	
	while patindex('%,%' , @array) <> 0 
	begin
	
	  select @separator_position =  patindex('%,%' , @array)
	  select @array_value = left(@array, @separator_position - 1)
	
		Insert @CharTable
		Values (Cast(@array_value as varchar(10)))

	  select @array = stuff(@array, 1, @separator_position, '')
	end

	return
end
