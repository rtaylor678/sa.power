﻿


CREATE VIEW _vPerf01 AS

SELECT t.Refnum, t.TurbineID, Period = DATEADD(dd, -1, DATEADD(mm, 1, REPT_PERD+'/01/'+CONVERT(varchar(4), EVNT_YEAR))),
p.GMC, p.GDC, p.GA_GEN, p.NMC, p.NDC, p.NA_GEN, p.LOAD_CHAR, p.ATM_STARTS, p.ACT_STARTS, p.QTRDATA
FROM Perf01 p 
INNER JOIN NERCTurbine t ON p.UTIL_CODE = t.Util_Code AND p.UNIT_CODE = t.Unit_Code



