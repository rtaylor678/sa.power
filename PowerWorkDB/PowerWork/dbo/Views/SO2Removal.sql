﻿
CREATE VIEW [dbo].[SO2Removal] AS

SELECT c.Refnum, TotScrubber, 
ISNULL(c.TotTons, 0) * ISNULL(c.SulfurPcnt,0)*2/100*0.95 - ISNULL(m.SO2Emitted,0) AS TonsSO2Removed,
CASE WHEN (ISNULL(c.TotTons, 0) * ISNULL(c.SulfurPcnt,0)*2/100*0.95 - ISNULL(m.SO2Emitted,0)) <> 0 THEN 
	1000*TotScrubber/(ISNULL(c.TotTons, 0) * ISNULL(c.SulfurPcnt,0)*2/100*0.95 - ISNULL(m.SO2Emitted,0)) END AS ScrubbingIndex,
ISNULL(c.TotTons, 0) * ISNULL(c.SulfurPcnt,0)*2/100*0.95 AS InputS02,
m.SO2Emitted
FROM CoalTotCalc c INNER JOIN MiscCalc m ON c.Refnum = m.Refnum
WHERE c.Refnum IN (SELECT Refnum FROM Breaks WHERE Scrubbers = 'Y')



