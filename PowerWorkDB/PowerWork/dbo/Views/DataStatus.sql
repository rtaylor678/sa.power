﻿


/****** Object:  View dbo.CRVFactors    Script Date: 4/18/2003 4:38:53 PM ******/
CREATE VIEW [dbo].[DataStatus]
AS

SELECT t.Refnum, 
	t.SiteID, 
	t.CoLoc, 
	GADS = CASE WHEN GADSAR IS NULL THEN 0 ELSE 1 END, 
	Timeline = CASE WHEN Timeline IS NULL OR Timeline < GADSAR THEN 0 ELSE 1 END,
	ROODs = CASE WHEN ROODs IS NULL OR ROODs < Timeline OR ROODs < GADSAR THEN 0 ELSE 1 END,
	Upload = CASE WHEN Upload IS NULL THEN 0 ELSE 1 END,
	Calcs = CASE WHEN ISNULL(calcsite.Calcs, calcref.Calcs) IS NULL OR ISNULL(calcsite.Calcs, calcref.Calcs) < Upload OR Upload IS NULL OR ISNULL(calcsite.Calcs, calcref.Calcs) < Timeline OR Timeline IS NULL OR ISNULL(calcsite.Calcs, calcref.Calcs) < GADSAR OR GADSAR IS NULL OR p.LatestPrice IS NULL OR YEAR(p.LatestPrice) <> 2013 THEN 0 ELSE 1 END,
	--,CalcB4ROODs = CASE WHEN ISNULL(calcsite.Calcs, calcref.Calcs) > ROODs OR ISNULL(calcsite.Calcs, calcref.Calcs) IS NULL THEN '' ELSE 'Y' END
	PricingHub = CASE WHEN p.LatestPrice IS NULL OR YEAR(p.LatestPrice) <> 2013 THEN 0 ELSE 1 END
FROM PowerWork.dbo.TSort t 
	LEFT JOIN (SELECT refnum, Upload = CAST(MAX(messagetime) AS SMALLDATETIME) FROM PowerWork.dbo.MessageLog WHERE refnum LIKE '%13' AND Source = 'UpLoad 3.0' GROUP BY refnum) Upload ON Upload.Refnum = t.siteid 
	LEFT JOIN (SELECT t.Refnum, GADSAR = dateadd(hh,-5,CAST(MAX(TimeStamp) AS SMALLDATETIME)) FROM PowerWork.dbo.TSort t LEFT JOIN PowerWork.dbo.NERCTurbine nt ON nt.Refnum = t.Refnum LEFT JOIN GADSOS.GADSNG.PerformanceDataAR p ON p.UtilityUnitCode = nt.UtilityUnitCode AND p.Year = t.EvntYear WHERE t.refnum LIKE '%13' GROUP BY t.Refnum) gadsar ON gadsar.Refnum = t.refnum 
	LEFT JOIN (SELECT refnum, Timeline = CAST(MAX(messagetime) AS SMALLDATETIME) FROM PowerWork.dbo.MessageLog WHERE refnum LIKE '%13' AND Source IN ('Timeline') GROUP BY refnum) timeline ON timeline.Refnum = t.siteid 
	LEFT JOIN (SELECT refnum, Calcs = CAST(MAX(messagetime) AS SMALLDATETIME) FROM PowerWork.dbo.MessageLog WHERE refnum LIKE '%13' AND Source IN ('FinalCalcs', 'Calculations') GROUP BY refnum) calcref ON calcref.Refnum = t.refnum 
	LEFT JOIN (SELECT refnum, Calcs = CAST(MAX(messagetime) AS SMALLDATETIME) FROM PowerWork.dbo.MessageLog WHERE refnum LIKE '%13' AND Source IN ('FinalCalcs', 'Calculations') GROUP BY refnum) calcsite ON calcsite.Refnum = t.siteid 
	LEFT JOIN (SELECT refnum, ROODs = CAST(MAX(messagetime) AS SMALLDATETIME) FROM PowerWork.dbo.MessageLog WHERE refnum LIKE '%13' AND Source IN ('ROODs') GROUP BY refnum) roods ON roods.Refnum = t.Refnum 
	LEFT JOIN PricingHub_LU p ON p.PricingHub = t.PricingHub
WHERE t.Refnum LIKE '%13'




