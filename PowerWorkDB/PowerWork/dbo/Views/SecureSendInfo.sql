﻿

CREATE VIEW SecureSendInfo
AS
SELECT StudySites.SiteID as RefNum, 
Company_LU.CompanyName as CompanyLong, 
StudySites.SiteName as Location, 
ClientInfo.CoordName As Name, 
ClientInfo.CoordEMail as 'Internet E-Mail', 
Company_LU.Password, Company_LU.CompanyID 
FROM dbo.ClientInfo ClientInfo
INNER JOIN dbo.StudySites StudySites ON StudySites.SiteID  = ClientInfo.SiteID
INNER JOIN dbo.Company_LU Company_LU ON Company_LU.CompanyID = StudySites.CompanyID


