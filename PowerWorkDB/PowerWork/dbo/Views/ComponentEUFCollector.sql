﻿CREATE     VIEW [dbo].[ComponentEUFCollector]
AS
-- ALTERd by FRS and GLC
-- Gives  the EUF by Component
-- Modified by FRS to include the UnitType from Gads Setup table.
-- It will be used to determine whether turbine and generator 
-- events go to the steam turbine or the CT turbine.
SELECT nt.Refnum, s.UnitType, cc.SAIMajorEquip, cc.SAIMinorEquip,
SUM(EquivMWhrs) /
(
SELECT SUM(E_PH) 
FROM GADSOS.GADSNG.EventHours eh2
INNER JOIN GADSOS.GADSNG.Setup s2 ON s2.UnitShortName = eh2.UnitShortName
INNER JOIN dbo.NERCTurbine nt2 on nt2.UtilityUnitCode = s2.UtilityUnitCode
INNER JOIN dbo.TSort t2 on t2.Refnum = nt2.Refnum
WHERE (DatePart(yy,eh2.TL_DateTime) =  t2.EvntYear) AND (eh2.Granularity = 'Yearly')
AND E_PH > 0 -- This line added to avoid Divide By Zero problem.
             -- Zero E_PH will now cause NULL EUF in the view instead of
             -- the view erroring due to Divide By Zero.
AND nt2.Refnum = nt.Refnum
GROUP BY nt2.Refnum
) * 100 AS EUF,
(
SELECT SUM(E_PH) 
FROM GADSOS.GADSNG.EventHours eh2
INNER JOIN GADSOS.GADSNG.Setup s2 ON s2.UnitShortName = eh2.UnitShortName
INNER JOIN dbo.NERCTurbine nt2 on nt2.UtilityUnitCode = s2.UtilityUnitCode
INNER JOIN dbo.TSort t2 on t2.Refnum = nt2.Refnum
WHERE (DatePart(yy,eh2.TL_DateTime) =  t2.EvntYear) AND (eh2.Granularity = 'Yearly')
AND nt2.Refnum = nt.Refnum
GROUP BY nt2.Refnum
) AS E_PH
FROM GADSOS.GADSNG.EventDetails e
INNER JOIN GADSOS.GADSNG.Setup s ON s.UnitShortName = e.UnitShortName
INNER JOIN dbo.NERCTurbine nt on nt.UtilityUnitCode = s.UtilityUnitCode
INNER JOIN dbo.TSort t on t.Refnum = nt.Refnum
INNER JOIN dbo.CauseCodes cc on cc.Cause_Code = e.CauseCode
WHERE (DatePart(yy,TL_DateTime) =  t.EvntYear) AND (e.Granularity = 'Yearly')
--AND nt.Refnum = '493CC05'
--AND nt.Refnum = '704104P05'
GROUP BY  nt.Refnum, s.UnitType, cc.SAIMajorEquip, cc.SAIMinorEquip
--ORDER BY nt.Refnum, cc.SAIMajorEquip, cc.SAIMinorEquip
