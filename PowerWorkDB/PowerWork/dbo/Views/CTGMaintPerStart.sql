﻿
CREATE VIEW CTGMaintPerStart
AS
SELECT Refnum, AvgTotStarts = AVG(AvgTotStarts), 
LTSAPerStart = AVG(ISNULL(LTSAAdj, 0))/AVG(AvgTotStarts),
AnnOHPerStart = AVG(ISNULL(AnnOHCost, 0))/AVG(AvgTotStarts),
NonOHPerStart = AVG(ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts),
MaintCostPerStart = AVG(ISNULL(LTSAAdj, 0)+ISNULL(AnnOHCost, 0)+ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts)
FROM CTGMaintCosts
GROUP BY Refnum
HAVING AVG(AvgTotStarts)>0

