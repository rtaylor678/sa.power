﻿CREATE VIEW CTGMaintCosts
AS
SELECT Refnum, TurbineID, AvgTotStarts, AvgTotRunHrs, LTSAAdj, 
AnnOHCost = (SELECT SUM(TotAnnOHCost) FROM OHMaint o WHERE o.Refnum = LTSA.Refnum AND o.TurbineID = LTSA.TurbineID),
AnnNonOHCost = (SELECT SUM(AnnNonOHCost) FROM NonOHMaint n WHERE n.Refnum = LTSA.Refnum AND n.TurbineID = LTSA.TurbineID)
FROM LTSA

