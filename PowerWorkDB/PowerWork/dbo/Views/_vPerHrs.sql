﻿

CREATE VIEW _vPerHrs AS
SELECT t.Refnum, t.TurbineID, n.TL_DATE, n.PO, n.PO_SE, n.MO, n.MO_SE, n.SF, n.U1, n.U2,
n.U3, n.D1, n.D2, n.D3, n.D4, n.D4_DE, n.PD, n.PD_DE, n.RS, n.NC, 
n.EUFDH_RS, n.SH, n.PH, n.ESEDH, n.AH, n.POF, n.UOF, n.FOF, n.MOF, 
n.SOF, n.UF, n.AF, n.SEF, n.SDF, n.UDF, n.EUF, n.EAF, n.FOR_, n.EFOR
FROM NERCTurbine t LEFT JOIN PerHrs n ON t.UnitAbbr = n.UnitAbbr


