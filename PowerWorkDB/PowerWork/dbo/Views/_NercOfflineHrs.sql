﻿CREATE VIEW dbo._NercOfflineHrs
AS
SELECT     Refnum, Util_Code, Unit_Code, DATEPART(Mm, Event_Start) AS [Month], DATEPART(Yy, Event_Start) AS [Year], SUM(DATEDIFF(Hh, Event_Start, 
                      Event_End)) AS OutageHours
FROM         dbo._NercOffline
GROUP BY Refnum, Util_Code, Unit_Code, DATEPART(Mm, Event_Start), DATEPART(Yy, Event_Start)
