﻿

CREATE VIEW MaintPerRunHr
AS
SELECT Refnum, LTSA = ISNULL((SELECT SUM(LTSAAdjTotRunHr) FROM LTSA WHERE LTSA.Refnum = TSort.Refnum), 0),
Overhaul = ISNULL((SELECT SUM(OHCostTotRunHr) FROM OHMaint WHERE OHMaint.Refnum = TSort.Refnum), 0),
NonOverhaul = ISNULL((SELECT SUM(NonOHCostTotRunHr) FROM NonOHMaint WHERE NonOHMaint.Refnum = TSort.Refnum), 0),
Maint = ISNULL((SELECT SUM(LTSAAdjTotRunHr) FROM LTSA WHERE LTSA.Refnum = TSort.Refnum), 0) + ISNULL((SELECT SUM(OHCostTotRunHr) FROM OHMaint WHERE OHMaint.Refnum = TSort.Refnum), 0) + ISNULL((SELECT SUM(NonOHCostTotRunHr) FROM NonOHMaint WHERE NonOHMaint.Refnum = TSort.Refnum), 0)
FROM TSort



