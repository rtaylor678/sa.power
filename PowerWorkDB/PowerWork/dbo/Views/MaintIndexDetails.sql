﻿




/****** Object:  View dbo.MaintIndexDetails    Script Date: 4/18/2003 4:38:57 PM ******/
/****** Object:  View dbo.MaintIndexDetails    Script Date: 12/28/2001 7:35:16 AM ******/
CREATE    VIEW MaintIndexDetails AS 
SELECT m.Refnum, l.EquipGroup2, SUM(m.AnnOHCostKUS) AS AnnOHCostKUS, 
SUM(m.AnnNonOHCostKUS) AS AnnNonOHCostKUS, 
SUM(m.AnnMaintCostKUS) + MIN(CASE WHEN l.EquipGroup2 = 'CTG' THEN t.AnnLTSACost ELSE 0 END) AS AnnMaintCostKUS, 
SUM(m.AnnOHCostMWH) AS AnnOHCostMWH, 
SUM(m.AnnNonOHCostMWH) AS AnnNonOHCostMWH, 
SUM(m.AnnMaintCostMWH) + MIN(CASE WHEN l.EquipGroup2 = 'CTG' THEN t.AnnLTSACostMWH ELSE 0 END) AS AnnMaintCostMWH, 
SUM(m.AnnOHCostMW) AS AnnOHCostMW, 
SUM(m.AnnNonOHCostMW) AS AnnNonOHCostMW, 
SUM(m.AnnMaintCostMW) + MIN(CASE WHEN l.EquipGroup2 = 'CTG' THEN t.AnnLTSACostMW ELSE 0 END) AS AnnMaintCostMW,
SUM(m.AnnOHCostEGC) AS AnnOHCostEGC, 
SUM(m.AnnNonOHCostEGC) AS AnnNonOHCostEGC, 
SUM(m.AnnMaintCostEGC) + MIN(CASE WHEN l.EquipGroup2 = 'CTG' THEN t.AnnLTSACostEGC ELSE 0 END) AS AnnMaintCostEGC
FROM MaintEquipCalc m 
INNER JOIN MaintTotCalc t ON t.Refnum = m.Refnum
INNER JOIN (SELECT DISTINCT EquipGroup, EquipGroup2 FROM Component_LU) l ON m.EquipGroup = l.EquipGroup
GROUP BY m.Refnum, l.EquipGroup2





