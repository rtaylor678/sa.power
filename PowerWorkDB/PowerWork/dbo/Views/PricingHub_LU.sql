﻿CREATE VIEW dbo.PricingHub_LU
AS
SELECT     *, 
                      (CASE
                                WHEN PricingHub = 'MIDC' THEN 'Mid-Columbia' 
                                WHEN PricingHub = 'CINER' THEN 'Cinergy' 
                                WHEN PricingHub = 'NEPOL'  THEN 'NEPOOL'
                                WHEN PricingHub = 'COMED' THEN 'Com-Ed'
                                WHEN PricingHub = 'ECAR' THEN 'ECAR Central,ECAR East,ECAR North,ECAR South,ECAR West'
                                WHEN PricingHub = 'ERNO' THEN 'ERCOT North' 
                                WHEN PricingHub= 'ERSO' THEN 'ERCOT South'
                                WHEN PricingHub = 'ERWE' THEN 'ERCOT West'
		     WHEN PricingHub = 'FLAIN' THEN 'FRCC,Fla/Ga Border'
                                WHEN PricingHub = 'FOURC' THEN 'Four Corners'
                                WHEN PricingHub = 'MAINN' THEN 'Main' 
                                WHEN PricingHub = 'MAINS' THEN 'Main Lower'
                                WHEN PricingHub = 'MAPPN' THEN 'MAPP North' 
                                WHEN PricingHub = 'MAPPS' THEN 'MAP,MAPP South , MAPP Lower,MAPP'
                                WHEN PricingHub = 'NYPPE' OR PricingHub = 'NYPPJ' THEN 'NYPOOL East'
                                WHEN PricingHub = 'NYPPW' THEN 'NYPOOL West' 
                                WHEN PricingHub = 'PALO' THEN 'Palo Verde'
                                WHEN PricingHub= 'PJMW' THEN 'PJM West,PJM East,PJM'
                                WHEN PricingHub = 'SOCO' THEN 'Southern'
                                WHEN PricingHub = 'SPPN' THEN 'SPP Northern'
                                WHEN PricingHub = 'SPP' THEN 'SPP Southern'
                                WHEN PricingHub = 'VACAR' THEN 'Va Power' 
                                WHEN PricingHub = 'TVA' THEN 'Into TVA'
                                WHEN PricingHub = 'ENTGY' THEN 'Entergy'
                      END)
                       AS [Legacy Name]
FROM         PowerGlobal.dbo.PricingHub_LU

























