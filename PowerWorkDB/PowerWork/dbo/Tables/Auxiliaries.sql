﻿CREATE TABLE [dbo].[Auxiliaries] (
    [Refnum]          [dbo].[Refnum] NOT NULL,
    [AuxID]           SMALLINT       NOT NULL,
    [AuxName]         CHAR (30)      NULL,
    [DesignStmFlow]   REAL           NULL,
    [InletPress]      REAL           NULL,
    [InletTemp]       REAL           NULL,
    [ExhaustPress]    REAL           NULL,
    [ExhaustTemp]     REAL           NULL,
    [ExhaustLocation] CHAR (30)      NULL,
    [LoadVary]        CHAR (1)       NULL,
    [Comments]        VARCHAR (100)  NULL,
    [CalcEnthalpy]    CHAR (1)       NULL,
    [EnthalpyIn]      REAL           NULL,
    [EnthalpyOut]     REAL           NULL,
    [AuxLvlMWH]       REAL           NULL,
    CONSTRAINT [PK___1__14] PRIMARY KEY CLUSTERED ([Refnum] ASC, [AuxID] ASC) WITH (FILLFACTOR = 90)
);


GO
/****** Object:  Trigger dbo.UpdateAuxiliaries    Script Date: 4/18/2003 4:39:05 PM ******/
/****** Object:  Trigger dbo.UpdateAuxiliaries    Script Date: 12/28/2001 7:35:18 AM ******/
CREATE TRIGGER UpdateAuxiliaries ON dbo.Auxiliaries 
FOR INSERT,UPDATE
AS
UPDATE Auxiliaries 
SET CalcEnthalpy = CASE
	WHEN inserted.ExhaustLocation = 'Gland Condenser' THEN 'Y'
	WHEN inserted.ExhaustLocation LIKE '%Cond%' THEN 'N'
	WHEN inserted.ExhaustLocation LIKE 'ATM%' THEN 'N'
	WHEN inserted.ExhaustLocation LIKE '%HOTWELL%' THEN 'N'
	WHEN inserted.ExhaustLocation LIKE '%SJAP%' THEN 'N'
	WHEN inserted.ExhaustLocation = 'Turbine Deck' THEN 'N'
	WHEN inserted.ExhaustLocation IS NULL AND inserted.LoadVary <> 'H' THEN 'N'
	ELSE 'Y'
	END
FROM Auxiliaries INNER JOIN inserted 
ON Auxiliaries.Refnum = inserted.Refnum
AND Auxiliaries.AuxID = inserted.AuxID
WHERE inserted.CalcEnthalpy IS NULL
