﻿CREATE TABLE [dbo].[MiscCalc] (
    [Refnum]               [dbo].[Refnum] NOT NULL,
    [RevAsh]               REAL           NULL,
    [RevOth]               REAL           NULL,
    [OthRevenue]           REAL           NULL,
    [OthNonT7Revenue]      REAL           NULL,
    [TotAncillaryRevenue]  REAL           NULL,
    [TotRevenueRevised]    REAL           NULL,
    [TotRevenue]           REAL           NULL,
    [TotAshHandling]       REAL           NULL,
    [TotScrubberNonMaint]  REAL           NULL,
    [AESoxTons]            REAL           NULL,
    [AESOXRevenue]         REAL           NULL,
    [AENoxTons]            REAL           NULL,
    [AENOXRevenue]         REAL           NULL,
    [SO2EmissionAdj]       REAL           NULL,
    [ScrubberAnnMaintCost] REAL           NULL,
    [TotScrubber]          REAL           NULL,
    [SO2Emitted]           REAL           NULL,
    [SO2RemovalCost]       REAL           NULL,
    [ScrubberPowerCost]    REAL           NULL,
    [NOxEmitted]           REAL           NULL,
    [NOxEmissionAdj]       REAL           NULL,
    [CO2Emitted]           REAL           NULL,
    [CO2EmissionAdj]       REAL           NULL,
    CONSTRAINT [PK_MiscCal_1__28] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'T7 Ash Sales', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiscCalc', @level2type = N'COLUMN', @level2name = N'RevAsh';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'T7 "Other Chemical and ByProduct Sales"', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiscCalc', @level2type = N'COLUMN', @level2name = N'RevOth';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'T8 Other Revenue (per inst, includes RevAsh and RevOth)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiscCalc', @level2type = N'COLUMN', @level2name = N'OthRevenue';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'OthRevenue minus (RevAsh + RevOth)', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiscCalc', @level2type = N'COLUMN', @level2name = N'OthNonT7Revenue';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'RevAsh + RevOth + OthNonT7Revenue + Ancillary Revenue', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiscCalc', @level2type = N'COLUMN', @level2name = N'TotRevenueRevised';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Obsolete, no longer used.', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MiscCalc', @level2type = N'COLUMN', @level2name = N'TotRevenue';

