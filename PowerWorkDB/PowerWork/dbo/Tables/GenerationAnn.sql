﻿CREATE TABLE [dbo].[GenerationAnn] (
    [Refnum]          [dbo].[Refnum] NOT NULL,
    [ColdStarts]      SMALLINT       NULL,
    [HotStarts]       SMALLINT       NULL,
    [STGStarts]       SMALLINT       NULL,
    [TotStarts]       SMALLINT       NULL,
    [AutoGenCtrlPcnt] REAL           NULL,
    [TestHeatRate]    REAL           NULL,
    [TestBasis]       CHAR (2)       NULL,
    [NetOutputFactor] REAL           NULL,
    [PurElec]         REAL           NULL,
    CONSTRAINT [PK_GenerationAnn_1__17] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

