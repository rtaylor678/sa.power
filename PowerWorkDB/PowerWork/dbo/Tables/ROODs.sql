﻿CREATE TABLE [dbo].[ROODs] (
    [Refnum]     [dbo].[Refnum]    NOT NULL,
    [TurbineID]  [dbo].[TurbineID] NOT NULL,
    [Start_Date] SMALLDATETIME     NOT NULL,
    [End_Date]   SMALLDATETIME     NOT NULL,
    [Success]    SMALLINT          NULL,
    [Forced]     SMALLINT          NULL,
    [Derate]     SMALLINT          NULL,
    [Maint]      SMALLINT          NULL,
    [Startup]    SMALLINT          NULL,
    CONSTRAINT [PK_ROODs] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [Start_Date] ASC, [End_Date] ASC) WITH (FILLFACTOR = 90)
);


GO
/****** Object:  Trigger dbo.updStartsAnalysis    Script Date: 4/18/2003 4:46:37 PM ******/
CREATE   TRIGGER updStartsAnalysis ON ROODs FOR INSERT, UPDATE, DELETE
AS
CREATE TABLE #Refs (
	Refnum char(12) NOT NULL)
INSERT INTO #Refs (Refnum)
SELECT Refnum FROM inserted
UNION
SELECT Refnum FROM deleted
DELETE FROM StartsAnalysis
WHERE Refnum IN (SELECT Refnum FROM #Refs)
INSERT StartsAnalysis (Refnum, TurbineID, Success, Forced, Derate, Maint, Startup, TotalOpps)
SELECT Refnum, TurbineID, SUM(ISNULL(Success,0)) AS Success, SUM(ISNULL(Forced,0)) AS Forced, 
SUM(ISNULL(Derate,0)) AS Derate, SUM(ISNULL(Maint,0)) AS Maint, SUM(ISNULL(Startup,0)) AS Startup,
SUM(ISNULL(Success,0)+ISNULL(Forced,0)+ISNULL(Derate,0)+ISNULL(Maint,0)+ISNULL(Startup,0)) AS TotalOpps
FROM ROODs
WHERE Refnum IN (SELECT Refnum FROM #Refs)
GROUP BY Refnum, TurbineID
UPDATE StartsAnalysis
SET SuccessPcnt = Success/TotalOpps*100,
ForcedPcnt = Forced/TotalOpps*100,
MaintPcnt = Maint/TotalOpps*100,
StartupPcnt = Startup/TotalOpps*100,
DeratePcnt = Derate/TotalOpps*100,
StartsGroup = CASE 
	WHEN TotalOpps<20 THEN 1
	WHEN TotalOpps BETWEEN 20 AND 70 THEN 2
	WHEN TotalOpps>70 THEN 3
	END 
WHERE Refnum IN (SELECT Refnum FROM #Refs) AND TotalOpps>0
