﻿CREATE TABLE [dbo].[AllUnitsToCopy] (
    [OldRefnum] [dbo].[Refnum]    NULL,
    [NewRefnum] [dbo].[Refnum]    NOT NULL,
    [StudyYear] [dbo].[StudyYear] NULL,
    [OldSiteID] [dbo].[SiteID]    NULL,
    [NewSiteID] [dbo].[SiteID]    NULL
);

