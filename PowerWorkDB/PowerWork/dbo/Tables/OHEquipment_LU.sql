﻿CREATE TABLE [dbo].[OHEquipment_LU] (
    [ComponentText]    VARCHAR (250)         NULL,
    [ProjectIDText]    VARCHAR (250)         NULL,
    [RankineOrBrayton] CHAR (1)              NULL,
    [EquipID]          [dbo].[EquipmentID]   NULL,
    [Component]        [dbo].[ComponentCode] NULL,
    [ProjectID]        CHAR (15)             NULL
);

