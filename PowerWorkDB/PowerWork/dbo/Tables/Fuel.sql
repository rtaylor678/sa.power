﻿CREATE TABLE [dbo].[Fuel] (
    [Refnum]           [dbo].[Refnum]    NOT NULL,
    [TurbineID]        [dbo].[TurbineID] NOT NULL,
    [Period]           [dbo].[GenPeriod] NOT NULL,
    [FuelType]         CHAR (6)          NOT NULL,
    [MBTU]             REAL              NULL,
    [CostMBTULocal]    REAL              NULL,
    [CostMBTU]         REAL              NULL,
    [MBTULHV]          REAL              NULL,
    [CostMBTULHVLocal] REAL              NULL,
    [CostMBTULHV]      REAL              NULL,
    [KGal]             REAL              NULL,
    [HeatValue]        REAL              NULL,
    [LHV]              REAL              NULL,
    [CostGalLocal]     REAL              NULL,
    [CostGal]          REAL              NULL,
    [TotCostKUS]       REAL              NULL,
    [ReportedLHV]      [dbo].[YorN]      NOT NULL,
    CONSTRAINT [PK_NewFuel] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [Period] ASC, [FuelType] ASC)
);

