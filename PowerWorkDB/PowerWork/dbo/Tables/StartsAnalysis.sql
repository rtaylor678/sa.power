﻿CREATE TABLE [dbo].[StartsAnalysis] (
    [Refnum]      [dbo].[Refnum]    NOT NULL,
    [TurbineID]   [dbo].[TurbineID] NOT NULL,
    [Success]     REAL              NOT NULL,
    [Forced]      REAL              NOT NULL,
    [Maint]       REAL              NOT NULL,
    [Startup]     REAL              NOT NULL,
    [Derate]      REAL              NOT NULL,
    [TotalOpps]   REAL              NOT NULL,
    [SuccessPcnt] REAL              NULL,
    [ForcedPcnt]  REAL              NULL,
    [MaintPcnt]   REAL              NULL,
    [StartupPcnt] REAL              NULL,
    [DeratePcnt]  REAL              NULL,
    [StartsGroup] SMALLINT          NULL,
    CONSTRAINT [PK_StartsAnalysis] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC) WITH (FILLFACTOR = 90)
);

