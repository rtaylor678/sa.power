﻿CREATE TABLE [dbo].[ProcessSteps] (
    [id]          INT          IDENTITY (1, 1) NOT NULL,
    [SiteID]      CHAR (12)    NULL,
    [Refnum]      CHAR (12)    NULL,
    [ProcessName] VARCHAR (50) NOT NULL,
    [Started]     DATETIME     NULL,
    [Completed]   DATETIME     NOT NULL
);

