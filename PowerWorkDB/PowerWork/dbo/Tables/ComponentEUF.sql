﻿CREATE TABLE [dbo].[ComponentEUF] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [EquipGroup] CHAR (8)       NOT NULL,
    [EUF]        REAL           NULL,
    [E_PH]       REAL           NULL,
    CONSTRAINT [PK_ComponentEUF] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EquipGroup] ASC)
);

