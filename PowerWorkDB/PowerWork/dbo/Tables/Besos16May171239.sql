﻿CREATE TABLE [dbo].[Besos16May171239] (
    [Generator Name or ID]       NVARCHAR (255) NULL,
    [Utility Unit Code]          FLOAT (53)     NULL,
    [Client Event Number]        FLOAT (53)     NULL,
    [Event Type]                 NVARCHAR (255) NULL,
    [Start of Event Date & Time] DATETIME       NULL,
    [End of Event Date & Time]   DATETIME       NULL,
    [Cause Code]                 FLOAT (53)     NULL,
    [Net Available Capacity]     FLOAT (53)     NULL,
    [Event Description]          NVARCHAR (255) NULL,
    [Amp Code Ext]               NVARCHAR (255) NULL
);

