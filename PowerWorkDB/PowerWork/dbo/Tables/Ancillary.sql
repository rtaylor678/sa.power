﻿CREATE TABLE [dbo].[Ancillary] (
    [Refnum]       [dbo].[Refnum]    NOT NULL,
    [Period]       [dbo].[GenPeriod] NOT NULL,
    [CapCommitted] REAL              NULL,
    [RampRate]     REAL              NULL,
    [Revenue]      REAL              NULL,
    [RevenueLocal] REAL              NULL,
    CONSTRAINT [PK_Ancillary] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Period] ASC)
);

