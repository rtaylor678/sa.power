﻿CREATE TABLE [dbo].[SiteAbsences] (
    [SiteID]      [dbo].[SiteID] NOT NULL,
    [AbsCategory] CHAR (6)       NOT NULL,
    [SortKey]     SMALLINT       NOT NULL,
    [OCCAbs]      REAL           NULL,
    [MPSAbs]      REAL           NULL,
    [TotAbs]      REAL           NULL,
    [OCCAbsPcnt]  REAL           NULL,
    [MPSAbsPcnt]  REAL           NULL,
    [TotAbsPcnt]  REAL           NULL,
    CONSTRAINT [PK_SiteAbsences] PRIMARY KEY NONCLUSTERED ([SiteID] ASC, [AbsCategory] ASC) WITH (FILLFACTOR = 90)
);


GO
CREATE CLUSTERED INDEX [SiteAbsencesSortKey]
    ON [dbo].[SiteAbsences]([SiteID] ASC, [SortKey] ASC);

