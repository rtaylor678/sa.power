﻿CREATE TABLE [dbo].[ReportOptionsRestore] (
    [ReportSetID]       INT           NOT NULL,
    [SheetName]         VARCHAR (30)  NOT NULL,
    [ReportID]          INT           NOT NULL,
    [RefListNo]         INT           NOT NULL,
    [BreakConditions]   VARCHAR (255) NOT NULL,
    [RptOptionScenario] CHAR (8)      NULL,
    [RptOptionSQL]      VARCHAR (255) NULL,
    [Orientation]       CHAR (1)      NOT NULL,
    [SortKey]           TINYINT       NULL,
    [PreferenceID]      CHAR (5)      NULL,
    [RptOptionCurrency] CHAR (4)      NULL
);

