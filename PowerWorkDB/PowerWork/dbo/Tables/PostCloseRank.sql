﻿CREATE TABLE [dbo].[PostCloseRank] (
    [Refnum]         [dbo].[Refnum] NOT NULL,
    [Percentile]     REAL           NULL,
    [ListName]       VARCHAR (30)   NOT NULL,
    [BreakCondition] VARCHAR (30)   NULL,
    [BreakValue]     CHAR (12)      NOT NULL,
    [RankVariableID] SMALLINT       NOT NULL
);

