﻿CREATE TABLE [dbo].[CarryoverParents] (
    [Refnum] [dbo].[Refnum] NOT NULL,
    [Parent] [dbo].[Refnum] NULL,
    CONSTRAINT [PK_CarryoverParents] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

