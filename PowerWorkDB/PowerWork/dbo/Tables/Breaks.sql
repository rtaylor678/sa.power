﻿CREATE TABLE [dbo].[Breaks] (
    [Refnum]          [dbo].[Refnum] NOT NULL,
    [FuelGroup]       VARCHAR (9)    NULL,
    [LoadType]        VARCHAR (4)    NULL,
    [CoalNMCGroup]    INT            NULL,
    [CoalNMCGroup2]   INT            NULL,
    [GasOilNMCGroup]  INT            NULL,
    [CoalHVGroup]     INT            NULL,
    [Scrubbers]       VARCHAR (1)    NULL,
    [ScrubberSize]    VARCHAR (2)    NULL,
    [CombinedCycle]   CHAR (1)       NULL,
    [BaseCoal]        CHAR (1)       NULL,
    [LoadType2]       VARCHAR (3)    NULL,
    [SteamGasOil]     CHAR (1)       NULL,
    [CRVGroup]        CHAR (5)       NULL,
    [CoalNMCOverlap]  INT            NULL,
    [CoalSulfurGroup] CHAR (2)       NULL,
    [CCNMCGroup]      TINYINT        NULL,
    [CRVSizeGroup]    CHAR (6)       NULL,
    [CoalNMCOverlap2] TINYINT        NULL,
    [LTSA]            [dbo].[YorN]   NULL,
    [CogenElec]       CHAR (5)       NULL,
    [CRVSizeGroup2]   VARCHAR (5)    NULL,
    [NCF2Yr]          TINYINT        NULL,
    [FTEMP]           TINYINT        NULL,
    [CTGS]            TINYINT        NULL,
    [CoalAshGroup]    INT            NULL,
    CONSTRAINT [PK_BreaksT_1__14] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

