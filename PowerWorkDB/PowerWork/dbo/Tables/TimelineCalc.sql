﻿CREATE TABLE [dbo].[TimelineCalc] (
    [IDNum]           INT            NULL,
    [UtilityUnitCode] CHAR (6)       NULL,
    [Evnt_Year]       SMALLINT       NULL,
    [Evnt_No]         SMALLINT       NULL,
    [Evnt_Type]       CHAR (2)       NULL,
    [Evnt_Group]      CHAR (2)       NULL,
    [Phase]           TINYINT        NULL,
    [StartTime]       SMALLDATETIME  NULL,
    [EndTime]         SMALLDATETIME  NULL,
    [AvailCap]        DECIMAL (7, 2) NULL,
    [LostMW]          SMALLINT       NULL
);

