﻿CREATE TABLE [dbo].[HRVariance] (
    [Refnum]   [dbo].[Refnum] NOT NULL,
    [HeatRate] REAL           NULL,
    [TestRate] REAL           NULL,
    [Variance] REAL           NULL,
    CONSTRAINT [PK___1__11] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

