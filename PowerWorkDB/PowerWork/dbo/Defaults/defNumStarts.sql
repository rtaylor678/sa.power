﻿CREATE DEFAULT [dbo].[defNumStarts]
    AS 0;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defNumStarts]', @objname = N'[dbo].[StartsAnalysis].[Success]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defNumStarts]', @objname = N'[dbo].[StartsAnalysis].[Forced]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defNumStarts]', @objname = N'[dbo].[StartsAnalysis].[Maint]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defNumStarts]', @objname = N'[dbo].[StartsAnalysis].[Startup]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defNumStarts]', @objname = N'[dbo].[StartsAnalysis].[Derate]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defNumStarts]', @objname = N'[dbo].[StartsAnalysis].[TotalOpps]';

