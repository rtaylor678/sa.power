﻿CREATE DEFAULT [dbo].[defBitFalse]
    AS 0;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitFalse]', @objname = N'[dbo].[TSort].[EGCManualTech]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitFalse]', @objname = N'[dbo].[CalcQueue].[EmailNotify]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitFalse]', @objname = N'[dbo].[CalcQueue].[Compare]';

