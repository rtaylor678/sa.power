﻿CREATE DEFAULT [dbo].[defTurbineID]
    AS 'STG';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[Misc].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[OHMaint].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[StartsAnalysis].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[EquipCount].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[CoalProperties].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[CTGSupplement].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[NERCTurbine].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[ROODs].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[NonOHMaint].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[PowerGeneration].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[EventLRO].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[Coal].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[Fuel].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[CptlMaintExpLocal].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[OHEquipCalc].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[Equipment].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[CptlMaintExp].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[Events].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[AssetCosts].[TurbineID]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defTurbineID]', @objname = N'[dbo].[TurbineID]';

