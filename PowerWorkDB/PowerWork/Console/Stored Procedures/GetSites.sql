﻿CREATE PROCEDURE [Console].[GetSites]
	@StudyYear dbo.StudyYear
	
AS
BEGIN

	SELECT DISTINCT SiteID, RefNum = RTRIM(CompanyID) + ' - ' + SiteName
	FROM dbo.StudySites
	WHERE StudyYear = @StudyYear and SiteID not like '%P'
	ORDER BY refnum ASC
	
END
