﻿CREATE PROCEDURE [Console].[GetLastCalcDate]
   	@SiteID varchar(10)

AS
BEGIN

SELECT MAX(MessageTime) as MessageTime
FROM MessageLog l INNER JOIN TSort t ON t.Refnum = l.Refnum
WHERE t.SiteID = @SiteID AND MessageID = 44

	
END
