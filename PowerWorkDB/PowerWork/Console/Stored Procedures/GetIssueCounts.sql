﻿CREATE PROCEDURE [Console].[GetIssueCounts]
	@RefNum dbo.Refnum,
	@Completed nvarchar(1)
AS
BEGIN

	SELECT COUNT(IssueID) FROM ValStat 
	WHERE Refnum = @RefNum
	AND (@Completed = 'A' OR completed = @Completed)

END
