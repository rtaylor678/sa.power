﻿CREATE proc [Console].[GetGradesBySection]
@RefNum dbo.Refnum,
@Version int
as
SELECT Section, ROUND(GRADE,0) AS Grade, NumReds, NumBlues, NumTeals, NumGreens FROM Val.SectionGrade WHERE Refnum = @RefNum and Version = @Version ORDER BY WtFactor DESC, Grade DESC
