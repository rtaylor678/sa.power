﻿CREATE PROCEDURE [Console].[GetInterimContactInfo]
	@RefNum dbo.Refnum
AS
BEGIN
	SELECT FirstName, LastName, JobTitle = NULL, Email, Phone  
	FROM Console.GetCompanyContact(@RefNum, 'Inter')
END
