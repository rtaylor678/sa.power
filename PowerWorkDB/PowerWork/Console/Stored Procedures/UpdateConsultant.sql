﻿CREATE PROCEDURE [Console].[UpdateConsultant]

	@RefNum dbo.Refnum,
	@StudyYear nvarchar(10),
	@Consultant nvarchar(5)

AS
BEGIN

UPDATE StudySites SET Consultant = @Consultant Where SiteID = @RefNum and StudyYear=@StudyYear

END
