﻿CREATE PROCEDURE [Console].[GetConsultants]
	@StudyYear dbo.StudyYear,
	@RefNum dbo.Refnum

AS
BEGIN

SELECT top 1 Consultant FROM dbo.StudySites 
WHERE StudyYear =  @StudyYear and SiteID =  @RefNum  
ORDER BY SiteID ASC
END
