﻿CREATE PROCEDURE [Console].[GetAllConsultants]
AS

select distinct [Consultant]
  FROM [PowerWork].[dbo].[StudySites]
  where StudyYear >= 2010
  and SiteID not like '%P'
  and Consultant <> ''
  order by Consultant

