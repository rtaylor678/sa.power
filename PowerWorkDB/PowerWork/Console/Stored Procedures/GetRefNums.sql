﻿CREATE PROCEDURE [Console].[GetRefNums]
	@StudyYear dbo.StudyYear,
	@Mode varchar(1)
AS
BEGIN
IF @Mode='0'
	SELECT DISTINCT RefNum = SiteID, CoLoc = RTRIM(CompanyID) + ' - ' + SiteName, Consultant 
	FROM dbo.StudySites
	WHERE StudyYear = @StudyYear and SiteID not like '%P'
	ORDER BY RefNum ASC
	ELSE
	SELECT DISTINCT RefNum = SiteID, CoLoc = RTRIM(CompanyID) + ' - ' + SiteName, Consultant 
	FROM dbo.StudySites
	WHERE StudyYear = @StudyYear  and SiteID not like '%P'
	ORDER BY Coloc ASC
	
END
