﻿CREATE PROCEDURE [Console].[GetAnIssue] 

	@RefNum dbo.Refnum,
	@IssueTitle nvarchar(50) = null,
	@IssueID nvarchar(50) = null

AS
BEGIN

Select * from ValStat
Where Refnum = @RefNum And ((@IssueTitle IS NULL) OR (IssueTitle = @IssueTitle)) and 
((@IssueID IS NULL) OR (IssueID = @IssueID))

END
