﻿CREATE PROCEDURE [Console].[GetStudies](@RefineryType varchar(5) = NULL)
AS
	SELECT distinct 'POWER' as Studies, StudyYear  
	FROM dbo.TSort
	WHERE StudyYear > 2006 
	ORDER BY StudyYear DESC
