﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.IO;

namespace Power_Calcs
{
	public class PowerCalc
	{
		private SqlConnection connPowerWork = new SqlConnection(Properties.Settings.Default.DatabaseConnection);
		private Excel.Application xlApp;
		private Excel.Workbooks xlWorkbooks;
		private string inSiteID;

		//constructor opens the db connection
		public PowerCalc(string SiteID)
		{
			inSiteID = SiteID.Trim();
			connPowerWork.Open();
		}

		#region Public Functions

		public PowerCalc(string SiteID, bool console)
		{
			inSiteID = SiteID.Trim();
			connPowerWork.Open();
			Calc();
		}

		public string Calc(bool Calc = true, bool CopyClean = true, bool ClientTable = true, bool SumCalc = true, bool ValidatedInput = true)
		{
			string complete = "";

			//check the validity of the SiteID
			string validSite = CheckSiteIDIsValid();
			if (validSite != "")
			{
				connPowerWork.Close();
				return validSite;
			}

			if (Calc == true)
			{
				complete = CalcSite();
				if (complete != "")
				{
					connPowerWork.Close();
					return complete;
				}
			}

			if (CopyClean == true)
			{
				complete = CopyCleanSite();
				if (complete != "")
				{
					connPowerWork.Close();
					return complete;
				}
			}

			//run Excel files if necessary
			if (ClientTable == true || SumCalc == true || ValidatedInput == true)
			{
				complete = RunExcelOutputs(ClientTable, SumCalc, ValidatedInput);
				if (complete != "")
				{
					connPowerWork.Close();
					return complete;
				}
			}

			connPowerWork.Close();
			return "Complete.";
		}

		public string AddToQueue(bool Calc = true, bool CopyClean = true, bool ClientTable = true, bool SumCalc = true, bool ValidatedInput = true)
		{
			//check the validity of the SiteID
			string validSite = CheckSiteIDIsValid();
			if (validSite != "")
			{
				connPowerWork.Close();
				return validSite;
			}

			//if valid, add to the queue using the stored procedure
			SqlCommand execCommand = new SqlCommand("spListToCalcQueue", connPowerWork);
			execCommand.CommandType = CommandType.StoredProcedure;
			execCommand.Parameters.Add("@ListName", SqlDbType.VarChar, 10).Value = inSiteID;
			execCommand.Parameters.Add("@Calculate", SqlDbType.Bit).Value = Calc;
			execCommand.Parameters.Add("@Compare", SqlDbType.Bit).Value = false;
			execCommand.Parameters.Add("@Copy", SqlDbType.Bit).Value = CopyClean;
			execCommand.Parameters.Add("@ClientTables", SqlDbType.Bit).Value = ClientTable;
			execCommand.Parameters.Add("@CalcSummary", SqlDbType.Bit).Value = SumCalc;
			execCommand.Parameters.Add("@MessageLog", SqlDbType.Bit).Value = ValidatedInput;//probably a bad idea but just putting VI into the MessageLog column for now
			execCommand.Parameters.Add("@EmailNotify", SqlDbType.Bit).Value = false;

			try
			{
				execCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

			connPowerWork.Close();

			return inSiteID.Trim() + " added to CalcQueue";
		}

		public string ProcessQueue()
		{
			//process the queue:
			//get list of sites in the queue
			string sqlString = "SELECT ListName, Calculate, Copy, ClientTables, CalcSummary, MessageLog FROM CalcQueue";//not using Compare or EmailNotify from the table

			SqlCommand queueCmd = new SqlCommand(sqlString, connPowerWork);
			SqlDataReader queueRdr = queueCmd.ExecuteReader();
			if (queueRdr.HasRows == false)
			{
				queueRdr.Close();
				connPowerWork.Close();
				return "Nothing in CalcQueue. Add units and try again.";
			}

			//now process each site one at a time
			DataTable data = new DataTable();//first convert to DataTable because otherwise the program complains about having two datareaders open at the same time
			data.Load(queueRdr);

			foreach (DataRow row in data.Rows)
			{
				string SiteID = (string)row["ListName"];
				bool Calculate = (bool)row["Calculate"];
				bool Copy = (bool)row["Copy"];
				bool ClientTables = (bool)row["ClientTables"];
				bool CalcSummary = (bool)row["CalcSummary"];
				bool ValidatedInput = (bool)row["MessageLog"];//VI is being stored in the MessageLog field for now

				this.inSiteID = SiteID;

				CheckOpenConnection();//the calcs function closes it so we need to reopen
				Calc(Calculate, Copy, ClientTables, CalcSummary, ValidatedInput);

				//and remove it from the CalcQueue
				sqlString = "DELETE FROM CalcQueue WHERE ListName = '" + SiteID + "'";

				CheckOpenConnection();
				SqlCommand execCommand = new SqlCommand(sqlString, connPowerWork);
				execCommand.CommandType = CommandType.Text;

				try
				{
					execCommand.ExecuteNonQuery();
				}
				catch (Exception ex)
				{
					return ex.Message;
				}
			}
			return "Process Queue Complete";
		}

		public Status GetStatus()
		{
			Status status = new Status(inSiteID);
			status.UpdateStatus();
			return status;
		}

		#endregion

		#region Private Functions

		private string CheckSiteIDIsValid()
		{
			//check if siteid is even populated, if not, do nothing
			if (inSiteID == "")
			{
				return "No SiteID entered.";
			}

			//check if it is a valid siteid, if not, error message
			SqlCommand cmd = new SqlCommand("SELECT SiteID FROM StudySites WHERE SiteID = '" + inSiteID + "'", connPowerWork);
			SqlDataReader rdr = cmd.ExecuteReader();
			if (rdr.HasRows == false)
			{
				rdr.Close();
				connPowerWork.Close();
				return "Invalid SiteID. Check and try again.";
			}
			rdr.Close();
			return "";

		}

		private void CheckOpenConnection()
		{
			//just checks if the connection is already open, if not, opens it
			if (connPowerWork.State == ConnectionState.Closed)
			{
				connPowerWork.Open();
			}
		}

		private string CalcSite()
		{

			//it is a valid SiteID, process it
			//do we need to process each individual refnum? no, the stored proc should do that, right?

			SqlCommand execCommand = new SqlCommand("spCalcs", connPowerWork);
			execCommand.CommandType = CommandType.StoredProcedure;
			execCommand.Parameters.Add("@SiteID", SqlDbType.VarChar, 10).Value = inSiteID;
			SqlParameter retValue = execCommand.Parameters.Add("return", SqlDbType.Int);
			retValue.Direction = ParameterDirection.ReturnValue;

			try
			{
				execCommand.ExecuteNonQuery();
			}
			catch(Exception ex)
			{
				return ex.Message;
			}

			return "";//empty string means everything worked fine
		}

		private string CopyCleanSite()
		{
			//just runs the spCopyClean proc, which copies the unit from PowerWork to Power

			SqlCommand execCommand = new SqlCommand("spCopyClean", connPowerWork);
			execCommand.CommandType = CommandType.StoredProcedure;
			execCommand.Parameters.Add("@SiteID", SqlDbType.VarChar, 10).Value = inSiteID;
			SqlParameter retValue = execCommand.Parameters.Add("return", SqlDbType.Int);
			retValue.Direction = ParameterDirection.ReturnValue;

			try
			{
				execCommand.ExecuteNonQuery();
			}
			catch (Exception ex)
			{
				return ex.Message;
			}

			return "";//empty string means everything worked fine

		}

		private string RunExcelOutputs(bool ClientTable, bool SumCalc, bool ValidatedInput)
		{

			//start the Excel application which will be used in the following functions
			xlApp = new Excel.Application();
			xlWorkbooks = xlApp.Workbooks;

			string complete = "";

			if (ClientTable == true)
			{
				complete = DoClientTable();
				if (complete != "")
				{
					ExcelCleanup();
					return complete;
				}
			}

			if (SumCalc == true)
			{
				complete = DoSumCalc();
				if (complete != "")
				{
					ExcelCleanup();
					return complete;
				}
			}

			if (ValidatedInput == true)
			{
				complete = DoValidatedInput();
				if (complete != "")
				{
					ExcelCleanup();
					return complete;
				}
			}

			ExcelCleanup();
			return complete;//should be blank here
		}

		private string DoClientTable()
		{
			//run the Client Tables by calling the spreadsheet that generates them

			string sqlString = "SELECT t.Refnum, s.SiteDirectory + '\' FROM TSort t LEFT JOIN StudySites s ON s.SiteID = t.SiteID WHERE t.SiteID = '" + inSiteID + "'";
			string excelWorkbook = Properties.Settings.Default.ClientTable;
			string function = "AutomatedCT";

			return RunExcel(excelWorkbook, sqlString, function);
		}

		private string DoSumCalc()
		{
			//run the SumCalcs by calling the spreadsheet that generates them

			string sqlString = "SELECT t.Refnum, '' FROM TSort t LEFT JOIN StudySites s ON s.SiteID = t.SiteID WHERE t.SiteID = '" + inSiteID + "'";
			string excelWorkbook = Properties.Settings.Default.SumCalc;
			string function = "AutomatedSumCalc";

			return RunExcel(excelWorkbook, sqlString, function);
		}

		private string DoValidatedInput()
		{
			//call the Validated Input spreadsheet
			string sqlString = "SELECT t.SiteID, '' FROM TSort t WHERE t.SiteID = '" + inSiteID + "'";
			string excelWorkbook = Properties.Settings.Default.ValidatedInput;
			string function = "AutomatedVI";

			return RunExcel(excelWorkbook, sqlString, function);
		}

		private void ExcelCleanup()
		{
			if (xlApp != null)
			{
				//end the Excel Application
				xlWorkbooks.Close();
				xlApp.Quit();

				//// Manual disposal because of COM
				while (Marshal.ReleaseComObject(xlApp) != 0) { }
				while (Marshal.ReleaseComObject(xlWorkbooks) != 0) { }

				xlApp = null;
				xlWorkbooks = null;

				GC.Collect();
				GC.WaitForPendingFinalizers();
			}
		}

		private string RunExcel(string excelWorkbook, string sqlString, string function)
		{
			//using the existing Excel app, open the workbook with the supplied name, and run the automatic function with the sql string

			//Start Excel and open the workbook.
			Excel.Workbook xlWorkBook = xlWorkbooks.Open(excelWorkbook);

			SqlCommand cmd = new SqlCommand(sqlString, connPowerWork);
			SqlDataReader rdr = cmd.ExecuteReader();
			if (rdr.HasRows == false)
			{
				rdr.Close();
				connPowerWork.Close();
				return "Invalid SiteID. Check and try again.";
			}

			while (rdr.Read())
			{
				string Field1 = rdr.GetValue(0).ToString().Trim();
				string Field2 = rdr.GetValue(1).ToString().Trim();

				//Run the macros by supplying the necessary arguments
				xlApp.Run(function, Field1, Field2);
			}

			rdr.Close();

			//Clean-up: Close the workbook
			xlWorkBook.Close(false);

			// Manual disposal because of COM
			while (Marshal.ReleaseComObject(xlWorkBook) != 0) { }

			xlWorkBook = null;

			GC.Collect();
			GC.WaitForPendingFinalizers();

			return "";
		}

		#endregion

	}

	public class Status
	{
		string internalSiteID;
		public string GADSDate;
		public DateTime dtGADS;
		public string TimelineDate;
		public DateTime dtTimeline;
		public string ROODsDate;
		public DateTime dtROODs;
		public string UploadDate;
		public DateTime dtUpload;
		public string FileDate;
		public DateTime dtFile;
		public string CalcsDate;
		public DateTime dtCalcs;
		public string PricingHubDate;
		public DateTime dtHub;
		public string PricingHub;
		private SqlConnection connPowerWork = new SqlConnection(Properties.Settings.Default.DatabaseConnection);
		public string statusErrors;

		public Status(string SiteID)
		{
			internalSiteID = SiteID.Trim();
			
		}

		public void UpdateStatus()
		{
			connPowerWork.Open();
			GetGADSDate();
			GetTimelineDate();
			GetROODsDate();
			GetUploadDate();
			GetFileDate();
			GetPricingHub();
			GetCalcsDate();
			CheckStatusErrors();
		}

		private void CheckStatusErrors()
		{
			statusErrors = "";

			if (dtGADS.Year == 1900)
				statusErrors += "GADS not done. ";

			if (dtTimeline.Year == 1900)
				statusErrors += "Timeline not done. ";

			if (dtROODs.Year == 1900)
				statusErrors += "ROODs not done. ";

			if (dtFile.Year == 1900)
				statusErrors += "Input file not found. ";

			if (dtUpload.Year == 1900)
				statusErrors += "Input file not uploaded. ";

			if (dtCalcs.Year == 1900)
				statusErrors += "Calcs not done. ";

			if (dtTimeline < dtGADS)
				statusErrors += "Timeline before GADS. ";

			if (dtROODs < dtTimeline)
				statusErrors += "ROODs before Timeline. ";

			if (dtCalcs < dtTimeline)
				statusErrors += "Calcs before Timeline. ";

			if (dtCalcs < dtROODs)
				statusErrors += "Calcs before ROODs. ";

			if (dtCalcs < dtUpload)
				statusErrors += "Calcs before Upload. ";



		}

		private void GetGADSDate()
		{
			string sqlQuery = "SELECT GADSAR = DATEADD(hh,-5,CAST(MAX(TimeStamp) AS SMALLDATETIME)) FROM PowerWork.dbo.TSort t LEFT JOIN PowerWork.dbo.NERCTurbine nt ON nt.Refnum = t.Refnum  LEFT JOIN GADSOS.GADSNG.PerformanceDataAR p ON p.UtilityUnitCode = nt.UtilityUnitCode AND p.Year = t.EvntYear WHERE t.SiteID = '" + internalSiteID + "' GROUP BY t.SiteID";

			dtGADS = GetDateValue(sqlQuery);

			GADSDate = GetDateText(dtGADS, "No GADS data.");

			return;
		}

		private void GetTimelineDate()
		{
			string sqlQuery = "SELECT Timeline = MAX(MessageTime) FROM TSort t LEFT JOIN PowerWork.dbo.MessageLog m ON m.Refnum = t.Refnum OR m.Refnum = t.SiteID WHERE t.SiteID = '" + internalSiteID + "' AND m.Source = 'Timeline' AND m.MessageTime IS NOT NULL";

			dtTimeline = GetDateValue(sqlQuery);

			TimelineDate = GetDateText(dtTimeline, "No Timeline data.");

			return;
		}

		private void GetCalcsDate()
		{
			string sqlQuery = "SELECT Calcs = MAX(MessageTime) FROM TSort t LEFT JOIN PowerWork.dbo.MessageLog m ON m.Refnum = t.Refnum OR m.Refnum = t.SiteID WHERE t.SiteID = '" + internalSiteID + "' AND m.Source IN ('FinalCalcs','Calculations') AND m.MessageTime IS NOT NULL";

			dtCalcs = GetDateValue(sqlQuery);

			CalcsDate = GetDateText(dtCalcs, "Calcs not done.");

			return;
		}

		private void GetROODsDate()
		{
			string sqlQuery = "SELECT ROODs = MAX(MessageTime) FROM TSort t LEFT JOIN PowerWork.dbo.MessageLog m ON m.Refnum = t.Refnum OR m.Refnum = t.SiteID WHERE t.SiteID = '" + internalSiteID + "' AND m.Source = 'ROODs' AND m.MessageTime IS NOT NULL";

			dtROODs = GetDateValue(sqlQuery);

			ROODsDate = GetDateText(dtROODs, "No ROODs data.");

			return;
		}

		private void GetUploadDate()
		{
			string sqlQuery = "SELECT Upload = CAST(MAX(messagetime) AS SMALLDATETIME) FROM TSort t LEFT JOIN PowerWork.dbo.MessageLog m ON m.Refnum = t.Refnum OR m.Refnum = t.SiteID WHERE t.SiteID = '" + internalSiteID + "' AND m.Source = 'UpLoad 3.0' AND m.MessageTime IS NOT NULL";

			dtUpload = GetDateValue(sqlQuery);

			UploadDate = GetDateText(dtUpload, "No Uploads.");

			return;
		}

		private System.DateTime GetDateValue(string sql)
		{
			//check if it is a valid siteid, if not, error message
			SqlCommand cmd = new SqlCommand(sql, connPowerWork);
			SqlDataReader rdr = cmd.ExecuteReader();

			System.DateTime dt = new DateTime();

			while (rdr.Read())
			{
				dt = (System.DateTime)rdr.GetSqlDateTime(0);
			}

			rdr.Close();
			return dt;

		}

		private string GetDateText(DateTime dt, string text)
		{
			if (dt.Year > 1900)
			{
				text = string.Format("{0} {1}", dt.ToShortDateString(), dt.ToString("HH:mm"));
			}

			return text;
		}

		private void GetFileDate()
		{
			string path = @"K:\STUDY\Power\20" + internalSiteID.Substring(internalSiteID.Length - 2, 2) + @"\Plant\" + internalSiteID + @"\" + internalSiteID + @".xls" ;

			dtFile = new DateTime();

			if (File.Exists(path))
			{
				FileInfo fi = new FileInfo(path);
				dtFile = fi.LastWriteTime;
			}
			FileDate = GetDateText(dtFile, "No input files.");
			return;
		}

		private void GetPricingHub()
		{
			//split in two so i can use GetDateValue for one of them
			string sqlQuery = "SELECT DISTINCT p.LatestPrice FROM TSort t LEFT JOIN PricingHub_LU p ON p.PricingHub = t.PricingHub WHERE t.SiteID = '" + internalSiteID + "'";
			dtHub = GetDateValue(sqlQuery);//, "No Pricing data.");
			
			PricingHubDate = GetDateText(dtHub, "No Pricing data.");

			sqlQuery = "SELECT DISTINCT t.PricingHub FROM TSort t LEFT JOIN PricingHub_LU p ON p.PricingHub = t.PricingHub WHERE t.SiteID = '" + internalSiteID + "'";
			SqlCommand cmd = new SqlCommand(sqlQuery, connPowerWork);
			SqlDataReader rdr = cmd.ExecuteReader();

			while (rdr.Read())
			{
				PricingHub = rdr.GetValue(0).ToString();
			}
			rdr.Close();


			return;
		}

	}
}
