﻿using System;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using ComUtilsAlias = Common.Utility;
using ComUIUtilsAlias = Common.UI;

namespace PowerCPA
{
    public partial class FormMain : Form
    {
        #region 3rd Party Controls
        #endregion 3rd Party Controls

        #region User Controls
        #endregion User Controls

        #region Window Controls
        #endregion Window Controls

        #region Delegates

        #endregion Delegates

        #region EventHandlers

        #endregion EventHandlers

        #region Events Raised
        #endregion Events Raised

        #region Variables
        #region Local Variables

        #endregion Local Variables

        #region Global Variables

        #endregion Global Variables

        #region Externally triggered event handlers
        #endregion Externally triggered event handlers

        #endregion Variables

        #region Public Properties

        #endregion Public Properties

        #region Constructors

        public FormMain()
        {
            InitializeComponent();
            Globals.StatusMessageChanged += new EventHandler(this.StatusMessageTextChanged);
            Globals.StatusBarLastProcessChanged += new EventHandler(this.StatusBarLastProcessChanged);
            Globals.StatusBarProgressBarValueChanged += new EventHandler(this.StatusBarProgressBarValueChanged);
            Globals.StatusBarProgressBarMaxChanged += new EventHandler(this.StatusBarProgressBarMaxChanged);

            Globals.dtErrors.Columns.Add(new DataColumn("TabName", typeof(string)));
            Globals.dtErrors.Columns.Add(new DataColumn("UtilityUnitCode", typeof(string)));
            Globals.dtErrors.Columns.Add(new DataColumn("Line", typeof(int)));
            Globals.dtErrors.Columns.Add(new DataColumn("Error", typeof(string)));

            Globals.dtErrSqlStatements.Columns.Add(new DataColumn("SqlStatement", typeof(string)));

            LoadConfiguration();

            Text = String.Format("{0} ({1})", Text, Globals.DataSource);

            // initialized clock
            ClockTimer.Interval = 5000;	// 5 seconds
            ClockTimer.Enabled = true;
            statusBarClock.Text = System.DateTime.Now.ToShortTimeString();
        }
        #endregion Constructors

        #region Overrides
        #endregion Overrides

        #region Form Load
        private void OnFormLoad(object sender, EventArgs e)
        {
            if (Globals.bFormCancel)
                CloseApplication();
            lblMainStartupMessage.Text = "Select process step from the buttons below.";
            Globals.CurrentProcessControl = Globals.ProcessCtrlID.Empty;
        }
        #endregion Form Load

        #region Events
        #region Form Events
        private void OnResizeFormMain(object sender, EventArgs e)
        {
            ComUIUtilsAlias.Utils.CenterControlItems(pnlBottom);
        }

        #endregion Form Events

        #region Click Events

        private void OnClickAbout(object sender, EventArgs e)
        {
            AboutBox dlg = new AboutBox();
            dlg.ShowDialog();
        }
        private void OnClickClose(object sender, EventArgs e)
        {
            CloseApplication();
        }
        private void OnClickImportNonNERCExcel(object sender, EventArgs e)
        {
            SetCurrentProcessControl(Globals.ProcessCtrlID.ImportNonNERC);
        }
        private void OnClickCreateLocalDatabase(object sender, EventArgs e)
        {
            SetCurrentProcessControl(Globals.ProcessCtrlID.CreateLocalDatabase);
        }
        private void OnClickUploadLocalDataToCPA(object sender, EventArgs e)
        {
            SetCurrentProcessControl(Globals.ProcessCtrlID.UploadLocalDataToCPA);
        }
        private void OnClosing(object sender, FormClosingEventArgs e)
        {
            SaveConfiguration();
        }

        #endregion Click Events

        #endregion Events

        #region Methods
        #region Public Methods
        #endregion Public Methods

        #region Private Methods
        private void StatusMessageTextChanged(object sender, EventArgs e)
        {
            statusBarStatus.Text = Globals.StatusMessageText;
            this.Update();
        }

        private void StatusBarLastProcessChanged(object sender, EventArgs e)
        {
            statusBarLastProcess.Text = Globals.StatusBarLastProcessText;
            this.Update();
        }
        private void StatusBarProgressBarValueChanged(object sender, EventArgs e)
        {
            statusBarProgressBar.Value = Globals.StatusBarProgressBarValue;
            this.Update();
        }
        private void StatusBarProgressBarMaxChanged(object sender, EventArgs e)
        {
            statusBarProgressBar.Maximum = Globals.StatusBarProgressBarMax;
            Update();
        }
        private void LoadConfiguration()
        {
            // Apply Configuration Data
            Globals.bRunningLocal = ((ConfigurationManager.AppSettings["Local"].ToUpper() == "TRUE") ? true : false);
            Globals.DataSource = Globals.bRunningLocal ? "(local)" : ConfigurationManager.AppSettings["DataSource"];
            Globals.InitialDirectory = ConfigurationManager.AppSettings["InitialDirectory"];

            Globals.GADSDirectory = ConfigurationManager.AppSettings["GADSDirectory"];
            if (!Globals.GADSDirectory.EndsWith(@"\"))
            {
                Globals.GADSDirectory += @"\";
            }
            Globals.GADSMasterDirectory = ConfigurationManager.AppSettings["GADSMasterDirectory"];
            if (!Globals.GADSMasterDirectory.EndsWith(@"\"))
            {
                Globals.GADSMasterDirectory += @"\";
            }
            Globals.StatusBarLastProcessText = ConfigurationManager.AppSettings["LastProcessText"];

            string _uid = string.Empty;
            string _pwd = string.Empty;
            _uid = ConfigurationManager.AppSettings["UserID"];
            _pwd = ConfigurationManager.AppSettings["Password"];

            Globals.UserID = Common.Utility.Security.Encryption.Decrypt(_uid, Globals.EncryptionKey, Globals.EncryptionIV);
            Globals.Password = Common.Utility.Security.Encryption.Decrypt(_pwd, Globals.EncryptionKey, Globals.EncryptionIV);

            string _formset = string.Empty;
            _formset = ConfigurationManager.AppSettings["Formset"];
            if (_formset.Length > 0)
            {
                string[] _settings = _formset.Split(',');
                if (_settings.Count() == 4)
                {
                    this.Location = new System.Drawing.Point(int.Parse(_settings[0]), int.Parse(_settings[1]));
                    this.Size = new System.Drawing.Size(int.Parse(_settings[2]), int.Parse(_settings[3]));
                }
            }

            // now get the list of valid Developers Users
            string _Developers = ConfigurationManager.AppSettings["Developers"];
            string[] lstDevelopers = ComUtilsAlias.Security.Encryption.Decrypt(_Developers, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');
            Array.ForEach(lstDevelopers,
            delegate(string str)
            {
                Globals.Developers.Add(str.ToUpper());
            });

            // now get the list of valid Developers Users
            string _Administrators = ConfigurationManager.AppSettings["Administrators"];
            string[] lstAdministrators = ComUtilsAlias.Security.Encryption.Decrypt(_Administrators, Globals.EncryptionKey, Globals.EncryptionIV).Split(';');
            Array.ForEach(lstAdministrators,
            delegate(string str)
            {
                Globals.Administrators.Add(str.ToUpper());
            });

            // read in the defaults used in both the Events & Perf files
            Globals.TIMESTAMP = DateTime.Parse(ConfigurationManager.AppSettings["TimeStamp"]);

            // read in the defaults used in Events
            //Globals.CAUSECODEEXT = ConfigurationManager.AppSettings["CauseCodeExt"];
            Globals.CONTRIBCODE = byte.Parse(ConfigurationManager.AppSettings["ContribCode"]);
            Globals.PRIMARYALERT = ((ConfigurationManager.AppSettings["PrimaryAlert"] == "True") ? true : false);
            Globals.MANHOURSWORKED = short.Parse(ConfigurationManager.AppSettings["ManhoursWorked"]);
            Globals.REVMONTHCARD01 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard01"]);
            Globals.REVMONTHCARD02 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard02"]);
            Globals.REVMONTHCARD03 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard03"]);
            Globals.DOMINANTDERATE = ((ConfigurationManager.AppSettings["DominantDerate"] == "True") ? true : false);
            Globals.REVISIONCARD01 = char.Parse(ConfigurationManager.AppSettings["RevisionCard01"]);
            Globals.REVISIONCARD02 = char.Parse(ConfigurationManager.AppSettings["RevisionCard02"]);
            Globals.REVISIONCARD03 = char.Parse(ConfigurationManager.AppSettings["RevisionCard03"]);
            Globals.CARRYOVERLASTYEAR = ((ConfigurationManager.AppSettings["CarryOverLastYear"] == "True") ? true : false);
            Globals.CARRYOVERNEXTYEAR = ((ConfigurationManager.AppSettings["CarryOverNextYear"] == "True") ? true : false);
            Globals.VERBALDESC1 = ConfigurationManager.AppSettings["VerbalDesc1"];

            // read in the defaults used in Perf
            Globals.GROSSMAXCAP = decimal.Parse(ConfigurationManager.AppSettings["GrossMaxCap"]);
            Globals.GROSSDEPCAP = decimal.Parse(ConfigurationManager.AppSettings["GrossDepCap"]);
            Globals.GROSSGEN = decimal.Parse(ConfigurationManager.AppSettings["GrossGen"]);
            Globals.ATTEMPTEDSTARTS = int.Parse(ConfigurationManager.AppSettings["AttemptedStarts"]);
            Globals.ACTUALSTARTS = int.Parse(ConfigurationManager.AppSettings["ActualStarts"]);
            Globals.TYPUNITLOADING = byte.Parse(ConfigurationManager.AppSettings["TypUnitLoading"]);
            Globals.PRIBTUS = int.Parse(ConfigurationManager.AppSettings["PriBtus"]);
            Globals.SECBTUS = int.Parse(ConfigurationManager.AppSettings["SecBtus"]);
            Globals.TERBTUS = int.Parse(ConfigurationManager.AppSettings["TerBtus"]);
            Globals.QUABTUS = int.Parse(ConfigurationManager.AppSettings["QuaBtus"]);
            Globals.REVMONTHCARD1 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard1"]);
            Globals.REVMONTHCARD2 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard2"]);
            Globals.REVMONTHCARD3 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard3"]);
            Globals.REVMONTHCARD4 = DateTime.Parse(ConfigurationManager.AppSettings["RevMonthCard4"]);
            Globals.INACTIVEHOURS = int.Parse(ConfigurationManager.AppSettings["InactiveHours"]);
            Globals.RSHOURS = decimal.Parse(ConfigurationManager.AppSettings["RSHours"]);
            Globals.PUMPINGHOURS = decimal.Parse(ConfigurationManager.AppSettings["PumpingHours"]);
            Globals.SYNCHCONDHOURS = decimal.Parse(ConfigurationManager.AppSettings["SynchCondHours"]);
            Globals.PLANNEDOUTAGEHOURS = decimal.Parse(ConfigurationManager.AppSettings["PlannedOutageHours"]);
            Globals.FORCEDOUTAGEHOURS = decimal.Parse(ConfigurationManager.AppSettings["ForcedOutageHours"]);
            Globals.MAINTOUTAGEHOURS = decimal.Parse(ConfigurationManager.AppSettings["MaintOutageHours"]);
            Globals.EXTOFSCHEDOUTAGES = decimal.Parse(ConfigurationManager.AppSettings["ExtofSchedOutages"]);
            Globals.REVISIONCARD1 = char.Parse(ConfigurationManager.AppSettings["RevisionCard1"]);
            Globals.REVISIONCARD2 = char.Parse(ConfigurationManager.AppSettings["RevisionCard2"]);
            Globals.REVISIONCARD3 = char.Parse(ConfigurationManager.AppSettings["RevisionCard3"]);
            Globals.REVISIONCARD4 = char.Parse(ConfigurationManager.AppSettings["RevisionCard4"]);

            //DisplayAppSettings();
        }
        private void SaveConfiguration()
        {
            System.Configuration.Configuration config =
                            ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            StringBuilder formInfo = new StringBuilder();
            Rectangle screen = That.Computer.Screen.Bounds;
            int x=this.Location.X;
            int y=this.Location.Y;
            if ((x < screen.Left) || (x>screen.Right))
                x = 0;
            if ((y < screen.Top)|| (y>screen.Bottom))
                y = 0;
            formInfo.Append(x.ToString() + ",");
            formInfo.Append(y.ToString() + ",");
            formInfo.Append(this.Size.Width.ToString() + ",");
            formInfo.Append(this.Size.Height.ToString() + "");

            config.AppSettings.Settings.Remove("Formset");
            config.AppSettings.Settings.Add("Formset", formInfo.ToString());

            config.AppSettings.Settings.Remove("LastProcessText");
            config.AppSettings.Settings.Add("LastProcessText", Globals.StatusBarLastProcessText);

            // Save the configuration file.
            config.Save(ConfigurationSaveMode.Modified);

            // Force a reload of a changed section.
            ConfigurationManager.RefreshSection("appSettings");

            ////Console.WriteLine("Using AppSettingSection Settings:");
            //DisplayAppSettings();
        }
        private void DisplayAppSettings()
        {
            // Get the configuration file.
            System.Configuration.Configuration config =
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            //Get the appSettings section.
            AppSettingsSection appSettings =
                (AppSettingsSection)config.GetSection("appSettings");
            if (appSettings != null)
            {
                foreach (string key in appSettings.Settings.AllKeys)
                {
                    string value = appSettings.Settings[key].Value;
                    Console.WriteLine("Key: {0} Value: {1}", key, value);
                }
            }
        }
        private void CloseApplication()
        {
            Application.Exit();
        }
        private void SetCurrentProcessControl(Globals.ProcessCtrlID _id)
        {
            lblMainStartupMessage.Visible = false;
            if (pnlMain.Controls.Count > 0)
                pnlMain.Controls.RemoveAt(0);
            switch (_id)
            {
                case Globals.ProcessCtrlID.Empty:
                    break;
                case Globals.ProcessCtrlID.ImportNonNERC:
                    PowerCPA.UserControls.ctrlImportNonNERC ctrl1 = new PowerCPA.UserControls.ctrlImportNonNERC();
                    ctrl1.Dock = System.Windows.Forms.DockStyle.Fill;
                    this.pnlMain.Controls.Add(ctrl1);
                    break;
                case Globals.ProcessCtrlID.CreateLocalDatabase:
                    PowerCPA.UserControls.ctrlCreateLocalDatabase ctrl2 = new PowerCPA.UserControls.ctrlCreateLocalDatabase();
                    ctrl2.Dock = System.Windows.Forms.DockStyle.Fill;
                    this.pnlMain.Controls.Add(ctrl2);
                    break;
                case Globals.ProcessCtrlID.UploadLocalDataToCPA:
                    PowerCPA.UserControls.ctrlUploadLocalDataToCPA ctrl3 = new PowerCPA.UserControls.ctrlUploadLocalDataToCPA();
                    ctrl3.Dock = System.Windows.Forms.DockStyle.Fill;
                    this.pnlMain.Controls.Add(ctrl3);
                    break;
                default:
                    break;
            }
            Globals.CurrentProcessControl = _id;
        }
        #endregion Private Methods

        #endregion Methods

        #region Timer

        private void OnTickClockTimer(object sender, System.EventArgs e)
        {
            statusBarClock.Text = System.DateTime.Now.ToShortTimeString();
        }

        #endregion Timer




    }
}
