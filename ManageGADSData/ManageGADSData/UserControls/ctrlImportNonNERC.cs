﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Collections.Generic;

using DataLayerAlias = PowerCPA.DataLayer.DataLayer;
using UtilitiesAlias = PowerCPA.Utilities;
using ComUtilsAlias = Common.Utility;
using ComUIUtilsAlias = Common.UI;
using DBUtilsAlias = Common.Database;

namespace PowerCPA.UserControls
{
    public partial class ctrlImportNonNERC : BaseUserControl
    {
        #region 3rd Party Controls
        #endregion 3rd Party Controls

        #region User Controls
        #endregion User Controls

        #region Delegates

        #endregion Delegates

        #region EventHandlers
        #endregion EventHandlers

        #region Events Raised
        #endregion Events Raised

        #region Variables
        #region Local Variables
        private string m_CompanyID = string.Empty;
        private string m_GADSMasterFile = Globals.GADSMasterDirectory + @"GADSNG.MDB";
        private string m_ProjectDirectory = string.Empty;
        private string m_ProjectFilename = string.Empty;
        protected Dictionary<string, string> m_Sheetnames = new Dictionary<string, string>();
        #endregion Local Variables

        #region Global Variables

        #endregion Global Variables

        #endregion Variables

        #region Public Properties

        #endregion Public Properties

        #region Constructors
        public ctrlImportNonNERC()
        {
            InitializeComponent();
            lblControlTitle.Text = "Import Excel Event or Performance data into local *.mdb for selected company";

            Globals.bInitialLoad = true;

            cmbCompanyID.DataSource = GetLocalCompanyList();
            cmbCompanyID.SelectedIndex = -1;
            Globals.bInitialLoad = false;

            // set up restrictions on the datagrid so user can NOT make any changes
            datagridGADSData.AllowUserToOrderColumns = true;
            datagridGADSData.AllowUserToAddRows = false;
            datagridGADSData.AllowUserToDeleteRows = false;
            datagridGADSData.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;

            btnImportData.Enabled = false;
            radEventData.Checked = false;
            radPerfData.Checked = false;
            radEventData.Enabled = false;
            radPerfData.Enabled = false;
            btnViewData.Enabled = false;
            btnImportData.Enabled = false;
            ComUIUtilsAlias.Utils.CenterControlItems(pnlTopImportType);
            datagridGADSData.Visible = false;
            pnlExcelCheckList.Visible = true;
        }
        #endregion Constructors

        #region Overrides
        #endregion Overrides

        #region Form Load
        private void OnLoad(object sender, EventArgs e)
        {
            SetInitialValuesBasedOnRadioButtonCheckedState();
            SetControlStatesBasedOnCheckBox();
        }
        #endregion Form Events

        #region Events
        #region Form Events
        private void OnResizeControl(object sender, EventArgs e)
        {
            ComUIUtilsAlias.Utils.CenterControlItems(this.pnlTopImportType);
        }

        #region Click Events
        private void OnClickFindExcelFile(object sender, EventArgs e)
        {
            datagridGADSData.DataSource = null;
            Globals.dtErrors.Rows.Clear();
            Globals.StatusMessageText = "Ready";
            Globals.StatusBarProgressBarMax = 0;
            Globals.StatusBarProgressBarValue = 0;
            btnViewData.Enabled = false;
            btnSelectTabs.Enabled = true;
            btnImportData.Enabled = false;

            Globals.ExcelDataFile = string.Empty;
            OpenFileDialog openDataFileDlg = new OpenFileDialog();
            openDataFileDlg.DefaultExt = ".xls";
            openDataFileDlg.Filter = "Excel (*.xls)|*.xls| All files (*.*)|*.*";
            openDataFileDlg.Multiselect = false;
            openDataFileDlg.InitialDirectory = Globals.InitialDirectory;

            DialogResult result = openDataFileDlg.ShowDialog();
            if (DialogResult.OK == result)
            {
                Globals.ExcelDataFile = openDataFileDlg.FileName;
                SetControlStatesBasedOnCheckBox();
            }
            else
            {
                return;
            }
            txtExcelFile.Text = Globals.ExcelDataFile;
        }
        private void OnSelectedIndexChangedCompanyID(object sender, EventArgs e)
        {
            btnViewData.Enabled = false;
            btnSelectTabs.Enabled = true;
            btnImportData.Enabled = false;
            pnlExcelCheckList.Visible = false;
            datagridGADSData.Visible = true;

            datagridGADSData.DataSource = null;
            if (Globals.bInitialLoad)
                return;
            else
            {
                Globals.dtErrors.Rows.Clear();
                Globals.StatusMessageText = "Ready";
                Globals.StatusBarProgressBarMax = 0;
                Globals.StatusBarProgressBarValue = 0;
                btnImportData.Enabled = false;
                btnViewData.Enabled = false;
                //radEventData.Checked = false;
                //radPerfData.Checked = false;

                Globals.AccessDataFile = string.Empty;

                m_CompanyID = cmbCompanyID.SelectedValue.ToString().Trim();
                m_ProjectDirectory = Globals.GADSDirectory + Globals.PROJECTDIRECTORY.Replace("XXCOMPANYIDXX", m_CompanyID);
                m_ProjectFilename = m_ProjectDirectory + @"GADSNG.MDB";

                Globals.AccessDataFile = m_ProjectFilename;

                if (!File.Exists(Globals.AccessDataFile))
                {
                    MessageBox.Show("Local GADS database not found");
                    return;
                }
                SetControlStatesBasedOnCheckBox();
                txtAccessFile.Text = Globals.AccessDataFile;
            }
        }
        private void OnClickSelectTabs(object sender, EventArgs e)
        {
            datagridGADSData.DataSource = null;
            if (String.IsNullOrEmpty(txtExcelFile.Text))
            {
                MessageBox.Show("You must select the Excel file to import");
                return;
            }

            m_Sheetnames = DataLayer.DataLayer.GetSheetNames();
            FormSheetSelection dlg = new FormSheetSelection();

            dlg.SheetNames = m_Sheetnames;
            DialogResult result = dlg.ShowDialog();
            if (DialogResult.OK == result)
            {
                m_Sheetnames.Clear();
                m_Sheetnames = dlg.SelectedSheets;
                btnViewData.Enabled = true;
                btnImportData.Enabled = false;
            }
        }
        private void OnClickViewData(object sender, EventArgs e)
        {
            datagridGADSData.DataSource = null;
            radEventData.Enabled = false;
            radPerfData.Enabled = false;
            btnViewData.Enabled = false;
            bool result = false;

            if (String.IsNullOrEmpty(txtExcelFile.Text))
            {
                MessageBox.Show("You must select the Excel file to import");
                return;
            }
            // switch read methods depending on type of data selected
            if (radEventData.Checked)
            {
                Globals.StatusMessageText = "Reading Event Data from Excel";
                result = DataLayerAlias.ReadExcelEventFile(m_Sheetnames);
                if (result)
                {
                    if (Globals.dtErrors.Rows.Count > 0)
                    {
                        datagridGADSData.DataSource = Globals.dtErrors;
                        btnImportData.Enabled = false;
                        Globals.StatusMessageText = "Input data had [" + Globals.dtErrors.Rows.Count.ToString() + "] errors that need to be corrected";
                    }
                    else
                    {
                        datagridGADSData.DataSource = Globals.WorkListEvents;
                        if (Globals.dtGADSNG.Rows.Count > 1)
                        {
                            btnImportData.Enabled = true;
                            Globals.StatusMessageText = "Loaded [" + Globals.WorkListEvents.Count.ToString() + "] Event records, review before clicking Import";
                        }
                        else
                        {
                            MessageBox.Show("Nothing to import, either no records or problem reading Excel data");
                            Globals.StatusMessageText = "Ready";
                        }
                    }
                }
            }
            if (radPerfData.Checked)
            {
                Globals.StatusMessageText = "Reading Performance Data from Excel";
                DataLayerAlias.SetupConnectStringForAccessDatabase();
                result = DataLayerAlias.ReadExcelPerfFile(m_Sheetnames);
                if (result)
                {
                    if (Globals.dtErrors.Rows.Count > 0)
                    {
                        datagridGADSData.DataSource = Globals.dtErrors;
                        btnImportData.Enabled = false;
                        Globals.StatusMessageText = "Input data had [" + Globals.dtErrors.Rows.Count.ToString() + "] errors that need to be corrected";
                    }
                    else
                    {
                        datagridGADSData.DataSource = Globals.WorkListPerfs;// Globals.dtGADSNG;
                        btnImportData.Enabled = true;
                        Globals.StatusMessageText = "Loaded [" + Globals.dtGADSNG.Rows.Count.ToString() + "] Performance records, review before clicking Import";
                    }
                }
            }
        }
        private void OnClickImportData(object sender, EventArgs e)
        {
            bool result = false;

            DataLayerAlias.SetupConnectStringForAccessDatabase();

            // switch read methods depending on type of data selected
            if (radEventData.Checked)
            {
                Globals.StatusMessageText = "Importing Event Data to GADSNG Database";
                result = DataLayerAlias.SaveEventsToDataSet();
                if (Globals.dsGADSNG.Tables["EventData01"].Rows.Count > 0)
                {
                    DataLayerAlias.UploadEventData01RecordsToAccess();
                    if (Globals.BuildDefPerfRecords)
                    {
                        DataLayerAlias.UploadPerformanceDataRecordsToAccess();
                    }
                }
                if (result)
                {
                    if (Globals.dtErrors.Rows.Count > 0)
                    {
                        datagridGADSData.DataSource = Globals.dtErrors;
                        btnImportData.Enabled = false;
                        Globals.StatusMessageText = "Input data had [" + Globals.dtErrors.Rows.Count.ToString() + "] errors that need to be corrected";
                    }
                    else
                    {
                        MessageBox.Show("Import Complete, now Validate in GADSNxL");
                    }
                }
            }
            else if (radPerfData.Checked)
            {
                Globals.StatusMessageText = "Importing Perfomance Data to GADSNG Database";
                result = DataLayerAlias.SavePerfToDataSet();
                if (Globals.dsGADSNG.Tables["PerformanceData"].Rows.Count > 0)
                {
                    DataLayerAlias.UploadPerformanceDataRecordsToAccess();
                }
                MessageBox.Show("Import Complete, now Validate in GADSNxL");
            }
            if (Globals.dtErrors.Rows.Count > 0)
            {
                Globals.StatusMessageText = "Error - review data listed above.";
                this.datagridGADSData.DataSource = Globals.dtErrors;
                if (Globals.dtErrSqlStatements.Rows.Count > 0)
                    Globals.dtErrSqlStatements.WriteXml("AccessErrorSqlStatements.xml");
            }
            else
            {
                Globals.StatusMessageText = "Ready";
                Globals.StatusBarLastProcessText = "Imported " + m_CompanyID + " - " +
                   DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
            }
            btnImportData.Enabled = false;
        }
        private void OnClickEventData(object sender, EventArgs e)
        {
            SetInitialValuesBasedOnRadioButtonCheckedState();
        }

        private void OnClickPerfData(object sender, EventArgs e)
        {
            SetInitialValuesBasedOnRadioButtonCheckedState();
        }
        private void OnCheckedChangedBuildDefaultPerfRecords(object sender, EventArgs e)
        {
            SetControlStatesBasedOnCheckBox();
        }

        #endregion Click Events

        #endregion Form Events
        #endregion Events

        #region Methods
        #region Public Methods
        #endregion Public Methods

        #region Private Methods
        private List<string> GetLocalCompanyList()
        {
            List<string> directories = new List<string>();

            DirectoryInfo di = new DirectoryInfo(Globals.GADSDirectory);
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                directories.Add(dir.Name);
            }


            return directories;
        }
        private void SetInitialValuesBasedOnRadioButtonCheckedState()
        {
            if (radEventData.Checked)
            {
                btnViewData.Enabled = false;
                btnImportData.Enabled = false;

                // set the column number for the common field names
                Globals.col_utilityunitcode = 2;
                Globals.col_year = 6;
                Globals.headerRow = 20;
            }
            else if (radPerfData.Checked)
            {
                btnViewData.Enabled = false;
                btnImportData.Enabled = false;

                // set the column number for the common field names
                Globals.col_utilityunitcode = 0;
                Globals.col_year = 2;
                Globals.headerRow = 0;
            }
        }
        private void SetControlStatesBasedOnCheckBox()
        {
            if (chkBuildDefaultPerfRecords.Checked)
            {
                radEventData.Enabled = true;
                radPerfData.Enabled = false;
                radPerfData.Checked = false;
                radEventData.Checked = true;
                Globals.BuildDefPerfRecords = true;
            }
            else
            {
                radEventData.Enabled = true;
                radPerfData.Enabled = true;
                radPerfData.Checked = false;
                radEventData.Checked = false;
                Globals.BuildDefPerfRecords = false;
            }
        }

        #endregion Private Methods




        #endregion Methods

    }
}
