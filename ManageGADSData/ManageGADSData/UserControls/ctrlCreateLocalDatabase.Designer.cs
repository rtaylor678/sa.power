﻿namespace PowerCPA.UserControls
{
    partial class ctrlCreateLocalDatabase
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCompanyID = new System.Windows.Forms.Panel();
            this.cmbCompanyID = new System.Windows.Forms.ComboBox();
            this.lblCompanyID = new System.Windows.Forms.Label();
            this.pnlTopButtons = new System.Windows.Forms.Panel();
            this.btnGetSetupOnlyData = new System.Windows.Forms.Button();
            this.btnSaveData = new System.Windows.Forms.Button();
            this.btnGetALLData = new System.Windows.Forms.Button();
            this.pnlTopInfoPrompt = new System.Windows.Forms.Panel();
            this.pnlTop = new System.Windows.Forms.Panel();
            this.pnlTopFilename = new System.Windows.Forms.Panel();
            this.txtAccessFile = new Common.CustomControls.MyTextBox();
            this.lblAccessFile = new System.Windows.Forms.Label();
            this.lblControlTitle = new System.Windows.Forms.Label();
            this.pnlMain = new System.Windows.Forms.Panel();
            this.datagridGADSData = new System.Windows.Forms.DataGridView();
            this.pnlCompanyID.SuspendLayout();
            this.pnlTopButtons.SuspendLayout();
            this.pnlTopInfoPrompt.SuspendLayout();
            this.pnlTop.SuspendLayout();
            this.pnlTopFilename.SuspendLayout();
            this.pnlMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.datagridGADSData)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlCompanyID
            // 
            this.pnlCompanyID.Controls.Add(this.cmbCompanyID);
            this.pnlCompanyID.Controls.Add(this.lblCompanyID);
            this.pnlCompanyID.Location = new System.Drawing.Point(32, 3);
            this.pnlCompanyID.Name = "pnlCompanyID";
            this.pnlCompanyID.Size = new System.Drawing.Size(323, 34);
            this.pnlCompanyID.TabIndex = 26;
            // 
            // cmbCompanyID
            // 
            this.cmbCompanyID.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCompanyID.FormattingEnabled = true;
            this.cmbCompanyID.Location = new System.Drawing.Point(116, 7);
            this.cmbCompanyID.Name = "cmbCompanyID";
            this.cmbCompanyID.Size = new System.Drawing.Size(196, 21);
            this.cmbCompanyID.TabIndex = 3;
            this.cmbCompanyID.SelectedIndexChanged += new System.EventHandler(this.OnSelectedIndexChangedCompanyID);
            // 
            // lblCompanyID
            // 
            this.lblCompanyID.Location = new System.Drawing.Point(10, 7);
            this.lblCompanyID.Name = "lblCompanyID";
            this.lblCompanyID.Size = new System.Drawing.Size(100, 20);
            this.lblCompanyID.TabIndex = 2;
            this.lblCompanyID.Text = "CompanyID";
            this.lblCompanyID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pnlTopButtons
            // 
            this.pnlTopButtons.Controls.Add(this.btnGetSetupOnlyData);
            this.pnlTopButtons.Controls.Add(this.btnSaveData);
            this.pnlTopButtons.Controls.Add(this.btnGetALLData);
            this.pnlTopButtons.Location = new System.Drawing.Point(351, 3);
            this.pnlTopButtons.Name = "pnlTopButtons";
            this.pnlTopButtons.Size = new System.Drawing.Size(460, 34);
            this.pnlTopButtons.TabIndex = 25;
            // 
            // btnGetSetupOnlyData
            // 
            this.btnGetSetupOnlyData.Enabled = false;
            this.btnGetSetupOnlyData.Location = new System.Drawing.Point(155, 6);
            this.btnGetSetupOnlyData.Name = "btnGetSetupOnlyData";
            this.btnGetSetupOnlyData.Size = new System.Drawing.Size(150, 23);
            this.btnGetSetupOnlyData.TabIndex = 21;
            this.btnGetSetupOnlyData.Text = "Get Setup Data from SQL ";
            this.btnGetSetupOnlyData.UseVisualStyleBackColor = true;
            this.btnGetSetupOnlyData.Click += new System.EventHandler(this.OnClickGetSetupOnlyData);
            // 
            // btnSaveData
            // 
            this.btnSaveData.Enabled = false;
            this.btnSaveData.Location = new System.Drawing.Point(305, 6);
            this.btnSaveData.Name = "btnSaveData";
            this.btnSaveData.Size = new System.Drawing.Size(150, 23);
            this.btnSaveData.TabIndex = 0;
            this.btnSaveData.Text = "Save Data Locally";
            this.btnSaveData.UseVisualStyleBackColor = true;
            this.btnSaveData.Click += new System.EventHandler(this.OnClickSaveData);
            // 
            // btnGetALLData
            // 
            this.btnGetALLData.Enabled = false;
            this.btnGetALLData.Location = new System.Drawing.Point(5, 6);
            this.btnGetALLData.Name = "btnGetALLData";
            this.btnGetALLData.Size = new System.Drawing.Size(150, 23);
            this.btnGetALLData.TabIndex = 20;
            this.btnGetALLData.Text = "Get ALL Data from SQL ";
            this.btnGetALLData.UseVisualStyleBackColor = true;
            this.btnGetALLData.Click += new System.EventHandler(this.OnClickGetAllData);
            // 
            // pnlTopInfoPrompt
            // 
            this.pnlTopInfoPrompt.Controls.Add(this.pnlCompanyID);
            this.pnlTopInfoPrompt.Controls.Add(this.pnlTopButtons);
            this.pnlTopInfoPrompt.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopInfoPrompt.Location = new System.Drawing.Point(0, 23);
            this.pnlTopInfoPrompt.Name = "pnlTopInfoPrompt";
            this.pnlTopInfoPrompt.Size = new System.Drawing.Size(842, 41);
            this.pnlTopInfoPrompt.TabIndex = 24;
            // 
            // pnlTop
            // 
            this.pnlTop.Controls.Add(this.pnlTopFilename);
            this.pnlTop.Controls.Add(this.pnlTopInfoPrompt);
            this.pnlTop.Controls.Add(this.lblControlTitle);
            this.pnlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTop.Location = new System.Drawing.Point(0, 0);
            this.pnlTop.Name = "pnlTop";
            this.pnlTop.Size = new System.Drawing.Size(842, 90);
            this.pnlTop.TabIndex = 28;
            // 
            // pnlTopFilename
            // 
            this.pnlTopFilename.Controls.Add(this.txtAccessFile);
            this.pnlTopFilename.Controls.Add(this.lblAccessFile);
            this.pnlTopFilename.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlTopFilename.Location = new System.Drawing.Point(0, 64);
            this.pnlTopFilename.Name = "pnlTopFilename";
            this.pnlTopFilename.Size = new System.Drawing.Size(842, 26);
            this.pnlTopFilename.TabIndex = 25;
            // 
            // txtAccessFile
            // 
            this.txtAccessFile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAccessFile.Location = new System.Drawing.Point(117, 3);
            this.txtAccessFile.Name = "txtAccessFile";
            this.txtAccessFile.ReadOnly = true;
            this.txtAccessFile.Size = new System.Drawing.Size(693, 20);
            this.txtAccessFile.TabIndex = 22;
            // 
            // lblAccessFile
            // 
            this.lblAccessFile.Location = new System.Drawing.Point(32, 3);
            this.lblAccessFile.Name = "lblAccessFile";
            this.lblAccessFile.Size = new System.Drawing.Size(85, 20);
            this.lblAccessFile.TabIndex = 21;
            this.lblAccessFile.Text = "GADS Data File:";
            this.lblAccessFile.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblControlTitle
            // 
            this.lblControlTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblControlTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblControlTitle.Location = new System.Drawing.Point(0, 0);
            this.lblControlTitle.Name = "lblControlTitle";
            this.lblControlTitle.Size = new System.Drawing.Size(842, 23);
            this.lblControlTitle.TabIndex = 29;
            this.lblControlTitle.Text = "ControlTitle";
            this.lblControlTitle.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlMain
            // 
            this.pnlMain.Controls.Add(this.datagridGADSData);
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(0, 90);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(842, 371);
            this.pnlMain.TabIndex = 29;
            // 
            // datagridGADSData
            // 
            this.datagridGADSData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.datagridGADSData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.datagridGADSData.Location = new System.Drawing.Point(0, 0);
            this.datagridGADSData.Name = "datagridGADSData";
            this.datagridGADSData.ReadOnly = true;
            this.datagridGADSData.Size = new System.Drawing.Size(842, 371);
            this.datagridGADSData.TabIndex = 0;
            this.datagridGADSData.TabStop = false;
            // 
            // ctrlCreateLocalDatabase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.pnlMain);
            this.Controls.Add(this.pnlTop);
            this.Name = "ctrlCreateLocalDatabase";
            this.Load += new System.EventHandler(this.OnLoad);
            this.pnlCompanyID.ResumeLayout(false);
            this.pnlTopButtons.ResumeLayout(false);
            this.pnlTopInfoPrompt.ResumeLayout(false);
            this.pnlTop.ResumeLayout(false);
            this.pnlTopFilename.ResumeLayout(false);
            this.pnlTopFilename.PerformLayout();
            this.pnlMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.datagridGADSData)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlTopButtons;
        private System.Windows.Forms.Button btnSaveData;
        private System.Windows.Forms.Button btnGetALLData;
        private System.Windows.Forms.Panel pnlCompanyID;
        private System.Windows.Forms.Panel pnlTopInfoPrompt;
        private System.Windows.Forms.ComboBox cmbCompanyID;
        private System.Windows.Forms.Label lblCompanyID;
        private System.Windows.Forms.Panel pnlTop;
        private System.Windows.Forms.Panel pnlTopFilename;
        private Common.CustomControls.MyTextBox txtAccessFile;
        private System.Windows.Forms.Label lblAccessFile;
        private System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.DataGridView datagridGADSData;
        private System.Windows.Forms.Label lblControlTitle;
        private System.Windows.Forms.Button btnGetSetupOnlyData;
    }
}
