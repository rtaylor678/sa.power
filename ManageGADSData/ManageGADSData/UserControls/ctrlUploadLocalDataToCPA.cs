﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

using ComUtilsAlias = Common.Utility;
using ComUIUtilsAlias = Common.UI;
using DBUtilsAlias = Common.Database;
using DataLayerAlias = PowerCPA.DataLayer.DataLayer;
using UtilitiesAlias = PowerCPA.Utilities;

namespace PowerCPA.UserControls
{
    public partial class ctrlUploadLocalDataToCPA : BaseUserControl
    {

        #region Variables
        #region Constants

        #endregion Constants

        #region Local Variables
        private string m_CompanyID = string.Empty;
        private string m_ProjectDirectory = string.Empty;
        private string m_ProjectFilename = string.Empty;

        #endregion Local Variables

        #region Global Variables

        #endregion Global Variables

        #endregion Variables

        #region Constructors
        public ctrlUploadLocalDataToCPA()
        {
            InitializeComponent();
            lblControlTitle.Text = "Upload Local GADS data up to the SQL Server for selected company";
            Globals.bInitialLoad = true;
        }
        #endregion Constructors

        #region Form Load
        private void OnLoad(object sender, EventArgs e)
        {
            Globals.PromptForSQLLogin();
            if (Globals.UserRole < (int)Globals.Roles.Consultant)
            {
                MessageBox.Show("You are not authorized to run this application");
                return;
            }
            Globals.sqlConnGD = Globals.InitializeSQLConnectionStringBuilder("GadsOS", 90);

            if (Globals.bFormCancel)
            {
                this.Enabled = false;
            }
            else
            {
                this.Enabled = true;
                try
                {
                    cmbCompanyID.DataSource = DataLayerAlias.GetSQLCompanyList(m_CompanyID);
                    cmbCompanyID.DisplayMember = "companyid";
                    cmbCompanyID.ValueMember = "companyid";
                    cmbCompanyID.SelectedIndex = -1;
                    Globals.bInitialLoad = false;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Problem retrieving list of companies from SQL.\n" + ex.Message);
                }
            }
        }
        #endregion Form Load

        #region Events
        #region Form Events
        private void OnResizeMainControl(object sender, EventArgs e)
        {
            ComUIUtilsAlias.Utils.CenterControlItems(pnlTopButtons);
            splitContainerGADSData.SplitterDistance = splitContainerGADSData.Height / 2;
        }

        #endregion Form Events

        #region Click Events
        private void OnSelectedIndexChangedCompanyID(object sender, EventArgs e)
        {
            datagridLocalData.DataSource = null;
            if (Globals.bInitialLoad)
                return;
            else
            {
                Globals.dtErrors.Rows.Clear();
                Globals.StatusMessageText = "Ready";
                Globals.StatusBarProgressBarMax = 0;
                Globals.StatusBarProgressBarValue = 0;

                Globals.AccessDataFile = string.Empty;

                m_CompanyID = cmbCompanyID.SelectedValue.ToString().Trim();
                m_ProjectDirectory = Globals.GADSDirectory + Globals.PROJECTDIRECTORY.Replace("XXCOMPANYIDXX", m_CompanyID);
                m_ProjectFilename = m_ProjectDirectory + @"GADSNG.MDB";

                Globals.AccessDataFile = m_ProjectFilename;

                if (File.Exists(Globals.AccessDataFile))
                {
                    txtAccessFile.Text = Globals.AccessDataFile;

                    btnGetData.Enabled = CheckIfReadyToGetData();
                }
                else
                {
                    btnGetData.Enabled = false;
                    MessageBox.Show("Local GADS database not found");
                    return;
                }
            }
        }
        private void OnClickGetData(object sender, EventArgs e)
        {
            string returnMessage = string.Empty;
            DialogResult dlgResult;
            DataLayerAlias.SetupConnectStringForAccessDatabase();
            DataTable dtAccess = DataLayerAlias.GetSetupValidatedRecordsFromAccess();
            this.datagridLocalData.DataSource = dtAccess;
            Globals.dsGADSNG = DataLayerAlias.GetSQLGADSData(m_CompanyID, true);
            this.datagridSQLData.DataSource = Globals.dsGADSNG.Setup;
            returnMessage = DataLayerAlias.CompareSetupRecords(dtAccess);

            switch (Globals.SetupCompareStatus)
            {
                case Globals.SetupRecordCheckReturn.Match:
                    btnUploadData.Enabled = true;
                    break;
                case Globals.SetupRecordCheckReturn.LocalGreater:
                    dlgResult = MessageBox.Show(returnMessage + " \n\nDo you want to replace SQL data with Local Records?",
                        "Local and SQL Setup data do not match",
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
                    if (dlgResult == DialogResult.No)
                    {
                        btnUploadData.Enabled = false;
                        return;
                    }
                    else
                    {
                        DataLayerAlias.TransferSetupRecordsFromDatatableDataSet(dtAccess);
                        btnUploadData.Enabled = true;
                    }
                    break;
                case Globals.SetupRecordCheckReturn.LocalSmaller:
                case Globals.SetupRecordCheckReturn.ShortnameExistAlready:
                case Globals.SetupRecordCheckReturn.UUCExistAlready:
                case Globals.SetupRecordCheckReturn.SameNumberButDontMatch:
                    btnUploadData.Enabled = false;
                    dlgResult = MessageBox.Show(returnMessage + " \n\nProblem with Local Records or SQL data must be fixed before importing this data.",
                        "Local and SQL Setup data do not match",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Stop);
                    btnUploadData.Enabled = false;
                    return;
            }
        }

        private void OnClickUploadData(object sender, EventArgs e)
        {
            DialogResult dlgResult;
            dlgResult = MessageBox.Show("Do you want to replace SQL data with Local Records?",
               "Local and SQL Setup data do not match",
               MessageBoxButtons.YesNo,
               MessageBoxIcon.Question);
            if (dlgResult == DialogResult.No)
            {
                MessageBox.Show("Nothing was uploaded");
                return;
            }
            else
            {
                btnUploadData.Enabled = false;
                if (Globals.SetupCompareStatus != Globals.SetupRecordCheckReturn.Match)
                    DataLayerAlias.ClearSQLSetupRecords(m_CompanyID);
                DataLayerAlias.UploadSetupRecordsToSQL();
                Globals.StatusBarLastProcessText = "Uploaded " + m_CompanyID + " - " +
                  DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString();
                MessageBox.Show("Complete");
            }
        }

        #endregion Click Events

        #endregion Events

        #region Methods
        private bool CheckIfReadyToGetData()
        {
            bool result = false;
            if ((txtAccessFile.Text.Length > 0) && (cmbCompanyID.SelectedIndex > -1))
                result = true;
            else
                result = false;

            return result;
        }

        #endregion Methods

    }
}
