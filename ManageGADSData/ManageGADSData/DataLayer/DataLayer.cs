﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Common.Utility.Excel;
using DBUtilsAlias = Common.Database;
using UtilLibAlias = PowerCPA.Utilities.UtilityLibrary;

namespace PowerCPA.DataLayer
{
    public class DataLayer
    {
        #region Variables
        #region Constants
        // pull down the Setup records for a company
        private static readonly string SQLCOMPANIES = "SELECT DISTINCT CompanyID FROM PowerWork.dbo.TSort ORDER BY CompanyID";

        // pull down the Setup records for a company
        private static readonly string SQLSETUP = "SELECT * FROM GADSOS.GADSNG.Setup WHERE UtilityUnitCode IN " +
            "(SELECT DISTINCT UtilityUnitCode FROM PowerWork.dbo.NERCTurbine " +
            "WHERE refnum IN (SELECT Refnum FROM PowerWork.dbo.TSort WHERE CompanyID='XXCOMPANYIDXX')) " +
            "ORDER BY UtilityUnitCode";

        // pull down the EventData01 records for a company
        private static readonly string SQLEVENTDATA01 = "SELECT * FROM GADSOS.GADSNG.EventData01 WHERE UtilityUnitCode IN " +
            "(select UtilityUnitCode FROM GADSOS.GADSNG.Setup WHERE UtilityUnitCode IN " +
            "(SELECT DISTINCT UtilityUnitCode FROM PowerWork.dbo.NERCTurbine " +
            "WHERE refnum IN (SELECT Refnum FROM PowerWork.dbo.TSort WHERE CompanyID='XXCOMPANYIDXX'))) " +
            "ORDER BY UtilityUnitCode";

        // pull down the EventData02 records for a company
        private static readonly string SQLEVENTDATA02 = "SELECT * FROM GADSOS.GADSNG.EventData02 WHERE UtilityUnitCode IN " +
            "(SELECT UtilityUnitCode FROM GADSOS.GADSNG.Setup WHERE UtilityUnitCode IN " +
            "(SELECT DISTINCT UtilityUnitCode FROM PowerWork.dbo.NERCTurbine " +
            "WHERE refnum IN (SELECT Refnum FROM PowerWork.dbo.TSort WHERE CompanyID='XXCOMPANYIDXX'))) " +
            "ORDER BY UtilityUnitCode";

        // pull down the PerformanceData records for a company
        private static readonly string SQLPERFORMANCEDATA = "SELECT * FROM GADSOS.GADSNG.PerformanceData WHERE UtilityUnitCode IN " +
            "(SELECT UtilityUnitCode FROM GADSOS.GADSNG.Setup WHERE UtilityUnitCode IN " +
            "(SELECT DISTINCT UtilityUnitCode FROM PowerWork.dbo.NERCTurbine " +
            "WHERE refnum IN (SELECT Refnum FROM PowerWork.dbo.TSort WHERE CompanyID='XXCOMPANYIDXX'))) " +
            "ORDER BY UtilityUnitCode";

        #endregion Constants

        #region Local Variables
        protected static GADSNG dsGADSNG = new GADSNG();
        protected static GADSNG dsNewGADSNG = new GADSNG();
        protected static DataReader excelReaderData = new DataReader();
        protected static bool bDataTabFound = false;
        protected static OleDbConnectionStringBuilder sbOleConnection;
        protected static DateTime m_TimeStamp;
        protected static decimal m_PriQtyBurned;
        protected static decimal m_QuaQtyBurned;
        protected static decimal m_SecQtyBurned;
        protected static decimal m_TerQtyBurned;
        protected static decimal? m_GrossDepCap;
        protected static decimal? m_GrossMaxCap;
        protected static decimal? m_NetDepCap;
        protected static decimal? m_NetMaxCap;
        protected static int m_ActualStarts;
        protected static int m_AttemptedStarts;
        protected static int m_DaylightSavingTime;
        protected static int m_PriAvgHeatContent;
        protected static int m_QuaAvgHeatContent;
        protected static int m_SecAvgHeatContent;
        protected static int m_TerAvgHeatContent;
        protected static int m_TypUnitLoading;
        protected static string m_PriFuelCode;
        protected static string m_QuaFuelCode;
        protected static string m_SecFuelCode;
        protected static string m_TerFuelCode;
        protected static string m_UnitShortName;
        protected static string m_UtilityUnitCode;
        protected static string m_VerbalDesc;
        protected static string m_NERCUtilityCode;
        protected static string m_NERCUnitCode;
        protected static decimal m_PeriodHours;
        private static int DBTRUE;
        private static int DBFALSE;

        private const int ACCESSFALSE = 0;
        private const int ACCESSTRUE = -1;
        private const int SQLTRUE = 1;
        private const int SQLFALSE = 0;
        #endregion Local Variables

        #region Global Variables
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// For guidelines regarding the creation of new enum types, see
        ///     http://msdn2.microsoft.com/en-us/library/ms229058.aspx
        /// and
        ///     http://msdn2.microsoft.com/en-us/library/4x252001.aspx
        /// </remarks>
        public enum DatabaseType
        {
            Access,
            SQL
        }

        #endregion Global Variables

        #endregion Variables

        #region Methods

        #region Public Methods
        public static void SetupConnectStringForAccessDatabase()
        {
            // setup connect string for Access database
            Globals.accessConnGD = new System.Data.OleDb.OleDbConnectionStringBuilder();
            Globals.accessConnGD.Provider = Globals.STR_MicrosoftJetOLEDB40;
            Globals.accessConnGD["User Id"] = Globals.STR_Admin;
            Globals.accessConnGD["Password"] = "";
            Globals.accessConnGD.DataSource = Globals.AccessDataFile;
        }

        public static string GetShortName(string utilityunitcode)
        {
            // "select UnitShortName from gadsng.setup where utilityunitcode='";
            string result = string.Empty;
            using (GadsngDataContext dc = new GadsngDataContext(Globals.sqlConnGD.ConnectionString))
            {
#if DEBUG
                dc.Log = Console.Out;
#endif
                var query = from dt in dc.Setups
                            where dt.UtilityUnitCode == "'" + utilityunitcode + "'"
                            select new { UnitShortName = dt.UnitShortName.ToUpper() };
                foreach (var item in query)
                {
                    result = item.UnitShortName.Trim();
                    break;
                }
            }
            return result;
        }
        public static string GetShortNameAccess(string utilityunitcode)
        {
            string sqlCmd = "select UnitShortName from setup where utilityunitcode='" + utilityunitcode + "'";
            DataTable dt;
            string result = string.Empty;

            try
            {
                dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "temp");
                result = DBUtilsAlias.Datasets.GetString(dt.Rows[0]["UnitShortName"]).Trim();

            }
            catch
            {
                result = string.Empty;
            }

            return result;
        }
        public static Dictionary<string, string> GetSheetNames()
        {
            Dictionary<string, string> Sheetnames = new Dictionary<string, string>();
            try
            {
                excelReaderData = new DataReader();
                excelReaderData.Connect(Globals.ExcelDataFile);
                excelReaderData.GetWorksheetNames();
                Sheetnames = excelReaderData.SheetNames;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Read Excel file :[" + Globals.ExcelDataFile + "]: " + ex.Message);
                Globals.StatusMessageText = "Ready";
                return Sheetnames;
            }
            finally
            {
                excelReaderData.CloseConnection();
            }
            return Sheetnames;
        }
        public static bool ReadExcelEventFile(Dictionary<string, string> _sheetnames)
        {
            Globals.dtErrors.Rows.Clear();
            int totalDataRows = 0;
            int tabDataRows = 0;
            int col = 0;
            bDataTabFound = false;

            Globals.WorkListEvents.Clear();
            Globals.StatusBarProgressBarValue = 0;
            try
            {
                excelReaderData = new DataReader();
                excelReaderData.Connect(Globals.ExcelDataFile);

                foreach (KeyValuePair<string, string> sh in _sheetnames)
                {
                    string tabName = sh.Value.ToUpper();
                    //if (tabName.EndsWith("EVENTS"))
                    //{
                    bDataTabFound = true;
                    Globals.StatusMessageText = "Reading Event Data from Excel tab [" + tabName + "]";
                    tabDataRows = 0;
                    Globals.dtGADSNG = excelReaderData.GetDataTable(sh.Key, sh.Value);
#if DEBUG
                    Globals.dtGADSNG.WriteXml(Application.StartupPath + @"\Events_" + sh.Value + ".xml");
                    StringBuilder sbcolmapping = new StringBuilder();
#endif
                    if (Globals.CN_UTILITYUNITCODE.ToUpper() == DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[0][0]).ToUpper())
                    {
                        Globals.headerRow = 0;
                        // rename columns to match the 1st row of data from Excel file
                        col = 0;
                        foreach (DataColumn dc in Globals.dtGADSNG.Columns)
                        {
                            string colName = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[0][col]).ToUpper();
                            if (colName != string.Empty)
                                dc.ColumnName = colName;
                            col++;
                        }
                        // now count the actual rows of event records
                        for (int r = Globals.headerRow + 1; r < Globals.dtGADSNG.Rows.Count; r++)
                        {
                            if (string.Empty == DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[r][Globals.CN_UTILITYUNITCODE]).Trim())
                            {
                                break;
                            }
                            tabDataRows++;
                        }
                        totalDataRows += tabDataRows;
                    }
                    else
                    {
                        // try to find the starting row and map the column #s to the right physical columns
                        bool firstFieldFound = false;
                        bool lastFieldFound = false;
                        for (col = 0; col < 5; col++)
                        {
                            for (int row = 0; row < 40; row++)
                            {
                                if (row > Globals.dtGADSNG.Rows.Count)
                                    break;
                                if (Globals.CN_EVENTNUMBER == DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[row][col]).ToUpper())
                                {
                                    firstFieldFound = true;
                                    Globals.headerRow = row;
                                    Globals.col_eventnumber = col;
                                    // now that we have the header row find the fields
                                    for (int y = col + 1; y < 20; y++)
                                    {
                                        if (y > Globals.dtGADSNG.Rows.Count)
                                            break;
                                        switch (DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[row][y]).ToUpper())
                                        {
                                            case Globals.CN_NETAVAILCAPACITY:
                                            case Globals.CN_NETAVAILCAPAC:
                                                Globals.col_netavailcapacity = y;
                                                lastFieldFound = true;
                                                break;
                                        }
                                        if (lastFieldFound)
                                            break;
                                    }
                                }
                                if (firstFieldFound)
                                    break;
                            }
                            if (firstFieldFound)
                                break;
                        }
                        // rename columns to match the 1st row of data from Excel file
                        col = Globals.col_eventnumber;
                        foreach (DataColumn dc in Globals.dtGADSNG.Columns)
                        {
                            //object objColName = Globals.dtGADSNG.Rows[Globals.headerRow][col];
                            string colName = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[Globals.headerRow][col]).Trim().ToUpper();
                            if (colName != string.Empty)
                                dc.ColumnName = colName;
                            col++;
                            if (col > Globals.col_netavailcapacity)
                                break;
                        }
                        // now count the actual rows of data records
                        tabDataRows = 0;
                        for (int r = Globals.headerRow + 1; r < Globals.dtGADSNG.Rows.Count; r++)
                        {
                            if (string.Empty == DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[r][Globals.col_eventnumber]).Trim())
                            {
                                break;
                            }
                            tabDataRows++;
                        }
                        totalDataRows += tabDataRows;
                    }
                    Globals.StatusBarProgressBarMax = tabDataRows;
                    for (int i = Globals.headerRow + 1; i < Globals.headerRow + 1 + tabDataRows; i++) //Globals.dtGADSNG.Rows.Count; i++)
                    {
                        Globals.StatusBarProgressBarValue = i - Globals.headerRow;
                        if (0 == DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_EVENTNUMBER]).Trim().Length)
                        {
                            break;
                        }
                        else
                        {
                            try
                            {
                                DateTime _StartDateTime;
                                DateTime _EndDateTime;
                                string _VerbalDesc1 = string.Empty;
                                string _VerbalDesc2 = string.Empty;
                                string _VerbalDesc86 = string.Empty;
                                string _VerbalDescFull = string.Empty;
                                short _EventNumber = 0;
                                string _UtilityUnitCode = string.Empty;
                                string _EventType = string.Empty;
                                short _Year = 0;
                                short _CauseCode = 0;
                                decimal _NetAvailCapacity = 0;
                                string _CauseCodeExt = string.Empty;

                                try
                                {
                                    _EventNumber = DBUtilsAlias.Datasets.GetShort(Globals.dtGADSNG.Rows[i][Globals.CN_EVENTNUMBER]);
                                    _UtilityUnitCode = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_UTILITYUNITCODE]).ToUpper();
                                    _EventType = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_EVENTTYPE]).ToUpper();
                                    _Year = DBUtilsAlias.Datasets.GetShort(Globals.dtGADSNG.Rows[i][Globals.CN_YEAR]);
                                    _CauseCode = DBUtilsAlias.Datasets.GetShort(Globals.dtGADSNG.Rows[i][Globals.CN_CAUSECODE]);
                                    _NetAvailCapacity = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_NETAVAILCAPACITY]);
                                    _CauseCodeExt = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_CAUSECODEEXT]).ToUpper();
                                }
                                catch
                                {
                                    MessageBox.Show("Unable to read key fields");
                                    return false;
                                }

                                try
                                {
                                    _VerbalDesc1 = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_VERBALDESC1]);
                                    if (_VerbalDesc1.Length > 31)
                                        _VerbalDesc1 = _VerbalDesc1.Substring(0, 31);
                                }
                                catch
                                {
                                    _VerbalDesc1 = string.Empty;
                                }

                                try
                                {
                                    _VerbalDesc2 = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_VERBALDESC2]);
                                    if (_VerbalDesc2.Length > 55)
                                        _VerbalDesc2 = _VerbalDesc2.Substring(0, 55);
                                }
                                catch
                                {
                                    _VerbalDesc2 = string.Empty;
                                }

                                try
                                {
                                    _VerbalDesc86 = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_VERBALDESC86]);
                                    if (_VerbalDesc86.Length > 86)
                                        _VerbalDesc86 = _VerbalDesc86.Substring(0, 86);
                                }
                                catch
                                {
                                    _VerbalDesc86 = string.Empty;
                                }

                                try
                                {
                                    _VerbalDescFull = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_VERBALDESCFULL]);
                                    if (_VerbalDescFull.Length > 4000)
                                        _VerbalDescFull = _VerbalDescFull.Substring(0, 4000);
                                }
                                catch
                                {
                                    _VerbalDescFull = string.Empty;
                                }

                                if (!DateTime.TryParse(DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_STARTDATETIME]), out _StartDateTime))
                                {
                                    _StartDateTime = DateTime.FromOADate(DBUtilsAlias.Datasets.GetDouble(Globals.dtGADSNG.Rows[i][Globals.CN_STARTDATETIME]));
                                }
                                if (!DateTime.TryParse(DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_ENDDATETIME]), out _EndDateTime))
                                {
                                    _EndDateTime = DateTime.FromOADate(DBUtilsAlias.Datasets.GetDouble(Globals.dtGADSNG.Rows[i][Globals.CN_ENDDATETIME]));
                                }

                                // do basic error checks 
                                if (_StartDateTime.CompareTo(_EndDateTime) == (int)Globals.DateComparisonResult.Later)
                                    Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "StartDateTime greater than EndDateTime");
                                if (_StartDateTime.Year != _Year)
                                    Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "StartDateTime not the same as year");
                                if (_EndDateTime.Year != _Year)
                                    Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "EndDateTime not the same as year");
                                if (_EndDateTime.Year != _StartDateTime.Year)
                                    Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "StartDateTime not the same year as EndDateTime");
                                if ((_EventType == "RS") && (_CauseCode != 0))
                                    Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "If EventType is RS then CauseCode must be 0");
                                if ((_EventType.Substring(0, 1) == "D") || (_EventType == "PD"))
                                {
                                    if (_NetAvailCapacity == 0)
                                        Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "If Derating, NetAvailCapacity can not be 0");
                                }
                                else
                                {
                                    if (_NetAvailCapacity != 0)
                                        Globals.AddErrorToTable(tabName, _UtilityUnitCode, i + 1, "If Outage, NetAvailCapacity must be 0");
                                }

                                // IF no error messages then update datarow
                                // add default values to object
                                // and add pd object to the List<>
                                if (Globals.dtErrors.Rows.Count == 0)
                                {
                                    Globals.WorkListEvents.Add(new WorkListEvents
                                                  {
                                                      EventNumber = _EventNumber,
                                                      UtilityUnitCode = _UtilityUnitCode,
                                                      EventType = _EventType,
                                                      StartDateTime = _StartDateTime,
                                                      EndDateTime = _EndDateTime,
                                                      Year = _Year,
                                                      CauseCode = _CauseCode,
                                                      CauseCodeExt = _CauseCodeExt,
                                                      VerbalDesc1 = _VerbalDesc1,
                                                      VerbalDesc2 = _VerbalDesc2,
                                                      VerbalDesc86 = _VerbalDesc86,
                                                      VerbalDescFull = _VerbalDescFull,
                                                      NetAvailCapacity = _NetAvailCapacity
                                                  });
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Error: " + ex.Message);
                                break;
                            }
                        }
                    }
                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Read Excel file :[" + Globals.ExcelDataFile + "]: " + ex.Message);
                Globals.StatusMessageText = "Ready";
                return false;
            }
            finally
            {
                excelReaderData.CloseConnection();
#if DEBUG
                //ObjectDumper.Write(Globals.workListEvents);
#endif
                if (!bDataTabFound)
                {
                    MessageBox.Show("No tab names found that end with the word [Events], " +
                        "rename the data tab(s) so they end with key phrase and try again");
                }
            }
            return true;
        }
        public static bool ReadExcelPerfFile(Dictionary<string, string> _sheetnames)
        {
            Globals.StatusMessageText = "Reading Performance Data from Excel";
            Dictionary<string, string> Sheetnames = new Dictionary<string, string>();
            Globals.dtErrors.Rows.Clear();
            Globals.dtGADSNG = new DataTable();
            Globals.StatusBarProgressBarValue = 0;
            bDataTabFound = false;
            int totalDataRows = 0;
            int tabDataRows = 0;

            try
            {
                excelReaderData = new DataReader();
                excelReaderData.Connect(Globals.ExcelDataFile);

                foreach (KeyValuePair<string, string> sh in _sheetnames)
                {
                    string tabName = sh.Value.ToUpper();
                    //if (tabName.StartsWith("PERF"))
                    //{
                    bDataTabFound = true;
                    Globals.dtGADSNG = excelReaderData.GetDataTable(sh.Key, sh.Value);

                    // rename columns to match the 1st row of data from Excel file
                    int col = 0;
                    foreach (DataColumn dc in Globals.dtGADSNG.Columns)
                    {
                        dc.ColumnName = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[0][col]);
                        col++;
                    }
                    if (Globals.dtGADSNG.Rows.Count > 0)
                        Globals.dtGADSNG.Rows[0].Delete();

                    for (int r = Globals.dtGADSNG.Rows.Count - 1; r > 0; r--)
                    {
                        if (string.Empty == DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[r][Globals.CN_UTILITYUNITCODE]).Trim())
                        {
                            Globals.dtGADSNG.Rows[r].Delete();
                        }
                    }
                    tabDataRows = Globals.dtGADSNG.Rows.Count;
                    totalDataRows += tabDataRows;

#if DEBUG
                    Globals.dtGADSNG.WriteXml(Application.StartupPath + @"\Perfomance_" + sh.Value + ".xml");
                    StringBuilder sbcolmapping = new StringBuilder();
#endif
                    Globals.StatusBarProgressBarMax = tabDataRows;
                    for (int i = Globals.headerRow; i < Globals.dtGADSNG.Rows.Count; i++)
                    {
                        Globals.StatusBarProgressBarValue = i;
                        PerformanceData pd = new PerformanceData();
                        m_UtilityUnitCode = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_UTILITYUNITCODE]);
                        if (0 == m_UtilityUnitCode.Trim().Length)
                        {
                            break;
                        }
                        else
                        {
                            try
                            {
                                GetSetupDefaults(DatabaseType.Access, m_UtilityUnitCode);
                                pd.UtilityUnitCode = m_UtilityUnitCode;
                                pd.UnitShortName = m_UnitShortName;
                                m_UnitShortName = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_UNITSHORTNAME]);
                                pd.Year = (short)DBUtilsAlias.Datasets.GetInt(Globals.dtGADSNG.Rows[i][Globals.CN_YEAR]);
                                pd.Period = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_PERIOD]);
                                pd.GrossMaxCap = m_GrossMaxCap;
                                m_GrossMaxCap = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_GROSSMAXCAP]);
                                if ((pd.GrossMaxCap != m_GrossMaxCap) && (m_GrossMaxCap > 0))
                                    pd.GrossMaxCap = m_GrossMaxCap;
                                pd.GrossDepCap = m_GrossDepCap;
                                m_GrossDepCap = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_GROSSDEPCAP]);
                                if ((pd.GrossDepCap != m_GrossDepCap) && (m_GrossDepCap > 0))
                                    pd.GrossDepCap = m_GrossDepCap;
                                pd.GrossGen = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_GROSSGEN]);
                                pd.NetMaxCap = m_NetMaxCap;
                                m_NetMaxCap = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_NETMAXCAP]);
                                if ((pd.NetMaxCap != m_NetMaxCap) && (m_NetMaxCap > 0))
                                    pd.NetMaxCap = m_NetMaxCap;
                                pd.NetDepCap = m_NetDepCap;
                                m_NetDepCap = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_NETDEPCAP]);
                                pd.NetGen = DBUtilsAlias.Datasets.GetDecimal(Globals.dtGADSNG.Rows[i][Globals.CN_NETGEN]);
                                if ((pd.NetDepCap != m_NetDepCap) && (m_NetDepCap > 0))
                                    pd.NetDepCap = m_NetDepCap;
                                pd.PeriodHours = UtilLibAlias.GetHrsInMonth(pd.Year, int.Parse(pd.Period), m_DaylightSavingTime);
                                pd.ServiceHours = DBUtilsAlias.Datasets.GetInt(Globals.dtGADSNG.Rows[i][Globals.CN_SERVICEHOURS]);
                                pd.AttemptedStarts = (short)DBUtilsAlias.Datasets.GetInt(Globals.dtGADSNG.Rows[i][Globals.CN_ATTEMPTEDSTARTS]);
                                pd.ActualStarts = (short)DBUtilsAlias.Datasets.GetInt(Globals.dtGADSNG.Rows[i][Globals.CN_ACTUALSTARTS]);
                                pd.VerbalDesc = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_VERBALDESC]);
                                pd.PriFuelCode = DBUtilsAlias.Datasets.GetString(Globals.dtGADSNG.Rows[i][Globals.CN_PRIFUELCODE]);


                                // now run some basic error checks against data
                                if (pd.UnitShortName.Trim() != m_UnitShortName.Trim())
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "Shortname doesn't match GADSNG UnitShortName");
                                if (pd.Year > DateTime.Now.Year)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "Year can not be greater than current year");
                                if ((int.Parse(pd.Period) < 1) || (int.Parse(pd.Period) > 12))
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "Period not a valid month");

                                // if all 3 of the Gross values are blank then set them equal to their Net values
                                if ((pd.GrossDepCap == 0) && (pd.GrossMaxCap == 0) && (pd.GrossGen == 0))
                                {
                                    pd.GrossDepCap = pd.NetDepCap;
                                    pd.GrossMaxCap = pd.NetMaxCap;
                                    pd.GrossGen = pd.NetGen;
                                }

                                if (pd.NetMaxCap > pd.GrossMaxCap)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "NetMaxCap > GrossMaxCap");
                                if (pd.NetDepCap > pd.GrossDepCap)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "NetDepCap > GrossDepCap");
                                if (pd.GrossDepCap > pd.GrossMaxCap)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "GrossDepCap > GrossMaxCap");
                                if (pd.NetDepCap > pd.NetMaxCap)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "NetDepCap > NetMaxCap");
                                if (pd.NetGen > pd.GrossGen)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "NetGen > GrossGen");
                                if (pd.GrossMaxCap < 0)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "GrossMaxCap < 0");
                                if (pd.GrossDepCap < 0)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "GrossDepCap < 0");
                                if (pd.GrossGen < 0)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "GrossGen < 0");
                                if (pd.NetMaxCap < 0)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "NetMaxCap < 0");
                                if (pd.NetDepCap < 0)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "NetDepCap < 0");
                                if (pd.ServiceHours > pd.PeriodHours)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "ServiceHours[" + pd.ServiceHours.ToString() + "] > Actual PeriodHours[" + pd.PeriodHours.ToString() + "] available for [" + pd.Period + "/" + pd.Year.ToString() + "]");
                                if ((pd.ActualStarts > 0) && (pd.ActualStarts > pd.AttemptedStarts))
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "ActualStarts > AttemptedStarts");
                                if (pd.PriFuelCode.Trim().Length == 0)
                                    Globals.AddErrorToTable(tabName, m_UtilityUnitCode, i + 1, "PriFuelCode is blank");

                                // IF no error messages then update datarow
                                // add default values to object
                                // and add pd object to the List<>
                                if (Globals.dtErrors.Rows.Count == 0)
                                {
                                    pd.TypUnitLoading = Globals.TYPUNITLOADING;
                                    pd.PriBtus = Globals.PRIBTUS;
                                    pd.QuaBtus = Globals.QUABTUS;
                                    pd.RevMonthCard1 = Globals.REVMONTHCARD1;
                                    pd.RevMonthCard2 = Globals.REVMONTHCARD2;
                                    pd.RevMonthCard3 = Globals.REVMONTHCARD3;
                                    pd.RevMonthCard4 = Globals.REVMONTHCARD4;
                                    pd.TimeStamp = DateTime.Now;    // Globals.TIMESTAMP;
                                    pd.InactiveHours = Globals.INACTIVEHOURS;
                                    pd.RSHours = Globals.RSHOURS;
                                    pd.PumpingHours = Globals.PUMPINGHOURS;
                                    pd.SynchCondHours = Globals.SYNCHCONDHOURS;
                                    pd.PlannedOutageHours = Globals.PLANNEDOUTAGEHOURS;
                                    pd.ForcedOutageHours = Globals.FORCEDOUTAGEHOURS;
                                    pd.MaintOutageHours = Globals.MAINTOUTAGEHOURS;
                                    pd.ExtofSchedOutages = Globals.EXTOFSCHEDOUTAGES;
                                    pd.RevisionCard1 = Globals.REVISIONCARD1;
                                    pd.RevisionCard2 = Globals.REVISIONCARD2;
                                    pd.RevisionCard3 = Globals.REVISIONCARD3;
                                    pd.RevisionCard4 = Globals.REVISIONCARD4;
                                    Globals.WorkListPerfs.Add(pd);
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show("Error: " + ex.Message);
                                break;
                            }
                        }
                    }
                    //}
                }
                Globals.StatusMessageText = "Ready";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to Read Excel file :[" + Globals.ExcelDataFile + "]: " + ex.Message);
                Globals.StatusMessageText = "Ready";
                return false;
            }
            finally
            {
                excelReaderData.CloseConnection();
#if DEBUG
                //Common.Utility.ObjectDumper.Write(Globals.WorkListPerfs);
#endif
                if (!bDataTabFound)
                {
                    MessageBox.Show("No tab names found that begin with the word [Perf], " +
                        "rename the data tab(s) so they start with key phrase and try again");
                }
            }
            return true;
        }
        /// <summary>
        /// build base of Insert SQL Statement since it never changes
        /// </summary>
        /// <returns></returns>
        public static string BuildInsertEventsBaseString()
        {
            StringBuilder sqlInsertBase = new StringBuilder();
            sqlInsertBase.Append("Insert GADSNG.EventData01 (");
            // default value fields
            sqlInsertBase.Append("[ContribCode],[PrimaryAlert],[ManhoursWorked],");
            sqlInsertBase.Append("[RevMonthCard01],[RevMonthCard02],[RevMonthCard03],[DominantDerate],[TimeStamp],");
            sqlInsertBase.Append("[RevisionCard01],[RevisionCard02],[RevisionCard03],[CarryOverLastYear],");
            sqlInsertBase.Append("[CarryOverNextYear],");
            // changing value fields
            sqlInsertBase.Append("[UtilityUnitCode],");
            sqlInsertBase.Append("[Year],");
            sqlInsertBase.Append("[EventNumber],");
            sqlInsertBase.Append("[UnitShortName],");
            sqlInsertBase.Append("[VerbalDesc1],");
            sqlInsertBase.Append("[VerbalDesc2],");
            sqlInsertBase.Append("[EventType],");
            sqlInsertBase.Append("[StartDateTime],");
            sqlInsertBase.Append("[EndDateTime],");
            sqlInsertBase.Append("[CauseCode],");
            sqlInsertBase.Append("[CauseCodeExt],");
            sqlInsertBase.Append("[VerbalDesc86],");
            sqlInsertBase.Append("[VerbalDescFull],");
            sqlInsertBase.Append("[NetAvailCapacity]");
            sqlInsertBase.Append(") VALUES (");
            //sqlInsertBase.Append("'" + Globals.CAUSECODEEXT + "',");
            sqlInsertBase.Append("'" + Globals.CONTRIBCODE + ",");
            sqlInsertBase.Append(((Globals.PRIMARYALERT) ? 1 : 0) + ",");
            sqlInsertBase.Append(Globals.MANHOURSWORKED + ",");
            sqlInsertBase.Append("'" + Globals.REVMONTHCARD01 + "',");
            sqlInsertBase.Append("'" + Globals.REVMONTHCARD02 + "',");
            sqlInsertBase.Append("'" + Globals.REVMONTHCARD03 + "',");
            sqlInsertBase.Append(((Globals.DOMINANTDERATE) ? 1 : 0) + ",");
            //sqlInsertBase.Append("'" + Globals.TIMESTAMP + "',");
            sqlInsertBase.Append("'" + DateTime.Now + "',");
            sqlInsertBase.Append("'" + Globals.REVISIONCARD01 + "',");
            sqlInsertBase.Append("'" + Globals.REVISIONCARD02 + "',");
            sqlInsertBase.Append("'" + Globals.REVISIONCARD03 + "',");
            sqlInsertBase.Append(((Globals.CARRYOVERLASTYEAR) ? 1 : 0) + ",");
            sqlInsertBase.Append(((Globals.CARRYOVERNEXTYEAR) ? 1 : 0) + ",");

            return sqlInsertBase.ToString();
        }
        public static string BuildInsertPerfString(DataRow dr)
        {
            StringBuilder sqlInsertPerf = new StringBuilder();
            sqlInsertPerf.Append("Insert GADSNG.PerformanceData (");
            sqlInsertPerf.Append("[UtilityUnitCode],[UnitShortName],[Year],[Period],[GrossMaxCap],[GrossDepCap],[GrossGen],");
            sqlInsertPerf.Append("[NetMaxCap],[NetDepCap],[NetGen],[AttemptedStarts],[ActualStarts],[TypUnitLoading],[VerbalDesc],");
            sqlInsertPerf.Append("[ServiceHours],[PeriodHours],[PriFuelCode],[PriBtus],[QuaBtus],");
            sqlInsertPerf.Append("[RevMonthCard1],[RevMonthCard2],[RevMonthCard3],[RevMonthCard4],[TimeStamp],[InactiveHours],[RSHours],");
            sqlInsertPerf.Append("[PumpingHours],[SynchCondHours],[PlannedOutageHours],[ForcedOutageHours],[MaintOutageHours],[ExtofSchedOutages],");
            sqlInsertPerf.Append("[RevisionCard1],[RevisionCard2],[RevisionCard3],[RevisionCard4]");
            sqlInsertPerf.Append(") VALUES (");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_UTILITYUNITCODE]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_UNITSHORTNAME]) + "',");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetShort(dr[Globals.CN_YEAR]) + ",");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_PERIOD]) + "',");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_GROSSMAXCAP]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_GROSSDEPCAP]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_GROSSGEN]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_NETMAXCAP]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_NETDEPCAP]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_NETGEN]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetInt(dr[Globals.CN_ATTEMPTEDSTARTS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetInt(dr[Globals.CN_ACTUALSTARTS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetInt(dr[Globals.CN_TYPUNITLOADING]) + ",");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_VERBALDESC]) + "',");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_SERVICEHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_PERIODHOURS]) + ",");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_PRIFUELCODE]) + "',");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_PRIBTUS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_QUABTUS]) + ",");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetDateTime(dr[Globals.CN_REVMONTHCARD1]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetDateTime(dr[Globals.CN_REVMONTHCARD2]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetDateTime(dr[Globals.CN_REVMONTHCARD3]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetDateTime(dr[Globals.CN_REVMONTHCARD4]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetDateTime(dr[Globals.CN_TIMESTAMP]) + "',");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetInt(dr[Globals.CN_INACTIVEHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_RSHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_PUMPINGHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_SYNCHCONDHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_PLANNEDOUTAGEHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_FORCEDOUTAGEHOURS]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_EXTOFSCHEDOUTAGES]) + ",");
            sqlInsertPerf.Append(DBUtilsAlias.Datasets.GetDecimal(dr[Globals.CN_MAINTOUTAGEHOURS]) + ",");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_REVISIONCARD1]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_REVISIONCARD2]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_REVISIONCARD3]) + "',");
            sqlInsertPerf.Append("'" + DBUtilsAlias.Datasets.GetString(dr[Globals.CN_REVISIONCARD4]) + "')");

            return sqlInsertPerf.ToString();
        }
        //public static void DeleteAllEventsForUtilityAndYear(string utilityCode, short year)
        //{
        //    StringBuilder sqlDelete = new StringBuilder(
        //        "Delete from GADSNG.EventData01 where UtilityUnitCode like '");
        //    sqlDelete.Append(utilityCode);
        //    sqlDelete.Append("%' AND Year=");
        //    sqlDelete.Append(year);
        //    DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlDelete.ToString());
        //}

        public static void DeleteAllEventsForUtilityUnitAndYear(string utilityunitcode, short year)
        {
            StringBuilder sqlDelete = new StringBuilder("Delete from GADSNG.EventData01 where UtilityUnitCode = '");
            sqlDelete.Append(utilityunitcode);
            sqlDelete.Append("' AND Year=");
            sqlDelete.Append(year);
            DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlDelete.ToString());
        }
        public static void DeleteAllPerfsForUtilityUnitAndYear(string utilityunitcode, short year)
        {
            StringBuilder sqlDelete = new StringBuilder("Delete from GADSNG.PerformanceData where UtilityUnitCode = '" + utilityunitcode);
            sqlDelete.Append("' AND Year=");
            sqlDelete.Append(year);
            DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlDelete.ToString());
        }
        //private bool EventsForUtilityYearExist(GadsngDataContext dc, string utilitycode, short year)
        //{
        //    int results = (from dt in dc.EventData01s
        //                   where dt.UtilityUnitCode.StartsWith(utilitycode) && dt.Year == year
        //                   select dt).Count();
        //    if (results > 0)
        //        return true;
        //    else
        //        return false;
        //}

        public static bool EventsForUtilityUnitYearExist(GadsngDataContext dc, string utilityunitcode, short year)
        {
            int results = (from dt in dc.EventData01s
                           where dt.UtilityUnitCode == utilityunitcode && dt.Year == year
                           select dt).Count();
            if (results > 0)
                return true;
            else
                return false;
        }
        public static bool PerfsForUtilityUnitYearExist(GadsngDataContext dc, string utilityunitcode, short year)
        {
            int results = (from dt in dc.PerformanceDatas
                           where dt.UtilityUnitCode == utilityunitcode && dt.Year == year
                           select dt).Count();
            if (results > 0)
                return true;
            else
                return false;
        }
        public static void SaveEventsToSQL()
        {
            string utilityCode = string.Empty;
            string msg = string.Empty;
            DialogResult result;
            Globals.ListUniqueUtilityUnitsByYear.Clear();
            Globals.StatusBarProgressBarValue = 0;

            using (GadsngDataContext dc = new GadsngDataContext(Globals.sqlConnGD.ConnectionString))
            {
#if DEBUG
                dc.Log = Console.Out;
                System.IO.FileInfo fi = new System.IO.FileInfo(Application.StartupPath + @"\" + "ProcessSteps.txt");
                if (fi.Exists)
                    fi.Delete();
                System.IO.StreamWriter sw = fi.CreateText();
#endif
                var uniqueutilityunitsbyyear = (from wl in Globals.WorkListEvents
                                                select new
                                                {
                                                    UtilityUnitCode = wl.UtilityUnitCode,
                                                    Year = wl.Year
                                                }).Distinct();
#if DEBUG
                sw.WriteLine("query UniqueUtilUnitsByYear");
                sw.WriteLine("build lstUniqueUtilityUnitsByYear");
#endif

                foreach (var item in uniqueutilityunitsbyyear)
                {
                    string _UnitShortName = GetShortName(item.UtilityUnitCode);
                    if (_UnitShortName == string.Empty)
                    {
                        Globals.AddErrorToTable(item.UtilityUnitCode, 0, "Unit not setup in GADS Setup Table, this must be done before loading records");
                    }
                    else
                    {
                        Globals.ListUniqueUtilityUnitsByYear.Add(new UniqueUtilityUnitsByYear
                        {
                            UtilityUnitCode = item.UtilityUnitCode,
                            UnitShortName = _UnitShortName,
                            Year = item.Year
                        });
                    }
#if DEBUG
                    sw.WriteLine("UtilityCode=" + item.UtilityUnitCode + " UnitShortName=" + GetShortName(item.UtilityUnitCode) + " Year=" + item.Year.ToString());
#endif
                }

                // now check each Unit and flag and/or delete existing records and insertion of new records
                foreach (UniqueUtilityUnitsByYear site in Globals.ListUniqueUtilityUnitsByYear)
                {
#if DEBUG
                    sw.WriteLine("Check to see if records exist for: UtilityUnitCode=" + site.UtilityUnitCode + " Year=" + site.Year.ToString());
#endif
                    if (EventsForUtilityUnitYearExist(dc, site.UtilityUnitCode, site.Year))
                    {
                        msg = "Records for [" + site.UtilityUnitCode + "] and [" + site.Year.ToString() + "] exist.\n" +
                            "Do you want to replace event records for Unit?";
                        result = MessageBox.Show(msg, "",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2);

                        if (result == DialogResult.Yes)
                        {
                            site.Insert = true;
                            DeleteAllEventsForUtilityUnitAndYear(site.UtilityUnitCode, site.Year);
                            // now insert new records where needed
                            site.ExcelEventCount = UploadUtilityUnitEvents(site.UtilityUnitCode, site.UnitShortName, site.Year);
                            site.UploadedEventCount = (from newEvnts in dc.EventData01s
                                                       where newEvnts.UtilityUnitCode == site.UtilityUnitCode && newEvnts.Year == site.Year
                                                       select newEvnts).Count();
                        }
                        else
                            site.Insert = false;
                    }
                    else
                    {
                        // now insert new records where needed
                        site.Insert = true;
                        site.ExcelEventCount = UploadUtilityUnitEvents(site.UtilityUnitCode, site.UnitShortName, site.Year);
                        site.UploadedEventCount = (from newEvnts in dc.EventData01s
                                                   where newEvnts.UtilityUnitCode == site.UtilityUnitCode && newEvnts.Year == site.Year
                                                   select newEvnts).Count();
                    }
                }
#if DEBUG
                sw.Close();
#endif
            }
        }
        public static bool SavePerfToSQL()
        {
            string utilityCode = string.Empty;
            string msg = string.Empty;
            DialogResult result;
            Globals.ListUniqueUtilityUnitsByYear.Clear();
            Globals.StatusBarProgressBarValue = 0;

            using (GadsngDataContext dc = new GadsngDataContext(Globals.sqlConnGD.ConnectionString))
            {
#if DEBUG
                dc.Log = Console.Out;
                System.IO.FileInfo fi = new System.IO.FileInfo(Application.StartupPath + @"\" + "ProcessSteps.txt");
                if (fi.Exists)
                    fi.Delete();
                System.IO.StreamWriter sw = fi.CreateText();
#endif

                var uniqueutilityunitsbyyear = (from dt in Globals.dtGADSNG.AsEnumerable()
                                                select new
                                                {
                                                    UUC = dt.Field<string>("UtilityUnitCode"),
                                                    Yr = dt.Field<string>("Year")
                                                }).Distinct();


#if DEBUG
                sw.WriteLine("query UniqueUtilUnitsByYear");
                sw.WriteLine("build lstUniqueUtilityUnitsByYear");
#endif

                foreach (var item in uniqueutilityunitsbyyear)
                {
                    Globals.ListUniqueUtilityUnitsByYear.Add(new UniqueUtilityUnitsByYear
                    {
                        UtilityUnitCode = item.UUC,
                        Year = short.Parse(item.Yr),
                        Insert = false
                    });
#if DEBUG
                    sw.WriteLine("UtilityUnitCode=" + item.UUC + " Year=" + item.Yr.ToString());
#endif
                }

                // now check each UtilityUnitCode & Year and flag and/or delete existing records and insertion of new records
                foreach (UniqueUtilityUnitsByYear site in Globals.ListUniqueUtilityUnitsByYear)
                {
#if DEBUG
                    sw.WriteLine("Check to see if records exist for: UtilityUnitCode=" + site.UtilityUnitCode + " Year=" + site.Year.ToString());
#endif
                    if (PerfsForUtilityUnitYearExist(dc, site.UtilityUnitCode, site.Year))
                    {
                        msg = "Records for [" + site.UtilityUnitCode + "] and [" +
                            site.Year.ToString() + "] exist.\n" +
                            "Do you want to replace event records for Unit?";
                        result = MessageBox.Show(msg, "",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2);

                        if (result == DialogResult.Yes)
                        {
                            site.Insert = true;
                            DeleteAllPerfsForUtilityUnitAndYear(site.UtilityUnitCode, site.Year);
                            // now insert new records where needed
                            UploadUtilityUnitPerfs(site.UtilityUnitCode, site.Year);
                        }
                        else
                            site.Insert = false;
                    }
                    else
                    {
                        site.Insert = true;
                        DeleteAllPerfsForUtilityUnitAndYear(site.UtilityUnitCode, site.Year);
                        // now insert new records where needed
                        UploadUtilityUnitPerfs(site.UtilityUnitCode, site.Year);
                    }
                }

#if DEBUG
                sw.Close();
#endif
            }
            return true;
        }
        public static int UploadUtilityUnitEvents(string utilityunitcode, string unitshortname, short year)
        {
            Globals.StatusBarProgressBarValue = 0;
#if DEBUG
            System.IO.FileInfo fi = new System.IO.FileInfo(Application.StartupPath + @"\" + "InsertStatements_" + utilityunitcode + year.ToString() + ".txt");
            if (fi.Exists)
                fi.Delete();
            System.IO.StreamWriter sw = fi.CreateText();
#endif
            string _EventType = string.Empty;
            string _VerbalDesc1 = string.Empty;
            string _VerbalDesc2 = string.Empty;
            string _VerbalDesc86 = string.Empty;
            string _VerbalDescFull = string.Empty;

            int counter = 0;
            int result = -1;
            var qUtilityUnitByYear = from wl in Globals.WorkListEvents
                                     where wl.UtilityUnitCode == utilityunitcode && wl.Year == year
                                     select wl;

            foreach (var item in qUtilityUnitByYear)
            {
                Globals.StatusBarProgressBarValue = counter;
                Globals.StatusBarProgressBarMax = qUtilityUnitByYear.Count();
                // check for any ' or -- in the incoming strings
                _EventType = item.EventType.Replace("--", "-");
                _EventType = _EventType.Replace("'", "");
                _EventType = _EventType.Replace(";", ":");

                if (item.VerbalDesc1.Length > 0)
                {
                    _VerbalDesc1 = item.VerbalDesc1.Replace("--", "-");
                    _VerbalDesc1 = _VerbalDesc1.Replace("'", "");
                    _VerbalDesc1 = _VerbalDesc1.Replace(";", ":");
                }

                if (item.VerbalDesc2.Length > 0)
                {
                    _VerbalDesc2 = item.VerbalDesc2.Replace("--", "-");
                    _VerbalDesc2 = _VerbalDesc2.Replace("'", "");
                    _VerbalDesc2 = _VerbalDesc2.Replace(";", ":");
                }

                if (item.VerbalDesc86.Length > 0)
                {

                    _VerbalDesc86 = item.VerbalDesc86.Replace("--", "-");
                    _VerbalDesc86 = _VerbalDesc86.Replace("'", "");
                    _VerbalDesc86 = _VerbalDesc86.Replace(";", ":");
                }

                if (item.VerbalDescFull.Length > 0)
                {

                    _VerbalDescFull = item.VerbalDescFull.Replace("--", "-");
                    _VerbalDescFull = _VerbalDescFull.Replace("'", "");
                    _VerbalDescFull = _VerbalDescFull.Replace(";", ":");
                }

                StringBuilder sqlInsert = new StringBuilder(BuildInsertEventsBaseString());
                sqlInsert.Append("'" + item.UtilityUnitCode + "',");
                sqlInsert.Append(item.Year + ",");
                sqlInsert.Append(item.EventNumber + ",");
                sqlInsert.Append("'" + unitshortname + "',");
                sqlInsert.Append("'" + _VerbalDesc1 + "',");
                sqlInsert.Append("'" + _VerbalDesc2 + "',");
                sqlInsert.Append("'" + _EventType + "',");
                sqlInsert.Append("'" + item.StartDateTime + "',");
                sqlInsert.Append("'" + item.EndDateTime + "',");
                sqlInsert.Append(item.CauseCode + ",");
                sqlInsert.Append("'" + item.CauseCodeExt + "',");
                sqlInsert.Append("'" + _VerbalDesc86 + "',");
                sqlInsert.Append("'" + _VerbalDescFull + "',");
                sqlInsert.Append(item.NetAvailCapacity + ")");

#if DEBUG
                sw.WriteLine(sqlInsert.ToString());
#endif

                try
                {
                    result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlInsert.ToString());
                    if (result > 0)
                    {
                        counter += result;
                    }
                    else
                    {
                        StringBuilder sb = new StringBuilder();
                        sb.Append("Problem saving record to database for UtilityUnitCode=[");
                        sb.Append(item.UtilityUnitCode);
                        sb.Append("] Year=[");
                        sb.Append(item.Year);
                        sb.Append("] EventNumber=[");
                        sb.Append(item.EventNumber);
                        sb.Append("]");
                        MessageBox.Show(sb.ToString());
                        Globals.AddErrorToTable(item.UtilityUnitCode, 0, sb.ToString());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error while uploading data: " + ex.Message);
                }
            }
#if DEBUG
            sw.Close();
#endif
            return counter;
        }
        public static void UploadUtilityUnitPerfs(string utilityunitcode, short year)
        {
            int counter = 0;
            Globals.StatusBarProgressBarValue = counter;
#if DEBUG
            System.IO.FileInfo fi = new System.IO.FileInfo(Application.StartupPath + @"\" + "InsertStatements_" + utilityunitcode + year.ToString() + ".txt");
            if (fi.Exists)
                fi.Delete();
            System.IO.StreamWriter sw = fi.CreateText();
#endif

            DataRow[] drs = Globals.dtGADSNG.Select("UtilityUnitCode='" + utilityunitcode + "' and year=" + year);
            Globals.StatusBarProgressBarMax = drs.Count();
            foreach (DataRow dr in drs)
            {
                // check for any ' or -- in the incoming strings
                string _VerbalDesc = DBUtilsAlias.Datasets.GetString(dr[Globals.CN_VERBALDESC]).Replace("--", "-");
                _VerbalDesc = _VerbalDesc.Replace("'", "");
                dr[Globals.CN_VERBALDESC] = _VerbalDesc.Replace(";", ":");
                Globals.StatusBarProgressBarValue = counter++;
                try
                {
                    DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, BuildInsertPerfString(dr));
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error while uploading data: " + ex.Message);
                }
            }
#if DEBUG
            sw.Close();
#endif
        }
        public static void GetSetupDefaults(DatabaseType _dbType, string utilityunitcode)
        {
            m_UnitShortName = null;
            DataTable resultTable = new DataTable("Setup");

            StringBuilder sqlCmd = new StringBuilder();
            sqlCmd.Append("SELECT UtilityUnitCode, UnitShortName, DaylightSavingTime, GrossMaxCapacity, ");
            sqlCmd.Append("GrossDepCapacity, NetMaxCapacity, NetDepCapacity, PriFuelCode, SecFuelCode, CheckStatus ");
            sqlCmd.Append(" FROM Setup WHERE UtilityUnitCode = '");
            sqlCmd.Append(utilityunitcode);
            sqlCmd.Append("'");

            switch (_dbType)
            {
                case DatabaseType.Access:
                    try
                    {
                        resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd.ToString(), "Setup");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem retrieving local Setup records\n" + ex.Message);
                        resultTable = new DataTable("Setup");
                    }

                    break;
                case DatabaseType.SQL:
                    sqlCmd.Replace("FROM Setup", "FROM GADSOS.GADSNG.Setup");
                    try
                    {
                        resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd.ToString(), "Setup");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem retrieving local Setup records\n" + ex.Message);
                        resultTable = new DataTable("Setup");
                    }
                    //using (GadsngDataContext dc = new GadsngDataContext(Globals.sqlConnGD.ConnectionString))
                    //{
                    //    var result = (from g in dc.Setups
                    //                  where g.UtilityUnitCode == utilityunitcode
                    //                  select new
                    //                  {
                    //                      g.UtilityUnitCode,
                    //                      g.UnitShortName,
                    //                      g.DaylightSavingTime,
                    //                      g.GrossMaxCapacity,
                    //                      g.GrossDepCapacity,
                    //                      g.NetMaxCapacity,
                    //                      g.NetDepCapacity,
                    //                      g.PriFuelCode,
                    //                      g.SecFuelCode,
                    //                      g.CheckStatus
                    //                  });
                    //}
                    break;
            }

            foreach (DataRow item in resultTable.Rows)
            {
                m_UtilityUnitCode = DBUtilsAlias.Datasets.GetString(item["UtilityUnitCode"]);
                m_UnitShortName = DBUtilsAlias.Datasets.GetString(item["UnitShortName"]);
                m_DaylightSavingTime = DBUtilsAlias.Datasets.GetInt(item["DaylightSavingTime"]);
                // for normal importing of Perf record from Excel do not get these defaults
                if (Globals.BuildDefPerfRecords)
                {
                    m_GrossMaxCap = DBUtilsAlias.Datasets.GetDecimal(item["GrossMaxCapacity"]);
                    m_GrossDepCap = DBUtilsAlias.Datasets.GetDecimal(item["GrossDepCapacity"]);
                    m_NetMaxCap = DBUtilsAlias.Datasets.GetDecimal(item["NetMaxCapacity"]);
                    m_NetDepCap = DBUtilsAlias.Datasets.GetDecimal(item["NetDepCapacity"]);
                }
                else
                {
                    m_GrossMaxCap = Globals.GROSSMAXCAP;
                    m_GrossDepCap = Globals.GROSSDEPCAP;
                    m_NetMaxCap = 0;
                    m_NetDepCap = 0;
                }
                m_PriFuelCode = DBUtilsAlias.Datasets.GetString(item["PriFuelCode"]);
                m_SecFuelCode = DBUtilsAlias.Datasets.GetString(item["SecFuelCode"]);
                m_AttemptedStarts = Globals.ATTEMPTEDSTARTS;
                m_ActualStarts = Globals.ACTUALSTARTS;
                m_TypUnitLoading = Globals.TYPUNITLOADING;
                m_VerbalDesc = string.Empty;
                m_PriQtyBurned = 0;
                m_PriAvgHeatContent = 0;
                m_TerFuelCode = string.Empty;
                m_TerQtyBurned = 0;
                m_TerAvgHeatContent = 0;
                m_QuaFuelCode = string.Empty;
                m_QuaQtyBurned = 0;
                m_QuaAvgHeatContent = 0;
                m_TimeStamp = DateTime.Parse("01/01/97");
                m_SecAvgHeatContent = 0;
                m_SecQtyBurned = 0;
            }
            if (null == m_UnitShortName)
            {
                MessageBox.Show("Unit [" + m_UtilityUnitCode + "] not in GADSNG SETUP Table or has no UnitShortName there");
                Application.Exit();
            }

        }
        public static void DisplayResults()
        {
            StringBuilder sbMsg = new StringBuilder();
            sbMsg.Append("UtilityUnit\tYear\tExcel\tUploaded\n");

            foreach (UniqueUtilityUnitsByYear item in Globals.ListUniqueUtilityUnitsByYear)
            {
                sbMsg.Append(item.UtilityUnitCode + "\t");
                sbMsg.Append(item.Year.ToString() + "\t");
                sbMsg.Append(item.ExcelEventCount.ToString() + "\t");
                sbMsg.Append(item.UploadedEventCount.ToString() + "\n");
            }
            MessageBox.Show(sbMsg.ToString(), "Review information",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public static bool SaveEventsToDataSet()
        {
            Globals.dsGADSNG = new GADSNG();
            int counter = 0;
            Globals.StatusBarProgressBarValue = counter;
            Globals.StatusBarProgressBarMax = Globals.WorkListEvents.Count;
			Globals.ListUniqueUtilityUnitsByYear.Clear(); // rmf change 201007211150

            var uniqueutilityunitsbyyear = (from wl in Globals.WorkListEvents
                                            select new
                                            {
                                                UtilityUnitCode = wl.UtilityUnitCode,
                                                Year = wl.Year
                                            }).Distinct();
            foreach (var item in uniqueutilityunitsbyyear)
            {
                string _UnitShortName = GetShortNameAccess(item.UtilityUnitCode);
                //string _UnitShortName = GetShortName(item.UtilityUnitCode);
                if (_UnitShortName == string.Empty)
                {
                    Globals.AddErrorToTable(item.UtilityUnitCode, 0, "Unit not setup in GADS Setup Table, this must be done before loading records");
                }
                else
                {
                    Globals.ListUniqueUtilityUnitsByYear.Add(new UniqueUtilityUnitsByYear
                    {
                        UtilityUnitCode = item.UtilityUnitCode,
                        UnitShortName = _UnitShortName,
                        Year = item.Year
                    });
                    if (Globals.BuildDefPerfRecords)
                    {
                        BuildDefaultPerfRecordsForUnit(item.UtilityUnitCode, item.Year);
                    }
                }
            }

            // check for existing records in the database
            foreach (UniqueUtilityUnitsByYear unit in Globals.ListUniqueUtilityUnitsByYear)
            {
                ClearAccessEventData01Records(unit.UtilityUnitCode, unit.Year);
            }

            foreach (WorkListEvents item in Globals.WorkListEvents)
            {
                Globals.StatusBarProgressBarValue = ++counter;
                string shortname = GetShortNameAccess(item.UtilityUnitCode);
                //string shortname = GetShortName(item.UtilityUnitCode);

                GADSNG.EventData01Row dr = Globals.dsGADSNG.EventData01.NewEventData01Row();
                // add default values to datarow
                //dr["CauseCodeExt"] = Globals.CAUSECODEEXT;
                dr["ContribCode"] = Globals.CONTRIBCODE;
                dr["PrimaryAlert"] = Globals.PRIMARYALERT;
                dr["ManhoursWorked"] = Globals.MANHOURSWORKED;
                dr["RevMonthCard01"] = Globals.REVMONTHCARD01;
                dr["RevMonthCard02"] = Globals.REVMONTHCARD02;
                dr["RevMonthCard03"] = Globals.REVMONTHCARD03;
                dr["DominantDerate"] = Globals.DOMINANTDERATE;
                //dr["TimeStamp"] = Globals.TIMESTAMP;
                dr["TimeStamp"] = DateTime.Now;
                dr["RevisionCard01"] = Globals.REVISIONCARD01;
                dr["RevisionCard02"] = Globals.REVISIONCARD02;
                dr["RevisionCard03"] = Globals.REVISIONCARD03;
                dr["CarryOverLastYear"] = Globals.CARRYOVERLASTYEAR;
                dr["CarryOverNextYear"] = Globals.CARRYOVERNEXTYEAR;
                // now add the values from Excel file
                dr["UtilityUnitCode"] = item.UtilityUnitCode;
                dr["Year"] = item.Year;
                dr["EventNumber"] = item.EventNumber;
                dr["UnitShortName"] = shortname;
                dr["VerbalDesc1"] = item.VerbalDesc1;
                dr["VerbalDesc2"] = item.VerbalDesc2;
                dr["EventType"] = item.EventType;
                dr["StartDateTime"] = item.StartDateTime;
                dr["EndDateTime"] = item.EndDateTime;
                dr["CauseCode"] = item.CauseCode;
                dr["CauseCodeExt"] = item.CauseCodeExt;
                dr["VerbalDesc86"] = item.VerbalDesc86;
                dr["VerbalDescFull"] = item.VerbalDescFull;
                dr["NetAvailCapacity"] = item.NetAvailCapacity;
                //dr["ChangeDateTime1"] = System.DBNull.Value;
                //dr["ChangeDateTime2"] = System.DBNull.Value;
                //dr["WorkStarted"] = System.DBNull.Value;
                //dr["WorkEnded"] = System.DBNull.Value;

                Globals.dsGADSNG.EventData01.AddEventData01Row(dr);
            }
            return true;
        }
        public static bool SavePerfToDataSet()
        {
            Globals.dsGADSNG = new GADSNG();
            int counter = 0;
            Globals.StatusBarProgressBarValue = counter;
            Globals.StatusBarProgressBarMax = Globals.WorkListPerfs.Count;
			Globals.ListUniqueUtilityUnitsByYear.Clear(); // rmf change 201007211150

            var uniqueutilityunitsbyyear = (from wl in Globals.WorkListPerfs
                                            select new
                                            {
                                                UtilityUnitCode = wl.UtilityUnitCode,
                                                Year = wl.Year
                                            }).Distinct();

            foreach (var item in uniqueutilityunitsbyyear)
            {
                string _UnitShortName = GetShortNameAccess(item.UtilityUnitCode);
                //string _UnitShortName = GetShortName(item.UtilityUnitCode);
                if (_UnitShortName == string.Empty)
                {
                    Globals.AddErrorToTable(item.UtilityUnitCode, 0, "Unit not setup in GADS Setup Table, this must be done before loading records");
                }
                else
                {
                    Globals.ListUniqueUtilityUnitsByYear.Add(new UniqueUtilityUnitsByYear
                    {
                        UtilityUnitCode = item.UtilityUnitCode,
                        UnitShortName = _UnitShortName,
                        Year = item.Year
                    });
                }
            }

            // check for existing records in the database
            foreach (UniqueUtilityUnitsByYear unit in Globals.ListUniqueUtilityUnitsByYear)
            {
                ClearAccessPerformanceDataRecords(unit.UtilityUnitCode, unit.Year);
            }

            foreach (PerformanceData item in Globals.WorkListPerfs)
            {
                Globals.StatusBarProgressBarValue = ++counter;
                string shortname = GetShortNameAccess(item.UtilityUnitCode);

                GADSNG.PerformanceDataRow dr = Globals.dsGADSNG.PerformanceData.NewPerformanceDataRow();
                dr["UtilityUnitCode"] = item.UtilityUnitCode;
                dr["UnitShortName"] = item.UnitShortName;
                dr["Year"] = item.Year;
                dr["Period"] = item.Period;
                dr["GrossMaxCap"] = (item.GrossMaxCap == null) ? 0 : item.GrossMaxCap;
                dr["GrossDepCap"] = (item.GrossDepCap == null) ? 0 : item.GrossDepCap;
                dr["GrossGen"] = (item.GrossGen == null) ? 0 : item.GrossGen;
                dr["NetMaxCap"] = (item.NetMaxCap == null) ? 0 : item.NetMaxCap;
                dr["NetDepCap"] = (item.NetDepCap == null) ? 0 : item.NetDepCap;
                dr["NetGen"] = (item.NetGen == null) ? 0 : item.NetGen;
                dr["AttemptedStarts"] = item.AttemptedStarts;
                dr["ActualStarts"] = item.ActualStarts;
                dr["TypUnitLoading"] = item.TypUnitLoading;
                dr["VerbalDesc"] = item.VerbalDesc;
                dr["ServiceHours"] = item.ServiceHours;
                dr["PeriodHours"] = item.PeriodHours;
                dr["PriFuelCode"] = item.PriFuelCode;
                dr["PriBtus"] = item.PriBtus;
                dr["QuaBtus"] = item.QuaBtus;
                dr["RevMonthCard1"] = item.RevMonthCard1;
                dr["RevMonthCard2"] = item.RevMonthCard2;
                dr["RevMonthCard3"] = item.RevMonthCard3;
                dr["RevMonthCard4"] = item.RevMonthCard4;
                dr["TimeStamp"] = item.TimeStamp;
                dr["InactiveHours"] = item.InactiveHours;
                dr["RSHours"] = item.RSHours;
                dr["PumpingHours"] = item.PumpingHours;
                dr["SynchCondHours"] = item.SynchCondHours;
                dr["PlannedOutageHours"] = item.PlannedOutageHours;
                dr["ForcedOutageHours"] = item.ForcedOutageHours;
                dr["MaintOutageHours"] = item.MaintOutageHours;
                dr["ExtofSchedOutages"] = item.ExtofSchedOutages;
                dr["RevisionCard1"] = item.RevisionCard1;
                dr["RevisionCard2"] = item.RevisionCard2;
                dr["RevisionCard3"] = item.RevisionCard3;
                dr["RevisionCard4"] = item.RevisionCard4;

                Globals.dsGADSNG.PerformanceData.AddPerformanceDataRow(dr);
            }

            return true;
        }
        private static void BuildDefaultPerfRecordsForUnit(string _UtilityUnitCode, short _Year)
        {
            ClearAccessPerformanceDataRecords(_UtilityUnitCode, _Year);

            GetSetupDefaults(DatabaseType.Access, _UtilityUnitCode);

            for (int i = 1; i < (13); i++)
            {
                decimal AvailHours = UtilLibAlias.GetHrsInMonth(_Year, i, m_DaylightSavingTime);

                GADSNG.PerformanceDataRow dr = Globals.dsGADSNG.PerformanceData.NewPerformanceDataRow();
                dr["UtilityUnitCode"] = _UtilityUnitCode;
                dr["UnitShortName"] = m_UnitShortName;
                dr["Year"] = _Year;
                dr["Period"] = i.ToString("00");
                dr["GrossMaxCap"] = m_GrossMaxCap;
                dr["GrossDepCap"] = m_GrossDepCap;
                dr["GrossGen"] = Globals.GROSSGEN;
                dr["NetMaxCap"] = m_NetMaxCap;
                dr["NetDepCap"] = m_NetDepCap;
                dr["NetGen"] = 0;
                dr["AttemptedStarts"] = m_AttemptedStarts;
                dr["ActualStarts"] = m_ActualStarts;
                dr["TypUnitLoading"] = m_TypUnitLoading;
                dr["VerbalDesc"] = m_VerbalDesc;
                dr["ServiceHours"] = AvailHours;
                dr["PeriodHours"] = AvailHours;
                dr["PriFuelCode"] = m_PriFuelCode;
                dr["PriBtus"] = Globals.PRIBTUS;
                dr["QuaBtus"] = Globals.QUABTUS;
                dr["RevMonthCard1"] = Globals.REVMONTHCARD1;
                dr["RevMonthCard2"] = Globals.REVMONTHCARD2;
                dr["RevMonthCard3"] = Globals.REVMONTHCARD3;
                dr["RevMonthCard4"] = Globals.REVMONTHCARD4;
                dr["TimeStamp"] = DateTime.Now;
                dr["InactiveHours"] = Globals.INACTIVEHOURS;
                dr["RSHours"] = Globals.RSHOURS;
                dr["PumpingHours"] = Globals.PUMPINGHOURS;
                dr["SynchCondHours"] = Globals.SYNCHCONDHOURS;
                dr["PlannedOutageHours"] = Globals.PLANNEDOUTAGEHOURS;
                dr["ForcedOutageHours"] = Globals.FORCEDOUTAGEHOURS;
                dr["MaintOutageHours"] = Globals.MAINTOUTAGEHOURS;
                dr["ExtofSchedOutages"] = Globals.EXTOFSCHEDOUTAGES;
                dr["RevisionCard1"] = Globals.REVISIONCARD1;
                dr["RevisionCard2"] = Globals.REVISIONCARD2;
                dr["RevisionCard3"] = Globals.REVISIONCARD3;
                dr["RevisionCard4"] = Globals.REVISIONCARD4;

                Globals.dsGADSNG.PerformanceData.AddPerformanceDataRow(dr);
            }
        }
        public static DataTable GetSQLCompanyList(string _companyID)
        {
            DataTable dt;
            string sqlCmd = SQLCOMPANIES.Replace("XXCOMPANYIDXX", _companyID);
            try
            {
                dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "Companies");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
                dt = new DataTable();
            }
            return dt;
        }

        public static GADSNG GetSQLGADSData(string _companyID)
        {
            GADSNG ds = new GADSNG();
            ds = GetSQLGADSData(_companyID, true);
            return ds;
        }
        public static GADSNG GetSQLGADSData(string _companyID, bool bAllData)
        {
            GADSNG ds = new GADSNG();
            DataSet dsSummary = new DataSet();
            DataTable dt;
            string sqlCmd = string.Empty;

            // pull down the Setup records for a company
            sqlCmd = SQLSETUP.Replace("XXCOMPANYIDXX", _companyID);
            try
            {
                dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "Setup");
                if (!bAllData)
                    dsSummary.Tables.Add(dt.Copy());
                ds.Tables["Setup"].Load(dt.CreateDataReader());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving Setup data from SQL database\n" + ex.Message);
            }
            if (!bAllData)
            {
                // grab only the summary data with the latest TimeStamp for Events & Perf data
                {
                    // pull down the EventData01 records for a company's latest timestamp by UtilityUnitCode
                    sqlCmd = SQLEVENTDATA01.Replace("SELECT * ", "SELECT UtilityUnitCode, [YEAR], max(TimeStamp) as 'TimeStamp' ");
                    sqlCmd = sqlCmd.Replace("XXCOMPANYIDXX", _companyID);
                    sqlCmd = sqlCmd.Replace("ORDER BY UtilityUnitCode", " GROUP BY UtilityUnitCode, [YEAR] ORDER BY UtilityUnitCode, [YEAR]");
                    try
                    {
                        dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "EventData01");
                        dsSummary.Tables.Add(dt.Copy());
                        //dsSummary.Tables["EventData01"].Load(dt.CreateDataReader());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
                    }

                    // pull down the EventData02 records for a company's latest timestamp by UtilityUnitCode
                    sqlCmd = SQLEVENTDATA02.Replace("SELECT * ", "SELECT UtilityUnitCode, [YEAR], max(TimeStamp) as 'TimeStamp' ");
                    sqlCmd = sqlCmd.Replace("XXCOMPANYIDXX", _companyID);
                    sqlCmd = sqlCmd.Replace("ORDER BY UtilityUnitCode", " GROUP BY UtilityUnitCode, [YEAR] ORDER BY UtilityUnitCode, [YEAR]");
                    try
                    {
                        dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "EventData02");
                        dsSummary.Tables.Add(dt.Copy());
                        //ds.Tables["EventData02"].Load(dt.CreateDataReader());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
                    }

                    // pull down the PerformanceData records for a company's latest timestamp by UtilityUnitCode
                    sqlCmd = SQLPERFORMANCEDATA.Replace("SELECT * ", "SELECT UtilityUnitCode, [YEAR], max(TimeStamp) as 'TimeStamp' ");
                    sqlCmd = sqlCmd.Replace("XXCOMPANYIDXX", _companyID);
                    sqlCmd = sqlCmd.Replace("ORDER BY UtilityUnitCode", " GROUP BY UtilityUnitCode, [YEAR] ORDER BY UtilityUnitCode, [YEAR]");
                    try
                    {
                        dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "PerformanceData");
                        dsSummary.Tables.Add(dt.Copy());
                        //ds.Tables["PerformanceData"].Load(dt.CreateDataReader());
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
                    }

                    return ds;
                }
            }

            // pull down the EventData01 records for a company
            sqlCmd = SQLEVENTDATA01.Replace("XXCOMPANYIDXX", _companyID);
            try
            {
                dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "EventData01");
                ds.Tables["EventData01"].Load(dt.CreateDataReader());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
            }

            // pull down the EventData02 records for a company
            sqlCmd = SQLEVENTDATA02.Replace("XXCOMPANYIDXX", _companyID);
            try
            {
                dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "EventData02");
                ds.Tables["EventData02"].Load(dt.CreateDataReader());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
            }

            // pull down the PerformanceData records for a company
            sqlCmd = SQLPERFORMANCEDATA.Replace("XXCOMPANYIDXX", _companyID);
            try
            {
                dt = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.sqlConnGD, sqlCmd, "PerformanceData");
                ds.Tables["PerformanceData"].Load(dt.CreateDataReader());
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving data from SQL database\n" + ex.Message);
            }

            return ds;
        }

        public static void ClearAccessSetupRecords(string _companyID)
        {
            int result;
            string sqlCmd = "DELETE FROM Setup";
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS Setup records from database.\n" + ex.Message);
            }
        }

        public static void ClearAccessEventData01Records(string _companyID)
        {
            int result;
            string sqlCmd = "DELETE FROM EventData01";
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS EventData01 records from database.\n" + ex.Message);
            }
        }
        public static void ClearAccessEventData01Records(string _uuc, int _year)
        {
            int result;
            string sqlCmd = "DELETE FROM EventData01 WHERE UtilityUnitCode='" + _uuc + "' and Year=" + _year;
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS EventData01 records from database.\n" + ex.Message);
            }
        }

        public static void ClearAccessEventData02Records(string _companyID)
        {
            int result;
            string sqlCmd = "DELETE FROM EventData02";
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS EventData02 records from database.\n" + ex.Message);
            }
        }

        public static void ClearAccessPerformanceDataRecords(string _companyID)
        {
            int result;
            string sqlCmd = "DELETE FROM PerformanceData";
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS PerformanceData records from database.\n" + ex.Message);
            }
        }
        public static void ClearAccessPerformanceDataRecords(string _uuc, int _year)
        {
            int result;
            string sqlCmd = "DELETE FROM PerformanceData WHERE UtilityUnitCode='" + _uuc + "' and Year=" + _year;
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS PerformanceData records from database.\n" + ex.Message);
            }
        }

        public static void ClearSQLSetupRecords(string _companyID)
        {
            int result;
            string sqlCmd = SQLSETUP.Replace("SELECT *", "DELETE");
            sqlCmd = sqlCmd.Replace("XXCOMPANYIDXX", _companyID);
            sqlCmd = sqlCmd.Replace(" ORDER BY UtilityUnitCode", "");
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS Setup records from database.\n" + ex.Message);
            }
        }

        public static void ClearSQLEventData01Records(string _uuc, int _year)
        {
            int result;
            string sqlCmd = "DELETE FROM GADSOS.GADSNG.EventData01 WHERE UtilityUnitCode='" + _uuc + "' and Year=" + _year;
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS EventData01 records from database.\n" + ex.Message);
            }
        }
        public static void ClearSQLEventData02Records(string _uuc, int _year)
        {
            int result;
            string sqlCmd = "DELETE FROM GADSOS.GADSNG.EventData02 WHERE UtilityUnitCode='" + _uuc + "' and Year=" + _year;
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS EventData02 records from database.\n" + ex.Message);
            }
        }

        public static void ClearSQLPerformanceDataRecords(string _uuc, int _year)
        {
            int result;
            string sqlCmd = "DELETE FROM GADSOS.GADSNG.PerformanceData WHERE UtilityUnitCode='" + _uuc + "' and Year=" + _year;
            try
            {
                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sqlCmd);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem deleting GADS PerformanceData records from database.\n" + ex.Message);
            }
        }

        public static DataTable GetSetupValidatedRecordsFromAccess()
        {
            DataTable resultTable;
            string sqlCmd = "SELECT * FROM Setup ORDER BY UtilityUnitCode";
            //string sqlCmd = "SELECT * FROM Setup WHERE CheckStatus='O' ORDER BY UtilityUnitCode";

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "Setup");
                foreach (DataRow dr in resultTable.Rows)
                {
                    if ("O" != DBUtilsAlias.Datasets.GetString(dr["CheckStatus"]))
                    {
                        dr.RowError = "Error: Unit has not been validated locally";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local Setup records\n" + ex.Message);
                resultTable = new DataTable("Setup");
            }
            return resultTable;
        }

        public static void GetEventData01UpdatedRecordsFromAccess(string _uuc)
        {
            DateTime udtAccess;
            DateTime udtSQL;
            DataRow[] foundRows;
            int uucYr;
            DataTable resultTable;
            string sqlCmd = "SELECT UtilityUnitCode, [YEAR], max(TimeStamp) as [MaxTimeStamp] FROM EventData01 WHERE UtilityUnitCode='" + _uuc +
                "' GROUP BY UtilityUnitCode, [YEAR] ORDER BY UtilityUnitCode, [YEAR]";

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "EventData01");
                foreach (DataRow dr in resultTable.Rows)
                {
                    udtAccess = DBUtilsAlias.Datasets.GetDateTime(dr["MaxTimeStamp"]);
                    uucYr = DBUtilsAlias.Datasets.GetInt(dr["Year"]);

                    foundRows = Globals.dsGADSNG.Tables["EventData01"].Select("UtilityUnitCode='" + _uuc + "' AND Year=" + uucYr);
                    if (foundRows.Count() == 0)
                    {
                        GetEventData01RecordsFromAccess(_uuc, uucYr);
                        ClearSQLEventData01Records(_uuc, uucYr);
                    }
                    else
                    {
                        udtSQL = DBUtilsAlias.Datasets.GetDateTime(foundRows[0]["TimeStamp"]);
                        if (DateTime.Compare(udtAccess, udtSQL) > 0)
                        {
                            GetEventData01RecordsFromAccess(_uuc, uucYr);
                            ClearSQLEventData01Records(_uuc, uucYr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local EventData01 records\n" + ex.Message);
                resultTable = new DataTable("EventData01");
            }
        }
        public static void GetEventData01RecordsFromAccess(string _uuc, int _yr)
        {
            DataTable resultTable;
            string sqlCmd = "SELECT * FROM EventData01 WHERE UtilityUnitCode='" + _uuc + "' AND [Year]=" + _yr;

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "EventData01");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local EventData01 records\n" + ex.Message);
                resultTable = new DataTable("EventData01");
            }
            dsNewGADSNG.Tables["EventData01"].Load(resultTable.CreateDataReader());
        }

        public static void GetEventData02UpdatedRecordsFromAccess(string _uuc)
        {
            DateTime udtAccess;
            DateTime udtSQL;
            DataRow[] foundRows;
            int uucYr;
            DataTable resultTable;
            string sqlCmd = "SELECT UtilityUnitCode, [YEAR], max(TimeStamp) as [TimeStamp] FROM EventData02 WHERE UtilityUnitCode='" + _uuc +
                "' GROUP BY UtilityUnitCode, [YEAR] ORDER BY UtilityUnitCode, [YEAR]";

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "EventData02");
                foreach (DataRow dr in resultTable.Rows)
                {
                    udtAccess = DBUtilsAlias.Datasets.GetDateTime(dr["TimeStamp"]);
                    uucYr = DBUtilsAlias.Datasets.GetInt(dr["Year"]);

                    foundRows = Globals.dsGADSNG.Tables["EventData02"].Select("UtilityUnitCode='" + _uuc + "' AND Year=" + uucYr);
                    if (foundRows.Count() == 0)
                    {
                        GetEventData02RecordsFromAccess(_uuc, uucYr);
                        ClearSQLEventData02Records(_uuc, uucYr);
                    }
                    else
                    {
                        udtSQL = DBUtilsAlias.Datasets.GetDateTime(foundRows[0]["TimeStamp"]);
                        if (DateTime.Compare(udtAccess, udtSQL) > 0)
                        {
                            GetEventData02RecordsFromAccess(_uuc, uucYr);
                            ClearSQLEventData02Records(_uuc, uucYr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local EventData02 records\n" + ex.Message);
                resultTable = new DataTable("EventData02");
            }
        }
        public static void GetEventData02RecordsFromAccess(string _uuc, int _yr)
        {
            DataTable resultTable;
            string sqlCmd = "SELECT * FROM EventData02 WHERE UtilityUnitCode='" + _uuc + "' AND [Year]=" + _yr;

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "EventData02");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local EventData02 records\n" + ex.Message);
                resultTable = new DataTable("EventData02");
            }
            dsNewGADSNG.Tables["EventData02"].Load(resultTable.CreateDataReader());
        }

        public static void GetPerformanceDataUpdatedRecordsFromAccess(string _uuc)
        {
            DateTime udtAccess;
            DateTime udtSQL;
            DataRow[] foundRows;
            int uucYr;
            DataTable resultTable;
            string sqlCmd = "SELECT UtilityUnitCode, [YEAR], max(TimeStamp) as [TimeStamp] FROM PerformanceData WHERE UtilityUnitCode='" + _uuc +
                "' GROUP BY UtilityUnitCode, [YEAR] ORDER BY UtilityUnitCode, [YEAR]";

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "PerformanceData");
                foreach (DataRow dr in resultTable.Rows)
                {
                    udtAccess = DBUtilsAlias.Datasets.GetDateTime(dr["TimeStamp"]);
                    uucYr = DBUtilsAlias.Datasets.GetInt(dr["Year"]);

                    foundRows = Globals.dsGADSNG.Tables["PerformanceData"].Select("UtilityUnitCode='" + _uuc + "' AND Year=" + uucYr);
                    if (foundRows.Count() == 0)
                    {
                        GetPerformanceDataRecordsFromAccess(_uuc, uucYr);
                        ClearSQLPerformanceDataRecords(_uuc, uucYr);
                    }
                    else
                    {
                        udtSQL = DBUtilsAlias.Datasets.GetDateTime(foundRows[0]["TimeStamp"]);
                        if (DateTime.Compare(udtAccess, udtSQL) > 0)
                        {
                            GetPerformanceDataRecordsFromAccess(_uuc, uucYr);
                            ClearSQLPerformanceDataRecords(_uuc, uucYr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local PerformanceData records\n" + ex.Message);
                resultTable = new DataTable("PerformanceData");
            }
        }
        public static void GetPerformanceDataRecordsFromAccess(string _uuc, int _yr)
        {
            DataTable resultTable;
            string sqlCmd = "SELECT * FROM PerformanceData WHERE UtilityUnitCode='" + _uuc + "' AND [Year]=" + _yr;

            try
            {
                resultTable = DBUtilsAlias.DatabaseAccess.GetDataTable(Globals.accessConnGD, sqlCmd, "PerformanceData");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problem retrieving local PerformanceData records\n" + ex.Message);
                resultTable = new DataTable("EventData02");
            }
            dsNewGADSNG.Tables["PerformanceData"].Load(resultTable.CreateDataReader());
        }

        public static void TransferSetupRecordsFromDatatableDataSet(DataTable _dtAccess)
        {
            Globals.dsGADSNG.Tables["Setup"].Rows.Clear();
            Globals.dsGADSNG.Tables["Setup"].Load(_dtAccess.CreateDataReader());
        }

        public static void UploadSetupRecordsToAccess()
        {
            UploadSetupRecords(DatabaseType.Access);
        }
        public static void UploadSetupRecordsToSQL()
        {
            UploadSetupRecords(DatabaseType.SQL);
        }
        public static void UploadEventData01RecordsToAccess()
        {
            UploadEventData01Records(DatabaseType.Access);
        }
        public static void UploadEventData01RecordsToSQL(string _uuc)
        {
            UploadEventData01Records(DatabaseType.SQL);
        }
        public static void UploadEventData02RecordsToAccess()
        {
            UploadEventData02Records(DatabaseType.Access);
        }
        public static void UploadEventData02RecordsToSQL()
        {
            UploadEventData02Records(DatabaseType.SQL);
        }

        public static void UploadPerformanceDataRecordsToAccess()
        {
            UploadPerformanceDataRecords(DatabaseType.Access);
        }
        public static void UploadPerformanceDataRecordsToSQL()
        {
            UploadPerformanceDataRecords(DatabaseType.SQL);
        }

        public static string CompareSetupRecords(DataTable dtAccess)
        {
            string _desc = string.Empty;
            int rowsAccess = dtAccess.Rows.Count;
            int rowsSQL = Globals.dsGADSNG.Setup.Rows.Count;
            int rowsDelta = rowsAccess - rowsSQL;
            // check basic is the record count the same
            if (rowsDelta < 0)
            {
                Globals.SetupCompareStatus = Globals.SetupRecordCheckReturn.LocalSmaller;
                _desc = "Local GADS has [" + rowsDelta.ToString() + "] fewer record(s) than the SQL Database.";
            }
            else if (rowsDelta > 0)
            {
                Globals.SetupCompareStatus = Globals.SetupRecordCheckReturn.LocalGreater;
                _desc = "Local GADS has [" + rowsDelta.ToString() + "] new record(s) that are not in the SQL Database.";
                // check to see if the new rows are already in the SQL database as 
                // either UtilityUnitCode OR UnitShortName
                // if they are then STOP and do not import


            }
            else
            {
                if (dtAccess.HasErrors)
                {
                    Globals.SetupCompareStatus = Globals.SetupRecordCheckReturn.NotAllValidated;
                    _desc = "Not all local Units have passed GADS Validation";
                }
                else
                {
                    foreach (DataRow dr in Globals.dsGADSNG.Setup.Rows)
                    {
                        DataRow[] found = dtAccess.Select("UtilityUnitCode='" + DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]) + "'");
                        if (found.Count() != 1)
                        {
                            dr.RowError = "Unit not found in the local database";
                        }
                    }
                    if (Globals.dsGADSNG.Setup.HasErrors)
                    {
                        Globals.SetupCompareStatus = Globals.SetupRecordCheckReturn.AllLocalRecordsNotInSQL;
                        _desc = "Not all Units in local database are in SQL Server";
                    }
                    else
                    {
                        Globals.SetupCompareStatus = Globals.SetupRecordCheckReturn.Match;
                        _desc = "rows equal";
                    }
                    // now check UtilityUnitCode & UnitShortName fields to see if all of the units from SQL are still 
                    // in the list from the local database



                    // if they are the same for the 2 key fields then just replace

                }
            }

            return _desc;
        }
        #endregion Public Methods

        #region Private Methods
        private static void Initialize_SETUP_INSERT_BASE(DatabaseType _dbType)
        {
            string dbName = string.Empty;
            switch (_dbType)
            {
                case DatabaseType.Access:
                    dbName = "INSERT INTO [Setup]";
                    break;
                case DatabaseType.SQL:
                    dbName = "INSERT INTO GADSOS.GADSNG.Setup";
                    break;
            }

            Globals.SETUP_INSERT_BASE = new StringBuilder();
            Globals.SETUP_INSERT_BASE.Append(dbName + " ([UtilityUnitCode],[UnitName],[UnitShortName],[GrossMaxCapacity],[NetMaxCapacity]");
            Globals.SETUP_INSERT_BASE.Append(",[PriFuelCode],[PriHeatContent],[SecFuelCode],[SecHeatContent],[UnitType],[GrossDepCapacity],[NetDepCapacity]");
            Globals.SETUP_INSERT_BASE.Append(",[TypUnitLoading],[VerbalDesc],[DaylightSavingTime],[JointOwnership],[ExpandedReporting],[InputDataRequirements]");
            Globals.SETUP_INSERT_BASE.Append(",[FuelQtyFormat],[BtuOrHeatContent],[FuelQualityShow],[Perf02DecimalPlaces],[BulkRS],[EnglishOrMetric]");
            Globals.SETUP_INSERT_BASE.Append(",[PeakPeriodSet],[ServiceHourMethod],[CommercialDate],[RetirementDate],[GMCWeighting],[NMCWeighting]");
            Globals.SETUP_INSERT_BASE.Append(",[PerformanceDate],[PerformanceStart],[Example3D],[CheckStatus],[NERC],[NYISO],[PJM],[ISONE],[UnitSelected]");
            Globals.SETUP_INSERT_BASE.Append(",[MaxCapReadOnly],[WorkDetails],[CopyToVerbDesc],[CauseCodeExtDisplay],[CauseCodeExtEditable]");
            Globals.SETUP_INSERT_BASE.Append(",[DepCapReadOnly],[PumpingData],[SynchCondData],[GrossNetBoth],[CombinedCycleName],[CombinedCycleShortName]");
            Globals.SETUP_INSERT_BASE.Append(",[PJMStartsCount],[Hourly],[Daily],[WeeklyMS],[WeeklySS],[FourWeeks],[Monthly],[Quarterly],[Yearly]");
            Globals.SETUP_INSERT_BASE.Append(",[FixedN],[FixedNValue],[FixedByUser],[FixedByUserFile],[PeakPeriod],[GenElect],[Siemens],[CAISO],[MISO]");
            Globals.SETUP_INSERT_BASE.Append(",[ISO1],[ISO2],[ISO3],[CEA],[CHP]) VALUES (");
        }
        private static void Initialize_EVENTDATA01_INSERT_BASE(DatabaseType _dbType)
        {
            string dbName = string.Empty;
            switch (_dbType)
            {
                case DatabaseType.Access:
                    dbName = "INSERT INTO [EventData01]";
                    break;
                case DatabaseType.SQL:
                    dbName = "INSERT INTO GADSOS.GADSNG.EventData01";
                    break;
            }

            // 02/27/09 DB removed 4 datetime fields that should never but uploaded
            Globals.EVENTDATA01_INSERT_BASE = new StringBuilder();
            Globals.EVENTDATA01_INSERT_BASE.Append(dbName + " ([UtilityUnitCode],[UnitShortName],[Year],[EventNumber],[RevisionCard01],[EventType]");
            Globals.EVENTDATA01_INSERT_BASE.Append(",[StartDateTime],[CarryOverLastYear],[ChangeInEventType1],[ChangeInEventType2]");
            //Globals.EVENTDATA01_INSERT_BASE.Append(",[StartDateTime],[CarryOverLastYear],[ChangeDateTime1],[ChangeInEventType1],[ChangeDateTime2],[ChangeInEventType2]");
            Globals.EVENTDATA01_INSERT_BASE.Append(",[EndDateTime],[CarryOverNextYear],[GrossAvailCapacity],[NetAvailCapacity],[CauseCode],[CauseCodeExt]");
            //Globals.EVENTDATA01_INSERT_BASE.Append(",[EndDateTime],[CarryOverNextYear],[GrossAvailCapacity],[NetAvailCapacity],[CauseCode],[CauseCodeExt],[WorkStarted]");
            Globals.EVENTDATA01_INSERT_BASE.Append(",[ContribCode],[PrimaryAlert],[ManhoursWorked],[VerbalDesc1],[RevisionCard02],[VerbalDesc2]");
            //Globals.EVENTDATA01_INSERT_BASE.Append(",[WorkEnded],[ContribCode],[PrimaryAlert],[ManhoursWorked],[VerbalDesc1],[RevisionCard02],[VerbalDesc2]");
            Globals.EVENTDATA01_INSERT_BASE.Append(",[RevisionCard03],[RevMonthCard01],[RevMonthCard02],[RevMonthCard03],[EditFlag],[VerbalDesc86],[VerbalDescFull]");
            Globals.EVENTDATA01_INSERT_BASE.Append(",[FailureMechCode],[TripMech],[CumFiredHours],[CumEngineStarts],[DominantDerate],[TimeStamp],[PJMIOCode]");
            Globals.EVENTDATA01_INSERT_BASE.Append(",[PJMLoadStatus],[PJMStatusDate]) VALUES (");
        }
        private static void Initialize_EVENTDATA02_INSERT_BASE(DatabaseType _dbType)
        {
            string dbName = string.Empty;
            switch (_dbType)
            {
                case DatabaseType.Access:
                    dbName = "INSERT INTO [EventData02]";
                    break;
                case DatabaseType.SQL:
                    dbName = "INSERT INTO GADSOS.GADSNG.EventData02";
                    break;
            }
            Globals.EVENTDATA02_INSERT_BASE = new StringBuilder();
            Globals.EVENTDATA02_INSERT_BASE.Append(dbName + " ([UtilityUnitCode],[UnitShortName],[Year],[EventNumber],[RevisionCardEven]");
            Globals.EVENTDATA02_INSERT_BASE.Append(",[EventType],[CauseCode],[CauseCodeExt],[WorkStarted],[WorkEnded],[ContribCode],[PrimaryAlert]");
            Globals.EVENTDATA02_INSERT_BASE.Append(",[ManhoursWorked],[VerbalDesc1],[EvenCardNumber],[VerbalDesc2],[RevisionCardOdd],[RevMonthCardEven]");
            Globals.EVENTDATA02_INSERT_BASE.Append(",[RevMonthCardOdd],[EditFlag],[VerbalDesc86],[VerbalDescFull],[FailureMechCode],[TripMech]");
            Globals.EVENTDATA02_INSERT_BASE.Append(",[CumFiredHours],[CumEngineStarts],[TimeStamp],[PJMLoadStatus],[PJMStatusDate]) VALUES (");

        }
        private static void Initialize_PERFORMANCEDATA_INSERT_BASE(DatabaseType _dbType)
        {
            string dbName = string.Empty;
            switch (_dbType)
            {
                case DatabaseType.Access:
                    dbName = "INSERT INTO [PerformanceData]";
                    break;
                case DatabaseType.SQL:
                    dbName = "INSERT INTO GADSOS.GADSNG.PerformanceData";
                    break;
            }

            Globals.PERFORMANCEDATA_INSERT_BASE = new StringBuilder();
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(dbName + " ([UtilityUnitCode],[UnitShortName],[Year],[Period],[RevisionCard1]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[GrossMaxCap],[GrossDepCap],[GrossGen],[NetMaxCap],[NetDepCap],[NetGen],[TypUnitLoading]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[AttemptedStarts],[ActualStarts],[VerbalDesc],[RevisionCard2],[ServiceHours],[RSHours]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[PumpingHours],[SynchCondHours],[PlannedOutageHours],[ForcedOutageHours],[MaintOutageHours]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[ExtofSchedOutages],[PeriodHours],[RevisionCard3],[PriFuelCode],[PriQtyBurned]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[PriAvgHeatContent],[PriBtus],[PriPercentAsh],[PriPercentMoisture],[PriPercentSulfur]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[PriPercentAlkalines],[PriGrindIndexVanad],[PriAshSoftTemp],[SecFuelCode],[SecQtyBurned]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[SecAvgHeatContent],[SecBtus],[SecPercentAsh],[SecPercentMoisture],[SecPercentSulfur]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[SecPercentAlkalines],[SecGrindIndexVanad],[SecAshSoftTemp],[RevisionCard4],[TerFuelCode]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[TerQtyBurned],[TerAvgHeatContent],[TerBtus],[TerPercentAsh],[TerPercentMoisture]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[TerPercentSulfur],[TerPercentAlkalines],[TerGrindIndexVanad],[TerAshSoftTemp],[QuaFuelCode]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[QuaQtyBurned],[QuaAvgHeatContent],[QuaBtus],[QuaPercentAsh],[QuaPercentMoisture]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[QuaPercentSulfur],[QuaPercentAlkalines],[QuaGrindIndexVanad],[QuaAshSoftTemp]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[RevMonthCard1],[RevMonthCard2],[RevMonthCard3],[RevMonthCard4],[JOGrossMaxCap]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[JOGrossGen],[JONetMaxCap],[JONetGen],[JOPriQtyBurned],[JOSecQtyBurned],[JOTerQtyBurned]");
            Globals.PERFORMANCEDATA_INSERT_BASE.Append(",[JOQuaQtyBurned],[TimeStamp],[PJMLoadStatus],[PJMStatusDate],[InactiveHours]) VALUES (");
        }
        private static void UpdateCheckStatusFlagInSetup(DatabaseType _dbType, string _uuc)
        {
            int result = 1;
            StringBuilder sbSQLCmd = new StringBuilder();
            switch (_dbType)
            {
                case DatabaseType.Access:
                    sbSQLCmd.Append("UPDATE Setup SET CheckStatus='R' WHERE UtilityUnitCode='");
                    sbSQLCmd.Append(_uuc);
                    sbSQLCmd.Append("'");
                    result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sbSQLCmd.ToString());

                    break;
                case DatabaseType.SQL:
                    sbSQLCmd.Append("UPDATE Setup SET CheckStatus='R' WHERE UtilityUnitCode='");
                    sbSQLCmd.Append(_uuc);
                    sbSQLCmd.Append("'");
                    result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sbSQLCmd.ToString());
                    break;
            }
        }
        private static void UploadSetupRecords(DatabaseType _dbType)
        {
            int result = 1;
            StringBuilder sbSQLCmd = new StringBuilder();
            // clear out the global error table
            Globals.dtErrors.Clear();
            Globals.dtErrSqlStatements.Clear();

            string errormsg;
            int rowCounter = 0;
            string uuc;

            // if the Setup records Match then we don't need to waste time setting these values
            if (Globals.SetupCompareStatus != Globals.SetupRecordCheckReturn.Match)
            {
                switch (_dbType)
                {
                    case DatabaseType.Access:
                        Globals.StatusMessageText = "Updating Local GADS Setup table";
                        DBTRUE = ACCESSTRUE;
                        DBFALSE = ACCESSFALSE;
                        break;
                    case DatabaseType.SQL:
                        Globals.StatusMessageText = "Updating SQL GADS Setup table";
                        DBTRUE = SQLTRUE;
                        DBFALSE = SQLFALSE;
                        break;
                }
                Globals.StatusBarProgressBarMax = Globals.dsGADSNG.Tables["Setup"].Rows.Count;
                Initialize_SETUP_INSERT_BASE(_dbType);
            }

            foreach (DataRow dr in Globals.dsGADSNG.Tables["Setup"].Rows)
            {
                uuc = DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]);
                errormsg = string.Empty;

                // if the Setup records Match then we don't need to waste time updating them
                if (Globals.SetupCompareStatus != Globals.SetupRecordCheckReturn.Match)
                {
                    Globals.StatusBarProgressBarValue = ++rowCounter;
                    sbSQLCmd = new StringBuilder(Globals.SETUP_INSERT_BASE.ToString());

                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]) + "',");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UnitName"]) + "',");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UnitShortName"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["GrossMaxCapacity"]) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["NetMaxCapacity"]) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["PriFuelCode"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["PriHeatContent"]) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["SecFuelCode"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["SecHeatContent"]) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UnitType"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["GrossDepCapacity"]) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["NetDepCapacity"]) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["TypUnitLoading"]) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["VerbalDesc"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["DaylightSavingTime"]) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["JointOwnership"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["ExpandedReporting"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["InputDataRequirements"]) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["FuelQtyFormat"]) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["BtuOrHeatContent"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["FuelQualityShow"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Perf02DecimalPlaces"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["BulkRS"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["EnglishOrMetric"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["PeakPeriodSet"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["ServiceHourMethod"]) + ",");
                    sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["CommercialDate"]) + "',");
                    sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RetirementDate"]) + "',");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["GMCWeighting"]) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["NMCWeighting"]) + ",");
                    sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["PerformanceDate"]) + "',");
                    sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["PerformanceStart"]) + "',");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Example3D"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["CheckStatus"]) + "',");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["NERC"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["NYISO"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["PJM"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["ISONE"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["UnitSelected"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["MaxCapReadOnly"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["WorkDetails"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CopyToVerbDesc"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CauseCodeExtDisplay"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CauseCodeExtEditable"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["DepCapReadOnly"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["PumpingData"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["SynchCondData"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["GrossNetBoth"]) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["CombinedCycleName"]) + "',");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["CombinedCycleShortName"]) + "',");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["PJMStartsCount"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Hourly"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Daily"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["WeeklyMS"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["WeeklySS"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["FourWeeks"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Monthly"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Quarterly"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Yearly"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["FixedN"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["FixedNValue"]) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["FixedByUser"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["FixedByUserFile"]) + "',");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["PeakPeriod"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["GenElect"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["Siemens"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CAISO"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["MISO"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["ISO1"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["ISO2"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["ISO3"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CEA"])) ? DBTRUE : DBFALSE) + ",");
                    sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CHP"])) ? DBTRUE : DBFALSE) + ")");
                    try
                    {
                        switch (_dbType)
                        {
                            case DatabaseType.Access:
                                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sbSQLCmd.ToString());
                                break;
                            case DatabaseType.SQL:
                                result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sbSQLCmd.ToString());
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        errormsg = ex.Message;
                    }
                }

                // whether the Setup records Match or not we still need to loop though the other tables with the UtilityUnitCode & Year to upload them
                if (result == 0)
                {
                    string msg = "Setup record for [" + uuc + "] failed on insert into table: " + errormsg;
                    Globals.AddErrorToTable(uuc, 0, msg);
                    Globals.AddErrorToSQLErrorTable(sbSQLCmd.ToString());
                }
                else
                {
                    // now switch uploading based on whether it is going to Access or SQL
                    switch (_dbType)
                    {
                        case DatabaseType.Access:
                            break;
                        case DatabaseType.SQL:
                            GetEventData01UpdatedRecordsFromAccess(uuc);

                            GetEventData02UpdatedRecordsFromAccess(uuc);

                            GetPerformanceDataUpdatedRecordsFromAccess(uuc);

                            break;
                        default:
                            break;
                    }

                }
            }
            if (Globals.SetupCompareStatus != Globals.SetupRecordCheckReturn.Match)
            {
                Globals.StatusMessageText = "Updating GADS Setup table complete.";
            }

            // we want to skip over this part if loading local clean ACCESS database from GADS data
            if (Globals.SetupCompareStatus != Globals.SetupRecordCheckReturn.NotSet)
            {
                Globals.StatusMessageText = "Updating GADS EventData01 table.";
                Globals.dsGADSNG.EventData01.Rows.Clear();
                Globals.dsGADSNG.Tables["EventData01"].Load(dsNewGADSNG.Tables["EventData01"].CreateDataReader());
                UploadEventData01Records(DatabaseType.SQL);

                Globals.StatusMessageText = "Updating GADS EventData02 table.";
                Globals.dsGADSNG.EventData02.Rows.Clear();
                Globals.dsGADSNG.Tables["EventData02"].Load(dsNewGADSNG.Tables["EventData02"].CreateDataReader());
                UploadEventData02Records(DatabaseType.SQL);

                Globals.StatusMessageText = "Updating GADS PerformanceData table.";
                Globals.dsGADSNG.PerformanceData.Rows.Clear();
                Globals.dsGADSNG.Tables["PerformanceData"].Load(dsNewGADSNG.Tables["PerformanceData"].CreateDataReader());
                UploadPerformanceDataRecords(DatabaseType.SQL);
            }
        }
        private static void UploadEventData01Records(DatabaseType _dbType)
        {
            int result = 0;
            StringBuilder sbSQLCmd;
            string errormsg;
            int rowCounter = 0;

            switch (_dbType)
            {
                case DatabaseType.Access:
                    Globals.StatusMessageText = "Updating Local GADS EventData01 table";
                    DBTRUE = ACCESSTRUE;
                    DBFALSE = ACCESSFALSE;
                    break;
                case DatabaseType.SQL:
                    Globals.StatusMessageText = "Updating SQL GADS EventData01 table";
                    DBTRUE = SQLTRUE;
                    DBFALSE = SQLFALSE;
                    break;
            }

            Globals.StatusBarProgressBarMax = Globals.dsGADSNG.Tables["EventData01"].Rows.Count;
            Initialize_EVENTDATA01_INSERT_BASE(_dbType);

            foreach (DataRow dr in Globals.dsGADSNG.Tables["EventData01"].Rows)
            {
                Globals.StatusBarProgressBarValue = ++rowCounter;
                sbSQLCmd = new StringBuilder(Globals.EVENTDATA01_INSERT_BASE.ToString());
                errormsg = string.Empty;

                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UnitShortName"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["Year"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["EventNumber"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard01"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["EventType"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["StartDateTime"]) + "',");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CarryOverLastYear"])) ? DBTRUE : DBFALSE) + ",");
                //sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["ChangeDateTime1"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["ChangeInEventType1"]) + "',");
                //sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["ChangeDateTime2"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["ChangeInEventType2"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["EndDateTime"]) + "',");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["CarryOverNextYear"])) ? DBTRUE : DBFALSE) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["GrossAvailCapacity"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["NetAvailCapacity"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["CauseCode"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["CauseCodeExt"]) + "',");
                //sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["WorkStarted"]) + "',");
                //sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["WorkEnded"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["ContribCode"]) + ",");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["PrimaryAlert"])) ? DBTRUE : DBFALSE) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["ManhoursWorked"]) + ",");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc1"])) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard02"]) + "',");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc2"])) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard03"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard01"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard02"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard03"]) + "',");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["EditFlag"])) ? DBTRUE : DBFALSE) + ",");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc86"])) + "',");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDescFull"])) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["FailureMechCode"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["TripMech"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["CumFiredHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["CumEngineStarts"]) + ",");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["DominantDerate"])) ? DBTRUE : DBFALSE) + ",");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["TimeStamp"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["PJMIOCode"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["PJMLoadStatus"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["PJMStatusDate"]) + "')");

                try
                {
                    switch (_dbType)
                    {
                        case DatabaseType.Access:
                            result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sbSQLCmd.ToString());
                            break;
                        case DatabaseType.SQL:
                            result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sbSQLCmd.ToString());
                            break;
                    }
                    UpdateCheckStatusFlagInSetup(_dbType, DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]));
                }
                catch (Exception ex)
                {
                    errormsg = ex.Message;
                }
                if (result == 0)
                {
                    string uuc = DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]);
                    string msg = "EventData01 record for Unit[" + uuc + "] Year[" +
                        DBUtilsAlias.Datasets.GetInt(dr["Year"]) + "] EventNumber[" +
                        DBUtilsAlias.Datasets.GetInt(dr["EventNumber"]) + "] failed on insert into table: " + errormsg;

                    Globals.AddErrorToTable(uuc, 0, msg);
                    Globals.AddErrorToSQLErrorTable(sbSQLCmd.ToString());
                }
            }
            Globals.StatusMessageText = Globals.StatusMessageText + " Complete.";
        }
        private static void UploadEventData02Records(DatabaseType _dbType)
        {
            int result = 0;
            StringBuilder sbSQLCmd;
            string errormsg;
            int rowCounter = 0;

            switch (_dbType)
            {
                case DatabaseType.Access:
                    Globals.StatusMessageText = "Updating Local GADS EventData02 table";
                    DBTRUE = ACCESSTRUE;
                    DBFALSE = ACCESSFALSE;
                    break;
                case DatabaseType.SQL:
                    Globals.StatusMessageText = "Updating SQL GADS EventData02 table";
                    DBTRUE = SQLTRUE;
                    DBFALSE = SQLFALSE;
                    break;
            }

            Globals.StatusBarProgressBarMax = Globals.dsGADSNG.Tables["EventData02"].Rows.Count;
            Initialize_EVENTDATA02_INSERT_BASE(_dbType);

            foreach (DataRow dr in Globals.dsGADSNG.Tables["EventData02"].Rows)
            {
                Globals.StatusBarProgressBarValue = ++rowCounter;
                sbSQLCmd = new StringBuilder(Globals.EVENTDATA02_INSERT_BASE.ToString());
                errormsg = string.Empty;

                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UnitShortName"]).Trim() + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["Year"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["EventNumber"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCardEven"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["EventType"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["CauseCode"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["CauseCodeExt"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["WorkStarted"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["WorkEnded"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["ContribCode"]) + ",");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["PrimaryAlert"])) ? DBTRUE : DBFALSE) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["ManhoursWorked"]) + ",");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc1"])).Trim() + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["EvenCardNumber"]) + ",");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc2"])).Trim() + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCardOdd"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCardEven"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCardOdd"]) + "',");
                sbSQLCmd.Append(((DBUtilsAlias.Datasets.GetBool(dr["EditFlag"])) ? DBTRUE : DBFALSE) + ",");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc86"])).Trim() + "',");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDescFull"])).Trim() + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["FailureMechCode"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["TripMech"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["CumFiredHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["CumEngineStarts"]) + ",");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["TimeStamp"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["PJMLoadStatus"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["PJMStatusDate"]) + "')");
                try
                {
                    switch (_dbType)
                    {
                        case DatabaseType.Access:
                            result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sbSQLCmd.ToString());
                            break;
                        case DatabaseType.SQL:
                            result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sbSQLCmd.ToString());
                            break;
                    }
                }
                catch (Exception ex)
                {
                    errormsg = ex.Message;
                }
                if (result == 0)
                {
                    string uuc = DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]);
                    string msg = "EventData02 record for [" + uuc + "] failed on insert into table: " + errormsg;
                    Globals.AddErrorToTable(uuc, 0, msg);
                    Globals.AddErrorToSQLErrorTable(sbSQLCmd.ToString());
                }
            }
            Globals.StatusMessageText = Globals.StatusMessageText + " Complete.";
        }
        private static void UploadPerformanceDataRecords(DatabaseType _dbType)
        {
            int result = 0;
            StringBuilder sbSQLCmd;
            // clear out the global error table
            string errormsg;
            int rowCounter = 0;

            switch (_dbType)
            {
                case DatabaseType.Access:
                    Globals.StatusMessageText = "Updating Local GADS PerformanceData table";
                    DBTRUE = ACCESSTRUE;
                    DBFALSE = ACCESSFALSE;
                    break;
                case DatabaseType.SQL:
                    Globals.StatusMessageText = "Updating SQL GADS PerformanceData table";
                    DBTRUE = SQLTRUE;
                    DBFALSE = SQLFALSE;
                    break;
            }

            Globals.StatusBarProgressBarMax = Globals.dsGADSNG.Tables["PerformanceData"].Rows.Count;
            Initialize_PERFORMANCEDATA_INSERT_BASE(_dbType);

            foreach (DataRow dr in Globals.dsGADSNG.Tables["PerformanceData"].Rows)
            {
                Globals.StatusBarProgressBarValue = ++rowCounter;
                sbSQLCmd = new StringBuilder(Globals.PERFORMANCEDATA_INSERT_BASE.ToString());
                errormsg = string.Empty;

                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["UnitShortName"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["Year"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["Period"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard1"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["GrossMaxCap"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["GrossDepCap"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["GrossGen"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["NetMaxCap"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["NetDepCap"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["NetGen"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetByte(dr["TypUnitLoading"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["AttemptedStarts"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["ActualStarts"]) + ",");
                sbSQLCmd.Append("'" + UtilLibAlias.CleanDatabaseString(DBUtilsAlias.Datasets.GetString(dr["VerbalDesc"])).Trim() + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard2"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["ServiceHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["RSHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PumpingHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SynchCondHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PlannedOutageHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["ForcedOutageHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["MaintOutageHours"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["ExtofSchedOutages"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PeriodHours"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard3"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["PriFuelCode"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["PriAvgHeatContent"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriBtus"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriPercentAsh"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriPercentMoisture"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriPercentSulfur"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriPercentAlkalines"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["PriGrindIndexVanad"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["PriAshSoftTemp"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["SecFuelCode"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["SecAvgHeatContent"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecBtus"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecPercentAsh"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecPercentMoisture"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecPercentSulfur"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecPercentAlkalines"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["SecGrindIndexVanad"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["SecAshSoftTemp"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["RevisionCard4"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["TerFuelCode"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["TerAvgHeatContent"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerBtus"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerPercentAsh"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerPercentMoisture"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerPercentSulfur"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerPercentAlkalines"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["TerGrindIndexVanad"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["TerAshSoftTemp"]) + ",");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["QuaFuelCode"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["QuaAvgHeatContent"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaBtus"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaPercentAsh"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaPercentMoisture"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaPercentSulfur"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaPercentAlkalines"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["QuaGrindIndexVanad"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetInt(dr["QuaAshSoftTemp"]) + ",");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard1"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard2"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard3"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["RevMonthCard4"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JOGrossMaxCap"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JOGrossGen"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JONetMaxCap"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JONetGen"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JOPriQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JOSecQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JOTerQtyBurned"]) + ",");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["JOQuaQtyBurned"]) + ",");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["TimeStamp"]) + "',");
                sbSQLCmd.Append("'" + DBUtilsAlias.Datasets.GetString(dr["PJMLoadStatus"]) + "',");
                sbSQLCmd.Append("'" + FormatDateTimeString(_dbType, dr["PJMStatusDate"]) + "',");
                sbSQLCmd.Append(DBUtilsAlias.Datasets.GetDouble(dr["InactiveHours"]) + ")");

                try
                {
                    switch (_dbType)
                    {
                        case DatabaseType.Access:
                            result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.accessConnGD, sbSQLCmd.ToString());
                            break;
                        case DatabaseType.SQL:
                            result = DBUtilsAlias.DatabaseAccess.GetIntValue(Globals.sqlConnGD, sbSQLCmd.ToString());
                            break;
                    }
                    UpdateCheckStatusFlagInSetup(_dbType, DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]));
                }
                catch (Exception ex)
                {
                    errormsg = ex.Message;
                }
                if (result == 0)
                {
                    string uuc = DBUtilsAlias.Datasets.GetString(dr["UtilityUnitCode"]);
                    string msg = "PerformanceData record for [" + uuc + "] failed on insert into table: " + errormsg;
                    Globals.AddErrorToTable(uuc, 0, msg);
                    Globals.AddErrorToSQLErrorTable(sbSQLCmd.ToString());
                }
            }
            Globals.StatusMessageText = Globals.StatusMessageText + " Complete.";
        }

        private static string FormatDateTimeString(DatabaseType _dbType, object _dtCol)
        {
            string _dt = string.Empty;
            switch (_dbType)
            {
                case DatabaseType.Access:
                    _dt = DBUtilsAlias.Datasets.GetDateTime(_dtCol).ToString();
                    break;
                case DatabaseType.SQL:
                    _dt = DBUtilsAlias.Datasets.GetDateTime(_dtCol).ToString("MM/dd/yyyy HH:mm:ss");
                    break;
            }
            if (_dt.StartsWith("01/01/0001"))
                _dt = string.Empty;
            return _dt;
        }
        #endregion Private Methods

        #endregion Methods

    }
}
