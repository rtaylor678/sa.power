using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("ManageGADSData")]
[assembly: AssemblyDescription("Manages all NERC and non-NERC GADS Data")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Solomon Associates")]
[assembly: AssemblyProduct("ManageGADSData")]
[assembly: AssemblyCopyright("Copyright © 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

[assembly: Guid("06b133df-f23e-4f10-a2ec-373ecebaa99a")]

[assembly: AssemblyVersion("10.0.0.0")]
[assembly: AssemblyFileVersion("10.7.22.0")]
