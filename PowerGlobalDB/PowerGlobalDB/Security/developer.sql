﻿CREATE ROLE [developer]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [developer] ADD MEMBER [DC1\Power_Developers];


GO
ALTER ROLE [developer] ADD MEMBER [DC1\Power_DBOwners];

