﻿ALTER ROLE [db_owner] ADD MEMBER [DC1\Power_DBOwners];


GO
ALTER ROLE [db_owner] ADD MEMBER [PWRAPPS];


GO
ALTER ROLE [db_datareader] ADD MEMBER [DC1\Power_DataProcessors];


GO
ALTER ROLE [db_datareader] ADD MEMBER [DC1\Console_Developers];


GO
ALTER ROLE [db_datareader] ADD MEMBER [PWRAPPS];


GO
ALTER ROLE [db_datareader] ADD MEMBER [Consultants];


GO
ALTER ROLE [db_datawriter] ADD MEMBER [PWRAPPS];

