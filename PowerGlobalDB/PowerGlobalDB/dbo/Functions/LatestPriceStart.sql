﻿

CREATE FUNCTION dbo.LatestPriceStart (@PricingHub varchar(15))  
RETURNS smalldatetime AS  
BEGIN 
DECLARE @date smalldatetime
SELECT @date = MAX(StartTime) FROM Prices WHERE PricingHub = @PricingHub
RETURN @date
END



