﻿
CREATE FUNCTION dbo.EarliestPrice (@PricingHub varchar(15))  
RETURNS smalldatetime AS  
BEGIN 
DECLARE @date smalldatetime
SELECT @date = MIN(StartTime) FROM Prices WHERE PricingHub = @PricingHub
RETURN @date
END

