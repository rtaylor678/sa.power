﻿CREATE RULE [dbo].[chkItemType]
    AS @Value IN ('I', 'S');


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkItemType]', @objname = N'[dbo].[ReportItems].[ItemType]';


GO
EXECUTE sp_bindrule @rulename = N'[dbo].[chkItemType]', @objname = N'[dbo].[ItemType]';

