﻿CREATE TABLE [dbo].[EGCCoefficientsRegions] (
    [id]            INT        IDENTITY (1, 1) NOT NULL,
    [Year]          INT        NOT NULL,
    [EGCTechnology] CHAR (15)  NOT NULL,
    [Region]        CHAR (20)  NOT NULL,
    [Coal]          FLOAT (53) NOT NULL,
    [Oil]           FLOAT (53) NOT NULL,
    [Gas]           FLOAT (53) NOT NULL,
    [CTG]           FLOAT (53) NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [YrTech]
    ON [dbo].[EGCCoefficientsRegions]([Year] ASC, [EGCTechnology] ASC, [Region] ASC);

