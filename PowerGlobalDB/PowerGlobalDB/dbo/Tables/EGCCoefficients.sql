﻿CREATE TABLE [dbo].[EGCCoefficients] (
    [id]                INT       IDENTITY (1, 1) NOT NULL,
    [Year]              INT       NOT NULL,
    [EGCTechnology]     CHAR (15) NOT NULL,
    [CTGs]              REAL      NOT NULL,
    [Scrubbers]         REAL      NOT NULL,
    [PrecBag]           REAL      NOT NULL,
    [MwhperM]           REAL      NOT NULL,
    [ScalingFactor]     REAL      NOT NULL,
    [Starts]            REAL      NOT NULL,
    [MwhperM_HHV]       REAL      NULL,
    [MwhperM_Sul]       REAL      NOT NULL,
    [MwhperM_Ash]       REAL      NOT NULL,
    [Age]               REAL      NOT NULL,
    [Pwr_CTGs]          REAL      NOT NULL,
    [Pwr_Scrubbers]     REAL      NOT NULL,
    [Pwr_PrecBag]       REAL      NOT NULL,
    [Pwr_Coal]          REAL      NOT NULL,
    [Pwr_Oil]           REAL      NOT NULL,
    [Pwr_Gas]           REAL      NOT NULL,
    [Pwr_CTG]           REAL      NOT NULL,
    [Pwr_MwhperM]       REAL      NOT NULL,
    [Pwr_ScalingFactor] REAL      NOT NULL,
    [Pwr_Starts]        REAL      NOT NULL,
    [Pwr_MwhperM_HHV]   REAL      NULL,
    [Pwr_MwhperM_Sul]   REAL      NOT NULL,
    [Pwr_MwhperM_Ash]   REAL      NOT NULL,
    [Pwr_Age]           REAL      NOT NULL
);


GO
CREATE UNIQUE CLUSTERED INDEX [YrTech]
    ON [dbo].[EGCCoefficients]([Year] ASC, [EGCTechnology] ASC);

