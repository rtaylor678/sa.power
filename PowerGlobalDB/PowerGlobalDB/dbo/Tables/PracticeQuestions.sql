﻿CREATE TABLE [dbo].[PracticeQuestions] (
    [QuestionCode] CHAR (4)      NOT NULL,
    [Question]     VARCHAR (255) NOT NULL,
    [GroupCode]    CHAR (4)      NULL,
    CONSTRAINT [PK_PracticeQuestions_1__13] PRIMARY KEY CLUSTERED ([QuestionCode] ASC) WITH (FILLFACTOR = 90)
);

