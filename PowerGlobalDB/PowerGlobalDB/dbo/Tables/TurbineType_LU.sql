﻿CREATE TABLE [dbo].[TurbineType_LU] (
    [id]   INT          IDENTITY (1, 1) NOT NULL,
    [Name] NCHAR (4)    NOT NULL,
    [Desc] VARCHAR (50) NULL,
    CONSTRAINT [PK_TurbineType_LU] PRIMARY KEY CLUSTERED ([Name] ASC)
);

