﻿CREATE TABLE [dbo].[Site_LU] (
    [SiteNo]    INT          NOT NULL,
    [CompanyID] CHAR (15)    NOT NULL,
    [SiteName]  VARCHAR (75) NOT NULL,
    [SiteLabel] CHAR (5)     NULL,
    [City]      CHAR (50)    NULL,
    [State]     CHAR (20)    NULL,
    CONSTRAINT [PK_SiteLU] PRIMARY KEY CLUSTERED ([SiteNo] ASC) WITH (FILLFACTOR = 90)
);

