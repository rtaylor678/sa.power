﻿CREATE TABLE [dbo].[Pricesbackup] (
    [PricingHub] CHAR (15)  NOT NULL,
    [StartTime]  DATETIME   NOT NULL,
    [EndTime]    DATETIME   NOT NULL,
    [Price]      FLOAT (53) NULL
);

