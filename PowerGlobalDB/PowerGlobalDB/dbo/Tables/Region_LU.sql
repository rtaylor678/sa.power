﻿CREATE TABLE [dbo].[Region_LU] (
    [id]     INT           IDENTITY (1, 1) NOT NULL,
    [Region] VARCHAR (50)  NOT NULL,
    [Desc]   VARCHAR (100) NULL,
    CONSTRAINT [PK_Region_LU] PRIMARY KEY CLUSTERED ([Region] ASC)
);

