﻿CREATE TABLE [dbo].[ReportOptions] (
    [ReportSetID]       [dbo].[DD_ID]            NOT NULL,
    [SheetName]         VARCHAR (30)             NOT NULL,
    [ReportID]          [dbo].[DD_ID]            NOT NULL,
    [RefListNo]         [dbo].[RefListNo]        NOT NULL,
    [BreakConditions]   VARCHAR (255)            NOT NULL,
    [RptOptionScenario] [dbo].[Scenario]         CONSTRAINT [DF_ReportOpti_RptOptionSc1__11] DEFAULT ('BASE') NULL,
    [RptOptionSQL]      VARCHAR (255)            NULL,
    [Orientation]       CHAR (1)                 CONSTRAINT [DF__ReportOpt__Orien__64ACF71D] DEFAULT ('R') NOT NULL,
    [SortKey]           TINYINT                  NULL,
    [PreferenceID]      [dbo].[UserPreferenceID] NULL,
    [RptOptionCurrency] [dbo].[CurrencyCode]     NULL,
    CONSTRAINT [PK_ReportOptions_1__19] PRIMARY KEY CLUSTERED ([ReportSetID] ASC, [SheetName] ASC) WITH (FILLFACTOR = 90)
);

