﻿CREATE TABLE [dbo].[FuelType_LU] (
    [FuelType]    CHAR (6)     NOT NULL,
    [Description] VARCHAR (50) NOT NULL,
    [LHVFactor]   REAL         NOT NULL,
    [FuelGroup]   CHAR (1)     NOT NULL,
    CONSTRAINT [PK_FuelType_LU] PRIMARY KEY CLUSTERED ([FuelType] ASC)
);

