﻿CREATE TABLE [dbo].[CRVGapFactors] (
    [StudyYear]                 SMALLINT NOT NULL,
    [CRVSizeGroup]              CHAR (6) NOT NULL,
    [PeakUnavail_Unp]           REAL     NOT NULL,
    [HeatRate]                  REAL     NOT NULL,
    [TotCashLessFuelMWH]        REAL     NULL,
    [TotCashLessFuelEmmMWH]     REAL     NULL,
    [PlantManageableMWH]        REAL     NULL,
    [TotCashLessFuelEmmMW]      REAL     NULL,
    [TotCashLessFuelEmmEGC]     REAL     NULL,
    [UnplannedCommUnavailIndex] REAL     NULL,
    [PlannedCommUnavailIndex]   REAL     NULL,
    [AuxPct]                    REAL     NULL,
    [ThermEff]                  REAL     NULL,
    [MaintIndexMWH]             REAL     NULL,
    [OHIndexMWH]                REAL     NULL,
    [NonOHIndexMWH]             REAL     NULL,
    [LTSAIndexMWH]              REAL     NULL,
    CONSTRAINT [PK_CRVGapFactors] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [CRVSizeGroup] ASC) WITH (FILLFACTOR = 90)
);

