﻿CREATE TABLE [dbo].[ComponentEUFPlus_LU] (
    [EquipGroup] CHAR (8)  NOT NULL,
    [Component]  CHAR (50) NOT NULL,
    CONSTRAINT [PK_ComponentEUFPlus_LU] PRIMARY KEY CLUSTERED ([EquipGroup] ASC)
);

