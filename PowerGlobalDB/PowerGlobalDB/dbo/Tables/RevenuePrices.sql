﻿CREATE TABLE [dbo].[RevenuePrices] (
    [PricingHub] [dbo].[PricingHub] NOT NULL,
    [StartTime]  SMALLDATETIME      NOT NULL,
    [EndTime]    SMALLDATETIME      NOT NULL,
    [Price]      SMALLMONEY         NULL,
    CONSTRAINT [PK_RevenuePrices_1__14] PRIMARY KEY CLUSTERED ([PricingHub] ASC, [StartTime] ASC) WITH (FILLFACTOR = 90)
);

