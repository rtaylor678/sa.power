﻿CREATE TABLE [dbo].[Project_LU] (
    [ProjectID]   CHAR (15)    NOT NULL,
    [Description] VARCHAR (75) NOT NULL,
    [SortKey]     SMALLINT     NULL,
    [ReportLabel] VARCHAR (50) NULL,
    CONSTRAINT [PK___2__21] PRIMARY KEY CLUSTERED ([ProjectID] ASC) WITH (FILLFACTOR = 90)
);

