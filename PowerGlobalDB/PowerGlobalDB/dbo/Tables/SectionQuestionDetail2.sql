﻿CREATE TABLE [dbo].[SectionQuestionDetail2] (
    [SectionID]         CHAR (3)      NOT NULL,
    [QuestionID]        SMALLINT      NOT NULL,
    [QuestionLvl]       CHAR (1)      NOT NULL,
    [QuestionDetailId]  CHAR (1)      NOT NULL,
    [QuestionDetailId2] CHAR (1)      NOT NULL,
    [QuestionDetail2]   VARCHAR (255) NULL,
    [GroupCode]         CHAR (255)    NULL,
    [QuestionCode]      CHAR (4)      NULL,
    CONSTRAINT [PK___3__19] PRIMARY KEY CLUSTERED ([SectionID] ASC, [QuestionID] ASC, [QuestionLvl] ASC, [QuestionDetailId] ASC, [QuestionDetailId2] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK___2__19] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[Sections] ([SectionID])
);

