﻿CREATE TABLE [dbo].[ComponentEUF_LU] (
    [UnitType]      CHAR (50) NOT NULL,
    [SAIMajorEquip] CHAR (40) NOT NULL,
    [SAIMinorEquip] CHAR (40) NOT NULL,
    [EquipGroup]    CHAR (8)  NOT NULL,
    CONSTRAINT [PK_ComponentEUF_LU] PRIMARY KEY CLUSTERED ([UnitType] ASC, [SAIMajorEquip] ASC, [SAIMinorEquip] ASC, [EquipGroup] ASC)
);

