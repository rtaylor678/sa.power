﻿CREATE TABLE [dbo].[Company_LU] (
    [CompanyID]   CHAR (15) NOT NULL,
    [CompanyName] CHAR (50) NOT NULL,
    [LastName]    CHAR (40) NULL,
    [FirstName]   CHAR (40) NULL,
    [Email]       CHAR (50) NULL,
    [Password]    CHAR (20) NULL,
    [Regulated]   CHAR (1)  NULL,
    CONSTRAINT [PK_CompanyLU_2__17] PRIMARY KEY CLUSTERED ([CompanyID] ASC) WITH (FILLFACTOR = 90)
);

