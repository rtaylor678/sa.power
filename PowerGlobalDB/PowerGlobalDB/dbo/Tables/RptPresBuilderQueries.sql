﻿CREATE TABLE [dbo].[RptPresBuilderQueries] (
    [id]                INT            IDENTITY (1, 1) NOT NULL,
    [StudyYear]         INT            NOT NULL,
    [ChartName]         VARCHAR (50)   NOT NULL,
    [SlideNumber]       INT            NULL,
    [CalcByPresBldr]    BIT            CONSTRAINT [DF_RptPresBuilderQueries_CalcByPresBldr] DEFAULT ((0)) NOT NULL,
    [StudyData]         BIT            CONSTRAINT [DF_PresBuilderQueries_StudyData] DEFAULT ((0)) NULL,
    [TemplateUpdated]   BIT            CONSTRAINT [DF_RptPresBuilderQueries_TemplateUpdated] DEFAULT ((0)) NULL,
    [SlideDataUpdated]  BIT            CONSTRAINT [DF_PresBuilderQueries_SlideDataUpdated] DEFAULT ((0)) NULL,
    [QuartileUpdated]   BIT            CONSTRAINT [DF_RptPresBuilderQueries_QuartileUpdated] DEFAULT ((0)) NULL,
    [HasQuartiles]      BIT            CONSTRAINT [DF_RptPresBuilderQueries_HasQuartiles] DEFAULT ((0)) NULL,
    [ChartType]         VARCHAR (30)   NULL,
    [PlotType]          VARCHAR (20)   NULL,
    [LiveChartLocation] VARCHAR (255)  NULL,
    [Title1]            VARCHAR (255)  NULL,
    [Title2]            VARCHAR (255)  NULL,
    [Description]       VARCHAR (500)  NULL,
    [QueryOrder]        INT            NULL,
    [Query]             VARCHAR (1000) NULL,
    [UpdatedBy]         VARCHAR (30)   NULL,
    [UpdatedOn]         DATETIME       NULL,
    [PPT_PageTemplate]  XML            NULL
);

