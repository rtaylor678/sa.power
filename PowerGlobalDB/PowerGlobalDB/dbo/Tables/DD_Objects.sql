﻿CREATE TABLE [dbo].[DD_Objects] (
    [ObjectID]     [dbo].[DD_ID] IDENTITY (1, 1) NOT NULL,
    [ObjectName]   [sysname]     NOT NULL,
    [SectionID]    TINYINT       NULL,
    [HelpID]       VARCHAR (30)  NULL,
    [ObjectDesc]   VARCHAR (50)  NULL,
    [KeyFields]    VARCHAR (255) NULL,
    [TableType]    CHAR (1)      NULL,
    [RptSortOrder] INT           NULL,
    [DescFields]   VARCHAR (255) NULL,
    [CopyData]     TINYINT       NULL,
    CONSTRAINT [PK_DD_Objects_1__13] PRIMARY KEY CLUSTERED ([ObjectID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueObject] UNIQUE NONCLUSTERED ([ObjectName] ASC) WITH (FILLFACTOR = 90)
);

