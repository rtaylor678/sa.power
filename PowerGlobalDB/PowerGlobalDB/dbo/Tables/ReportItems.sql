﻿CREATE TABLE [dbo].[ReportItems] (
    [ReportID]             [dbo].[DD_ID]        NOT NULL,
    [DisplayOrder]         SMALLINT             NOT NULL,
    [ReportItemID]         [dbo].[DD_ID]        NOT NULL,
    [ItemType]             [dbo].[ItemType]     NOT NULL,
    [LabelOverride]        VARCHAR (50)         NULL,
    [FormatOverride]       TINYINT              NULL,
    [DisplayUnitsOverride] [dbo].[UOM]          NULL,
    [RptItemScenario]      [dbo].[Scenario]     NULL,
    [RptItemSQL]           VARCHAR (255)        NULL,
    [RptItemCurrency]      [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_ReportItems_1__19] PRIMARY KEY CLUSTERED ([ReportID] ASC, [DisplayOrder] ASC) WITH (FILLFACTOR = 90)
);

