﻿CREATE TABLE [dbo].[Equip_LU] (
    [EquipType]   CHAR (8)     NOT NULL,
    [Description] VARCHAR (50) NULL,
    [SortKey]     TINYINT      NULL,
    CONSTRAINT [PK_Equip_LU_1__10] PRIMARY KEY CLUSTERED ([EquipType] ASC) WITH (FILLFACTOR = 90)
);

