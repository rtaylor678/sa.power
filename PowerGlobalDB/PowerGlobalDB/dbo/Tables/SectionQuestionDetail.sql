﻿CREATE TABLE [dbo].[SectionQuestionDetail] (
    [SectionID]        CHAR (3)      NOT NULL,
    [QuestionID]       SMALLINT      NOT NULL,
    [QuestionLvl]      CHAR (1)      NOT NULL,
    [QuestionDetailId] CHAR (1)      NOT NULL,
    [QuestionDetail]   VARCHAR (255) NULL,
    [DetailedCategory] CHAR (3)      NULL,
    [GroupCode]        CHAR (4)      NULL,
    [QuestionCode]     CHAR (4)      NULL,
    CONSTRAINT [PK___3__17] PRIMARY KEY CLUSTERED ([SectionID] ASC, [QuestionID] ASC, [QuestionLvl] ASC, [QuestionDetailId] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK___2__17] FOREIGN KEY ([SectionID]) REFERENCES [dbo].[Sections] ([SectionID])
);

