﻿CREATE TABLE [dbo].[Component_LU] (
    [Component]      CHAR (8)     NOT NULL,
    [Description]    VARCHAR (75) NOT NULL,
    [EquipGroup]     CHAR (8)     NULL,
    [SortKey]        SMALLINT     NULL,
    [ReportLabel]    VARCHAR (50) NULL,
    [EquipGroup2]    VARCHAR (8)  NULL,
    [EquipGroupSort] SMALLINT     NULL,
    CONSTRAINT [PK_ComponentLU] PRIMARY KEY CLUSTERED ([Component] ASC) WITH (FILLFACTOR = 90)
);

