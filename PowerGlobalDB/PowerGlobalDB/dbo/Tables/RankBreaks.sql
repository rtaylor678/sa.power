﻿CREATE TABLE [dbo].[RankBreaks] (
    [BreakID]     [dbo].[DD_ID] NOT NULL,
    [Description] VARCHAR (30)  NULL,
    [Active]      [dbo].[YorN]  CONSTRAINT [DF__RankBreak__Activ__1EF99443] DEFAULT ('Y') NOT NULL,
    CONSTRAINT [PK_RankBreaks_1] PRIMARY KEY CLUSTERED ([BreakID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueBreakDesc] UNIQUE NONCLUSTERED ([Description] ASC) WITH (FILLFACTOR = 90)
);

