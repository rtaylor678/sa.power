﻿CREATE TABLE [dbo].[ReportDef] (
    [ReportID]          [dbo].[DD_ID]        IDENTITY (1, 1) NOT NULL,
    [ReportName]        VARCHAR (30)         NOT NULL,
    [Description]       VARCHAR (100)        NULL,
    [ReportDefSQL]      VARCHAR (255)        NULL,
    [ReportDefScenario] [dbo].[Scenario]     NULL,
    [ReportDefCurrency] [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_ReportDef_1__14] PRIMARY KEY CLUSTERED ([ReportID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueReportName] UNIQUE NONCLUSTERED ([ReportName] ASC) WITH (FILLFACTOR = 90)
);

