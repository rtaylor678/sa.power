﻿CREATE TABLE [dbo].[ReportSets] (
    [ReportSetID]   [dbo].[DD_ID] IDENTITY (1, 1) NOT NULL,
    [ReportSetName] VARCHAR (30)  NOT NULL,
    [CreateDate]    SMALLDATETIME CONSTRAINT [DF_ReportSets_CreateDate_1__13] DEFAULT (getdate()) NOT NULL,
    [JobNo]         VARCHAR (10)  NULL,
    [Owner]         VARCHAR (5)   NULL,
    [Description]   VARCHAR (100) NULL,
    [FileName]      VARCHAR (100) NULL,
    CONSTRAINT [PK_ReportSets_1__14] PRIMARY KEY CLUSTERED ([ReportSetID] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [UniqueRptSetName] UNIQUE NONCLUSTERED ([ReportSetName] ASC) WITH (FILLFACTOR = 90)
);

