﻿CREATE TABLE [dbo].[SectionSelection] (
    [GroupCode]          CHAR (4) NOT NULL,
    [AvailableSelection] CHAR (1) NOT NULL,
    [ListCode]           CHAR (4) NOT NULL,
    CONSTRAINT [PK___3__12] PRIMARY KEY CLUSTERED ([GroupCode] ASC, [AvailableSelection] ASC) WITH (FILLFACTOR = 90)
);

