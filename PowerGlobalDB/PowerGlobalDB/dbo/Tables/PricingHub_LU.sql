﻿CREATE TABLE [dbo].[PricingHub_LU] (
    [PricingHub]     VARCHAR (15)  NOT NULL,
    [Source]         VARCHAR (20)  NULL,
    [HubDescription] VARCHAR (200) NULL,
    [EarliestPrice]  AS            ([dbo].[EarliestPrice]([PricingHub])),
    [LatestPrice]    AS            ([dbo].[LatestPrice]([PricingHub])),
    CONSTRAINT [PK_PricingHub_LU] PRIMARY KEY CLUSTERED ([PricingHub] ASC)
);

