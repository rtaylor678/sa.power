﻿CREATE TABLE [dbo].[Class_LU] (
    [StudyYear] INT      NOT NULL,
    [ClassType] CHAR (5) NOT NULL,
    [ClassNum]  TINYINT  NOT NULL,
    [MinCap]    REAL     CONSTRAINT [DF_ClassLU_MinCap_2__13] DEFAULT (0.1) NOT NULL,
    [MaxCap]    REAL     CONSTRAINT [DF_ClassLU_MaxCap_1__13] DEFAULT (1000000) NOT NULL,
    CONSTRAINT [PK_Class_LU] PRIMARY KEY NONCLUSTERED ([StudyYear] ASC, [ClassType] ASC, [ClassNum] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [ClassID] UNIQUE CLUSTERED ([StudyYear] ASC, [ClassType] ASC, [ClassNum] ASC) WITH (FILLFACTOR = 90)
);

