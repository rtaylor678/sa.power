﻿
/****** Object:  Stored Procedure dbo.spGetColumnID    Script Date: 4/19/03 1:58:20 PM ******/

/****** Object:  Stored Procedure dbo.spGetColumnID    Script Date: 12/28/2001 7:35:04 AM ******/
CREATE PROCEDURE spGetColumnID
	@ObjectName varchar(30) = NULL, 
	@ColumnName varchar(30),
	@ObjectID integer = NULL,
	@ColumnID integer OUTPUT
AS
IF @ObjectName IS NULL AND @ObjectID IS NULL 
	RETURN -101
ELSE BEGIN
	IF @ObjectName IS NOT NULL
		SELECT @ColumnID = ColumnID
		FROM DD_Objects o INNER JOIN DD_Columns c
		ON o.ObjectID = c.ObjectID
		WHERE o.ObjectName = @ObjectName AND c.ColumnName = @ColumnName
	ELSE
		SELECT @ColumnID = ColumnID
		FROM DD_Columns c
		WHERE c.ObjectID = @ObjectID AND c.ColumnName = @ColumnName
END

