﻿
/****** Object:  Stored Procedure dbo.spAddRefList    Script Date: 4/19/03 1:58:20 PM ******/
/****** Object:  Stored Procedure dbo.spAddRefList    Script Date: 12/28/2001 7:35:04 AM ******/
CREATE PROCEDURE spAddRefList @ListName varchar(30),
			 @Owner varchar(5) = NULL,
			 @JobNo varchar(10) = NULL,
			 @Description varchar(50) = NULL,
			 @ListNo int OUTPUT
AS
IF EXISTS (SELECT * FROM RefList_LU WHERE ListName = @ListName)
	SELECT @ListNo = -1
ELSE
BEGIN
	INSERT INTO RefList_LU (ListName, Owner, JobNo, Description)
	VALUES (@ListName, @Owner, @JobNo, @Description)
	SELECT @ListNo = RefListNo
	FROM RefList_LU
	WHERE ListName = @ListName
END
