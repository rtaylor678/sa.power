﻿CREATE PROCEDURE [dbo].[CalcVarCost](@SiteID SiteID)
AS
-- Only do these calculations for data after the 1998 study
IF EXISTS (SELECT * FROM StudySites WHERE SiteID = @SiteID AND StudyYear <= 1998)
	RETURN 0
	
-- Get generation and non-fuel variable cost for each unit
DECLARE @Units TABLE (
	Refnum varchar(12) NOT NULL,
	AdjNetMWH float NULL,
	NonFuelVarOpex real NULL)
INSERT @Units (Refnum, AdjNetMWH, NonFuelVarOpex)
SELECT t.Refnum, (SELECT AdjNetMWH FROM GenerationTotCalc gtc WHERE gtc.Refnum = t.Refnum AND gtc.AdjNetMWH > 0),
	(SELECT ISNULL(STVar,0) - ISNULL(STFuelCost,0) FROM OpexCalc WHERE OpexCalc.Refnum = t.Refnum AND OpexCalc.DataType = 'MWH')
FROM TSort t WHERE t.SiteID = @SiteID

-- Get the cost for fuel for each period
DECLARE @FuelCost TABLE (
	Refnum varchar(12) NOT NULL,
	Period smalldatetime NOT NULL,
	TotCostKUS real NULL,
	MBTU float NULL,
	MBTULHV float NULL)
INSERT @FuelCost (Refnum, Period, TotCostKUS, MBTU, MBTULHV)
SELECT Refnum, Period, SUM(TotCostKUS), SUM(MBTU), SUM(MBTULHV)
FROM (
	SELECT Refnum, Period, TotCostKUS, MBTU, MBTULHV
	FROM Fuel WHERE Refnum IN (SELECT Refnum FROM @Units)
	UNION ALL
	SELECT Refnum, Period, TotCostKUS, MBTU, MBTULHV 
	FROM Coal WHERE Refnum IN (SELECT Refnum FROM @Units)) x
GROUP BY Refnum, Period

UPDATE VarCostCalc SET
	FuelCostMWH = CASE WHEN VarCostCalc.AdjNetMWH > 0 THEN fc.TotCostKUS*1000/VarCostCalc.AdjNetMWH END, 
	VarCostMWH = CASE WHEN VarCostCalc.AdjNetMWH > 0 THEN fc.TotCostKUS*1000/VarCostCalc.AdjNetMWH + u.NonFuelVarOpex END,
    TotMBTU = ISNULL(fc.MBTU, 0), 
    FuelCostMBTU = CASE WHEN fc.MBTU > 0 THEN fc.TotCostKUS*1000/fc.MBTU END,
	TotMBTULHV = ISNULL(fc.MBTULHV, 0), 
	FuelCostMBTULHV = CASE WHEN fc.MBTULHV > 0 THEN fc.TotCostKUS*1000/fc.MBTULHV END
FROM VarCostCalc INNER JOIN @Units u ON u.Refnum = VarCostCalc.Refnum
LEFT JOIN @FuelCost fc ON fc.Refnum = u.Refnum AND fc.Period = VarCostCalc.Period

-- Quarterly Heat Rates
DECLARE @QHeatRates TABLE (
	Refnum varchar(12) NOT NULL, 
	Quarter tinyint NOT NULL,
	HeatRate real NULL,
	HeatRateLHV real NULL,
	AvgFuelCostMBTU real NULL)
INSERT @QHeatRates (Refnum, Quarter, HeatRate, HeatRateLHV, AvgFuelCostMBTU)
SELECT vcc.Refnum, Quarter = DATEPART(qq, vcc.Period), 
	HeatRate = SUM(TotMBTU)/SUM(vcc.AdjNetMWH)*1000,
	HeatRateLHV = SUM(TotMBTULHV)/SUM(vcc.AdjNetMWH)*1000,
	AvgFuelCostMBTU = GlobalDB.dbo.WtAvg(FuelCostMBTU,TotMBTU)
FROM VarCostCalc vcc INNER JOIN @Units u ON u.Refnum = vcc.Refnum
WHERE vcc.AdjNetMWH > 0 AND u.AdjNetMWH > 0
GROUP BY vcc.Refnum, DATEPART(qq, Period), u.AdjNetMWH
HAVING SUM(vcc.AdjNetMWH)/u.AdjNetMWH > 0.05

-- If any quarters did not run enough to qualify, then apply the heat rate and avg cost from the quarter after.
-- If the fourth quarter is missing, then use the last quarter that we have data for.
IF EXISTS (SELECT * FROM @Units WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates WHERE Quarter = 4))
	INSERT @QHeatRates (Refnum, Quarter, HeatRate, HeatRateLHV, AvgFuelCostMBTU)
	SELECT Refnum, 1, HeatRate, HeatRateLHV, AvgFuelCostMBTU
	FROM @QHeatRates a WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates b WHERE b.Quarter = 4)
	AND a.Quarter = (SELECT MAX(Quarter) FROM @QHeatRates c WHERE c.Refnum = a.Refnum AND c.Quarter < 4)
IF EXISTS (SELECT * FROM @Units WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates WHERE Quarter = 3))
	INSERT @QHeatRates (Refnum, Quarter, HeatRate, HeatRateLHV, AvgFuelCostMBTU)
	SELECT Refnum, 3, HeatRate, HeatRateLHV, AvgFuelCostMBTU
	FROM @QHeatRates a WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates b WHERE b.Quarter = 3)
	AND a.Quarter = 4
IF EXISTS (SELECT * FROM @Units WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates WHERE Quarter = 2))
	INSERT @QHeatRates (Refnum, Quarter, HeatRate, HeatRateLHV, AvgFuelCostMBTU)
	SELECT Refnum, 2, HeatRate, HeatRateLHV, AvgFuelCostMBTU
	FROM @QHeatRates a WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates b WHERE b.Quarter = 2)
	AND a.Quarter = 3
IF EXISTS (SELECT * FROM @Units WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates WHERE Quarter = 1))
	INSERT @QHeatRates (Refnum, Quarter, HeatRate, HeatRateLHV, AvgFuelCostMBTU)
	SELECT Refnum, 1, HeatRate, HeatRateLHV, AvgFuelCostMBTU
	FROM @QHeatRates a WHERE Refnum NOT IN (SELECT Refnum FROM @QHeatRates b WHERE b.Quarter = 1)
	AND a.Quarter = 2

UPDATE VarCostCalc SET
	AdjFuelCostMWH = qhr.HeatRate * ISNULL(FuelCostMBTU, qhr.AvgFuelCostMBTU)/1000,
	VarCostMWH = qhr.HeatRate * ISNULL(FuelCostMBTU, qhr.AvgFuelCostMBTU)/1000+ u.NonFuelVarOpex
FROM VarCostCalc INNER JOIN @QHeatRates qhr ON qhr.Refnum = VarCostCalc.Refnum AND DATEPART(qq, VarCostCalc.Period) = qhr.Quarter
INNER JOIN @Units u ON u.Refnum = VarCostCalc.Refnum
