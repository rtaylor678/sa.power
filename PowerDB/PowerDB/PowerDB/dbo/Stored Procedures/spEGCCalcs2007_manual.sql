﻿-- =============================================
-- Author:		Dennis Burdett
-- Create date: 05/12/2009
-- Description:	Calculates EGC based on Rod's new method with the raw data being passed in from external software
-- =============================================
create PROCEDURE [dbo].[spEGCCalcs2007_manual] 
	-- Add the parameters for the stored procedure here
	@refnum varchar(12) = null,
	@year int = 0,
	@DEBUG bit = 0,
	@EGCTechnology varchar(15), 
	@EGCRegion varchar(20),
	@CTG_NDC real, 
	@NDC real, 
	@FuelType varchar(8), 
	@CoalMBTU real,
	@CoalSulfur real, 
	@CoalAsh real, 
	@OilSulfur real,
	@OilAsh real, 
	@NumUnitsAtSite real, 
	@OilMBTU real, 
	@GasMBTU real, 
	@CoalNDC real, 
	@OilNDC real,
	@GasNDC real,
	@CTGs int, 
	@ScrubbersYN int, 
	@PrecBagYN int, 
	@AdjNetMWH2Yr real, 
	@TotCashLessFuelEm real, 
	@Age real, 
	@ElecGenStarts real
AS
BEGIN
	SET NOCOUNT ON;
	SET @refnum = rtrim(@refnum)
print 'inside spEGCCalcs2007_manual with refnum=[' + @refnum + ']'

	DECLARE @CombinedCycle char 
	DECLARE @Coeff_CTGs real, @Coeff_Scrubbers real, @Coeff_PrecBag real, @Coeff_MwhperM real 
	DECLARE @Coeff_ScalingFactor real, @Coeff_Starts real, @Coeff_MwhperM_Sul real
	DECLARE @Coeff_MwhperM_Ash real, @Coeff_Age real, @Coeff_Coal real, @Coeff_Oil real, @Coeff_Gas real 
	DECLARE @Coeff_CTG real
	DECLARE @Pwr_CTGs real, @Pwr_Scrubbers real, @Pwr_PrecBag real, @Pwr_Coal real, @Pwr_Oil real
	DECLARE @Pwr_Gas real, @Pwr_CTG real, @Pwr_MwhperM real, @Pwr_ScalingFactor real, @Pwr_Starts real
	DECLARE @Pwr_MwhperM_Sul real, @Pwr_MwhperM_Ash real, @Pwr_Age real

	DECLARE @Sulfur real, @calc_MwhM_Ash real, @calc_Age real, @calc_Mwh_M real, @calc_Mwh_M_Sul real, @AdjPerSulfur real
	DECLARE @calc_NoScrubbers real, @calc_NoCTG real, @calc_Coal real, @calc_Oil real, @calc_Gas real 
	DECLARE @calc_CTG real, @calc_EGC real, @AshPcnt real, @CoalPcnt real, @OilPcnt real, @GasPcnt real

	DECLARE @calc_Grp_NoCTGs real, @calc_Grp_Scrubbers real, @calc_Grp_Precip real, @calc_Grp_NDCCoal real, @calc_Grp_NDCOil real 
	DECLARE @calc_Grp_NDCGas real, @calc_Grp_NDCCTG real, @calc_Grp_MWh real, @calc_Grp_NoUnits real, @calc_Grp_Starts real 
	DECLARE @calc_Grp_Age real, @calc_Grp_Ash real, @calc_Grp_Sulfur real


	--IF @year < 2007
	--	return

	-- run stored proc to try to fill in the EGCTechnology field in case this is the 1st time the unit has been calc'd
	--EXEC spUpdateEGCTechnologyForRefnum @Refnum = @refnum
	
	set @EGCTechnology=rtrim(@EGCTechnology)
	-- get the base required fields
	--SELECT @EGCRegion=rtrim(EGCRegion)  
	--	FROM dbo.tsort 
	--	WHERE refnum=@refnum
	IF (@EGCRegion is null) or (@EGCRegion = '')
		return

	IF (@EGCTechnology is null) or (@EGCTechnology = '')
		return

	SELECT @CombinedCycle = CombinedCycle
		FROM Breaks WHERE Refnum = @Refnum

	--IF RIGHT(@Refnum,1) = 'P' 
	--BEGIN
	--	SET @EGCTechnology='CARRYOVER'
	--END

	-- get the Coefficients
	SELECT @Coeff_CTGs = CTGs, @Coeff_Scrubbers = Scrubbers, @Coeff_PrecBag = PrecBag, @Coeff_MwhperM = MwhperM, 
			@Coeff_ScalingFactor = ScalingFactor, @Coeff_Starts = Starts, @Coeff_MwhperM_Sul=MwhperM_Sul, 
			@Coeff_MwhperM_Ash = MwhperM_Ash, @Coeff_Age = Age, @Pwr_CTGs = Pwr_CTGs, 
			@Pwr_Scrubbers = Pwr_Scrubbers, @Pwr_PrecBag = Pwr_PrecBag, @Pwr_Coal = Pwr_Coal, 
			@Pwr_Oil = Pwr_Oil, @Pwr_Gas = Pwr_Gas, @Pwr_CTG = Pwr_CTG, @Pwr_MwhperM = Pwr_MwhperM, 
			@Pwr_ScalingFactor = Pwr_ScalingFactor, @Pwr_Starts = Pwr_Starts,	
			@Pwr_MwhperM_Sul = Pwr_MwhperM_Sul, @Pwr_MwhperM_Ash = Pwr_MwhperM_Ash, @Pwr_Age = Pwr_Age
		FROM   PowerGlobal.dbo.EGCCoefficients 
		WHERE Year=@year and EGCTechnology=@EGCTechnology

	SELECT @Coeff_Coal=Coal, @Coeff_Oil=Oil, @Coeff_Gas=Gas, @Coeff_CTG=CTG 
		FROM   PowerGlobal.dbo.EGCCoefficientsRegions 
		WHERE Year=@year and EGCTechnology=@EGCTechnology and Region=@EGCRegion

	--SELECT @CTGs = ISNULL(ctgs,0), @CTG_NDC = ISNULL(ctg_ndc,0), @NDC = ISNULL(ndc,0), 
	--		@CoalNDC = ISNULL(Coal_NDC,0), @OilNDC = ISNULL(Oil_NDC,0), @GasNDC = ISNULL(Gas_NDC,0), 
	--		@PrecBagYN = ISNULL(PrecBagYN,0), @ScrubbersYN = ISNULL(ScrubbersYN,0), 
	--		@NumUnitsAtSite = ISNULL(NumUnitsAtSite,0)
	--	FROM dbo.tsort 
	--	WHERE refnum=@refnum

	IF (@CTG_NDC IS NULL)
		SET @CTG_NDC = 0
	ELSE
		BEGIN
			IF (@CTGs > 0)
				SET @CTG_NDC = @CTG_NDC --/ @CTGs
		END

	--SELECT @FuelType=fueltype 
	--	FROM dbo.FuelTotCalc  
	--	WHERE refnum=@refnum

	--SELECT @Age=max(age) 
	--	FROM dbo.nercturbine 
	--	WHERE refnum=@refnum

	--SELECT @TotCashLessFuelEm = ISNULL(TotCashLessFuelEm,0)
	--	FROM   dbo.OpExCalc
	--	WHERE  (Refnum = @refnum) AND (DataType = 'ADJ')

	--SELECT @ElecGenStarts=ISNULL(SUM(TotalOpps),0) 
	--	FROM dbo.StartsAnalysis sa 
	--	INNER JOIN NERCTurbine nt ON nt.Refnum = sa.Refnum AND nt.TurbineID = sa.TurbineID 
	--	WHERE nt.TurbineType IN ('CTG', 'STG', 'CST') AND sa.Refnum = @Refnum
	--SELECT @AdjNetMWH2Yr = ISNULL(AdjNetMWH2Yr,0)/1000000 
	--	FROM dbo.GenerationTotCalc 
	--	WHERE Refnum = @Refnum

	--SELECT @CoalSulfur = ISNULL(SulfurPcnt,0), @CoalAsh = ISNULL(AshPcnt,0) 
	--	FROM CoalTotCalc
	--	WHERE Refnum = @Refnum

	--SELECT @OilSulfur = ISNULL(Sulfur,0), @OilAsh = 0.1 
	--	FROM FuelProperties 
	--	WHERE Refnum = @Refnum
	IF @OilSulfur IS NULL
		SELECT @OilSulfur = 2.3, @OilAsh = 0.1
	SELECT @Sulfur = (ISNULL(@CoalSulfur,0)*@CoalNDC + ISNULL(@OilSulfur,0)*@OilNDC)/@NDC
	SELECT @AshPcnt = (ISNULL(@CoalAsh,0)*@CoalNDC + ISNULL(@OilAsh,0)*@OilNDC)/@NDC

	--SELECT @OilMBTU = SUM(MBTU) 
	--	FROM Fuel 
	--	WHERE Refnum = @Refnum AND FuelType IN ('Jet', 'FOil', 'Diesel') AND TurbineID IN ('STG','Site')
	--IF @OilMBTU IS NULL 
	--	SELECT @OilMBTU = 0

	--SELECT @GasMBTU = SUM(MBTU) 
	--	FROM Fuel 
	--	WHERE Refnum = @Refnum AND FuelType IN ('NatGas', 'OffGas', 'H2Gas') AND TurbineID IN ('STG','Site')
	--IF @GasMBTU IS NULL 
	--	SELECT @GasMBTU = 0

	--SELECT @CoalMBTU = SUM(MBTU) 
	--	FROM Coal 
	--	WHERE Refnum = @Refnum

	IF (@OilMBTU + @GasMBTU) > 0
		SELECT @OilPcnt = @OilMBTU/(@OilMBTU + @GasMBTU)
	ELSE
		SELECT @OilPcnt = 0

	IF @OilPcnt < 0.1
		SELECT @OilPcnt = 0

	IF @OilPcnt > 0.9
		SELECT @OilPcnt = 1

	IF (@OilMBTU + @GasMBTU) > 0
		SELECT @GasPcnt = @GasMBTU/(@OilMBTU + @GasMBTU)
	ELSE
		SELECT @GasPcnt = 0

	IF @GasPcnt < 0.1
		SELECT @GasPcnt = 0

	IF @GasPcnt > 0.9
		SELECT @GasPcnt = 1

	IF @CoalMBTU > 0
		SELECT @CoalPcnt = @CoalMBTU/(@CoalMBTU + @OilMBTU + @GasMBTU)
	ELSE
		SELECT @CoalPcnt = 0

	IF @CoalPcnt < 0.1
		SELECT @CoalPcnt = 0

	IF @CoalPcnt > 0.9
		SELECT @CoalPcnt = 1

	-- do actual Calculations now
	SELECT @AdjPerSulfur = 
		CASE @FuelType
			WHEN 'Oil' THEN @OilSulfur
			WHEN 'Gas' THEN 0
			WHEN 'Coal' THEN @CoalSulfur
		END

	DECLARE	@ctg_float float
	-- 09/23/08 DB -> had to add this to keep the POWER() function from truncating the returned value since CTG is an int
	set @ctg_float = @ctgs

	SET @calc_NoCTG = POWER(@ctg_float, @Pwr_CTGs)
	SET @calc_MwhM_Ash = POWER((@AdjNetMWH2Yr*@AshPcnt), @Pwr_MwhperM_Ash)
	SET @calc_Age = POWER(@Age, @Pwr_Age)
	SET @calc_Mwh_M = POWER(@AdjNetMWH2Yr, @Pwr_MwhperM)
	SET @calc_Mwh_M_Sul = POWER((@AdjNetMWH2Yr*@AdjPerSulfur), @Pwr_MwhperM_Sul)
	SET @calc_NoScrubbers = POWER((@ScrubbersYN*@AdjNetMWH2Yr*@CoalPcnt), @Pwr_Scrubbers)
	SET @calc_Coal = POWER(@CoalNDC, @Pwr_Coal)
	SET @calc_Gas = POWER(@GasNDC, @Pwr_Gas)
	SET @calc_Oil = POWER(@OilNDC, @Pwr_Oil)
	SET @calc_CTG = POWER(@CTG_NDC, @Pwr_CTG)

	SET @calc_Grp_NoCTGs = (@calc_NoCTG * @Coeff_CTGs) 
	SET @calc_Grp_Scrubbers = (@calc_NoScrubbers * @Coeff_Scrubbers) 
	SET @calc_Grp_Precip = (@PrecBagYN * @Coeff_PrecBag) 
	SET @calc_Grp_NDCCoal = (@calc_Coal * @Coeff_Coal)
	SET @calc_Grp_NDCOil = (@calc_Oil * @Coeff_Oil)
	SET @calc_Grp_NDCGas = (@calc_Gas * @Coeff_Gas)
	SET @calc_Grp_NDCCTG = (@calc_CTG * @Coeff_CTG)
	SET @calc_Grp_MWh = (@calc_Mwh_M * @Coeff_MwhperM)
	SET @calc_Grp_NoUnits = (@NumUnitsAtSite * @Coeff_ScalingFactor)
	SET @calc_Grp_Starts = (@ElecGenStarts * @Coeff_Starts)
	SET @calc_Grp_Sulfur = (@calc_Mwh_M_Sul * @Coeff_MwhperM_Sul)
	SET @calc_Grp_Ash = (@calc_MwhM_Ash * @Coeff_MwhperM_Ash)
	SET @calc_Grp_Age = (@Calc_Age * @Coeff_Age)

	SET @calc_EGC = @calc_Grp_NoCTGs + @calc_Grp_Scrubbers + @calc_Grp_Precip + @calc_Grp_NDCCoal + @calc_Grp_NDCOil + @calc_Grp_NDCGas + @calc_Grp_NDCCTG + 
		@calc_Grp_MWh + @calc_Grp_NoUnits + @calc_Grp_Starts + @calc_Grp_Sulfur + @calc_Grp_Ash + @calc_Grp_Age

	-- Build the "REAL" EGCCalc record. The one that will be used.
	-- if DEBUG then ONLY display the value do NOT update any database records
	--IF (@DEBUG = 0)
	--BEGIN
		--DELETE FROM EGCCalc WHERE Refnum = @Refnum
		--INSERT INTO EGCCalc (Refnum, Method, Technology, EGC, CTGs, CTG_NDC, NDC, AdjNetMWH, Sulfur, AshPcnt, 
		--	FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, 
		--	ScrubbersYN,  PrecBagYN)
		--VALUES (@Refnum, '2007', @EGCTechnology, @calc_EGC, @CTGs, @CTG_NDC, @NDC, @AdjNetMWH2Yr, @Sulfur, @AshPcnt,
		--	@FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, 
		--	@ScrubbersYN, @PrecBagYN)

		---- Go update all of the EGC fields spread thoughout the database.
		--UPDATE GenerationTotCalc SET EGC = @calc_EGC WHERE Refnum = @Refnum
		--UPDATE Gensum SET EGC = @calc_EGC WHERE Refnum = @Refnum

		--UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN TotAnnOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

		--UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

		--UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHProjCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHExclProj/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintExclProj/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @calc_EGC > 0 THEN AnnLTSACost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

		--UPDATE Pers SET TotEffPersEGC = CASE WHEN @calc_EGC > 0 THEN TotEffPers*1000/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
		--UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @calc_EGC > 0 THEN TotEffPers*1000/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

		--UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

		--EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @calc_EGC
	--END
	--ELSE
	BEGIN
		SELECT @calc_EGC as 'calc_EGC', @calc_NoCTG as 'calc_NoCTG', @calc_MwhM_Ash as 'calc_MwhM_Ash', 
			@calc_Age as 'calc_Age', @calc_Mwh_M as 'calc_Mwh_M', @calc_Mwh_M_Sul as 'calc_Mwh_M_Sul', 
			@calc_NoScrubbers as 'calc_NoScrubbers', @calc_Coal as 'calc_Coal', @calc_Oil as 'calc_Oil', 
			@calc_Gas as 'calc_Gas', @calc_CTG as 'calc_CTG', @Sulfur as 'Sulfur',

			@Coeff_CTGs as 'Coeff_CTGs', @Coeff_Scrubbers as 'Coeff_Scrubbers', @Coeff_PrecBag as 'Coeff_PrecBag', 
			@Coeff_MwhperM as 'Coeff_MwhperM', @Coeff_ScalingFactor as 'Coeff_ScalingFactor', 
			@Coeff_Starts as 'Coeff_Starts', @Coeff_MwhperM_Sul as 'Coeff_MwhperM_Sul', @Coeff_Age as 'Coeff_Age', 
			@Coeff_MwhperM_Ash as 'Coeff_MwhperM_Ash', @Coeff_Coal as 'Coeff_Coal', @Coeff_Oil as 'Coeff_Oil',
			@Coeff_Gas as 'Coeff_Gas', @Coeff_CTG as 'Coeff_CTG', 

			@Pwr_CTGs as 'Pwr_CTGs', @Pwr_Scrubbers as 'Pwr_Scrubbers', @Pwr_PrecBag as 'Pwr_PrecBag', 
			@Pwr_Coal as 'Pwr_Coal', @Pwr_Oil as 'Pwr_Oil', @Pwr_Gas as 'Pwr_Gas', @Pwr_CTG as 'Pwr_CTG', 
			@Pwr_MwhperM as 'Pwr_MwhperM', @Pwr_ScalingFactor as 'Pwr_ScalingFactor', @Pwr_Starts as 'Pwr_Starts', 
			@Pwr_MwhperM_Sul as 'Pwr_MwhperM_Sul', @Pwr_MwhperM_Ash as 'Pwr_MwhperM_Ash', @Pwr_Age as 'Pwr_Age', 

			@Coeff_MwhperM  as 'Coeff_MwhperM', @Coeff_ScalingFactor as 'Coeff_ScalingFactor', 
			@Coeff_Starts as 'Coeff_Starts', @Coeff_MwhperM_Sul as 'Coeff_MwhperM_Sul', 
			@Coeff_MwhperM_Ash as 'Coeff_MwhperM_Ash', @Coeff_Age as 'Coeff_Age', @Coeff_Coal as 'Coeff_Coal', 
			@Coeff_Oil as 'Coeff_Oil', @Coeff_Gas as 'Coeff_Gas', @Coeff_CTG as 'Coeff_CTG', 

			@year as 'year', @refnum as 'refnum', @EGCTechnology as 'EGCTechnology', @EGCRegion as 'EGCRegion', 

			@CTGs as 'CTGs', @CTG_NDC as 'CTG_NDC', @NDC as 'NDC', @CoalNDC as 'CoalNDC', @OilNDC as 'OilNDC', 
			@GasNDC as 'GasNDC', @ElecGenStarts as 'ElecGenStarts', @AdjNetMWH2Yr as 'AdjNetMWH2Yr', @Age as 'Age', 
			@AshPcnt as 'AshPcnt', @FuelType as 'FuelType', @CoalPcnt as 'CoalPcnt', @CoalMBTU as 'CoalMBTU', 
			@OilPcnt as 'OilPcnt', @GasPcnt as 'GasPcnt', @CoalSulfur as 'CoalSulfur', @CoalAsh as 'CoalAsh', 
			@OilSulfur as 'OilSulfur', @OilAsh as 'OilAsh', @NumUnitsAtSite as 'NumUnitsAtSite', 
			@OilMBTU as 'OilMBTU', @GasMBTU as 'GasMBTU',  @ScrubbersYN as 'ScrubbersYN', 
			@PrecBagYN as 'PrecBagYN', @TotCashLessFuelEm as 'TotCashLessFuelEm',
			@calc_Grp_NoCTGs as 'calc_Grp_NoCTGs', @calc_Grp_Scrubbers as 'calc_Grp_Scrubbers', @calc_Grp_Precip as 'calc_Grp_Precip', 
			@calc_Grp_NDCCoal as 'calc_Grp_NDCCoal', @calc_Grp_NDCOil as 'calc_Grp_NDCOil', @calc_Grp_NDCGas as 'calc_Grp_NDCGas', 
			@calc_Grp_NDCCTG as 'calc_Grp_NDCCTG', @calc_Grp_MWh as 'calc_Grp_MWh', @calc_Grp_NoUnits as 'calc_Grp_NoUnits', @calc_Grp_Starts as 'calc_Grp_Starts',
			@calc_Grp_Age as 'calc_Grp_Age', @calc_Grp_Ash as 'calc_Grp_Ash', @calc_Grp_Sulfur as 'calc_Grp_Sulfur'
			
	END
END
