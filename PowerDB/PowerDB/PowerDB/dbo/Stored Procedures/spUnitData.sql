﻿-- =============================================
-- Author:		SW
-- Create date: 1/30/15
-- Description:	Get unit data for loading in the new unit builder
-- =============================================
CREATE PROCEDURE spUnitData @SiteID char(10)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

  select distinct t.siteid, s.CompanyID, t.CompanyName, s.SiteName, ci.PlantName, nt.Util_Code, clu.CurrencyCode, Metric = CASE t.Metric WHEN 0 THEN 'N' ELSE 'Y' END, HeatValue = case when c.ReportedLHV = 'Y' THEN 'LHV' else 'HHV' end, Steam = CASE WHEN ISNULL(ss.ReportedMBTU,'Y') = 'Y' THEN 'Energy' ELSE 'Steam' END, s.StudyYear from StudySites s left join TSort t on t.SiteID = s.SiteID left join ClientInfo ci on ci.SiteID = s.SiteID left join NERCTurbine nt on nt.Refnum = t.Refnum left join Currency_LU clu on clu.CurrencyID = t.CurrencyID left join Coal c on c.Refnum = t.Refnum left join SteamSales ss on ss.Refnum = t.Refnum where s.SiteID = @SiteID
END
