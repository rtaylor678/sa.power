﻿CREATE PROCEDURE procLoadDDObjects AS
DELETE FROM DD_Relationships
WHERE NOT EXISTS (SELECT o.* FROM sysobjects o, DD_Objects d
	WHERE o.name = d.ObjectName
	AND (d.ObjectID = DD_Relationships.ObjectA OR d.ObjectID = DD_Relationships.ObjectB))
DELETE FROM DD_Columns
WHERE NOT EXISTS (SELECT o.* FROM sysobjects o, DD_Objects d
	WHERE o.name = d.ObjectName AND d.ObjectID = DD_Columns.ObjectID)
DELETE FROM DD_Objects
WHERE NOT EXISTS (SELECT * FROM sysobjects
	 WHERE name = DD_Objects.ObjectName)
INSERT INTO DD_Objects (ObjectName)
SELECT Name FROM sysobjects
WHERE type in ('U', 'V')
 AND Name NOT IN (SELECT ObjectName FROM DD_Objects)
