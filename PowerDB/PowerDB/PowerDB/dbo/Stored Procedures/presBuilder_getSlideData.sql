﻿-- =============================================
-- Author:		rvb
-- Create date: <Create Date,,>
-- Description:	Just testing PresBuilder Data Access.  This is temporary.
-- =============================================
CREATE PROCEDURE presBuilder_getSlideData 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT TOP(50)
		SlideDataID,
		SlideChartID,
		AxisNumber,
		AxisTitle,
		DataSource,
		Filter1, 
		SlideColorGroup,
		Filter2,
		Filter3,
		DataType,
		Filter4,
		Filter5,
		BarGapWidth,
		LineFormat,
		Filter6,
		Filter7,
		Multi
	FROM SlideData

END
