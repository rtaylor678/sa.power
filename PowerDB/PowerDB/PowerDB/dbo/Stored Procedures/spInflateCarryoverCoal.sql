﻿CREATE PROCEDURE [dbo].[spInflateCarryoverCoal](@Refnum char(12), @InflFactor real)
AS

UPDATE Coal
SET MineCostTon = MineCostTon * @InflFactor,
	TransCostTon = TransCostTon * @InflFactor,
	OnsitePrepCostTon = OnsitePrepCostTon * @InflFactor,
	MineCostTonLocal = MineCostTonLocal * @InflFactor,
	TransCostTonLocal = TransCostTonLocal * @InflFactor,
	OnSitePrepCostTonLocal = OnSitePrepCostTonLocal * @InflFactor,
	TransCostTM = TransCostTM * @InflFactor,
	TotCostTon = TotCostTon * @InflFactor,
	TotCostKUS = TotCostKUS * @InflFactor

WHERE Refnum = @Refnum

UPDATE OpexLocal
SET PurSolid = (SELECT SUM(Tons*(ISNULL(MineCostTonLocal, 0) + ISNULL(TransCostTonLocal, 0))/1000) FROM Coal WHERE Coal.Refnum = OpexLocal.Refnum)
WHERE Refnum = @Refnum

--GO
