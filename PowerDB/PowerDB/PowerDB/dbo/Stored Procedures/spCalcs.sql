﻿CREATE PROC [dbo].[spCalcs](@SiteID SiteID)
AS
	DECLARE @result int, @msgSource varchar(12)
	SET NOCOUNT ON

	EXEC CalcInitial @SiteID
	EXEC CalcTurbineNERCFactors @SiteID
	EXEC CalcComponentEUF @SiteID
	EXEC spLHVCalcs @SiteID -- can be called anytime before this point
	EXEC spAllocateSpares @SiteID -- can be called anytime before this point
	EXEC spCurrencyConv @SiteID
	
	EXEC CalcMaintExp @SiteID
	EXEC CalcCoalCost @SiteID
	EXEC CalcSteam @SiteID
	EXEC CalcMaint @SiteID
	EXEC CalcPers @SiteID
	EXEC CalcCost @SiteID
	EXEC CalcVarCost @SiteID
	EXEC CalcSummary @SiteID

	-- These procedures are unit based and complex, 
	-- so we will calculate them one unit at a time.
	DECLARE cUnits CURSOR LOCAL FAST_FORWARD
	FOR	SELECT Refnum FROM TSort WHERE SiteID = @SiteID
	DECLARE @Refnum Refnum
	OPEN cUnits
	FETCH NEXT FROM cUnits INTO @Refnum
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC spUpdateBreaks @Refnum
		EXEC spCogenCalc @Refnum
		EXEC spCommUnavail @Refnum
		EXEC spEGCCalcs @Refnum
		EXEC spUpdateGaps @Refnum
		FETCH NEXT FROM cUnits INTO @Refnum
	END
	CLOSE cUnits
	DEALLOCATE cUnits
