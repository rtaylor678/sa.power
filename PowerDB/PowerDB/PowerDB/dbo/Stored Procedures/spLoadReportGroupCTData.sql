﻿


CREATE PROC [dbo].[spLoadReportGroupCTData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

	--remove the existing record in the table
	DELETE FROM ReportGroupCTData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


			INSERT ReportGroupCTData (RefNum, 
				ReportTitle, 
				ListName, 
				NMC,
				PriorNetGWH,
				NetGWH,
				ServiceHours,
				NCF2Yr,
				NOF2Yr,
				FiringTemp,
				MaintCostPerStart,
				AvgStartsPerCT,
				EquivStarts,
				EquivRunHrs,
				StartSuccessPct,
				ForcedOutagePct,
				MaintenancePct,
				StartupFailurePct,
				DeratedPct,
				HHVHeatRateKWH,
				LHVHeatRateKWH,
				HrsBetweenCombInspection,
				HrsBetweenHGPInspection,
				HrsBetweenMajorOverhaul,
				StartsBetweenCombInspection,
				StartsBetweenHGPInspection,
				StartsBetweenMajorOverhaul)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				NMC = AVG(CASE WHEN ct.NMC <> 0 THEN ct.NMC END),
				PriorNetGWH = AVG(CASE WHEN ct.PriorNetMWH <> 0 THEN ct.PriorNetMWH END)/1000,
				NetGWH = AVG(CASE WHEN ct.NetMWH <> 0 THEN ct.NetMWH END)/1000,
				ServiceHours = AVG(CASE WHEN ct.ServiceHrs <> 0 THEN ct.ServiceHrs END),
				NCF2Yr = GlobalDB.dbo.WtAvg(ct.NCF2Yr, ct.WPH2Yr),
				NOF2Yr = GlobalDB.dbo.WtAvg(ct.NOF2Yr, ct.NOF2YrWtFactor),
				FiringTemp = AVG(cd.FiringTemp),
				MaintCostPerStart = SUM(CASE WHEN m.MaintCostPerStart <> 0 THEN m.MaintCostPerStart END)/SUM(CASE WHEN m.MaintCostPerStart * m.AvgTotStarts <> 0 THEN m.AvgTotStarts END),
				AvgStartsPerCT = AVG(m.AvgTotStarts),
				EquivStarts = SUM(CASE WHEN l.CurrEquivStarts <> 0 THEN l.CurrEquivStarts END)/SUM(CASE WHEN l.CurrEquivStarts * l.Currtotstarts <> 0 THEN l.Currtotstarts END),
				EquivRunHrs = SUM(CASE WHEN l.currEquivRunHrs <> 0 THEN l.currEquivRunHrs END)/SUM(CASE WHEN l.currEquivRunHrs * l.CurrTotRunHrs <> 0 THEN l.CurrTotRunHrs END),
				StartSuccessPct = GlobalDB.dbo.WtAvg(ct.SuccessPcnt,ct.TotalOpps),
				ForcedOutagePct = GlobalDB.dbo.WtAvg(ct.ForcedPcnt,ct.TotalOpps),
				MaintenancePct = GlobalDB.dbo.WtAvg(ct.MaintPcnt,ct.TotalOpps),
				StartupFailurePct = GlobalDB.dbo.WtAvg(ct.StartupPcnt,ct.TotalOpps),
				DeratedPct = GlobalDB.dbo.WtAvg(ct.DeratePcnt,ct.TotalOpps),
				HHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(ct.HeatRate, ct.NetMWH),
				LHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(ct.HeatRateLHV, ct.NetMWH),
				HrsBetweenCombInspection = AVG(CASE WHEN crt.CombInspHrs <> 0 THEN crt.CombInspHrs END),
				HrsBetweenHGPInspection = AVG(CASE WHEN crt.HGPInspHrs <> 0 THEN crt.HGPInspHrs END),
				HrsBetweenMajorOverhaul = AVG(CASE WHEN crt.OverhaulInspHrs <> 0 THEN crt.OverhaulInspHrs END),
				StartsBetweenCombInspection = AVG(CASE WHEN crt.CombInspStarts <> 0 THEN crt.CombInspStarts END),
				StartsBetweenHGPInspection = AVG(CASE WHEN crt.HGPInspStarts <> 0 THEN crt.HGPInspStarts END),
				StartsBetweenMajorOverhaul = AVG(CASE WHEN crt.OverhaulInspStarts <> 0 THEN crt.OverhaulInspStarts END)

				FROM [PowerGlobal].[dbo].[RefList_LU] rlu
					inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
					inner join CTGSummary ct on ct.Refnum = rl.Refnum 
					LEFT JOIN CTGData cd on cd.Refnum = ct.Refnum and cd.TurbineID = ct.TurbineID
					LEFT JOIN CTGMaintPerStart m on m.Refnum = ct.Refnum 
					LEFT JOIN LTSA l on l.Refnum = ct.Refnum and l.TurbineID = ct.TurbineID 
					LEFT JOIN CTGMaintByRefnumAndTurbine crt ON crt.Refnum = ct.Refnum and crt.TurbineID = ct.TurbineID
					INNER JOIN Breaks b ON b.Refnum = ct.Refnum
					inner join _RankView r on r.Refnum = ct.Refnum 
				WHERE rlu.listname = @ListName 
					and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
					and r.Variable = @tiledesc 
					and r.ListName = @ListName 
					and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
					and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 'ct.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''
				
			--and this is the actual query being run
			EXEC ('INSERT ReportGroupCTData (RefNum,
					ReportTitle,
					ListName,
					NMC,
					PriorNetGWH,
					NetGWH,
					ServiceHours,
					NCF2Yr,
					NOF2Yr,
					FiringTemp,
					MaintCostPerStart,
					AvgStartsPerCT,
					EquivStarts,
					EquivRunHrs,
					StartSuccessPct,
					ForcedOutagePct,
					MaintenancePct,
					StartupFailurePct,
					DeratedPct,
					HHVHeatRateKWH,
					LHVHeatRateKWH,
					HrsBetweenCombInspection,
					HrsBetweenHGPInspection,
					HrsBetweenMajorOverhaul,
					StartsBetweenCombInspection,
					StartsBetweenHGPInspection,
					StartsBetweenMajorOverhaul)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					NMC = AVG(CASE WHEN ct.NMC <> 0 THEN ct.NMC END),
					PriorNetGWH = AVG(CASE WHEN ct.PriorNetMWH <> 0 THEN ct.PriorNetMWH END)/1000,
					NetGWH = AVG(CASE WHEN ct.NetMWH <> 0 THEN ct.NetMWH END)/1000,
					ServiceHours = AVG(CASE WHEN ct.ServiceHrs <> 0 THEN ct.ServiceHrs END),
					NCF2Yr = GlobalDB.dbo.WtAvg(ct.NCF2Yr, ct.WPH2Yr),
					NOF2Yr = GlobalDB.dbo.WtAvg(ct.NOF2Yr, ct.NOF2YrWtFactor),
					FiringTemp = AVG(cd.FiringTemp),
					MaintCostPerStart = SUM(CASE WHEN m.MaintCostPerStart <> 0 THEN m.MaintCostPerStart END)/SUM(CASE WHEN m.MaintCostPerStart * m.AvgTotStarts <> 0 THEN m.AvgTotStarts END),
					AvgStartsPerCT = AVG(m.AvgTotStarts),
					EquivStarts = SUM(CASE WHEN l.CurrEquivStarts <> 0 THEN l.CurrEquivStarts END)/SUM(CASE WHEN l.CurrEquivStarts * l.Currtotstarts <> 0 THEN l.Currtotstarts END),
					EquivRunHrs = SUM(CASE WHEN l.currEquivRunHrs <> 0 THEN l.currEquivRunHrs END)/SUM(CASE WHEN l.currEquivRunHrs * l.CurrTotRunHrs <> 0 THEN l.CurrTotRunHrs END),
					StartSuccessPct = GlobalDB.dbo.WtAvg(ct.SuccessPcnt,ct.TotalOpps),
					ForcedOutagePct = GlobalDB.dbo.WtAvg(ct.ForcedPcnt,ct.TotalOpps),
					MaintenancePct = GlobalDB.dbo.WtAvg(ct.MaintPcnt,ct.TotalOpps),
					StartupFailurePct = GlobalDB.dbo.WtAvg(ct.StartupPcnt,ct.TotalOpps),
					DeratedPct = GlobalDB.dbo.WtAvg(ct.DeratePcnt,ct.TotalOpps),
					HHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(ct.HeatRate, ct.NetMWH),
					LHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(ct.HeatRateLHV, ct.NetMWH),
					HrsBetweenCombInspection = AVG(CASE WHEN crt.CombInspHrs <> 0 THEN crt.CombInspHrs END),
					HrsBetweenHGPInspection = AVG(CASE WHEN crt.HGPInspHrs <> 0 THEN crt.HGPInspHrs END),
					HrsBetweenMajorOverhaul = AVG(CASE WHEN crt.OverhaulInspHrs <> 0 THEN crt.OverhaulInspHrs END),
					StartsBetweenCombInspection = AVG(CASE WHEN crt.CombInspStarts <> 0 THEN crt.CombInspStarts END),
					StartsBetweenHGPInspection = AVG(CASE WHEN crt.HGPInspStarts <> 0 THEN crt.HGPInspStarts END),
					StartsBetweenMajorOverhaul = AVG(CASE WHEN crt.OverhaulInspStarts <> 0 THEN crt.OverhaulInspStarts END)

				FROM CTGSummary ct
					LEFT JOIN CTGData cd on cd.Refnum = ct.Refnum and cd.TurbineID = ct.TurbineID
					LEFT JOIN CTGMaintPerStartWithTurbineByRefnum m on m.Refnum = ct.Refnum and m.TurbineID = ct.TurbineID 
					LEFT JOIN LTSA l on l.Refnum = ct.Refnum and l.TurbineID = ct.TurbineID 
					LEFT JOIN CTGMaintByRefnumAndTurbine crt ON crt.Refnum = ct.Refnum and crt.TurbineID = ct.TurbineID
					INNER JOIN Breaks b ON b.Refnum = ct.Refnum
					INNER JOIN TSort t on t.Refnum = ct.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = ct.Refnum' + @Turbine + 
				' WHERE ' + @Where)
			
		END

