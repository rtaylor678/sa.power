﻿



CREATE PROC [dbo].[spLoadReportGroupPersonnelCompAbsenceData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

	--remove the existing record in the table
	DELETE FROM ReportGroupPersonnelCompAbsenceData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


			INSERT ReportGroupPersonnelCompAbsenceData (RefNum, 
				ReportTitle, 
				ListName, 
				SiteEffPers,
				CentralEffPers,
				AGEffPers,
				ContractalEffPers,
				TotalEffPers,
				OCCCompensation,
				OCCBenefitPct,
				MPSCompensation,
				MPSBenefitPct,
				OCCOverTimePctOps,
				OCCOverTimePctNonOvhlMaint,
				OCCOverTimePctInspectors,
				OCCOverTimePctOvhlMaint,
				OCCOverTimePctAverageMaint,
				OCCOverTimePctTechnical,
				OCCOverTimePctAdmin,
				OCCOverTimePctSupport,
				OCCOverTimePctAverage,
				MPSOverTimePctOps,
				MPSOverTimePctNonOvhlMaint,
				MPSOverTimePctInspectors,
				MPSOverTimePctOvhlMaint,
				MPSOverTimePctAverageMaint,
				MPSOverTimePctTechnical,
				MPSOverTimePctAdmin,
				MPSOverTimePctSupport,
				MPSOverTimePctAverage,
				OCCEmpAbsence,
				OCCOnJobInjuries,
				OCCSickness,
				OCCLegal,
				OCCVacation,
				OCCHolidays,
				OCCExcused,
				OCCUnexcused,
				OCCSteamTotal,
				OCCCogenOnJobInjuries,
				OCCCogenHolidays,
				OCCCogenExcused,
				OCCCogenTotal,
				MPSEmpAbsence,
				MPSOnJobInjuries,
				MPSSickness,
				MPSLegal,
				MPSVacation,
				MPSHolidays,
				MPSExcused,
				MPSUnexcused,
				MPSSteamTotal,
				MPSCogenOnJobInjuries,
				MPSCogenHolidays,
				MPSCogenExcused,
				MPSCogenTotal)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				SiteEffPers = AVG(ISNULL(pstc.SiteEffPers,0)),
				CentralEffPers = AVG(ISNULL(pstc.CentralEffPers,0)),
				AGEffPers = AVG(ISNULL(pstc.AGEffPers,0)),
				ContractalEffPers = AVG(ISNULL(pstc.ContractEffPers,0)),
				TotalEffPers = AVG(CASE WHEN pstc.TotalEffPers <> 0 THEN pstc.TotalEffPers END),
				OCCCompensation = AVG(CASE WHEN gs.OCCAnnComp <> 0 THEN gs.OCCAnnComp END) * 0.001,
				OCCBenefitPct = GlobalDB.dbo.WtAvg(gs.OCCBenPcnt, oec.OCCWages),
				MPSCompensation = AVG(CASE WHEN gs.MPSAnnComp <> 0 THEN gs.MPSAnnComp END) * 0.001,
				MPSBenefitPct = GlobalDB.dbo.WtAvg(gs.MPSBenPcnt, oec.MPSWages),
				OCCOverTimePctOps = CASE WHEN SUM(per.OCCOperSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCOperSiteOVTHrs,0))/SUM(per.OCCOperSiteSTH) END*100,
				OCCOverTimePctNonOvhlMaint = CASE WHEN SUM(per.OCCNOHMaintInspSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCNOHMaintInspSiteOVTHrs,0))/SUM(per.OCCNOHMaintInspSiteSTH) END*100,
				OCCOverTimePctInspectors = CASE WHEN SUM(per.OCCInspSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCInspSiteOVTHrs,0))/SUM(per.OCCInspSiteSTH) END*100,
				OCCOverTimePctOvhlMaint = CASE WHEN SUM(per.OCCOHAdjSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCOHAdjSiteOVTHrs,0))/SUM(per.OCCOHAdjSiteSTH) END*100,
				OCCOverTimePctAverageMaint = GlobalDB.dbo.WtAvg(pstc.OCCSiteOVTPcntMaint, pstc.OCCSiteSTHMaint),
				OCCOverTimePctTechnical = CASE WHEN SUM(per.OCCTechSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCTechSiteOVTHrs,0))/SUM(per.OCCTechSiteSTH) END*100,
				OCCOverTimePctAdmin = CASE WHEN SUM(per.OCCAdminSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCAdminSiteOVTHrs,0))/SUM(per.OCCAdminSiteSTH) END*100,
				OCCOverTimePctSupport = CASE WHEN SUM(per.OCCNonMaintSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCNonMaintSiteOVTHrs,0))/SUM(per.OCCNonMaintSiteSTH) END*100,
				OCCOverTimePctAverage = GlobalDB.dbo.WtAvg(pstc.OCCSiteOVTPcntTotal, pstc.OCCSiteSTHTotal),
				MPSOverTimePctOps = CASE WHEN SUM(per.MPSOperSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSOperSiteOVTHrs,0))/SUM(per.MPSOperSiteSTH) END*100,
				MPSOverTimePctNonOvhlMaint = CASE WHEN SUM(per.MPSNOHMaintInspSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSNOHMaintInspSiteOVTHrs,0))/SUM(per.MPSNOHMaintInspSiteSTH) END*100,
				MPSOverTimePctInspectors = CASE WHEN SUM(per.MPSInspSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSInspSiteOVTHrs,0))/SUM(per.MPSInspSiteSTH) END*100,
				MPSOverTimePctOvhlMaint = CASE WHEN SUM(per.MPSOHAdjSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSOHAdjSiteOVTHrs,0))/SUM(per.MPSOHAdjSiteSTH) END*100,
				MPSOverTimePctAverageMaint = GlobalDB.dbo.WtAvg(pstc.MPSSiteOVTPcntMaint, pstc.MPSSiteSTHMaint),
				MPSOverTimePctTechnical = CASE WHEN SUM(per.MPSTechSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSTechSiteOVTHrs,0))/SUM(per.MPSTechSiteSTH) END*100,
				MPSOverTimePctAdmin = CASE WHEN SUM(per.MPSAdminSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSAdminSiteOVTHrs,0))/SUM(per.MPSAdminSiteSTH) END*100,
				MPSOverTimePctSupport = CASE WHEN SUM(per.MPSNonMaintSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSNonMaintSiteOVTHrs,0))/SUM(per.MPSNonMaintSiteSTH) END*100,
				MPSOverTimePctAverage = GlobalDB.dbo.WtAvg(pstc.MPSSiteOVTPcntTotal, pstc.MPSSiteSTHTotal),
				OCCEmpAbsence = GlobalDB.dbo.WtAvg(atc.OCCAbsPcnt, pstc.OCCSiteSTHTotal),
				OCCOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceONJOB, pstc.OCCSiteSTHTotal),
				OCCSickness = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceSICK, pstc.OCCSiteSTHTotal),
				OCCLegal = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceLEGAL, pstc.OCCSiteSTHTotal),
				OCCVacation = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceVAC, pstc.OCCSiteSTHTotal),
				OCCHolidays = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceHOL, pstc.OCCSiteSTHTotal),
				OCCExcused = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceOTHEXC, pstc.OCCSiteSTHTotal),
				OCCUnexcused = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceUNEXC, pstc.OCCSiteSTHTotal),
				OCCSteamTotal = GlobalDB.dbo.WtAvgNN((OCCAbsenceONJOB + acr.OCCAbsenceSICK + acr.OCCAbsenceLEGAL + acr.OCCAbsenceVAC + acr.OCCAbsenceHOL + acr.OCCAbsenceOTHEXC + acr.OCCAbsenceUNEXC), pstc.OCCSiteSTHTotal),
				OCCCogenOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceONJOBC, pstc.OCCSiteSTHTotal),
				OCCCogenHolidays = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceHOLVAC, pstc.OCCSiteSTHTotal),
				OCCCogenExcused = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceOTHER, pstc.OCCSiteSTHTotal),
				OCCCogenTotal = GlobalDB.dbo.WtAvgNN((OCCAbsenceONJOBC + acr.OCCAbsenceHOLVAC + acr.OCCAbsenceOTHER), pstc.OCCSiteSTHTotal),
				MPSEmpAbsence = GlobalDB.dbo.WtAvg(atc.MPSAbsPcnt, pstc.MPSSiteSTHTotal),
				MPSOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceONJOB, pstc.MPSSiteSTHTotal),
				MPSSickness = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceSICK, pstc.MPSSiteSTHTotal),
				MPSLegal = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceLEGAL, pstc.MPSSiteSTHTotal),
				MPSVacation = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceVAC, pstc.MPSSiteSTHTotal),
				MPSHolidays = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceHOL, pstc.MPSSiteSTHTotal),
				MPSExcused = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceOTHEXC, pstc.MPSSiteSTHTotal),
				MPSUnexcused = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceUNEXC, pstc.MPSSiteSTHTotal),
				MPSSteamTotal = GlobalDB.dbo.WtAvgNN((MPSAbsenceONJOB + acr.MPSAbsenceSICK + acr.MPSAbsenceLEGAL + acr.MPSAbsenceVAC + acr.MPSAbsenceHOL + acr.MPSAbsenceOTHEXC + acr.MPSAbsenceUNEXC), pstc.MPSSiteSTHTotal),
				MPSCogenOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceONJOBC, pstc.MPSSiteSTHTotal),
				MPSCogenHolidays = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceHOLVAC, pstc.MPSSiteSTHTotal),
				MPSCogenExcused = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceOTHER, pstc.MPSSiteSTHTotal),
				MPSCogenTotal = GlobalDB.dbo.WtAvgNN((MPSAbsenceONJOBC + acr.MPSAbsenceHOLVAC + acr.MPSAbsenceOTHER), pstc.MPSSiteSTHTotal)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN PersSTCalcByRefnum pstc ON pstc.Refnum = t.Refnum
				INNER JOIN GenSum gs ON gs.Refnum = t.Refnum
				INNER JOIN OpExCalc oec ON oec.Refnum = t.Refnum AND oec.DataType = 'ADJ'
				INNER JOIN PersByRefnum per ON per.Refnum = t.Refnum
				LEFT JOIN AbsenceTotCalc atc ON atc.Refnum = t.Refnum 
				LEFT JOIN AbsenceCalcByRefnum acr ON acr.Refnum = t.Refnum
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''

			
			--and this is the actual query being run
			EXEC ('INSERT ReportGroupPersonnelCompAbsenceData (RefNum, 
					ReportTitle, 
					ListName, 
					SiteEffPers,
					CentralEffPers,
					AGEffPers,
					ContractalEffPers,
					TotalEffPers,
					OCCCompensation,
					OCCBenefitPct,
					MPSCompensation,
					MPSBenefitPct,
					OCCOverTimePctOps,
					OCCOverTimePctNonOvhlMaint,
					OCCOverTimePctInspectors,
					OCCOverTimePctOvhlMaint,
					OCCOverTimePctAverageMaint,
					OCCOverTimePctTechnical,
					OCCOverTimePctAdmin,
					OCCOverTimePctSupport,
					OCCOverTimePctAverage,
					MPSOverTimePctOps,
					MPSOverTimePctNonOvhlMaint,
					MPSOverTimePctInspectors,
					MPSOverTimePctOvhlMaint,
					MPSOverTimePctAverageMaint,
					MPSOverTimePctTechnical,
					MPSOverTimePctAdmin,
					MPSOverTimePctSupport,
					MPSOverTimePctAverage,
					OCCEmpAbsence,
					OCCOnJobInjuries,
					OCCSickness,
					OCCLegal,
					OCCVacation,
					OCCHolidays,
					OCCExcused,
					OCCUnexcused,
					OCCSteamTotal,
					OCCCogenOnJobInjuries,
					OCCCogenHolidays,
					OCCCogenExcused,
					OCCCogenTotal,
					MPSEmpAbsence,
					MPSOnJobInjuries,
					MPSSickness,
					MPSLegal,
					MPSVacation,
					MPSHolidays,
					MPSExcused,
					MPSUnexcused,
					MPSSteamTotal,
					MPSCogenOnJobInjuries,
					MPSCogenHolidays,
					MPSCogenExcused,
					MPSCogenTotal)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					SiteEffPers = AVG(ISNULL(pstc.SiteEffPers,0)),
					CentralEffPers = AVG(ISNULL(pstc.CentralEffPers,0)),
					AGEffPers = AVG(ISNULL(pstc.AGEffPers,0)),
					ContractalEffPers = AVG(ISNULL(pstc.ContractEffPers,0)),
					TotalEffPers = AVG(CASE WHEN pstc.TotalEffPers <> 0 THEN pstc.TotalEffPers END),
					OCCCompensation = AVG(CASE WHEN gs.OCCAnnComp <> 0 THEN gs.OCCAnnComp END) * 0.001,
					OCCBenefitPct = GlobalDB.dbo.WtAvg(gs.OCCBenPcnt, oec.OCCWages),
					MPSCompensation = AVG(CASE WHEN gs.MPSAnnComp <> 0 THEN gs.MPSAnnComp END) * 0.001,
					MPSBenefitPct = GlobalDB.dbo.WtAvg(gs.MPSBenPcnt, oec.MPSWages),
					OCCOverTimePctOps = CASE WHEN SUM(per.OCCOperSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCOperSiteOVTHrs,0))/SUM(per.OCCOperSiteSTH) END*100,
					OCCOverTimePctNonOvhlMaint = CASE WHEN SUM(per.OCCNOHMaintInspSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCNOHMaintInspSiteOVTHrs,0))/SUM(per.OCCNOHMaintInspSiteSTH) END*100,
					OCCOverTimePctInspectors = CASE WHEN SUM(per.OCCInspSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCInspSiteOVTHrs,0))/SUM(per.OCCInspSiteSTH) END*100,
					OCCOverTimePctOvhlMaint = CASE WHEN SUM(per.OCCOHAdjSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCOHAdjSiteOVTHrs,0))/SUM(per.OCCOHAdjSiteSTH) END*100,
					OCCOverTimePctAverageMaint = GlobalDB.dbo.WtAvg(pstc.OCCSiteOVTPcntMaint, pstc.OCCSiteSTHMaint),
					OCCOverTimePctTechnical = CASE WHEN SUM(per.OCCTechSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCTechSiteOVTHrs,0))/SUM(per.OCCTechSiteSTH) END*100,
					OCCOverTimePctAdmin = CASE WHEN SUM(per.OCCAdminSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCAdminSiteOVTHrs,0))/SUM(per.OCCAdminSiteSTH) END*100,
					OCCOverTimePctSupport = CASE WHEN SUM(per.OCCNonMaintSiteSTH) <> 0 THEN SUM(ISNULL(per.OCCNonMaintSiteOVTHrs,0))/SUM(per.OCCNonMaintSiteSTH) END*100,
					OCCOverTimePctAverage = GlobalDB.dbo.WtAvg(pstc.OCCSiteOVTPcntTotal, pstc.OCCSiteSTHTotal),
					MPSOverTimePctOps = CASE WHEN SUM(per.MPSOperSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSOperSiteOVTHrs,0))/SUM(per.MPSOperSiteSTH) END*100,
					MPSOverTimePctNonOvhlMaint = CASE WHEN SUM(per.MPSNOHMaintInspSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSNOHMaintInspSiteOVTHrs,0))/SUM(per.MPSNOHMaintInspSiteSTH) END*100,
					MPSOverTimePctInspectors = CASE WHEN SUM(per.MPSInspSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSInspSiteOVTHrs,0))/SUM(per.MPSInspSiteSTH) END*100,
					MPSOverTimePctOvhlMaint = CASE WHEN SUM(per.MPSOHAdjSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSOHAdjSiteOVTHrs,0))/SUM(per.MPSOHAdjSiteSTH) END*100,
					MPSOverTimePctAverageMaint = GlobalDB.dbo.WtAvg(pstc.MPSSiteOVTPcntMaint, pstc.MPSSiteSTHMaint),
					MPSOverTimePctTechnical = CASE WHEN SUM(per.MPSTechSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSTechSiteOVTHrs,0))/SUM(per.MPSTechSiteSTH) END*100,
					MPSOverTimePctAdmin = CASE WHEN SUM(per.MPSAdminSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSAdminSiteOVTHrs,0))/SUM(per.MPSAdminSiteSTH) END*100,
					MPSOverTimePctSupport = CASE WHEN SUM(per.MPSNonMaintSiteSTH) <> 0 THEN SUM(ISNULL(per.MPSNonMaintSiteOVTHrs,0))/SUM(per.MPSNonMaintSiteSTH) END*100,
					MPSOverTimePctAverage = GlobalDB.dbo.WtAvg(pstc.MPSSiteOVTPcntTotal, pstc.MPSSiteSTHTotal),
					OCCEmpAbsence = GlobalDB.dbo.WtAvg(atc.OCCAbsPcnt, pstc.OCCSiteSTHTotal),
					OCCOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceONJOB, pstc.OCCSiteSTHTotal),
					OCCSickness = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceSICK, pstc.OCCSiteSTHTotal),
					OCCLegal = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceLEGAL, pstc.OCCSiteSTHTotal),
					OCCVacation = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceVAC, pstc.OCCSiteSTHTotal),
					OCCHolidays = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceHOL, pstc.OCCSiteSTHTotal),
					OCCExcused = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceOTHEXC, pstc.OCCSiteSTHTotal),
					OCCUnexcused = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceUNEXC, pstc.OCCSiteSTHTotal),
					OCCSteamTotal = GlobalDB.dbo.WtAvgNN((OCCAbsenceONJOB + acr.OCCAbsenceSICK + acr.OCCAbsenceLEGAL + acr.OCCAbsenceVAC + acr.OCCAbsenceHOL + acr.OCCAbsenceOTHEXC + acr.OCCAbsenceUNEXC), pstc.OCCSiteSTHTotal),
					OCCCogenOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceONJOBC, pstc.OCCSiteSTHTotal),
					OCCCogenHolidays = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceHOLVAC, pstc.OCCSiteSTHTotal),
					OCCCogenExcused = GlobalDB.dbo.WtAvgNN(acr.OCCAbsenceOTHER, pstc.OCCSiteSTHTotal),
					OCCCogenTotal = GlobalDB.dbo.WtAvgNN((OCCAbsenceONJOBC + acr.OCCAbsenceHOLVAC + acr.OCCAbsenceOTHER), pstc.OCCSiteSTHTotal),
					MPSEmpAbsence = GlobalDB.dbo.WtAvg(atc.MPSAbsPcnt, pstc.MPSSiteSTHTotal),
					MPSOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceONJOB, pstc.MPSSiteSTHTotal),
					MPSSickness = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceSICK, pstc.MPSSiteSTHTotal),
					MPSLegal = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceLEGAL, pstc.MPSSiteSTHTotal),
					MPSVacation = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceVAC, pstc.MPSSiteSTHTotal),
					MPSHolidays = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceHOL, pstc.MPSSiteSTHTotal),
					MPSExcused = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceOTHEXC, pstc.MPSSiteSTHTotal),
					MPSUnexcused = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceUNEXC, pstc.MPSSiteSTHTotal),
					MPSSteamTotal = GlobalDB.dbo.WtAvgNN((MPSAbsenceONJOB + acr.MPSAbsenceSICK + acr.MPSAbsenceLEGAL + acr.MPSAbsenceVAC + acr.MPSAbsenceHOL + acr.MPSAbsenceOTHEXC + acr.MPSAbsenceUNEXC), pstc.MPSSiteSTHTotal),
					MPSCogenOnJobInjuries = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceONJOBC, pstc.MPSSiteSTHTotal),
					MPSCogenHolidays = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceHOLVAC, pstc.MPSSiteSTHTotal),
					MPSCogenExcused = GlobalDB.dbo.WtAvgNN(acr.MPSAbsenceOTHER, pstc.MPSSiteSTHTotal),
					MPSCogenTotal = GlobalDB.dbo.WtAvgNN((MPSAbsenceONJOBC + acr.MPSAbsenceHOLVAC + acr.MPSAbsenceOTHER), pstc.MPSSiteSTHTotal)
				FROM TSort t 
					INNER JOIN PersSTCalcByRefnum pstc ON pstc.Refnum = t.Refnum
					INNER JOIN GenSum gs ON gs.Refnum = t.Refnum
					INNER JOIN OpExCalc oec ON oec.Refnum = t.Refnum AND oec.DataType = ''ADJ''
					INNER JOIN PersByRefnum per ON per.Refnum = t.Refnum
					LEFT JOIN AbsenceTotCalc atc ON atc.Refnum = t.Refnum 
					LEFT JOIN AbsenceCalcByRefnum acr ON acr.Refnum = t.Refnum
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END





