﻿
CREATE  PROC [dbo].[spCRVCalcs] (@Refnum Refnum)
AS
SET NOCOUNT ON
DECLARE @CogenElec varchar(5), @CRVGroup char(5), @CRVClassName char(5), @CRVSizeGroup varchar(6), @StudyYear smallint
DECLARE @AvgFuelCost real, @HeatRate real, @OpexLessFuel real
DECLARE @UnplanMWH real, @UnplanLROP real, @UnplanUnavail real, @UUWorth real
DECLARE @TotRevenue real, @AdjNetMWH real, @NOF real, @NMC real, @PSIG real, @ServiceHrs real, @Starts real
DECLARE @ConstantFactor real, @NOFFactor real,  @NMCFactor real, @PSIGFactor real, @ServiceHrsFactor real, @StartsFactor real

SELECT @CRVGroup = CRVGroup, @CogenElec = CogenElec FROM Breaks WHERE Refnum = @Refnum
SELECT @StudyYear = dbo.StudyYear(@Refnum)

IF @CRVGroup Is NOT NULL
BEGIN
	SELECT @AvgFuelCost = AvgFuelCostMBTU, @UUWorth = AvgUUWorth,
		@ConstantFactor = ConstantFactor, @NOFFactor = NOFFactor,  
		@NMCFactor = NMCFactor, @PSIGFactor = PSIGFactor,
		@ServiceHrsFactor = ServiceHrsFactor, @StartsFactor = StartsFactor
	FROM CRVFactors f 
	WHERE f.StudyYear = @StudyYear AND f.CRVGroup = @CRVGroup
	
	SELECT @HeatRate = HeatRate, @OpexLessFuel = TotCashwoFuelCost, 
		@AdjNetMWH = AdjNetMWH
	FROM GenSum WHERE Refnum = @Refnum
	
	SELECT @UnplanMWH = PeakMWHLost_M + PeakMWHLost_F, 
		@UnplanLROP = (ISNULL(LROP_M,0)+ISNULL(LROP_F,0)),
		@UnplanUnavail = (ISNULL(PeakUnavail_M,0)+ISNULL(PeakUnavail_F,0))
	FROM CommUnavail WHERE Refnum = @Refnum
	
	SELECT @TotRevenue = TotRevenue
	FROM MiscCalc WHERE Refnum = @Refnum
	
	SELECT @NOF = NOF, @NMC = NMC, @ServiceHrs = ServiceHrs, @Starts = ACTStarts
	FROM NERCFactors WHERE Refnum = @Refnum
	
	SELECT @PSIG = AVG(BlrPSIG)
	FROM NERCTurbine t INNER JOIN DesignData d ON d.Utility = t.Util_Code AND d.Unit = t.Unit_Code
	WHERE t.Refnum = @Refnum
	
	DECLARE @CRV real, @PredictedCRV real
	DECLARE @CRVFuel real, @CRVTotOpex real, @CRVUnplan real, @CRVRevenue real
	
	SELECT 	@CRVFuel = @AvgFuelCost * @HeatRate/1000
	SELECT 	@CRVTotOpex = @OpexLessFuel + @CRVFuel, 
			@CRVRevenue = @TotRevenue*1000/@AdjNetMWH
			
	IF @UnplanUnavail > 0
		SELECT @CRVUnplan = @UnplanMWH*@UUWorth*(@UnplanLROP/@UnplanUnavail)/@AdjNetMWH
	ELSE
		SELECT @CRVUnplan = 0
		
	SELECT @CRV = @CRVTotOpex + @CRVUnplan - @CRVRevenue
	SELECT @PredictedCRV = @ConstantFactor + @NOFFactor * @NOF 
			+ @NMCFactor*@NMC + @PSIGFactor*@PSIG
			+ @ServiceHrsFactor * @ServiceHrs + @StartsFactor * @Starts
			
	IF EXISTS (SELECT * FROM CRVCalc WHERE Refnum = @Refnum)
		UPDATE CRVCalc
		SET 	CRVGroup = @CRVGroup, 
			CRVActual = @CRV, 
			CRVPredicited = @PredictedCRV, 
			CRVVariance = (@CRV-@PredictedCRV)/@PredictedCRV, 
			CRVFuel = @CRVFuel, 
			CRVTotOpex = @CRVTotOpex, 
			CRVUnplanned = @CRVUnplan, 
			CRVRevenue = @CRVRevenue
		WHERE Refnum = @Refnum
	ELSE
		INSERT INTO CRVCalc (Refnum, CRVGroup, CRVActual, CRVPredicited, CRVVariance, 
			CRVFuel, CRVTotOpex, CRVUnplanned, CRVRevenue)
		VALUES (@Refnum, @CRVGroup, @CRV, @PredictedCRV, (@CRV-@PredictedCRV)/@PredictedCRV, 
			@CRVFuel, @CRVTotOpex, @CRVUnplan, @CRVRevenue)

	SELECT @CRVClassName = CASE @CRVGroup 
		WHEN 'GAS' THEN 'CRVGS'
		WHEN 'CC' THEN 'CRVCC'
		WHEN 'ED' THEN 'CRVED'
		WHEN 'ESC' THEN 'CRVES'
		WHEN 'LB' THEN 'CRVLB' END
		
	IF @CRVGroup = 'CC' AND EXISTS (SELECT * FROM Class_LU WHERE StudyYear = @StudyYear AND ClassType = 'CRVCE')
	BEGIN
		IF @CogenElec = 'ELEC' 
			SELECT @CRVClassName = 'CRVCE', @CRVGroup = 'CE'
		ELSE
			SELECT @CRVClassname = 'CRVCG', @CRVGroup = 'CG'
	END
	
	IF EXISTS (SELECT * FROM Class_LU WHERE StudyYear = @StudyYear AND ClassType = @CRVClassName AND ClassNum > 1)
		SELECT @CRVSizeGroup = rtrim(@CRVGroup) + convert(varchar(1), ClassNum)
		FROM Class_LU
		WHERE StudyYear = @StudyYear AND ClassType = @CRVClassName
		AND MinCap <= @NMC AND MaxCap > @NMC
	ELSE
		SELECT @CRVSizeGroup = @CRVGroup
		
	UPDATE Breaks
	SET CRVSizeGroup = @CRVSizeGroup
	WHERE Refnum = @Refnum
END


