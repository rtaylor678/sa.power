﻿

CREATE PROCEDURE spAddReportOption
	@ReportSetID DD_ID, @ReportSetName varchar(30),
	@SheetName varchar(30), 
	@ReportID integer = NULL, @ReportName varchar(30) = NULL,
	@RefListNo RefListNo = NULL, @RefList RefListName = NULL,
	@BreakConditions varchar(255), @Scenario Scenario = NULL,
	@Criteria varchar(255) = NULL, @Orientation char(1) = 'R',
	@SortKey integer = NULL, @PreferenceID varchar(5) = NULL,
	@Currency varchar(5) = NULL
AS
DECLARE @Check integer, @CurrUser varchar(20), @ErrMsg varchar(255)
/* Make sure that a ReportSet was specified */
IF (@ReportSetID IS NULL AND @ReportSetName IS NULL) 
BEGIN
	RAISERROR('You must supply a ReportSet ID or Name', 16, -1)
	RETURN -100
END
/* If ReportSet was identified by ReportSetName, get ReportSetID for ReportSetName */
IF @ReportSetID IS NULL
	SELECT @ReportSetID = ReportSetID FROM ReportSets
	WHERE ReportSetName = @ReportSetName
IF @ReportSetID IS NULL
BEGIN
	RAISERROR('Invalid ReportSet Name', 16, -1)
	RETURN -100
END
/* Get user information and check rights to modify the ReportSet */
SELECT @CurrUser = USER_NAME()
EXEC @Check = spCheckReportSetOwner @ReportSetID, @ReportSetName, @CurrUser
IF @Check < 0 
BEGIN
	SELECT @ErrMsg = 'You do not have permission to modify ReportSet '+CONVERT(varchar(20), @ReportSetID)
	RAISERROR(@ErrMsg, 16, -1)
	RETURN -101
END
/* Make sure that Report was identified */
IF (@ReportID IS NULL AND @ReportName IS NULL) 
BEGIN
	RAISERROR('You must supply a ReportID or ReportName', 16, -1)
	RETURN -102
END
/* Get ReportID if Report was given by name */
IF @ReportID IS NULL
	SELECT @ReportID = ReportID FROM ReportDef
	WHERE ReportName = @ReportName
IF @ReportID IS NULL
BEGIN
	RAISERROR('Invalid ReportName', 16, -1)
	RETURN -102
END
/* Make sure that a Refinery List was given */
IF (@RefListNo IS NULL AND @RefList IS NULL)
BEGIN
	RAISERROR('You must supply a Refinery List Name or Number', 16, -1)
	RETURN -104
END
/* Get RefListNo if Refinery List was given by Name */
IF @RefListNo IS NULL
	SELECT @RefListNo = RefListNo FROM RefList_LU
	WHERE ListName = @RefList
IF @RefListNo IS NULL
BEGIN
	RAISERROR('Invalid Refinery List Name', 16, -1)
	RETURN -104
END
/* Calculate a sort key (make it last) if none was given */
IF @SortKey IS NULL
	SELECT @SortKey = ISNULL(Max(SortKey), 0) + 1
	FROM ReportOptions
	WHERE ReportSetID = @ReportSetID
/* Set Scenario to default of BASE if none was given */
IF @Scenario IS NULL
	SELECT @Scenario = 'BASE'
/*  Insert the ReportOption */
INSERT INTO ReportOptions (ReportSetID, SheetName, ReportID,
	RefListNo, BreakConditions, RptOptionScenario,
	RptOptionSQL, Orientation, SortKey, PreferenceID, RptOptionCurrency)
VALUES (@ReportSetID, @SheetName, @ReportID,
	@RefListNo, @BreakConditions, @Scenario,
	@Criteria, @Orientation, @SortKey, @PreferenceID, @Currency)

