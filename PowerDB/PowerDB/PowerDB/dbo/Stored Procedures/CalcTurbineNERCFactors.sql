﻿CREATE  PROC [dbo].[CalcTurbineNERCFactors] (@SiteID SiteID)
AS
DECLARE @Refnum varchar(12)
Declare @NMC real, @NDC real
Declare @EFOR real, @EFORWtFactor real
Declare @EPOF real
Declare @EUOF real
Declare @EPOR real, @EPORWtFactor real
Declare @EUOR real, @EUORWtFactor real
Declare @EUF  real
Declare @EAF  real
Declare @POF  real
Declare @EOR  real, @EORWtFactor real
Declare @FOF  real
Declare @MOF  real 
Declare	@NUMFO real 	-- F5 #16
Declare	@NUMMO real 	-- F5 #17
Declare	@NUMPO real 	-- F4 #14
Declare	@NUMUO real  	-- F4 #15
Declare @PH    real 	-- F2 #11
Declare @SH    real     -- F1 #1
Declare @UOF   real	-- F7 #2
Declare @PH_Unweighted real
Declare @SH_Unweighted real
Declare @AGE	real
Declare	@ACT_Starts real 
Declare	@ATM_Starts real 
-- 01/21/09 DB new table fields and weight factors
Declare @WEFOF real --, @EFOFWtFactor real
Declare @WUOR real, @UORWtFactor real
Declare @WFOR real, @FORWtFactor real
Declare @WPOR real, @PORWtFactor real
	
Declare @UtilityUnitCode char(6)	
Declare @EventYear int
Declare @EventYearMinusOne as int

Declare cTurbines CURSOR LOCAL FAST_FORWARD
For SELECT nt.Refnum, nt.UtilityUnitCode, t.EvntYear
	FROM NERCTurbine nt INNER JOIN TSort t ON t.Refnum = nt.Refnum AND ISNULL(t.CalcCommUnavail, 'Y') <> 'N'
	WHERE SiteID = @SiteID

OPEN cTurbines
FETCH NEXT FROM cTurbines INTO @Refnum, @UtilityUnitCode, @EventYear
WHILE (@@FETCH_STATUS <> -1)
BEGIN
	SELECT @EventYearMinusOne = @EventYear - 1
	-- Load up all of the 1-year fields
	EXEC spGadsFactors @UtilityUnitCode ,@EventYear , @EventYear,
		@NMC = @NMC output, @NDC = @NDC output,
		@EFOR = @EFOR output, @EFORWtFactor = @EFORWtFactor output,
		@EPOF = @EPOF output, 
		@EUOF = @EUOF output, 
		@EPOR  = @EPOR output, @EPORWtFactor = @EPORWtFactor output,
		@EUOR = @EUOR output, @EUORWtFactor = @EUORWtFactor output,
		@EUF = @EUF output, 
		@EAF = @EAF output,
		@POF = @POF output, 
		@EOR = @EOR output, @EORWtFactor = @EORWtFactor output,
		@FOF = @FOF output,
		@MOF = @MOF  output,
		@NUMFO = @NUMFO output,	-- F5 #16
		@NUMMO = @NUMMO output,	-- F5 #17
		@NUMPO = @NUMPO output,	-- F4 #14
		@NUMUO = @NUMUO output,	-- F4 #15
		@PH = @PH	output,
		@SH = @SH	output,
		@UOF = @UOF     output, -- F7 #2
		@PH_Unweighted = @PH_Unweighted output,
		@SH_Unweighted = @SH_Unweighted output,
		@AGE = @AGE output,
		@ACT_Starts = @ACT_Starts output,
		@ATM_Starts = @ATM_Starts output,
		@WEFOF = @WEFOF output, --@EFOFWtFactor = @EFOFWtFactor output,
		@WUOR = @WUOR output, @UORWtFactor = @UORWtFactor output,
		@WFOR = @WFOR output, @FORWtFactor = @FORWtFactor output,
		@WPOR = @WPOR output, @PORWtFactor = @PORWtFactor output

	-- DB 08/2008 changed the way the age value is calculated to use the GADSNG.Setup value
	declare @StartDate datetime
	declare @EndDate datetime
	select @StartDate=s.CommercialDate from GADSOS.GADSNG.Setup s where s.UtilityUnitCode=@UtilityUnitCode
	select @EndDate= CONVERT(char(4),@EventYear)+'-12-31'
	select @Age=DATEDIFF (d, @StartDate,@EndDate)
	select @Age=@Age/365
	
	-- If we did not get a good NDC from Gads, 
	-- try to get it from the DesignData table.
	IF @NDC IS NULL OR @NDC = 0 
		SELECT @NDC = Nameplat
		FROM DesignData 
		WHERE UtilityUnitCode = @UtilityUnitCode
    -- SAME FOR NMC --
	-- If we did not get a good NMC from Gads, 
	-- try to get it from the DesignData table.
	IF @NMC IS NULL OR @NMC = 0 
		SELECT @NMC = Nameplat
		FROM DesignData 
		WHERE UtilityUnitCode = @UtilityUnitCode

	-- Update the record with the 1-year fields
	UPDATE NERCTurbine SET 
		NMC = @NMC, NDC = @NDC,
		EFOR = @EFOR, EFORWtFactor = @EFORWtFactor,  
		EPOF = @EPOF, 
		EUOF = @EUOF, 
		EPOR = @EPOR, EPORWtFactor = @EPORWtFactor,
		EUOR = @EUOR, EUORWtFactor = @EUORWtFactor,
		EUF = @EUF, 
		EAF = @EAF,
		POF = @POF, 
		EOR = @EOR, EORWtFactor = @EORWtFactor,
		FOF = @FOF,
		MOF = @MOF,
		NUMFO = @NUMFO,
		NUMMO = @NUMMO,
		NUMPO = @NUMPO,
		NUMUO = @NUMUO,
		PeriodHrs = @PH_Unweighted,
		ServiceHrs = @SH_Unweighted,
		UOF = @UOF,
		Age = @AGE,
		ACTStarts = @ACT_Starts,
		ATMStarts = @ATM_Starts,
		-- 01/21/09 DB new table fields and weight factors
		WEFOF = @WEFOF,
		--	EFOFWtFactor = @EFOFWtFactor,
		WUOR = @WUOR, 
		UORWtFactor = @UORWtFactor,
		WFOR = @WFOR, 
		FORWtFactor = @FORWtFactor,
		WPOR = @WPOR, 
		PORWtFactor = @PORWtFactor 
	WHERE Refnum = @Refnum AND UtilityUnitCode = @UtilityUnitCode

	-- Load up all of the 2-year fields
	EXEC spGadsFactors @UtilityUnitCode ,@EventYear , @EventYearMinusOne,
		@NMC = @NMC output, @NDC = @NDC output,
		@EFOR = @EFOR output, @EFORWtFactor = @EFORWtFactor output,
		@EPOF = @EPOF output, 
		@EUOF = @EUOF output, 
		@EPOR  = @EPOR output, @EPORWtFactor = @EPORWtFactor output,
		@EUOR = @EUOR output, @EUORWtFactor = @EUORWtFactor output,
		@EUF = @EUF output, 
		@EAF = @EAF output,
		@POF = @POF output, 
		@EOR = @EOR output, @EORWtFactor = @EORWtFactor output,
		@FOF = @FOF output,
		@MOF = @MOF  output,
		@NUMFO = @NUMFO output,	-- F5 #16
		@NUMMO = @NUMMO output,	-- F5 #17
		@NUMPO = @NUMPO output,	-- F4 #14
		@NUMUO = @NUMUO output,	-- F4 #15
		@PH = @PH	output,
		@SH = @SH	output,
		@UOF = @UOF     output, -- F7 #2
		@PH_Unweighted = @PH_Unweighted output,
		@SH_Unweighted = @SH_Unweighted output,
		@AGE = @AGE output,
		@ACT_Starts = @ACT_Starts output,
		@ATM_Starts = @ATM_Starts output,
		@WEFOF = @WEFOF output, --@EFOFWtFactor = @EFOFWtFactor output,
		@WUOR = @WUOR output, @UORWtFactor = @UORWtFactor output,
		@WFOR = @WFOR output, @FORWtFactor = @FORWtFactor output,
		@WPOR = @WPOR output, @PORWtFactor = @PORWtFactor output

	-- If we did not get a good NDC from Gads, 
	-- try to get it from the DesignData table.
	IF @NDC IS NULL OR @NDC = 0 
		SELECT @NDC = Nameplat
		FROM DesignData 
		WHERE UtilityUnitCode = @UtilityUnitCode
    -- SAME FOR NMC --
	-- If we did not get a good NMC from Gads, 
	-- try to get it from the DesignData table.
	IF @NMC IS NULL OR @NMC = 0 
		SELECT @NMC = Nameplat
		FROM DesignData 
		WHERE UtilityUnitCode = @UtilityUnitCode

	-- Update the record with the 2-year fields
	UPDATE NERCTurbine SET 
		NMC2Yr = @NMC, NDC2Yr = @NDC,
		EFOR2Yr = @EFOR, EFOR2YrWtFactor = @EFORWtFactor,  
		EPOF2Yr = @EPOF, 
		EUOF2Yr = @EUOF, 
		EPOR2Yr = @EPOR, EPOR2YrWtFactor = @EPORWtFactor,
		EUOR2Yr = @EUOR, EUOR2YrWtFactor = @EUORWtFactor,
		EUF2Yr = @EUF, 
		EAF2Yr = @EAF,
		POF2Yr = @POF, 
		EOR2Yr = @EOR, EOR2YrWtFactor = @EORWtFactor,
		FOF2Yr = @FOF,
		MOF2Yr = @MOF,
		PeriodHrs2Yr = @PH_Unweighted,
		ServiceHrs2Yr = @SH_Unweighted,
		ServiceHrs2YrAvg = @SH_Unweighted / 2,
		UOF2Yr = @UOF,
		-- 01/21/09 DB new table fields and weight factors
		WEFOF2Yr = @WEFOF,
		--EFOF2YrWtFactor = @EFOFWtFactor,
		WUOR2Yr = @WUOR, 
		UOR2YrWtFactor = @UORWtFactor,
		WFOR2Yr = @WFOR, 
		FOR2YrWtFactor = @FORWtFactor,
		WPOR2Yr = @WPOR, 
		POR2YrWtFactor = @PORWtFactor
	WHERE Refnum = @Refnum AND UtilityUnitCode = @UtilityUnitCode

	FETCH NEXT FROM cTurbines INTO @Refnum, @UtilityUnitCode, @EventYear
END
CLOSE cTurbines
DEALLOCATE cTurbines
