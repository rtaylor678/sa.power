﻿






CREATE PROC [dbo].[spLoadReportGroupPersonnelMWData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

		DECLARE @temptable TABLE (people REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL


	
	--remove the existing record in the table
	DELETE FROM ReportGroupPersonnelMWData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it


		INSERT @temptable
		SELECT TOP 2 gs.TotEffPersMW AS people
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				INNER JOIN PersMWByRefnum per ON Per.Refnum = t.Refnum
				INNER JOIN PersSTCalcMWByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
			ORDER BY gs.TotEffPersMW ASC

		SELECT @minvalue = AVG(people) FROM @temptable 

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 gs.TotEffPersMW AS people
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				INNER JOIN PersMWByRefnum per ON Per.Refnum = t.Refnum
				INNER JOIN PersSTCalcMWByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
			ORDER BY gs.TotEffPersMW DESC

		SELECT @maxvalue = AVG(people) FROM @temptable 
		
		
		
			INSERT ReportGroupPersonnelMWData (RefNum,
				ReportTitle,
				ListName,
				TotEffPersonnel,
				MinimumPersonnel,
				MaximumPersonnel,
				OCCTotalOps,
				OCCTotalNonOvhlMaint,
				OCCTotalInspectors,
				OCCTotalLumpSumContract,
				OCCTotalLTSA,
				OCCTotalOvhlMaint,
				OCCTotalSubTotalMaint,
				OCCTotalTechnical,
				OCCTotalAdmin,
				OCCTotalSupport,
				OCCTotalTotal,
				MPSTotalOps,
				MPSTotalNonOvhlMaint,
				MPSTotalInspectors,
				MPSTotalLumpSumContract,
				MPSTotalLTSA,
				MPSTotalOvhlMaint,
				MPSTotalSubTotalMaint,
				MPSTotalTechnical,
				MPSTotalAdmin,
				MPSTotalSupport,
				MPSTotalTotal,
				OCCSiteOps,
				OCCSiteNonOvhlMaint,
				OCCSiteInspectors,
				OCCSiteOvhlMaint,
				OCCSiteSubTotalMaint,
				OCCSiteTechnical,
				OCCSiteAdmin,
				OCCSiteSupport,
				OCCSiteTotal,
				OCCCentralNonOvhlMaint,
				OCCCentralInspectors,
				OCCCentralOvhlMaint,
				OCCCentralSubTotalMaint,
				OCCCentralTotal,
				OCCAGOps,
				OCCAGNonOvhlMaint,
				OCCAGInspectors,
				OCCAGOvhlMaint,
				OCCAGSubTotalMaint,
				OCCAGTechnical,
				OCCAGAdmin,
				OCCAGSupport,
				OCCAGTotal,
				OCCContractOps,
				OCCContractNonOvhlMaint,
				OCCContractInspectors,
				OCCContractLumpSumContract,
				OCCContractLTSA,
				OCCContractOvhlMaint,
				OCCContractSubTotalMaint,
				OCCContractTechnical,
				OCCContractAdmin,
				OCCContractSupport,
				OCCContractTotal,
				MPSSiteOps,
				MPSSiteNonOvhlMaint,
				MPSSiteInspectors,
				MPSSiteOvhlMaint,
				MPSSiteSubTotalMaint,
				MPSSiteTechnical,
				MPSSiteAdmin,
				MPSSiteSupport,
				MPSSiteTotal,
				MPSCentralNonOvhlMaint,
				MPSCentralInspectors,
				MPSCentralOvhlMaint,
				MPSCentralSubTotalMaint,
				MPSCentralTotal,
				MPSAGOps,
				MPSAGNonOvhlMaint,
				MPSAGInspectors,
				MPSAGOvhlMaint,
				MPSAGSubTotalMaint,
				MPSAGTechnical,
				MPSAGAdmin,
				MPSAGSupport,
				MPSAGTotal,
				MPSContractOps,
				MPSContractNonOvhlMaint,
				MPSContractInspectors,
				MPSContractLumpSumContract,
				MPSContractLTSA,
				MPSContractOvhlMaint,
				MPSContractSubTotalMaint,
				MPSContractTechnical,
				MPSContractAdmin,
				MPSContractSupport,
				MPSContractTotal)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				TotEffPersonnel = GlobalDB.dbo.WtAvg(gs.TotEffPersMW, nf.NMC),
				MinimumPersonnel = @minvalue,
				MaximumPersonnel = @maxvalue,
				OCCTotalOps = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCOper, nf.NMC),
				OCCTotalNonOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCNOHMaintInsp, nf.NMC),
				OCCTotalInspectors = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCInsp, nf.NMC),
				OCCTotalLumpSumContract = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCLSCAdj, nf.NMC),
				OCCTotalLTSA = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCLTSA, nf.NMC),
				OCCTotalOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCOHAdj, nf.NMC),
				OCCTotalSubTotalMaint = GlobalDB.dbo.WtAvg(Pst.TotMWOCCSubtotalMaint, nf.NMC),
				OCCTotalTechnical = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCTech, nf.NMC),
				OCCTotalAdmin = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCAdmin, nf.NMC),
				OCCTotalSupport = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCNonMaint, nf.NMC),
				OCCTotalTotal = GlobalDB.dbo.WtAvg(Pst.TotMWOCCTotal, nf.NMC),
				MPSTotalOps = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSOper, nf.NMC),
				MPSTotalNonOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSNOHMaintInsp, nf.NMC),
				MPSTotalInspectors = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSInsp, nf.NMC),
				MPSTotalLumpSumContract = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSLSCAdj, nf.NMC),
				MPSTotalLTSA = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSLTSA, nf.NMC),
				MPSTotalOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSOHAdj, nf.NMC),
				MPSTotalSubTotalMaint = GlobalDB.dbo.WtAvg(Pst.TotMWMPSSubtotalMaint, nf.NMC),
				MPSTotalTechnical = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSTech, nf.NMC),
				MPSTotalAdmin = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSAdmin, nf.NMC),
				MPSTotalSupport = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSNonMaint, nf.NMC),
				MPSTotalTotal = GlobalDB.dbo.WtAvg(Pst.TotMWMPSTotal, nf.NMC),
				OCCSiteOps = SUM(CASE WHEN Per.SiteOCCOper IS NOT NULL THEN Per.SiteOCCOper END)/SUM(CASE WHEN Per.SiteOCCOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteNonOvhlMaint = SUM(CASE WHEN Per.SiteOCCNOHMaintInsp IS NOT NULL THEN Per.SiteOCCNOHMaintInsp END)/SUM(CASE WHEN Per.SiteOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteInspectors = SUM(CASE WHEN Per.SiteOCCInsp IS NOT NULL THEN Per.SiteOCCInsp END)/SUM(CASE WHEN Per.SiteOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteOvhlMaint = SUM(CASE WHEN Per.SiteOCCOHAdj IS NOT NULL THEN Per.SiteOCCOHAdj END)/SUM(CASE WHEN Per.SiteOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				OCCSiteTechnical = SUM(CASE WHEN Per.SiteOCCTech IS NOT NULL THEN Per.SiteOCCTech END)/SUM(CASE WHEN Per.SiteOCCTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteAdmin = SUM(CASE WHEN Per.SiteOCCAdmin IS NOT NULL THEN Per.SiteOCCAdmin END)/SUM(CASE WHEN Per.SiteOCCAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteSupport = SUM(CASE WHEN Per.SiteOCCNonMaint IS NOT NULL THEN Per.SiteOCCNonMaint END)/SUM(CASE WHEN Per.SiteOCCNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCSiteTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteOCCTotal,0))/SUM(nf.NMC) END * 100,
				OCCCentralNonOvhlMaint = SUM(CASE WHEN Per.CentralOCCNOHMaintInsp IS NOT NULL THEN Per.CentralOCCNOHMaintInsp END)/SUM(CASE WHEN Per.CentralOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCCentralInspectors = SUM(CASE WHEN Per.CentralOCCInsp IS NOT NULL THEN Per.CentralOCCInsp END)/SUM(CASE WHEN Per.CentralOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCCentralOvhlMaint = SUM(CASE WHEN Per.CentralOCCOHAdj IS NOT NULL THEN Per.CentralOCCOHAdj END)/SUM(CASE WHEN Per.CentralOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCCentralSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				OCCCentralTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralOCCTotal,0))/SUM(nf.NMC) END * 100,
				OCCAGOps = SUM(CASE WHEN Per.AGOCCOper IS NOT NULL THEN Per.AGOCCOper END)/SUM(CASE WHEN Per.AGOCCOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGNonOvhlMaint = SUM(CASE WHEN Per.AGOCCNOHMaintInsp IS NOT NULL THEN Per.AGOCCNOHMaintInsp END)/SUM(CASE WHEN Per.AGOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGInspectors = SUM(CASE WHEN Per.AGOCCInsp IS NOT NULL THEN Per.AGOCCInsp END)/SUM(CASE WHEN Per.AGOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGOvhlMaint = SUM(CASE WHEN Per.AGOCCOHAdj IS NOT NULL THEN Per.AGOCCOHAdj END)/SUM(CASE WHEN Per.AGOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				OCCAGTechnical = SUM(CASE WHEN Per.AGOCCTech IS NOT NULL THEN Per.AGOCCTech END)/SUM(CASE WHEN Per.AGOCCTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGAdmin = SUM(CASE WHEN Per.AGOCCAdmin IS NOT NULL THEN Per.AGOCCAdmin END)/SUM(CASE WHEN Per.AGOCCAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGSupport = SUM(CASE WHEN Per.AGOCCNonMaint IS NOT NULL THEN Per.AGOCCNonMaint END)/SUM(CASE WHEN Per.AGOCCNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCAGTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGOCCTotal,0))/SUM(nf.NMC) END * 100,
				OCCContractOps = SUM(CASE WHEN Per.ContractOCCOper IS NOT NULL THEN Per.ContractOCCOper END)/SUM(CASE WHEN Per.ContractOCCOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractNonOvhlMaint = SUM(CASE WHEN Per.ContractOCCNOHMaintInsp IS NOT NULL THEN Per.ContractOCCNOHMaintInsp END)/SUM(CASE WHEN Per.ContractOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractInspectors = SUM(CASE WHEN Per.ContractOCCInsp IS NOT NULL THEN Per.ContractOCCInsp END)/SUM(CASE WHEN Per.ContractOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractLumpSumContract = SUM(CASE WHEN Per.ContractOCCLSCAdj IS NOT NULL THEN Per.ContractOCCLSCAdj END)/SUM(CASE WHEN Per.ContractOCCLSCAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractLTSA = SUM(CASE WHEN Per.ContractOCCLTSA IS NOT NULL THEN Per.ContractOCCLTSA END)/SUM(CASE WHEN Per.ContractOCCLTSA IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractOvhlMaint = SUM(CASE WHEN Per.ContractOCCOHAdj IS NOT NULL THEN Per.ContractOCCOHAdj END)/SUM(CASE WHEN Per.ContractOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				OCCContractTechnical = SUM(CASE WHEN Per.ContractOCCTech IS NOT NULL THEN Per.ContractOCCTech END)/SUM(CASE WHEN Per.ContractOCCTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractAdmin = SUM(CASE WHEN Per.ContractOCCAdmin IS NOT NULL THEN Per.ContractOCCAdmin END)/SUM(CASE WHEN Per.ContractOCCAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractSupport = SUM(CASE WHEN Per.ContractOCCNonMaint IS NOT NULL THEN Per.ContractOCCNonMaint END)/SUM(CASE WHEN Per.ContractOCCNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				OCCContractTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractOCCTotal,0))/SUM(nf.NMC) END * 100,
				MPSSiteOps = SUM(CASE WHEN Per.SiteMPSOper IS NOT NULL THEN Per.SiteMPSOper END)/SUM(CASE WHEN Per.SiteMPSOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteNonOvhlMaint = SUM(CASE WHEN Per.SiteMPSNOHMaintInsp IS NOT NULL THEN Per.SiteMPSNOHMaintInsp END)/SUM(CASE WHEN Per.SiteMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteInspectors = SUM(CASE WHEN Per.SiteMPSInsp IS NOT NULL THEN Per.SiteMPSInsp END)/SUM(CASE WHEN Per.SiteMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteOvhlMaint = SUM(CASE WHEN Per.SiteMPSOHAdj IS NOT NULL THEN Per.SiteMPSOHAdj END)/SUM(CASE WHEN Per.SiteMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				MPSSiteTechnical = SUM(CASE WHEN Per.SiteMPSTech IS NOT NULL THEN Per.SiteMPSTech END)/SUM(CASE WHEN Per.SiteMPSTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteAdmin = SUM(CASE WHEN Per.SiteMPSAdmin IS NOT NULL THEN Per.SiteMPSAdmin END)/SUM(CASE WHEN Per.SiteMPSAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteSupport = SUM(CASE WHEN Per.SiteMPSNonMaint IS NOT NULL THEN Per.SiteMPSNonMaint END)/SUM(CASE WHEN Per.SiteMPSNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSSiteTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteMPSTotal,0))/SUM(nf.NMC) END * 100,
				MPSCentralNonOvhlMaint = SUM(CASE WHEN Per.CentralMPSNOHMaintInsp IS NOT NULL THEN Per.CentralMPSNOHMaintInsp END)/SUM(CASE WHEN Per.CentralMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSCentralInspectors = SUM(CASE WHEN Per.CentralMPSInsp IS NOT NULL THEN Per.CentralMPSInsp END)/SUM(CASE WHEN Per.CentralMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSCentralOvhlMaint = SUM(CASE WHEN Per.CentralMPSOHAdj IS NOT NULL THEN Per.CentralMPSOHAdj END)/SUM(CASE WHEN Per.CentralMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSCentralSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				MPSCentralTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralMPSTotal,0))/SUM(nf.NMC) END * 100,
				MPSAGOps = SUM(CASE WHEN Per.AGMPSOper IS NOT NULL THEN Per.AGMPSOper END)/SUM(CASE WHEN Per.AGMPSOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGNonOvhlMaint = SUM(CASE WHEN Per.AGMPSNOHMaintInsp IS NOT NULL THEN Per.AGMPSNOHMaintInsp END)/SUM(CASE WHEN Per.AGMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGInspectors = SUM(CASE WHEN Per.AGMPSInsp IS NOT NULL THEN Per.AGMPSInsp END)/SUM(CASE WHEN Per.AGMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGOvhlMaint = SUM(CASE WHEN Per.AGMPSOHAdj IS NOT NULL THEN Per.AGMPSOHAdj END)/SUM(CASE WHEN Per.AGMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				MPSAGTechnical = SUM(CASE WHEN Per.AGMPSTech IS NOT NULL THEN Per.AGMPSTech END)/SUM(CASE WHEN Per.AGMPSTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGAdmin = SUM(CASE WHEN Per.AGMPSAdmin IS NOT NULL THEN Per.AGMPSAdmin END)/SUM(CASE WHEN Per.AGMPSAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGSupport = SUM(CASE WHEN Per.AGMPSNonMaint IS NOT NULL THEN Per.AGMPSNonMaint END)/SUM(CASE WHEN Per.AGMPSNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSAGTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGMPSTotal,0))/SUM(nf.NMC) END * 100,
				MPSContractOps = SUM(CASE WHEN Per.ContractMPSOper IS NOT NULL THEN Per.ContractMPSOper END)/SUM(CASE WHEN Per.ContractMPSOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractNonOvhlMaint = SUM(CASE WHEN Per.ContractMPSNOHMaintInsp IS NOT NULL THEN Per.ContractMPSNOHMaintInsp END)/SUM(CASE WHEN Per.ContractMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractInspectors = SUM(CASE WHEN Per.ContractMPSInsp IS NOT NULL THEN Per.ContractMPSInsp END)/SUM(CASE WHEN Per.ContractMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractLumpSumContract = SUM(CASE WHEN Per.ContractMPSLSCAdj IS NOT NULL THEN Per.ContractMPSLSCAdj END)/SUM(CASE WHEN Per.ContractMPSLSCAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractLTSA = SUM(CASE WHEN Per.ContractMPSLTSA IS NOT NULL THEN Per.ContractMPSLTSA END)/SUM(CASE WHEN Per.ContractMPSLTSA IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractOvhlMaint = SUM(CASE WHEN Per.ContractMPSOHAdj IS NOT NULL THEN Per.ContractMPSOHAdj END)/SUM(CASE WHEN Per.ContractMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
				MPSContractTechnical = SUM(CASE WHEN Per.ContractMPSTech IS NOT NULL THEN Per.ContractMPSTech END)/SUM(CASE WHEN Per.ContractMPSTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractAdmin = SUM(CASE WHEN Per.ContractMPSAdmin IS NOT NULL THEN Per.ContractMPSAdmin END)/SUM(CASE WHEN Per.ContractMPSAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractSupport = SUM(CASE WHEN Per.ContractMPSNonMaint IS NOT NULL THEN Per.ContractMPSNonMaint END)/SUM(CASE WHEN Per.ContractMPSNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
				MPSContractTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractMPSTotal,0))/SUM(nf.NMC) END * 100
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				INNER JOIN PersMWByRefnum per ON Per.Refnum = t.Refnum
				INNER JOIN PersSTCalcMWByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			----STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it
		--DECLARE @temptable TABLE (totcash REAL)
		--DECLARE @minvalue REAL
		--DECLARE @maxvalue REAL

		EXEC ('		DECLARE @temptable TABLE (people REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL
		
		
		INSERT @temptable
		SELECT TOP 2 gs.TotEffPersMW AS people
		FROM TSort t 
			INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
			INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
			INNER JOIN PersMWByRefnum per ON Per.Refnum = t.Refnum
			INNER JOIN PersSTCalcMWByRefnum pst ON pst.Refnum = t.Refnum 
			INNER JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
		' WHERE ' + @Where +
				' ORDER BY gs.TotEffPersMW ASC 


		SELECT @minvalue =AVG(people) FROM @temptable 

		DELETE FROM @temptable 

		
		INSERT @temptable
		SELECT TOP 2 gs.TotEffPersMW AS people
		FROM TSort t 
			INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
			INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
			INNER JOIN PersMWByRefnum per ON Per.Refnum = t.Refnum
			INNER JOIN PersSTCalcMWByRefnum pst ON pst.Refnum = t.Refnum 
			INNER JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
		' WHERE ' + @Where +
				' ORDER BY gs.TotEffPersMW DESC 

		SELECT @maxvalue = AVG(people)  FROM @temptable 
		
		

			
			INSERT ReportGroupPersonnelMWData (RefNum,
					ReportTitle,
					ListName,
					TotEffPersonnel,
					MinimumPersonnel,
					MaximumPersonnel,
					OCCTotalOps,
					OCCTotalNonOvhlMaint,
					OCCTotalInspectors,
					OCCTotalLumpSumContract,
					OCCTotalLTSA,
					OCCTotalOvhlMaint,
					OCCTotalSubTotalMaint,
					OCCTotalTechnical,
					OCCTotalAdmin,
					OCCTotalSupport,
					OCCTotalTotal,
					MPSTotalOps,
					MPSTotalNonOvhlMaint,
					MPSTotalInspectors,
					MPSTotalLumpSumContract,
					MPSTotalLTSA,
					MPSTotalOvhlMaint,
					MPSTotalSubTotalMaint,
					MPSTotalTechnical,
					MPSTotalAdmin,
					MPSTotalSupport,
					MPSTotalTotal,
					OCCSiteOps,
					OCCSiteNonOvhlMaint,
					OCCSiteInspectors,
					OCCSiteOvhlMaint,
					OCCSiteSubTotalMaint,
					OCCSiteTechnical,
					OCCSiteAdmin,
					OCCSiteSupport,
					OCCSiteTotal,
					OCCCentralNonOvhlMaint,
					OCCCentralInspectors,
					OCCCentralOvhlMaint,
					OCCCentralSubTotalMaint,
					OCCCentralTotal,
					OCCAGOps,
					OCCAGNonOvhlMaint,
					OCCAGInspectors,
					OCCAGOvhlMaint,
					OCCAGSubTotalMaint,
					OCCAGTechnical,
					OCCAGAdmin,
					OCCAGSupport,
					OCCAGTotal,
					OCCContractOps,
					OCCContractNonOvhlMaint,
					OCCContractInspectors,
					OCCContractLumpSumContract,
					OCCContractLTSA,
					OCCContractOvhlMaint,
					OCCContractSubTotalMaint,
					OCCContractTechnical,
					OCCContractAdmin,
					OCCContractSupport,
					OCCContractTotal,
					MPSSiteOps,
					MPSSiteNonOvhlMaint,
					MPSSiteInspectors,
					MPSSiteOvhlMaint,
					MPSSiteSubTotalMaint,
					MPSSiteTechnical,
					MPSSiteAdmin,
					MPSSiteSupport,
					MPSSiteTotal,
					MPSCentralNonOvhlMaint,
					MPSCentralInspectors,
					MPSCentralOvhlMaint,
					MPSCentralSubTotalMaint,
					MPSCentralTotal,
					MPSAGOps,
					MPSAGNonOvhlMaint,
					MPSAGInspectors,
					MPSAGOvhlMaint,
					MPSAGSubTotalMaint,
					MPSAGTechnical,
					MPSAGAdmin,
					MPSAGSupport,
					MPSAGTotal,
					MPSContractOps,
					MPSContractNonOvhlMaint,
					MPSContractInspectors,
					MPSContractLumpSumContract,
					MPSContractLTSA,
					MPSContractOvhlMaint,
					MPSContractSubTotalMaint,
					MPSContractTechnical,
					MPSContractAdmin,
					MPSContractSupport,
					MPSContractTotal)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					TotEffPersonnel = GlobalDB.dbo.WtAvg(gs.TotEffPersMW, nf.NMC),
					MinimumPersonnel = @minvalue,
					MaximumPersonnel = @maxvalue,
					OCCTotalOps = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCOper, nf.NMC),
					OCCTotalNonOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCNOHMaintInsp, nf.NMC),
					OCCTotalInspectors = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCInsp, nf.NMC),
					OCCTotalLumpSumContract = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCLSCAdj, nf.NMC),
					OCCTotalLTSA = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCLTSA, nf.NMC),
					OCCTotalOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCOHAdj, nf.NMC),
					OCCTotalSubTotalMaint = GlobalDB.dbo.WtAvg(Pst.TotMWOCCSubtotalMaint, nf.NMC),
					OCCTotalTechnical = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCTech, nf.NMC),
					OCCTotalAdmin = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCAdmin, nf.NMC),
					OCCTotalSupport = GlobalDB.dbo.WtAvgNN(Per.TotMWOCCNonMaint, nf.NMC),
					OCCTotalTotal = GlobalDB.dbo.WtAvg(Pst.TotMWOCCTotal, nf.NMC),
					MPSTotalOps = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSOper, nf.NMC),
					MPSTotalNonOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSNOHMaintInsp, nf.NMC),
					MPSTotalInspectors = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSInsp, nf.NMC),
					MPSTotalLumpSumContract = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSLSCAdj, nf.NMC),
					MPSTotalLTSA = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSLTSA, nf.NMC),
					MPSTotalOvhlMaint = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSOHAdj, nf.NMC),
					MPSTotalSubTotalMaint = GlobalDB.dbo.WtAvg(Pst.TotMWMPSSubtotalMaint, nf.NMC),
					MPSTotalTechnical = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSTech, nf.NMC),
					MPSTotalAdmin = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSAdmin, nf.NMC),
					MPSTotalSupport = GlobalDB.dbo.WtAvgNN(Per.TotMWMPSNonMaint, nf.NMC),
					MPSTotalTotal = GlobalDB.dbo.WtAvg(Pst.TotMWMPSTotal, nf.NMC),
					OCCSiteOps = SUM(CASE WHEN Per.SiteOCCOper IS NOT NULL THEN Per.SiteOCCOper END)/SUM(CASE WHEN Per.SiteOCCOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteNonOvhlMaint = SUM(CASE WHEN Per.SiteOCCNOHMaintInsp IS NOT NULL THEN Per.SiteOCCNOHMaintInsp END)/SUM(CASE WHEN Per.SiteOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteInspectors = SUM(CASE WHEN Per.SiteOCCInsp IS NOT NULL THEN Per.SiteOCCInsp END)/SUM(CASE WHEN Per.SiteOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteOvhlMaint = SUM(CASE WHEN Per.SiteOCCOHAdj IS NOT NULL THEN Per.SiteOCCOHAdj END)/SUM(CASE WHEN Per.SiteOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					OCCSiteTechnical = SUM(CASE WHEN Per.SiteOCCTech IS NOT NULL THEN Per.SiteOCCTech END)/SUM(CASE WHEN Per.SiteOCCTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteAdmin = SUM(CASE WHEN Per.SiteOCCAdmin IS NOT NULL THEN Per.SiteOCCAdmin END)/SUM(CASE WHEN Per.SiteOCCAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteSupport = SUM(CASE WHEN Per.SiteOCCNonMaint IS NOT NULL THEN Per.SiteOCCNonMaint END)/SUM(CASE WHEN Per.SiteOCCNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCSiteTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteOCCTotal,0))/SUM(nf.NMC) END * 100,
					OCCCentralNonOvhlMaint = SUM(CASE WHEN Per.CentralOCCNOHMaintInsp IS NOT NULL THEN Per.CentralOCCNOHMaintInsp END)/SUM(CASE WHEN Per.CentralOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCCentralInspectors = SUM(CASE WHEN Per.CentralOCCInsp IS NOT NULL THEN Per.CentralOCCInsp END)/SUM(CASE WHEN Per.CentralOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCCentralOvhlMaint = SUM(CASE WHEN Per.CentralOCCOHAdj IS NOT NULL THEN Per.CentralOCCOHAdj END)/SUM(CASE WHEN Per.CentralOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCCentralSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					OCCCentralTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralOCCTotal,0))/SUM(nf.NMC) END * 100,
					OCCAGOps = SUM(CASE WHEN Per.AGOCCOper IS NOT NULL THEN Per.AGOCCOper END)/SUM(CASE WHEN Per.AGOCCOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGNonOvhlMaint = SUM(CASE WHEN Per.AGOCCNOHMaintInsp IS NOT NULL THEN Per.AGOCCNOHMaintInsp END)/SUM(CASE WHEN Per.AGOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGInspectors = SUM(CASE WHEN Per.AGOCCInsp IS NOT NULL THEN Per.AGOCCInsp END)/SUM(CASE WHEN Per.AGOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGOvhlMaint = SUM(CASE WHEN Per.AGOCCOHAdj IS NOT NULL THEN Per.AGOCCOHAdj END)/SUM(CASE WHEN Per.AGOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					OCCAGTechnical = SUM(CASE WHEN Per.AGOCCTech IS NOT NULL THEN Per.AGOCCTech END)/SUM(CASE WHEN Per.AGOCCTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGAdmin = SUM(CASE WHEN Per.AGOCCAdmin IS NOT NULL THEN Per.AGOCCAdmin END)/SUM(CASE WHEN Per.AGOCCAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGSupport = SUM(CASE WHEN Per.AGOCCNonMaint IS NOT NULL THEN Per.AGOCCNonMaint END)/SUM(CASE WHEN Per.AGOCCNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCAGTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGOCCTotal,0))/SUM(nf.NMC) END * 100,
					OCCContractOps = SUM(CASE WHEN Per.ContractOCCOper IS NOT NULL THEN Per.ContractOCCOper END)/SUM(CASE WHEN Per.ContractOCCOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractNonOvhlMaint = SUM(CASE WHEN Per.ContractOCCNOHMaintInsp IS NOT NULL THEN Per.ContractOCCNOHMaintInsp END)/SUM(CASE WHEN Per.ContractOCCNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractInspectors = SUM(CASE WHEN Per.ContractOCCInsp IS NOT NULL THEN Per.ContractOCCInsp END)/SUM(CASE WHEN Per.ContractOCCInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractLumpSumContract = SUM(CASE WHEN Per.ContractOCCLSCAdj IS NOT NULL THEN Per.ContractOCCLSCAdj END)/SUM(CASE WHEN Per.ContractOCCLSCAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractLTSA = SUM(CASE WHEN Per.ContractOCCLTSA IS NOT NULL THEN Per.ContractOCCLTSA END)/SUM(CASE WHEN Per.ContractOCCLTSA IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractOvhlMaint = SUM(CASE WHEN Per.ContractOCCOHAdj IS NOT NULL THEN Per.ContractOCCOHAdj END)/SUM(CASE WHEN Per.ContractOCCOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractOCCSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					OCCContractTechnical = SUM(CASE WHEN Per.ContractOCCTech IS NOT NULL THEN Per.ContractOCCTech END)/SUM(CASE WHEN Per.ContractOCCTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractAdmin = SUM(CASE WHEN Per.ContractOCCAdmin IS NOT NULL THEN Per.ContractOCCAdmin END)/SUM(CASE WHEN Per.ContractOCCAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractSupport = SUM(CASE WHEN Per.ContractOCCNonMaint IS NOT NULL THEN Per.ContractOCCNonMaint END)/SUM(CASE WHEN Per.ContractOCCNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					OCCContractTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractOCCTotal,0))/SUM(nf.NMC) END * 100,
					MPSSiteOps = SUM(CASE WHEN Per.SiteMPSOper IS NOT NULL THEN Per.SiteMPSOper END)/SUM(CASE WHEN Per.SiteMPSOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteNonOvhlMaint = SUM(CASE WHEN Per.SiteMPSNOHMaintInsp IS NOT NULL THEN Per.SiteMPSNOHMaintInsp END)/SUM(CASE WHEN Per.SiteMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteInspectors = SUM(CASE WHEN Per.SiteMPSInsp IS NOT NULL THEN Per.SiteMPSInsp END)/SUM(CASE WHEN Per.SiteMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteOvhlMaint = SUM(CASE WHEN Per.SiteMPSOHAdj IS NOT NULL THEN Per.SiteMPSOHAdj END)/SUM(CASE WHEN Per.SiteMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					MPSSiteTechnical = SUM(CASE WHEN Per.SiteMPSTech IS NOT NULL THEN Per.SiteMPSTech END)/SUM(CASE WHEN Per.SiteMPSTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteAdmin = SUM(CASE WHEN Per.SiteMPSAdmin IS NOT NULL THEN Per.SiteMPSAdmin END)/SUM(CASE WHEN Per.SiteMPSAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteSupport = SUM(CASE WHEN Per.SiteMPSNonMaint IS NOT NULL THEN Per.SiteMPSNonMaint END)/SUM(CASE WHEN Per.SiteMPSNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSSiteTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.SiteMPSTotal,0))/SUM(nf.NMC) END * 100,
					MPSCentralNonOvhlMaint = SUM(CASE WHEN Per.CentralMPSNOHMaintInsp IS NOT NULL THEN Per.CentralMPSNOHMaintInsp END)/SUM(CASE WHEN Per.CentralMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSCentralInspectors = SUM(CASE WHEN Per.CentralMPSInsp IS NOT NULL THEN Per.CentralMPSInsp END)/SUM(CASE WHEN Per.CentralMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSCentralOvhlMaint = SUM(CASE WHEN Per.CentralMPSOHAdj IS NOT NULL THEN Per.CentralMPSOHAdj END)/SUM(CASE WHEN Per.CentralMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSCentralSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					MPSCentralTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.CentralMPSTotal,0))/SUM(nf.NMC) END * 100,
					MPSAGOps = SUM(CASE WHEN Per.AGMPSOper IS NOT NULL THEN Per.AGMPSOper END)/SUM(CASE WHEN Per.AGMPSOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGNonOvhlMaint = SUM(CASE WHEN Per.AGMPSNOHMaintInsp IS NOT NULL THEN Per.AGMPSNOHMaintInsp END)/SUM(CASE WHEN Per.AGMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGInspectors = SUM(CASE WHEN Per.AGMPSInsp IS NOT NULL THEN Per.AGMPSInsp END)/SUM(CASE WHEN Per.AGMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGOvhlMaint = SUM(CASE WHEN Per.AGMPSOHAdj IS NOT NULL THEN Per.AGMPSOHAdj END)/SUM(CASE WHEN Per.AGMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					MPSAGTechnical = SUM(CASE WHEN Per.AGMPSTech IS NOT NULL THEN Per.AGMPSTech END)/SUM(CASE WHEN Per.AGMPSTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGAdmin = SUM(CASE WHEN Per.AGMPSAdmin IS NOT NULL THEN Per.AGMPSAdmin END)/SUM(CASE WHEN Per.AGMPSAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGSupport = SUM(CASE WHEN Per.AGMPSNonMaint IS NOT NULL THEN Per.AGMPSNonMaint END)/SUM(CASE WHEN Per.AGMPSNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSAGTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.AGMPSTotal,0))/SUM(nf.NMC) END * 100,
					MPSContractOps = SUM(CASE WHEN Per.ContractMPSOper IS NOT NULL THEN Per.ContractMPSOper END)/SUM(CASE WHEN Per.ContractMPSOper IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractNonOvhlMaint = SUM(CASE WHEN Per.ContractMPSNOHMaintInsp IS NOT NULL THEN Per.ContractMPSNOHMaintInsp END)/SUM(CASE WHEN Per.ContractMPSNOHMaintInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractInspectors = SUM(CASE WHEN Per.ContractMPSInsp IS NOT NULL THEN Per.ContractMPSInsp END)/SUM(CASE WHEN Per.ContractMPSInsp IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractLumpSumContract = SUM(CASE WHEN Per.ContractMPSLSCAdj IS NOT NULL THEN Per.ContractMPSLSCAdj END)/SUM(CASE WHEN Per.ContractMPSLSCAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractLTSA = SUM(CASE WHEN Per.ContractMPSLTSA IS NOT NULL THEN Per.ContractMPSLTSA END)/SUM(CASE WHEN Per.ContractMPSLTSA IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractOvhlMaint = SUM(CASE WHEN Per.ContractMPSOHAdj IS NOT NULL THEN Per.ContractMPSOHAdj END)/SUM(CASE WHEN Per.ContractMPSOHAdj IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractSubTotalMaint = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractMPSSubtotalMaint,0))/SUM(nf.NMC) END * 100,
					MPSContractTechnical = SUM(CASE WHEN Per.ContractMPSTech IS NOT NULL THEN Per.ContractMPSTech END)/SUM(CASE WHEN Per.ContractMPSTech IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractAdmin = SUM(CASE WHEN Per.ContractMPSAdmin IS NOT NULL THEN Per.ContractMPSAdmin END)/SUM(CASE WHEN Per.ContractMPSAdmin IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractSupport = SUM(CASE WHEN Per.ContractMPSNonMaint IS NOT NULL THEN Per.ContractMPSNonMaint END)/SUM(CASE WHEN Per.ContractMPSNonMaint IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END) * 100,
					MPSContractTotal = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(Pst.ContractMPSTotal,0))/SUM(nf.NMC) END * 100
				FROM TSort t 
					INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
					INNER JOIN PersMWByRefnum per ON Per.Refnum = t.Refnum
					INNER JOIN PersSTCalcMWByRefnum pst ON pst.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END






