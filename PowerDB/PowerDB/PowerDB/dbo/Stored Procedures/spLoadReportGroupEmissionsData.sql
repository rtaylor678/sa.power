﻿



CREATE PROC [dbo].[spLoadReportGroupEmissionsData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

	--need to use a temp table, because we'll be using multiple sources and need multiple queries to get the final output
	CREATE TABLE #tmp ([SO2LbsPerMWH] [real] NULL,
		[NOxLbsPerMWH] [real] NULL,
		[CO2TonsPerMWH] [real] NULL,
		[SO2Emitted] [real] NULL,
		[SO2Allowed] [real] NULL,
		[SO2Pur] [real] NULL,
		[SO2Sold] [real] NULL,
		[NOxEmitted] [real] NULL,
		[NOxAllowed] [real] NULL,
		[NOxPur] [real] NULL,
		[NOxSold] [real] NULL,
		[CO2Emitted] [real] NULL,
		[CO2Allowed] [real] NULL,
		[CO2Pur] [real] NULL,
		[CO2Sold] [real] NULL)
	
	--remove the existing record in the table
	DELETE FROM ReportGroupEmissionsData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


			INSERT #tmp ( 
				SO2LbsPerMWH,
				NOxLbsPerMWH,
				CO2TonsPerMWH)

			SELECT SO2LbsPerMWH = GlobalDB.dbo.WtAvgNZ(e.SO2LbsPerMWH,g.PotentialGrossMWH ),
				NOxLbsPerMWH = GlobalDB.dbo.WtAvgNZ(e.NOxLbsPerMWH ,g.PotentialGrossMWH ),
				CO2TonsPerMWH = GlobalDB.dbo.WtAvgNZ(e.CO2TonsPerMWH ,g.PotentialGrossMWH )

			FROM [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join Emissions e on e.Refnum = rl.Refnum 
				inner join GenerationTotCalc g on g.Refnum = e.Refnum 
				INNER JOIN Breaks b ON b.Refnum = e.Refnum
				inner join _RankView r on r.Refnum = e.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
				
				
				
				

			INSERT #tmp (SO2Emitted,
				SO2Allowed,
				SO2Pur,
				SO2Sold,
				NOxEmitted,
				NOxAllowed,
				NOxPur,
				NOxSold,
				CO2Emitted,
				CO2Allowed,
				CO2Pur,
				CO2Sold)

			SELECT SO2Emitted = AVG(m.SO2Emitted),
				SO2Allowed = AVG(m.SO2Allow),
				SO2Pur = AVG(m.SO2Pur),
				SO2Sold = AVG(m.SO2Sold),
				NOxEmitted = AVG(m.NOxEmitted),
				NOxAllowed = AVG(m.NOxAllow),
				NOxPur = AVG(m.NOxPur),
				NOxSold = AVG(m.NOxSold),
				CO2Emitted = AVG(m.CO2Emitted),
				CO2Allowed = AVG(m.CO2Allow),
				CO2Pur = AVG(m.CO2Pur),
				CO2Sold = AVG(m.CO2Sold)

			FROM [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join Emissions e on e.Refnum = rl.Refnum 
				inner join GenerationTotCalc g on g.Refnum = e.Refnum 
				inner join Misc m on m.refnum = e.refnum
				INNER JOIN Breaks b ON b.Refnum = e.Refnum
				inner join _RankView r on r.Refnum = e.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
				
				
				INSERT ReportGroupEmissionsData (RefNum, 
				ReportTitle, 
				ListName, 
				SO2LbsPerMWH,
				NOxLbsPerMWH,
				CO2TonsPerMWH,
				SO2Emitted,
				SO2Allowed,
				SO2Pur,
				SO2Sold,
				NOxEmitted,
				NOxAllowed,
				NOxPur,
				NOxSold,
				CO2Emitted,
				CO2Allowed,
				CO2Pur,
				CO2Sold)
					
				SELECT @ReportRefnum, 
					@ReportTitle,
					@ListName,
					SO2LbsPerMWH = SUM(SO2LbsPerMWH),
					NOxLbsPerMWH = SUM(NOxLbsPerMWH),
					CO2TonsPerMWH = SUM(CO2TonsPerMWH),
					SO2Emitted = SUM(SO2Emitted),
					SO2Allowed = SUM(SO2Allowed),
					SO2Pur = SUM(SO2Pur),
					SO2Sold = SUM(SO2Sold),
					NOxEmitted = SUM(NOxEmitted),
					NOxAllowed = SUM(NOxAllowed),
					NOxPur = SUM(NOxPur),
					NOxSold = SUM(NOxSold),
					CO2Emitted = SUM(CO2Emitted),
					CO2Allowed = SUM(CO2Allowed),
					CO2Pur = SUM(CO2Pur),
					CO2Sold = SUM(CO2Sold)
					FROM #tmp
					
					
				
				
				
		END

	ELSE 
		BEGIN
			--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 'e.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''

			--first insert to the tmp table - note that this query does not use the table MISC
			EXEC ('INSERT #tmp (SO2LbsPerMWH,
				NOxLbsPerMWH,
				CO2TonsPerMWH)
					
				SELECT SO2LbsPerMWH = GlobalDB.dbo.WtAvgNZ(e.SO2LbsPerMWH,g.PotentialGrossMWH ),
					NOxLbsPerMWH = GlobalDB.dbo.WtAvgNZ(e.NOxLbsPerMWH ,g.PotentialGrossMWH ),
					CO2TonsPerMWH = GlobalDB.dbo.WtAvgNZ(e.CO2TonsPerMWH ,g.PotentialGrossMWH )
				FROM Emissions e
					INNER JOIN GenerationTotCalc g on g.Refnum = e.Refnum 
					INNER JOIN Breaks b ON b.Refnum = e.Refnum
					INNER JOIN TSort t on t.Refnum = e.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = e.Refnum' + @Turbine + 
				' WHERE ' + @Where)

			--second insert to the tmp table - note that this query uses the table MISC
			EXEC ('INSERT #tmp (SO2Emitted,
				SO2Allowed,
				SO2Pur,
				SO2Sold,
				NOxEmitted,
				NOxAllowed,
				NOxPur,
				NOxSold,
				CO2Emitted,
				CO2Allowed,
				CO2Pur,
				CO2Sold)
					
				SELECT SO2Emitted = AVG(m.SO2Emitted),
					SO2Allowed = AVG(m.SO2Allow),
					SO2Pur = AVG(m.SO2Pur),
					SO2Sold = AVG(m.SO2Sold),
					NOxEmitted = AVG(m.NOxEmitted),
					NOxAllowed = AVG(m.NOxAllow),
					NOxPur = AVG(m.NOxPur),
					NOxSold = AVG(m.NOxSold),
					CO2Emitted = AVG(m.CO2Emitted),
					CO2Allowed = AVG(m.CO2Allow),
					CO2Pur = AVG(m.CO2Pur),
					CO2Sold = AVG(m.CO2Sold)
				FROM Emissions e
					inner join GenerationTotCalc g on g.Refnum = e.Refnum 
					inner join Misc m on m.refnum = e.refnum
					INNER JOIN Breaks b ON b.Refnum = e.Refnum
					INNER JOIN TSort t on t.Refnum = e.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = e.Refnum' + @Turbine + 
				' WHERE ' + @Where)


			--and this is the final load to the output table
			EXEC ('INSERT ReportGroupEmissionsData (RefNum, 
				ReportTitle, 
				ListName, 
				SO2LbsPerMWH,
				NOxLbsPerMWH,
				CO2TonsPerMWH,
				SO2Emitted,
				SO2Allowed,
				SO2Pur,
				SO2Sold,
				NOxEmitted,
				NOxAllowed,
				NOxPur,
				NOxSold,
				CO2Emitted,
				CO2Allowed,
				CO2Pur,
				CO2Sold)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					SO2LbsPerMWH = SUM(SO2LbsPerMWH),
					NOxLbsPerMWH = SUM(NOxLbsPerMWH),
					CO2TonsPerMWH = SUM(CO2TonsPerMWH),
					SO2Emitted = SUM(SO2Emitted),
					SO2Allowed = SUM(SO2Allowed),
					SO2Pur = SUM(SO2Pur),
					SO2Sold = SUM(SO2Sold),
					NOxEmitted = SUM(NOxEmitted),
					NOxAllowed = SUM(NOxAllowed),
					NOxPur = SUM(NOxPur),
					NOxSold = SUM(NOxSold),
					CO2Emitted = SUM(CO2Emitted),
					CO2Allowed = SUM(CO2Allowed),
					CO2Pur = SUM(CO2Pur),
					CO2Sold = SUM(CO2Sold)
					FROM #tmp')
			
		END


