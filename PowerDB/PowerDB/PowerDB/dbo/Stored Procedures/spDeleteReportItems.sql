﻿CREATE PROCEDURE spDeleteReportItems
		@ReportName varchar(30) = NULL,
		@ReportID DD_ID = NULL
AS
IF @ReportID IS NULL
	SELECT @ReportID = ReportID
	FROM ReportDef
	WHERE ReportName = @ReportName
DELETE FROM ReportItems
WHERE ReportID = @ReportID
