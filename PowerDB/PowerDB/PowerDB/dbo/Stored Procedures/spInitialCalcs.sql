﻿CREATE PROC [dbo].[spInitialCalcs](@Refnum Refnum)
AS

UPDATE OHMaint
SET Component = 'SITETRAN', ProjectID = CASE WHEN ID = 600110 THEN 'OVHL' ELSE 'OTHER' END
FROM OHMaint INNER JOIN Equipment e ON e.Refnum = OHMaint.Refnum AND e.TurbineID = OHMaint.TurbineID AND e.EquipID = OHMaint.EquipID
WHERE OHMaint.Refnum = @Refnum AND e.EquipType = 'SITETRAN' AND OHMaint.Component = 'BOIL'

UPDATE OHMaint
SET ProjectID = 'OTHER'
WHERE Refnum = @Refnum AND Component = 'SITETRAN' AND ProjectID = 'FWHEAT'

UPDATE CTGData
SET FiringTemp = CTGSupplement.FiringTemp
FROM CTGData INNER JOIN CTGSupplement
	ON CTGData.Refnum = CTGSupplement.Refnum AND CTGData.TurbineID = CTGSupplement.TurbineID
WHERE CTGData.FiringTemp IS NULL AND CTGData.Refnum = @Refnum

UPDATE PowerGeneration
SET NetMWH = ISNULL(GrossMWH, 0) - ISNULL(StationSvcElec, 0)
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE Refnum = @Refnum AND StudyYear >= 1999)

UPDATE PowerGeneration
SET ElecMBTU = NetMWH*3.41214
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE Refnum = @Refnum AND StudyYear >= 1999)
