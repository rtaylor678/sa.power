﻿



CREATE PROC [dbo].[spLoadReportGroupCogenData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)


	--remove the existing record in the table
	DELETE FROM ReportGroupCogenData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )



			INSERT ReportGroupCogenData (RefNum, 
				ReportTitle, 
				ListName, 
				PercentThermal,
				SteamRedundancyFactor,
				ProcCycleMassReturns,
				TotEnergyOutput,
				ThermalEffPowerBlock,
				ThermEffCTG,
				HeatRateElectricOnly,
				NCFThermalPct,
				EUFThermalPct,
				NCFElectricPct,
				NOFElectricPct,
				EFORElectricPct,
				TotCashExpendMBTU,
				TotCashLessFuelMBTU,
				PlantManageableExpMBTU,
				MaintenanceIndexMBTU)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				PercentThermal = GlobalDB.dbo.WtAvgNN(cc.ThermPcnt, cc.TotExportMBTU),
				SteamRedundancyFactor = GlobalDB.dbo.WtAvg(cc.StmRedFactor, cc.MaxMBTUExport),
				ProcCycleMassReturns = GlobalDB.dbo.WtAvgNZ(cc.StmReturnPcnt, cc.NetStmExportKLb),
				TotEnergyOutput = AVG(CASE WHEN cc.TotExportMBTU <> 0 THEN cc.TotExportMBTU END) * 0.000001,
				ThermalEffPowerBlock = GlobalDB.dbo.WtAvgNZ(cc.ThermEff, cc.TotInputMBTU),
				ThermEffCTG = GlobalDB.dbo.WtAvgNZ(cc.CTGThermEff, cc.CTGFuelMBTU),
				HeatRateElectricOnly = GlobalDB.dbo.WtAvgNZ(cc.ElecHeatRate, gtc.NetMWH),
				NCFThermalPct = GlobalDB.dbo.WtAvgNZ(CASE WHEN cc.ThermNCF < 101 THEN cc.ThermNCF END, cc.MaxStmMakeMBTU),
				EUFThermalPct = GlobalDB.dbo.WtAvgNN(cc.ThermEUF, cc.MaxMBTUExport),
				NCFElectricPct = GlobalDB.dbo.WtAvgNN(cc.ElecNCF, cc.MaxElecMWH),
				NOFElectricPct = GlobalDB.dbo.WtAvgNN(cc.ElecNOF, cc.MaxElecMWH),
				EFORElectricPct = GlobalDB.dbo.WtAvgNN(cc.ElecEFOR, cc.MaxElecMWH),
				TotCashExpendMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotCashOpexMBTU, cc.TotExportMBTU),
				TotCashLessFuelMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotCashLessFuelMBTU, cc.TotExportMBTU),
				PlantManageableExpMBTU = GlobalDB.dbo.WtAvgNZ(cc.PlantMngMBTU, cc.TotExportMBTU),
				MaintenanceIndexMBTU = GlobalDB.dbo.WtAvgNZ(cc.AnnMaintCostMBTU, cc.TotExportMBTU)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join CogenCalc cc on cc.Refnum = rl.Refnum 
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = cc.Refnum
				INNER JOIN TSort t ON t.Refnum = cc.Refnum
				LEFT JOIN StartsAnalysis st on st.Refnum = cc.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = cc.Refnum
				inner join _RankView r on r.Refnum = cc.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 'cc.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

		
			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''


			--and this is the actual query being run
			EXEC ('

			
			INSERT ReportGroupCogenData (RefNum, 
					ReportTitle, 
					ListName, 
					PercentThermal,
					SteamRedundancyFactor,
					ProcCycleMassReturns,
					TotEnergyOutput,
					ThermalEffPowerBlock,
					ThermEffCTG,
					HeatRateElectricOnly,
					NCFThermalPct,
					EUFThermalPct,
					NCFElectricPct,
					NOFElectricPct,
					EFORElectricPct,
					TotCashExpendMBTU,
					TotCashLessFuelMBTU,
					PlantManageableExpMBTU,
					MaintenanceIndexMBTU)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					PercentThermal = GlobalDB.dbo.WtAvgNN(cc.ThermPcnt, cc.TotExportMBTU),
					SteamRedundancyFactor = GlobalDB.dbo.WtAvg(cc.StmRedFactor, cc.MaxMBTUExport),
					ProcCycleMassReturns = GlobalDB.dbo.WtAvgNZ(cc.StmReturnPcnt, cc.NetStmExportKLb),
					TotEnergyOutput = AVG(CASE WHEN cc.TotExportMBTU <> 0 THEN cc.TotExportMBTU END) * 0.000001,
					ThermalEffPowerBlock = GlobalDB.dbo.WtAvgNZ(cc.ThermEff, cc.TotInputMBTU),
					ThermEffCTG = GlobalDB.dbo.WtAvgNZ(cc.CTGThermEff, cc.CTGFuelMBTU),
					HeatRateElectricOnly = GlobalDB.dbo.WtAvgNZ(cc.ElecHeatRate, gtc.NetMWH),
					NCFThermalPct = GlobalDB.dbo.WtAvgNZ(CASE WHEN cc.ThermNCF < 101 THEN cc.ThermNCF END, cc.MaxStmMakeMBTU),
					EUFThermalPct = GlobalDB.dbo.WtAvgNN(cc.ThermEUF, cc.MaxMBTUExport),
					NCFElectricPct = GlobalDB.dbo.WtAvgNN(cc.ElecNCF, cc.MaxElecMWH),
					NOFElectricPct = GlobalDB.dbo.WtAvgNN(cc.ElecNOF, cc.MaxElecMWH),
					EFORElectricPct = GlobalDB.dbo.WtAvgNN(cc.ElecEFOR, cc.MaxElecMWH),
					TotCashExpendMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotCashOpexMBTU, cc.TotExportMBTU),
					TotCashLessFuelMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotCashLessFuelMBTU, cc.TotExportMBTU),
					PlantManageableExpMBTU = GlobalDB.dbo.WtAvgNZ(cc.PlantMngMBTU, cc.TotExportMBTU),
					MaintenanceIndexMBTU = GlobalDB.dbo.WtAvgNZ(cc.AnnMaintCostMBTU, cc.TotExportMBTU)
				FROM CogenCalc cc
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = cc.Refnum
					INNER JOIN TSort t ON t.Refnum = cc.Refnum
					INNER JOIN Breaks b ON b.Refnum = cc.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = cc.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END





