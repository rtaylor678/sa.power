﻿
CREATE PROCEDURE [dbo].[spGetRankQuartiles] (@ListName VARCHAR(30), @RankVariableID SMALLINT, @BreakCondition VARCHAR(30), @BreakValue CHAR(12))

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Min FLOAT
	DECLARE @Q12 FLOAT
	DECLARE @Q23 FLOAT
	DECLARE @Q34 FLOAT
	DECLARE @Max FLOAT
	DECLARE @PS FLOAT
	DECLARE @NumTiles TINYINT

	SELECT * INTO #tmp 
	FROM _RankView 
	WHERE ListName = @ListName AND RankVariableID = @RankVariableID AND BreakCondition = @BreakCondition AND BreakValue = @BreakValue

	SELECT @Min = AVG(Value) FROM #tmp WHERE Rank <=2

	SELECT @Q12 = AVG(value) FROM #tmp WHERE Rank IN (SELECT MIN(rank) FROM #tmp WHERE tile = 2) OR Rank IN (SELECT MAX(rank) FROM  #tmp WHERE tile = 1)
	SELECT @Q23 = AVG(value) FROM #tmp WHERE Rank IN (SELECT MIN(rank) FROM #tmp WHERE tile = 3) OR Rank IN (SELECT MAX(rank) FROM  #tmp WHERE tile = 2)
	SELECT @Q34 = AVG(value) FROM #tmp WHERE Rank IN (SELECT MIN(rank) FROM #tmp WHERE tile = 4) OR Rank IN (SELECT MAX(rank) FROM  #tmp WHERE tile = 3)

	SELECT @Max = AVG(value) FROM #tmp WHERE Rank >= (datacount - 1)

	--SELECT @PS = AVG(value) FROM #tmp WHERE refnum IN (SELECT refnum FROM _RL WHERE ListName = LEFT(@ListName,7) + ' Pacesetters' AND UserGroup = @BreakValue)
	SELECT @PS = CASE @RankVariableID 
		WHEN 1 THEN HeatRate
		WHEN 6 THEN MaintIndexMWH
		WHEN 7 THEN OHIndexMWH
		WHEN 8 THEN NonOHIndexMWH
		WHEN 46 THEN PeakUnavail_Unp 
		WHEN 47 THEN UnplannedCommUnavailIndex
		WHEN 49 THEN PlannedCommUnavailIndex
		WHEN 55 THEN AuxPct
		WHEN 71 THEN TotCashLessFuelEmmEGC
		WHEN 78 THEN ThermEff
		WHEN 81 THEN LTSAIndexMWH
		WHEN 86 THEN TotCashLessFuelEmmMWH
		WHEN 88 THEN TotCashLessFuelEmmMW
		ELSE 0 END --if they're not defined here, how to handle this? just returning 0 ought to give an odd gap, yes?
		FROM CRVGapFactors WHERE StudyYear = '20' + SUBSTRING(@ListName,6,2) AND CRVSizeGroup = @BreakValue

	SELECT @NumTiles = MAX(NumTiles) FROM #tmp
	
	IF @NumTiles <= 3
		SET @Q34 = NULL
	IF @NumTiles <= 2
		SET @Q23 = NULL
	IF @NumTiles <= 1
		SET @Q12 = NULL
	
	DROP TABLE #tmp

	SELECT Min = @Min, Q12 = @Q12, Q23 = @Q23, Q34 = @Q34, Max = @Max, PS = @PS--, NumTiles = @NumTiles

END
