﻿CREATE PROCEDURE [dbo].[spInflateCarryoverExtra](@SiteID varchar(10), @InflFactor real)
AS

	UPDATE OpexLocal
	SET PropTax=PropTax*@InflFactor,
	OthTax=OthTax*@InflFactor,
	Insurance=Insurance*@InflFactor,
	OthVar=OthVar*@InflFactor,
	AGNonPers=AGNonPers*@InflFactor,
	Water=Water*@InflFactor
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

	UPDATE LTSA
	SET PaymentCurrKLocal = PaymentCurrKLocal*@InflFactor,
	PaymentPrevKLocal = PaymentPrevKLocal*@InflFactor,
	TotCostKLocal = TotCostKLocal*@InflFactor
	WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
