﻿

CREATE PROC [dbo].[SlideChooser](@Listname varchar(50), @StudyYear INT)
AS

	--this is to populate slides with the appropriate slides for the company

	--null out everything
	UPDATE Slides set Active = null

	select * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear)


	--All Coal by Size
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) and CoalNMCGroup2 = 1)
		UPDATE Slides SET Active = 1 where SubTitle = 'Coal &lt;210 MW'

	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) and CoalNMCGroup2 = 2)
		UPDATE Slides SET Active = 1 where SubTitle = 'Coal 210–549 MW'

	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) and CoalNMCGroup2 = 3)
		UPDATE Slides SET Active = 1 where SubTitle = 'Coal &ge;550 MW'

	--LSC
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup = 'LSC')
		UPDATE slides SET Active = 1 where SubTitle = 'Coal – Lignitic & Subbituminous (Super-Critical)' OR SubTitle like '%Super-Critical'

	--ESC
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup = 'ESC')
		UPDATE slides SET Active = 1 where SubTitle = 'Coal – Bituminous & Anthracitic (Super-Critical)' OR SubTitle like '%Super-Critical'

	--ED
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup = 'ED')
		UPDATE slides SET Active = 1 where SubTitle = 'Coal – Bituminous & Anthracitic (Sub-Critical)' OR SubTitle like '%Sub-Critical'

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'ED1')
			UPDATE slides SET Active = 1 where SubTitle = 'Coal – Bituminous & Anthracitic (Sub-Critical) &lt;300 MW' OR SubTitle like '%Sub-Critical'

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'ED2')
			UPDATE slides SET Active = 1 where SubTitle = 'Coal – Bituminous & Anthracitic (Sub-Critical) 300–500 MW' OR SubTitle like '%Sub-Critical'

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'ED3')
			UPDATE slides SET Active = 1 where SubTitle = 'Coal – Bituminous & Anthracitic (Sub-Critical) &ge;500 MW' OR SubTitle like '%Sub-Critical'

	--LB
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup = 'LB')
		UPDATE slides SET Active = 1 where SubTitle = 'Coal – Lignitic & Subbituminous (Sub-Critical)' OR SubTitle like '%Sub-Critical'

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'LB1')
			UPDATE slides SET Active = 1 where SubTitle = 'Coal – Lignitic & Subbituminous &lt;200 MW' OR SubTitle like '%Sub-Critical'

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'LB2')
			UPDATE slides SET Active = 1 where SubTitle = 'Coal – Lignitic & Subbituminous 200–450 MW' OR SubTitle like '%Sub-Critical'

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'LB3')
			UPDATE slides SET Active = 1 where SubTitle = 'Coal – Lignitic & Subbituminous &ge;450 MW' OR SubTitle like '%Sub-Critical'

	--CoalSulfurGroup
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CoalSulfurGroup = 'Lo' AND Scrubbers = 'Y')
		UPDATE slides SET Active = 1 where SubTitle = 'Peer Group &lt;0.4% Sulfur'

	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CoalSulfurGroup = 'Md' AND Scrubbers = 'Y')
		UPDATE slides SET Active = 1 where SubTitle = 'Peer Group 0.4–0.8% Sulfur'

	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CoalSulfurGroup = 'Hi' AND Scrubbers = 'Y')
		UPDATE slides SET Active = 1 where SubTitle = 'Peer Group &gt;0.8% Sulfur'

	--CE
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup IN ('CE1','CE2','CE3'))
		UPDATE slides SET Active = 1 where SubTitle IN ('Combined Cycle - Electric Only','Combined Cycle (Electric Only)')

	--better way to do this one?
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup IN ('CE1','CE2','CE3'))
		UPDATE slides SET Active = 1 where SubTitle IN ('Combined Cycle Electric Only – Balance of Plant','Combined Cycle Electric Only – Boiler','Combined Cycle Electric Only – CTG/HRSG','Combined Cycle Electric Only – Steam Turbine Generator','Company – Brayton – Electric Only')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'CE1')
			UPDATE slides SET Active = 1 where SubTitle IN ('Combined Cycle (Electric Only) &lt;300 MW','Combined Cycle Electric Only &lt;300 MW')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'CE2')
			UPDATE slides SET Active = 1 where SubTitle IN ('Combined Cycle (Electric Only) 300–550 MW','Combined Cycle Electric Only 300–550 MW')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'CE3')
			UPDATE slides SET Active = 1 where SubTitle IN ('Combined Cycle (Electric Only) &ge;550 MW','Combined Cycle Electric Only &ge;550 MW')

	--CG
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup IN ('CG1','CG2','CG3'))
		UPDATE slides SET Active = 1 where SubTitle IN ('Cogen','Cogeneration')

	--better way to do this one?
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup IN ('CG1','CG2','CG3'))
		UPDATE slides SET Active = 1 where SubTitle IN ('Cogen – Balance of Plant','Cogen – Boiler','Cogen – CTG/HRSG','Cogen – Steam Turbine Generator','Company – Cogen')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'CG1')
			UPDATE slides SET Active = 1 where SubTitle IN ('Cogen &lt;150 MW')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'CG2')
			UPDATE slides SET Active = 1 where SubTitle IN ('Cogen 150–400 MW')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'CG3')
			UPDATE slides SET Active = 1 where SubTitle IN ('Cogen &ge;400 MW')


	--GAS1
	--GAS2
		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'GAS1')
			UPDATE slides SET Active = 1 where SubTitle IN ('Rankine (Gas & Oil) &lt;325 MW')

		IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVSizeGroup = 'GAS2')
			UPDATE slides SET Active = 1 where SubTitle IN ('Rankine (Gas & Oil) &ge;325 MW')


	--GasNA
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear AND Continent = 'N America') AND FuelType = 'Gas')
		UPDATE slides SET Active = 1 where SubTitle IN ('Gas – US & Canada')
	--GasEUR
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear AND Continent = 'Europe') AND FuelType = 'Gas')
		UPDATE slides SET Active = 1 where SubTitle IN ('Gas – Europe')
	--GasWorld
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear AND Continent NOT IN ('N America', 'Europe')) AND FuelType = 'Gas')
		UPDATE slides SET Active = 1 where SubTitle IN ('Gas – World')


	--SGO
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup = 'GAS')
		UPDATE slides SET Active = 1 where SubTitle IN (
			'Rankine (Gas & Oil)','Company – Rankine (Gas & Oil)','Rankine Gas & Oil','Rankine (Gas & Oil) – Balance of Plant','Rankine (Gas & Oil) – Boiler','Rankine (Gas & Oil) – Turbine/Generator','Rankine (Gas & Oil) Units','Rankine (Gas & Oil) Units – Company')
			OR Title IN ('LRO Rankine (Gas & Oil) by Outage Type') --this for one specific slide
		

	--Brayton
	--CC
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup IN ('CG','SC','CE'))
		UPDATE slides SET Active = 1 where Name LIKE '%Brayton'

	--Coal
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND FuelGroup = 'Coal')
		UPDATE slides SET Active = 1 where Name LIKE '%Coal' OR Name LIKE 'P6Coal%'

	--CoalNA
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear AND Continent = 'N America') AND FuelType = 'Coal')
		UPDATE slides SET Active = 1 where SubTitle IN ('Coal – US & Canada')
	--CoalEUR
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear AND Continent = 'Europe') AND FuelType = 'Coal')
		UPDATE slides SET Active = 1 where SubTitle IN ('Coal – Europe')
	--CoalWorld
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear AND Continent NOT IN ('N America', 'Europe')) AND FuelType = 'Coal')
		UPDATE slides SET Active = 1 where SubTitle IN ('Coal – World')

	--NotCoal
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND FuelGroup <> 'Coal')
		UPDATE slides SET Active = 1 where Name LIKE '%Gas' AND SubTitle = 'Company' AND Title <> 'LRO Rankine (Gas & Oil) by Outage Type'

	--Oil
	IF EXISTS (SELECT * from FuelTotCalc where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND FuelType = 'Oil')
		UPDATE slides SET Active = 1 where SubTitle = 'Oil'

	--Rankine
	IF EXISTS (SELECT * from TSort where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND Refnum NOT LIKE '%CC%')
		UPDATE slides SET Active = 1 where Name LIKE '%Rankine'

	--SC
	IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND CRVGroup = 'SC')
		UPDATE slides SET Active = 1 where SubTitle LIKE 'Simple%'

	----Scrub
	--IF EXISTS (SELECT * from Breaks where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND Scrubbers = 'Y')
	--	UPDATE slides SET Active = 1 where Name LIKE '%Scrubbed')
	--TODO fix scrubbed slide

	--ALL
		UPDATE slides SET Active = 1 where SubTitle LIKE 'Study%'
		UPDATE slides SET Active = 1 where SubTitle = 'Unit'
		UPDATE slides SET Active = 1 where name IN ('RoodsTemplate','PricingHub')
		UPDATE slides SET Active = 1 where name LIKE 'WaterfallNew%'
		UPDATE slides SET Active = 1 where Title IN ('Gap Analysis','Gap Analysis Comparison', 'Unit Designations')
	
		--only give the Company title slides if there is more than one type
		DECLARE @Count INT
		SELECT @Count = COUNT(*) FROM (
			SELECT DISTINCT CASE 
				WHEN CRVGroup IN ('ED','ESC','LB','LSC') THEN 'Coal'
				WHEN CRVGroup IN ('GAS') THEN 'Gas'
				WHEN CRVGroup IN ('CG') THEN 'Cogen'
				WHEN CRVGroup IN ('CE','SC') THEN 'Elec'
				ELSE '' END AS TypeOfUnit
			FROM Breaks b
				LEFT JOIN TSort t ON t.Refnum = b.Refnum
			WHERE b.Refnum IN (select Refnum from _RL where ListName = @ListName) and b.Refnum in (select Refnum from TSort where StudyYear = @StudyYear)
			) b
	
		IF @Count > 1
		BEGIN
			UPDATE slides SET Active = 1 where SubTitle = 'Company' AND Title IN ('Unplanned LRO Analysis by Technology','LRO by Technology Type','Unplanned LRO by Technology Type')
		END

		UPDATE Slides SET Active = null where Name like 'perf%'

		UPDATE slides SET Active = 1 where Title = 'Template'
		UPDATE Slides SET Active = null where Name in ('Template5','Template20') -- unused slides
		UPDATE Slides SET Active = null where Name in ('Template11','Template19','Template21','Template22','Template35','Template49','Template50','Template51') -- unused slides 2015

		--remove the appropriate title slide
		IF EXISTS (SELECT * from TSort where Refnum in (select Refnum from _RL where ListName = @ListName) and Refnum in (select Refnum from TSort where StudyYear = @StudyYear) AND Refnum LIKE '%CC%')
			UPDATE slides SET Active = null where Name = 'Template1'
		ELSE
			UPDATE slides SET Active = null where Name = 'Template2'








