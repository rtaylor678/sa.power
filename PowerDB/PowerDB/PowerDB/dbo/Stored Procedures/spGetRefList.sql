﻿CREATE PROCEDURE spGetRefList @ListName RefListName AS
SELECT l.Refnum, CoLoc = dbo.CoLoc(l.Refnum), l.UserGroup
FROM RefList_LU rl INNER JOIN RefList l ON l.RefListNo = rl.RefListNo
WHERE rl.ListName = @ListName
ORDER BY l.UserGroup, l.Refnum
