﻿








CREATE         PROC spEGCCalcs (@Refnum Refnum)
AS
DECLARE @EGC real
DECLARE @Boilers tinyint, @STGs tinyint, @CTGs tinyint, @Scrubbers tinyint, @Prec tinyint, @Bag tinyint
DECLARE @CoalNDC real, @OilNDC real, @GasNDC real, 
	@STG_NDC real, @CTG_NDC real, @NDC real, @NOF real, @ServiceHrs real, @BlrPSIG real, @Age real,
	@AdjNetMWH real, @Sulfur real, @AshPcnt real, @FuelType varchar(8), @LoadType varchar(4)
DECLARE @OilMBTU real, @GasMBTU real, @NumUnitsAtSite real, @ElecGenStarts real
DECLARE @CoalSulfur real, @CoalAsh real, @OilSulfur real, @OilAsh real

SELECT @Boilers = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'BOIL'
SELECT @Scrubbers = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType IN ('DGS', 'WGS')
SELECT @Prec = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'PREC'
SELECT @Bag = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'BAG'
SELECT @STGs = COUNT(*) FROM Equipment WHERE Refnum = @Refnum AND EquipType = 'STG'
--SELECT @CTGs = COUNT(*) FROM CTGData WHERE Refnum = @Refnum
SELECT @CTGs = COUNT(*), @CTG_NDC = SUM(NDC) FROM NERCTurbine WHERE Refnum = @Refnum AND TurbineType = 'CTG'
SELECT @STGs = COUNT(*), @STG_NDC = SUM(NDC) FROM NERCTurbine WHERE Refnum = @Refnum AND TurbineType IN ('STG', 'CST')
SELECT @BlrPSIG = AVG(BlrPSIG) FROM DesignData d INNER JOIN NERCTurbine NT ON nt.Util_code = d.Utility AND nt.unit_code = d.unit and nt.refnum =@Refnum
SELECT @NDC = NDC, @NOF = NOF, @ServiceHrs = ServiceHrs, @Age = Age 
FROM NERCFactors WHERE Refnum = @Refnum
IF @Scrubbers > @Boilers
	SELECT @Scrubbers = @Boilers
IF @Prec > @Boilers 
	SELECT @Prec = @Boilers
IF @Bag > @Boilers 
	SELECT @Bag = @Boilers

SELECT @CoalNDC = SUM(CASE WHEN Design_Fuel_1 IN ('CC','LI','PC') THEN NDC ELSE 0 END) ,
@OilNDC = SUM(CASE WHEN Design_Fuel_1 IN ('OO','DI') THEN NDC ELSE 0 END),
@GasNDC = SUM(CASE WHEN Design_Fuel_1 IN ('GG','OG') THEN NDC ELSE 0 END)
FROM NERCTurbine n LEFT JOIN DesignData d ON d.Utility = n.Util_Code AND d.Unit = n.Unit_Code
WHERE Design_Fuel_1 Is NOT NULL AND TurbineType IN ('BLR') AND n.Refnum = @Refnum

SELECT @AdjNetMWH = AdjNetMWH FROM Gensum WHERE Refnum = @Refnum
SELECT @FuelType = FuelType FROM FuelTotCalc WHERE Refnum = @Refnum
SELECT @LoadType = LoadType FROM Breaks WHERE Refnum = @Refnum
SELECT @OilMBTU = SUM(MBTU) FROM Fuel 
WHERE Refnum = @Refnum AND FuelType IN ('Jet', 'FOil', 'Diesel') AND TurbineID IN ('STG','Site')
IF @OilMBTU IS NULL 
	SELECT @OilMBTU = 0
SELECT @GasMBTU = SUM(MBTU) FROM Fuel 
WHERE Refnum = @Refnum AND FuelType IN ('NatGas', 'OffGas', 'H2Gas') AND TurbineID IN ('STG','Site')
IF @GasMBTU IS NULL 
	SELECT @GasMBTU = 0

SELECT @CTG_NDC = 0 WHERE @CTG_NDC IS NULL
SELECT @STG_NDC = 0 WHERE @STG_NDC IS NULL

IF @CoalNDC IS NULL AND @OilNDC IS NULL AND @GasNDC IS NULL AND @CTGs = 0
BEGIN
	SELECT @CoalNDC = @NDC-@CTG_NDC WHERE @FuelType = 'Coal'
	SELECT @OilNDC = @NDC-@CTG_NDC WHERE @FuelType = 'Oil'
	SELECT @GasNDC = @NDC-@CTG_NDC WHERE @FuelType = 'Gas'
END
SELECT @CoalNDC = 0 WHERE @CoalNDC IS NULL
SELECT @OilNDC = 0 WHERE @OilNDC IS NULL
SELECT @GasNDC = 0 WHERE @GasNDC IS NULL

SELECT @NumUnitsAtSite = TotExclSCNum from UnitGenData WHERE Refnum = @Refnum
SELECT @ElecGenStarts = SUM(TotalOpps) 
FROM StartsAnalysis sa INNER JOIN NERCTurbine nt ON nt.Refnum = sa.Refnum AND nt.TurbineID = sa.TurbineID
WHERE nt.TurbineType IN ('CTG', 'STG', 'CST') AND sa.Refnum = @Refnum


SELECT @CoalSulfur = SulfurPcnt, @CoalAsh = AshPcnt FROM CoalTotCalc
WHERE Refnum = @Refnum
SELECT @OilSulfur = Sulfur, @OilAsh = 0.1 FROM FuelProperties WHERE Refnum = @Refnum
IF @OilSulfur IS NULL
	SELECT @OilSulfur = 2.3, @OilAsh = 0.1 
SELECT @Sulfur = (ISNULL(@CoalSulfur,0)*@CoalNDC + ISNULL(@OilSulfur,0)*@OilNDC)/@NDC
SELECT @AshPcnt = (ISNULL(@CoalAsh,0)*@CoalNDC + ISNULL(@OilAsh,0)*@OilNDC)/@NDC

DECLARE @OilPcnt real
IF (@OilMBTU + @GasMBTU) > 0
	SELECT @OilPcnt = @OilMBTU/(@OilMBTU + @GasMBTU)
ELSE
	SELECT @OilPcnt = 0
IF @OilPcnt < 0.1
	SELECT @OilPcnt = 0
IF @OilPcnt > 0.9
	SELECT @OilPcnt = 1

SELECT @EGC = 1488*POWER(@Scrubbers*(@AdjNetMWH/1000000), 0.24) + 604*(@Prec+@Bag) 
		+ 1123*POWER(@AdjNetMWH/1000000, 0.6)
		- 75*ISNULL(@NumUnitsAtSite, 0)
		+ 1*ISNULL(@ElecGenStarts, 0)
		+ 571*POWER(@AdjNetMWH/1000000*@Sulfur, 0.96)
		+ 0.00142*POWER(@AdjNetMWH/1000000*ISNULL(@AshPcnt, 0), 3.52)
		+ 208.7*POWER(@CoalNDC, 0.68)
		+ 10.7*POWER(@OilPcnt*(@OilNDC+@GasNDC), 1.11)
		+ 69.9*POWER((1-@OilPcnt)*(@OilNDC+@GasNDC), 0.68)
IF @CTGs > 0
	SELECT @EGC = @EGC + 48.5*POWER(@CTGs, 3.12)
			+ 215.7*POWER(@CTG_NDC, 0.71)

DELETE FROM EGCCalc WHERE Refnum = @Refnum
INSERT INTO EGCCalc (Refnum, EGC, STGs, STG_NDC, CTGs, CTG_NDC, NDC, Scrubbers, PrecBag, AdjNetMWH, Sulfur, AshPcnt, FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt)
VALUES (@Refnum, @EGC, @STGs, @STG_NDC, @CTGs, @CTG_NDC, @NDC, @Scrubbers, @Prec+@Bag, @AdjNetMWH, @Sulfur, @AshPcnt, @FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt)

UPDATE GenerationTotCalc SET EGC = @EGC WHERE Refnum = @Refnum
UPDATE Gensum SET EGC = @EGC WHERE Refnum = @Refnum

UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @EGC > 0 THEN AnnNonOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @EGC > 0 THEN TotAnnOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @EGC > 0 THEN AnnOHCostKUS/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @EGC > 0 THEN AnnNonOHCostKUS/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @EGC > 0 THEN AnnMaintCostKUS/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @EGC > 0 THEN AnnNonOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @EGC > 0 THEN AnnOHCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @EGC > 0 THEN AnnMaintCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @EGC > 0 THEN AnnOHProjCost/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @EGC > 0 THEN AnnOHExclProj/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @EGC > 0 THEN AnnMaintExclProj/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @EGC > 0 THEN AnnLTSACost/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE Pers SET TotEffPersEGC = CASE WHEN @EGC > 0 THEN TotEffPers*1000/@EGC ELSE NULL END WHERE Refnum = @Refnum
UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @EGC > 0 THEN TotEffPers*1000/@EGC ELSE NULL END WHERE Refnum = @Refnum

UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @EGC





