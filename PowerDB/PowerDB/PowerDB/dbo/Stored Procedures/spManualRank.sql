﻿CREATE PROC spManualRank (@RefListName RefListName, @BreakCondition varchar(30), @BreakValue char(12), @RankVariableID smallint,
		@Refnum Refnum, @Value float, @Rank smallint OUTPUT, @Percentile real OUTPUT, @Tile tinyint OUTPUT)
AS
SET NOCOUNT ON
DECLARE @Value1 float, @Rank1 smallint, @Percentile1 real, @Tile1 tinyint
DECLARE @Value2 float, @Rank2 smallint, @Percentile2 real, @Tile2 tinyint
DECLARE @RankSpecsID int, @RefInList bit

SELECT @RankSpecsID = RankSpecsID FROM RankSpecs_LU
WHERE RefListNo = (SELECT RefListNo FROM RefList_LU WHERE ListName = @RefListName)
AND BreakID = (SELECT BreakID FROM RankBreaks WHERE Description = @BreakCondition)
AND BreakValue = @BreakValue AND RankVariableID = @RankVariableID

IF EXISTS (SELECT * FROM Rank WHERE RankSpecsID = @RankSpecsID AND Refnum = @Refnum)
	SELECT @RefInList = 1
ELSE
	SELECT @RefInList = 0

SELECT @Value1 = Value, @Rank1 = Rank, @Percentile1 = Percentile, @Tile1 = Tile
FROM Rank 
WHERE RankSpecsID = @RankSpecsID AND Value<=@Value
ORDER BY Value, Rank ASC

SELECT @Value2 = Value, @Rank2 = Rank, @Percentile2 = Percentile, @Tile2 = Tile
FROM Rank 
WHERE RankSpecsID = @RankSpecsID AND Value>=@Value
ORDER BY Value DESC, Rank ASC

--SELECT @Value1, @Rank1, @Percentile1, @Tile1
--SELECT @Value2, @Rank2, @Percentile2, @Tile2

IF @Value1 IS NULL
BEGIN
	IF @Rank2 = 1
		SELECT @Rank = 1, @Percentile = (@Percentile2+100.0)/2, @Tile = 1
	ELSE
	BEGIN
		IF @RefInList = 1
			SELECT @Rank = @Rank2, @Percentile = @Percentile2, @Tile = @Tile2
		ELSE
			SELECT @Rank = @Rank2 + 1, @Percentile = (@Percentile2+0.0)/2, @Tile = @Tile2
	END
END
ELSE
BEGIN
	IF @Value2 IS NULL
	BEGIN
		IF @Rank1 = 1
			SELECT @Rank = 1, @Percentile = (@Percentile1+100.0)/2, @Tile = 1
		ELSE
		BEGIN
			IF @RefInList = 1
				SELECT @Rank = @Rank1, @Percentile = @Percentile1, @Tile = @Tile1
			ELSE
				SELECT @Rank = @Rank1 + 1, @Percentile = (@Percentile1+0.0)/2, @Tile = @Tile1
		END
	END
	ELSE 
	BEGIN
		IF @Value1 = @Value2
		BEGIN
			IF @Rank1<=@Rank2
				SELECT @Rank = @Rank1, @Percentile = @Percentile1, @Tile = @Tile1
			ELSE
				SELECT @Rank = @Rank2, @Percentile = @Percentile2, @Tile = @Tile2
		END
		ELSE
		BEGIN	
			SELECT @Percentile = (@Percentile2 - @Percentile1)/(@Value2-@Value1) * (@Value-@Value1) + @Percentile1
			IF ABS(@Value-@Value1) < ABS(@Value-@Value2)
				SELECT @Rank = @Rank1, @Tile = @Tile1
			ELSE
				SELECT @Rank = @Rank2, @Tile = @Tile2
		END
	END
END

