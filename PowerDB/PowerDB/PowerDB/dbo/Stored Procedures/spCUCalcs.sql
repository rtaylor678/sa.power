﻿CREATE PROCEDURE spCUCalcs (
	@TotPeakMWH real, @TotMRO real,
	@PeakMWHLost_P real, @PeakMWHLost_M real, 
	@PeakMWHLost_F real, @PeakMWHLost_Tot real,
	@LRO_P real, @LRO_M real, @LRO_F real, @LRO_Tot real, 
	@LROP_P real OUTPUT, @LROP_M real OUTPUT, 
	@LROP_F real OUTPUT, @LROP_Tot real OUTPUT,
	@PeakUnavail_P real OUTPUT, @PeakUnavail_M real OUTPUT, 
	@PeakUnavail_F real OUTPUT, @PeakUnavail_Tot real OUTPUT,
	@CUTI_P real OUTPUT, @CUTI_M real OUTPUT, 
	@CUTI_F real OUTPUT, @CUTI_Tot real OUTPUT,
	@PeakMWHLost_Unp real OUTPUT, @LRO_Unp real OUTPUT, 
	@LROP_Unp real OUTPUT, @PeakUnavail_Unp real OUTPUT, 
	@CUTI_Unp real OUTPUT
) AS
SELECT 	@PeakMWHLost_Unp = @PeakMWHLost_F + @PeakMWHLost_M,
	@LRO_Unp = @LRO_F + @LRO_M
IF @TotMRO>0 
	SELECT 	@LROP_P = @LRO_P/@TotMRO*100, 
		@LROP_M = @LRO_M/@TotMRO*100, 
		@LROP_F = @LRO_F/@TotMRO*100, 
		@LROP_Tot = @LRO_Tot/@TotMRO*100 ,
		@LROP_Unp = @LRO_Unp/@TotMRO*100
ELSE
	SELECT 	@LROP_P = NULL, @LROP_M = NULL, 
		@LROP_F = NULL, @LROP_Tot = NULL, @LROP_Unp = NULL
IF @TotPeakMWH>0 
BEGIN
	SELECT 	@PeakUnavail_P = @PeakMWHLost_P/@TotPeakMWH*100, 
		@PeakUnavail_M = @PeakMWHLost_M/@TotPeakMWH*100, 
		@PeakUnavail_F = @PeakMWHLost_F/@TotPeakMWH*100, 
		@PeakUnavail_Tot = @PeakMWHLost_Tot/@TotPeakMWH*100,
		@PeakUnavail_Unp = @PeakMWHLost_Unp/@TotPeakMWH*100
	IF @TotMRO > 0
	BEGIN
		IF @PeakMWHLost_P > 0 
			SELECT @CUTI_P = @LRO_P/@TotMRO/(@PeakMWHLost_P/@TotPeakMWH) 
		ELSE 
			SELECT @CUTI_P = 0
		IF @PeakMWHLost_M > 0 
			SELECT @CUTI_M = @LRO_M/@TotMRO/(@PeakMWHLost_M/@TotPeakMWH) 
		ELSE 
			SELECT @CUTI_M = 0 
		IF @PeakMWHLost_F > 0 
			SELECT @CUTI_F = @LRO_F/@TotMRO/(@PeakMWHLost_F/@TotPeakMWH) 
		ELSE 
			SELECT @CUTI_F = 0
		IF @PeakMWHLost_Tot > 0 
			SELECT @CUTI_Tot = @LRO_Tot/@TotMRO/(@PeakMWHLost_Tot/@TotPeakMWH) 
		ELSE 
			SELECT @CUTI_Tot = 0
		IF @PeakMWHLost_Unp > 0 
			SELECT @CUTI_Unp = @LRO_Unp/@TotMRO/(@PeakMWHLost_Unp/@TotPeakMWH) 
		ELSE 
			SELECT @CUTI_Unp = 0
	END
	ELSE
		SELECT 	@CUTI_P = NULL, @CUTI_M = NULL, 
			@CUTI_F = NULL, @CUTI_Tot = NULL, @CUTI_Unp = NULL
END
ELSE
	SELECT 	@PeakUnavail_P = NULL, @PeakUnavail_M = NULL, 
		@PeakUnavail_F = NULL, @PeakUnavail_Tot = NULL,
		@CUTI_P = NULL, @CUTI_M = NULL, 
		@CUTI_F = NULL, @CUTI_Tot = NULL, 
		@PeakUnavail_Unp = NULL, @CUTI_Unp = NULL
