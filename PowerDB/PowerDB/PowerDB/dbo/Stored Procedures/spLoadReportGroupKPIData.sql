﻿


CREATE PROC [dbo].[spLoadReportGroupKPIData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

	DECLARE @temptable TABLE (HeatRate REAL)
	DECLARE @HHVminvalue REAL
	DECLARE @HHVmaxvalue REAL
	DECLARE @LHVminvalue REAL
	DECLARE @LHVmaxvalue REAL
	
	--remove the existing record in the table
	DELETE FROM ReportGroupKPIData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )



--------------------------
--	--the following calculations are to get the average of the two minimum and two maximum records respectively
--	--done here because i couldn't get an easier way to do it right in the query
--	--if you know of such a way, please do it


		INSERT @temptable
		SELECT TOP 2 gtc.HeatRate AS HeatRate
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				--inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = rl.Refnum
				--INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				--INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
				--INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = rl.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = rl.Refnum
				inner join _RankView r on r.Refnum = rl.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
			ORDER BY gtc.HeatRate ASC

		SELECT @HHVminvalue = AVG(HeatRate) FROM @temptable

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 gtc.HeatRate AS HeatRate
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				--inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = rl.Refnum
				--INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				--INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
				--INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = rl.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = rl.Refnum
				inner join _RankView r on r.Refnum = rl.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
			ORDER BY gtc.HeatRate DESC

		SELECT @HHVmaxvalue = AVG(HeatRate) FROM @temptable 

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 gtc.HeatRateLHV AS HeatRate
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				--inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = rl.Refnum
				--INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				--INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
				--INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = rl.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = rl.Refnum
				inner join _RankView r on r.Refnum = rl.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
			ORDER BY gtc.HeatRateLHV ASC

		SELECT @LHVminvalue = AVG(HeatRate) FROM @temptable 

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 gtc.HeatRateLHV AS HeatRate
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				--inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = rl.Refnum
				--INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
				--INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
				--INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = rl.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = rl.Refnum
				inner join _RankView r on r.Refnum = rl.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
			ORDER BY gtc.HeatRateLHV DESC

		SELECT @LHVmaxvalue = AVG(HeatRate) FROM @temptable 
		
		
				
---------------------------


--Power Block HHV Heat Rate, BTU/KWH	GenSum:HeatRate	GenSum:AdjNetMWH	Wt.Avg.NZ
--  Range - Minimum	GenerationTotCalc:HeatRate		Min2
--        - Maximum	GenerationTotCalc:HeatRate		Max2
--Power Block LHV Heat Rate, BTU/KWH	GenSum:HeatRateLHV	GenSum:AdjNetMWH	Wt.Avg.NZ
--  Range - Minimum	GenerationTotCalc:HeatRateLHV		Min2
--        - Maximum	GenerationTotCalc:HeatRateLHV		Max2

			INSERT ReportGroupKPIData (RefNum, 
				ReportTitle, 
				ListName, 
				CommUnavailPct,
				CommUnavailPctForced,
				CommUnavailPctMaint,
				CommUnavailPctPlanned,
				CommUnavailPctJanFeb,
				CommUnavailPctForcedJanFeb,
				CommUnavailPctJunAug,
				CommUnavailPctForcedJunAug,
				CommUnavailIndex,
				CommUnavailIndexForced,
				CommUnavailIndexMaint,
				CommUnavailIndexPlanned,
				CommUnavailIndexJanFeb,
				CommUnavailIndexForcedJanFeb,
				CommUnavailIndexJunAug,
				CommUnavailIndexForcedJunAug,
				UnplannedCommUnavailPct,
				UnplannedCommUnavailIndex,
				EFOR2YrPct,
				EUF2YrPct,
				StartSuccessPct,
				ForcedOutagePct,
				MaintenancePct,
				StartupFailurePct,
				DeratedPct,
				GrandTotalExpendMWH,
				GrandTotalExpendMW,
				CashExpendMWH,
				CashExpendMW,
				CashLessFuelMWH,
				CashLessFuelMW,
				CashLessFuelEmissionsMWH,
				CashLessFuelEmissionsMW,
				CashLessFuelEGC,
				CashLessFuelEmissionsEGC,
				EGC,
				PlantManageableExpMWH,
				PlantManageableExpMW,
				HHVHeatRateKWH,
				HHVHeatRateKWHMinimum,
				HHVHeatRateKWHMaximum,
				LHVHeatRateKWH,
				LHVHeatRateKWHMinimum,
				LHVHeatRateKWHMaximum,
				MaintIndexMWH,
				MaintIndexMW,
				EffectivePersonnelMWH,
				EffectivePersonnelMW,
				OSHARate,
				CurrYearEORPct,
				CurrYearEUORPct,
				CurrYearEFORPct,
				CurrYearEPORPct,
				CurrYearEUFPct,
				CurrYearEUOFPct,
				CurrYearEPOFPct,
				CurrYearNCFPct,
				CurrYearNOFPct,
				GenUnitsExclCT,
				GenUnits,
				CurrYearSvcTimeUnplannedOutage,
				CurrYearSvcTimeForcedOutage,
				CurrYearSvcTimeMaintOutage,
				NumberOfStarts,
				MeanRunTime)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				CommUnavailPct = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Tot,0))/SUM(cu.TotPeakMWH) END*100,
				CommUnavailPctForced = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_F,0))/SUM(cu.TotPeakMWH) END*100,
				CommUnavailPctMaint = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_M,0))/SUM(cu.TotPeakMWH) END*100,
				CommUnavailPctPlanned = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_P,0))/SUM(cu.TotPeakMWH) END*100,
				CommUnavailPctJanFeb = CASE WHEN SUM(cu.TotPeakMWH_Win) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Tot_Win,0))/SUM(cu.TotPeakMWH_Win) END*100,
				CommUnavailPctForcedJanFeb = CASE WHEN SUM(cu.TotPeakMWH_Win) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_F_Win,0))/SUM(cu.TotPeakMWH_Win) END*100,
				CommUnavailPctJunAug = CASE WHEN SUM(cu.TotPeakMWH_Sum) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Tot_Sum,0))/SUM(cu.TotPeakMWH_Sum) END*100,
				CommUnavailPctForcedJunAug = CASE WHEN SUM(cu.TotPeakMWH_Sum) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_F_Sum,0))/SUM(cu.TotPeakMWH_Sum) END*100,
				CommUnavailIndex = AVG(ISNULL(cu.CUTI_Tot,0)),
				CommUnavailIndexForced = AVG(ISNULL(cu.CUTI_F,0)),
				CommUnavailIndexMaint = AVG(ISNULL(cu.CUTI_M,0)),
				CommUnavailIndexPlanned = AVG(ISNULL(cu.CUTI_P,0)),
				CommUnavailIndexJanFeb = AVG(ISNULL(cu.CUTI_Tot_Win,0)),
				CommUnavailIndexForcedJanFeb = AVG(ISNULL(cu.CUTI_F_Win,0)),
				CommUnavailIndexJunAug = AVG(ISNULL(cu.CUTI_Tot_Sum,0)),
				CommUnavailIndexForcedJunAug = AVG(ISNULL(cu.CUTI_F_Sum,0)),
				UnplannedCommUnavailPct = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Unp,0))/SUM(cu.TotPeakMWH) END*100,
				UnplannedCommUnavailIndex = AVG(ISNULL(cu.CUTI_Unp,0)),
				EFOR2YrPct = GlobalDB.dbo.WtAvg(nf.EFOR2Yr,nf.EFOR2YrWtFactor),
				EUF2YrPct = GlobalDB.dbo.WtAvg(nf.EUF2Yr, nf.WPH),
				StartSuccessPct = GlobalDB.dbo.WtAvg(st.SuccessPcnt, st.TotalOpps),
				ForcedOutagePct = GlobalDB.dbo.WtAvg(st.ForcedPcnt, st.TotalOpps),
				MaintenancePct = GlobalDB.dbo.WtAvg(st.MaintPcnt, st.TotalOpps),
				StartupFailurePct = GlobalDB.dbo.WtAvg(st.StartupPcnt, st.TotalOpps),
				DeratedPct = GlobalDB.dbo.WtAvg(st.DeratePcnt, st.TotalOpps),
				GrandTotalExpendMWH = GlobalDB.dbo.WtAvg(omwh.GrandTot, gtc.AdjNetMWH),
				GrandTotalExpendMW = GlobalDB.dbo.WtAvg(okw.GrandTot, nf.NMC),
				CashExpendMWH = GlobalDB.dbo.WtAvg(omwh.TotCash, gtc.AdjNetMWH),
				CashExpendMW = GlobalDB.dbo.WtAvg(okw.TotCash, nf.NMC),
				CashLessFuelMWH = GlobalDB.dbo.WtAvg(omwh.TotCashLessFuel, gtc.AdjNetMWH),
				CashLessFuelMW = GlobalDB.dbo.WtAvg(okw.TotCashLessFuel, nf.NMC),
				CashLessFuelEmissionsMWH = GlobalDB.dbo.WtAvg(omwh.TotCashLessFuelEm, gtc.AdjNetMWH),
				CashLessFuelEmissionsMW = GlobalDB.dbo.WtAvg(okw.TotCashLessFuelEm, nf.NMC),
				CashLessFuelEGC = CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(oadj.TotCashLessFuel,0))/SUM(gtc.EGC) END,
				CashLessFuelEmissionsEGC = CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(oadj.TotCashLessFuelEm,0))/SUM(gtc.EGC) END,
				EGC = AVG(CASE WHEN gs.EGC <> 0 THEN gs.EGC END)/1000,
				PlantManageableExpMWH = GlobalDB.dbo.WtAvg(omwh.ActCashLessFuel, gtc.AdjNetMWH),
				PlantManageableExpMW = GlobalDB.dbo.WtAvg(okw.ActCashLessFuel, nf.NMC),
				HHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(gs.HeatRate, gs.AdjNetMWH),
				HHVHeatRateKWHMinimum = @HHVminvalue,
				HHVHeatRateKWHMaximum = @HHVmaxvalue,
				LHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(gs.HeatRateLHV, gs.AdjNetMWH),
				LHVHeatRateMinimum = @LHVminvalue,
				LHVHeatRateMaximum = @LHVmaxvalue,
				MaintIndexMWH = GlobalDB.dbo.WtAvg(mt.AnnMaintCostMWH,gtc.AdjNetMWH2Yr),
				MaintIndexMW = GlobalDB.dbo.WtAvg(mt.AnnMaintCostMW, nf.NMC2Yr),
				EffectivePersonnelMWH = GlobalDB.dbo.WtAvg(gs.TotEffPersMWH, gtc.AdjNetMWH),
				EffectivePersonnelMW = GlobalDB.dbo.WtAvg(gs.TotEffPersMW, nf.NMC),
				OSHARate = GlobalDB.dbo.WtAvg(gs.OSHARate, ps.SiteEffPers),
				CurrYearEORPct = GlobalDB.dbo.WtAvg(nf.EOR, nf.EORWtFactor),
				CurrYearEUORPct = GlobalDB.dbo.WtAvg(nf.EUOR, nf.EUORWtFactor),
				CurrYearEFORPct = GlobalDB.dbo.WtAvg(nf.EFOR, nf.EFORWtFactor),
				CurrYearEPORPct = GlobalDB.dbo.WtAvg(nf.EPOR, nf.EPORWtFactor),
				CurrYearEUFPct = GlobalDB.dbo.WtAvg(nf.EUF, nf.WPH),
				CurrYearEUOFPct = GlobalDB.dbo.WtAvg(nf.EUOF, nf.WPH),
				CurrYearEPOFPct = GlobalDB.dbo.WtAvg(nf.EPOF, nf.WPH),
				CurrYearNCFPct = GlobalDB.dbo.WtAvg(nf.NCF, nf.WPH),
				CurrYearNOFPct = GlobalDB.dbo.WtAvg(nf.NOF, nf.NOFWtFactor),
				GenUnitsExclCT = AVG(CASE WHEN ugd.TotExclSCNum <> 0 THEN CAST(ugd.TotExclSCNum AS real) END),
				GenUnits = AVG(CASE WHEN ugd.TotUnitNum <> 0 THEN CAST(ugd.TotUnitNum AS real) END),
				CurrYearSvcTimeUnplannedOutage = GlobalDB.dbo.WtAvgNZ(nf.MSTUO, nf.NumUO),
				CurrYearSvcTimeForcedOutage = GlobalDB.dbo.WtAvgNZ(nf.MSTFO, nf.NumFO),
				CurrYearSvcTimeMaintOutage = GlobalDB.dbo.WtAvgNZ(nf.MSTMO, nf.NumMO),
				NumberOfStarts = AVG(gtc.TotStarts),
				MeanRunTime = GlobalDB.dbo.WtAvgNZ(gtc.MeanRunTime, gtc.TotStarts)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN CommUnavail cu ON cu.Refnum = t.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = 'MWH'
				INNER JOIN OpExCalc oadj on oadj.Refnum = t.Refnum AND oadj.DataType = 'ADJ'
				INNER JOIN OpExCalc okw on okw.Refnum = t.Refnum AND okw.DataType = 'KW'
				INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
				INNER JOIN GenSum gs on gs.refnum = t.refnum
				INNER JOIN MaintTotCalc mt on mt.Refnum = t.Refnum 
				INNER JOIN PersSTCalc ps on ps.Refnum = t.Refnum AND ps.SectionID = 'TP'
				INNER JOIN UnitGenData ugd on ugd.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

		
			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''


			--and this is the actual query being run
			EXEC ('DECLARE @temptable TABLE (HeatRate REAL)
				DECLARE @HHVminvalue REAL
				DECLARE @HHVmaxvalue REAL
				DECLARE @LHVminvalue REAL
				DECLARE @LHVmaxvalue REAL		
		
			INSERT @temptable
				SELECT TOP 2 gtc.HeatRate AS HeatRate
					FROM TSort t 
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
					INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
					INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
					' ORDER BY gtc.HeatRate ASC

				SELECT @HHVminvalue = AVG(HeatRate) FROM @temptable 

				DELETE FROM @temptable 

				INSERT @temptable
				SELECT TOP 2 gtc.HeatRate AS HeatRate
					FROM TSort t 
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
					INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
					INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
						' ORDER BY gtc.HeatRate DESC

				SELECT @HHVmaxvalue = AVG(HeatRate) FROM @temptable 	
	


				DELETE FROM @temptable 

				INSERT @temptable
				SELECT TOP 2 gtc.HeatRateLHV AS HeatRate
					FROM TSort t 
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
					INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
					INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
						' ORDER BY gtc.HeatRateLHV ASC

				SELECT @LHVminvalue = AVG(HeatRate) FROM @temptable 

				DELETE FROM @temptable 

				INSERT @temptable
				SELECT TOP 2 gtc.HeatRateLHV AS HeatRate
					FROM TSort t 
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					INNER JOIN GenSum gs ON gs.Refnum = t.Refnum 
					INNER JOIN PersByRefnum per ON Per.Refnum = t.Refnum
					INNER JOIN PersSTCalcByRefnum pst ON pst.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
						' ORDER BY gtc.HeatRateLHV DESC

				SELECT @LHVmaxvalue = AVG(HeatRate) FROM @temptable 


			
			INSERT ReportGroupKPIData (RefNum, 
					ReportTitle, 
					ListName, 
					CommUnavailPct,
					CommUnavailPctForced,
					CommUnavailPctMaint,
					CommUnavailPctPlanned,
					CommUnavailPctJanFeb,
					CommUnavailPctForcedJanFeb,
					CommUnavailPctJunAug,
					CommUnavailPctForcedJunAug,
					CommUnavailIndex,
					CommUnavailIndexForced,
					CommUnavailIndexMaint,
					CommUnavailIndexPlanned,
					CommUnavailIndexJanFeb,
					CommUnavailIndexForcedJanFeb,
					CommUnavailIndexJunAug,
					CommUnavailIndexForcedJunAug,
					UnplannedCommUnavailPct,
					UnplannedCommUnavailIndex,
					EFOR2YrPct,
					EUF2YrPct,
					StartSuccessPct,
					ForcedOutagePct,
					MaintenancePct,
					StartupFailurePct,
					DeratedPct,
					GrandTotalExpendMWH,
					GrandTotalExpendMW,
					CashExpendMWH,
					CashExpendMW,
					CashLessFuelMWH,
					CashLessFuelMW,
					CashLessFuelEmissionsMWH,
					CashLessFuelEmissionsMW,
					CashLessFuelEGC,
					CashLessFuelEmissionsEGC,
					EGC,
					PlantManageableExpMWH,
					PlantManageableExpMW,
					HHVHeatRateKWH,
					HHVHeatRateKWHMinimum,
					HHVHeatRateKWHMaximum,
					LHVHeatRateKWH,
					LHVHeatRateKWHMinimum,
					LHVHeatRateKWHMaximum,
					MaintIndexMWH,
					MaintIndexMW,
					EffectivePersonnelMWH,
					EffectivePersonnelMW,
					OSHARate,
					CurrYearEORPct,
					CurrYearEUORPct,
					CurrYearEFORPct,
					CurrYearEPORPct,
					CurrYearEUFPct,
					CurrYearEUOFPct,
					CurrYearEPOFPct,
					CurrYearNCFPct,
					CurrYearNOFPct,
					GenUnitsExclCT,
					GenUnits,
					CurrYearSvcTimeUnplannedOutage,
					CurrYearSvcTimeForcedOutage,
					CurrYearSvcTimeMaintOutage,
					NumberOfStarts,
					MeanRunTime)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					CommUnavailPct = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Tot,0))/SUM(cu.TotPeakMWH) END*100,
					CommUnavailPctForced = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_F,0))/SUM(cu.TotPeakMWH) END*100,
					CommUnavailPctMaint = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_M,0))/SUM(cu.TotPeakMWH) END*100,
					CommUnavailPctPlanned = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_P,0))/SUM(cu.TotPeakMWH) END*100,
					CommUnavailPctJanFeb = CASE WHEN SUM(cu.TotPeakMWH_Win) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Tot_Win,0))/SUM(cu.TotPeakMWH_Win) END*100,
					CommUnavailPctForcedJanFeb = CASE WHEN SUM(cu.TotPeakMWH_Win) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_F_Win,0))/SUM(cu.TotPeakMWH_Win) END*100,
					CommUnavailPctJunAug = CASE WHEN SUM(cu.TotPeakMWH_Sum) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Tot_Sum,0))/SUM(cu.TotPeakMWH_Sum) END*100,
					CommUnavailPctForcedJunAug = CASE WHEN SUM(cu.TotPeakMWH_Sum) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_F_Sum,0))/SUM(cu.TotPeakMWH_Sum) END*100,
					CommUnavailIndex = AVG(ISNULL(cu.CUTI_Tot,0)),
					CommUnavailIndexForced = AVG(ISNULL(cu.CUTI_F,0)),
					CommUnavailIndexMaint = AVG(ISNULL(cu.CUTI_M,0)),
					CommUnavailIndexPlanned = AVG(ISNULL(cu.CUTI_P,0)),
					CommUnavailIndexJanFeb = AVG(ISNULL(cu.CUTI_Tot_Win,0)),
					CommUnavailIndexForcedJanFeb = AVG(ISNULL(cu.CUTI_F_Win,0)),
					CommUnavailIndexJunAug = AVG(ISNULL(cu.CUTI_Tot_Sum,0)),
					CommUnavailIndexForcedJunAug = AVG(ISNULL(cu.CUTI_F_Sum,0)),
					UnplannedCommUnavailPct = CASE WHEN SUM(cu.TotPeakMWH) <> 0 THEN SUM(ISNULL(cu.PeakMWHLost_Unp,0))/SUM(cu.TotPeakMWH) END*100,
					UnplannedCommUnavailIndex = AVG(ISNULL(cu.CUTI_Unp,0)),
					EFOR2YrPct = GlobalDB.dbo.WtAvg(nf.EFOR2Yr,nf.EFOR2YrWtFactor),
					EUF2YrPct = GlobalDB.dbo.WtAvg(nf.EUF2Yr, nf.WPH),
					StartSuccessPct = GlobalDB.dbo.WtAvg(st.SuccessPcnt, st.TotalOpps),
					ForcedOutagePct = GlobalDB.dbo.WtAvg(st.ForcedPcnt, st.TotalOpps),
					MaintenancePct = GlobalDB.dbo.WtAvg(st.MaintPcnt, st.TotalOpps),
					StartupFailurePct = GlobalDB.dbo.WtAvg(st.StartupPcnt, st.TotalOpps),
					DeratedPct = GlobalDB.dbo.WtAvg(st.DeratePcnt, st.TotalOpps),
					GrandTotalExpendMWH = GlobalDB.dbo.WtAvg(omwh.GrandTot, gtc.AdjNetMWH),
					GrandTotalExpendMW = GlobalDB.dbo.WtAvg(okw.GrandTot, nf.NMC),
					CashExpendMWH = GlobalDB.dbo.WtAvg(omwh.TotCash, gtc.AdjNetMWH),
					CashExpendMW = GlobalDB.dbo.WtAvg(okw.TotCash, nf.NMC),
					CashLessFuelMWH = GlobalDB.dbo.WtAvg(omwh.TotCashLessFuel, gtc.AdjNetMWH),
					CashLessFuelMW = GlobalDB.dbo.WtAvg(okw.TotCashLessFuel, nf.NMC),
					CashLessFuelEmissionsMWH = GlobalDB.dbo.WtAvg(omwh.TotCashLessFuelEm, gtc.AdjNetMWH),
					CashLessFuelEmissionsMW = GlobalDB.dbo.WtAvg(okw.TotCashLessFuelEm, nf.NMC),
					CashLessFuelEGC = CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(oadj.TotCashLessFuel,0))/SUM(gtc.EGC) END,
					CashLessFuelEmissionsEGC = CASE WHEN SUM(gtc.EGC) <> 0 THEN SUM(ISNULL(oadj.TotCashLessFuelEm,0))/SUM(gtc.EGC) END,
					EGC = AVG(CASE WHEN gs.EGC <> 0 THEN gs.EGC END)/1000,
					PlantManageableExpMWH = GlobalDB.dbo.WtAvg(omwh.ActCashLessFuel, gtc.AdjNetMWH),
					PlantManageableExpMW = GlobalDB.dbo.WtAvg(okw.ActCashLessFuel, nf.NMC),
					HHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(gs.HeatRate, gs.AdjNetMWH),
					HHVHeatRateKWHMinimum = @HHVminvalue,
					HHVHeatRateKWHMaximum = @HHVmaxvalue,
					LHVHeatRateKWH = GlobalDB.dbo.WtAvgNZ(gs.HeatRateLHV, gs.AdjNetMWH),
					LHVHeatRateMinimum = @LHVminvalue,
					LHVHeatRateMaximum = @LHVmaxvalue,
					MaintIndexMWH = GlobalDB.dbo.WtAvg(mt.AnnMaintCostMWH,gtc.AdjNetMWH2Yr),
					MaintIndexMW = GlobalDB.dbo.WtAvg(mt.AnnMaintCostMW, nf.NMC2Yr),
					EffectivePersonnelMWH = GlobalDB.dbo.WtAvg(gs.TotEffPersMWH, gtc.AdjNetMWH),
					EffectivePersonnelMW = GlobalDB.dbo.WtAvg(gs.TotEffPersMW, nf.NMC),
					OSHARate = GlobalDB.dbo.WtAvg(gs.OSHARate, ps.SiteEffPers),
					CurrYearEORPct = GlobalDB.dbo.WtAvg(nf.EOR, nf.EORWtFactor),
					CurrYearEUORPct = GlobalDB.dbo.WtAvg(nf.EUOR, nf.EUORWtFactor),
					CurrYearEFORPct = GlobalDB.dbo.WtAvg(nf.EFOR, nf.EFORWtFactor),
					CurrYearEPORPct = GlobalDB.dbo.WtAvg(nf.EPOR, nf.EPORWtFactor),
					CurrYearEUFPct = GlobalDB.dbo.WtAvg(nf.EUF, nf.WPH),
					CurrYearEUOFPct = GlobalDB.dbo.WtAvg(nf.EUOF, nf.WPH),
					CurrYearEPOFPct = GlobalDB.dbo.WtAvg(nf.EPOF, nf.WPH),
					CurrYearNCFPct = GlobalDB.dbo.WtAvg(nf.NCF, nf.WPH),
					CurrYearNOFPct = GlobalDB.dbo.WtAvg(nf.NOF, nf.NOFWtFactor),
					GenUnitsExclCT = AVG(CASE WHEN ugd.TotExclSCNum <> 0 THEN CAST(ugd.TotExclSCNum AS real) END),
					GenUnits = AVG(CASE WHEN ugd.TotUnitNum <> 0 THEN CAST(ugd.TotUnitNum AS real) END),
					CurrYearSvcTimeUnplannedOutage = GlobalDB.dbo.WtAvgNZ(nf.MSTUO, nf.NumUO),
					CurrYearSvcTimeForcedOutage = GlobalDB.dbo.WtAvgNZ(nf.MSTFO, nf.NumFO),
					CurrYearSvcTimeMaintOutage = GlobalDB.dbo.WtAvgNZ(nf.MSTMO, nf.NumMO),
					NumberOfStarts = AVG(gtc.TotStarts),
					MeanRunTime = GlobalDB.dbo.WtAvgNZ(gtc.MeanRunTime, gtc.TotStarts)
				FROM TSort t 
					INNER JOIN CommUnavail cu ON cu.Refnum = t.Refnum 
					INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
					INNER JOIN OpExCalc omwh ON omwh.refnum = t.refnum AND omwh.DataType = ''MWH''
					INNER JOIN OpExCalc oadj on oadj.Refnum = t.Refnum AND oadj.DataType = ''ADJ''
					INNER JOIN OpExCalc okw on okw.Refnum = t.Refnum AND okw.DataType = ''KW''
					INNER JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
					INNER JOIN GenSum gs on gs.refnum = t.refnum
					INNER JOIN MaintTotCalc mt on mt.Refnum = t.Refnum 
					INNER JOIN PersSTCalc ps on ps.Refnum = t.Refnum AND ps.SectionID = ''TP''
					INNER JOIN UnitGenData ugd on ugd.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END




