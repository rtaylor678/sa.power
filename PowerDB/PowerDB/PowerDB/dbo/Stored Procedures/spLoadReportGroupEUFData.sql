﻿





CREATE PROC [dbo].[spLoadReportGroupEUFData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL


	
	--remove the existing record in the table
	DELETE FROM ReportGroupEUFData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


			INSERT ReportGroupEUFData (RefNum
			  ,ReportTitle
			  ,ListName
			  ,Pulverizers
			  ,BoilerAux
			  ,Turbine
			  ,Generator
			  ,Valves
			  ,Condenser
			  ,Baghouse
			  ,Precipitator
			  ,WetScrubber
			  ,DryScrubber
			  ,SCR
			  ,CTTurbine
			  ,CTGenerator
			  ,Transformer
			  ,HRSG
			  ,CTOther
			  ,FuelDeliverySystem
			  ,CoolingWater
			  ,AshHandling
			  ,DemineralWater
			  ,SiteTransformer
			  ,WasteWater
			  ,Other)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				Pulverizers = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CPHM' THEN EUF END, CASE WHEN EquipGroup = 'CPHM' THEN E_PH END),
				BoilerAux = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'BOIL' THEN EUF END, CASE WHEN EquipGroup = 'BOIL' THEN E_PH END),
				Turbine = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'TURB' THEN EUF END, CASE WHEN EquipGroup = 'TURB' THEN E_PH END),
				Generator = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'GEN' THEN EUF END, CASE WHEN EquipGroup = 'GEN' THEN E_PH END),
				Valves = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'VC' THEN EUF END, CASE WHEN EquipGroup = 'VC' THEN E_PH END),
				Condenser = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CA' THEN EUF END, CASE WHEN EquipGroup = 'CA' THEN E_PH END),
				Baghouse = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'BAG' THEN EUF END, CASE WHEN EquipGroup = 'BAG' THEN E_PH END),
				Precipitator = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'PREC' THEN EUF END, CASE WHEN EquipGroup = 'PREC' THEN E_PH END),
				WetScrubber = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'WGS' THEN EUF END, CASE WHEN EquipGroup = 'WGS' THEN E_PH END),
				DryScrubber = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'DGS' THEN EUF END, CASE WHEN EquipGroup = 'DGS' THEN E_PH END),
				SCR = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'SCR' THEN EUF END, CASE WHEN EquipGroup = 'SCR' THEN E_PH END),
				CTTurbine = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CTTURB' THEN EUF END, CASE WHEN EquipGroup = 'CTTURB' THEN E_PH END),
				CTGenerator = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CTGEN' THEN EUF END, CASE WHEN EquipGroup = 'CTGEN' THEN E_PH END),
				Transformer = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CTTRANS' THEN EUF END, CASE WHEN EquipGroup = 'CTTRANS' THEN E_PH END),
				HRSG = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CTHRSG' THEN EUF END, CASE WHEN EquipGroup = 'CTHRSG' THEN E_PH END),
				CTOther = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CTOTH' THEN EUF END, CASE WHEN EquipGroup = 'CTOTH' THEN E_PH END),
				FuelDeliverySystem = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'FHF' THEN EUF END, CASE WHEN EquipGroup = 'FHF' THEN E_PH END),
				CoolingWater = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'CWF' THEN EUF END, CASE WHEN EquipGroup = 'CWF' THEN E_PH END),
				AshHandling = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'ASH' THEN EUF END, CASE WHEN EquipGroup = 'ASH' THEN E_PH END),
				DemineralWater = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'DQWS' THEN EUF END, CASE WHEN EquipGroup = 'DQWS' THEN E_PH END),
				SiteTransformer = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'TRANS' THEN EUF END, CASE WHEN EquipGroup = 'TRANS' THEN E_PH END),
				WasteWater = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'WASTEH2O' THEN EUF END, CASE WHEN EquipGroup = 'WASTEH2O' THEN E_PH END),
				Other = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = 'OTHER' THEN EUF END, CASE WHEN EquipGroup = 'OTHER' THEN E_PH END)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN ComponentEUF	ceuf ON ceuf.Refnum = t.Refnum
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''




		EXEC ('INSERT ReportGroupEUFData (RefNum
				,ReportTitle
				,ListName
				,Pulverizers
				,BoilerAux
				,Turbine
				,Generator
				,Valves
				,Condenser
				,Baghouse
				,Precipitator
				,WetScrubber
				,DryScrubber
				,SCR
				,CTTurbine
				,CTGenerator
				,Transformer
				,HRSG
				,CTOther
				,FuelDeliverySystem
				,CoolingWater
				,AshHandling
				,DemineralWater
				,SiteTransformer
				,WasteWater
				,Other)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					Pulverizers = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CPHM'' THEN EUF END, CASE WHEN EquipGroup = ''CPHM'' THEN E_PH END),
					BoilerAux = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''BOIL'' THEN EUF END, CASE WHEN EquipGroup = ''BOIL'' THEN E_PH END),
					Turbine = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''TURB'' THEN EUF END, CASE WHEN EquipGroup = ''TURB'' THEN E_PH END),
					Generator = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''GEN'' THEN EUF END, CASE WHEN EquipGroup = ''GEN'' THEN E_PH END),
					Valves = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''VC'' THEN EUF END, CASE WHEN EquipGroup = ''VC'' THEN E_PH END),
					Condenser = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CA'' THEN EUF END, CASE WHEN EquipGroup = ''CA'' THEN E_PH END),
					Baghouse = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''BAG'' THEN EUF END, CASE WHEN EquipGroup = ''BAG'' THEN E_PH END),
					Precipitator = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''PREC'' THEN EUF END, CASE WHEN EquipGroup = ''PREC'' THEN E_PH END),
					WetScrubber = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''WGS'' THEN EUF END, CASE WHEN EquipGroup = ''WGS'' THEN E_PH END),
					DryScrubber = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''DGS'' THEN EUF END, CASE WHEN EquipGroup = ''DGS'' THEN E_PH END),
					SCR = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''SCR'' THEN EUF END, CASE WHEN EquipGroup = ''SCR'' THEN E_PH END),
					CTTurbine = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CTTURB'' THEN EUF END, CASE WHEN EquipGroup = ''CTTURB'' THEN E_PH END),
					CTGenerator = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CTGEN'' THEN EUF END, CASE WHEN EquipGroup = ''CTGEN'' THEN E_PH END),
					Transformer = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CTTRANS'' THEN EUF END, CASE WHEN EquipGroup = ''CTTRANS'' THEN E_PH END),
					HRSG = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CTHRSG'' THEN EUF END, CASE WHEN EquipGroup = ''CTHRSG'' THEN E_PH END),
					CTOther = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CTOTH'' THEN EUF END, CASE WHEN EquipGroup = ''CTOTH'' THEN E_PH END),
					FuelDeliverySystem = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''FHF'' THEN EUF END, CASE WHEN EquipGroup = ''FHF'' THEN E_PH END),
					CoolingWater = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''CWF'' THEN EUF END, CASE WHEN EquipGroup = ''CWF'' THEN E_PH END),
					AshHandling = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''ASH'' THEN EUF END, CASE WHEN EquipGroup = ''ASH'' THEN E_PH END),
					DemineralWater = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''DQWS'' THEN EUF END, CASE WHEN EquipGroup = ''DQWS'' THEN E_PH END),
					SiteTransformer = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''TRANS'' THEN EUF END, CASE WHEN EquipGroup = ''TRANS'' THEN E_PH END),
					WasteWater = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''WASTEH2O'' THEN EUF END, CASE WHEN EquipGroup = ''WASTEH2O'' THEN E_PH END),
					Other = GlobalDB.dbo.WtAvgNZ(CASE WHEN EquipGroup = ''OTHER'' THEN EUF END, CASE WHEN EquipGroup = ''OTHER'' THEN E_PH END)
				FROM TSort t 
					INNER JOIN ComponentEUF	ceuf ON ceuf.Refnum = t.Refnum
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END





