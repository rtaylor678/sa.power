﻿/****** Object:  Stored Procedure dbo.spRankInsert    Script Date: 4/18/2003 4:39:03 PM ******/
CREATE PROCEDURE spRankInsert;1
@Refnum Refnum,
@RefListNo RefListNo,
@BreakID DD_ID,
@BreakValue varchar(12), 
@RankVariableID DD_ID,
@Rank smallint,
@Percentile real,
@Tile Tinyint,
@Value float
AS
DECLARE @RankSpecsID integer
EXEC spAddRankSpecs @RefListNo, @BreakID, @BreakValue, @RankVariableID, @RankSpecsID OUTPUT
EXEC spRankInsert;2 @Refnum, @RankSpecsID, @Rank, @Percentile, @Tile, @Value
