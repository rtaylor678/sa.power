﻿
CREATE PROC [dbo].[GetDist](@sourceData DistTable READONLY)
AS

	--this is essentially a copy of the code in the Power Pres Data 2013 spreadsheet (which is presumably a copy from elsewhere)
	--it takes an input table which is a User Defined Table Type containing two fields, a value and a weighting factor
	--from that table it calculates a 0 to 100 range of values for a curve

	--how to call:
	--pass it a table, and get a table back
	--create a table to pass:
			--DECLARE @test AS DistTable
	--populate it
			--INSERT @test
			--SELECT o.HeatRate, g.AdjNetMWH
			--FROM TSort t INNER JOIN GenerationTotCalc o 
			--ON o.Refnum = t.Refnum INNER JOIN GenerationTotCalc g ON t.Refnum = g.Refnum
			--INNER JOIN Breaks b ON b.Refnum = t.Refnum
			--WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13')
			--AND (b.CRVSizeGroup like 'LB%' or b.CRVSizeGroup = 'LSC')
			--ORDER BY o.HeatRate
	--run the query and get the data back
			--exec GetDist @test ---------------------------------------------------<<<<<<<<<<<<<<< TLDR this is the useful part


	-------------------------------------------------------------------------------------------

	--get a count of the records we received, to be used later
	DECLARE @recCount INT
	SELECT @recCount = COUNT(*) FROM @sourceData

	--figure out the top and bottom values (so we know if it is sorted ASC or DESC, and for later use)
	DECLARE @FirstValue REAL
	DECLARE @FirstWeight REAL
	DECLARE @LastValue REAL
	DECLARE @LastWeight REAL

	DECLARE @Cursor CURSOR
	DECLARE @value REAL
	DECLARE @weight REAL

	--loop through to get the values for the first and last
	--better way to do this? can't use TOP because it will sort them and we don't want that
	SET @Cursor = CURSOR FOR SELECT value, weightfactor FROM @sourceData

	OPEN @Cursor
	FETCH NEXT FROM @Cursor INTO @value, @weight

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF @FirstValue IS NULL --then we need to get it
			BEGIN
				SET @FirstValue = @value
				SET @FirstWeight = @weight
			END

		--and get the last values every time, at the end they will be the last ones
		SET @LastValue = @value
		SET @LastWeight = @weight
		FETCH NEXT FROM @Cursor INTO @value, @weight
	END

	CLOSE @Cursor
	DEALLOCATE @Cursor

	--determine the sort order
	DECLARE @SortedASC BIT

	IF @FirstValue <= @LastValue
		SET @SortedASC = 1
	ELSE
		SET @SortedASC = 0

	--create a distribution table with pct set from 0 to 100
    CREATE TABLE #arDist(pct INT, value REAL)
	INSERT #arDist(pct) SELECT Num - 1 FROM PowerGlobal.dbo.Numbers WHERE Num <= 101

	--calculate the total weight factor
    DECLARE @TotalWeight REAL
	SELECT @TotalWeight = SUM(weightfactor) FROM @sourceData

	--calculate the end value, which is the average of the last two values in the list
	DECLARE @EndValue REAL

	IF @SortedASC = 1
		SELECT @EndValue = AVG(value) FROM (SELECT TOP 2 value FROM @sourceData ORDER BY value DESC) b
	ELSE
		SELECT @EndValue = AVG(value) FROM (SELECT TOP 2 value FROM @sourceData ORDER BY value ASC) b

	--adjust TotalWeight by removing the end two points
	SET @TotalWeight = @TotalWeight - @FirstWeight
	SET @TotalWeight = @TotalWeight - @LastWeight

	IF @SortedASC = 1
		SELECT @LastValue = AVG(value) FROM (SELECT TOP 2 value FROM @sourceData ORDER BY value ASC) b
	ELSE
		SELECT @LastValue = AVG(value) FROM (SELECT TOP 2 value FROM @sourceData ORDER BY value DESC) b

	--populate the two end values
	UPDATE #arDist SET value = @LastValue WHERE pct = 0
	UPDATE #arDist SET value = @EndValue WHERE pct = 100

	--set up some variables for later use
	DECLARE @CummPcnt REAL
	SET @CummPcnt = 0

	DECLARE @r REAL
	SET @r = 1

	DECLARE @c INT
	SET @c = 2

	DECLARE @weightfactor REAL
	DECLARE @CummMP REAL
	DECLARE @arDatacminus1 REAL
	DECLARE @arDist100 REAL
	DECLARE @arDist0 REAL
	DECLARE @arDatac REAL
	DECLARE @LastMP REAL
	SET @LastMP = 0

	SELECT @arDist0 = value FROM #arDist WHERE pct = 0
	SELECT @arDist100 = value FROM #arDist WHERE pct = 100


	--now loop through each record in the source data, calculating as we go
	SET @Cursor = CURSOR FOR SELECT value, weightfactor FROM @sourceData

	OPEN @Cursor
	FETCH NEXT FROM @Cursor INTO @value, @weightfactor
	SELECT @arDatacminus1 = @arDatac --this is the previous value in the source
	SELECT @arDatac = @value --this is the current value in the source

	FETCH NEXT FROM @Cursor INTO @value, @weightfactor --we want to throw out the first row (already calced it), so we do this twice
	SELECT @arDatacminus1 = @arDatac
	SELECT @arDatac = @value
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		--all this math comes from the spreadsheet
		SET @CummPcnt = @CummPcnt + ((@weightfactor/@TotalWeight) * 100)
		SET @CummMP = @CummPcnt - ((0.5 * (@weightfactor/@TotalWeight)) * 100)

		WHILE @CummMP > @r AND @r < 100
			BEGIN
				IF @c = @recCount
					UPDATE #arDist SET value = @arDatacminus1 - (@arDatacminus1 - @arDist100) * (@LastMP - @r) / (@LastMP - @CummMP) WHERE pct = @r
				ELSE
					IF @c = 2
						UPDATE #arDist SET value = @arDist0 - (@arDist0 - @arDatac) * (0 - @r)/(0 - @CummMP) WHERE pct = @r
					ELSE
						UPDATE #arDist SET value = @arDatacminus1 - (@arDatacminus1 - @arDatac) * (@LastMP - @r)/(@LastMP - @CummMP) WHERE pct = @r

				SET @r = @r + 1
			END

		SET @LastMP = @CummMP
		SET @c = @c + 1

		FETCH NEXT FROM @Cursor INTO @value, @weightfactor
		
		SELECT @arDatacminus1 = @arDatac
		SELECT @arDatac = @value

	END

	CLOSE @Cursor
	DEALLOCATE @Cursor

	SELECT pct = ISNULL(pct,-1), value = ISNULL(value, -1) FROM #arDist
