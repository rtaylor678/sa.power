﻿/*
   Returns days when there is not a price recorded  for a price hub
   @ph  - Price Hub Name
*/
CREATE PROCEDURE [dbo].[sp_CheckMissingDates](@ph varchar(6)) 
AS


select distinct
convert(char(10), a.Starttime,120) as OmittedDates
from revenueprices a 
where 
datepart(dd,a.Starttime) < Dateadd(dd,-1, Dateadd(mm,1 ,Cast(Month(a.starttime) as varchar(2)) + '/1/'+ Cast(Year(a.starttime) as varchar(4))))
and NOT EXISTS 
( select * from revenueprices b 
  where 
 datepart(mm,b.starttime)=datepart(mm,a.starttime) 
and datepart(yy,b.starttime)=datepart(yy,a.starttime)
and datepart(dd,b.starttime)=datepart(dd,a.starttime)
and b.pricinghub=@ph)
and a.starttime >= ( select Min(starttime) from revenueprices where pricinghub=@ph)
order by  convert(char(10), a.Starttime,120)
