﻿CREATE PROC [dbo].[spCurrencyConv](@SiteID SiteID)
AS
DECLARE @StudyYear StudyYear, @FirstYear StudyYear, @LastYear StudyYear, @CurrencyID smallint
DECLARE @CurrConv real, @PrevConv real, @FirstConv real, @LastConv real

SELECT @StudyYear = StudyYear FROM StudySites WHERE SiteID = @SiteID
SELECT @CurrencyID = CurrencyID FROM PlantGenData p WHERE SiteID = @SiteID 
IF @CurrencyID IS NULL
	SET @CurrencyID = 62  --US Dollars

SELECT @CurrConv = ConvRate FROM CurrencyConv
WHERE CurrencyID = @CurrencyID AND Year = @StudyYear

SELECT @PrevConv = ConvRate FROM CurrencyConv
WHERE CurrencyID = @CurrencyID AND Year = @StudyYear - 1

SELECT @FirstYear = Year, @FirstConv = ConvRate 
FROM CurrencyConv WHERE CurrencyID = @CurrencyID AND Year = (SELECT MIN(Year) FROM CurrencyConv x WHERE x.CurrencyID = @CurrencyID)

SELECT @LastYear = Year, @LastConv = ConvRate 
FROM CurrencyConv WHERE CurrencyID = @CurrencyID AND Year = (SELECT MAX(Year) FROM CurrencyConv x WHERE x.CurrencyID = @CurrencyID)

UPDATE Misc SET 
	RevAsh = RevAshLocal/@CurrConv,
	RevOth = RevOthLocal/@CurrConv,
	AshCostOP = AshCostOPLocal/@CurrConv,
	AshCostDisp = AshCostDispLocal/@CurrConv,
	ScrCostLime = ScrCostLimeLocal/@CurrConv,
	ScrCostChem = ScrCostChemLocal/@CurrConv,
	ScrCostNMaint = ScrCostNMaintLocal/@CurrConv,
	SO2PurValue = SO2PurValueLocal/@CurrConv,
	SO2SoldValue = SO2SoldValueLocal/@CurrConv,
	NOxPurValue = NOxPurValueLocal/@CurrConv,
	NOxSoldValue = NOxSoldValueLocal/@CurrConv,
	CO2PurValue = CO2PurValueLocal/@CurrConv,
	CO2SoldValue = CO2SoldValueLocal/@CurrConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE PlantGenData SET
	PlantPrevMMOACost = PlantPrevMMOACostLocal/@PrevConv,
	PlantCurrMMOACost = PlantCurrMMOACostLocal/@CurrConv,
	SparesInvenCentral = SparesInvenCentralLocal/@CurrConv,
	SparesInvenOnSite = SparesInvenOnSiteLocal/@CurrConv,
	PrevSparesInven = PrevSparesInvenLocal/@PrevConv,
	CurrSparesInven = CurrSparesInvenLocal/@CurrConv
WHERE SiteID = @SiteID

UPDATE Coal SET
	MineCostTon = MineCostTonLocal/@CurrConv,
	TransCostTon = TransCostTonLocal/@CurrConv,
	OnSitePrepCostTon = OnSitePrepCostTonLocal/@CurrConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE Fuel SET
	CostMBTU = CostMBTULocal/@CurrConv,
	CostMBTULHV = CostMBTULHVLocal/@CurrConv,
	CostGal = CostGalLocal/@CurrConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE Fuel
SET TotCostKUS = CASE ReportedLHV WHEN 'Y' THEN CostMBTULHV * MBTULHV/1000 ELSE CostMBTU * MBTU/1000 END
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND FuelType IN ('NatGas', 'H2Gas', 'OffGas')

UPDATE Fuel
SET TotCostKUS = KGal * CostGal
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND FuelType IN ('Jet', 'Diesel', 'FOil')

UPDATE SteamSales
SET MarketValue = MarketValueLocal/@CurrConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE OHMaint SET
	DirectCost = DirectCostLocal/ConvRate,
	AMMOCost = AMMOCostLocal/ConvRate,
	LumpSumCost = LumpSumCostLocal/ConvRate
FROM OHMaint INNER JOIN CurrencyConv c ON DATEPART(yy, OHDate) = c.Year
AND c.CurrencyID = ISNULL(OHMaint.CurrencyID, @CurrencyID)
WHERE OHMaint.Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE OHMaint SET
	DirectCost = DirectCostLocal/@FirstConv,
	AMMOCost = AMMOCostLocal/@FirstConv,
	LumpSumCost = LumpSumCostLocal/@FirstConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND DATEPART(yy, OHDate) < @FirstYear
AND (CurrencyID IS NULL OR CurrencyID = @CurrencyID)

UPDATE OHMaint SET
	DirectCost = DirectCostLocal/@LastConv,
	AMMOCost = AMMOCostLocal/@LastConv,
	LumpSumCost = LumpSumCostLocal/@LastConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
AND DATEPART(yy, OHDate) > @LastYear
AND (CurrencyID IS NULL OR CurrencyID = @CurrencyID)

UPDATE NonOHMaint SET
	CurrCptl = CurrCptlLocal/@CurrConv,
	CurrExp = CurrExpLocal/@CurrConv,
	CurrLaborCost = CurrLaborCostLocal/@CurrConv,
	CurrMMOCost = CurrMMOCostLocal/@CurrConv,
	CurrAMMOCost = CurrAMMOCostLocal/@CurrConv,
	PrevCptl = PrevCptlLocal/@PrevConv,
	PrevExp = PrevExpLocal/@PrevConv,
	PrevLaborCost = PrevLaborCostLocal/@PrevConv,
	PrevMMOCost = PrevMMOCostLocal/@PrevConv,
	PrevAMMOCost = PrevAMMOCostLocal/@PrevConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

DELETE FROM CptlMaintExp
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

DECLARE @CptlConvRates TABLE (CptlCode char(4) NOT NULL, ConvRate real NULL)
INSERT @CptlConvRates VALUES ('CURR', @CurrConv)
INSERT @CptlConvRates VALUES ('EST5', @CurrConv)
INSERT @CptlConvRates VALUES ('P1', @PrevConv)
INSERT INTO CptlMaintExp (Refnum, TurbineID, CptlCode, SortKey, Description,
	TotCptl, CptlExcl, Energy, RegEnv, Admin, ConstrRmvl,
	STMaintCptl, TotMaintExp, MaintExpExcl, STMaintExp, TotMMO,
	AllocMMO, MMOExcl, STMMO, TotMaint, InvestCptl, OtherCptl)
SELECT Refnum, TurbineID, l.CptlCode, SortKey, Description,
	TotCptl/c.ConvRate, CptlExcl/c.ConvRate, Energy/c.ConvRate,
	RegEnv/c.ConvRate, Admin/c.ConvRate, ConstrRmvl/c.ConvRate,
	STMaintCptl/c.ConvRate, TotMaintExp/c.ConvRate,
	MaintExpExcl/c.ConvRate, STMaintExp/c.ConvRate, TotMMO/c.ConvRate,
	AllocMMO/c.ConvRate, MMOExcl/c.ConvRate, STMMO/c.ConvRate,
	TotMaint/c.ConvRate, InvestCptl/c.ConvRate, OtherCptl/c.ConvRate
FROM CptlMaintExpLocal l INNER JOIN @CptlConvRates c ON c.CptlCode = l.CptlCode
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

DELETE FROM Opex
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

INSERT INTO Opex (Refnum, OCCWages, MPSWages, OCCBenefits, MPSBenefits,
	CentralOCCWagesBen, CentralMPSWagesBen, MaintMatl, ContMaintLabor,
	ContMaintMatl, ContMaintLumpSum, OthContSvc, OverhaulAdj,
	Envir, PropTax, OthTax, Insurance, AGPers, Supply,
	OthFixed, PurGas, PurLiquid, PurSolid, FuelInven, OthVar,
	AGNonPers, Depreciation, Interest, OthRevenue, Accuracy, BookValue,
	FireSafetyLoss, EnvirFines, ExclOth, TotExpExcl, Chemicals, Water,
	AllocSparesInven)
SELECT Refnum, OCCWages/@CurrConv, MPSWages/@CurrConv,
	OCCBenefits/@CurrConv, MPSBenefits/@CurrConv,
	CentralOCCWagesBen/@CurrConv, CentralMPSWagesBen/@CurrConv,
	MaintMatl/@CurrConv, ContMaintLabor/@CurrConv,
	ContMaintMatl/@CurrConv, ContMaintLumpSum/@CurrConv,
	OthContSvc/@CurrConv, OverhaulAdj/@CurrConv,
	Envir/@CurrConv, PropTax/@CurrConv, OthTax/@CurrConv,
	Insurance/@CurrConv, AGPers/@CurrConv, Supply/@CurrConv,
	OthFixed/@CurrConv, PurGas/@CurrConv, PurLiquid/@CurrConv,
	PurSolid/@CurrConv, FuelInven/@CurrConv, OthVar/@CurrConv,
	AGNonPers/@CurrConv, Depreciation/@CurrConv, Interest/@CurrConv,
	OthRevenue/@CurrConv, Accuracy, BookValue/@CurrConv,
	FireSafetyLoss/@CurrConv, EnvirFines/@CurrConv, ExclOth/@CurrConv,
	TotExpExcl/@CurrConv, Chemicals/@CurrConv, Water/@CurrConv,
	AllocSparesInven/@CurrConv
FROM OpexLocal
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE Ancillary SET Revenue = RevenueLocal/@CurrConv
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)
