﻿CREATE PROC [dbo].[CalcInitial](@SiteID SiteID)
AS

UPDATE OHMaint
SET Component = 'SITETRAN', ProjectID = CASE WHEN ID = 600110 THEN 'OVHL' ELSE 'OTHER' END
FROM OHMaint INNER JOIN Equipment e ON e.Refnum = OHMaint.Refnum AND e.TurbineID = OHMaint.TurbineID AND e.EquipID = OHMaint.EquipID
WHERE OHMaint.Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND e.EquipType = 'SITETRAN' AND OHMaint.Component = 'BOIL'

UPDATE OHMaint
SET ProjectID = 'OTHER'
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID) AND Component = 'SITETRAN' AND ProjectID = 'FWHEAT'

UPDATE CTGData
SET FiringTemp = CTGSupplement.FiringTemp
FROM CTGData INNER JOIN CTGSupplement
	ON CTGData.Refnum = CTGSupplement.Refnum AND CTGData.TurbineID = CTGSupplement.TurbineID
WHERE CTGData.FiringTemp IS NULL AND CTGData.Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID)

UPDATE PowerGeneration
SET NetMWH = ISNULL(GrossMWH, 0) - ISNULL(StationSvcElec, 0), 
	ElecMBTU = (ISNULL(GrossMWH, 0) - ISNULL(StationSvcElec, 0))*3.41214
WHERE Refnum IN (SELECT Refnum FROM TSort WHERE SiteID = @SiteID AND StudyYear >= 1999)
