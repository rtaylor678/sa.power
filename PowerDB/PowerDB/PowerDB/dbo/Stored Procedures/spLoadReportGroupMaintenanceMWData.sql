﻿





CREATE PROC [dbo].[spLoadReportGroupMaintenanceMWData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL


	
	--remove the existing record in the table
	DELETE FROM ReportGroupMaintenanceMWData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it


		INSERT @temptable
		SELECT TOP 2 mtc.AnnMaintCostMW AS totcash
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN CptlMaintExp	cmecurr ON cmecurr.Refnum = t.Refnum AND cmecurr.CptlCode = 'CURR'
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum 
				INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
				LEFT JOIN CptlMaintExpByRefnum cmeest5 ON cmeest5.Refnum = t.Refnum AND cmeest5.CptlCode = 'EST5' --and cmeest5.TurbineID <> 'STG'
				INNER JOIN OpExCalc oMW ON oMW.Refnum = t.Refnum AND oMW.DataType = 'MW'
				INNER JOIN MaintEquipCalcMWByRefnum mec ON mec.Refnum = t.Refnum
				INNER JOIN NonOHMaintMWByRefnum noh ON noh.Refnum = t.Refnum 
				LEFT JOIN OHEquipCalcMWByRefnum ohc ON ohc.Refnum = t.Refnum 
				LEFT JOIN CTGTurbineProjects ctg ON CTG.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		ORDER BY mtc.AnnMaintCostMW ASC

		SELECT @minvalue = AVG(totcash) FROM @temptable 

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 mtc.AnnMaintCostMW AS totcash
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN CptlMaintExp	cmecurr ON cmecurr.Refnum = t.Refnum AND cmecurr.CptlCode = 'CURR'
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum 
				INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
				LEFT JOIN CptlMaintExpByRefnum cmeest5 ON cmeest5.Refnum = t.Refnum AND cmeest5.CptlCode = 'EST5' --and cmeest5.TurbineID <> 'STG'
				INNER JOIN OpExCalc oMW ON oMW.Refnum = t.Refnum AND oMW.DataType = 'MW'
				INNER JOIN MaintEquipCalcMWByRefnum mec ON mec.Refnum = t.Refnum
				INNER JOIN NonOHMaintMWByRefnum noh ON noh.Refnum = t.Refnum 
				LEFT JOIN OHEquipCalcMWByRefnum ohc ON ohc.Refnum = t.Refnum 
				LEFT JOIN CTGTurbineProjects ctg ON CTG.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		ORDER BY mtc.AnnMaintCostMW DESC

		SELECT @maxvalue = AVG(totcash) FROM @temptable 
		
		
		
			INSERT ReportGroupMaintenanceMWData (RefNum
			  ,ReportTitle
			  ,ListName
			  ,MaintCapital
			  ,MaintExpenses
			  ,MaintOverhead
			  ,MaintIndex
			  ,MaintMinimum
			  ,MaintMaximum
			  ,NonOverhaulIndex2Yr
			  ,OverhaulIndex
			  ,MajorOverhaulIndex
			  ,LTSAIndex
			  ,EnergyConservation
			  ,Regulatory
			  ,AdminFacilities
			  ,ConstraintRemoval
			  ,TotalCapitalMaintExp
			  ,ForecastRegulatory
			  ,ForecastAllOther
			  ,ForecastTotal
			  ,InventoryValue
			  ,MaintPulverizers
			  ,MaintBoilerAux
			  ,MaintTurbine
			  ,MaintGenerator
			  ,MaintValves
			  ,MaintCondenser
			  ,MaintBaghouse
			  ,MaintPrecipitator
			  ,MaintWetScrubber
			  ,MaintDryScrubber
			  ,MaintSCR
			  ,MaintCTTurbine
			  ,MaintCTGenerator
			  ,MaintLTSA
			  ,MaintTransformer
			  ,MaintHRSG
			  ,MaintCTOther
			  ,MaintFuelDeliverySystem
			  ,MaintCoolingWater
			  ,MaintAshHandling
			  ,MaintDemineralWater
			  ,MaintSiteTransformer
			  ,MaintWasteWater
			  ,MaintOther
			  ,NonOHMaintPulverizers
			  ,NonOHMaintBoilerAux
			  ,NonOHMaintTurbine
			  ,NonOHMaintGenerator
			  ,NonOHMaintValves
			  ,NonOHMaintCondenser
			  ,NonOHMaintBaghouse
			  ,NonOHMaintPrecipitator
			  ,NonOHMaintWetScrubber
			  ,NonOHMaintDryScrubber
			  ,NonOHMaintSCR
			  ,NonOHMaintCTTurbine
			  ,NonOHMaintCombustorInsp
			  ,NonOHMaintCTGenerator
			  ,NonOHMaintTransformer
			  ,NonOHMaintHRSG
			  ,NonOHMaintCTOther
			  ,NonOHMaintFuelDeliverySystem
			  ,NonOHMaintCoolingWater
			  ,NonOHMaintAshHandling
			  ,NonOHMaintDemineralWater
			  ,NonOHMaintSiteTransformer
			  ,NonOHMaintWasteWater
			  ,NonOHMaintOther
			  ,OHExpPulverizers
			  ,OHExpBoilerAux
			  ,OHExpHPS
			  ,OHExpIPS
			  ,OHExpLPS
			  ,OHExpProjects
			  ,OHExpGenerator
			  ,OHExpValves
			  ,OHExpCondenser
			  ,OHExpBaghouse
			  ,OHExpPrecipitator
			  ,OHExpWetScrubber
			  ,OHExpDryScrubber
			  ,OHExpSCR
			  ,OHExpCTHotGasPathInsp
			  ,OHExpCTMajorOverhaul
			  ,OHExpCTProjects
			  ,OHExpCTGenerator
			  ,OHExpLTSA
			  ,OHExpTransformer
			  ,OHExpHRSG
			  ,OHExpCTOther
			  ,OHExpFuelDeliverySystem
			  ,OHExpCoolingWater
			  ,OHExpAshHandling
			  ,OHExpDemineralWater
			  ,OHExpSiteTransformer
			  ,OHExpWasteWater
			  ,OHExpOther)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				MaintCapital = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.STMaintCptl,0))/SUM(nf.NMC) END,
				MaintExpenses = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.STMaintExp,0))/SUM(nf.NMC) END,
				MaintOverhead = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.STMMO,0))/SUM(nf.NMC) END,
				MaintIndex = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMW, nf.NMC2Yr),
				MaintMinimum = @minvalue,
				MaintMaximum = @maxvalue,
				NonOverhaulIndex2Yr = GlobalDB.dbo.WtAvg(mtc.AnnNonOHCostMW, nf.NMC2Yr),
				OverhaulIndex = GlobalDB.dbo.WtAvg(mtc.AnnOHCostMW, nf.NMC2Yr),
				MajorOverhaulIndex = GlobalDB.dbo.WtAvg(mtc.AnnOHProjCostMW, nf.NMC2Yr),
				LTSAIndex = GlobalDB.dbo.WtAvg(mtc.AnnLTSACostMW, nf.NMC2Yr),
				EnergyConservation = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.Energy,0))/SUM(nf.NMC) END,
				Regulatory = CASE WHEN SUM(nf.NMC) <> 0 AND SUM(cmecurr.TotCptl) > 0 THEN SUM(ISNULL(cmecurr.RegEnv,0))/SUM(nf.NMC) END,
				AdminFacilities = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.Admin,0))/SUM(nf.NMC) END,
				ConstraintRemoval = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.ConstrRmvl,0))/SUM(nf.NMC) END,
				TotalCapitalMaintExp = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.InvestCptl,0))/SUM(nf.NMC) END,
				ForecastRegulatory = SUM(CASE WHEN cmeest5.RegEnv IS NOT NULL THEN cmeest5.RegEnv END)/SUM(CASE WHEN cmeest5.RegEnv IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END),
				ForecastAllOther = SUM(CASE WHEN cmeest5.OtherCptl IS NOT NULL THEN cmeest5.OtherCptl END)/SUM(CASE WHEN cmeest5.OtherCptl IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END),
				ForecastTotal = SUM(CASE WHEN cmeest5.TotCptl <> 0 THEN cmeest5.TotCptl END)/SUM(CASE WHEN cmeest5.TotCptl*nf.NMC <> 0 THEN nf.NMC END),
				InventoryValue = GlobalDB.dbo.WtAvgNZ(oMW.AllocSparesInven, nf.NMC),
				MaintPulverizers = GlobalDB.dbo.WtAvgNZ(mec.CPHM, nf.NMC2Yr),
				MaintBoilerAux = GlobalDB.dbo.WtAvgNZ(mec.BOIL, nf.NMC2Yr),
				MaintTurbine = GlobalDB.dbo.WtAvgNZ(mec.TURB, nf.NMC2Yr),
				MaintGenerator = GlobalDB.dbo.WtAvgNZ(mec.GEN, nf.NMC2Yr),
				MaintValves = GlobalDB.dbo.WtAvgNZ(mec.VC, nf.NMC2Yr),
				MaintCondenser = GlobalDB.dbo.WtAvgNZ(mec.CA, nf.NMC2Yr),
				MaintBaghouse = GlobalDB.dbo.WtAvgNZ(mec.BAG, nf.NMC2Yr),
				MaintPrecipitator = GlobalDB.dbo.WtAvgNZ(mec.PREC, nf.NMC2Yr),
				MaintWetScrubber = GlobalDB.dbo.WtAvgNZ(mec.WGS, nf.NMC2Yr),
				MaintDryScrubber = GlobalDB.dbo.WtAvgNZ(mec.DGS, nf.NMC2Yr),
				MaintSCR = GlobalDB.dbo.WtAvgNZ(ISNULL(mec.CTSCR, mec.SCR), nf.NMC2Yr),
				MaintCTTurbine = GlobalDB.dbo.WtAvgNZ(mec.CTTURB, nf.NMC2Yr),
				MaintCTGenerator = GlobalDB.dbo.WtAvgNZ(mec.CTGEN, nf.NMC2Yr),
				MaintLTSA = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMW, nf.NMC2Yr),
				MaintTransformer = GlobalDB.dbo.WtAvgNZ(mec.CTTRANS, nf.NMC2Yr),
				MaintHRSG = GlobalDB.dbo.WtAvgNZ(ISNULL(mec.CTHRSG, mec.HRSG), nf.NMC2Yr),
				MaintCTOther = GlobalDB.dbo.WtAvgNZ(mec.CTOTH, nf.NMC2Yr),
				MaintFuelDeliverySystem = GlobalDB.dbo.WtAvgNZ(mec.FHF, nf.NMC2Yr),
				MaintCoolingWater = GlobalDB.dbo.WtAvgNZ(mec.CWF, nf.NMC2Yr),
				MaintAshHandling = GlobalDB.dbo.WtAvgNZ(mec.ASH, nf.NMC2Yr),
				MaintDemineralWater = GlobalDB.dbo.WtAvgNZ(ISNULL(mec.BOILH2O, mec.DQWS), nf.NMC2Yr),
				MaintSiteTransformer = GlobalDB.dbo.WtAvgNZ(mec.TRANS, nf.NMC2Yr),
				MaintWasteWater = GlobalDB.dbo.WtAvgNZ(mec.WASTEH2O, nf.NMC2Yr),
				MaintOther = GlobalDB.dbo.WtAvgNZ(mec.OTHER, nf.NMC2Yr),
				NonOHMaintPulverizers = GlobalDB.dbo.WtAvgNN(noh.CPHM, nf.NMC2Yr),
				--writing the next one out by hand since we're having to use more than two fields, so the easy ways won't work
				NonOHMaintBoilerAux = SUM((ISNULL(noh.boil,0) * ISNULL(nf.NMC2Yr,0)) + (ISNULL(noh.blrair,0) * ISNULL(nf.NMC2Yr,0)) + 
					(ISNULL(noh.blrboil,0) * ISNULL(nf.NMC2Yr,0)) + (ISNULL(noh.blrcond,0) * ISNULL(nf.NMC2Yr,0))) /
					SUM(CASE WHEN (ISNULL(noh.boil,0) + ISNULL(noh.blrair,0) + ISNULL(noh.blrboil,0) + ISNULL(noh.blrcond,0)) > 0 AND nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
				NonOHMaintTurbine = GlobalDB.dbo.WtAvgNN(noh.STGTURB, nf.NMC2Yr),
				NonOHMaintGenerator = GlobalDB.dbo.WtAvgNN(noh.STGGEN, nf.NMC2Yr),
				NonOHMaintValves = GlobalDB.dbo.WtAvgNN(noh.STGVC, nf.NMC2Yr),
				NonOHMaintCondenser = GlobalDB.dbo.WtAvgNN(noh.STGCA, nf.NMC2Yr),
				NonOHMaintBaghouse = GlobalDB.dbo.WtAvgNN(noh.BAG, nf.NMC2Yr),
				NonOHMaintPrecipitator = GlobalDB.dbo.WtAvgNN(noh.PREC, nf.NMC2Yr),
				NonOHMaintWetScrubber = GlobalDB.dbo.WtAvgNN(noh.WGS, nf.NMC2Yr),
				NonOHMaintDryScrubber = GlobalDB.dbo.WtAvgNN(noh.DGS, nf.NMC2Yr),
				NonOHMaintSCR = GlobalDB.dbo.WtAvgNN(ISNULL(noh.CTGSCR, noh.SCR), nf.NMC2Yr),
				NonOHMaintCTTurbine = GlobalDB.dbo.WtAvgNN(noh.CTGTURB, nf.NMC2Yr),
				NonOHMaintCombustorInsp = GlobalDB.dbo.WtAvgNN(noh.CTGCOMB, nf.NMC2Yr),
				NonOHMaintCTGenerator = GlobalDB.dbo.WtAvgNN(noh.CTGGEN, nf.NMC2Yr),
				NonOHMaintTransformer = GlobalDB.dbo.WtAvgNN(noh.CTGTRAN, nf.NMC2Yr),
				NonOHMaintHRSG = GlobalDB.dbo.WtAvgNN(ISNULL(noh.CTGHRSG, noh.HRSG), nf.NMC2Yr),
				NonOHMaintCTOther = GlobalDB.dbo.WtAvgNN(noh.CTGOTH, nf.NMC2Yr),
				NonOHMaintFuelDeliverySystem = GlobalDB.dbo.WtAvgNN(ISNULL(noh.FHF, noh.FUELDEL), nf.NMC2Yr),
				NonOHMaintCoolingWater = GlobalDB.dbo.WtAvgNN(noh.CWF, nf.NMC2Yr),
				NonOHMaintAshHandling = GlobalDB.dbo.WtAvgNN(noh.ASH, nf.NMC2Yr),
				NonOHMaintDemineralWater = GlobalDB.dbo.WtAvgNN(ISNULL(noh.BOILH2O, noh.DQWS), nf.NMC2Yr),
				NonOHMaintSiteTransformer = GlobalDB.dbo.WtAvgNN(noh.SITETRAN, nf.NMC2Yr),
				NonOHMaintWasteWater = GlobalDB.dbo.WtAvgNN(noh.WASTEH2O, nf.NMC2Yr),
				NonOHMaintOther = GlobalDB.dbo.WtAvgNN(noh.OTHER, nf.NMC2Yr),
				OHExpPulverizers = GlobalDB.dbo.WtAvgNZ(ohc.CPHM, nf.NMC2Yr),
				OHExpBoilerAux = GlobalDB.dbo.WtAvgNZ(ohc.BOIL, nf.NMC2Yr),
				OHExpHPS = GlobalDB.dbo.WtAvgNZ(ohc.STGHPS, nf.NMC2Yr),
				OHExpIPS = GlobalDB.dbo.WtAvgNZ(ohc.STGIPS, nf.NMC2Yr),
				OHExpLPS = GlobalDB.dbo.WtAvgNZ(ohc.STGLPS, nf.NMC2Yr),
				OHExpProjects = GlobalDB.dbo.WtAvgNZ(ohc.STGTURB, nf.NMC2Yr),
				OHExpGenerator = GlobalDB.dbo.WtAvgNZ(ohc.STGGEN, nf.NMC2Yr),
				OHExpValves = GlobalDB.dbo.WtAvgNZ(ohc.STGVC, nf.NMC2Yr),
				OHExpCondenser = GlobalDB.dbo.WtAvgNZ(ohc.STGCA, nf.NMC2Yr),
				OHExpBaghouse = GlobalDB.dbo.WtAvgNZ(ohc.BAG, nf.NMC2Yr),
				OHExpPrecipitator = GlobalDB.dbo.WtAvgNZ(ohc.PREC, nf.NMC2Yr),
				OHExpWetScrubber = GlobalDB.dbo.WtAvgNZ(ohc.WGS, nf.NMC2Yr),
				OHExpDryScrubber = GlobalDB.dbo.WtAvgNZ(ohc.DGS, nf.NMC2Yr),
				OHExpSCR = GlobalDB.dbo.WtAvgNZ(ISNULL(ohc.SCR, ohc.CTGSCR), nf.NMC2Yr),
				OHExpCTHotGasPathInsp = SUM(CASE WHEN ctg.Hgpinsp <> 0 THEN ctg.Hgpinsp END)/SUM(CASE WHEN ctg.Hgpinsp * nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
				OHExpCTMajorOverhaul = SUM(CASE WHEN ctg.Ovhl <> 0 THEN ctg.Ovhl END)/SUM(CASE WHEN ctg.Ovhl * nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
				OHExpCTProjects = SUM(CASE WHEN ctg.Other <> 0 THEN ctg.Other END)/SUM(CASE WHEN ctg.Other * nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
				OHExpCTGenerator = GlobalDB.dbo.WtAvgNZ(ohc.CTGGEN, nf.NMC2Yr),
				OHExpLTSA = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMW, nf.NMC2Yr),
				OHExpTransformer = GlobalDB.dbo.WtAvgNZ(ohc.CTGTRAN, nf.NMC2Yr),
				OHExpHRSG = GlobalDB.dbo.WtAvgNZ(ISNULL(ohc.CTGHRSG, ohc.HRSG), nf.NMC2Yr),
				OHExpCTOther = GlobalDB.dbo.WtAvgNZ(ohc.CTGOTH, nf.NMC2Yr),
				OHExpFuelDeliverySystem = GlobalDB.dbo.WtAvgNZ(ohc.FHF, nf.NMC2Yr),
				OHExpCoolingWater = GlobalDB.dbo.WtAvgNZ(ohc.CWF, nf.NMC2Yr),
				OHExpAshHandling = GlobalDB.dbo.WtAvgNZ(ohc.ASH, nf.NMC2Yr),
				OHExpDemineralWater = GlobalDB.dbo.WtAvgNZ(ISNULL(ohc.BOILH2O, ohc.DQWS), nf.NMC2Yr),
				OHExpSiteTransformer = GlobalDB.dbo.WtAvgNZ(ohc.SITETRAN, nf.NMC2Yr),
				OHExpWasteWater = GlobalDB.dbo.WtAvgNZ(ohc.WASTEH2O, nf.NMC2Yr),
				OHExpOther = GlobalDB.dbo.WtAvgNZ(ohc.OTHER, nf.NMC2Yr)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN CptlMaintExp	cmecurr ON cmecurr.Refnum = t.Refnum AND cmecurr.CptlCode = 'CURR'
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum 
				INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
				LEFT JOIN CptlMaintExpByRefnum cmeest5 ON cmeest5.Refnum = t.Refnum AND cmeest5.CptlCode = 'EST5' --and cmeest5.TurbineID <> 'STG'
				INNER JOIN OpExCalc oMW ON oMW.Refnum = t.Refnum AND oMW.DataType = 'KW'
				INNER JOIN MaintEquipCalcMWByRefnum mec ON mec.Refnum = t.Refnum
				INNER JOIN NonOHMaintMWByRefnum noh ON noh.Refnum = t.Refnum 
				LEFT JOIN OHEquipCalcMWByRefnum ohc ON ohc.Refnum = t.Refnum 
				LEFT JOIN CTGTurbineProjects ctg ON CTG.Refnum = t.Refnum 
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it
		--DECLARE @temptable TABLE (totcash REAL)
		--DECLARE @minvalue REAL
		--DECLARE @maxvalue REAL

		EXEC ('		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL
		
		
		INSERT @temptable
		SELECT TOP 2 mtc.AnnMaintCostMW AS totcash
		FROM TSort t 
			INNER JOIN CptlMaintExp	cmecurr ON cmecurr.Refnum = t.Refnum AND cmecurr.CptlCode = ''CURR''
			INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum 
			INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
			LEFT JOIN CptlMaintExpByRefnum cmeest5 ON cmeest5.Refnum = t.Refnum AND cmeest5.CptlCode = ''EST5'' --and cmeest5.TurbineID <> ''STG''
			INNER JOIN OpExCalc oMW ON oMW.Refnum = t.Refnum AND oMW.DataType = ''KW''
			INNER JOIN MaintEquipCalcMWByRefnum mec ON mec.Refnum = t.Refnum
			INNER JOIN NonOHMaintMWByRefnum noh ON noh.Refnum = t.Refnum 
			LEFT JOIN OHEquipCalcMWByRefnum ohc ON ohc.Refnum = t.Refnum 
			LEFT JOIN CTGTurbineProjects ctg ON CTG.Refnum = t.Refnum 
			INNER JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
				' ORDER BY mtc.AnnMaintCostMW ASC 


		SELECT @minvalue =AVG(totcash) FROM @temptable 

		DELETE FROM @temptable 

		
		INSERT @temptable
		SELECT TOP 2 mtc.AnnMaintCostMW AS totcash
		FROM TSort t 
			INNER JOIN CptlMaintExp	cmecurr ON cmecurr.Refnum = t.Refnum AND cmecurr.CptlCode = ''CURR''
			INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum 
			INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
			LEFT JOIN CptlMaintExpByRefnum cmeest5 ON cmeest5.Refnum = t.Refnum AND cmeest5.CptlCode = ''EST5'' --and cmeest5.TurbineID <> ''STG''
			INNER JOIN OpExCalc oMW ON oMW.Refnum = t.Refnum AND oMW.DataType = ''KW''
			INNER JOIN MaintEquipCalcMWByRefnum mec ON mec.Refnum = t.Refnum
			INNER JOIN NonOHMaintMWByRefnum noh ON noh.Refnum = t.Refnum 
			LEFT JOIN OHEquipCalcMWByRefnum ohc ON ohc.Refnum = t.Refnum 
			LEFT JOIN CTGTurbineProjects ctg ON CTG.Refnum = t.Refnum 
			INNER JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
				' ORDER BY mtc.AnnMaintCostMW DESC 

		SELECT @maxvalue = AVG(totcash)  FROM @temptable 
		
		

			
			INSERT ReportGroupMaintenanceMWData (RefNum
			  ,ReportTitle
			  ,ListName
			  ,MaintCapital
			  ,MaintExpenses
			  ,MaintOverhead
			  ,MaintIndex
			  ,MaintMinimum
			  ,MaintMaximum
			  ,NonOverhaulIndex2Yr
			  ,OverhaulIndex
			  ,MajorOverhaulIndex
			  ,LTSAIndex
			  ,EnergyConservation
			  ,Regulatory
			  ,AdminFacilities
			  ,ConstraintRemoval
			  ,TotalCapitalMaintExp
			  ,ForecastRegulatory
			  ,ForecastAllOther
			  ,ForecastTotal
			  ,InventoryValue
			  ,MaintPulverizers
			  ,MaintBoilerAux
			  ,MaintTurbine
			  ,MaintGenerator
			  ,MaintValves
			  ,MaintCondenser
			  ,MaintBaghouse
			  ,MaintPrecipitator
			  ,MaintWetScrubber
			  ,MaintDryScrubber
			  ,MaintSCR
			  ,MaintCTTurbine
			  ,MaintCTGenerator
			  ,MaintLTSA
			  ,MaintTransformer
			  ,MaintHRSG
			  ,MaintCTOther
			  ,MaintFuelDeliverySystem
			  ,MaintCoolingWater
			  ,MaintAshHandling
			  ,MaintDemineralWater
			  ,MaintSiteTransformer
			  ,MaintWasteWater
			  ,MaintOther
			  ,NonOHMaintPulverizers
			  ,NonOHMaintBoilerAux
			  ,NonOHMaintTurbine
			  ,NonOHMaintGenerator
			  ,NonOHMaintValves
			  ,NonOHMaintCondenser
			  ,NonOHMaintBaghouse
			  ,NonOHMaintPrecipitator
			  ,NonOHMaintWetScrubber
			  ,NonOHMaintDryScrubber
			  ,NonOHMaintSCR
			  ,NonOHMaintCTTurbine
			  ,NonOHMaintCombustorInsp
			  ,NonOHMaintCTGenerator
			  ,NonOHMaintTransformer
			  ,NonOHMaintHRSG
			  ,NonOHMaintCTOther
			  ,NonOHMaintFuelDeliverySystem
			  ,NonOHMaintCoolingWater
			  ,NonOHMaintAshHandling
			  ,NonOHMaintDemineralWater
			  ,NonOHMaintSiteTransformer
			  ,NonOHMaintWasteWater
			  ,NonOHMaintOther
			  ,OHExpPulverizers
			  ,OHExpBoilerAux
			  ,OHExpHPS
			  ,OHExpIPS
			  ,OHExpLPS
			  ,OHExpProjects
			  ,OHExpGenerator
			  ,OHExpValves
			  ,OHExpCondenser
			  ,OHExpBaghouse
			  ,OHExpPrecipitator
			  ,OHExpWetScrubber
			  ,OHExpDryScrubber
			  ,OHExpSCR
			  ,OHExpCTHotGasPathInsp
			  ,OHExpCTMajorOverhaul
			  ,OHExpCTProjects
			  ,OHExpCTGenerator
			  ,OHExpLTSA
			  ,OHExpTransformer
			  ,OHExpHRSG
			  ,OHExpCTOther
			  ,OHExpFuelDeliverySystem
			  ,OHExpCoolingWater
			  ,OHExpAshHandling
			  ,OHExpDemineralWater
			  ,OHExpSiteTransformer
			  ,OHExpWasteWater
			  ,OHExpOther)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					MaintCapital = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.STMaintCptl,0))/SUM(nf.NMC) END,
					MaintExpenses = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.STMaintExp,0))/SUM(nf.NMC) END,
					MaintOverhead = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.STMMO,0))/SUM(nf.NMC) END,
					MaintIndex = GlobalDB.dbo.WtAvg(mtc.AnnMaintCostMW, nf.NMC2Yr),
					MaintMinimum = @minvalue,
					MaintMaximum = @maxvalue,
					NonOverhaulIndex2Yr = GlobalDB.dbo.WtAvg(mtc.AnnNonOHCostMW, nf.NMC2Yr),
					OverhaulIndex = GlobalDB.dbo.WtAvg(mtc.AnnOHCostMW, nf.NMC2Yr),
					MajorOverhaulIndex = GlobalDB.dbo.WtAvg(mtc.AnnOHProjCostMW, nf.NMC2Yr),
					LTSAIndex = GlobalDB.dbo.WtAvg(mtc.AnnLTSACostMW, nf.NMC2Yr),
					EnergyConservation = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.Energy,0))/SUM(nf.NMC) END,
					Regulatory = CASE WHEN SUM(nf.NMC) <> 0 AND SUM(cmecurr.TotCptl) > 0  THEN SUM(ISNULL(cmecurr.RegEnv,0))/SUM(nf.NMC) END,
					AdminFacilities = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.Admin,0))/SUM(nf.NMC) END,
					ConstraintRemoval = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.ConstrRmvl,0))/SUM(nf.NMC) END,
					TotalCapitalMaintExp = CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(cmecurr.InvestCptl,0))/SUM(nf.NMC) END,
					ForecastRegulatory = SUM(CASE WHEN cmeest5.RegEnv IS NOT NULL THEN cmeest5.RegEnv END)/SUM(CASE WHEN cmeest5.RegEnv IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END),
					ForecastAllOther = SUM(CASE WHEN cmeest5.OtherCptl IS NOT NULL THEN cmeest5.OtherCptl END)/SUM(CASE WHEN cmeest5.OtherCptl IS NOT NULL AND nf.NMC <> 0 THEN nf.NMC END),
					ForecastTotal = SUM(CASE WHEN cmeest5.TotCptl <> 0 THEN cmeest5.TotCptl END)/SUM(CASE WHEN cmeest5.TotCptl*nf.NMC <> 0 THEN nf.NMC END),
					InventoryValue = GlobalDB.dbo.WtAvgNZ(oMW.AllocSparesInven, nf.NMC),
					MaintPulverizers = GlobalDB.dbo.WtAvgNZ(mec.CPHM, nf.NMC2Yr),
					MaintBoilerAux = GlobalDB.dbo.WtAvgNZ(mec.BOIL, nf.NMC2Yr),
					MaintTurbine = GlobalDB.dbo.WtAvgNZ(mec.TURB, nf.NMC2Yr),
					MaintGenerator = GlobalDB.dbo.WtAvgNZ(mec.GEN, nf.NMC2Yr),
					MaintValves = GlobalDB.dbo.WtAvgNZ(mec.VC, nf.NMC2Yr),
					MaintCondenser = GlobalDB.dbo.WtAvgNZ(mec.CA, nf.NMC2Yr),
					MaintBaghouse = GlobalDB.dbo.WtAvgNZ(mec.BAG, nf.NMC2Yr),
					MaintPrecipitator = GlobalDB.dbo.WtAvgNZ(mec.PREC, nf.NMC2Yr),
					MaintWetScrubber = GlobalDB.dbo.WtAvgNZ(mec.WGS, nf.NMC2Yr),
					MaintDryScrubber = GlobalDB.dbo.WtAvgNZ(mec.DGS, nf.NMC2Yr),
					MaintSCR = GlobalDB.dbo.WtAvgNZ(ISNULL(mec.CTSCR, mec.SCR), nf.NMC2Yr),
					MaintCTTurbine = GlobalDB.dbo.WtAvgNZ(mec.CTTURB, nf.NMC2Yr),
					MaintCTGenerator = GlobalDB.dbo.WtAvgNZ(mec.CTGEN, nf.NMC2Yr),
					MaintLTSA = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMW, nf.NMC2Yr),
					MaintTransformer = GlobalDB.dbo.WtAvgNZ(mec.CTTRANS, nf.NMC2Yr),
					MaintHRSG = GlobalDB.dbo.WtAvgNZ(ISNULL(mec.CTHRSG, mec.HRSG), nf.NMC2Yr),
					MaintCTOther = GlobalDB.dbo.WtAvgNZ(mec.CTOTH, nf.NMC2Yr),
					MaintFuelDeliverySystem = GlobalDB.dbo.WtAvgNZ(mec.FHF, nf.NMC2Yr),
					MaintCoolingWater = GlobalDB.dbo.WtAvgNZ(mec.CWF, nf.NMC2Yr),
					MaintAshHandling = GlobalDB.dbo.WtAvgNZ(mec.ASH, nf.NMC2Yr),
					MaintDemineralWater = GlobalDB.dbo.WtAvgNZ(ISNULL(mec.BOILH2O, mec.DQWS), nf.NMC2Yr),
					MaintSiteTransformer = GlobalDB.dbo.WtAvgNZ(mec.TRANS, nf.NMC2Yr),
					MaintWasteWater = GlobalDB.dbo.WtAvgNZ(mec.WASTEH2O, nf.NMC2Yr),
					MaintOther = GlobalDB.dbo.WtAvgNZ(mec.OTHER, nf.NMC2Yr),
					NonOHMaintPulverizers = GlobalDB.dbo.WtAvgNN(noh.CPHM, nf.NMC2Yr),
					--writing the next one out by hand since we''re having to use more than two fields, so the easy ways won''t work
					NonOHMaintBoilerAux = SUM((ISNULL(noh.boil,0) * ISNULL(nf.NMC2Yr,0)) + (ISNULL(noh.blrair,0) * ISNULL(nf.NMC2Yr,0)) + 
						(ISNULL(noh.blrboil,0) * ISNULL(nf.NMC2Yr,0)) + (ISNULL(noh.blrcond,0) * ISNULL(nf.NMC2Yr,0))) /
						SUM(CASE WHEN (ISNULL(noh.boil,0) + ISNULL(noh.blrair,0) + ISNULL(noh.blrboil,0) + ISNULL(noh.blrcond,0)) > 0 AND nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
					NonOHMaintTurbine = GlobalDB.dbo.WtAvgNN(noh.STGTURB, nf.NMC2Yr),
					NonOHMaintGenerator = GlobalDB.dbo.WtAvgNN(noh.STGGEN, nf.NMC2Yr),
					NonOHMaintValves = GlobalDB.dbo.WtAvgNN(noh.STGVC, nf.NMC2Yr),
					NonOHMaintCondenser = GlobalDB.dbo.WtAvgNN(noh.STGCA, nf.NMC2Yr),
					NonOHMaintBaghouse = GlobalDB.dbo.WtAvgNN(noh.BAG, nf.NMC2Yr),
					NonOHMaintPrecipitator = GlobalDB.dbo.WtAvgNN(noh.PREC, nf.NMC2Yr),
					NonOHMaintWetScrubber = GlobalDB.dbo.WtAvgNN(noh.WGS, nf.NMC2Yr),
					NonOHMaintDryScrubber = GlobalDB.dbo.WtAvgNN(noh.DGS, nf.NMC2Yr),
					NonOHMaintSCR = GlobalDB.dbo.WtAvgNN(ISNULL(noh.CTGSCR, noh.SCR), nf.NMC2Yr),
					NonOHMaintCTTurbine = GlobalDB.dbo.WtAvgNN(noh.CTGTURB, nf.NMC2Yr),
					NonOHMaintCombustorInsp = GlobalDB.dbo.WtAvgNN(noh.CTGCOMB, nf.NMC2Yr),
					NonOHMaintCTGenerator = GlobalDB.dbo.WtAvgNN(noh.CTGGEN, nf.NMC2Yr),
					NonOHMaintTransformer = GlobalDB.dbo.WtAvgNN(noh.CTGTRAN, nf.NMC2Yr),
					NonOHMaintHRSG = GlobalDB.dbo.WtAvgNN(ISNULL(noh.CTGHRSG, noh.HRSG), nf.NMC2Yr),
					NonOHMaintCTOther = GlobalDB.dbo.WtAvgNN(noh.CTGOTH, nf.NMC2Yr),
					NonOHMaintFuelDeliverySystem = GlobalDB.dbo.WtAvgNN(ISNULL(noh.FHF, noh.FUELDEL), nf.NMC2Yr),
					NonOHMaintCoolingWater = GlobalDB.dbo.WtAvgNN(noh.CWF, nf.NMC2Yr),
					NonOHMaintAshHandling = GlobalDB.dbo.WtAvgNN(noh.ASH, nf.NMC2Yr),
					NonOHMaintDemineralWater = GlobalDB.dbo.WtAvgNN(ISNULL(noh.BOILH2O, noh.DQWS), nf.NMC2Yr),
					NonOHMaintSiteTransformer = GlobalDB.dbo.WtAvgNN(noh.SITETRAN, nf.NMC2Yr),
					NonOHMaintWasteWater = GlobalDB.dbo.WtAvgNN(noh.WASTEH2O, nf.NMC2Yr),
					NonOHMaintOther = GlobalDB.dbo.WtAvgNN(noh.OTHER, nf.NMC2Yr),
					OHExpPulverizers = GlobalDB.dbo.WtAvgNZ(ohc.CPHM, nf.NMC2Yr),
					OHExpBoilerAux = GlobalDB.dbo.WtAvgNZ(ohc.BOIL, nf.NMC2Yr),
					OHExpHPS = GlobalDB.dbo.WtAvgNZ(ohc.STGHPS, nf.NMC2Yr),
					OHExpIPS = GlobalDB.dbo.WtAvgNZ(ohc.STGIPS, nf.NMC2Yr),
					OHExpLPS = GlobalDB.dbo.WtAvgNZ(ohc.STGLPS, nf.NMC2Yr),
					OHExpProjects = GlobalDB.dbo.WtAvgNZ(ohc.STGTURB, nf.NMC2Yr),
					OHExpGenerator = GlobalDB.dbo.WtAvgNZ(ohc.STGGEN, nf.NMC2Yr),
					OHExpValves = GlobalDB.dbo.WtAvgNZ(ohc.STGVC, nf.NMC2Yr),
					OHExpCondenser = GlobalDB.dbo.WtAvgNZ(ohc.STGCA, nf.NMC2Yr),
					OHExpBaghouse = GlobalDB.dbo.WtAvgNZ(ohc.BAG, nf.NMC2Yr),
					OHExpPrecipitator = GlobalDB.dbo.WtAvgNZ(ohc.PREC, nf.NMC2Yr),
					OHExpWetScrubber = GlobalDB.dbo.WtAvgNZ(ohc.WGS, nf.NMC2Yr),
					OHExpDryScrubber = GlobalDB.dbo.WtAvgNZ(ohc.DGS, nf.NMC2Yr),
					OHExpSCR = GlobalDB.dbo.WtAvgNZ(ISNULL(ohc.SCR, ohc.CTGSCR), nf.NMC2Yr),
					OHExpCTHotGasPathInsp = SUM(CASE WHEN ctg.Hgpinsp <> 0 THEN ctg.Hgpinsp END)/SUM(CASE WHEN ctg.Hgpinsp * nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
					OHExpCTMajorOverhaul = SUM(CASE WHEN ctg.Ovhl <> 0 THEN ctg.Ovhl END)/SUM(CASE WHEN ctg.Ovhl * nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
					OHExpCTProjects = SUM(CASE WHEN ctg.Other <> 0 THEN ctg.Other END)/SUM(CASE WHEN ctg.Other * nf.NMC2Yr <> 0 THEN nf.NMC2Yr END),
					OHExpCTGenerator = GlobalDB.dbo.WtAvgNZ(ohc.CTGGEN, nf.NMC2Yr),
					OHExpLTSA = GlobalDB.dbo.WtAvgNZ(mtc.AnnLTSACostMW, nf.NMC2Yr),
					OHExpTransformer = GlobalDB.dbo.WtAvgNZ(ohc.CTGTRAN, nf.NMC2Yr),
					OHExpHRSG = GlobalDB.dbo.WtAvgNZ(ohc.CTGHRSG, nf.NMC2Yr),
					OHExpCTOther = GlobalDB.dbo.WtAvgNZ(ohc.CTGOTH, nf.NMC2Yr),
					OHExpFuelDeliverySystem = GlobalDB.dbo.WtAvgNZ(ohc.FHF, nf.NMC2Yr),
					OHExpCoolingWater = GlobalDB.dbo.WtAvgNZ(ohc.CWF, nf.NMC2Yr),
					OHExpAshHandling = GlobalDB.dbo.WtAvgNZ(ohc.ASH, nf.NMC2Yr),
					OHExpDemineralWater = GlobalDB.dbo.WtAvgNZ(ISNULL(ohc.BOILH2O, ohc.DQWS), nf.NMC2Yr),
					OHExpSiteTransformer = GlobalDB.dbo.WtAvgNZ(ohc.SITETRAN, nf.NMC2Yr),
					OHExpWasteWater = GlobalDB.dbo.WtAvgNZ(ohc.WASTEH2O, nf.NMC2Yr),
					OHExpOther = GlobalDB.dbo.WtAvgNZ(ohc.OTHER, nf.NMC2Yr)
				FROM TSort t 
					INNER JOIN CptlMaintExp	cmecurr ON cmecurr.Refnum = t.Refnum AND cmecurr.CptlCode = ''CURR''
					INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum 
					INNER JOIN MaintTotCalc mtc ON mtc.Refnum = t.Refnum
					LEFT JOIN CptlMaintExpByRefnum cmeest5 ON cmeest5.Refnum = t.Refnum AND cmeest5.CptlCode = ''EST5'' --and cmeest5.TurbineID <> ''STG''
					INNER JOIN OpExCalc oMW ON oMW.Refnum = t.Refnum AND oMW.DataType = ''KW''
					INNER JOIN MaintEquipCalcMWByRefnum mec ON mec.Refnum = t.Refnum
					INNER JOIN NonOHMaintMWByRefnum noh ON noh.Refnum = t.Refnum 
					LEFT JOIN OHEquipCalcMWByRefnum ohc ON ohc.Refnum = t.Refnum 
					LEFT JOIN CTGTurbineProjects ctg ON CTG.Refnum = t.Refnum 
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END





