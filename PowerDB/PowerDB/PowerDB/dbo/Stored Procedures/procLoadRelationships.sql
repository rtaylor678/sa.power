﻿CREATE PROC procLoadRelationships AS
INSERT INTO DD_Relationships
SELECT a.ObjectID, b.ObjectID, c.ColumnID, d.ColumnID
FROM DD_Objects a, DD_Objects b, DD_Columns c, DD_Columns d
WHERE a.ObjectID < b.ObjectID AND a.ObjectID = c.ObjectID
AND b.ObjectID = d.ObjectID AND c.ColumnName = d.ColumnName
AND ((NOT EXISTS (SELECT * FROM DD_Relationships
	WHERE ObjectA IN (a.ObjectID, b.ObjectID)))
OR (NOT EXISTS (SELECT * FROM DD_Relationships
	WHERE ObjectB IN (a.ObjectID, b.ObjectID))))
ORDER BY a.ObjectID, b.ObjectID
