﻿




CREATE PROC [dbo].[spLoadReportGroupExpenseMWData] (@ReportRefnum char(20))
AS
	--this procedure takes a single Report from ReportGroups and loads its General data into ReportGroupGeneralData
	--@ReportRefnum must be one of the unique values in the Refnum field in ReportGroups

	DECLARE @ReportTitle char(50)
	DECLARE @ListName char(20)

	SET @ReportTitle = (SELECT ReportTitle FROM ReportGroups WHERE RefNum = @ReportRefnum)
	SET @ListName = (SELECT ListName FROM ReportGroups WHERE RefNum = @ReportRefnum)

		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL


	
	--remove the existing record in the table
	DELETE FROM ReportGroupExpenseMWData WHERE Refnum = @ReportRefnum

	IF EXISTS (SELECT * FROM ReportGroups WHERE Refnum = @ReportRefnum AND TileDescription IS NOT NULL)
		--this block looks for the records with a TileDescription, because they have some extra steps that need to be done
		--to handle the Quartile data and how it is pulled
		BEGIN

			--the following vars are used to get the right results for the quartile records
			DECLARE @tiledesc varchar (40)
			DECLARE @breakvalue varchar(12)
			DECLARE @breakcondition varchar(30)
			DECLARE @tile tinyint

			----all the following is just grabbing the set of values that will be needed for the output query
			set @tiledesc = (SELECT tiledescription from ReportGroups where RefNum = @reportrefnum)
			set @breakvalue = (select tilebreakvalue from ReportGroups where RefNum = @reportrefnum)
			set @breakcondition = (select tilebreakcondition from ReportGroups where RefNum = @ReportRefnum)
			set @tile = (select tiletile from ReportGroups where RefNum = @ReportRefnum )


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it


		INSERT @temptable
		SELECT TOP 2 omw.totcash AS totcash
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omw ON omw.refnum = t.refnum AND omw.DataType = 'KW'
				INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = 'KW'
				INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
				INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
				LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
				LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		ORDER BY omw.TotCash ASC

		SELECT @minvalue = AVG(totcash) FROM @temptable 

		DELETE FROM @temptable 

		INSERT @temptable
		SELECT TOP 2 omw.totcash AS totcash
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omw ON omw.refnum = t.refnum AND omw.DataType = 'KW'
				INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = 'KW'
				INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
				INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
				LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
				LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		ORDER BY omw.TotCash DESC

		SELECT @maxvalue = AVG(totcash) FROM @temptable 
		
		
		
			INSERT ReportGroupExpenseMWData (RefNum
			  ,ReportTitle
			  ,ListName
			  ,FixedExpMW
			  ,VariableExpMW
			  ,TotalCashExpMW
			  ,TotalCashRangeMinimumMW
			  ,TotalCashRangeMaximumMW
			  ,OCCWagesMW
			  ,MPSWagesMW
			  ,SubTotalWagesMW
			  ,OCCBenefitsMW
			  ,MPSBenefitsMW
			  ,SubTotalBenefitsMW
			  ,CentralOCCWageBenMW
			  ,CentralMPSWageBenMW
			  ,MaintMaterialsMW
			  ,ContractMaintLaborMW
			  ,ContractMaintMaterialMW
			  ,ContractMaintLumpSumMW
			  ,OtherContractServicesMW
			  ,OverhaulAdjMW
			  ,LTSAAdjMW
			  ,EnvironmentExpMW
			  ,PropertyTaxesMW
			  ,OtherTaxesMW
			  ,InsuranceMW
			  ,AllocatedAGPersCostMW
			  ,AllocatedAGNonPersCostMW
			  ,OtherFixedExpMW
			  ,SubTotalFixedExpMW
			  ,ChemicalsMW
			  ,WaterMW
			  ,GasMW
			  ,LiquidMW
			  ,SolidMW
			  ,SubTotalFuelConsumedMW
			  ,SO2EmissionAdjMW
			  ,NOxEmissionAdjMW
			  ,CO2EmissionAdjMW
			  ,OtherVarExpMW
			  ,SubTotalVarExpMW
			  ,DepreciationMW
			  ,InterestExpMW
			  ,SupplyCarryCostMW
			  ,FuelInvCarryCostMW
			  ,SubTotalNonCashExpMW
			  ,GrandTotalExpMW
			  ,PlantSiteWagesBenefitsMW
			  ,PlantCentralEmployeesMW
			  ,PlantMaterialsMW
			  ,PlantOverhaulsMW
			  ,PlantLTSAPSAMW
			  ,PlantOtherVarMW
			  ,PlantMiscMW
			  ,PlantManageExpLessFuelMW
			  ,AncillaryRevenueMW
			  ,AshSalesMW
			  ,OtherChemicalSalesMW
			  ,OtherRevenueMW
			  ,TotalRevenueMW
			  ,AshOperationsCostMW
			  ,AshDisposalCostMW
			  ,AshTotalCostMW
			  ,ScrubberLimeCostMW
			  ,ScrubberOtherChemCostMW
			  ,ScrubberAuxCostMW
			  ,ScrubberOtherNonMaintCostMW
			  ,ScrubberTotalCostMW
			  ,ScrubbingCostTonMW)

			SELECT @ReportRefnum, 
				@ReportTitle,
				@ListName,
				FixedExpMW = GlobalDB.dbo.WtAvg(omw.STFixed, nf.NMC),
				VariableExpMW = GlobalDB.dbo.WtAvg(omw.STVar, nf.NMC),
				TotalCashExpMW = GlobalDB.dbo.WtAvg(omw.TotCash, nf.NMC),
				TotalCashRangeMinimumMW = @minvalue,
				TotalCashRangeMaximumMW = @maxvalue,
				OCCWagesMW = GlobalDB.dbo.WtAvg(omw.OCCWages, nf.NMC),
				MPSWagesMW = GlobalDB.dbo.WtAvg(omw.MPSWages, nf.NMC),
				SubTotalWagesMW = GlobalDB.dbo.WtAvg(omw.STWages, nf.NMC),
				OCCBenefitsMW = GlobalDB.dbo.WtAvg(omw.OCCBenefits, nf.NMC),
				MPSBenefitsMW = GlobalDB.dbo.WtAvg(omw.MPSBenefits, nf.NMC),
				SubTotalBenefitsMW = GlobalDB.dbo.WtAvg(omw.STBenefits, nf.NMC),
				CentralOCCWageBenMW = GlobalDB.dbo.WtAvg(omw.CentralOCCWagesBen, nf.NMC),
				CentralMPSWageBenMW = GlobalDB.dbo.WtAvg(omw.CentralMPSWagesBen, nf.NMC),
				MaintMaterialsMW = GlobalDB.dbo.WtAvg(omw.MaintMatl, nf.NMC),
				ContractMaintLaborMW = GlobalDB.dbo.WtAvg(omw.ContMaintLabor, nf.NMC),
				ContractMaintMaterialMW = GlobalDB.dbo.WtAvg(omw.ContMaintMatl, nf.NMC),
				ContractMaintLumpSumMW = GlobalDB.dbo.WtAvg(omw.ContMaintLumpSum, nf.NMC),
				OtherContractServicesMW = GlobalDB.dbo.WtAvg(omw.OthContSvc, nf.NMC),
				OverhaulAdjMW = GlobalDB.dbo.WtAvg(omw.OverhaulAdj, nf.NMC),
				LTSAAdjMW = GlobalDB.dbo.WtAvg(omw.LTSAAdj, nf.NMC),
				EnvironmentExpMW = GlobalDB.dbo.WtAvg(omw.Envir, nf.NMC),
				PropertyTaxesMW = GlobalDB.dbo.WtAvg(omw.PropTax, nf.NMC),
				OtherTaxesMW = GlobalDB.dbo.WtAvg(omw.OthTax, nf.NMC),
				InsuranceMW = GlobalDB.dbo.WtAvg(omw.Insurance, nf.NMC),
				AllocatedAGPersCostMW = GlobalDB.dbo.WtAvg(omw.AGPers, nf.NMC),
				AllocatedAGNonPersCostMW = GlobalDB.dbo.WtAvg(omw.AGNonPers, nf.NMC),
				OtherFixedExpMW = GlobalDB.dbo.WtAvg(omw.OthFixed, nf.NMC),
				SubTotalFixedExpMW = GlobalDB.dbo.WtAvg(omw.STFixed, nf.NMC),
				ChemicalsMW = GlobalDB.dbo.WtAvg(omw.Chemicals, nf.NMC),
				WaterMW = GlobalDB.dbo.WtAvg(omw.Water, nf.NMC),
				GasMW = GlobalDB.dbo.WtAvg(omw.PurGas, nf.NMC),
				LiquidMW = GlobalDB.dbo.WtAvg(omw.PurLiquid, nf.NMC),
				SolidMW = GlobalDB.dbo.WtAvg(omw.PurSolid, nf.NMC),
				SubTotalFuelConsumedMW = GlobalDB.dbo.WtAvg(omw.STFuelCost, nf.NMC),
				SO2EmissionAdjMW = GlobalDB.dbo.WtAvg(omw.SO2EmissionAdj, nf.NMC),
				NOxEmissionAdjMW = GlobalDB.dbo.WtAvg(omw.NOxEmissionAdj, nf.NMC),
				COEmissionAdjMW = GlobalDB.dbo.WtAvg(omw.CO2EmissionAdj, nf.NMC),
				OtherVarExpMW = GlobalDB.dbo.WtAvg(omw.OthVar, nf.NMC),
				SubTotalVarExpMW = GlobalDB.dbo.WtAvg(omw.STVar, nf.NMC),
				DepreciationMW = GlobalDB.dbo.WtAvg(omw.Depreciation, nf.NMC),
				InterestExpMW = GlobalDB.dbo.WtAvg(omw.Interest, nf.NMC),
				SupplyCarryCostMW = GlobalDB.dbo.WtAvg(omw.Supply, nf.NMC),
				FuelInvCarryCostMW = GlobalDB.dbo.WtAvg(omw.FuelInven, nf.NMC),
				SubTotalNonCashExpMW = GlobalDB.dbo.WtAvg(omw.STNonCash, nf.NMC),
				GrandTotalExpMW = GlobalDB.dbo.WtAvg(omw.GrandTot, nf.NMC),
				PlantSiteWagesBenefitsMW = GlobalDB.dbo.WtAvg(plnt.SiteWagesBen, nf.NMC),
				PlantCentralEmployeesMW = GlobalDB.dbo.WtAvg(plnt.CentralContract, nf.NMC),
				PlantMaterialsMW = GlobalDB.dbo.WtAvg(plnt.MaintMatl, nf.NMC),
				PlantOverhaulsMW = GlobalDB.dbo.WtAvg(plnt.OverhaulAdj, nf.NMC),
				PlantLTSAPSAMW = GlobalDB.dbo.WtAvg(plnt.LTSAAdj, nf.NMC),
				PlantOtherVarMW = GlobalDB.dbo.WtAvg(plnt.OthVar, nf.NMC),
				PlantMiscMW = GlobalDB.dbo.WtAvg(plnt.Misc, nf.NMC),
				PlantManageExpLessFuelMW = GlobalDB.dbo.WtAvg(omw.ActCashLessFuel, nf.NMC),
				AncillaryRevenueMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.TotAncillaryRevenue,0))/SUM(nf.NMC) END),
				AshSalesMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.RevAsh,0))/SUM(nf.NMC) END),
				OtherChemicalSalesMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.RevOth,0))/SUM(nf.NMC) END),
				OtherRevenueMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.OthNonT7Revenue,0))/SUM(nf.NMC) END),
				TotalRevenueMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.TotRevenueRevised,0))/SUM(nf.NMC) END),
				AshOperationsCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(m.AshCostOP,0))/SUM(nf.NMC) END),
				AshDisposalCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(m.AshCostDisp,0))/SUM(nf.NMC) END),
				AshTotalCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.TotAshHandling,0))/SUM(nf.NMC) END),
				ScrubberLimeCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrCostLime,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
				ScrubberOtherChemCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrCostChem,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
				ScrubberAuxCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrubberPowerCost,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
				ScrubberOtherNonMaintCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrCostNMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
				ScrubberTotalCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.TotScrubberNonMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
				ScrubbingCostTonMW = GlobalDB.dbo.WtAvgNZ(so2.ScrubbingIndex, so2.TonsSO2Removed)
			FROM  [PowerGlobal].[dbo].[RefList_LU] rlu
				inner join [PowerGlobal].[dbo].[RefList] rl on rl.RefListNo  = rlu.RefListNo 
				inner join TSort t on t.Refnum = rl.Refnum 
				INNER JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				INNER JOIN OpExCalc omw ON omw.refnum = t.refnum AND omw.DataType = 'KW'
				INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = 'KW'
				INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
				INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
				LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
				LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
				LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum AND st.TurbineID = 'STG'
				INNER JOIN Breaks b ON b.Refnum = t.Refnum
				inner join _RankView r on r.Refnum = t.Refnum 
			WHERE rlu.listname = @ListName 
				and ((r.Tile = @tile and @tile > 0) or (r.Tile > @tile and @tile = 0)) -- if the table has a number > 0, return that quarter, if 0 return all quarters
				and r.Variable = @tiledesc 
				and r.ListName = @ListName 
				and ((r.BreakValue = @breakvalue and @breakvalue is not null) or (r.BreakValue <> '' and @breakvalue is null))
				and r.BreakCondition = @breakcondition
		END

	ELSE 
		BEGIN
		--this block is the non-Quartile data, so it doesn't need the extra steps that the prior data has

			--but we do need to build a Where block to handle all the different variations
			DECLARE @Where varchar(4000)

			SELECT @Where = 't.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = ''' + RTRIM(@ListName) + ''')'
				+ CASE WHEN rg.Field1Name IS NOT NULL THEN ' AND (' + rg.Field1Name + ' =''' + rg.Field1Value + ''')' ELSE '' END
				+ CASE WHEN rg.Field2Name IS NOT NULL THEN ' AND (' + rg.Field2Name + ' =''' + rg.Field2Value + ''')' ELSE '' END
				+ CASE WHEN rg.SpecialCriteria IS NOT NULL THEN ' AND (' + rg.SpecialCriteria + ')' ELSE '' END
			FROM ReportGroups rg
			WHERE rg.Refnum = @ReportRefnum

			--the following @Turbine part is in here for a specific situation. For most records, the StartsAnalysis table doesn't match up correctly, 
			--as it doesn't have a 1 to 1 match. Adding a qualifier so that TurbineID = 'STG' fixes the mismatch in most cases, but it messes up the
			--StartsGroup calculation for Steam records (possibly others too?). By adding this variable we can control when the query looks for the 
			--STG records or not at the appropriate time, and solve the problem.
			DECLARE @Turbine varchar(100) = ''
			IF CHARINDEX('STMStarts', @ReportRefnum) = 0
				SET @Turbine = ' AND st.TurbineID = ''STG'''


	--the following calculations are to get the average of the two minimum and two maximum records respectively
	--done here because i couldn't get an easier way to do it right in the query
	--if you know of such a way, please do it
		--DECLARE @temptable TABLE (totcash REAL)
		--DECLARE @minvalue REAL
		--DECLARE @maxvalue REAL

		EXEC ('		DECLARE @temptable TABLE (totcash REAL)
		DECLARE @minvalue REAL
		DECLARE @maxvalue REAL
		
		
		INSERT @temptable
		SELECT TOP 2 omw.totcash AS totcash
				FROM TSort t 
					INNER JOIN OpExCalc omw ON omw.refnum = t.refnum AND omw.DataType = ''KW''
					INNER JOIN NercFactors nf ON nf.refnum = t.refnum
					INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = ''KW''
					INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
					INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
					LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
					LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
				' ORDER BY omw.TotCash ASC 

		SELECT @minvalue =AVG(totcash) FROM @temptable 

		DELETE FROM @temptable 

		
		INSERT @temptable
		SELECT TOP 2 omw.totcash AS totcash
				FROM TSort t 
					INNER JOIN OpExCalc omw ON omw.refnum = t.refnum AND omw.DataType = ''KW''
					INNER JOIN NercFactors nf on nf.refnum = t.refnum
					INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = ''KW''
					INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
					INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
					LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
					LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where +
				' ORDER BY omw.TotCash DESC 

		SELECT @maxvalue = AVG(totcash)  FROM @temptable 
		
		

			
			INSERT ReportGroupExpenseMWData (RefNum
				  ,ReportTitle
				  ,ListName
				  ,FixedExpMW
				  ,VariableExpMW
				  ,TotalCashExpMW
				  ,TotalCashRangeMinimumMW
				  ,TotalCashRangeMaximumMW
				  ,OCCWagesMW
				  ,MPSWagesMW
				  ,SubTotalWagesMW
				  ,OCCBenefitsMW
				  ,MPSBenefitsMW
				  ,SubTotalBenefitsMW
				  ,CentralOCCWageBenMW
				  ,CentralMPSWageBenMW
				  ,MaintMaterialsMW
				  ,ContractMaintLaborMW
				  ,ContractMaintMaterialMW
				  ,ContractMaintLumpSumMW
				  ,OtherContractServicesMW
				  ,OverhaulAdjMW
				  ,LTSAAdjMW
				  ,EnvironmentExpMW
				  ,PropertyTaxesMW
				  ,OtherTaxesMW
				  ,InsuranceMW
				  ,AllocatedAGPersCostMW
				  ,AllocatedAGNonPersCostMW
				  ,OtherFixedExpMW
				  ,SubTotalFixedExpMW
				  ,ChemicalsMW
				  ,WaterMW
				  ,GasMW
				  ,LiquidMW
				  ,SolidMW
				  ,SubTotalFuelConsumedMW
				  ,SO2EmissionAdjMW
				  ,NOxEmissionAdjMW
				  ,CO2EmissionAdjMW
				  ,OtherVarExpMW
				  ,SubTotalVarExpMW
				  ,DepreciationMW
				  ,InterestExpMW
				  ,SupplyCarryCostMW
				  ,FuelInvCarryCostMW
				  ,SubTotalNonCashExpMW
				  ,GrandTotalExpMW
				  ,PlantSiteWagesBenefitsMW
				  ,PlantCentralEmployeesMW
				  ,PlantMaterialsMW
				  ,PlantOverhaulsMW
				  ,PlantLTSAPSAMW
				  ,PlantOtherVarMW
				  ,PlantMiscMW
				  ,PlantManageExpLessFuelMW
				  ,AncillaryRevenueMW
				  ,AshSalesMW
				  ,OtherChemicalSalesMW
				  ,OtherRevenueMW
				  ,TotalRevenueMW
				  ,AshOperationsCostMW
				  ,AshDisposalCostMW
				  ,AshTotalCostMW
				  ,ScrubberLimeCostMW
				  ,ScrubberOtherChemCostMW
				  ,ScrubberAuxCostMW
				  ,ScrubberOtherNonMaintCostMW
				  ,ScrubberTotalCostMW
				  ,ScrubbingCostTonMW)
					
				SELECT ''' + @ReportRefnum + ''', 
					''' + @ReportTitle + ''',
					''' + @ListName + ''',
					FixedExpMW = GlobalDB.dbo.WtAvg(omw.STFixed, nf.NMC),
					VariableExpMW = GlobalDB.dbo.WtAvg(omw.STVar, nf.NMC),
					TotalCashExpMW = GlobalDB.dbo.WtAvg(omw.TotCash, nf.NMC),
					TotalCashRangeMinimumMW = @minvalue,
					TotalCashRangeMaximumMW = @maxvalue,
					OCCWagesMW = GlobalDB.dbo.WtAvg(omw.OCCWages, nf.NMC),
					MPSWagesMW = GlobalDB.dbo.WtAvg(omw.MPSWages, nf.NMC),
					SubTotalWagesMW = GlobalDB.dbo.WtAvg(omw.STWages, nf.NMC),
					OCCBenefitsMW = GlobalDB.dbo.WtAvg(omw.OCCBenefits, nf.NMC),
					MPSBenefitsMW = GlobalDB.dbo.WtAvg(omw.MPSBenefits, nf.NMC),
					SubTotalBenefitsMW = GlobalDB.dbo.WtAvg(omw.STBenefits, nf.NMC),
					CentralOCCWageBenMW = GlobalDB.dbo.WtAvg(omw.CentralOCCWagesBen, nf.NMC),
					CentralMPSWageBenMW = GlobalDB.dbo.WtAvg(omw.CentralMPSWagesBen, nf.NMC),
					MaintMaterialsMW = GlobalDB.dbo.WtAvg(omw.MaintMatl, nf.NMC),
					ContractMaintLaborMW = GlobalDB.dbo.WtAvg(omw.ContMaintLabor, nf.NMC),
					ContractMaintMaterialMW = GlobalDB.dbo.WtAvg(omw.ContMaintMatl, nf.NMC),
					ContractMaintLumpSumMW = GlobalDB.dbo.WtAvg(omw.ContMaintLumpSum, nf.NMC),
					OtherContractServicesMW = GlobalDB.dbo.WtAvg(omw.OthContSvc, nf.NMC),
					OverhaulAdjMW = GlobalDB.dbo.WtAvg(omw.OverhaulAdj, nf.NMC),
					LTSAAdjMW = GlobalDB.dbo.WtAvg(omw.LTSAAdj, nf.NMC),
					EnvironmentExpMW = GlobalDB.dbo.WtAvg(omw.Envir, nf.NMC),
					PropertyTaxesMW = GlobalDB.dbo.WtAvg(omw.PropTax, nf.NMC),
					OtherTaxesMW = GlobalDB.dbo.WtAvg(omw.OthTax, nf.NMC),
					InsuranceMW = GlobalDB.dbo.WtAvg(omw.Insurance, nf.NMC),
					AllocatedAGPersCostMW = GlobalDB.dbo.WtAvg(omw.AGPers, nf.NMC),
					AllocatedAGNonPersCostMW = GlobalDB.dbo.WtAvg(omw.AGNonPers, nf.NMC),
					OtherFixedExpMW = GlobalDB.dbo.WtAvg(omw.OthFixed, nf.NMC),
					SubTotalFixedExpMW = GlobalDB.dbo.WtAvg(omw.STFixed, nf.NMC),
					ChemicalsMW = GlobalDB.dbo.WtAvg(omw.Chemicals, nf.NMC),
					WaterMW = GlobalDB.dbo.WtAvg(omw.Water, nf.NMC),
					GasMW = GlobalDB.dbo.WtAvg(omw.PurGas, nf.NMC),
					LiquidMW = GlobalDB.dbo.WtAvg(omw.PurLiquid, nf.NMC),
					SolidMW = GlobalDB.dbo.WtAvg(omw.PurSolid, nf.NMC),
					SubTotalFuelConsumedMW = GlobalDB.dbo.WtAvg(omw.STFuelCost, nf.NMC),
					SO2EmissionAdjMW = GlobalDB.dbo.WtAvg(omw.SO2EmissionAdj, nf.NMC),
					NOxEmissionAdjMW = GlobalDB.dbo.WtAvg(omw.NOxEmissionAdj, nf.NMC),
					COEmissionAdjMW = GlobalDB.dbo.WtAvg(omw.CO2EmissionAdj, nf.NMC),
					OtherVarExpMW = GlobalDB.dbo.WtAvg(omw.OthVar, nf.NMC),
					SubTotalVarExpMW = GlobalDB.dbo.WtAvg(omw.STVar, nf.NMC),
					DepreciationMW = GlobalDB.dbo.WtAvg(omw.Depreciation, nf.NMC),
					InterestExpMW = GlobalDB.dbo.WtAvg(omw.Interest, nf.NMC),
					SupplyCarryCostMW = GlobalDB.dbo.WtAvg(omw.Supply, nf.NMC),
					FuelInvCarryCostMW = GlobalDB.dbo.WtAvg(omw.FuelInven, nf.NMC),
					SubTotalNonCashExpMW = GlobalDB.dbo.WtAvg(omw.STNonCash, nf.NMC),
					GrandTotalExpMW = GlobalDB.dbo.WtAvg(omw.GrandTot, nf.NMC),
					PlantSiteWagesBenefitsMW = GlobalDB.dbo.WtAvg(plnt.SiteWagesBen, nf.NMC),
					PlantCentralEmployeesMW = GlobalDB.dbo.WtAvg(plnt.CentralContract, nf.NMC),
					PlantMaterialsMW = GlobalDB.dbo.WtAvg(plnt.MaintMatl, nf.NMC),
					PlantOverhaulsMW = GlobalDB.dbo.WtAvg(plnt.OverhaulAdj, nf.NMC),
					PlantLTSAPSAMW = GlobalDB.dbo.WtAvg(plnt.LTSAAdj, nf.NMC),
					PlantOtherVarMW = GlobalDB.dbo.WtAvg(plnt.OthVar, nf.NMC),
					PlantMiscMW = GlobalDB.dbo.WtAvg(plnt.Misc, nf.NMC),
					PlantManageExpLessFuelMW = GlobalDB.dbo.WtAvg(omw.ActCashLessFuel, nf.NMC),
					AncillaryRevenueMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.TotAncillaryRevenue,0))/SUM(nf.NMC) END),
					AshSalesMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.RevAsh,0))/SUM(nf.NMC) END),
					OtherChemicalSalesMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.RevOth,0))/SUM(nf.NMC) END),
					OtherRevenueMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.OthNonT7Revenue,0))/SUM(nf.NMC) END),
					TotalRevenueMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.TotRevenueRevised,0))/SUM(nf.NMC) END),
					AshOperationsCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(m.AshCostOP,0))/SUM(nf.NMC) END),
					AshDisposalCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(m.AshCostDisp,0))/SUM(nf.NMC) END),
					AshTotalCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(mc.TotAshHandling,0))/SUM(nf.NMC) END),
					ScrubberLimeCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrCostLime,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
					ScrubberOtherChemCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrCostChem,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
					ScrubberAuxCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrubberPowerCost,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
					ScrubberOtherNonMaintCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.ScrCostNMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
					ScrubberTotalCostMW = (CASE WHEN SUM(nf.NMC) <> 0 THEN SUM(ISNULL(sc.TotScrubberNonMaint,0))/SUM(case when sc.TotScrubberNonMaint IS NULL then NULL else nf.NMC end) END),
					ScrubbingCostTonMW = GlobalDB.dbo.WtAvgNZ(so2.ScrubbingIndex, so2.TonsSO2Removed)
				FROM TSort t 
					INNER JOIN OpExCalc omw ON omw.refnum = t.refnum AND omw.DataType = ''KW''
					INNER JOIN NercFactors nf ON nf.refnum = t.refnum
					INNER JOIN PlantActionExp plnt ON plnt.Refnum = t.Refnum and plnt.DataType = ''KW''
					INNER JOIN MiscCalc mc ON mc.Refnum = t.Refnum
					INNER JOIN MiscGroupedByRefnum m ON m.Refnum = t.Refnum 
					LEFT JOIN ScrubberCosts sc ON sc.refnum = t.refnum --and sc.TotScrubberNonMaint > 0
					LEFT JOIN SO2Removal so2 ON so2.Refnum = t.Refnum and so2.ScrubbingIndex > 0
					INNER JOIN Breaks b ON b.Refnum = t.Refnum
					LEFT JOIN StartsAnalysis st on st.Refnum = t.Refnum' + @Turbine +
				' WHERE ' + @Where)
			
		END






