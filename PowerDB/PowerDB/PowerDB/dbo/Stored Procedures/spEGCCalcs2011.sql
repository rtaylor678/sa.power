﻿-- =============================================
-- Author:		Steve West
-- Create date: 06/06/2012
-- Description:	Calculates EGC based ON Rick's new method
-- Based off spEGCCalcs2007, THEN modified to use the new calcs
-- =============================================
CREATE PROCEDURE [dbo].[spEGCCalcs2011] 
	@refnum varchar(12) = null,
	@year int = 0,
	@DEBUG bit = 0,
	@Manual bit = 0,
	@EGCTechnology varchar(15) = null, 
	@EGCRegion varchar(20) = null,
	@CTG_NDC real = null, 
	@NDC real = null, 
	@FuelType varchar(8) = null, 
	@CoalMBTU real = null,
	@CoalSulfur real = null, 
	@CoalAsh real = null,
	@OilSulfur real = null,
	@OilAsh real = null,
	@NumUnitsAtSite real = null, 
	@OilMBTU real = null,
	@GasMBTU real = null,
	@CoalNDC real = null,
	@OilNDC real = null,
	@GasNDC real = null,
	@CTGs int = null,
	@ScrubbersYN int = null, 
	@PrecBagYN int = null,
	@AdjNetMWH2Yr real = null,
	--@TotCashLessFuelEm real = null,
	@Age real = null,
	@ElecGenStarts real = null
AS
BEGIN
	SET NOCOUNT ON;
	SET @refnum = rtrim(@refnum)
	
	IF @year < 2011
		return

----------------------------------------------------------------------------
--Declare AND load all the coefficients 

	DECLARE @Coeff_SCR real, @Coeff_WGS real, @Coeff_DGS real, @Coeff_PrecBag real, @Coeff_OtherSiteCapCoalOil real, @Coeff_OtherSiteCapGas real, @Coeff_MeanRunTimePerMW real,
		@Coeff_SteamSalesCoalOil real, @Coeff_SteamSalesGas real, @Coeff_Coal_NDC real, @Coeff_Oil_NDC real, @Coeff_Gas_NDC real, @Coeff_Coal1 real, @Coeff_Coal2 real, @Coeff_Coal3 real, 
		@Coeff_Sludge real, @Coeff_CoalDust real, @Coeff_Landfill real, @Coeff_Woodgas real, @Coeff_NatGasMBTU real, @Coeff_OffGasMBTU real,
		@Coeff_H2GasMBTU real, @Coeff_JetFuel real, @Coeff_Diesel real, @Coeff_FuelOil real, 
		@Coeff_GasTotalNonGasUnits real, @Coeff_GasTotalGasUnits real, @Coeff_LiquidFuelsGasUnits real, @Coeff_LiquidFuelsNoSulfurCoal real, @Coeff_LiquidFuelsSulfurCoal real,
		@Pwr_SCR real, @Pwr_WGS real, @Pwr_DGS real, @Pwr_PrecBag real, @Pwr_OtherSiteCapCoalOil real, @Pwr_OtherSiteCapGas real, @Pwr_MeanRunTimePerMW real, 
		@Pwr_SteamSalesCoalOil real, @Pwr_SteamSalesGas real,
		@Pwr_Coal_NDC real, @Pwr_Oil_NDC real, @Pwr_Gas_NDC real, @Pwr_Coal1 real, @Pwr_Coal2 real, @Pwr_Coal3 real, @Pwr_Sludge real,
		@Pwr_CoalDust real, @Pwr_Landfill real, @Pwr_Woodgas real, @Pwr_NatGasMBTU real, @Pwr_OffGasMBTU real, @Pwr_H2GasMBTU real, 
		@Pwr_JetFuel real, @Pwr_Diesel real, @Pwr_FuelOil real,
		@Pwr_GasTotalNonGasUnits real, @Pwr_GasTotalGasUnits real, @Pwr_LiquidFuelsGasUnits real, @Pwr_LiquidFuelsNoSulfurCoal real, @Pwr_LiquidFuelsSulfurCoal real,
		@Coeff_CTG_Age real, @Coeff_CTG_NDC real, @Coeff_CTG_FiringTemp real, @Coeff_CTG_Starts real, @Coeff_CTG_AvgSizeUnder150 real, 
		@Coeff_CTG_AvgSizeOver150 real, @Coeff_CTG_SiteEffect real, @Coeff_CTG_SCRNDC real, @Coeff_CTG_SteamSales real, @Coeff_CTG_HRSGCapNDC real,
		@Coeff_CTG_HRSGCap real, @Coeff_CTG_GasMBTU real, @Coeff_CTG_LiquidMBTU real,
		@Pwr_CTG_Age real, @Pwr_CTG_NDC real, @Pwr_CTG_FiringTemp real, @Pwr_CTG_Starts real, @Pwr_CTG_AvgSizeUnder150 real, 
		@Pwr_CTG_AvgSizeOver150 real, @Pwr_CTG_SiteEffect real, @Pwr_CTG_SCRNDC real, @Pwr_CTG_SteamSales real, @Pwr_CTG_HRSGCapNDC real,
		@Pwr_CTG_HRSGCap real, @Pwr_CTG_GasMBTU real, @Pwr_CTG_LiquidMBTU real

	SELECT @Coeff_SCR = SCR, @Coeff_WGS = WGS, @Coeff_DGS = DGS, @Coeff_PrecBag = PrecBag, @Coeff_OtherSiteCapCoalOil = OtherSiteCapCoalOil, @Coeff_OtherSiteCapGas = OtherSiteCapGas,
		@Coeff_MeanRunTimePerMW = MeanRunTimePerMW,	@Coeff_SteamSalesCoalOil = SteamSalesCoalOil, @Coeff_SteamSalesGas = SteamSalesGas, @Coeff_Coal_NDC = Coal_NDC, @Coeff_Oil_NDC = Oil_NDC, 
		@Coeff_Gas_NDC = Gas_NDC, @Coeff_Coal1 = Coal1, @Coeff_Coal2 = Coal2, @Coeff_Coal3 = Coal3, @Coeff_Sludge = Sludge,
		@Coeff_CoalDust = CoalDust, @Coeff_Landfill = Landfill, @Coeff_Woodgas = Woodgas,  @Coeff_NatGasMBTU = NatGasMBTU, @Coeff_OffGasMBTU = OffGasMBTU, 
		@Coeff_H2GasMBTU = H2GasMBTU, @Coeff_JetFuel = JetFuel, @Coeff_Diesel = Diesel, @Coeff_FuelOil = FuelOil, 
		@Coeff_GasTotalNonGasUnits = GasTotalNonGasUnits, @Coeff_GasTotalGasUnits = GasTotalGasUnits, @Coeff_LiquidFuelsGasUnits = LiquidFuelsGasUnits, 
		@Coeff_LiquidFuelsNoSulfurCoal = LiquidFuelsNoSulfurCoal, @Coeff_LiquidFuelsSulfurCoal = LiquidFuelsSulfurCoal,
		@Pwr_SCR =Pwr_SCR, @Pwr_WGS = Pwr_WGS, @Pwr_DGS = Pwr_DGS, @Pwr_PrecBag =Pwr_PrecBag, @Pwr_OtherSiteCapCoalOil = Pwr_OtherSiteCapCoalOil, @Pwr_OtherSiteCapGas = Pwr_OtherSiteCapGas,
		@Pwr_MeanRunTimePerMW = Pwr_MeanRunTimePerMW, @Pwr_SteamSalesCoalOil = Pwr_SteamSalesCoalOil, @Pwr_SteamSalesGas = Pwr_SteamSalesGas, @Pwr_Coal_NDC =Pwr_Coal_NDC, @Pwr_Oil_NDC = Pwr_Oil_NDC, 
		@Pwr_Gas_NDC = Pwr_Gas_NDC, @Pwr_Coal1 = Pwr_Coal1, @Pwr_Coal2 = Pwr_Coal2, @Pwr_Coal3 = Pwr_Coal3, @Pwr_Sludge = Pwr_Sludge,
		@Pwr_CoalDust = Pwr_CoalDust, @Pwr_Landfill = Pwr_Landfill, @Pwr_Woodgas = Pwr_Woodgas, @Pwr_NatGasMBTU = Pwr_NatGasMBTU, 
		@Pwr_OffGasMBTU = Pwr_OffGasMBTU, @Pwr_H2GasMBTU = Pwr_H2GasMBTU, @Pwr_JetFuel = Pwr_JetFuel, @Pwr_Diesel = Pwr_Diesel, @Pwr_FuelOil = Pwr_FuelOil,
		@Pwr_GasTotalNonGasUnits = Pwr_GasTotalNonGasUnits, @Pwr_GasTotalGasUnits = Pwr_GasTotalGasUnits, @Pwr_LiquidFuelsGasUnits = Pwr_LiquidFuelsGasUnits, @Pwr_LiquidFuelsNoSulfurCoal = Pwr_LiquidFuelsNoSulfurCoal, @Pwr_LiquidFuelsSulfurCoal = Pwr_LiquidFuelsSulfurCoal,
		@Coeff_CTG_Age = CTG_Age, @Coeff_CTG_NDC = CTG_NDC, @Coeff_CTG_FiringTemp = CTG_FiringTemp, @Coeff_CTG_Starts = CTG_Starts, 
		@Coeff_CTG_AvgSizeUnder150 = CTG_AvgSizeUnder150, @Coeff_CTG_AvgSizeOver150 = CTG_AvgSizeOver150, @Coeff_CTG_SiteEffect = CTG_SiteEffect, 
		@Coeff_CTG_SCRNDC = CTG_SCRNDC, @Coeff_CTG_SteamSales = CTG_SteamSales, @Coeff_CTG_HRSGCapNDC = CTG_HRSGCapNDC, @Coeff_CTG_HRSGCap = CTG_HRSGCap, 
		@Coeff_CTG_GasMBTU = CTG_GasMBTU, @Coeff_CTG_LiquidMBTU = CTG_LiquidMBTU, @Pwr_CTG_Age = Pwr_CTG_Age, @Pwr_CTG_NDC = Pwr_CTG_NDC, 
		@Pwr_CTG_FiringTemp = Pwr_CTG_FiringTemp, @Pwr_CTG_Starts = Pwr_CTG_Starts, @Pwr_CTG_AvgSizeUnder150 = Pwr_CTG_AvgSizeUnder150, 
		@Pwr_CTG_AvgSizeOver150 = Pwr_CTG_AvgSizeOver150, @Pwr_CTG_SiteEffect = Pwr_CTG_SiteEffect, @Pwr_CTG_SCRNDC = Pwr_CTG_SCRNDC, 
		@Pwr_CTG_SteamSales = Pwr_CTG_SteamSales, @Pwr_CTG_HRSGCapNDC = Pwr_CTG_HRSGCapNDC, @Pwr_CTG_HRSGCap = Pwr_CTG_HRSGCap, 
		@Pwr_CTG_GasMBTU = Pwr_CTG_GasMBTU, @Pwr_CTG_LiquidMBTU = Pwr_CTG_LiquidMBTU
	
	FROM POWERGlobal.dbo.EGCCoefficients2011 
	WHERE Year = @year 

	----------------------------------------------------------------------------

	--then some calcs ON the coal to get the data we need
	--basically what this is doing is getting the tons of coal for each coal#, and removing the ash percentage, then summing it for the outputs
	CREATE TABLE #coaltmp (refnum char(12), coalID char(2), Name char(30) NULL, AshPcnt real NULL, Tons real NULL, TonsLessAsh real NULL, CoalType char(15) NULL)
	CREATE TABLE #coal (refnum char(12), CoalType char(15), coalID char(2), Tons real NULL, TonsLessAsh real NULL, AshTons real NULL)

	INSERT #coaltmp  SELECT Refnum, CoalID, Name, AshPcnt, Tons, Tons * ((100 - isnull(ashpcnt,0))/100), '' from Coal where Refnum = @refnum 

	--update the types based on the name
	UPDATE #coaltmp SET CoalType = 'Biomass' WHERE Name LIKE '%bio%' OR Name LIKE '%meal%'
	UPDATE #coaltmp SET CoalType = 'Coal Dust' WHERE Name LIKE '%dust%'
	UPDATE #coaltmp SET CoalType = 'Petcoke' WHERE Name LIKE '%petcoke%'
	UPDATE #coaltmp SET CoalType = 'Sludge' WHERE Name LIKE '%sludge%'
	UPDATE #coaltmp SET CoalType = 'Woodgas' WHERE Name LIKE '%woodgass%' OR Name LIKE '%wood gass%'
	UPDATE #coaltmp SET CoalType = 'Tires' WHERE Name LIKE '%tires%'
	UPDATE #coaltmp SET CoalType = 'Coal Gassified' WHERE Name LIKE '%coal gass%'
	UPDATE #coaltmp SET CoalType = 'Landfill' WHERE Name LIKE '%sklad%'
	UPDATE #coaltmp SET CoalType = 'Coal' WHERE CoalType = ''
	
	INSERT #coal SELECT refnum, coaltype, coalid, SUM(Tons), SUM(tonslessash), AshTons = SUM(Tons - TonsLessAsh) FROM #coaltmp GROUP BY refnum, CoalType, coalID 

	DROP TABLE #coaltmp

	
	


	----------------------------------------------------------------------------

	--now we create a temporary table to hold all the calculations, THEN load it

	CREATE TABLE #tmp (refnum char(12), EGCTot real NULL, SCR real NULL, WGS real NULL, DGS real NULL, PrecBag real NULL, OtherSiteCapCoalOil real NULL, 
		OtherSiteCapGas real NULL, MeanRunTimePerMW real NULL, SteamSalesCoalOil real NULL, SteamSalesGas real NULL,
		Coal_NDC real NULL, Oil_NDC real NULL, Gas_NDC real NULL, Coal1 real NULL, Coal2 real NULL, Coal3 real NULL, Sludge real NULL, CoalDust real NULL, Landfill real NULL, Woodgas real NULL, 
		NatGasMBTU real NULL, OffGasMBTU real NULL, H2GasMBTU real NULL, JetFuel real NULL, Diesel real NULL, FuelOil real NULL, 
		GasTotalNonGasUnits real NULL, GasTotalGasUnits real NULL, LiquidFuelsGasUnits real NULL, LiquidFuelsNoSulfurCoal real NULL, LiquidFuelsSulfurCoal real NULL,
		CTG_Age real NULL, CTG_NDC real NULL, CTG_FiringTemp real NULL, 
		CTG_Starts real NULL, CTG_AvgSizeUnder150 real NULL, CTG_AvgSizeOver150 real NULL, CTG_SiteEffect real NULL, CTG_SCRNDC real NULL, CTG_SteamSales real NULL, CTG_HRSGCapNDC real NULL, 
		CTG_HRSGCap real NULL, CTG_GasMBTU real NULL, CTG_LiquidMBTU real NULL,
		Calc_SCR real NULL, Calc_NDC real NULL, Calc_WGS real NULL, Calc_DGS real NULL, Calc_Ash real NULL, Calc_SiteTot real NULL, Calc_NumUnitsAtSite real NULL, Calc_NDCDiff real NULL,
		Calc_OtherNDCs real NULL, Calc_MeanRunTimeMW real NULL, Calc_SteamSales real NULL, Calc_Coal_NDC real NULL, Calc_Oil_NDC real NULL, Calc_Gas_NDC real NULL, Calc_CTG_NDC real NULL,
		FiringTemp real NULL, AdjNetMWH real NULL, AdjNetMWH2Yr real NULL, NDC real NULL, CTGCount real NULL, Starts real NULL, AvgTotStarts real NULL, NumUnitsAtSite real NULL, SiteNDC real NULL,
		SCRExists real NULL, StmSales real NULL, DieselKGal real NULL, FuelOilKGal real NULL, JetTurbKGal real NULL, Age real NULL,
		Calc_Age real NULL, Calc_CTGNDC real NULL, Calc_CTG_Starts real NULL, Calc_AvgCTGSizeUnder150 real NULL, Calc_AvgCTGSizeOver150 real NULL, Calc_SiteEffect real NULL,
		Calc_SCRNDC real NULL, Calc_HRSGCapN real NULL, Calc_HRSGCap real NULL, Calc_GasMBTU real NULL, Calc_LiquidMBTU real NULL,
		FuelType varchar(8) NULL
		)



	--and put the refnum into the table
	INSERT #tmp (refnum) SELECT refnum FROM TSort WHERE StudyYear >= 2011 AND Refnum = @refnum 
	--UPDATE #tmp SET UtilityUnitCode = NERCTurbine.UtilityUnitCode FROM NERCTurbine WHERE NERCTurbine.Refnum = #tmp.refnum 






	----------------------------------------------------------------------------

	--now do the coal units
	IF @refnum NOT LIKE '%CC%'
	BEGIN

		--fill in some initial data that will be used for calcs
		UPDATE #tmp SET --Calc_ServiceHours = b.ServiceHrs, 
			Calc_SCR = b.scr, Calc_NDC = b.NDC, Calc_WGS = 0.001 * WGSExists * ISNULL(SulfurTons,0), Calc_DGS = 0.001 * DGSExists * ISNULL(SulfurTons,0),
			Calc_Ash = b.AshTons, Calc_SiteTot = b.SiteNDC, Calc_NumUnitsAtSite = b.NumUnitsAtSite, Calc_NDCDiff = b.SiteNDC - b.NDC,--CASE WHEN b.SiteNDC > b.NDC THEN b.SiteNDC ELSE b.NDC END, -- - b.OtherNDCs,
			Calc_OtherNDCs = 0, --b.OtherNDCs, --note OtherNDCs were dropped 8/21/12 SW
			Calc_MeanRunTimeMW = 1/(b.ServiceHrs/CASE ISNULL(TotStarts,0) WHEN 0 THEN 1 ELSE TotStarts END/b.NDC), Calc_SteamSales = b.StmSalesMWH,
			Calc_Coal_NDC = CASE b.FuelType WHEN 'Gas' THEN 0 ELSE b.NDC END, --ISNULL(b.Coal_NDC,0), 
			Calc_Oil_NDC = 0, -- ISNULL(b.Oil_NDC,0), 
			Calc_Gas_NDC = CASE b.FuelType WHEN 'Gas' THEN b.NDC ELSE 0 END, -- ISNULL(b.Gas_NDC,0), 
			Calc_CTG_NDC = ISNULL(b.CTG_NDC,0),
			AdjNetMWH2Yr = b.AdjNetMWH2Yr, FuelType = b.FuelType
			
			FROM (
			SELECT t.Refnum, ServiceHrs = case ISNULL(nf.ServiceHrs,0) WHEN 0 THEN 8760 ELSE nf.ServiceHrs END, gtc.TotStarts, gtc.StmSalesMWH, t.coal_NDC, t.Oil_NDC, t.Gas_NDC, t.CTG_NDC,
---NEED TO FIX THE nf.ServiceHrs - should not be putting 8760 in! This was just for testing 7/13/12 SW
				SCR = ISNULL(scrcalc.SCR,0), nf.NDC, SulfurTons = coalsulfur.SulfurTons
				--, WGS = CASE ISNULL(wgnonoh.annnonohcost,0) + ISNULL(
				,WGSExists = CASE WHEN (ISNULL(wgnonoh.AnnNonOHCost,0) + ISNULL(wgoh.totannohcost,0)) > 0 THEN 1 ELSE 0 END
				,DGSExists = CASE WHEN (ISNULL(dgnonoh.AnnNonOHCost,0) + ISNULL(dgoh.totannohcost,0)) > 0 THEN 1 ELSE 0 END
				,AshTons = coalash.AshTons
				,SiteNDC = SiteTot.SiteNDC
				,t.NumUnitsAtSite
				--,OtherNDCs = ISNULL(Coal_NDC,0) + ISNULL(Oil_NDC,0) + ISNULL(Gas_NDC,0) + ISNULL(CTG_NDC,0)
				,gtc.AdjNetMWH2Yr
				,t.FuelType
				 FROM TSort t
				LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
				LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
				LEFT JOIN (select distinct Refnum, SCR = 1 from (
					SELECT distinct Refnum FROM [PowerWork].[dbo].[NonOHMaint] where Component = 'SCR'
					union select distinct Refnum FROM [PowerWork].[dbo].[OHMaint] where Component = 'SCR') c) scrcalc on scrcalc.Refnum = t.Refnum
				left join (select refnum, AnnNonOHCost = sum(ISNULL(CurrCptlLocal,0) + ISNULL(CurrMMOCostLocal,0) + ISNULL(CurrExpLocal,0)) from NonOHMaint where Component = 'WGS' GROUP BY Refnum) wgnonoh on wgnonoh.Refnum = t.Refnum 
				left join (select refnum, TotAnnOHCost = sum(ISNULL(directcostlocal,0) + ISNULL(ammocostlocal,0) + ISNULL(lumpsumcostlocal,0)) from OHMaint where Component = 'WGS' group by Refnum ) wgoh on wgoh.Refnum = t.Refnum 
				left join (select refnum, AnnNonOHCost = sum(ISNULL(CurrCptlLocal,0) + ISNULL(CurrMMOCostLocal,0) + ISNULL(CurrExpLocal,0)) from NonOHMaint where Component = 'DGS' GROUP BY Refnum) dgnonoh on dgnonoh.Refnum = t.Refnum 
				left join (select refnum, TotAnnOHCost = sum(ISNULL(directcostlocal,0) + ISNULL(ammocostlocal,0) + ISNULL(lumpsumcostlocal,0)) from OHMaint where Component = 'DGS' group by Refnum ) dgoh on dgoh.Refnum = t.Refnum 
				left join (select refnum, SulfurTons = sum(ISNULL(Tons,0) * ISNULL(SulfurPcnt,0)) /100 from Coal group by Refnum) coalsulfur ON coalsulfur.Refnum = t.Refnum
				left join (select refnum, AshTons = sum(Ashtons)  from #Coal where CoalType = 'Coal' group by Refnum) coalash ON coalash.Refnum = t.Refnum
				left join (select Refnum = @refnum, SiteNDC = SUM(ndc) from NERCFactors nf where Refnum in (select Refnum from TSort where SiteID in (select SiteID from TSort where Refnum = @refnum))) SiteTot ON SiteTot.refnum = T.Refnum
				
				 ) b where b.Refnum = #tmp.refnum
		
			
		--UPDATE #tmp set SCR = Calc_SCR * (1000/Calc_NDC) * @Pwr_SCR WHERE ISNULL(Calc_NDC ,0) > 0
		UPDATE #tmp set SCR = Calc_SCR * (AdjNetMWH2Yr/1000000) * @Pwr_SCR --WHERE ISNULL(Calc_NDC ,0) > 0
		
		UPDATE #tmp SET WGS = POWER(Calc_WGS, @Pwr_WGS)
		
		UPDATE #tmp SET DGS = POWER(Calc_DGS, @Pwr_DGS)
		
		UPDATE #tmp SET PrecBag = POWER(Calc_Ash, @Pwr_PrecBag)

		--8/21/12 SW Changed OtherSiteCap to CoalOil and Gas versions, adjusted calculations
		UPDATE #tmp SET OtherSiteCapCoalOil = CASE FuelType WHEN 'Gas' THEN 0 ELSE CASE Calc_NumUnitsAtSite WHEN 1 THEN 0 ELSE 
			91.6442499651945 * POWER((cast(Calc_NDCDiff as real)/(Calc_NumUnitsAtSite-1)),0.610759682791099) - 382.799775241381 * POWER(cast(Calc_NDCDiff as real), 0.333) - 495.435453279966 * POWER((Calc_SiteTot/Calc_NDC),0.333) END END

		UPDATE #tmp SET OtherSiteCapGas = CASE FuelType WHEN 'Gas' THEN CASE Calc_NumUnitsAtSite WHEN 1 THEN 0 ELSE 
			91.6442499651945 * POWER((cast(Calc_NDCDiff as real)/(Calc_NumUnitsAtSite-1)),0.610759682791099) - 382.799775241381 * POWER(cast(Calc_NDCDiff as real), 0.333) - 495.435453279966 * POWER((Calc_SiteTot/Calc_NDC),0.333) END ELSE 0 END
		
		--8/10/12 SW MeanRunTimePerMW removed from model
		--UPDATE #tmp SET MeanRunTimePerMW = POWER(Calc_MeanRunTimeMW, @Pwr_MeanRunTimePerMW)
		
		UPDATE #tmp SET SteamSalesCoalOil = CASE FuelType WHEN 'Gas' THEN 0 ELSE POWER(Calc_SteamSales/1000000, @Pwr_SteamSalesCoalOil) END

		UPDATE #tmp SET SteamSalesGas = CASE FuelType WHEN 'Gas' THEN POWER(Calc_SteamSales/1000000, @Pwr_SteamSalesGas) ELSE 0 END
				
		UPDATE #tmp SET Coal_NDC = POWER(Calc_Coal_NDC, @Pwr_Coal_NDC)
		
		UPDATE #tmp SET Oil_NDC = POWER(Calc_Oil_NDC, @Pwr_Oil_NDC)
		
		UPDATE #tmp SET Gas_NDC = POWER(Calc_Gas_NDC, @Pwr_Gas_NDC)
		
		--8/10/12 SW CTG NDC removed from model
		--UPDATE #tmp SET CTG_NDC = POWER(Calc_CTG_NDC, 0.333)--@Pwr_CTG_NDC) this needs to be fixed in the table

		--8/21/12 SW Coal1 removed from model
		--UPDATE #tmp SET Coal1 = TonsLessAsh/1000
		--FROM #coal
		--WHERE CoalType = 'Coal' and coalID like '%1'
		
		UPDATE #tmp SET Coal2 = TonsLessAsh/1000
		FROM #coal
		WHERE CoalType = 'Coal' and coalID like '%2'

		--8/10/12 SW Coal3 removed from model
		--8/21/12 SW Coal3 added back to model - we're just guessing here, aren't we?
		UPDATE #tmp SET Coal3 = TonsLessAsh/1000
		FROM #coal
		WHERE CoalType = 'Coal' and coalID like '%3'

		--then flip anyone that is NULL to the end
		--UPDATE #tmp SET Coal1 = Coal2, Coal2 = null WHERE Coal1 is null AND Coal2 is not null
		--UPDATE #tmp SET Coal2 = Coal3, Coal3 = null WHERE Coal2 is null AND Coal3 is not null
		--UPDATE #tmp SET Coal1 = Coal2, Coal2 = null WHERE Coal1 is null AND Coal2 is not null

		----and order them so that the most tons are in position 1 AND the least in position 3 (important later because the coeffs are different)
		--UPDATE #tmp SET Coal1 = Coal2, Coal2 = Coal1 WHERE Coal2 > Coal1 
		--UPDATE #tmp SET Coal3 = Coal2, Coal2 = Coal3 WHERE Coal3 > Coal2 
		--UPDATE #tmp SET Coal1 = Coal2, Coal2 = Coal1 WHERE Coal2 > Coal1 
		
		--now they've been ordered we can do the Powers
		--UPDATE #tmp SET Coal1 = Power(Coal1, @Pwr_Coal1)
		UPDATE #tmp SET Coal2 = Power(Coal2, @Pwr_Coal2)
		UPDATE #tmp SET Coal3 = Power(Coal3, @Pwr_Coal3)
		
		--8/21/12 SW solid fuels combined then removed (!) from model 
		--UPDATE #tmp SET Sludge = b.Sludge FROM 
		--	(SELECT Sludge = SUM(TonsLessAsh/1000) FROM #coal WHERE CoalType = 'Sludge') b

		--UPDATE #tmp SET CoalDust = b.CoalDust FROM 
		--	(SELECT CoalDust = POWER(SUM(TonsLessAsh/1000), @Pwr_CoalDust) FROM #coal WHERE CoalType = 'Coal Dust') b

		--UPDATE #tmp SET Landfill = b.Landfill FROM 
		--	(SELECT Landfill = POWER(SUM(TonsLessAsh/1000), @Pwr_Landfill) FROM #coal WHERE CoalType = 'Landfill') b

		--UPDATE #tmp SET Woodgas = b.Woodgas FROM 
		--	(SELECT Woodgas = POWER(SUM(TonsLessAsh/1000), @Pwr_Woodgas) FROM #coal WHERE CoalType = 'Woodgas') b

		--UPDATE #tmp SET NatGasMBTU = b.NatGas, OffGasMBTU = b.OffGas, H2GasMBTU = b.H2Gas, 
		----8/10/12 SW Jet and FOil removed from model
		--8/21/12 SW all other Gas and Liquid removed from model
		--	--JetFuel = b.JetFuel, 
		--	Diesel = b.Diesel--, FuelOil = b.FuelOil 
		--	FROM (
		--	SELECT refnum, 
		--		NatGas = POWER(SUM(CASE WHEN FuelType = 'NatGas' THEN MBTU ELSE 0 END)/1000, @Pwr_NatGasMBTU),
		--		OffGas = POWER(SUM(CASE WHEN FuelType = 'OffGas' THEN MBTU ELSE 0 END)/1000, @Pwr_OffGasMBTU),
		--		H2Gas = POWER(SUM(CASE WHEN FuelType = 'H2Gas' THEN MBTU ELSE 0 END)/1000, @Pwr_H2GasMBTU),
		--		--JetFuel = POWER(SUM(CASE WHEN FuelType = 'Jet' THEN KGal ELSE 0 END), @Pwr_JetFuel),
		--		Diesel = POWER(SUM(CASE WHEN FuelType = 'Diesel' THEN KGal ELSE 0 END), @Pwr_Diesel)
		--		--FuelOil = POWER(SUM(CASE WHEN FuelType = 'FOil' THEN KGal ELSE 0 END), @Pwr_FuelOil)
		--	FROM Fuel
		--	GROUP BY Refnum--, FuelType 
		--	) b
		--WHERE b.Refnum = #tmp.refnum 

		--8/21/12 SW gas and liquid treated differently...
		UPDATE #tmp SET GasTotalNonGasUnits = CASE FuelType WHEN 'Gas' THEN 0 ELSE POWER(b.MBTU/1000, @Pwr_GasTotalNonGasUnits) END
					FROM (
			SELECT refnum, MBTU = SUM(CASE WHEN FuelType = 'NatGas' THEN MBTU 
				WHEN FuelType = 'OffGas' THEN MBTU 
				WHEN FuelType = 'H2Gas' THEN MBTU ELSE 0 END)
				FROM Fuel
				GROUP BY Refnum--, FuelType 
				) b WHERE b.Refnum = #tmp.refnum 
		
		UPDATE #tmp SET GasTotalGasUnits = CASE FuelType WHEN 'Gas' THEN POWER(b.MBTU/1000, @Pwr_GasTotalGasUnits) ELSE 0 END
					FROM (
			SELECT refnum, MBTU = SUM(CASE WHEN FuelType = 'NatGas' THEN MBTU 
				WHEN FuelType = 'OffGas' THEN MBTU 
				WHEN FuelType = 'H2Gas' THEN MBTU ELSE 0 END)
				FROM Fuel
				GROUP BY Refnum--, FuelType 
				) b WHERE b.Refnum = #tmp.refnum 
		

		UPDATE #tmp SET LiquidFuelsGasUnits = CASE FuelType WHEN 'Gas' THEN POWER(b.KGal, @Pwr_LiquidFuelsGasUnits) ELSE 0 END
					FROM (
			SELECT refnum, KGal = SUM(CASE WHEN FuelType = 'Jet' THEN KGal 
				WHEN FuelType = 'Diesel' THEN KGal 
				WHEN FuelType = 'FOil' THEN KGal ELSE 0 END)
				FROM Fuel
				GROUP BY Refnum--, FuelType 
				) b WHERE b.Refnum = #tmp.refnum 

		UPDATE #tmp SET LiquidFuelsNoSulfurCoal = CASE FuelType WHEN 'Gas' THEN 0 ELSE POWER(b.KGal, @Pwr_LiquidFuelsNoSulfurCoal) END
					FROM (
			SELECT f.refnum, KGal = SUM(CASE WHEN f.FuelType = 'Jet' THEN KGal * 0.9985
				WHEN f.FuelType = 'Diesel' THEN KGal * 0.9965
				WHEN f.FuelType = 'FOil' THEN KGal * ((100 - fp.Sulfur)/100)  ELSE 0 END)
				FROM Fuel f
				LEFT JOIN FuelProperties fp on fp.Refnum = f.Refnum
				GROUP BY f.Refnum--, FuelType 
				) b WHERE b.Refnum = #tmp.refnum 

		UPDATE #tmp SET LiquidFuelsSulfurCoal = 0

	--now the math to calculate egctotal, which is simply multiplying each item by its coefficient
	UPDATE #tmp SET EGCTot = 0
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((SCR * @Coeff_SCR),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((WGS * @Coeff_WGS),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((DGS * @Coeff_DGS),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((PrecBag * @Coeff_PrecBag),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((OtherSiteCapCoalOil * @Coeff_OtherSiteCapCoalOil),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((OtherSiteCapGas * @Coeff_OtherSiteCapGas),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((MeanRunTimePerMW * @Coeff_MeanRunTimePerMW),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((SteamSalesCoalOil * @Coeff_SteamSalesCoalOil),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((SteamSalesGas * @Coeff_SteamSalesGas),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Coal_NDC * @Coeff_Coal_NDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Oil_NDC * @Coeff_Oil_NDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Gas_NDC * @Coeff_Gas_NDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((CTG_NDC * @Coeff_CTG_NDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Coal1 * @Coeff_Coal1),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Coal2 * @Coeff_Coal2),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Coal3 * @Coeff_Coal3),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Sludge * @Coeff_Sludge),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((CoalDust * @Coeff_CoalDust),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Landfill * @Coeff_Landfill),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Woodgas * @Coeff_Woodgas),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((NatGasMBTU * @Coeff_NatGasMBTU),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((OffGasMBTU * @Coeff_OffGasMBTU),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((H2GasMBTU * @Coeff_H2GasMBTU),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((JetFuel * @Coeff_JetFuel),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Diesel * @Coeff_Diesel),0)
	--UPDATE #tmp SET EGCTot = EGCTot + ISNULL((FuelOil * @Coeff_FuelOil),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((GasTotalNonGasUnits * @Coeff_GasTotalNonGasUnits),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((GasTotalGasUnits * @Coeff_GasTotalGasUnits),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((LiquidFuelsGasUnits * @Coeff_LiquidFuelsGasUnits),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((LiquidFuelsNoSulfurCoal * @Coeff_LiquidFuelsNoSulfurCoal),0)
	
	
	--end of PN
	END

	----------------------------------------------------------------------------

	--now the CC units
	IF @refnum LIKE '%CC%'
	BEGIN


		--@Coeff_CTG_Age real, @Coeff_CTG_NDC real, @Coeff_CTG_FiringTemp real, @Coeff_CTG_Starts real, @Coeff_CTG_AvgSizeUnder150 real, 
		--@Coeff_CTG_AvgSizeOver150 real, @Coeff_CTG_SiteEffect real, @Coeff_CTG_SCRNDC real, @Coeff_CTG_SteamSales real, @Coeff_CTG_HRSGCapNDC real,
		--@Coeff_CTG_HRSGCap real, @Coeff_CTG_GasMBTU real, @Coeff_CTG_LiquidMBTU real,
		--@Pwr_CTG_Age real, @Pwr_CTG_NDC real, @Pwr_CTG_FiringTemp real, @Pwr_CTG_Starts real, @Pwr_CTG_AvgSizeUnder150 real, 
		--@Pwr_CTG_AvgSizeOver150 real, @Pwr_CTG_SiteEffect real, @Pwr_CTG_SCRNDC real, @Pwr_CTG_SteamSales real, @Pwr_CTG_HRSGCapNDC real,
		--@Pwr_CTG_HRSGCap real, @Pwr_CTG_GasMBTU real, @Pwr_CTG_LiquidMBTU real


	--fill in some initial data that will be used for calcs
	UPDATE #tmp SET FiringTemp = c.firingtemp, AdjNetMWH = c.AdjNetMWH, AdjNetMWH2Yr = c.AdjNetMWH2Yr, CTG_NDC = c.CTGNDC, NDC = c.NDC,
		CTGCount = c.CTGCount, Starts = c.Starts, AvgTotStarts = c.AvgTotStarts, NumUnitsAtSite = c.NumUnitsAtSite, SiteNDC = c.SiteNDC, 
		SCRExists = c.SCR, StmSales = c.StmSalesMWH, NatGasMBTU = c.NatGasMBTU, OffGasMBTU = c.OffGasMBTU, H2GasMBTU = c.H2GasMBTU, 
		DieselKGal = c.DieselKGal, FuelOilKGal = c.FuelOilKGal, JetTurbKGal = c.JetTurbKGal, Age = c.Age
	FROM (
		SELECT t.Refnum, ctgd.firingtemp, gtc.AdjNetMWH, gtc.AdjNetMWH2Yr, CTGNDC = ISNULL(nt.CTGNDC, ISNULL(t.CTG_NDC,0)), nercf.NDC, CTGCount = ISNULL(nt.CTGCount, ISNULL(t.CTGs,0)),
			Starts = gtc.TotStarts, ctgm.AvgTotStarts, NumUnitsAtSite, b.SiteNDC, SCR = ISNULL(scrcalc.SCR,0), gtc.StmSalesMWH, ftc.NatGasMBTU, 
			ftc.OffGasMBTU, ftc.H2GasMBTU, ftc.DieselKGal, ftc.FuelOilKGal, ftc.JetTurbKGal, nercf.Age
		FROM TSort t
			left join (select siteid, SiteNDC = SUM(nf.ndc) from TSort t left join NERCFactors nf on nf.Refnum = t.Refnum group by SiteID) b on b.SiteID = t.SiteID
			left join GenerationTotCalc gtc on gtc.Refnum = t.Refnum 
			left join (select refnum, firingtemp = MAX(firingtemp) from CTGData group by Refnum ) ctgd on ctgd.Refnum = t.Refnum 
			left join (select refnum, CTGNDC = SUM(ndc), CTGCount = COUNT(*) from NERCTurbine where turbinetype = 'CTG' group by Refnum) nt on nt.Refnum = t.Refnum 
			left join NERCFactors nercf on nercf.Refnum = t.Refnum 
			left join CTGMaintPerStart ctgm on ctgm.Refnum = t.Refnum 
			left join (select distinct Refnum, SCR = 1 from (
				SELECT distinct Refnum FROM [PowerWork].[dbo].[NonOHMaint] where Component = 'CTG-SCR'
					union select distinct Refnum FROM [PowerWork].[dbo].[Misc] where SCR = 'Y'
					union select distinct Refnum FROM [PowerWork].[dbo].[OHMaint] where Component = 'SCR'
					) b) scrcalc on scrcalc.Refnum = t.Refnum 
			left join FuelTotCalc ftc on ftc.Refnum = t.Refnum) c
	WHERE c.Refnum = #tmp.refnum

		--Age(CTG NDC/"yr")
		UPDATE #tmp SET Calc_Age = POWER((CTG_NDC / FLOOR(Age/5)), @Pwr_CTG_Age) WHERE FLOOR(Age/5) > 0

		--CTG NDC / Unit NDC
		UPDATE #tmp SET Calc_CTGNDC = POWER((CTG_NDC/NDC), @Pwr_CTG_NDC)


		--Firing Temp*CTG MW
		UPDATE #tmp SET FiringTemp = POWER(((firingTemp*CTG_NDC)/1000), @Pwr_CTG_FiringTemp)
	
		--Ln(CTG starts)*CTG NDC
		--updated 8/10/12 SW - removed from model
		--UPDATE #tmp SET Calc_CTG_Starts = POWER(LOG(CASE WHEN AvgTotStarts > 1 THEN AvgTotStarts ELSE 1 END) * NDC, @Pwr_CTG_Starts)
		
		--Avg CTG Size <=150 NDC
		UPDATE #tmp SET Calc_AvgCTGSizeUnder150 = POWER(NDC, @Pwr_CTG_AvgSizeUnder150) WHERE CTG_NDC / CTGCount <= 150
		
		--Avg CTG Size>150 NDC -different coefficients for the under and over
		UPDATE #tmp SET Calc_AvgCTGSizeOver150 = POWER(NDC, @Pwr_CTG_AvgSizeOver150) WHERE CTG_NDC / CTGCount > 150
		
		--Site Effect
		UPDATE #tmp SET Calc_SiteEffect =  POWER((0.0074885512878783 * POWER(SiteNDC/NumUnitsAtSite, 2) + POWER(NDC, 1.10029714213866)), @Pwr_CTG_SiteEffect) --beware magic numbers from the spreadsheet
		
		--SCR Mwh/CTG NDC
		UPDATE #tmp SET Calc_SCRNDC = SCRExists * POWER(AdjNetMWH/CTG_NDC, @Pwr_CTG_SCRNDC)

		--Steam Sales (1k lbs)
		UPDATE #tmp SET Calc_SteamSales = POWER(StmSales, @Pwr_CTG_SteamSales) WHERE StmSales >= 1
		
		--HRSG Cap/N
		UPDATE #tmp SET Calc_HRSGCapN = POWER((NDC - CTG_NDC)/CTGCount, @Pwr_CTG_HRSGCapNDC)
		
		--HRSG Cap
		UPDATE #tmp SET Calc_HRSGCap = POWER((NDC - CTG_NDC), @Pwr_CTG_HRSGCap)

		--Gas MBTU
		UPDATE #tmp SET Calc_GasMBTU = POWER(ISNULL(NatGasMBTU,0) + ISNULL(OffGasMBTU,0) + ISNULL(H2GasMBTU,0) , @Pwr_CTG_GasMBTU)

		--Liquid MBTU
		UPDATE #tmp SET Calc_LiquidMBTU = POWER(ISNULL(DieselKGal,0) + ISNULL(FuelOilKGal,0) + ISNULL(JetTurbKGal,0) , @Pwr_CTG_LiquidMBTU)


	--now the math to calculate egctotal, which is simply multiplying each item by its coefficient
	UPDATE #tmp SET EGCTot = 0
	--and CCs
		--	@Coeff_CTG_Age real, @Coeff_CTG_NDC real, @Coeff_CTG_FiringTemp real, @Coeff_CTG_Starts real, @Coeff_CTG_AvgSizeUnder150 real, 
		--@Coeff_CTG_AvgSizeOver150 real, @Coeff_CTG_SiteEffect real, @Coeff_CTG_SCRNDC real, @Coeff_CTG_SteamSales real, @Coeff_CTG_HRSGCapNDC real,
		--@Coeff_CTG_HRSGCap real, @Coeff_CTG_GasMBTU real, @Coeff_CTG_LiquidMBTU real,

	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_Age * @Coeff_CTG_Age),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_CTGNDC * @Coeff_CTG_NDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((FiringTemp * @Coeff_CTG_FiringTemp),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_CTG_Starts * @Coeff_CTG_Starts),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_AvgCTGSizeUnder150 * @Coeff_CTG_AvgSizeUnder150),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_AvgCTGSizeOver150 * @Coeff_CTG_AvgSizeOver150),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_SiteEffect * @Coeff_CTG_SiteEffect),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_SCRNDC * @Coeff_CTG_SCRNDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_SteamSales * @Coeff_CTG_SteamSales),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_HRSGCapN * @Coeff_CTG_HRSGCapNDC),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_HRSGCap * @Coeff_CTG_HRSGCap),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_GasMBTU * @Coeff_CTG_GasMBTU),0)
	UPDATE #tmp SET EGCTot = EGCTot + ISNULL((Calc_LiquidMBTU * @Coeff_CTG_LiquidMBTU),0)
	

------------------------------	


		--UPDATE #tmp SET FiringTemp1 = ft FROM (
		--	SELECT b.Refnum, ft = POWER(gtc.AdjNetMWH/1000000, @Pwr_FiringTemp1)  FROM (
		--		SELECT Refnum, ft = MAX(FiringTemp) 
		--		FROM CTGData 
		--		WHERE refnum LIKE '%CC[0-9][0-9]' 
		--		GROUP BY Refnum) b
		--	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = b.Refnum 
		--	WHERE b.ft < 1600) c
		--WHERE c.Refnum = #tmp.refnum 

		--UPDATE #tmp SET FiringTemp2 = ft FROM (
		--	SELECT b.Refnum, ft = POWER(gtc.AdjNetMWH/1000000, @Pwr_FiringTemp2)  FROM (
		--		SELECT Refnum, ft = MAX(FiringTemp) 
		--		FROM CTGData 
		--		WHERE refnum LIKE '%CC[0-9][0-9]' 
		--		GROUP BY Refnum) b
		--	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = b.Refnum 
		--	WHERE b.ft >= 1600 AND b.ft < 2100) c
		--WHERE c.Refnum = #tmp.refnum 


		--UPDATE #tmp SET CTGSize1 = Power(t.CTG_NDC/CTGs,@Pwr_CTGSize1)
		--	FROM TSort t 
		--	WHERE t.Refnum = @refnum AND ISNULL(ctgs,0) > 0 AND ISNULL(t.ctg_ndc,0) > 0 AND t.CTG_NDC/CTGs <= 200

		--UPDATE #tmp SET CTGSize2 = Power(t.CTG_NDC/CTGs,@Pwr_CTGSize2)
		--	FROM TSort t 
		--	WHERE t.Refnum = @refnum AND ISNULL(ctgs,0) > 0 AND ISNULL(t.ctg_ndc,0) > 0 AND t.CTG_NDC/CTGs > 200

		--UPDATE #tmp SET NatGasMBTU =  mbtu FROM (
		--SELECT Refnum, mbtu = POWER(LOG(Sum([MBTU])), @Pwr_NatGasMBTU)
		--  FROM Fuel
		--WHERE refnum = @refnum 
		--and FuelType  = 'NatGas'
		--  GROUP BY Refnum, FuelType ) b
		--  WHERE b.Refnum = #tmp.refnum 

		--UPDATE #tmp SET OffGasMBTU =  mbtu FROM (
		--SELECT Refnum, mbtu = POWER(LOG(Sum([MBTU])), @Pwr_OffGasMBTU)
		--  FROM Fuel
		--WHERE refnum = @refnum 
		--and FuelType  = 'OffGas'
		--  GROUP BY Refnum, FuelType ) b
		--  WHERE b.Refnum = #tmp.refnum 

		--UPDATE #tmp SET MWUnitSite = POWER(ndc/ISNULL(numunitsatsite,1), @Pwr_MWUnitSite) 
		--	FROM TSort t
		--	WHERE t.refnum = @refnum AND ISNULL(ndc,0) > 0 AND ISNULL(numunitsatsite,1) > 0

		--UPDATE #tmp SET WaterInjection = b.CTG_NDC FROM (
		--SELECT Refnum, CTG_NDC FROM TSort WHERE Refnum in (
		--SELECT distinct Refnum FROM Misc 
		--WHERE refnum = @refnum 
		--and WaterInjection = 'Y') AND CTG_NDC is not null ) b 
		--WHERE b.Refnum = #tmp.refnum 

		--UPDATE #tmp SET HRSGCapPerCTG = POWER(((NDC - t.CTG_NDC)/CTGs), @Pwr_HRSGCapPerCTG), 
		--	HRSGCap = (NDC - t.CTG_NDC)
		--	FROM TSort t
		--	WHERE t.refnum =@refnum AND ISNULL(ctgs,0) > 0 AND ISNULL(ndc,0) - ISNULL(t.ctg_ndc, 0) > 0

		--UPDATE #tmp SET StartsPerMW = b.NumStarts FROM (
		--SELECT gtc.Refnum, NumStarts = 
		--POWER( 0.001 * t.ndc * CASE WHEN CASE ISNULL(gtc.TotStarts,0) WHEN 0 THEN 1 ELSE ISNULL(gtc.totstarts,0) end > ISNULL(ctgm.AvgTotStarts, 0) 
		--	then CASE ISNULL(gtc.TotStarts,0) WHEN 0 THEN 1 ELSE ISNULL(gtc.totstarts,0) end ELSE ISNULL(ctgm.AvgTotStarts, 0) end, @Pwr_StartsPerMW) 
		--FROM GenerationTotCalc gtc
		-- LEFT JOIN CTGMaintPerStart ctgm ON ctgm.Refnum = gtc.Refnum 
		-- LEFT JOIN TSort t ON t.Refnum = gtc.Refnum 
		--WHERE gtc.refnum = @refnum) b
		--WHERE b.Refnum = #tmp.refnum 

		--UPDATE #tmp SET MWH = POWER((AdjNetMWH2Yr/1000000), @Pwr_MWH) 
		--	FROM GenerationTotCalc gtc 
		--	WHERE gtc.refnum = @refnum

		--UPDATE #tmp SET LiqFuel1 = kgal FROM (
		--SELECT t.Refnum, kgal = POWER(log(ISNULL(jetturbkgal,0) + ISNULL(dieselkgal,0) + ISNULL(fueloilkgal,0)), @Pwr_LiqFuel1) FROM TSort t
		--LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum 
		--WHERE t.refnum LIKE '%CC[0-9][0-9]'
		--and ISNULL(ctgs,0) > 0 AND ISNULL(ctg_ndc,0)/ISNULL(ctgs,0) <= 200
		--and (ISNULL(jetturbkgal,0) + ISNULL(dieselkgal,0) + ISNULL(fueloilkgal,0)) > 0) b
		--WHERE b.Refnum = #tmp.refnum 

		--UPDATE #tmp SET LiqFuel2 = kgal FROM (
		--SELECT t.Refnum, kgal = POWER(log(ISNULL(jetturbkgal,0) + ISNULL(dieselkgal,0) + ISNULL(fueloilkgal,0)),@Pwr_LiqFuel2) FROM TSort t
		--LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = t.Refnum 
		--WHERE t.refnum LIKE '%CC[0-9][0-9]'
		--AND ISNULL(ctgs,0) > 0 AND ISNULL(ctg_ndc,0)/ISNULL(ctgs,0) > 200
		--AND (ISNULL(jetturbkgal,0) + ISNULL(dieselkgal,0) + ISNULL(fueloilkgal,0)) > 0) b
		--WHERE b.Refnum = #tmp.refnum 

	--end of CC
	END
	-------------------------------------------------------------------------------------------------------------------------------------------------



	-------------------------------------------------------------------------------------------------
	--now we get a little tricky
	--the following isn't really necessary for the new calculation, but it is legacy from the old calculation
	--basically we're filling in a bunch of fields in the EGCCalc table, which may or may not be needed later

	--first declare some of the fields we'll be calcing
	DECLARE @AshPcnt real, @CoalPcnt real, @OilPcnt real, @GasPcnt real, @Sulfur real
	--and of course our total
	DECLARE @calc_EGC real

	--next, we're just doing a straight rip of the old code to populate the appropriate variables, so we can insert them
	IF (@Manual = 0)
	--note that manual can be used if you just want to look at the total, not populate the database with it
	BEGIN
		-- run stored proc to try to fill in the EGCTechnology field in CASE this is the 1st time the unit has been calc'd
		EXEC spUPDATEEGCTechnologyForRefnum @Refnum = @refnum

		-- get the base required fields
		SELECT @EGCRegion=rtrim(EGCRegion), @EGCTechnology=rtrim(EGCTechnology) 
			FROM dbo.tsort 
			WHERE refnum=@refnum

		IF RIGHT(@Refnum,1) = 'P' AND @year < 2010 --oops, this will never be called now (since we're 2011 or higher). just leaving it in for posterity
		BEGIN
			SET @EGCTechnology='CARRYOVER'
		END

		SELECT @CTGs = ISNULL(ctgs,0), @CTG_NDC = ISNULL(ctg_ndc,0), @NDC = ISNULL(ndc,0), 
				@CoalNDC = ISNULL(Coal_NDC,0), @OilNDC = ISNULL(Oil_NDC,0), @GasNDC = ISNULL(Gas_NDC,0), 
				@PrecBagYN = ISNULL(PrecBagYN,0), @ScrubbersYN = ISNULL(ScrubbersYN,0), 
				@NumUnitsAtSite = ISNULL(NumUnitsAtSite,0)
			FROM dbo.tsort 
			WHERE refnum=@refnum

		SELECT @FuelType=fueltype 
			FROM dbo.FuelTotCalc  
			WHERE refnum=@refnum

		SELECT @Age=max(age) 
			FROM dbo.nercturbine 
			WHERE refnum=@refnum

		SELECT @ElecGenStarts=ISNULL(SUM(TotalOpps),0) 
			FROM dbo.StartsAnalysis sa 
			INNER JOIN NERCTurbine nt ON nt.Refnum = sa.Refnum AND nt.TurbineID = sa.TurbineID 
			WHERE nt.TurbineType IN ('CTG', 'STG', 'CST') AND sa.Refnum = @Refnum
		SELECT @AdjNetMWH2Yr = ISNULL(AdjNetMWH2Yr,0)/1000000 
			FROM dbo.GenerationTotCalc 
			WHERE Refnum = @Refnum

		SELECT @CoalSulfur = ISNULL(SulfurPcnt,0), @CoalAsh = ISNULL(AshPcnt,0)
			FROM CoalTotCalc
			WHERE Refnum = @Refnum

		SELECT @OilSulfur = ISNULL(Sulfur,0), @OilAsh = 0.1 
			FROM FuelProperties 
			WHERE Refnum = @Refnum

	END

	--more calcs to fill in old data
	IF (@CTG_NDC IS NULL)
		SET @CTG_NDC = 0
	ELSE
		BEGIN
			IF (@CTGs > 0)
				SET @CTG_NDC = @CTG_NDC --/ @CTGs
		END

	IF @OilSulfur IS NULL
		SELECT @OilSulfur = 2.3, @OilAsh = 0.1
	SELECT @Sulfur = (ISNULL(@CoalSulfur,0)*@CoalNDC + ISNULL(@OilSulfur,0)*@OilNDC)/@NDC
	SELECT @AshPcnt = (ISNULL(@CoalAsh,0)*@CoalNDC + ISNULL(@OilAsh,0)*@OilNDC)/@NDC

	IF (@Manual = 0)
	BEGIN
		SELECT @OilMBTU = SUM(MBTU) 
			FROM Fuel 
			WHERE Refnum = @Refnum AND FuelType IN ('Jet', 'FOil', 'Diesel') AND TurbineID IN ('STG','Site')
		IF @OilMBTU IS NULL 
			SELECT @OilMBTU = 0

		SELECT @GasMBTU = SUM(MBTU) 
			FROM Fuel 
			WHERE Refnum = @Refnum AND FuelType IN ('NatGas', 'OffGas', 'H2Gas') AND TurbineID IN ('STG','Site')
		IF @GasMBTU IS NULL 
			SELECT @GasMBTU = 0

		SELECT @CoalMBTU = SUM(MBTU) 
			FROM Coal 
			WHERE Refnum = @Refnum
	END
	
	IF (@OilMBTU + @GasMBTU) > 0
		SELECT @OilPcnt = @OilMBTU/(@OilMBTU + @GasMBTU)
	ELSE
		SELECT @OilPcnt = 0

	IF @OilPcnt < 0.1
		SELECT @OilPcnt = 0

	IF @OilPcnt > 0.9
		SELECT @OilPcnt = 1

	IF (@OilMBTU + @GasMBTU) > 0
		SELECT @GasPcnt = @GasMBTU/(@OilMBTU + @GasMBTU)
	ELSE
		SELECT @GasPcnt = 0

	IF @GasPcnt < 0.1
		SELECT @GasPcnt = 0

	IF @GasPcnt > 0.9
		SELECT @GasPcnt = 1

	IF @CoalMBTU > 0
		SELECT @CoalPcnt = @CoalMBTU/(@CoalMBTU + @OilMBTU + @GasMBTU)
	ELSE
		SELECT @CoalPcnt = 0

	IF @CoalPcnt < 0.1
		SELECT @CoalPcnt = 0

	IF @CoalPcnt > 0.9
		SELECT @CoalPcnt = 1

	--get the EGC value we came up with
	SELECT @calc_EGC = EGCTot FROM #tmp WHERE refnum = @refnum 
	
	--Build the "REAL" EGCCalc record. The one that will be used.
	--if DEBUG THEN ONLY display the value do NOT UPDATE any database records
	IF (@DEBUG = 0)
		BEGIN
			DELETE FROM EGCCalc WHERE Refnum = @Refnum
			INSERT INTO EGCCalc (Refnum, Method, Technology, EGC, CTGs, CTG_NDC, NDC, AdjNetMWH, Sulfur, AshPcnt, 
				FuelType, Coal_NDC, Oil_NDC, Gas_NDC, Starts, OilPcnt, GasPcnt, CoalPcnt, Age, NumUnitsAtSite, 
				ScrubbersYN,  PrecBagYN)
			VALUES (@Refnum, '2011', @EGCTechnology, @calc_EGC, @CTGs, @CTG_NDC, @NDC, @AdjNetMWH2Yr, @Sulfur, @AshPcnt,
				@FuelType, @CoalNDC, @OilNDC, @GasNDC, @ElecGenStarts, @OilPcnt, @GasPcnt, @CoalPcnt, @Age, @NumUnitsAtSite, 
				@ScrubbersYN, @PrecBagYN)

	--		-- Go UPDATE all of the EGC fields spread thoughout the database.
			UPDATE GenerationTotCalc SET EGC = @calc_EGC WHERE Refnum = @Refnum
			UPDATE Gensum SET EGC = @calc_EGC WHERE Refnum = @Refnum

			UPDATE NonOHMaint SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE OHEquipCalc SET TotAnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN TotAnnOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE MaintEquipCalc SET AnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintEquipCalc SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintEquipCalc SET AnnMaintCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintCostKUS/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE MaintTotCalc SET AnnNonOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnNonOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnOHCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnMaintCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnOHProjCostEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHProjCost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnOHExclProjEGC = CASE WHEN @calc_EGC > 0 THEN AnnOHExclProj/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnMaintExclProjEGC = CASE WHEN @calc_EGC > 0 THEN AnnMaintExclProj/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE MaintTotCalc SET AnnLTSACostEGC = CASE WHEN @calc_EGC > 0 THEN AnnLTSACost/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE Pers SET TotEffPersEGC = CASE WHEN @calc_EGC > 0 THEN TotEffPers*1000/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum
			UPDATE PersSTCalc SET TotEffPersEGC = CASE WHEN @calc_EGC > 0 THEN TotEffPers*1000/@calc_EGC ELSE NULL END WHERE Refnum = @Refnum

			UPDATE GenSum SET TotEffPersEGC = (SELECT TotEffPersEGC FROM PersSTCalc WHERE PersSTCalc.Refnum = Gensum.Refnum AND PersSTCalc.SectionID = 'TP') WHERE Refnum = @Refnum

			EXEC spAddOpexCalc @Refnum = @Refnum, @DataType = 'EGC', @DivFactor = @calc_EGC
--PRINT 'remove me'
		END
	ELSE
		BEGIN

			--and if we're just running in DEBUG, all we do is select from the temp table we built
			SELECT * FROM #tmp

		END

	--and get rid of the temp tables we used
	DROP TABLE #tmp
	DROP TABLE #coal

END
