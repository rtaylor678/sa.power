﻿CREATE  VIEW PresOpex AS
SELECT Refnum, DataType, 
TotCompensation AS SiteWagesBen, 
ISNULL(CentralOCCWagesBen,0) + ISNULL(CentralMPSWagesBen,0) AS CentralWagesBen,
ISNULL(ContMaintLabor,0) + ISNULL(ContMaintMatl,0) 
	+ ISNULL(ContMaintLumpSum,0) + ISNULL(OthContSvc,0) AS Contract,
ISNULL(OverhaulAdj,0) + ISNULL(LTSAAdj, 0) AS OverhaulAdj,
ISNULL(MaintMatl,0) + ISNULL(Supply,0) AS Materials,
ISNULL(Envir,0) + ISNULL(PropTax,0) + ISNULL(Insurance,0) 
	+ ISNULL(OthTax,0) AS TaxInsurEnvir,
ISNULL(AGPers,0) AS AGPers,
ISNULL(OthFixed,0) AS OthFixed,
ISNULL(FuelInven,0) + ISNULL(OthVar,0) + ISNULL(Chemicals,0) + ISNULL(Water,0) AS OtherVar
FROM OpexCalc


