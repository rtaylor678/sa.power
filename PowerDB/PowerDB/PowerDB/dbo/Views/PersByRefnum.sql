﻿







CREATE VIEW [dbo].[PersByRefnum]
AS
SELECT     Refnum, 
		ContractMPSAdmin = SUM(CASE WHEN PersCat = 'MPS-Admin' THEN ContractEffPers ELSE NULL END),
		ContractMPSInsp = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN ContractEffPers ELSE NULL END),
		ContractMPSLSCAdj = SUM(CASE WHEN PersCat = 'MPS-LSCAdj' THEN ContractEffPers ELSE NULL END),
		ContractMPSLTSA = SUM(CASE WHEN PersCat = 'MPS-LTSA' THEN ContractEffPers ELSE NULL END),
		ContractMPSNOHMaintInsp = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN ContractEffPers ELSE NULL END),
		ContractMPSNonMaint = SUM(CASE WHEN PersCat = 'MPS-NonMaint' THEN ContractEffPers ELSE NULL END),
		ContractMPSOHAdj = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN ContractEffPers ELSE NULL END),
		ContractMPSOper = SUM(CASE WHEN PersCat = 'MPS-Oper' THEN ContractEffPers ELSE NULL END),
		ContractMPSTech = SUM(CASE WHEN PersCat = 'MPS-Tech' THEN ContractEffPers ELSE NULL END),
		ContractOCCAdmin = SUM(CASE WHEN PersCat = 'OCC-Admin' THEN ContractEffPers ELSE NULL END),
		ContractOCCInsp = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN ContractEffPers ELSE NULL END),
		ContractOCCLSCAdj = SUM(CASE WHEN PersCat = 'OCC-LSCAdj' THEN ContractEffPers ELSE NULL END),
		ContractOCCLTSA = SUM(CASE WHEN PersCat = 'OCC-LTSA' THEN ContractEffPers ELSE NULL END),
		ContractOCCNOHMaintInsp = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN ContractEffPers ELSE NULL END),
		ContractOCCNonMaint = SUM(CASE WHEN PersCat = 'OCC-NonMaint' THEN ContractEffPers ELSE NULL END),
		ContractOCCOHAdj = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN ContractEffPers ELSE NULL END),
		ContractOCCOper = SUM(CASE WHEN PersCat = 'OCC-Oper' THEN ContractEffPers ELSE NULL END),
		ContractOCCTech = SUM(CASE WHEN PersCat = 'OCC-Tech' THEN ContractEffPers ELSE NULL END),
		TotMWHMPSAdmin = SUM(CASE WHEN PersCat = 'MPS-Admin' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSInsp = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSLSCAdj = SUM(CASE WHEN PersCat = 'MPS-LSCAdj' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSLTSA = SUM(CASE WHEN PersCat = 'MPS-LTSA' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSNOHMaintInsp = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSNonMaint = SUM(CASE WHEN PersCat = 'MPS-NonMaint' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSOHAdj = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSOper = SUM(CASE WHEN PersCat = 'MPS-Oper' THEN TotEffPersMWH ELSE NULL END),
		TotMWHMPSTech = SUM(CASE WHEN PersCat = 'MPS-Tech' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCAdmin = SUM(CASE WHEN PersCat = 'OCC-Admin' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCInsp = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCLSCAdj = SUM(CASE WHEN PersCat = 'OCC-LSCAdj' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCLTSA = SUM(CASE WHEN PersCat = 'OCC-LTSA' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCNOHMaintInsp = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCNonMaint = SUM(CASE WHEN PersCat = 'OCC-NonMaint' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCOHAdj = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCOper = SUM(CASE WHEN PersCat = 'OCC-Oper' THEN TotEffPersMWH ELSE NULL END),
		TotMWHOCCTech = SUM(CASE WHEN PersCat = 'OCC-Tech' THEN TotEffPersMWH ELSE NULL END),
		AGMPSAdmin = SUM(CASE WHEN PersCat = 'MPS-Admin' THEN AGEffPers ELSE NULL END),
		AGMPSInsp = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN AGEffPers ELSE NULL END),
		AGMPSNOHMaintInsp = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN AGEffPers ELSE NULL END),
		AGMPSNonMaint = SUM(CASE WHEN PersCat = 'MPS-NonMaint' THEN AGEffPers ELSE NULL END),
		AGMPSOHAdj = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN AGEffPers ELSE NULL END),
		AGMPSOper = SUM(CASE WHEN PersCat = 'MPS-Oper' THEN AGEffPers ELSE NULL END),
		AGMPSTech = SUM(CASE WHEN PersCat = 'MPS-Tech' THEN AGEffPers ELSE NULL END),
		AGOCCAdmin = SUM(CASE WHEN PersCat = 'OCC-Admin' THEN AGEffPers ELSE NULL END),
		AGOCCInsp = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN AGEffPers ELSE NULL END),
		AGOCCNOHMaintInsp = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN AGEffPers ELSE NULL END),
		AGOCCNonMaint = SUM(CASE WHEN PersCat = 'OCC-NonMaint' THEN AGEffPers ELSE NULL END),
		AGOCCOHAdj = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN AGEffPers ELSE NULL END),
		AGOCCOper = SUM(CASE WHEN PersCat = 'OCC-Oper' THEN AGEffPers ELSE NULL END),
		AGOCCTech = SUM(CASE WHEN PersCat = 'OCC-Tech' THEN AGEffPers ELSE NULL END),
		SiteMPSAdmin = SUM(CASE WHEN PersCat = 'MPS-Admin' THEN SiteEffPers ELSE NULL END),
		SiteMPSInsp = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN SiteEffPers ELSE NULL END),
		SiteMPSNOHMaintInsp = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN SiteEffPers ELSE NULL END),
		SiteMPSNonMaint = SUM(CASE WHEN PersCat = 'MPS-NonMaint' THEN SiteEffPers ELSE NULL END),
		SiteMPSOHAdj = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN SiteEffPers ELSE NULL END),
		SiteMPSOper = SUM(CASE WHEN PersCat = 'MPS-Oper' THEN SiteEffPers ELSE NULL END),
		SiteMPSTech = SUM(CASE WHEN PersCat = 'MPS-Tech' THEN SiteEffPers ELSE NULL END),
		SiteOCCAdmin = SUM(CASE WHEN PersCat = 'OCC-Admin' THEN SiteEffPers ELSE NULL END),
		SiteOCCInsp = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN SiteEffPers ELSE NULL END),
		SiteOCCNOHMaintInsp = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN SiteEffPers ELSE NULL END),
		SiteOCCNonMaint = SUM(CASE WHEN PersCat = 'OCC-NonMaint' THEN SiteEffPers ELSE NULL END),
		SiteOCCOHAdj = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN SiteEffPers ELSE NULL END),
		SiteOCCOper = SUM(CASE WHEN PersCat = 'OCC-Oper' THEN SiteEffPers ELSE NULL END),
		SiteOCCTech = SUM(CASE WHEN PersCat = 'OCC-Tech' THEN SiteEffPers ELSE NULL END),
		CentralMPSInsp = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN CentralEffPers ELSE NULL END),
		CentralMPSNOHMaintInsp = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN CentralEffPers ELSE NULL END),
		CentralMPSOHAdj = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN CentralEffPers ELSE NULL END),
		CentralOCCInsp = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN CentralEffPers ELSE NULL END),
		CentralOCCNOHMaintInsp = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN CentralEffPers ELSE NULL END),
		CentralOCCOHAdj = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN CentralEffPers ELSE NULL END),
		OCCOperSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-Oper' THEN SiteOVTHrs ELSE NULL END),
		OCCNOHMaintInspSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN SiteOVTHrs ELSE NULL END),
		OCCInspSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN SiteOVTHrs ELSE NULL END),
		OCCOHAdjSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN SiteOVTHrs ELSE NULL END),
		OCCTechSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-Tech' THEN SiteOVTHrs ELSE NULL END),
		OCCAdminSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-Admin' THEN SiteOVTHrs ELSE NULL END),
		OCCNonMaintSiteOVTHrs = SUM(CASE WHEN PersCat = 'OCC-NonMaint' THEN SiteOVTHrs ELSE NULL END),
		OCCOperSiteSTH = SUM(CASE WHEN PersCat = 'OCC-Oper' THEN SiteSTH ELSE NULL END),
		OCCNOHMaintInspSiteSTH = SUM(CASE WHEN PersCat = 'OCC-NOHMaint' OR PersCat = 'OCC-Insp' THEN SiteSTH ELSE NULL END),
		OCCInspSiteSTH = SUM(CASE WHEN PersCat = 'OCC-Insp' THEN SiteSTH ELSE NULL END),
		OCCOHAdjSiteSTH = SUM(CASE WHEN PersCat = 'OCC-OHAdj' THEN SiteSTH ELSE NULL END),
		OCCTechSiteSTH = SUM(CASE WHEN PersCat = 'OCC-Tech' THEN SiteSTH ELSE NULL END),
		OCCAdminSiteSTH = SUM(CASE WHEN PersCat = 'OCC-Admin' THEN SiteSTH ELSE NULL END),
		OCCNonMaintSiteSTH = SUM(CASE WHEN PersCat = 'OCC-NonMaint' THEN SiteSTH ELSE NULL END),
		MPSOperSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-Oper' THEN SiteOVTHrs ELSE NULL END),
		MPSNOHMaintInspSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN SiteOVTHrs ELSE NULL END),
		MPSInspSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN SiteOVTHrs ELSE NULL END),
		MPSOHAdjSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN SiteOVTHrs ELSE NULL END),
		MPSTechSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-Tech' THEN SiteOVTHrs ELSE NULL END),
		MPSAdminSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-Admin' THEN SiteOVTHrs ELSE NULL END),
		MPSNonMaintSiteOVTHrs = SUM(CASE WHEN PersCat = 'MPS-NonMaint' THEN SiteOVTHrs ELSE NULL END),
		MPSOperSiteSTH = SUM(CASE WHEN PersCat = 'MPS-Oper' THEN SiteSTH ELSE NULL END),
		MPSNOHMaintInspSiteSTH = SUM(CASE WHEN PersCat = 'MPS-NOHMaint' OR PersCat = 'MPS-Insp' THEN SiteSTH ELSE NULL END),
		MPSInspSiteSTH = SUM(CASE WHEN PersCat = 'MPS-Insp' THEN SiteSTH ELSE NULL END),
		MPSOHAdjSiteSTH = SUM(CASE WHEN PersCat = 'MPS-OHAdj' THEN SiteSTH ELSE NULL END),
		MPSTechSiteSTH = SUM(CASE WHEN PersCat = 'MPS-Tech' THEN SiteSTH ELSE NULL END),
		MPSAdminSiteSTH = SUM(CASE WHEN PersCat = 'MPS-Admin' THEN SiteSTH ELSE NULL END),
		MPSNonMaintSiteSTH = SUM(CASE WHEN PersCat = 'MPS-NonMaint' THEN SiteSTH ELSE NULL END)


	
	                     
                      
FROM         dbo.Pers
GROUP BY Refnum









GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Pers"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PersByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'PersByRefnum';

