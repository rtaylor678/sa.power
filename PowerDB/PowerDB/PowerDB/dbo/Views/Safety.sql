﻿
CREATE VIEW dbo.Safety
AS
SELECT     dbo.TSort.Refnum, dbo.SiteSafety.CompOSHAIncRate, dbo.SiteSafety.ContOSHAIncRate, dbo.SiteSafety.OSHAIncRate, dbo.SiteSafety.LostTimeInjNum, 
                      dbo.SiteSafety.FatalNum
FROM         dbo.TSort INNER JOIN
                      dbo.SiteSafety ON dbo.TSort.SiteID = dbo.SiteSafety.SiteID

