﻿CREATE VIEW DataAccuracy AS
SELECT o.Refnum, o.Accuracy, 
AccuracyA = CASE o.Accuracy
	WHEN 'A' THEN 100
	WHEN 'B' THEN 0
	WHEN 'C' THEN 0
	ELSE NULL
	END,
AccuracyB = CASE o.Accuracy
	WHEN 'A' THEN 0
	WHEN 'B' THEN 100
	WHEN 'C' THEN 0
	ELSE NULL
	END,
AccuracyC = CASE o.Accuracy
	WHEN 'A' THEN 0
	WHEN 'B' THEN 0
	WHEN 'C' THEN 100
	ELSE NULL
	END,
AccuracyMWH = CASE o.Accuracy
	WHEN 'A' THEN g.AdjNetMWH
	WHEN 'B' THEN g.AdjNetMWH
	WHEN 'C' THEN g.AdjNetMWH
	ELSE NULL
	END
FROM OpEx o INNER JOIN GenerationTotCalc g
ON o.Refnum = g.Refnum


