﻿CREATE VIEW _RankView AS
SELECT r.Refnum, ts.CoLoc, Variable = v.Description, r.Value,
r.Rank, s.DataCount, r.Percentile, r.Tile, s.NumTiles, 
l.ListName, BreakCondition = b.Description, s.BreakValue,
s.RankSpecsID, s.BreakID, s.RankVariableID /*, v.VariableID, v.Scenario, s.RefListNo*/
FROM Rank r, RankSpecs_LU s, RankBreaks b, RankVariables v,
	RefList_LU l, Tsort ts
WHERE r.RankSpecsID = s.RankSpecsID
and s.BreakID = b.BreakID 
and s.RankVariableID = v.RankVariableID
and s.RefListNo = l.RefListNo
and r.Refnum = ts.Refnum


