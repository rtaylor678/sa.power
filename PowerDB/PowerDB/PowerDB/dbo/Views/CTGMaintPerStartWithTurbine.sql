﻿
CREATE VIEW [dbo].[CTGMaintPerStartWithTurbine]
AS
SELECT Refnum, TurbineID, AvgTotStarts = AVG(AvgTotStarts), 
LTSAPerStart = AVG(ISNULL(LTSAAdj, 0))/AVG(AvgTotStarts),
AnnOHPerStart = AVG(ISNULL(AnnOHCost, 0))/AVG(AvgTotStarts),
NonOHPerStart = AVG(ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts),
MaintCostPerStart = AVG(ISNULL(LTSAAdj, 0)+ISNULL(AnnOHCost, 0)+ISNULL(AnnNonOHCost, 0))/AVG(AvgTotStarts)
FROM CTGMaintCosts
GROUP BY Refnum, TurbineID 
HAVING AVG(AvgTotStarts)>0


