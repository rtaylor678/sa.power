﻿


CREATE VIEW UnitGenData AS 

SELECT t.Refnum, 
OilGasUnitNum, OilGasUnitCap, OilGasUnitMWH, 
CoalUnitNum, CoalUnitCap, CoalUnitMWH, 
CTNum, CTCap, CTMWH, 
CCUnitNum, CCUnitCap, CCUnitMWH, 
SCNum, SCCap, SCMWH, 
OthUnitNum, OthUnitCap, OthUnitMWH, 
TotUnitNum = ISNULL(OilGasUnitNum,0) + ISNULL(CoalUnitNum,0) + ISNULL(CTNum,0) + ISNULL(CCUnitNum,0) + ISNULL(SCNum,0) + ISNULL(OthUnitNum,0), 
TotCap = ISNULL(OilGasUnitCap,0) + ISNULL(CoalUnitCap,0) + ISNULL(CTCap,0) + ISNULL(CCUnitCap,0) + ISNULL(SCCap,0) + ISNULL(OthUnitCap,0),  
TotMWH = ISNULL(OilGasUnitMWH,0) + ISNULL(CoalUnitMWH,0) + ISNULL(CTMWH,0) + ISNULL(CCUnitMWH,0) + ISNULL(SCMWH,0) + ISNULL(OthUnitMWH,0), 
TotExclCTNum = ISNULL(OilGasUnitNum,0) + ISNULL(CoalUnitNum,0) + ISNULL(OthUnitNum,0), 
TotExclCTCap = ISNULL(OilGasUnitCap,0) + ISNULL(CoalUnitCap,0) + ISNULL(OthUnitCap,0),  
TotExclCTMWH = ISNULL(OilGasUnitMWH,0) + ISNULL(CoalUnitMWH,0) + ISNULL(OthUnitMWH,0),
TotExclSCNum = ISNULL(OilGasUnitNum,0) + ISNULL(CoalUnitNum,0) + ISNULL(CCUnitNum,0) + ISNULL(OthUnitNum,0), 
TotExclSCCap = ISNULL(OilGasUnitCap,0) + ISNULL(CoalUnitCap,0) + ISNULL(CCUnitCap,0) + ISNULL(OthUnitCap,0),  
TotExclSCMWH = ISNULL(OilGasUnitMWH,0) + ISNULL(CoalUnitMWH,0) + ISNULL(CCUnitMWH,0) + ISNULL(OthUnitMWH,0)
FROM PlantGenData p INNER JOIN TSort t ON p.SiteID = t.SiteID



