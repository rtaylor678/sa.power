﻿


/****** Object:  View dbo.CRVGapFactors    Script Date: 4/18/2003 4:38:53 PM ******/
CREATE VIEW [dbo].[CRVGapFactors]
AS
SELECT StudyYear, 
	CRVSizeGroup, 
	PeakUnavail_Unp, 
	HeatRate, 
	TotCashLessFuelMWH, 
	TotCashLessFuelEmmMWH, 
	PlantManageableMWH, 
	TotCashLessFuelEmmMW, 
	TotCashLessFuelEmmEGC,
    UnplannedCommUnavailIndex,
    PlannedCommUnavailIndex,
    AuxPct,
    ThermEff,
    MaintIndexMWH,
    OHIndexMWH,
    NonOHIndexMWH,
    LTSAIndexMWH
    
FROM         PowerGlobal.dbo.CRVGapFactors



