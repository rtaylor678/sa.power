﻿










CREATE VIEW [dbo].[EGC13CC]
AS
SELECT 
	e.Refnum, 
	t.CoLoc, 
	t.StudyYear,
	nf.Age,
	NDC = ROUND(nf.NDC,1),
	CTG_Count = ISNULL(ISNULL(nt_CTG2.CTG_Count,nt_CTG.CTG_Count),1), --theory is that this is set up as a single shaft unit, so 1 CTG if NULL
	CTG_NDC = ROUND(ISNULL(nt_CTG.CTG_NDC, nf.NDC * 2/3),1) , -- if it is a single shaft the CTG is producing 2/3 and the STG 1/3
	NumUnitsAtSite = ISNULL(p.CCUnitNum,0) + ISNULL(p.SCNum,0) + ISNULL(p.OthUnitNum,0),
	Site_NDC = ROUND(ISNULL(p.CCUnitCap,0) + ISNULL(p.SCCap,0) + ISNULL(p.OthUnitCap,0),1),
	EGCRegion,
	ftc.FuelType,
	SCR = ISNULL(ps.SCR,0),
	dd.BlrPSIG,
	dd.BlrTemp,
	ctgd.FiringTemp,
	ctgd.Class,
	clu.FrequencyHz,
	t.SingleShaft,
	t.NumSTs,
	t.DryCoolTower,
	nf.ServiceHrs,
	gtc.AdjNetMWH,
	gtc.AdjNetMWH2Yr,
	--Starts = CASE ISNULL(gtc.TotStarts,0) WHEN 0 THEN ISNULL(nf.ACTStarts,0) ELSE ISNULL(gtc.TotStarts,0) END,
	Starts = CASE ISNULL(nf.ACTStarts,0) WHEN 0 THEN ISNULL(gtc.TotStarts,0) ELSE ISNULL(nf.ACTStarts,0) END,
	AnnOHCostMW = ISNULL(mtc.AnnOHCostMW,0) + ISNULL(mtc.AnnLTSACostMW,0),
	mtc.AnnNonOHCostMW,
	ss.StmSalesMBTU,
	ss.ABSStmSalesMBTU,
	fuel.CTGGasMBTU,
	fuel.CTGLiquidMBTU,
	fuel.SiteMBTU,
	o.TotCashLessFuelEm

FROM dbo.EGC13 AS e
	LEFT JOIN TSort t ON t.Refnum = e.Refnum
	LEFT JOIN NERCFactors nf ON nf.Refnum = e.Refnum
	LEFT JOIN PlantGenData p ON p.SiteID = t.SiteID
	LEFT JOIN FuelTotCalc ftc ON ftc.Refnum = e.Refnum
	LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = e.Refnum
	LEFT JOIN MaintTotCalc mtc ON mtc.Refnum = e.Refnum
	LEFT JOIN OpExCalc o ON o.Refnum = e.Refnum AND o.DataType = 'ADJ'
	LEFT JOIN PowerGlobal.dbo.Country_LU clu ON clu.Country = t.Country

	LEFT JOIN (SELECT Refnum, FiringTemp = MAX(FiringTemp), Class FROM CTGData GROUP BY Refnum, Class) ctgd ON ctgd.Refnum = t.Refnum

	LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*), CTG_NDC = SUM(CASE WHEN TurbineType = 'CC' THEN (NDC * 2/3) ELSE NDC END) -- trying to handle blocks
				FROM NERCTurbine 
				WHERE TurbineType IN( 'CTG','CC') AND NDC IS NOT NULL 
				GROUP BY Refnum) nt_CTG ON nt_CTG.Refnum = e.Refnum
				
	LEFT JOIN (SELECT Refnum, CTG_Count = COUNT(*) FROM NERCTurbine WHERE TurbineType = 'CTG' AND NDC IS NULL GROUP BY Refnum) nt_CTG2 ON nt_CTG2.Refnum = e.Refnum
	--this one is trying to handle the (very rare, perhaps only Capital Bridgeport?) unit that has a CC set up with an NDC, and CTGs set up but NULL, to get the correct CTG count

	LEFT JOIN (SELECT Refnum, 
					SCR = CASE SUM(CASE WHEN Component IN ('SCR','CTG-SCR') THEN totannohcost else 0 end) WHEN 0 THEN 0 ELSE 1 END
				FROM (SELECT Refnum, Component, TotAnnOHCost FROM OHMaint UNION SELECT Refnum, Component, AnnNonOHCost FROM NonOHMaint) b
				WHERE Component IN ('CTG-SCR','SCR')
				GROUP BY Refnum) ps ON ps.Refnum = e.Refnum

	LEFT JOIN (SELECT nt.Refnum, BlrPSIG = MAX(d.BlrPSIG), BlrTemp = MAX(d.blrtemp) FROM NERCTurbine nt LEFT JOIN DesignData d ON d.UtilityUnitCode = nt.UtilityUnitCode GROUP BY nt.Refnum) dd ON dd.Refnum = e.Refnum
	LEFT JOIN (SELECT s.refnum, StmSalesMBTU = SUM(StmSalesMBTU), ABSStmSalesMBTU = SUM(ABS(StmSalesMBTU)) FROM SteamSales s LEFT JOIN TSort t ON t.Refnum = s.Refnum WHERE YEAR(s.Period) = t.StudyYear GROUP BY s.Refnum) ss ON ss.Refnum = e.Refnum

	LEFT JOIN (SELECT Refnum,
						SiteMBTU = SUM(CASE TurbineID WHEN 'Site' THEN MBTU ELSE 0 END),
						CTGLiquidMBTU = SUM(CASE TurbineID WHEN 'Site' THEN 0 ELSE CASE WHEN FuelType IN ('Diesel','Jet','FOil') THEN MBTU ELSE 0 END END),
						CTGGasMBTU = SUM(CASE TurbineID WHEN 'Site' THEN 0 ELSE CASE WHEN FuelType IN ('Diesel','Jet','FOil') THEN 0 ELSE MBTU END END)
				FROM Fuel
				GROUP BY Refnum) fuel ON fuel.Refnum = e.Refnum
				

WHERE e.Refnum LIKE '%CC%'







