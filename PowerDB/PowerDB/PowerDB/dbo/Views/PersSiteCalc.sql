﻿CREATE VIEW PersSiteCalc AS 
SELECT t.SiteID, p.PersCat, l.SortKey, Sum(p.SiteNumPers) AS SiteNumPers, Sum(p.SiteSTH) AS SiteSTH,
Sum(p.SiteOVTHrs) AS SiteOVTHrs, Sum(p.CentralNumPers) AS CentralNumPers, 
Sum(p.CentralSTH) AS CentralSTH, Sum(p.CentralOVTHrs) AS CentralOVTHrs, 
Sum(p.AGOnSite) AS AGOnSite, Sum(p.AGOthLoc) AS AGOthLoc, Sum(p.Contract) AS Contract
FROM Pers p, Pers_LU l, TSort t
WHERE p.Refnum = t.Refnum AND l.PersCat = p.PersCat
GROUP BY t.SiteID, p.PersCat, l.SortKey



