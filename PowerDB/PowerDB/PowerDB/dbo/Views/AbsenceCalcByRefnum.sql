﻿











CREATE VIEW [dbo].[AbsenceCalcByRefnum]
AS
SELECT     Refnum, 
		OCCAbsenceHOL = SUM(CASE WHEN AbsCategory = 'HOL' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceHOLVAC = SUM(CASE WHEN AbsCategory = 'HOLVAC' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceLEGAL = SUM(CASE WHEN AbsCategory = 'LEGAL' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceONJOB = SUM(CASE WHEN AbsCategory = 'ONJOB' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceONJOBC = SUM(CASE WHEN AbsCategory = 'ONJOBC' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceOTHER = SUM(CASE WHEN AbsCategory = 'OTHER' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceOTHEXC = SUM(CASE WHEN AbsCategory = 'OTHEXC' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceSICK = SUM(CASE WHEN AbsCategory = 'SICK' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceUNEXC = SUM(CASE WHEN AbsCategory = 'UNEXC' THEN OCCAbsPcnt ELSE NULL END),
		OCCAbsenceVAC = SUM(CASE WHEN AbsCategory = 'VAC' THEN OCCAbsPcnt ELSE NULL END),
		MPSAbsenceHOL = SUM(CASE WHEN AbsCategory = 'HOL' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceHOLVAC = SUM(CASE WHEN AbsCategory = 'HOLVAC' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceLEGAL = SUM(CASE WHEN AbsCategory = 'LEGAL' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceONJOB = SUM(CASE WHEN AbsCategory = 'ONJOB' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceONJOBC = SUM(CASE WHEN AbsCategory = 'ONJOBC' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceOTHER = SUM(CASE WHEN AbsCategory = 'OTHER' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceOTHEXC = SUM(CASE WHEN AbsCategory = 'OTHEXC' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceSICK = SUM(CASE WHEN AbsCategory = 'SICK' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceUNEXC = SUM(CASE WHEN AbsCategory = 'UNEXC' THEN MPSAbsPcnt ELSE NULL END),
		MPSAbsenceVAC = SUM(CASE WHEN AbsCategory = 'VAC' THEN MPSAbsPcnt ELSE NULL END),
		TotAbsenceHOL = SUM(CASE WHEN AbsCategory = 'HOL' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceHOLVAC = SUM(CASE WHEN AbsCategory = 'HOLVAC' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceLEGAL = SUM(CASE WHEN AbsCategory = 'LEGAL' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceONJOB = SUM(CASE WHEN AbsCategory = 'ONJOB' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceONJOBC = SUM(CASE WHEN AbsCategory = 'ONJOBC' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceOTHER = SUM(CASE WHEN AbsCategory = 'OTHER' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceOTHEXC = SUM(CASE WHEN AbsCategory = 'OTHEXC' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceSICK = SUM(CASE WHEN AbsCategory = 'SICK' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceUNEXC = SUM(CASE WHEN AbsCategory = 'UNEXC' THEN TotAbsPcnt ELSE NULL END),
		TotAbsenceVAC = SUM(CASE WHEN AbsCategory = 'VAC' THEN TotAbsPcnt ELSE NULL END)

FROM         dbo.AbsenceCalc
GROUP BY Refnum













GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPane1', @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "AbsenceCalc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 227
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'AbsenceCalcByRefnum';


GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW', @level1name = N'AbsenceCalcByRefnum';

