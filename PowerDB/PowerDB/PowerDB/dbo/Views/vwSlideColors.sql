﻿

CREATE VIEW [dbo].[vwSlideColors]
AS

	SELECT SlideColorGroup
		,Color
		,ColorOrder
	FROM SlideColors

