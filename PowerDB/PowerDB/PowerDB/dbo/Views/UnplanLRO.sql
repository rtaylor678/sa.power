﻿
CREATE VIEW [dbo].[UnplanLRO] AS

	SELECT 
		t.Refnum, 
		t.SiteID, 
		s.SiteLabel, 
		t.UnitLabel, 
		t.StudyYear, 
		b.FuelGroup, 
		e.CAUSE_CODE, 
		e.EVNT_NO, 
		LostMWH = e.LOSTMW * e.DURATION, 
		e.EVNT_Category, 
		LRO.LRO, 
		cc.PresDescription

	FROM TSort t
		LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN Events e ON e.Refnum = t.Refnum
		LEFT JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
		LEFT JOIN CauseCodes cc ON cc.CAUSE_CODE = e.CAUSE_CODE

	WHERE lro.LRO > 0
		AND e.CAUSE_CODE <> 7777
		AND e.EVNT_Category in ('F', 'M')
		AND (e.LOSTMW * e.DURATION) > 0

