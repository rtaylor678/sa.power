﻿
CREATE  VIEW [dbo].[CoalCostMBTU] AS 
SELECT	c.Refnum, MineCostMBTU = MineCostTon*(TotTons/MBTU), 
	TransCostMBTU = Isnull(TransCostTon*(TotTons/MBTU),0), 
	OnSitePrepCostMBTU = OnSitePrepCostTon*(TotTons/MBTU),
	TotCostMBTU = CASE WHEN t.StudyYear <=2013 THEN MineCostTon*(TotTons/MBTU) + Isnull(TransCostTon*(TotTons/MBTU),0) ELSE (DeliveredCostTon*TotTons)/MBTU END,
	MBTU,
	MineCostMBTULHV = MineCostTon*(TotTons/MBTULHV), 
	TransCostMBTULHV = Isnull(TransCostTon*(TotTons/MBTULHV),0), 
	OnSitePrepCostMBTULHV = OnSitePrepCostTon*(TotTons/MBTULHV),
	TotCostMBTULHV = CASE WHEN t.StudyYear <=2013 THEN MineCostTon*(TotTons/MBTU) + Isnull(TransCostTon*(TotTons/MBTULHV),0) ELSE (DeliveredCostTon*TotTons)/MBTULHV END,
	MBTULHV
from coaltotcalc c
left join TSort t on t.refnum = c.refnum




