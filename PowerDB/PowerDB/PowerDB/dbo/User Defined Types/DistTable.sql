﻿CREATE TYPE [dbo].[DistTable] AS TABLE (
    [value]        REAL NOT NULL,
    [weightfactor] REAL NOT NULL);

