﻿

CREATE FUNCTION [dbo].[EGC]
(	
	@ListName VARCHAR(30),
	@Group1 VARCHAR(30),
	@Group2 VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 t.UnitLabel, o.TotCashLessFuelEM
	FROM OpexCalc o
		LEFT JOIN TSort t ON o.Refnum = t.Refnum
		LEFT JOIN GenSum g ON o.Refnum = g.Refnum
	WHERE o.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND t.StudyYear = @Group1
		AND o.DataType = 'EGC'
	ORDER BY o.TotCashLessFuel

)


