﻿




CREATE FUNCTION [dbo].[MaintCost]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@DataType VARCHAR(3),
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)
)
RETURNS TABLE 
AS
RETURN 
(




	SELECT UnitLabel = Label,
		'Non-Overhaul' = SUM(NonOH) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Overhaul & Special Projects <15 yrs' = SUM(OHUnder15) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Special Projects >=15 yrs' = SUM(OHOver15) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'LTSA/CSA' = SUM(LTSA) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END

		FROM (
		SELECT Label = 'Group Avg', NonOH = Value, OHUnder15 = NULL, OHOver15 = NULL, LTSA = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Non_Overhaul'
		UNION
		SELECT Label = 'Group Avg', NonOH = NULL, OHUnder15 = Value, OHOver15 = NULL, LTSA = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Overhaul'
		UNION
		SELECT Label = 'Group Avg', NonOH = NULL, OHUnder15 = NULL, OHOver15 = Value, LTSA = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Projects'
		UNION
		SELECT Label = 'Group Avg', NonOH = NULL, OHUnder15 = NULL, OHOver15 = NULL, LTSA = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'LTSA/CSA') b
	GROUP BY Label



	UNION ALL

		SELECT UnitLabel = Label,
		'Non-Overhaul' = SUM(NonOH) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Overhaul & Special Projects <15 yrs' = SUM(OHUnder15) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Special Projects >= 15 yrs' = SUM(OHOver15) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'LTSA/CSA' = SUM(LTSA) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END

		FROM (
		SELECT Label = 'Pace- setter', NonOH = Value, OHUnder15 = NULL, OHOver15 = NULL, LTSA = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Non_Overhaul_PS'
		UNION
		SELECT Label = 'Pace- setter', NonOH = NULL, OHUnder15 = Value, OHOver15 = NULL, LTSA = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Overhaul_PS'
		UNION
		SELECT Label = 'Pace- setter', NonOH = NULL, OHUnder15 = NULL, OHOver15 = Value, LTSA = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'Projects_PS'
		UNION
		SELECT Label = 'Pace- setter', NonOH = NULL, OHUnder15 = NULL, OHOver15 = NULL, LTSA = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'MaintIndex' AND Filter1 = @CRVGroup AND Filter2 = @DataType AND Label = 'LTSA/CSA_PS') b
	GROUP BY Label



	UNION ALL

	SELECT UnitLabel, [Non-Overhaul], [Overhaul & Special Projects <15 yrs], [Special Projects >= 15 yrs], [LTSA/CSA] FROM (
		SELECT UnitLabel = 'Co Avg',
			'Non-Overhaul' = GlobalDB.dbo.WtAvg(ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnNonOHCostMW WHEN 'EGC' THEN o.AnnNonOHCostEGC ELSE o.AnnNonOHCostMWH END,0), CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH2Yr END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnOHCostMW WHEN 'EGC' THEN o.AnnOHCostEGC ELSE o.AnnOHCostMWH END,0) - ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnOHCostOver15MW WHEN 'EGC' THEN o.AnnOHCostOver15EGC ELSE o.AnnOHCostOver15MWH END,0), CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH2Yr END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(ISNULL(CASE @DataType WHEN 'KW' THEN o.annOHCostover15MW WHEN 'EGC' THEN o.annOHCostover15EGC ELSE o.annOHCostover15MWH END,0), CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH2Yr END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			'LTSA/CSA' = GlobalDB.dbo.WtAvg(ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnLTSACostMW WHEN 'EGC' THEN o.AnnLTSACostEGC ELSE o.AnnLTSACostMWH END,0), CASE @DataType WHEN 'EGC' THEN 1 ELSE g.AdjNetMWH2Yr END) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
			COUNT(*) AS cnt
		FROM TSort t
			LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
			LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) AND t.Refnum NOT LIKE '%G'
			AND (
				(@CRVGroup = 'C1' AND b.CoalNMCGroup2 = 1) OR 
				(@CRVGroup = 'C2' AND b.CoalNMCGroup2 = 2) OR 
				(@CRVGroup = 'C3' AND b.CoalNMCGroup2 = 3) OR 
				(@CRVGroup = 'GAS' AND b.SteamGasOil = 'Y') OR
				(b.CRVSizeGroup = @CRVGroup AND b.CombinedCycle = 'Y') OR
				(@CRVGroup = 'ALL')
				)
	) b
	WHERE cnt > 1

	UNION ALL


	SELECT UnitLabel, [Non-Overhaul], [Overhaul & Special Projects <15 yrs],[Special Projects >= 15 yrs],[LTSA/CSA]
	FROM PowerGlobal.dbo.Numbers

	LEFT JOIN (
	SELECT TOP 1000 UnitLabel = rtrim(UnitLabel), --TOP 1000 here so that the ORDER BY will work in a TVF
		'Non-Overhaul' = ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnNonOHCostMW WHEN 'EGC' THEN o.AnnNonOHCostEGC ELSE o.AnnNonOHCostMWH END,0) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Overhaul & Special Projects <15 yrs' = (ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnOHCostMW WHEN 'EGC' THEN o.AnnOHCostEGC ELSE o.AnnOHCostMWH END, 0) - ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnOHCostOver15MW WHEN 'EGC' THEN o.AnnOHCostOver15EGC ELSE o.AnnOHCostOver15MWH END, 0)) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'Special Projects >= 15 yrs' = ISNULL(CASE @DataType WHEN 'KW' THEN o.annOHCostover15MW WHEN 'EGC' THEN o.annOHCostover15EGC ELSE o.annOHCostover15MWH END,0) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		'LTSA/CSA' = ISNULL(CASE @DataType WHEN 'KW' THEN o.AnnLTSACostMW WHEN 'EGC' THEN o.AnnLTSACostEGC ELSE o.AnnLTSACostMWH END,0) * CASE WHEN @DataType = 'EGC' THEN 1 ELSE GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) END,
		Rank = RANK() OVER (ORDER BY CASE WHEN t.refnum LIKE '%G' THEN 1 ELSE 0 END, CASE @DataType WHEN 'KW' THEN o.AnnMaintCostMW WHEN 'EGC' THEN o.AnnMaintCostEGC ELSE o.AnnMaintCostMWH END DESC)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND ((
			(@CRVGroup = 'C1' AND b.CoalNMCGroup2 = 1) OR 
			(@CRVGroup = 'C2' AND b.CoalNMCGroup2 = 2) OR 
			(@CRVGroup = 'C3' AND b.CoalNMCGroup2 = 3) OR 
			(@CRVGroup = 'GAS' AND b.SteamGasOil = 'Y') OR
			(b.CRVSizeGroup = @CRVGroup AND b.CombinedCycle = 'Y') OR
			(@CRVGroup = 'ALL')
			)
			)--OR t.Refnum LIKE '%G')
--	ORDER BY CASE @DataType WHEN 'KW' THEN o.AnnMaintCostMW WHEN 'EGC' THEN o.AnnMaintCostEGC ELSE o.AnnMaintCostMWH END DESC
	) c ON Num = Rank
	WHERE Num <= 9 OR Rank IS NOT NULL


)





