﻿



CREATE FUNCTION [dbo].[zzzWaterfallGetData]
(	
	@ListName VARCHAR(30),
	@DataType VARCHAR(10) --should be MWH or MW
	--@StudyYear INT,
--	@CRVGroup VARCHAR(30),
	--@ParentList VARCHAR(30)
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 Title, Title2, BeginCell = CAST(ROUND(BeginCell,2) AS money), Gap1, Label = CAST(ROUND(Label,2) AS money), Down, Up, EndCell = CAST(ROUND(EndCell,2) AS money)
	FROM WaterfallData 
	WHERE Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND DataType = @DataType
	ORDER BY Title, sort

)




