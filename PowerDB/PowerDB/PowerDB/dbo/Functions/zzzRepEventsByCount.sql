﻿

CREATE FUNCTION [dbo].[zzzRepEventsByCount]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30), --coal or gas & oil
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 7 UnitLabel, TotalDolperLostMWH
	FROM PowerGlobal.dbo.Numbers n
		LEFT JOIN (

		SELECT TOP 7 UnitLabel = '',
			CASE WHEN SUM(e.LOSTMW*e.DURATION) > 0 THEN (SUM(lro.LRO * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))/SUM(e.LOSTMW*e.DURATION))*1000 END AS TotalDolperLostMWH,
			RANK() OVER (ORDER BY COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) DESC) AS Rank
  
		FROM TSort t
			INNER JOIN Breaks b ON b.Refnum = t.Refnum
			INNER JOIN Events e ON e.Refnum = t.Refnum 
			INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
			INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
		WHERE --lro.LRO >0 AND e.CAUSE_CODE <> 7777 		AND 
			t.studyyear = @StudyYear 
			AND (
				(@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
				(@CRVGroup = 'Gas & Oil' AND b.FuelGroup = 'Gas & Oil')
				)
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) 
			AND e.EVNT_Category in ('F', 'M')
		GROUP BY e.CAUSE_CODE, PresDescription
		HAVING COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) >1
		ORDER BY COUNT(DISTINCT t.Refnum + CAST(e.EVNT_NO as varchar(10))) DESC

		) b ON b.Rank = n.Num
	WHERE n.Num <= 7
)

