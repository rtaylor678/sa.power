﻿

CREATE FUNCTION [dbo].[GetDollarsText](@Dollars REAL)
RETURNS VARCHAR(255)
BEGIN

	--give the function a REAL value and it will round it, cast as money and return it
	--e.g. 3161.49 gets 3,161; while 3161.58 get 3,162

	DECLARE @Result VARCHAR(255)
	SET @Result = REPLACE(CONVERT(varchar,CAST(ROUND(@Dollars,0) AS money),1), '.00','')

	RETURN @Result
END



