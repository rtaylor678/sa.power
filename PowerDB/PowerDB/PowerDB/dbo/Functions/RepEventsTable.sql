﻿





CREATE FUNCTION [dbo].[RepEventsTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ByUnit BIT, -- all units (0) or each unit individually (1)
	@Filter VARCHAR(10), -- Coal, Gas & Oil, or ''
	@ResultType INT, -- total LRO (0) or LRO/MWh (1) or Count (2)
	@RepetitiveEvents BIT, -- all events (0) or only repetitive (1) (repetitive is defined as >3 of the same cause code)
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT TOP 1000 SiteLabel2, CauseCode, NumEvents, TotalLRO, NumSites, NumUnits, PresDescription FROM (

		-- we get a header for each unit
		SELECT DISTINCT SiteLabel2, 
			'<FMT colWidth=106 LeftMargin=7 ColorFont=98002E><b>Cause Code</b>' as CauseCode,--<html ><head><title></title></head><body><b>Hello</b></body></html >
			'<FMT LeftMargin=7># Events' as NumEvents,
			'<FMT LeftMargin=7>Total LRO, k ' + RTRIM(@CurrencyCode) as TotalLRO,
			'<FMT LeftMargin=7># Plants' as NumSites,
			'<FMT LeftMargin=7># Units' as NumUnits,
			'' AS PresDescription,
			Num = 0,--dummy values to force the header in each unit
			Rank = 0
		FROM RepEventsParent(@ListName,@StudyYear,@ByUnit,@Filter,@ResultType,@RepetitiveEvents)

		UNION ALL

		--and add the data for each unit
		SELECT SiteLabel2,
			'<FMT LeftMargin=7 Align=Center ColorFont=98002E>' + '<b>' + ISNULL(CAST(Cause_Code AS varchar),'') + '</b>', 
			'<FMT LeftMargin=7 Align=Center>' + CAST(NumEvents as varchar), 
			'<FMT LeftMargin=7 Align=Center>' + FORMAT(LRO * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),'#,##0'), 
			'<FMT LeftMargin=7 Align=Center>' + cast(NumPlants as varchar), 
			'<FMT LeftMargin=7 Align=Center>' + cast(NumUnits as varchar), 
			'<FMT LeftMargin=7 Align=Center>' + PresDescription,
			Num,
			Rank
		FROM RepEventsParent(@ListName,@StudyYear,@ByUnit,@Filter,@ResultType,@RepetitiveEvents)

	) b ORDER BY SiteLabel2, Num, Rank

)




