﻿





CREATE FUNCTION [dbo].[ScrubbingIndex]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CoalSulfurGroup VARCHAR(30),
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(



	SELECT top 1000 UnitLabel, 
		[Maintenance Cost] = [Maintenance Cost] * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear), 
		[Non-Maintenance Cost] = [Non-Maintenance Cost] * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear), 
		[Limestone/Chemical Cost] = [Limestone/Chemical Cost] * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear), 
		[Power Consumption] = [Power Consumption] * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)

	FROM PowerGlobal.dbo.Numbers n 
		LEFT OUTER JOIN (


		select *, rownum = ROW_NUMBER() OVER(ORDER BY Sort) from (


	select UnitLabel = 'Group Avg', 
		'Maintenance Cost' = SUM(case when Label = 'Maint' then Value end), 
		'Non-Maintenance Cost' = SUM(case when Label = 'NonMaint' then Value end),
		'Limestone/Chemical Cost' = SUM(case when Label = 'Chem' then Value end), 
		'Power Consumption' = SUM(case when Label = 'Power' then Value end),
		Sort = 1
	from SlideSourceData 
	where Source = @ShortListName
		and Type = 'ScrubIndex' 
		and Filter1 = @CoalSulfurGroup 
		and Filter2 = 'Group'



	UNION ALL



		select UnitLabel = 'Pace- setter', 
		'Maintenance Cost' = SUM(case when Label = 'Maint' then Value end), 
		'Non-Maintenance Cost' = SUM(case when Label = 'NonMaint' then Value end),
		'Limestone/Chemical Cost' = SUM(case when Label = 'Chem' then Value end), 
		'Power Consumption' = SUM(case when Label = 'Power' then Value end),
		Sort = 2
	from SlideSourceData 
	where Source = @ShortListName
		and Type = 'ScrubIndex' 
		and Filter1 = @CoalSulfurGroup 
		and Filter2 = 'PS'


	UNION ALL

	SELECT UnitLabel, [Maintenance Cost], [Non-Maintenance Cost], [Limestone/Chemical Cost], [Power Consumption], Sort FROM (
		SELECT UnitLabel = 'Co Avg',
			'Maintenance Cost' = GlobalDB.dbo.WtAvg(mc.ScrubberAnnMaintCost*1000/s.TonsSO2Removed, s.TonsSO2Removed),
			'Non-Maintenance Cost' = GlobalDB.dbo.WtAvg(m.ScrCostNMaint*1000/s.TonsSO2Removed, s.TonsSO2Removed),
			'Limestone/Chemical Cost' = GlobalDB.dbo.WtAvg((ISNULL(m.ScrCostLime, 0) + ISNULL(m.ScrCostChem, 0))*1000/s.TonsSO2Removed, s.TonsSO2Removed),
			'Power Consumption' = GlobalDB.dbo.WtAvg(mc.ScrubberPowerCost*1000/s.TonsSO2Removed, s.TonsSO2Removed),
			Sort = 3,
			COUNT(*) AS cnt
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN Misc m ON t.Refnum = m.Refnum
			LEFT JOIN MiscCalc mc ON t.Refnum = mc.Refnum
			LEFT JOIN SO2Removal s ON t.Refnum = s.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND b.Scrubbers = 'Y'
			AND b.CoalSulfurGroup = @CoalSulfurGroup
	) b
	WHERE cnt > 1


	UNION ALL

	SELECT TOP 1000 rtrim(UnitLabel), --TOP 1000 here so that the ORDER BY will work in a TVF
		'Maintenance Cost' = mc.ScrubberAnnMaintCost*1000/s.TonsSO2Removed,
		'Non-Maintenance Cost' = ISNULL(m.ScrCostNMaint,0)*1000/s.TonsSO2Removed,
		'Limestone/Chemical Cost' = (ISNULL(m.ScrCostLime, 0) + ISNULL(m.ScrCostChem, 0))*1000/s.TonsSO2Removed,
		'Power Consumption' = mc.ScrubberPowerCost*1000/s.TonsSO2Removed,
		Sort = 4
	FROM TSort t
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN Misc m ON t.Refnum = m.Refnum
		LEFT JOIN MiscCalc mc ON t.Refnum = mc.Refnum
		LEFT JOIN SO2Removal s ON t.Refnum = s.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.Scrubbers = 'Y'
		AND b.CoalSulfurGroup = @CoalSulfurGroup
	ORDER BY s.ScrubbingIndex DESC


	) c

	) b on Num = rownum
	WHERE Num <= 12 --because we need 12 rows
	order by Num


)



