﻿


CREATE FUNCTION [dbo].[WaterfallLeftJoin]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
--	@CRVGroup VARCHAR(30),
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10),
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(


select wes.title, wt.title2, wes.rank,
--val1 = BeginCell - ABS(wes.value), 
--val2 = BeginCell - ABS(wes.value) + case when wes.Value < 0 then wes.Value else 0 end,
--val3 = BeginCell - ABS(wes.value) + case when wes.Value < 0 then wes.Value else 0 end + ISNULL(c.Value,0),
val4 = BeginCell - ABS(wes.value) + case when wes.Value < 0 then wes.Value else 0 end + ISNULL(c.Value,0) + ISNULL(d.value,0)

from WaterfallTop (@ListName,@StudyYear,@ParentList, @DataType, @BySite) wt
left join (select * from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite)) wes on wes.unitname = wt.Title2
left join (select a.unitname, a.Rank, Value = SUM(b.value)
			from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite) a
			left join (select * from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite)) b on  b.unitname = a.unitname where b.Rank < a.Rank and b.Value < 0
			group by a.unitname, a.Rank) c on c.unitname = wes.unitname and c.Rank = wes.Rank
left join (select a.unitname, a.Rank, Value = SUM(b.value)
			from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite) a
			left join (select * from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite)) b on  b.unitname = a.unitname where b.Rank < a.Rank and b.Value > 0
			group by a.unitname, a.Rank) d on d.unitname = wes.unitname and d.Rank = wes.Rank




	--select top 1000 unitname, title, rank, startpoint = sum(isnull(f1,0) + isnull(f2,0) + isnull(f3,0) + isnull(f4,0) + isnull(f5,0)) from (

	--	select unitname, title, value, sortvalue, Rank, f1, f2, f3, f4, f5 = SUM(c5val) from (

	--		select unitname, title, value, sortvalue, Rank, f1, f2, f3, f4 = SUM(f4), c5unit, c5rank, c5val from (

	--			select c.unitname, c.title, value = MAX(c.value),sortvalue = MAX(c.sortvalue), c.Rank, f1 = MAX(tp.f1), f2 = SUM(case when c2.rank <= c.rank then ABS(c2.value) else 0 end) * -1 
	--				,f3 = SUM(case when c2.rank <= c.rank then case when c2.Value < 0 then c2.Value else 0 end else 0 end)
	--				,f4 = case when c.Value < 0 then 0 else ABS(c4.Value) end
	--				,c5unit = c5.unitname,c5rank = c5.Rank, c5val = ABS(c5.Value)
	--			from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite) c
	--				left join (select title, f1 = SUM(BeginCell) FROM WaterfallTop(@ListName,@StudyYear,@ParentList, @DataType, @BySite) group by title) tp on tp.title = c.unitname
	--				left join (select * from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite)) c2 on c2.unitname = c.unitname
	--				left join (select unitname, maxnegrank = MAX(rank) from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite) where Value < 0 group by unitname) c3 on c3.unitname = c.unitname
	--				left join (select unitname, Value, rank from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite)) c4 on c4.unitname = c.unitname and c4.Rank >= maxnegrank and c4.Rank < c.Rank
	--				left join (select unitname, Value, rank from WaterfallEventSummary(@ListName,@StudyYear,@ParentList, @DataType, @BySite)) c5 on c5.unitname = c.unitname and c5.Rank < c.Rank and c5.Rank <> maxnegrank
	--			group by c.unitname, c.title, c.Rank, c3.maxnegrank, c4.unitname, c4.Rank,c4.Value,c.value, c5.unitname, c5.Rank, c5.Value	) d

	--		group by unitname, title, value, sortvalue, Rank, f1, f2, f3, c5unit, c5rank, c5val) e

	--	group by unitname, title, value, sortvalue, Rank, f1, f2, f3, f4) f

	--group by unitname, title, rank

	--order by unitname, Rank

)



