﻿


CREATE FUNCTION [dbo].[P6]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@ProcessType VARCHAR(30), -- should be Boiler, CTG, Other, or Turbine
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(


SELECT UnitLabel, ROSMaint, Maint FROM P6New(@ListName, @StudyYear, @FuelGroup, @ProcessType, @ShortListName, @CurrencyCode) 


--select top 1000 unitLabel, ROSMaint, Maint FROM (
--	SELECT UnitLabel = CASE WHEN n.Num = 1 THEN 'Study' ELSE 'Company' END,
--		ROSMaint = CASE WHEN n.Num = 1 THEN SUM(CASE WHEN x.ROS = 'Y' THEN 
--						CASE LEFT(@ProcessType,3)
--							WHEN 'Boi' THEN s.BoilerMaintKUS WHEN 'CTG' THEN s.CTGMaintKUS WHEN 'Oth' THEN s.OtherMaintKUS WHEN 'Tur' THEN s.TurbineMaintKUS 
--						END  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
--					ELSE 0 END)/SUM(CASE WHEN x.ROS = 'Y' THEN CASE WHEN RIGHT(@ProcessType,3) = 'EGC' THEN gtc.EGC ELSE g.NMC2Yr END ELSE 0.001 END)
--					ELSE 0 END,
--		Maint = CASE WHEN n.Num = 2 THEN SUM(CASE WHEN x.ROS = 'N' THEN 
--						CASE LEFT(@ProcessType,3)
--							WHEN 'Boi' THEN s.BoilerMaintKUS WHEN 'CTG' THEN s.CTGMaintKUS WHEN 'Oth' THEN s.OtherMaintKUS WHEN 'Tur' THEN s.TurbineMaintKUS 
--						END  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
--					ELSE 0 END)/SUM(CASE WHEN x.ROS = 'N' THEN CASE WHEN RIGHT(@ProcessType,3) = 'EGC' THEN gtc.EGC ELSE g.NMC2Yr END ELSE 0.001 END)
--					ELSE 0 END,
--		Sortorder = case when n.Num = 1 then 1 else 2 end

--	FROM PowerGlobal.dbo.Numbers n
--		LEFT JOIN TwoPack s ON 1=1 --replaced SixPack
--		LEFT JOIN TSort t ON t.Refnum = s.Refnum
--		LEFT JOIN NERCFactors g ON g.Refnum = s.Refnum
--		LEFT JOIN Breaks b ON b.Refnum = s.Refnum
--		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = s.Refnum
--		LEFT JOIN 
--			(SELECT Refnum, ROS = 'Y' FROM _RL WHERE ListName = @ShortListName AND Refnum NOT IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
--			UNION
--			SELECT Refnum, ROS = 'N' FROM _RL WHERE ListName = @ListName AND Refnum IN (SELECT Refnum FROM TSort WHERE StudyYear = @StudyYear)) x ON x.Refnum = s.Refnum
--	WHERE n.Num <= 2 AND 
--	(
--		(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
--		(@FuelGroup = 'Gas' AND b.SteamGasOil = 'Y') OR
--		(@FuelGroup = 'CE' AND b.CRVSizeGroup LIKE 'CE%') OR
--		(@FuelGroup = 'CG' AND b.CRVSizeGroup LIKE 'CG%') OR
--		(@FuelGroup = 'SC' AND b.CRVSizeGroup = 'SC')
--	)

--	GROUP BY n.Num

--	union 
--	(select '',0,0,3)
--	union 
--	(select 'Study',0,0,4)
--	union 
--	(select 'Company',0,0,5)
--	union 
--	(select '',0,0,6)
--	union 
--	(select 'Study',0,0,7)
--	union 
--	(select 'Company',0,0,8)

--	) d
--	order by Sortorder

)



