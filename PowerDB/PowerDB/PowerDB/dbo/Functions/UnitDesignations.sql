﻿


CREATE FUNCTION [dbo].[UnitDesignations]
(	
	@ListName VARCHAR(30),
	@StudyYear INT
)
RETURNS TABLE 
AS
RETURN 
(


select top 1000 Units = '<FMT colWidth = 300>' + CHAR(149) + ' ' + rtrim(s.SiteName) + ' – ' + rtrim(s.SiteLabel) + case when UnitLabels = s.SiteLabel THEN '' ELSE ' – ' + UnitLabels END
	--+ case when unit1 is null then '' else rtrim(Unit1) end 
	--+ case when unit2 is null then '' else ', ' + rtrim(Unit2) end 
	--+ case when unit3 is null then '' else ', ' + rtrim(Unit3) end 
from StudySites s
left join (
	select SiteID, --[1] AS Unit1, [2] AS Unit2, [3] AS Unit3
		UnitLabels = 
		case when [1] is null then '' else rtrim([1]) end 
		+ case when [2] is null then '' else ', ' + rtrim([2]) end 
		+ case when [3] is null then '' else ', ' + rtrim([3]) end 
		+ case when [4] is null then '' else ', ' + rtrim([4]) end 
		+ case when [5] is null then '' else ', ' + rtrim([5]) end 
		+ case when [6] is null then '' else ', ' + rtrim([6]) end 
		+ case when [7] is null then '' else ', ' + rtrim([7]) end 
		+ case when [8] is null then '' else ', ' + rtrim([8]) end 
		+ case when [9] is null then '' else ', ' + rtrim([9]) end 
	FROM
	(
	select SiteID, UnitLabel, ROW_NUMBER() OVER (PARTITION BY SiteID ORDER BY SiteID) as RowNum
	from TSort t where t.Refnum in (select Refnum from _RL where ListName = @ListName)
	and t.EvntYear = @StudyYear
	) a
	PIVOT (MAX(UnitLabel) FOR RowNum IN ([1],[2],[3],[4],[5],[6],[7],[8],[9])) As pvt --will they have more than 9 units at one site? doubtful
) b on b.SiteID = s.SiteID
where s.SiteID in (select SiteID from TSort where Refnum in (select Refnum from _RL where ListName = @ListName) and EvntYear = @StudyYear)
order by case when s.SiteID like '%CC%' then 1 else 0 end, s.SiteName



)



