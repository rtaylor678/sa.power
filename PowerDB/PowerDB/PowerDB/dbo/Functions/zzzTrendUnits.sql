﻿

CREATE FUNCTION [dbo].[zzzTrendUnits]
(	
	@ListName VARCHAR(30),
	@PeerGroup VARCHAR(30),
	@RankVariableID VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(
--select 'Year' = '2008A', Q12, Q34 from Quartiles ('Power08',2008,'Fuel Group',46) where CRVSizeGroup = 'Coal'
--union all
--select '2009', Q12, Q34 from Quartiles ('Power09',2009,'Fuel Group',46) where CRVSizeGroup = 'Coal'
--union all
--select '2010', Q12, Q34 from Quartiles ('Power10',2010,'Fuel Group',46) where CRVSizeGroup = 'Coal'
--union all
--select '2011', Q12, Q34 from Quartiles ('Power11',2011,'Fuel Group',46) where CRVSizeGroup = 'Coal'
--union all
--select '2012', Q12, Q34 from Quartiles ('Power12',2012,'Fuel Group',46) where CRVSizeGroup = 'Coal'
--union all
--select '2013', Q12, Q34 from Quartiles ('Power13',2013,'Fuel Group',46) where CRVSizeGroup = 'Coal'

--select Year, Q1 = Q12, Q4 = Q34 from SlideQuartiles where ListName = @ListName and PeerGroup = @PeerGroup AND RankVariableID = @RankVariableID

SELECT UnitType, SiteLabel, [2009], [2010], [2011], [2012], [2013], [2014] FROM (
SELECT s.SiteLabel, t.studyyear, EFOR = CASE WHEN SUM(c.EFORWtFactor) <> 0 THEN SUM(ISNULL(c.EFOR*c.EFORWtFactor,0))/SUM(c.EFORWtFactor) END,
UnitType = CASE b.FuelGroup WHEN 'Coal' THEN 'Coal' ELSE CASE b.CombinedCycle WHEN 'N' THEN 'GO' ELSE CASE b.CogenElec WHEN 'Elec' THEN 'CE' ELSE 'CG' END END END
FROM NERCFactors c LEFT JOIN TSort t ON t.Refnum = c.Refnum LEFT JOIN StudySites s ON s.SiteID = t.SiteID LEFT JOIN Breaks b ON b.Refnum = t.Refnum 
WHERE t.Refnum in (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear >= 2009 and b.FuelGroup = 'Coal' GROUP BY s.SiteLabel , t.StudyYear, b.FuelGroup, b.CombinedCycle, b.CogenElec
) src
PIVOT (SUM(EFOR) FOR Studyyear IN ([2009],[2010],[2011],[2012],[2013],[2014])) pvt


)


