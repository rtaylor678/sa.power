﻿





CREATE FUNCTION [dbo].[MaintCostEGC]
(	
	@ListName VARCHAR(30),
	@StudyYear INT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 t.UnitLabel, a.AnnMaintCostEGC
	FROM MaintTotCalc a 
		INNER JOIN GenSum g ON a.Refnum = g.Refnum
		INNER JOIN Breaks b on a.Refnum = b.Refnum 
		INNER JOIN Tsort t on t.Refnum = a.Refnum
	WHERE a.refnum in (select refnum from Breaks where Refnum in (select refnum from _rl where listname = @ListName)) and t.studyyear = @StudyYear
	order by a.AnnMaintCostEGC




)






