﻿


CREATE FUNCTION [dbo].[zzzRepEventsByValueTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30), --coal or gas & oil
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT '<FMT colWidth=106 LeftMargin=7 ColorFont=98002E><b>Cause Code</b>' AS CauseCode,
		'<FMT LeftMargin=7># Events' AS NumEvents,
		'<FMT LeftMargin=7>Total LRO, k ' + RTRIM(@CurrencyCode) AS TotalLRO,
		'<FMT LeftMargin=7># Plants' AS NumSites,
		'<FMT LeftMargin=7># Units' AS NumUnits,
		'' AS PresDescription

	UNION ALL

	SELECT '<FMT LeftMargin=7 ColorFont=98002E>' + '<b>' + CAST(ISNULL(CauseCode,'') AS varchar) + '</b>', 
		'<FMT LeftMargin=7>' + CAST(NumEvents AS varchar), 
		'<FMT LeftMargin=7>' + FORMAT(TotalLRO,'#,##0'),
		'<FMT LeftMargin=7>' + CAST(NumSites AS varchar), 
		'<FMT LeftMargin=7>' + CAST(NumUnits AS varchar), 
		'<FMT LeftMargin=7>' + PresDescription
	FROM PowerGlobal.dbo.Numbers num
		LEFT JOIN (

			SELECT CauseCode = Cause_Code, 
				NumEvents = COUNT(*),
				TotalLRO = SUM(lro),
				NumSites = COUNT(distinct SiteLabel),
				NumUnits = COUNT(distinct UnitLabel),
				PresDescription,
				Rank = RANK() OVER (ORDER BY SUM(LRO)/SUM(LostMWH)*1000 DESC)
			FROM RepEventsSource(@ListName, @StudyYear) 
			WHERE FuelGroup = @FuelGroup
			GROUP BY Cause_Code, PresDescription
			HAVING COUNT(RefEventNum) >1

		) b ON b.Rank = num.Num
	WHERE num.Num <= 7

)

