﻿




CREATE FUNCTION [dbo].[PlantActionEGC]
(	
	@ListName VARCHAR(30),
	@StudyYear INT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 t.UnitLabel, (o.SiteWagesBen + o.CentralContract + o.MaintMatl + o.OverhaulAdj + o.LTSAAdj + o.OthVar + o.Misc) as PMEEGC
	FROM TSort t 
	inner Join PlantActionExp o ON o.Refnum = t.Refnum AND o.DataType = 'EGC'
	inner join Breaks b on b.Refnum = t.Refnum 
	inner join Gensum g on g.Refnum = t.Refnum
	WHERE t.studyyear = @StudyYear AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
	ORDER BY PMEEGC 



)





