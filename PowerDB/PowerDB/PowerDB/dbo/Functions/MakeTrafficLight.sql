﻿



CREATE FUNCTION [dbo].[MakeTrafficLight]
(	
@SourceData TrafficLightTable READONLY,
@PercentileOrTile char(1) -- pass P or T
)
RETURNS @T TABLE (UnitLabel VARCHAR(100), Col1 VARCHAR(200), Col2 VARCHAR(100), Col3 VARCHAR(100), Col4 VARCHAR(100), Col5 VARCHAR(100), Col6 VARCHAR(100))
AS
BEGIN


--DECLARE @RankVariable1 INT
--DECLARE @RankVariable2 INT
--DECLARE @RankVariable3 INT
--SET @RankVariable1 = case @RankVariableGroup when 1 then 46 else 0 end
--SET @RankVariable2 = case @RankVariableGroup when 1 then 47 else 0 end
--SET @RankVariable3 = case @RankVariableGroup when 1 then 49 else 0 end

--DECLARE @temp TABLE (UnitLabel VARCHAR(100), Col1 VARCHAR(200), Col2 VARCHAR(100), Col3 VARCHAR(100), Col4 VARCHAR(100), Col5 VARCHAR(100), Col6 VARCHAR(100))--, Rownum Float)

--INSERT INTO @temp (Unitlabel, Col1, Col2, Col3, Col4, Col5, Col6)
--SELECT UnitLabel, Col1, Col2, Col3, Col4, Col5, Col6 FROM @SourceData

DECLARE @rowcounter INT = 0

DECLARE @MyCursor CURSOR;
--DECLARE @MyField YourFieldDataType;
DECLARE @UnitLabel VARCHAR(100), @Col1 VARCHAR(200), @Col2 VARCHAR(100), @Col3 VARCHAR(100), @Col4 VARCHAR(100), @Col5 VARCHAR(100), @Col6 VARCHAR(100)
BEGIN
    SET @MyCursor = CURSOR FOR select * from @SourceData

    OPEN @MyCursor 
    FETCH NEXT FROM @MyCursor 
    INTO @UnitLabel, @Col1, @Col2, @Col3, @Col4, @Col5, @Col6

	INSERT INTO @t (Unitlabel, Col1, Col2, Col3, Col4, Col5, Col6) values (@UnitLabel, @Col1, @Col2, @Col3, @Col4, @Col5, @Col6)

	set @rowcounter = @rowcounter + 1
    WHILE @@FETCH_STATUS = 0
    BEGIN

      FETCH NEXT FROM @MyCursor 
      INTO @UnitLabel, @Col1, @Col2, @Col3, @Col4, @Col5, @Col6 

	  set @rowcounter = @rowcounter + 1

	  if @rowcounter > 2
	  begin
		INSERT INTO @t (Unitlabel, Col1, Col2, Col3, Col4, Col5, Col6) values (null, null, null, null, null, null, null)

	  end

		INSERT INTO @t (Unitlabel, Col1, Col2, Col3, Col4, Col5, Col6) values (@UnitLabel, @Col1, @Col2, @Col3, @Col4, @Col5, @Col6)

    END

    CLOSE @MyCursor
    DEALLOCATE @MyCursor
END












--INSERT INTO @t (Unitlabel, Col1, Col2, Col3, Col4, Col5, Col6)
--SELECT UnitLabel, Col1, Col2, Col3, Col4, Col5, Col6 FROM @SourceData

update @T set UnitLabel = '<FMT colWidth=50 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(UnitLabel) + '</b>' where RTRIM(unitlabel) <> ''
update @T set UnitLabel = '<FMT colWidth=250 ColorFont=98002E Align=Center>' + '<b>' + RTRIM(UnitLabel) + '</b>' where RTRIM(unitlabel) = ''
update @T set UnitLabel = '<FMT colWidth=10>' where UnitLabel is null

update @T set Col1 = '<FMT fontHeight=10 rowHeight=10>' + RTRIM(Col1) where ISNUMERIC(col1) = 0
update @T set Col1 = '<FMT Align=Center fontHeight=16 AllBorders=true Color=' + case when Col1 >= 75 then '00B050' else case when Col1 >= 50 then 'FFD966' else case when Col1 >=25 then 'F79646' else 'C00000' end end end + '>' + RTRIM(Col1) where ISNUMERIC(col1) = 1

update @T set Col2 = '<FMT rowHeight=10>' where Col2 is null
update @T set Col4 = '<FMT rowHeight=10>' where Col4 is null
update @T set Col6 = '<FMT rowHeight=10>' where Col6 is null



--update @T set Col1 = '<FMT Align=Center Color=' + case when ISNUMERIC(col1) = 0 then rtrim(Col1) else case when Col1 >= 75 then '00FF00' else '555555' end end + '>' + RTRIM(Col1)






--select UnitLabel = '',
--	col1 = '' + CASE @RankVariable1
--					WHEN 1 THEN 'HeatRate'
--					WHEN 6 THEN 'AnnMaintCostMWH'
--					WHEN 7 THEN 'AnnOHCostMWH'
--					WHEN 8 THEN 'AnnNonOHCostMWH'
--					WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
--					WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
--					WHEN 49 THEN 'Planned Commercial Unavailability Index'
--					WHEN 55 THEN 'InternalLessScrubberPcnt'
--					WHEN 78 THEN 'ThermEff'
--					WHEN 81 THEN 'AnnLTSACostMWH'
--					WHEN 86 THEN 'TotCashLessFuelEm'
--					WHEN 88 THEN 'TotCashLessFuelEm'
--					ELSE '' END,
--						col2 = CASE @RankVariable2
--					WHEN 1 THEN 'HeatRate'
--					WHEN 6 THEN 'AnnMaintCostMWH'
--					WHEN 7 THEN 'AnnOHCostMWH'
--					WHEN 8 THEN 'AnnNonOHCostMWH'
--					WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
--					WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
--					WHEN 49 THEN 'Planned Commercial Unavailability Index'
--					WHEN 55 THEN 'InternalLessScrubberPcnt'
--					WHEN 78 THEN 'ThermEff'
--					WHEN 81 THEN 'AnnLTSACostMWH'
--					WHEN 86 THEN 'TotCashLessFuelEm'
--					WHEN 88 THEN 'TotCashLessFuelEm'
--					ELSE '' END,
--						col3 = CASE @RankVariable3
--					WHEN 1 THEN 'HeatRate'
--					WHEN 6 THEN 'AnnMaintCostMWH'
--					WHEN 7 THEN 'AnnOHCostMWH'
--					WHEN 8 THEN 'AnnNonOHCostMWH'
--					WHEN 46 THEN 'Unplanned Commercial Unavailability, %'
--					WHEN 47 THEN 'Unplanned Commercial Unavailability Index'
--					WHEN 49 THEN 'Planned Commercial Unavailability Index'
--					WHEN 55 THEN 'InternalLessScrubberPcnt'
--					WHEN 78 THEN 'ThermEff'
--					WHEN 81 THEN 'AnnLTSACostMWH'
--					WHEN 86 THEN 'TotCashLessFuelEm'
--					WHEN 88 THEN 'TotCashLessFuelEm'
--					ELSE '' END,
--					col4 = null,
--					col5 = null,
--					col6 = null

--union

--select t.UnitLabel, CAST(rv1.Percentile AS varchar), CAST(rv2.Percentile AS varchar), CAST(rv3.Percentile as varchar), null, null, null
--from TSort t
--left join _RankView rv1 on rv1.RankVariableID = @RankVariable1 and rv1.Refnum = t.Refnum and rv1.ListName = @ParentList and rv1.BreakCondition = @BreakCondition
--left join _RankView rv2 on rv2.RankVariableID = @RankVariable2 and rv2.Refnum = t.Refnum and rv2.ListName = @ParentList and rv2.BreakCondition = @BreakCondition
--left join _RankView rv3 on rv3.RankVariableID = @RankVariable3 and rv3.Refnum = t.Refnum and rv3.ListName = @ParentList and rv3.BreakCondition = @BreakCondition
--where t.Refnum in (select Refnum from _RL where ListName = @ListName)
--and t.StudyYear = @StudyYear
--and rv1.BreakValue = @CRVGroup

RETURN


END
