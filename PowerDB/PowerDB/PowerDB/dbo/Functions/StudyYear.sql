﻿CREATE FUNCTION dbo.StudyYear(@Refnum Refnum)
RETURNS smallint
AS
BEGIN
	DECLARE @StudyYear smallint
	SELECT @StudyYear = StudyYear FROM TSort WHERE Refnum = @Refnum
	RETURN @StudyYear
END
