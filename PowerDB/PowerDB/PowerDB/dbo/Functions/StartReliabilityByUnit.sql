﻿

CREATE FUNCTION [dbo].[StartReliabilityByUnit]
(	
	@ListName VARCHAR(30),
	@StudyYear INT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 
		c.SiteName, 
		d.Label, 
		Success = CONVERT(DECIMAL(5,2),d.Success)/CONVERT(DECIMAL(5,2),d.Total) * 100, 
		Forced = CONVERT(DECIMAL(5,2),d.Forced)/CONVERT(DECIMAL(5,2),d.Total) * 100, 
		Derates = CONVERT(DECIMAL(5,2),d.Derates)/CONVERT(DECIMAL(5,2),d.Total) * 100, 
		Maintenance = CONVERT(DECIMAL(5,2),d.Maintenance)/CONVERT(DECIMAL(5,2),d.Total) * 100, 
		[Startup Failure] = CONVERT(DECIMAL(5,2),d.[Startup Failure])/CONVERT(DECIMAL(5,2),d.Total) * 100
	FROM (

		SELECT SiteName, n.Num
			FROM PowerGlobal.dbo.Numbers n
				FULL OUTER JOIN (SELECT DISTINCT SiteName
					FROM TSort t
						LEFT JOIN ROODs r ON r.Refnum = t.Refnum
						LEFT JOIN StudySites s ON s.SiteID = t.SiteID
					WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
						AND t.StudyYear = @StudyYear
						AND r.Start_Date = ('1/1/' + CAST(@StudyYear AS Varchar)) AND r.End_Date = ('12/31/' + CAST(@StudyYear AS varchar))
					) b ON 1=1
			) c
		LEFT JOIN (

		SELECT TOP 1000 s.SiteName, 
			Label = RTRIM(t.UnitLabel) + CASE WHEN RTRIM(r.TurbineID) <> 'STG' AND RTRIM(r.TurbineID) <> RTRIM(t.UnitLabel) THEN ' - ' + RTRIM(r.TurbineID) ELSE '' END + ' (' + CAST(ISNULL(r.Success,0) + ISNULL(r.Forced,0) + ISNULL(r.Derate,0) + ISNULL(r.Maint,0) + ISNULL(r.Startup,0) AS varchar) + ' starts)',
			Success = ISNULL(r.Success,0),
			Forced = ISNULL(r.Forced,0),
			Derates = ISNULL(r.Derate,0),
			Maintenance = ISNULL(r.Maint,0),
			[Startup Failure]= ISNULL(r.Startup,0),
			Total = ISNULL(r.Success,0) + ISNULL(r.Forced,0) + ISNULL(r.Derate,0) + ISNULL(r.Maint,0) + ISNULL(r.Startup,0),
			Rank = RANK() OVER (PARTITION BY s.SiteName ORDER BY RTRIM(t.UnitLabel) + CASE WHEN RTRIM(r.TurbineID) <> 'STG' AND RTRIM(r.TurbineID) <> RTRIM(t.UnitLabel) THEN ' - ' + RTRIM(r.TurbineID) ELSE '' END + ' (' + CAST(ISNULL(r.Success,0) + ISNULL(r.Forced,0) + ISNULL(r.Derate,0) + ISNULL(r.Maint,0) + ISNULL(r.Startup,0) AS varchar) + ' starts)' DESC)
		FROM TSort t
			LEFT JOIN ROODs r ON r.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
			AND t.StudyYear = @StudyYear
			AND r.Start_Date = ('1/1/' + CAST(@StudyYear AS Varchar)) AND r.End_Date = ('12/31/' + CAST(@StudyYear AS varchar))
		) d ON d.SiteName = c.SiteName AND d.Rank = c.Num

	where Rank IS NOT NULL OR Num <=4

	ORDER BY c.SiteName ASC, Rank, Num

)


