﻿

CREATE FUNCTION [dbo].[UnplanLROTreeTop]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30) --"", "Tech", "Coal", "SGO", "Elec","Cogen", Tech and blank return the same
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT LRO = SUM(LRO),LostMWh = SUM(LostMWH)
	FROM UnplanLROTreeSource (@ListName, @StudyYear)
	WHERE (
		(@TreeType = '' OR @TreeType = 'Tech') OR
		(@TreeType = 'Coal' AND FuelGroup = 'Coal' AND CombinedCycle = 'N') OR
		(@TreeType = 'SGO' AND FuelGroup = 'Gas & Oil' AND CombinedCycle = 'N') OR
		(@TreeType = 'Elec' AND CombinedCycle = 'Y' AND CogenElec = 'Elec') OR
		(@TreeType = 'Cogen' AND CombinedCycle = 'Y' AND CogenElec = 'Cogen')
		)

)


