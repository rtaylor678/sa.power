﻿



CREATE FUNCTION [dbo].[GetHeatRateColors]
(	
	@ShortListName VARCHAR(30),
	@ColorNumber INT,
	@Metric BIT

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 Label, Value = CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Value,'BTU','MJ') ELSE Value END
	FROM SlideSourceData 
	WHERE Source = @ShortListName AND Type = 'HeatRateColors' AND Filter1 = @ColorNumber ORDER BY CAST(Filter2 AS int)

)




