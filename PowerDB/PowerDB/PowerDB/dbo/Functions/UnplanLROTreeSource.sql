﻿

CREATE FUNCTION [dbo].[UnplanLROTreeSource]
(	
	@ListName VARCHAR(30),
	@StudyYear INT
	--@TreeType VARCHAR(30) --"Tech", "Coal", "SGO", "Elec","Cogen"

)
RETURNS TABLE 
AS
RETURN 
(
	--this is source data, you should use the function UnplanLROTree which pulls data from this

	SELECT TOP 10000 CauseCodes.SAIMajorEquip, 
		SAIMinorEquip = 
			CASE SAIMajorEquip 
				WHEN 'Balance of Plant' THEN 
					CASE WHEN CauseCodes.SAIMinorEquip IN ('Ash Handling Facilities','Baghouse','Dry Gas Scrubber (DGS)','Precipitators (PREC)','Selective Catalyst Reduction (SCR)',
							'Wet Gas Scrubber (WGS)') THEN 'Emission Controls'
						WHEN CauseCodes.SAIMinorEquip IN ('Site Transformer') THEN 'Other'
						ELSE SAIMinorEquip 
					END
				WHEN 'Boiler' THEN 
					CASE WHEN ccl.level2 IN ('Slag and Ash Removal','Heater Drain Systems','Boiler Control Systems','Boiler Fuel Supply from Bunkers to Boiler','Boiler Internals and Structures',
							'Boiler Overhaul and Inspections','Miscellaneous (Boiler)','Miscellaneous Boiler Tube Problems') THEN 'Boiler Misc'
						WHEN ccl.level2 IN ('Condensate System','Feedwater System','Boiler Piping System','Boiler Water Condition','HRSG Boiler Piping System') THEN 'Steam/Feedwater'
						WHEN ccl.level2 IN ('Boiler Air and Gas Systems','Boiler Design Limitations','Boiler Tube Fireside Slagging or Fouling','Miscellaneous (HRSG Boiler)') THEN 'Air/Gas/Slag'
						WHEN ccl.level2 IN ('Boiler Tube Leaks','HRSG Boiler Tube Leaks') THEN 'Tube Leaks'
						ELSE 'Boiler Misc' 
					END
				WHEN 'Combustion Turbine' THEN 
					CASE ccl.Level2
						WHEN 'Fuel, Ignition, and Combustion Systems' THEN 'Combustion'
						WHEN 'Inlet Air System and Compressors' THEN 'Inlet'
						WHEN 'Turbine' THEN 'Turbine'
						ELSE 'Other' 
					END
				ELSE CauseCodes.SAIMinorEquip 
			END,
		b.FuelGroup, 
		CombinedCycle = CASE b.FuelGroup WHEN 'Coal' THEN 'N' ELSE b.CombinedCycle END, 
		CogenElec = CASE b.FuelGroup WHEN 'Coal' THEN NULL ELSE b.CogenElec END,
		LRO = SUM(ISNULL(lro.LRO,0)), 
		LostMWH = SUM(ISNULL(LOSTMW,0)*ISNULL(PeakHrs,0))
	FROM TSort t
		INNER JOIN Breaks b ON b.Refnum = t.Refnum
		INNER JOIN Events e ON e.Refnum = t.Refnum 
		INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
		INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
		INNER JOIN CauseCodeLevels ccl ON ccl.CauseCode = CauseCodes.Cause_Code
	WHERE lro.LRO >0 
		AND e.CAUSE_CODE <> 7777
		AND t.studyyear = @StudyYear
		AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName) 
		AND e.EVNT_Category in ('F', 'M')
		AND t.Refnum NOT LIKE '%G'
	GROUP BY CauseCodes.SAIMajorEquip, 
		CASE SAIMajorEquip 
				WHEN 'Balance of Plant' THEN 
					CASE WHEN CauseCodes.SAIMinorEquip IN ('Ash Handling Facilities','Baghouse','Dry Gas Scrubber (DGS)','Precipitators (PREC)','Selective Catalyst Reduction (SCR)',
							'Wet Gas Scrubber (WGS)') THEN 'Emission Controls'
						WHEN CauseCodes.SAIMinorEquip IN ('Site Transformer') THEN 'Other'
						ELSE SAIMinorEquip 
					END
				WHEN 'Boiler' THEN 
					CASE WHEN ccl.level2 IN ('Slag and Ash Removal','Heater Drain Systems','Boiler Control Systems','Boiler Fuel Supply from Bunkers to Boiler','Boiler Internals and Structures',
							'Boiler Overhaul and Inspections','Miscellaneous (Boiler)','Miscellaneous Boiler Tube Problems') THEN 'Boiler Misc'
						WHEN ccl.level2 IN ('Condensate System','Feedwater System','Boiler Piping System','Boiler Water Condition','HRSG Boiler Piping System') THEN 'Steam/Feedwater'
						WHEN ccl.level2 IN ('Boiler Air and Gas Systems','Boiler Design Limitations','Boiler Tube Fireside Slagging or Fouling','Miscellaneous (HRSG Boiler)') THEN 'Air/Gas/Slag'
						WHEN ccl.level2 IN ('Boiler Tube Leaks','HRSG Boiler Tube Leaks') THEN 'Tube Leaks'
						ELSE 'Boiler Misc' 
					END
			WHEN 'Combustion Turbine' THEN 
				CASE ccl.Level2
					WHEN 'Fuel, Ignition, and Combustion Systems' THEN 'Combustion'
					WHEN 'Inlet Air System and Compressors' THEN 'Inlet'
					WHEN 'Turbine' THEN 'Turbine'
					ELSE 'Other' 
				END
			ELSE CauseCodes.SAIMinorEquip 
		END,
		b.FuelGroup, 
		b.CombinedCycle, 
		b.CogenElec
	ORDER BY CombinedCycle, CogenElec,FuelGroup, SAIMajorEquip, SAIMinorEquip


)


