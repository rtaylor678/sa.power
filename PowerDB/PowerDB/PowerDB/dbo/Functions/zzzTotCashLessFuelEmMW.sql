﻿

CREATE FUNCTION [dbo].[zzzTotCashLessFuelEmMW]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT UnitLabel = 'Group Avg',
		'Plant-Manageable' = GlobalDB.dbo.WtAvg(o.ActCashLessFuel, gtc.AdjNetMWH),
		'Taxes' = GlobalDB.dbo.WtAvg(o.PropTax + o.OthTax, gtc.AdjNetMWH),
		'Insurance' = GlobalDB.dbo.WtAvg(o.Insurance, gtc.AdjNetMWH),
		'A&G Personnel Costs' = GlobalDB.dbo.WtAvg(o.AGPers, gtc.AdjNetMWH),
		'A&G Non-Personnel Costs' = GlobalDB.dbo.WtAvg(o.AGNonPers, gtc.AdjNetMWH)
	FROM TSort t
		LEFT JOIN OpexCalc o ON o.Refnum = t.Refnum AND o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13')
		AND b.CRVGroup = @CRVGroup

	UNION ALL

	SELECT UnitLabel = 'Pace- setter',
		'Plant-Manageable' = GlobalDB.dbo.WtAvg(o.ActCashLessFuel, gtc.AdjNetMWH),
		'Taxes' = GlobalDB.dbo.WtAvg(o.PropTax + o.OthTax, gtc.AdjNetMWH),
		'Insurance' = GlobalDB.dbo.WtAvg(o.Insurance, gtc.AdjNetMWH),
		'A&G Personnel Costs' = GlobalDB.dbo.WtAvg(o.AGPers, gtc.AdjNetMWH),
		'A&G Non-Personnel Costs' = GlobalDB.dbo.WtAvg(o.AGNonPers, gtc.AdjNetMWH)
	FROM TSort t
		LEFT JOIN OpexCalc o ON o.Refnum = t.Refnum AND o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13 Pacesetters 2')
		AND b.CRVGroup = @CRVGroup

	UNION ALL

	SELECT UnitLabel = 'Co Avg',
		'Plant-Manageable' = GlobalDB.dbo.WtAvg(o.ActCashLessFuel, gtc.AdjNetMWH),
		'Taxes' = GlobalDB.dbo.WtAvg(o.PropTax + o.OthTax, gtc.AdjNetMWH),
		'Insurance' = GlobalDB.dbo.WtAvg(o.Insurance, gtc.AdjNetMWH),
		'A&G Personnel Costs' = GlobalDB.dbo.WtAvg(o.AGPers, gtc.AdjNetMWH),
		'A&G Non-Personnel Costs' = GlobalDB.dbo.WtAvg(o.AGNonPers, gtc.AdjNetMWH)
	FROM TSort t
		LEFT JOIN OpexCalc o ON o.Refnum = t.Refnum AND o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CRVGroup = @CRVGroup

	UNION ALL

	SELECT TOP 1000 UnitLabel, --TOP 1000 here so that the ORDER BY will work in a TVF
		'Plant-Manageable' = o.ActCashLessFuel,
		'Taxes' = o.PropTax + o.OthTax,
		'Insurance' = o.Insurance,
		'A&G Personnel Costs' = o.AGPers,
		'A&G Non-Personnel Costs' = o.AGNonPers
	FROM TSort t
		LEFT JOIN OpexCalc o ON o.Refnum = t.Refnum AND o.DataType = 'KW'
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CRVGroup = @CRVGroup
	ORDER BY o.TotCashLessFuelEm DESC


)


