﻿



CREATE FUNCTION [dbo].[BarTest]
(	
	--@ListName VARCHAR(30),
	--@StudyYear INT,
	--@FuelGroup VARCHAR(30),
	--@Location VARCHAR(30),
	--@CurrencyCode VARCHAR(4),
	--@Metric BIT
	----@PeerGroup1 VARCHAR(50),
	----@PeerGroup2 VARCHAR(50),
	----@PeerGroup3 VARCHAR(50)

)
RETURNS TABLE 
AS
RETURN 
(

	--select subtitle = 'Group1: Tesoro',PresLa = 'TS',MaintCap = 221, Discretion = 733, NonDisc = 129, OH = 7
	--union
	--select  'Group1: Tesoro','NAEUR',182,334,82,7
	--union
	--select  'Group1: Tesoro','CC',182,334,82,7
	--union
	--select  'Group1: Tesoro','Co',182,334,82,7
	--union
	--select  'Group1: Tesoro','Ha',182,334,82,7
	--union
	--select  'Group2: Tesoro','TS',182,334,82,7
	--union
	--select  'Group2: Tesoro','NAEUR',182,334,82,7
	--union
	--select  'Group2: Tesoro','CC',182,334,82,7
	--union
	--select  'Group2: Tesoro','Co',182,334,82,7
	--union
	--select  'Group2: Tesoro','Ha',182,334,82,7

select subtitle = 'Group1:Tesoro', PresLabel = 'TS', [Maintenance Capital] = 221.6653, Discretionary = 733.5708, [Non-Discretionary] = 129.2035, Overhead = 7.789655 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'NAEUR', [Maintenance Capital] = 182.9003, Discretionary = 334.2103, [Non-Discretionary] = 82.11878, Overhead = 7.726374 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'CC', [Maintenance Capital] = 395.7776, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'Co', [Maintenance Capital] = 67.9689, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'Ha', [Maintenance Capital] = 0, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'Hy', [Maintenance Capital] = 76.26857, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'LB', [Maintenance Capital] = 480.9674, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'CM2', [Maintenance Capital] = 578.1086, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'CM3', [Maintenance Capital] = 0, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'SD', [Maintenance Capital] = 8.171557, Discretionary = 433.6344, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'Vi', [Maintenance Capital] = 323.6503, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'Wi', [Maintenance Capital] = 0, Discretionary = 3166.552, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group1:Tesoro', PresLabel = 'CP', [Maintenance Capital] = 768.8691, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'TS', [Maintenance Capital] = 221.6653, Discretionary = 733.5708, [Non-Discretionary] = 129.2035, Overhead = 7.789655 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'NAEUR', [Maintenance Capital] = 182.9003, Discretionary = 334.2103, [Non-Discretionary] = 82.11878, Overhead = 7.726374 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Am', [Maintenance Capital] = 22.94255, Discretionary = 29.97768, [Non-Discretionary] = 855.2891, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'An', [Maintenance Capital] = 57.79263, Discretionary = 13.30751, [Non-Discretionary] = 0, Overhead = 23.1931 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Bo', [Maintenance Capital] = 0, Discretionary = 0.664012, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Bu', [Maintenance Capital] = 0, Discretionary = 3.588372, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Ni', [Maintenance Capital] = 161.4296, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Pa', [Maintenance Capital] = 0, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Po', [Maintenance Capital] = 0, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'PAn', [Maintenance Capital] = 1962.851, Discretionary = 0, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'SLC', [Maintenance Capital] = 230.5602, Discretionary = 1062.126, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'St', [Maintenance Capital] = 90.84401, Discretionary = 8649.373, [Non-Discretionary] = 0, Overhead = 0 union 
select subtitle = 'Group2:Tesoro', PresLabel = 'Va', [Maintenance Capital] = 34.40893, Discretionary = 1138.286, [Non-Discretionary] = 0, Overhead = 0  

--	SELECT Label = 'test1', UnitLabel, FuelCost = CASE @Metric WHEN 1 THEN GlobalDB.dbo.UnitsConv(FuelCost,'MBTU','GJ') ELSE FuelCost END
--	FROM (
--		SELECT t.UnitLabel, FuelCost = (CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
--		FROM TSort t
--			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
--			LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
--			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
--			LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
--		WHERE t.studyyear = @StudyYear 
--			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
--			AND (
--				(@Location = 'N America' AND t.Continent = 'N America') OR
--				(@Location = 'Europe' AND t.Continent = 'Europe') OR
--				(@Location = 'World' AND t.Continent NOT IN ('N America','Europe'))
--				)
--			AND (
--				(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
--				(@FuelGroup <> 'Coal' AND f.FuelType IN ('Gas','Oil'))
--				)
--	) b

--union

--	SELECT Label = 'test2', UnitLabel, FuelCost = CASE @Metric WHEN 1 THEN GlobalDB.dbo.UnitsConv(FuelCost,'MBTU','GJ') ELSE FuelCost END
--	FROM (
--		SELECT t.UnitLabel, FuelCost = (CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
--		FROM TSort t
--			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
--			LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
--			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
--			LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
--		WHERE t.studyyear = @StudyYear 
--			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
--			AND (
--				(@Location = 'N America' AND t.Continent = 'N America') OR
--				(@Location = 'Europe' AND t.Continent = 'Europe') OR
--				(@Location = 'World' AND t.Continent NOT IN ('N America','Europe'))
--				)
--			AND (
--				(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
--				(@FuelGroup <> 'Coal' AND f.FuelType IN ('Gas','Oil'))
--				)
--	) b


--union
--select Label = 'Test3', 'T3',FuelCost = null





)


