﻿



CREATE FUNCTION [dbo].[UnplanLROTreeFormatPivot]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30), --"", "Tech", "Coal", "SGO", "Elec","Cogen", Tech and blank return the same
	@CurrencyCode VARCHAR(4)
)
RETURNS TABLE 
AS
RETURN 
(

	select row1,
		row2,
		row3,
		row4 = '<FMT rowHeight=10 fontHeight=1' + case when sort = 27 then ' RightBorder=1' else '' end + case when sort between 4 and 51 then ' BottomBorder=1' else '' end + '>',
		row5 = '<FMT rowHeight=10 fontHeight=1' + case when sort in (3, 15, 27, 39, 51) then ' RightBorder=1' else '' end + '>',
		row6,
		row7,
		row8 = '<FMT rowHeight=10 fontHeight=1' + case when sort in (3, 15, 39, 51) then ' RightBorder=1' else '' end + '>',
		row9 = case when sort in (3, 15) then '<FMT RightBorder=1>'
				when sort in (4, 16, 52) then '<FMT BottomBorder=1>'
				when sort in (39, 51) then '<FMT RightBorder=1 BottomBorder=1>' else '' end + row9,
		row10 =  case when sort in (3, 15, 39, 51) then '<FMT RightBorder=1>' else '' end,
		row11 = '<FMT rowHeight=10 fontHeight=1' + case when sort in (3, 15, 39, 51) then ' RightBorder=1' else '' end + '>',
		row12 = case when sort in (3, 15) then '<FMT RightBorder=1>'
				when sort in (4, 16, 52) then '<FMT BottomBorder=1>'
				when sort in (39, 51) then '<FMT RightBorder=1 BottomBorder=1>' else '' end + row12,
		row13 =  case when sort in (3, 15, 39, 51) then '<FMT RightBorder=1>' else '' end,
		row14 = '<FMT rowHeight=10 fontHeight=1' + case when sort in (3, 15, 39, 51) then ' RightBorder=1' else '' end + '>',
		row15 = case when sort in (3, 15) then '<FMT RightBorder=1>'
				when sort in (4, 16) then '<FMT BottomBorder=1>'
				when sort in (39, 51) then '<FMT RightBorder=1 BottomBorder=1>' else '' end + row15,
		row16 =  case when sort in (3, 15, 39) then '<FMT RightBorder=1>' else '' end,
		row17 = '<FMT rowHeight=10 fontHeight=1' + case when sort in (3, 15, 39) then ' RightBorder=1' else '' end + '>',
		row18 = case when sort in (3, 15) then '<FMT RightBorder=1>'
				when sort in (4, 16) then '<FMT BottomBorder=1>'
				when sort in (39) then '<FMT RightBorder=1 BottomBorder=1>' else '' end + row18,
		row19

		from (
	SELECT top 1000 sort,
		row1 = '',
		row2 = MAX(CASE WHEN [1] IS NOT NULL THEN value END),
		row3= '',
		row4='',
		row5='', 
		row6 = MAX(CASE WHEN [2] IS NOT NULL THEN value END),
		row7='',
		row8='',
		row9 = MAX(CASE WHEN [3] IS NOT NULL THEN value END),
		row10='',
		row11='',
		row12 = MAX(CASE WHEN [4] IS NOT NULL THEN value END),
		row13='',
		row14='',
		row15 = MAX(CASE WHEN [5] IS NOT NULL THEN value END),
		row16='',
		row17='',
		row18 = MAX(CASE WHEN [6] IS NOT NULL THEN value END),
		row19=''

	FROM
	(
	select *, sort = ISNULL([1],isnull([2],isnull([3],isnull([4],isnull([5],isnull([6],7)))))) from (
select * 	FROM
	(
		SELECT 
			rownum, 
			LROType, 
			value 
			,sortorder = case LROType when 'col1' then 1 
										when 'col2' then 2 
										when 'col3' then 3 
										when 'col4' then 4 
										when 'col5' then 5 
										when 'col6' then 6 
										when 'col7' then 7 
										when 'col8' then 8 
										when 'col9' then 9
										when 'col10' then 10
										when 'col11' then 11
										when 'col12' then 12
										when 'col13' then 13
										when 'col14' then 14
										when 'col15' then 15
										when 'col16' then 16
										when 'col17' then 17
										when 'col18' then 18 
										when 'col19' then 19
										when 'col20' then 20
										when 'col21' then 21
										when 'col22' then 22
										when 'col23' then 23
										when 'col24' then 24
										when 'col25' then 25
										when 'col26' then 26
										when 'col27' then 27
										when 'col28' then 28 
										when 'col29' then 29
										when 'col30' then 30
										when 'col31' then 31
										when 'col32' then 32
										when 'col33' then 33
										when 'col34' then 34
										when 'col35' then 35
										when 'col36' then 36
										when 'col37' then 37
										when 'col38' then 38 
										when 'col39' then 39
										when 'col40' then 40
										when 'col41' then 41
										when 'col42' then 42
										when 'col43' then 43
										when 'col44' then 44
										when 'col45' then 45
										when 'col46' then 46
										when 'col47' then 47
										when 'col48' then 48 
										when 'col49' then 49
										when 'col50' then 50
										when 'col51' then 51
										when 'col52' then 52
										when 'col53' then 53
										when 'col54' then 54
										when 'col55' then 55
										when 'col56' then 56
										when 'col57' then 57
										when 'col58' then 58 
										else 100 end
		FROM (
			--select rownum, 
			--	col1 = CAST('' as varchar(200)),
			--	col2 = CAST(f1 as varchar(200)), 
			--	col3 = CAST(f2 as varchar(200)), 
			--	col4 = CAST(f3 as varchar(200)), 
			--	col5 = CAST(f4 as varchar(200)), 
			--	col6 = CAST(f5 as varchar(200)),
			--	col7 = CAST(case when rownum = 2 then f1 else '' end as varchar(200)),
			--	col8 = CAST('' as varchar(200)),
			--	col9 = CAST('' as varchar(200)),
			--	col10 = CAST('' as varchar(200)),
			--	col11 = CAST('' as varchar(200)),
			--	col12 = CAST('' as varchar(200)),
			--	col13 = CAST('' as varchar(200)),
			--	col14 = CAST('' as varchar(200)),
			--	col15 = CAST('' as varchar(200)),
			--	col16 = CAST('' as varchar(200)),
			--	col17 = CAST('' as varchar(200)),
			--	col18 = CAST('' as varchar(200)),
			--	col19 = CAST('' as varchar(200))
			select rownum, 
				col1 = CAST(case when rownum = 2 then f1 else '' end as varchar(200)),
				col2 = CAST('' as varchar(200)), 
				col3 = CAST('' as varchar(200)), 
				col4 = CAST('' as varchar(200)), 
				col5 = CAST(case when rownum > 2 then f1 else '' end as varchar(200)), 
				col6 = CAST('' as varchar(200)),
				col7 = CAST('' as varchar(200)),
				col8 = CAST('' as varchar(200)),
				col9 = CAST('' as varchar(200)),
				col10 = CAST('' as varchar(200)),
				col11 = CAST('' as varchar(200)),
				col12 = CAST('' as varchar(200)),
				col13 = CAST(case when rownum = 2 then f2 else '' end as varchar(200)),
				col14 = CAST('' as varchar(200)),
				col15 = CAST('' as varchar(200)),
				col16 = CAST('' as varchar(200)),
				col17 = CAST(case when rownum > 2 then f2 else '' end as varchar(200)),
				col18 = CAST('' as varchar(200)),
				col19 = CAST('' as varchar(200)),
				col20 = CAST('' as varchar(200)),
				col21 = CAST('' as varchar(200)),
				col22 = CAST('' as varchar(200)),
				col23 = CAST(case when  rownum = 1 then f3 else '' end as varchar(200)),
				col24 = CAST('' as varchar(200)),
				col25 = CAST(case when  rownum = 2 then f3 else '' end as varchar(200)),
				col26 = CAST('' as varchar(200)),
				col27 = CAST('' as varchar(200)),
				col28 = CAST('' as varchar(200)),
				col29 = CAST('' as varchar(200)),
				col30 = CAST('' as varchar(200)),
				col31 = CAST('' as varchar(200)),
				col32 = CAST('' as varchar(200)),
				col33 = CAST(case when rownum > 2 then f3 else '' end as varchar(200)),
				col34 = CAST('' as varchar(200)),
				col35 = CAST('' as varchar(200)),
				col36 = CAST('' as varchar(200)),
				col37 = CAST(case when rownum = 2 then f4 else '' end as varchar(200)),
				col38 = CAST('' as varchar(200)),
				col39 = CAST('' as varchar(200)),
				col40 = CAST('' as varchar(200)),
				col41 = CAST('' as varchar(200)),
				col42 = CAST('' as varchar(200)),
				col43 = CAST('' as varchar(200)),
				col44 = CAST('' as varchar(200)),
				col45 = CAST(case when rownum > 2 then f4 else '' end as varchar(200)),
				col46 = CAST('' as varchar(200)),
				col47 = CAST('' as varchar(200)),
				col48 = CAST('' as varchar(200)),
				col49 = CAST(case when rownum = 2 then f5 else '' end as varchar(200)),
				col50 = CAST('' as varchar(200)),
				col51 = CAST('' as varchar(200)),
				col52 = CAST('' as varchar(200)),
				col53 = CAST(case when rownum > 2 then f5 else '' end as varchar(200)),
				col54 = CAST('' as varchar(200)),
				col55 = CAST('' as varchar(200)),
				col56 = CAST('' as varchar(200)),
				col57 = CAST('' as varchar(200)),
				col58 = CAST('' as varchar(200))
			from UnplanLROTreeFormat (@ListName,@StudyYear,@TreeType, @CurrencyCode)
			) c

		UNPIVOT (
			value FOR LROType IN (col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12, col13, col14, col15, col16, col17, col18, col19, col20,
				col21, col22, col23, col24, col25, col26, col27, col28, col29, col30, col31, col32, col33, col34, col35, col36, col37, col38, col39, col40,
				col41, col42, col43, col44, col45, col46, col47, col48, col49, col50, col51, col52, col53, col54, col55, col56, col57, col58)
			) unpiv

		) src

		PIVOT (
			SUM (sortorder) FOR rownum IN ([1],[2],[3],[4],[5],[6])
		) piv

		--group by LROType
		
		) d

		) e group by LROType, sort
		order by sort

		) f
)




