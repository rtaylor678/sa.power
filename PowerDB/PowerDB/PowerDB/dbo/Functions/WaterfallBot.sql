﻿


CREATE FUNCTION [dbo].[WaterfallBot]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10), --should be MWH or MW
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT Title = a.UnitName, Title2 = CASE WHEN @ParentList LIKE '%Pacesetter%' THEN 'Pacesetter' ELSE 'Peer Group' END, BeginCell = null, Gap1 = null, Label = null, Down = null, Up = null, [EndCell] = b.BeginCell, sort = 8 
	FROM (
		SELECT UnitName = CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, GroupGroup = CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		GROUP BY CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
		) a

		LEFT JOIN (


		SELECT Title = CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END, [BeginCell] = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH), Gap1 = null, Label = null, Down = null, Up = null, [EndCell] = null
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) AND t.StudyYear = @StudyYear
		GROUP BY CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
		) b ON b.Title = a.GroupGroup

)

