﻿



CREATE FUNCTION [dbo].[StartReliability]
(	
	@ListName VARCHAR(30),
	@StudyYear VARCHAR(30),
	@CRVGroup VARCHAR(30),
	--@RankVariable INT,
	@ShortListName VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

	--we sort the Company to the top because the presbuilder will put them in reverse order

	SELECT TOP 1000 UnitLabel, Success = Success * 100, Forced = Forced * 100, Derates = Derates * 100, Maintenance = Maintenance * 100, [Startup Failure] = [Startup Failure] * 100
	FROM (

		SELECT UnitLabel = 'All Study Units',
			SUM(success)/SUM(TotalOpps) AS Success,
			SUM(forced)/SUM(TotalOpps) AS Forced,
			SUM(derate)/SUM(TotalOpps) AS Derates,
			SUM(maint)/SUM(TotalOpps) AS Maintenance,
			SUM(startup)/SUM(TotalOpps) AS [Startup Failure],
			sortorder = 2
		FROM STARTSANALYSIS r
		INNER JOIN tsort t ON t.REFNUM = r.REFNUM
		INNER JOIN breaks b ON t.refnum = b.refnum
		INNER JOIN _RL ON _RL.REFNUM = R.REFNUM
		WHERE _RL.LISTNAME = @ShortListName AND (
			(@CRVGroup = 'Rankine' AND (b.FuelGroup = 'Coal' OR b.CombinedCycle = 'N')) 
			OR (@CRVGroup = 'Brayton' AND b.CombinedCycle = 'Y')
			OR (@CRVGroup = 'CoalSuper' AND (b.FuelGroup = 'Coal' AND b.CRVGroup IN ('ESC','LSC')))
			OR (@CRVGroup = 'CoalSub' AND (b.FuelGroup = 'Coal' AND b.CRVGroup NOT IN ('ESC','LSC')))
			OR (@CRVGroup = 'SGO' AND (b.FuelGroup = 'Gas & Oil' AND b.SteamGasOil = 'Y'))
			OR (@CRVGroup = 'CE' AND b.CRVSizeGroup LIKE 'CE%')
			OR (@CRVGroup = 'SC' AND b.CRVSizeGroup = 'SC')
			OR (@CRVGroup = 'Cogen' AND b.CRVSizeGroup LIKE 'CG%')
			) 
			and t.StudyYear = @StudyYear

		UNION

		SELECT UnitLabel = 'Company (' + CAST(SUM(TotalOpps) AS varchar) + ' starts)',
			SUM(success)/SUM(TotalOpps) AS Success,
			SUM(forced)/SUM(TotalOpps) AS Forced,
			SUM(derate)/SUM(TotalOpps) AS Derates,
			SUM(maint)/SUM(TotalOpps) AS Maintenance,
			SUM(startup)/SUM(TotalOpps) AS [Startup Failure],
			sortorder = 1
		FROM STARTSANALYSIS r
		INNER JOIN tsort t ON t.REFNUM = r.REFNUM
		INNER JOIN breaks b ON t.refnum = b.refnum
		INNER JOIN _RL ON _RL.REFNUM = R.REFNUM
		WHERE _RL.LISTNAME = @ListName AND (
			(@CRVGroup = 'Rankine' AND (b.FuelGroup = 'Coal' OR b.CombinedCycle = 'N')) 
			OR (@CRVGroup = 'Brayton' AND b.CombinedCycle = 'Y')
			OR (@CRVGroup = 'CoalSuper' AND (b.FuelGroup = 'Coal' AND b.CRVGroup IN ('ESC','LSC')))
			OR (@CRVGroup = 'CoalSub' AND (b.FuelGroup = 'Coal' AND b.CRVGroup NOT IN ('ESC','LSC')))
			OR (@CRVGroup = 'SGO' AND (b.FuelGroup = 'Gas & Oil' AND b.SteamGasOil = 'Y'))
			OR (@CRVGroup = 'CE' AND b.CRVSizeGroup LIKE 'CE%')
			OR (@CRVGroup = 'SC' AND b.CRVSizeGroup = 'SC')
			OR (@CRVGroup = 'Cogen' AND b.CRVSizeGroup LIKE 'CG%')
			) 
			and t.StudyYear = @StudyYear

		--and two blank rows
		UNION ALL
		SELECT '',0,0,0,0,0,0
		UNION ALL
		SELECT '',0,0,0,0,0,0

	) c 

	ORDER BY sortorder 

)




