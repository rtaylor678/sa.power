﻿




CREATE FUNCTION [dbo].[WaterfallEventSummaryNew]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
--	@CRVGroup VARCHAR(30),
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10), -- should be MWH or MW
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 10000 unitname, 
		title, 
		Value,-- = SUM(value), 
		--Sortvalue = SUM(sortvalue),	
		RankOrder --= RANK() OVER (PARTITION BY UnitName ORDER BY SUM(Value)) 
	FROM (

		SELECT unitname, Title, value, rankorder from (
				SELECT UnitName, Title = 'Non-OH Site Labor', Value = [Non-OH Site Labor], RankOrder = 1 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Non-OH Materials', [Non-OH Materials], RankOrder = 2 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Non-OH Contract Labor', [Non-OH Contract Labor], RankOrder = 3 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'OH & SP Annual ization', [OH & SP Annualization], RankOrder = 4 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Admin & General', [Administrative & General], RankOrder = 5 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Chemicals', [Chemicals], RankOrder = 6 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Water', [Water], RankOrder = 7 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Taxes & Insurance', [Taxes & Insurance], RankOrder = 8 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'All Other', [All Other], RankOrder = 9 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite)
		) b
		) c

order by unitname, rankorder

)

