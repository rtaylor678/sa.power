﻿



CREATE FUNCTION [dbo].[TrafficLight]
(	
	@ListName VARCHAR(30),
	@StudyYear VARCHAR(30),
	@CRVGroup VARCHAR(30),	--changed to bring in CRVGroup and RankVariableGroup in one field, because I ran out of fields in PresBuilder (only 7 and I needed 8 when I added currency and metric)
							--so it brings them in, separated by a \, e.g. 'CE\1' means CRVGroup = CE and RankVariableGroup = 1
							--just below it splits them into two separate variables
	--@RankVariableGroup INT,
	@ShortListName VARCHAR(30),
	@BreakCondition VARCHAR(30),
	@CurrencyCode VARCHAR(4),
	@Metric BIT

)
RETURNS @T TABLE (UnitLabel VARCHAR(100), Col1 VARCHAR(100), Col2 VARCHAR(100), Col3 VARCHAR(100), Col4 VARCHAR(100))--, Col5 VARCHAR(100), Col6 VARCHAR(100), Col7 VARCHAR(100), Col8 VARCHAR(100))
AS
BEGIN

	DECLARE @RankVariableGroup INT
	SET @RankVariableGroup = SUBSTRING(@CRVGroup, CHARINDEX('\', @CRVGroup) + 1, LEN(@CRVGroup))
	SET @CRVGroup = SUBSTRING(@CRVGroup, 1, CHARINDEX('\', @CRVGroup) - 1)
	SET @CurrencyCode = RTRIM(@CurrencyCode)


	DECLARE @RankVariable1 INT
	DECLARE @RankVariable2 INT
	DECLARE @RankVariable3 INT
	DECLARE @RankVariable4 INT

	SET @RankVariable1 = case @RankVariableGroup when 1 then 46 when 2 then  1 when 3 then 78 when 4 then 46 when 5 then 46 when 6 then 46 when 7 then 46 when 8 then  6 when 9 then  6 when 10 then 46 else 0 end
	SET @RankVariable2 = case @RankVariableGroup when 1 then 47 when 2 then 55 when 3 then 55 when 4 then  1 when 5 then  1 when 6 then 78 when 7 then 78 when 8 then  7 when 9 then  7 when 10 then  1 else 0 end
	SET @RankVariable3 = case @RankVariableGroup when 1 then 49 when 2 then  0 when 3 then  0 when 4 then 86 when 5 then 88 when 6 then 86 when 7 then 88 when 8 then  8 when 9 then  8 when 10 then 71 else 0 end
	SET @RankVariable4 = case @RankVariableGroup when 1 then  0 when 2 then  0 when 3 then  0 when 4 then  0 when 5 then  0 when 6 then  0 when 7 then  0 when 8 then  0 when 9 then 81 when 10 then  0 else 0 end

	INSERT INTO @T (Unitlabel, Col1, Col2, Col3, Col4)--, Col5, Col6, Col7, Col8)

	--this part gets the labels in the first column (and has additional junk because of metric and currency conversions
	SELECT UnitLabel, 
		col1 = CASE WHEN @Metric = 1 THEN REPLACE(col1, 'Btu','MJ') ELSE col1 END, 
		col2 = CASE WHEN @Metric = 1 THEN REPLACE(col2, 'Btu','MJ') ELSE col2 END, 
		col3 = CASE WHEN @Metric = 1 THEN REPLACE(col3, 'Btu','MJ') ELSE col3 END, 
		col4 = CASE WHEN @Metric = 1 THEN REPLACE(col4, 'Btu','MJ') ELSE col4 END
		FROM (
			SELECT UnitLabel = '',
				col1 = '' + (SELECT REPLACE(ISNULL(FullDescription,''),'USD', @CurrencyCode) FROM RankVariables WHERE RankVariableID = @RankVariable1),
				col2 = (SELECT REPLACE(ISNULL(FullDescription,''),'USD', @CurrencyCode) FROM RankVariables WHERE RankVariableID = @RankVariable2),
				col3 = (SELECT REPLACE(ISNULL(FullDescription,''),'USD', @CurrencyCode) FROM RankVariables WHERE RankVariableID = @RankVariable3),
				col4 = (SELECT REPLACE(ISNULL(FullDescription,''),'USD', @CurrencyCode) FROM RankVariables WHERE RankVariableID = @RankVariable4)
		) lbl

	UNION

	--this part fills in the units and their values
	SELECT t.UnitLabel, 
		FORMAT(CASE WHEN rv1.Percentile < 1 THEN 1 WHEN rv1.Percentile > 99 THEN 99 ELSE rv1.Percentile END, '#,##0'), --null, 
		FORMAT(CASE WHEN rv2.Percentile < 1 THEN 1 WHEN rv2.Percentile > 99 THEN 99 ELSE rv2.Percentile END, '#,##0'),--null, 
		FORMAT(CASE WHEN rv3.Percentile < 1 THEN 1 WHEN rv3.Percentile > 99 THEN 99 ELSE rv3.Percentile END, '#,##0'), --null, 
		FORMAT(CASE WHEN rv4.Percentile < 1 THEN 1 WHEN rv4.Percentile > 99 THEN 99 ELSE rv4.Percentile END, '#,##0')--, null
	FROM TSort t
		LEFT JOIN _Traffic rv1 ON rv1.RankVariableID = @RankVariable1 AND rv1.Refnum = t.Refnum AND rv1.ListName = @ShortListName AND rv1.BreakCondition = @BreakCondition
		LEFT JOIN _Traffic rv2 ON rv2.RankVariableID = @RankVariable2 AND rv2.Refnum = t.Refnum AND rv2.ListName = @ShortListName AND rv2.BreakCondition = @BreakCondition
		LEFT JOIN _Traffic rv3 ON rv3.RankVariableID = @RankVariable3 AND rv3.Refnum = t.Refnum AND rv3.ListName = @ShortListName AND rv3.BreakCondition = @BreakCondition
		LEFT JOIN _Traffic rv4 ON rv4.RankVariableID = @RankVariable4 AND rv4.Refnum = t.Refnum AND rv4.ListName = @ShortListName AND rv4.BreakCondition = @BreakCondition
	WHERE t.Refnum in (SELECT Refnum FROM _RL WHERE ListName = @ListName)
		AND t.StudyYear = @StudyYear
		AND rv1.BreakValue = @CRVGroup

RETURN


END
