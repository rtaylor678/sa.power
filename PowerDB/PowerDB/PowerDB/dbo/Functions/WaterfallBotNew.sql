﻿



CREATE FUNCTION [dbo].[WaterfallBotNew]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10), --should be MWH or MW
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(

	--if BySite = 0 then we return each unit individually, and return values based on the CRV Size Group
	--if BySite = 1 we return the site values based on CRV Group, unless there is more than one CRV Group at the site in which case it drops down to use CRV Size Group

	SELECT DISTINCT Title = CASE WHEN @BySite = 0 THEN a.UnitName ELSE CASE WHEN CRVGroupCount = 1 THEN a.SiteName ELSE a.UnitName END END, 
		Title2 = CASE WHEN @ParentList LIKE '%Pacesetter%' THEN 'Pacesetter' ELSE 'Peer Group' END,
		EndCell = CASE WHEN @BySite = 0 THEN c.BeginCell ELSE CASE WHEN CRVGroupCount = 1 THEN b.BeginCell ELSE c.BeginCell END END

	FROM (
		--this part just gets the possible site/unit name and crv and size groups to join to
		SELECT s.SiteName, t.UnitName, b.CRVGroup, b.CRVSizeGroup
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		) a

	LEFT JOIN (
		--this part gets the values for the different CRV Groups, which will be chosen from later
		SELECT b.crvgroup,
			BeginCell = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) AND t.StudyYear = @StudyYear
		GROUP BY b.CRVGroup
		) b ON b.CRVGroup = a.CRVGroup

	LEFT JOIN (
		--this part gets the values for the different CRV Size Groups, which will be chosen from later
		SELECT b.CRVSizeGroup, 
			BeginCell = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
			LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) AND t.StudyYear = @StudyYear
		GROUP BY b.CRVSizeGroup
		) c ON c.CRVSizeGroup = a.CRVSizeGroup

	LEFT JOIN (
		--this part counts the number of CRV Groups for that sitename - in the query at the top we use this to decide whether to use CRV Group or CRV Size Group (if the count = 1 we use CRV Group)
		SELECT s.SiteName, CRVGroupCount = COUNT(distinct b.CRVGroup)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN StudySites s ON s.SiteID = t.SiteID
		WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
		GROUP BY s.SiteName
		) d ON d.SiteName = a.SiteName







	--------------this version replaced 8/31/16 SW because it was messing up where a site had two different CRV Groups (example was KCPL Iatan, where Iatan 1 is LB and Iatan 2 is LSC
	--------------had to make it drop down to the CRV Size Group in those cases, but stay with the CRV Group where it wasn't a problem
	--------------
	--------------SELECT Title = a.UnitName, 
	--------------Title2 = CASE WHEN @ParentList LIKE '%Pacesetter%' THEN 'Pacesetter' ELSE 'Peer Group' END, 
	----------------BeginCell = null, 
	----------------Gap1 = null, 
	----------------Down = null, 
	----------------Up = null, 
	--------------[EndCell] = b.BeginCell
	----------------sort = 99
	--------------FROM (
	--------------	SELECT UnitName = CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, GroupGroup = CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
	--------------	FROM TSort t
	--------------		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--------------		LEFT JOIN StudySites s ON s.SiteID = t.SiteID
	--------------		LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
	--------------		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	--------------	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName) AND t.StudyYear = @StudyYear
	--------------	GROUP BY CASE WHEN @BySite = 1 THEN s.SiteName else t.UnitName end, CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
	--------------	) a

	--------------	LEFT JOIN (


	--------------	SELECT Title = CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END, [BeginCell] = GlobalDB.dbo.WtAvg(o.TotCashLessFuelEm, gtc.AdjNetMWH), Gap1 = null, Label = null, Down = null, Up = null, [EndCell] = null
	--------------	FROM TSort t
	--------------		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--------------		LEFT JOIN StudySites s ON s.SiteID = t.SiteID
	--------------		LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = CASE WHEN @DataType = 'MW' THEN 'KW' ELSE @DataType END
	--------------		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	--------------	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ParentList) AND t.StudyYear = @StudyYear
	--------------	GROUP BY CASE WHEN @BySite = 1 THEN b.CRVGroup ELSE b.CRVSizeGroup END
	--------------	) b ON b.Title = a.GroupGroup

)


