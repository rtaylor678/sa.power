﻿


CREATE FUNCTION [dbo].[LROByOutTypeFuelPercent]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30) -- Coal or Gas

)
RETURNS TABLE 
AS
RETURN 
(

	--this is terrible code, but it works. change it, how to do a running total for each part?

	SELECT TOP 1000 LROType, Pct FROM (

	SELECT LROType = 'Forced', 
		Pct = CASE WHEN MAX(SumLROTotal) = 0 THEN 0 ELSE SUM(Forced)/MAX(SUMLROTotal) * 100 END,
		sortorder = 1
	FROM LROByOutTypeFuelWithHeader(@ListName, @StudyYear, @CRVGroup) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(Forced) + SUM(Maint) + SUM(Planned) FROM LROByOutTypeFuel (@ListName, @StudyYear, @CRVGroup)) b ON 1 = 1
	WHERE LROType = 'Forced'

	UNION

	SELECT LROType = 'Maintenance (Prescheduled)', 
		Pct = CASE WHEN MAX(SumLROTotal) = 0 THEN 0 ELSE (SUM(Maint) + SUM(Forced))/MAX(SUMLROTotal) * 100 END,
		sortorder = 2
	FROM LROByOutTypeFuelWithHeader (@ListName, @StudyYear, @CRVGroup) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(Forced) + SUM(Maint) + SUM(Planned) FROM LROByOutTypeFuel (@ListName, @StudyYear, @CRVGroup)) b ON 1 = 1
	WHERE LROType IN ('Forced', 'Maintenance (Prescheduled)')
	GROUP BY SumLROTotal

	UNION

	SELECT LROType = 'Planned', 
		Pct = CASE WHEN MAX(SumLROTotal) = 0 THEN 0 ELSE (SUM(Maint) + SUM(Forced) + SUM(Planned))/MAX(SUMLROTotal) * 100 END,
		sortorder = 3
	FROM LROByOutTypeFuelWithHeader (@ListName, @StudyYear, @CRVGroup) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(Forced) + SUM(Maint) + SUM(Planned) FROM LROByOutTypeFuel (@ListName, @StudyYear, @CRVGroup)) b ON 1 = 1
	GROUP BY SumLROTotal

	) b

	ORDER BY sortorder

)



