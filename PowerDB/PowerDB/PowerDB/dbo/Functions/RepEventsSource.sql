﻿


CREATE FUNCTION [dbo].[RepEventsSource]
(	
	@ListName VARCHAR(30),
	@StudyYear INT

)
RETURNS TABLE 
AS
RETURN 
(

	-- this code returns ALL appropriate event records for a particular list
	-- it is up to the function user to determine which ones to break out for their own purposes
	-- it also returns values (LRO) in USD

	SELECT ss.SiteLabel, 
		t.UnitLabel,
		RefEventNum = t.Refnum + CAST(e.EVNT_NO as varchar(10)), -- used for calculating number of individual events in cases where you are looking for repetitive events
		e.Cause_Code, 
		PresDescription,
		LRO = SUM(LRO.lro), 
		LostMWH = SUM(e.LOSTMW * e.DURATION),
		b.FuelGroup
	FROM TSort t
		INNER JOIN Breaks b ON b.Refnum = t.Refnum
		INNER JOIN Events e ON e.Refnum = t.Refnum 
		INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
		INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
		INNER JOIN StudySites ss on ss.SiteID = t.SiteID
	WHERE e.CAUSE_CODE <> 7777
		AND t.studyyear = @StudyYear
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) 
		AND e.EVNT_Category in ('F', 'M')
	GROUP BY ss.SiteLabel, t.UnitLabel, t.Refnum + CAST(e.EVNT_NO as varchar(10)), e.CAUSE_CODE, PresDescription, b.FuelGroup

)



