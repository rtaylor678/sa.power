﻿

CREATE FUNCTION [dbo].[zzzUnplanLROByCauseCodeTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30), --coal or gas & oil
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT '<FMT colWidth=106 LeftMargin=7 ColorFont=98002E><b>Cause Code</b>' as CauseCode,
		'<FMT LeftMargin=7># Events' as NumEvents,
		'<FMT LeftMargin=7>Total LRO, k ' + RTRIM(@CurrencyCode) as TotalLRO,
		'<FMT LeftMargin=7># Plants' as NumSites,
		'<FMT LeftMargin=7># Units' as NumUnits,
		'' as PresDescription

	UNION ALL

	SELECT '<FMT LeftMargin=7 ColorFont=98002E>' + '<b>' + ISNULL(CauseCode,'') + '</b>', 
		'<FMT LeftMargin=7>' + NumEvents, 
		'<FMT LeftMargin=7>' + TotalLRO, 
		'<FMT LeftMargin=7>' + NumSites, 
		'<FMT LeftMargin=7>' + NumUnits, 
		'<FMT LeftMargin=7>' + PresDescription
	FROM PowerGlobal.dbo.Numbers n
		LEFT JOIN (

		SELECT CauseCode = CAST(CAUSE_CODE AS varchar), 
			NumEvents = CAST(COUNT(*) AS varchar),
			TotalLRO = CAST(Power.dbo.GetDollarsText(SUM(LRO) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS varchar), 
			Rank = RANK() OVER (ORDER BY SUM(LRO) DESC),
			NumSites = CAST(COUNT(DISTINCT SiteID) AS varchar),
			NumUnits = CAST(COUNT(DISTINCT Refnum) AS varchar),
			PresDescription
		FROM UnplanLRO
		WHERE Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
			AND FuelGroup = @CRVGroup 
			AND StudyYear = @StudyYear
		GROUP BY CAUSE_CODE, PresDescription
		) u on u.Rank = n.Num
		--	SELECT 
		--		CauseCode = CAST(e.CAUSE_CODE AS varchar),
		--		NumEvents = CAST(COUNT(*) AS varchar),
		--		TotalLRO = CAST(LEFT(CONVERT(varchar(50), CAST(sum(ISNULL(lro.LRO,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS money),1), LEN(CONVERT(varchar(50), CAST(sum(ISNULL(lro.LRO,0) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)) AS money),1)) -3) AS varchar),
		--		NumSites = CAST(COUNT(DISTINCT SiteID) AS varchar),
		--		NumUnits = CAST(COUNT(DISTINCT t.Refnum) AS varchar),
		--		PresDescription,
		--		Rank = RANK() OVER (ORDER BY SUM(lro.LRO) DESC)
		--	FROM TSort t
		--		INNER JOIN Breaks b ON b.Refnum = t.Refnum
		--		INNER JOIN Events e ON e.Refnum = t.Refnum 
		--		INNER JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
		--		INNER JOIN CauseCodes ON e.CAUSE_CODE = CauseCodes.CAUSE_CODE
		--	WHERE lro.LRO > 0 
		--		AND e.CAUSE_CODE <> 7777 
		--		AND t.studyyear = @StudyYear 
		--		AND (
		--			(@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
		--			(@CRVGroup = 'Gas & Oil' AND b.FuelGroup = 'Gas & Oil')
		--			)
		--		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName) 
		--		AND e.EVNT_Category in ('F', 'M')
		--	GROUP BY e.CAUSE_CODE, PresDescription

		--) b ON b.Rank = num.Num
	WHERE n.Num <= 7
)
