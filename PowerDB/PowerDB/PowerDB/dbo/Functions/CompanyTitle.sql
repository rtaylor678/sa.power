﻿



CREATE FUNCTION [dbo].[CompanyTitle]
(	
	@ListName VARCHAR(30),
	@StudyYear INT
)
RETURNS TABLE 
AS
RETURN 
(

	--titles for the first page

	SELECT DISTINCT '<FMT colWidth=500 rowHeight=40 fontBold=true fontHeight=24 ColorFont=FFFFFF Transparent=true>' + rtrim(CompanyName) AS col1
	FROM TSort t
	WHERE t.Refnum IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
		AND t.EvntYear = @StudyYear

	UNION

	SELECT '<FMT rowHeight=40 fontHeight=20 ColorFont=FFFFFF Transparent=true>' + datename(mm, GETDATE()) + ' '  + DATENAME(dd,GETDATE()) + ', ' + datename(yyyy, GETDATE())


)




