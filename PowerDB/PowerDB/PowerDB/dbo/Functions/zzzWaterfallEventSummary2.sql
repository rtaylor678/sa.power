﻿



CREATE FUNCTION [dbo].[zzzWaterfallEventSummary2]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
--	@CRVGroup VARCHAR(30),
	@ParentList VARCHAR(30),
	@DataType VARCHAR(10), -- should be MWH or MW
	@BySite BIT
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 10000 unitname, 
		title, 
		Value,-- = SUM(value), 
		Sortvalue,-- = SUM(sortvalue),	
		Rank --= RANK() OVER (PARTITION BY UnitName ORDER BY SUM(Value)) 
	FROM (

--		SELECT TOP 10000 unitname, title = case when Rank <= 5 then Title else 'All Other' END, value, sortvalue, rank from (
--			SELECT TOP 10000 *,	Rank = RANK() OVER (PARTITION BY UnitName ORDER BY SortValue DESC) from (
				SELECT UnitName, Title = 'Non-OH Site Labor', Value = [Non-OH Site Labor], SortValue = ABS([Non-OH Site Labor]), Rank = 2 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Non-OH Materials', [Non-OH Materials], SortValue = ABS([Non-OH Materials]), Rank = 3 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Non-OH Contract Labor', [Non-OH Contract Labor], SortValue = ABS([Non-OH Contract Labor]), Rank = 4 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'OH & SP Annualization', [OH & SP Annualization], SortValue = ABS([OH & SP Annualization]), Rank = 5 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Administrative & General', [Administrative & General], SortValue = ABS([Administrative & General]), Rank = 6 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Chemicals', [Chemicals], SortValue = ABS([Chemicals]), Rank = 7 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Water', [Water], SortValue = ABS([Water]), Rank = 8 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite) UNION ALL
				SELECT UnitName, Title = 'Taxes & Insurance', [Taxes & Insurance], SortValue = ABS([Taxes & Insurance]), Rank = 9 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite)

--		) b

	) c

	UNION ALL

	SELECT UnitName, Title = 'All Other', [All Other], SortValue = ABS([All Other]), Rank = 99 FROM WaterfallDiff(@ListName, @StudyYear, @ParentList, @DataType, @BySite)

--) d

--group by UnitName, Title
--order by unitname, value

)
