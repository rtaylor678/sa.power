﻿
CREATE FUNCTION [dbo].[ThermEff]
(	

	@ListName VARCHAR(30),
	@StudyYear INT
)
RETURNS TABLE 
AS
RETURN 
(
	
	SELECT t.UnitLabel, c.ThermEff
	FROM CogenCalc c
	INNER JOIN TSort t on c.Refnum = t.Refnum
	WHERE c.refnum in (select refnum from _rl where listname = @ListName) and t.studyyear = @StudyYear
	--ORDER by c.ThermEff

)

