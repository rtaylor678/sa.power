﻿

CREATE FUNCTION [dbo].[FuelCost]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@Location VARCHAR(30),
	@CurrencyCode VARCHAR(4),
	@Metric BIT
	--@PeerGroup1 VARCHAR(50),
	--@PeerGroup2 VARCHAR(50),
	--@PeerGroup3 VARCHAR(50)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT UnitLabel = RTRIM(UnitLabel), FuelCost = CASE @Metric WHEN 1 THEN GlobalDB.dbo.UnitsConv(FuelCost,'MBTU','GJ') ELSE FuelCost END
	FROM (
		SELECT t.UnitLabel, FuelCost = (CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
			LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
			LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
		WHERE t.studyyear = @StudyYear 
			AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND (
				(@Location = 'N America' AND t.Continent = 'N America') OR
				(@Location = 'Europe' AND t.Continent = 'Europe') OR
				(@Location = 'World' AND t.Continent NOT IN ('N America','Europe'))
				)
			AND (
				(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
				(@FuelGroup <> 'Coal' AND f.FuelType IN ('Gas','Oil'))
				)

		--UNION ALL

		--SELECT UnitLabel, FuelCost
		--FROM (
		--	SELECT UnitLabel = RIGHT(@PeerGroup1,LEN(RTRIM(@PeerGroup1))-CHARINDEX('#',@PeerGroup1)), 
		--		FuelCost = GlobalDB.dbo.WtAvg(CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END,f.TotMBTU) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),
		--		COUNT(*) AS cnt
		--	FROM TSort t
		--		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		--		LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
		--		LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
		--		LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
		--	WHERE t.studyyear = @StudyYear 
		--		AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname =  LEFT(@PeerGroup1, CHARINDEX('#',@PeerGroup1)-CASE WHEN LEN(rtrim(@PeerGroup1)) = 0 THEN 0 ELSE 1 END))
		--	) c
		--WHERE cnt > 0

		--UNION ALL

		--SELECT UnitLabel, FuelCost
		--FROM (
		--	SELECT UnitLabel = RIGHT(@PeerGroup2,LEN(RTRIM(@PeerGroup2))-CHARINDEX('#',@PeerGroup2)), 
		--		FuelCost = GlobalDB.dbo.WtAvg(CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END,f.TotMBTU) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),
		--		COUNT(*) AS cnt
		--	FROM TSort t
		--		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		--		LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
		--		LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
		--		LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
		--	WHERE t.studyyear = @StudyYear 
		--		AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname =  LEFT(@PeerGroup2, CHARINDEX('#',@PeerGroup2)-CASE WHEN LEN(rtrim(@PeerGroup2)) = 0 THEN 0 ELSE 1 END))
		--	) d
		--WHERE cnt > 0
		
		--UNION ALL

		--SELECT UnitLabel, FuelCost
		--FROM (
		--	SELECT UnitLabel = RIGHT(@PeerGroup3,LEN(RTRIM(@PeerGroup3))-CHARINDEX('#',@PeerGroup3)), 
		--		FuelCost = GlobalDB.dbo.WtAvg(CASE @FuelGroup WHEN 'Coal' THEN c.TotCostMBTU ELSE o.STFuelCost/f.TotMBTU * 1000 END,f.TotMBTU) * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),
		--		COUNT(*) AS cnt
		--	FROM TSort t
		--		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		--		LEFT JOIN CoalCostMBTU c ON c.Refnum = t.Refnum
		--		LEFT JOIN OpExCalc o ON o.Refnum = t.Refnum AND o.DataType = 'ADJ'
		--		LEFT JOIN FuelTotCalc f ON f.Refnum = t.Refnum
		--	WHERE t.studyyear = @StudyYear 
		--		AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname =  LEFT(@PeerGroup3, CHARINDEX('#',@PeerGroup3)-CASE WHEN LEN(rtrim(@PeerGroup3)) = 0 THEN 0 ELSE 1 END))
		--	) e
		--WHERE cnt > 0

	) b
	--where 1 = 0

)
