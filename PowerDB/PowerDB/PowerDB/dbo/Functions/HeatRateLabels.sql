﻿



CREATE FUNCTION [dbo].[HeatRateLabels]
(	
	@ShortListName VARCHAR(30),
	@Metric BIT
)
RETURNS TABLE 
AS
RETURN 
(

	--this creates the labels for the Heat Rate slide, these should be in a fixed position over the years
	--turns out they won't be fixed because the line moves back and forth in the heat rate chart

	SELECT Label, Xaxis = Filter1, Yaxis = CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(Filter2,'BTU','MJ') ELSE Filter2 END 
	FROM SlideSourceData 
	WHERE [Type] = 'HeatRateLabels' AND Source = @ShortListName

	--SELECT Label = 'Combined Cycle/ Cogeneration/Aeros', Xaxis = 45, Yaxis = CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(6600,'BTU','MJ') ELSE 6600 END
	--UNION
	--SELECT 'Super-Critical Rankine',75, CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(8800,'BTU','MJ') ELSE 8800 END
	--UNION
	--SELECT 'Sub-Critical Rankine', 52, CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(12300,'BTU','MJ') ELSE 12300 END
	--UNION
	--SELECT 'Simple Cycle/ Peaking',90, CASE WHEN @Metric = 1 THEN GlobalDB.dbo.UnitsConv(15500,'BTU','MJ') ELSE 15500 END

)




