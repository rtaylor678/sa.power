﻿

CREATE FUNCTION [dbo].[zzzUnplanLROTreeTable]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@TreeType VARCHAR(30) --"", "Tech", "Coal", "SGO", "Elec","Cogen"
)
RETURNS TABLE 
AS
RETURN 
(

	select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 1

	union all select '','',CAST(LRO as varchar) + ' ' + CAST (lostmwh as varchar),'','' from unplanLROTree (@ListName,@StudyYear,'','','') --row 2

	union all select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 3
	union all select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 4

	union all --row 5

		select CASE @TreeType WHEN '' THEN a.Name + ' ' + CAST(a.LRO as varchar) + ' ' + CAST(a.lostmwh as varchar) ELSE e.Name + ' ' + CAST(e.LRO as varchar) + ' ' + CAST(e.lostmwh as varchar) END ,
		CASE @TreeType WHEN '' THEN b.Name + ' ' + CAST(b.LRO as varchar) + ' ' + CAST(b.lostmwh as varchar) ELSE f.Name + ' ' + CAST(f.LRO as varchar) + ' ' + CAST(f.lostmwh as varchar) END ,
		CASE @TreeType WHEN '' THEN c.Name + ' ' + CAST(c.LRO as varchar) + ' ' + CAST(c.lostmwh as varchar) ELSE g.Name + ' ' + CAST(g.LRO as varchar) + ' ' + CAST(g.lostmwh as varchar) END ,
		CASE @TreeType WHEN '' THEN d.Name + ' ' + CAST(d.LRO as varchar) + ' ' + CAST(d.lostmwh as varchar) ELSE h.Name + ' ' + CAST(h.LRO as varchar) + ' ' + CAST(h.lostmwh as varchar) END ,
		''
		 from UnplanLROTreeParent (@ListName,@StudyYear,'Coal','','') a
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'SGO','','')) b on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Elec','','')) c on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Cogen','','')) d on 1=1

		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Boiler','')) e on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Turbine','')) f on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','External','')) g on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Balance of Plant','')) h on 1=1

	union all select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 6

	union all --row 7

		select a.Name + ' ' + CAST(a.LRO as varchar) + ' ' + CAST(a.lostmwh as varchar),
		b.Name + ' ' + CAST(b.LRO as varchar) + ' ' + CAST(b.lostmwh as varchar),
		'',
		c.Name + ' ' + CAST(c.LRO as varchar) + ' ' + CAST(c.lostmwh as varchar),
		d.Name + ' ' + CAST(d.LRO as varchar) + ' ' + CAST(d.lostmwh as varchar)
		 from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Boiler','Air/Gas/Slag') a
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Turbine','Valves & Controls (VC)')) b on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Balance of Plant','Cooling Water Facilities (CWF)')) c on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Balance of Plant','Fuel Delivery System (FHF)')) d on 1=1

	union all select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 8

	union all --row 9

		select a.Name + ' ' + CAST(a.LRO as varchar) + ' ' + CAST(a.lostmwh as varchar),
		b.Name + ' ' + CAST(b.LRO as varchar) + ' ' + CAST(b.lostmwh as varchar),
		'',
		c.Name + ' ' + CAST(c.LRO as varchar) + ' ' + CAST(c.lostmwh as varchar),
		d.Name + ' ' + CAST(d.LRO as varchar) + ' ' + CAST(d.lostmwh as varchar)
		 from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Boiler','Tube Leaks') a
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Turbine','Turbine')) b on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Balance of Plant','Coal Pulverizers/Hammer Mills (CPHM)')) c on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Balance of Plant','Emission Controls')) d on 1=1

	union all select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 10

	union all --row 11

		select a.Name + ' ' + CAST(a.LRO as varchar) + ' ' + CAST(a.lostmwh as varchar),
		b.Name + ' ' + CAST(b.LRO as varchar) + ' ' + CAST(b.lostmwh as varchar),
		'',
		'',
		c.Name + ' ' + CAST(c.LRO as varchar) + ' ' + CAST(c.lostmwh as varchar)
		 from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Boiler','Steam/Feedwater') a
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Turbine','Condenser & Auxiliaries (CA)')) b on 1=1
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Balance of Plant','Other')) c on 1=1

	union all select col1 = '', col2 = '', col3 = '', col4 = '', col5 = '' --row 12

	union all --row 13

		select a.Name + ' ' + CAST(a.LRO as varchar) + ' ' + CAST(a.lostmwh as varchar),
		b.Name + ' ' + CAST(b.LRO as varchar) + ' ' + CAST(b.lostmwh as varchar),
		'',
		'',
		''
		 from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Boiler','Boiler Misc') a
		left join (select * from UnplanLROTreeParent ('@ListName',@StudyYear,'Coal','Turbine','Generator (Gen)')) b on 1=1
		



--Boiler                                  	Air/Gas/Slag
--Boiler                                  	Tube Leaks
--Boiler                                  	Steam/Feedwater
--Boiler                                  	Boiler Misc

--Combustion Turbine                      	Combustion
--Combustion Turbine                      	Inlet
--Combustion Turbine                      	Turbine
--Combustion Turbine                      	Other

--Turbine                                 	Valves & Controls (VC)                  
--Turbine                                 	Turbine                                 
--Turbine                                 	Condenser & Auxiliaries (CA)            
--Turbine                                 	Generator (Gen)                         

--External                                	External                                

--Balance of Plant                        	Cooling Water Facilities (CWF)          
--Balance of Plant                        	Fuel Delivery System (FHF)              
--Balance of Plant                        	Coal Pulverizers/Hammer Mills (CPHM)    
--Balance of Plant                        	Emission Controls
--Balance of Plant                        	Other                                   



)


                               