﻿



CREATE FUNCTION [dbo].[zzzMaintCostCoal]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CoalNMCGroup VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT UnitLabel = 'Group Avg',
		'Non-Overhaul' = GlobalDB.dbo.WtAvg(o.AnnNonOHCostMWH, g.AdjNetMWH2Yr),
		'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(o.AnnOHCostMWH - o.AnnOHCostOver15MWH, g.AdjNetMWH2Yr),
		'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(o.annOHCostover15MWH, g.AdjNetMWH2Yr)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

		SELECT UnitLabel = 'Pace- setter',
		'Non-Overhaul' = GlobalDB.dbo.WtAvg(o.AnnNonOHCostMWH, g.AdjNetMWH2Yr),
		'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(o.AnnOHCostMWH - o.AnnOHCostOver15MWH, g.AdjNetMWH2Yr),
		'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(o.annOHCostover15MWH, g.AdjNetMWH2Yr)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = 'Power13 Pacesetters 2')
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT UnitLabel = 'Co Avg',
		'Non-Overhaul' = GlobalDB.dbo.WtAvg(o.AnnNonOHCostMWH, g.AdjNetMWH2Yr),
		'Overhaul & Special Projects <15 yrs' = GlobalDB.dbo.WtAvg(o.AnnOHCostMWH - o.AnnOHCostOver15MWH, g.AdjNetMWH2Yr),
		'Special Projects >= 15 yrs' = GlobalDB.dbo.WtAvg(o.annOHCostover15MWH, g.AdjNetMWH2Yr)
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup

	UNION ALL

	SELECT TOP 1000 UnitLabel, --TOP 1000 here so that the ORDER BY will work in a TVF
		'Non-Overhaul' = o.AnnNonOHCostMWH,
		'Overhaul & Special Projects <15 yrs' = o.AnnOHCostMWH - o.AnnOHCostOver15MWH,
		'Special Projects >= 15 yrs' = o.annOHCostover15MWH
	FROM TSort t
		LEFT JOIN MainttotCalc o ON o.Refnum = t.Refnum
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN GenerationTotCalc g ON g.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CoalNMCGroup2 = @CoalNMCGroup
	ORDER BY o.AnnMaintCostMWH DESC


)




