﻿




CREATE FUNCTION [dbo].[EGCLabels] 
( 
@ShortListName VARCHAR(50)
)
RETURNS TABLE 
AS
RETURN 
(

	--this creates the labels for the EGC slide, these should be in a fixed position over the years

	SELECT Label, Filter1, Filter2 FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'EGCLabels'

	--SELECT Label = 'Potential Under Spending', Xaxis = 10, Yaxis = 0.15

	--UNION

	--SELECT 'Pacesetters',35, 0.5

	--UNION

	--SELECT 'Typical Range', 50, 1.35

	--UNION

	--SELECT 'Potential Over Spending',80, 2.25

)





