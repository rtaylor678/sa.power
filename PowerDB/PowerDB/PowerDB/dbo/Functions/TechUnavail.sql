﻿CREATE FUNCTION [dbo].[TechUnavail]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30)
	--@PeerGroup1 VARCHAR(50),
	--@PeerGroup2 VARCHAR(50),
	--@PeerGroup3 VARCHAR(50)

)
RETURNS TABLE 
AS
RETURN 
(

	--Group and Pacesetter values
	SELECT UnitLabel = Label, 
		'Equivalent Unplanned Outage Factor' = SUM(EUOF), 
		'Equivalent Planned Outage Factor' = SUM(EPOF) FROM (
		SELECT Label, EUOF = Value, EPOF = NULL FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'TechUnavail' AND Filter1 = @CRVGroup AND Filter2 = 'EUOF'
		UNION
		SELECT Label, EUOF = NULL, EPOF = Value FROM SlideSourceData WHERE Source = @ShortListName AND Type = 'TechUnavail' AND Filter1 = @CRVGroup AND Filter2 = 'EPOF') b
	GROUP BY Label

	UNION ALL

	--Company average values
	SELECT UnitLabel, EUOF, EPOF FROM (
		SELECT UnitLabel = 'Co Avg',
			EUOF = GlobalDB.dbo.WtAvg(EUOF2Yr, WPH2Yr),
			EPOF = GlobalDB.dbo.WtAvg(EPOF2Yr, WPH2Yr),
			COUNT(*) AS cnt
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND b.CRVGroup = @CRVGroup
			AND t.Refnum NOT LIKE '%G') b
	WHERE cnt > 1

	--UNION ALL

	--	--Peer1 average values
	--SELECT UnitLabel, EUOF, EPOF FROM (
	--	SELECT UnitLabel = RIGHT(@PeerGroup1,LEN(RTRIM(@PeerGroup1))-CHARINDEX('#',@PeerGroup1)),
	--		EUOF = GlobalDB.dbo.WtAvg(EUOF2Yr, WPH2Yr),
	--		EPOF = GlobalDB.dbo.WtAvg(EPOF2Yr, WPH2Yr),
	--		COUNT(*) AS cnt
	--	FROM TSort t
	--		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	--	WHERE t.StudyYear = @StudyYear 
	--		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = LEFT(@PeerGroup1, CHARINDEX('#',@PeerGroup1)-CASE WHEN LEN(rtrim(@PeerGroup1)) = 0 THEN 0 ELSE 1 END))
	--		--AND b.CRVGroup = @CRVGroup
	--	) c
	--WHERE cnt > 0

	--UNION ALL

	--	--Peer2 average values
	--SELECT UnitLabel, EUOF, EPOF FROM (
	--	SELECT UnitLabel = RIGHT(@PeerGroup2,LEN(RTRIM(@PeerGroup2))-CHARINDEX('#',@PeerGroup2)),
	--		EUOF = GlobalDB.dbo.WtAvg(EUOF2Yr, WPH2Yr),
	--		EPOF = GlobalDB.dbo.WtAvg(EPOF2Yr, WPH2Yr),
	--		COUNT(*) AS cnt
	--	FROM TSort t
	--		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	--	WHERE t.StudyYear = @StudyYear 
	--		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = LEFT(@PeerGroup2, CHARINDEX('#',@PeerGroup2)-CASE WHEN LEN(rtrim(@PeerGroup2)) = 0 THEN 0 ELSE 1 END))
	--		--AND b.CRVGroup = @CRVGroup
	--	) d
	--WHERE cnt > 0

	--UNION ALL

	--		--Peer3 average values
	--SELECT UnitLabel, EUOF, EPOF FROM (
	--	SELECT UnitLabel = RIGHT(@PeerGroup3,LEN(RTRIM(@PeerGroup3))-CHARINDEX('#',@PeerGroup3)),
	--		EUOF = GlobalDB.dbo.WtAvg(EUOF2Yr, WPH2Yr),
	--		EPOF = GlobalDB.dbo.WtAvg(EPOF2Yr, WPH2Yr),
	--		COUNT(*) AS cnt
	--	FROM TSort t
	--		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
	--		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	--	WHERE t.StudyYear = @StudyYear 
	--		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = LEFT(@PeerGroup3, CHARINDEX('#',@PeerGroup3)-CASE WHEN LEN(rtrim(@PeerGroup3)) = 0 THEN 0 ELSE 1 END))
	--		--AND b.CRVGroup = @CRVGroup
	--	) e
	--WHERE cnt > 0

	UNION ALL

	--Unit Values
	SELECT UnitLabel, EUOF, EPOF FROM (
	SELECT TOP 1000 UnitLabel = RTRIM(UnitLabel), --TOP 1000 here so that the ORDER BY will work in a TVF
		EUOF = ISNULL(nf.EUOF2Yr,0),
		EPOF = ISNULL(nf.EPOF2Yr,0),
		SortOrder = CASE WHEN t.Refnum LIKE '%G' THEN 1 ELSE 0 END
		--WPH = ISNULL(nf.WPH2Yr,0)
	FROM TSort t
		LEFT JOIN Breaks b ON b.Refnum = t.Refnum
		LEFT JOIN NERCFactors nf ON nf.Refnum = t.Refnum
	WHERE t.StudyYear = @StudyYear 
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
		AND b.CRVGroup = @CRVGroup
	ORDER BY SortOrder, ISNULL(nf.EUOF2yr,0) DESC) d

)
