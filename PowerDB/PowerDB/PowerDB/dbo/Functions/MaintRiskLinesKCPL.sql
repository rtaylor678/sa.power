﻿




CREATE FUNCTION [dbo].[MaintRiskLinesKCPL]
(	
	--@ListName VARCHAR(30),
	--@StudyYear INT,
	--@CRVGroup VARCHAR(30),
	--@ParentList VARCHAR(30),
	@LineNumber INT


)
RETURNS TABLE 
AS
RETURN 
(

	--this is a whole bunch of math translated from the Excel presbuilder, it creates the lines drawn on the MaintRisk charts one by one
	--basically it takes the MaintRisk group we're plotting, then figures out the maximum value for each axis
	--then rounds them up a little depending on how big they are
	--then figures out how big the gap between the lines should be (mult) based on how big they are
	--and returns the one line that is requested

	SELECT XAxis = ((mult / 10) * @LineNumber) / yMax * 100, YAxis = yMax
	FROM (
		SELECT yMax = CASE 
						WHEN yMax <= 0.5 THEN 0.5
						WHEN yMax > 0.5 AND yMax <= 1 THEN 1
						WHEN yMax > 1 AND yMax <= 2 THEN 2
						WHEN yMax > 2 THEN CEILING((yMax/5)) * 5
						END
		FROM (
			SELECT xMax = MAX(EUOF), yMax = MAX(AnnNonOHCostEGC) FROM MaintRiskKCPL()) b) c

		LEFT JOIN (SELECT mult = CASE
						WHEN mult < 0.1 THEN CEILING((mult/0.01))*0.01
						WHEN mult >= 0.1 AND mult < 0.5 then CEILING((mult/0.05))*0.05
						WHEN mult >= 0.5 AND mult < 1 then CEILING((mult/0.1))*0.1
						WHEN mult >= 1 AND mult < 2 then CEILING((mult/0.2))*0.2
						WHEN mult >= 2 AND mult < 5 then CEILING((mult/0.5))*0.5
						WHEN mult >= 5 THEN CEILING((mult/1))*1
						END
				FROM (
	
				SELECT mult = MAX((EUOF * AnnNonOHCostEGC) / 100) FROM MaintRiskKCPL())
			 e) 
		d on 1=1


UNION ALL



	SELECT XAxis, YAxis FROM (
		SELECT XAxis, YAxis = ((mult / 10) * @LineNumber) / XAxis * 100, mult FROM (
			SELECT XAxis = CAST(Num AS REAL) / 10 
			FROM PowerGlobal.dbo.Numbers
			WHERE Num <= (SELECT xMax = CASE WHEN xMax < 25 THEN CEILING((xmax/5))*5 else CEILING((xmax/10))*10 END FROM (
				SELECT xMax = MAX(EUOF), yMax = MAX(AnnNonOHCostEGC) 
				FROM MaintRiskKCPL()
				) b) * 10
			) c

		LEFT JOIN (SELECT mult = CASE
			WHEN mult < 0.1 THEN CEILING((mult/0.01))*0.01
			WHEN mult >= 0.1 AND mult < 0.5 then CEILING((mult/0.05))*0.05
			WHEN mult >= 0.5 AND mult < 1 then CEILING((mult/0.1))*0.1
			WHEN mult >= 1 AND mult < 2 then CEILING((mult/0.2))*0.2
			WHEN mult >= 2 AND mult < 5 then CEILING((mult/0.5))*0.5
			WHEN mult >= 5 THEN CEILING((mult/1))*1
			END

	FROM (
		SELECT mult = MAX((EUOF * AnnNonOHCostEGC) / 100) FROM MaintRiskKCPL()
		) b) d ON 1=1 
	) e
	
	WHERE YAxis < (SELECT yMax = CASE 
								WHEN yMax <= 0.5 THEN 0.5
								WHEN yMax > 0.5 AND yMax <= 1 THEN 1
								WHEN yMax > 1 AND yMax <= 2 THEN 2
								WHEN yMax > 2 THEN CEILING((yMax/5)) * 5
								END
	FROM (
		SELECT xMax = MAX(EUOF), yMax = MAX(AnnNonOHCostEGC) FROM MaintRiskKCPL()
	) b) 

)





