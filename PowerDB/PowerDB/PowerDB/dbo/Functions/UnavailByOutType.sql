﻿

CREATE FUNCTION [dbo].[UnavailByOutType]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30), --Brayton or Rankine
	@ShortListName VARCHAR(30), --Power13 Steam or Power13 CC
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 10 Total, Forced, Maint, Planned FROM (

	SELECT Total = '',--'Forced',
		Forced = CASE WHEN Sum(CommUnavail.PeakMWHLost_F) > 0 THEN Sum(CommUnavail.LRO_F)/sum(CommUnavail.PeakMWHLost_F)*1000  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 0 END, 
		Maint = 0,
		Planned = 0,
		SortOrder = 1
	FROM CommUnavail, TSort t, Breaks b
	WHERE t.Refnum = CommUnavail.Refnum 
		and t.refnum = b.refnum 
		and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
		and t.studyyear = @StudyYear
		and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)

UNION

	SELECT Total = '',--'Maintenance (Prescheduled)',
		Forced = 0, 
		Maint = CASE WHEN Sum(CommUnavail.PeakMWHLost_M) > 0 THEN Sum(CommUnavail.LRO_M)/sum(CommUnavail.PeakMWHLost_M)*1000  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 0 END,
		Planned = 0,
		SortOrder = 2
	FROM CommUnavail, TSort t, Breaks b
	WHERE t.Refnum = CommUnavail.Refnum 
		and t.refnum = b.refnum 
		and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
		and t.studyyear = @StudyYear
		and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)

UNION

	SELECT Total = '',--'Planned',
		Forced = 0, 
		Maint = 0,
		Planned = CASE WHEN Sum(CommUnavail.PeakMWHLost_P) > 0 THEN Sum(CommUnavail.LRO_P)/sum(CommUnavail.PeakMWHLost_P)*1000  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) ELSE 0 END,
		SortOrder = 3
	FROM CommUnavail, TSort t, Breaks b
	WHERE t.Refnum = CommUnavail.Refnum 
		and t.refnum = b.refnum 
		and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
		and t.studyyear = @StudyYear
		and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)

	) b
	ORDER BY SortOrder



	--SELECT TOP 1000 -- TOP 1000 here so we can sort it the way we want
	--	LROType = CASE LROType
	--		WHEN 'F' THEN 'Forced'
	--		WHEN 'M' THEN 'Maintenance (Prescheduled)'
	--		WHEN 'P' THEN 'Planned'
	--		END,
	--	LROTotal = [1]
	--FROM
	--(
	--	SELECT 
	--		Total, 
	--		LROType, 
	--		value, 
	--		sortorder = CASE lrotype WHEN 'F' THEN 1 WHEN 'M' THEN 2 WHEN 'P' THEN 3 END --to sort them the way we want
	--	FROM (
	--			SELECT Total = 1,
	--				CASE WHEN Sum(CommUnavail.PeakMWHLost_F) > 0 THEN Sum(CommUnavail.LRO_F)/sum(CommUnavail.PeakMWHLost_F)*1000 ELSE 0 END as F, 
	--				CASE WHEN Sum(CommUnavail.PeakMWHLost_M) > 0 THEN Sum(CommUnavail.LRO_M)/sum(CommUnavail.PeakMWHLost_M)*1000 ELSE 0 END as M,
	--				CASE WHEN Sum(CommUnavail.PeakMWHLost_P) > 0 THEN Sum(CommUnavail.LRO_P)/sum(CommUnavail.PeakMWHLost_P)*1000 ELSE 0 END as P
	--			FROM CommUnavail, TSort t, Breaks b
	--			WHERE t.Refnum = CommUnavail.Refnum 
	--				and t.refnum = b.refnum 
	--				and ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
	--				and t.studyyear = @StudyYear
	--				and t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)
	--		) c

	--	UNPIVOT (
	--		value FOR LROType IN (F, M, P)
	--		) unpiv
	--	) src

	--	PIVOT (
	--		SUM (value) FOR Total IN ([1])
	--	) piv

	--ORDER BY sortorder





	--SELECT 
	--	CASE WHEN Sum(CommUnavail.PeakMWHLost_F) > 0 THEN Sum(CommUnavail.LRO_F)/sum(CommUnavail.PeakMWHLost_F)*1000 ELSE 0 END as F, 
	--	CASE WHEN Sum(CommUnavail.PeakMWHLost_M) > 0 THEN Sum(CommUnavail.LRO_M)/sum(CommUnavail.PeakMWHLost_M)*1000 ELSE 0 END as M,
	--	CASE WHEN Sum(CommUnavail.PeakMWHLost_P) > 0 THEN Sum(CommUnavail.LRO_P)/sum(CommUnavail.PeakMWHLost_P)*1000 ELSE 0 END as P
	--FROM CommUnavail, TSort t, Breaks b
	--WHERE t.Refnum = CommUnavail.Refnum 
	--	AND t.refnum = b.refnum 
	--	AND ((@CRVGroup = 'Coal' AND b.FuelGroup = 'Coal') OR (@CRVGroup = 'Gas' AND b.SteamGasOil = 'Y'))
	--	AND t.studyyear = @StudyYear
	--	AND t.Refnum IN(SELECT Refnum FROM _RL WHERE Listname = @ListName)


)


