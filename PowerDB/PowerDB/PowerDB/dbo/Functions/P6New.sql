﻿



CREATE FUNCTION [dbo].[P6New]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@ProcessType VARCHAR(30), -- should be Boiler, CTG, Other, or Turbine
	@ParentList VARCHAR(30),
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(


	SELECT TOP 100 UnitLabel, ROSMaint, Maint, ROSPeakUnavail_Tot, PeakUnavail_Tot, ROSPeakUnavail_Unp, PeakUnavail_Unp FROM (

		SELECT UnitLabel = 'Study', 
			ROSMaint = CASE WHEN Label = 'ROSMaint' THEN Value ELSE 0 END, 
			Maint = 0, 
			ROSPeakUnavail_Tot = CASE WHEN Label = 'ROSCommUnavail' THEN Value else 0 END,
			PeakUnavail_Tot = 0,
			ROSPeakUnavail_Unp = CASE WHEN Label = 'ROSCuUnp' THEN Value else 0 END,
			PeakUnavail_Unp = 0,
			SortOrder = CASE Label WHEN 'ROSMaint' THEN 1 WHEN 'ROSCommUnavail' THEN 4 WHEN 'ROSCuUnp' THEN 7 END
		FROM SlideSourceData 
		WHERE [Type] LIKE 'Major%' 
			AND Filter1 = @FuelGroup 
			AND (Filter2 = @ProcessType OR (Filter2 = LEFT(@ProcessType, LEN(RTRIM(@ProcessType))-3) AND @ProcessType LIKE '%EGC' AND Label <> 'ROSMaint'))

			--AND ( 
			--	(@ProcessType LIKE '%EGC' AND Label like '%EGC') OR 
			--	(@ProcessType NOT LIKE '%EGC' AND Label NOT LIKE '%EGC')
			--	)

		UNION ALL
		SELECT '',0,0,0,0,0,0,3 --first spacer

		UNION ALL 
		SELECT '',0,0,0,0,0,0,6 --second spacer

		UNION ALL

		SELECT UnitLabel = 'Company',
			ROSMaint = 0,
			Maint = CASE WHEN n.Num = 2 THEN SUM(CASE LEFT(@ProcessType,3)
							WHEN 'Boi' THEN s.BoilerMaintKUS 
							WHEN 'CTG' THEN s.CTGMaintKUS 
							WHEN 'Oth' THEN s.OtherMaintKUS 
							WHEN 'Tur' THEN s.TurbineMaintKUS 
							ELSE 0 END * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear))
						/ SUM(CASE WHEN RIGHT(@ProcessType,3) = 'EGC' THEN gtc.EGC ELSE g.NMC2Yr END) ELSE 0 END,
			ROSPeakUnavail_Tot = 0,
			PeakUnavail_Tot = CASE WHEN SUM(s.TotPeakMWH) = 0 THEN 0 ELSE CASE WHEN n.Num = 5 THEN SUM(CASE LEFT(@ProcessType,3)
								WHEN 'Boi' THEN s.BoilerLostPeakMWH_Tot 
								WHEN 'CTG' THEN s.CTGLostPeakMWH_Tot 
								WHEN 'Oth' THEN s.OtherLostPeakMWH_Tot 
								WHEN 'Tur' THEN s.TurbineLostPeakMWH_Tot
							ELSE 0 END)
							/SUM(s.TotPeakMWH)*100 ELSE 0 END END,
			ROSPeakUnavail_Unp = 0,
			PeakUnavail_Unp = CASE WHEN SUM(s.TotPeakMWH) = 0 THEN 0 ELSE CASE WHEN n.num = 8 THEN SUM(CASE LEFT(@ProcessType,3)
								WHEN 'Boi' THEN s.BoilerLostPeakMWH_Unp 
								WHEN 'CTG' THEN s.CTGLostPeakMWH_Unp 
								WHEN 'Oth' THEN s.OtherLostPeakMWH_Unp 
								WHEN 'Tur' THEN s.TurbineLostPeakMWH_Unp
							ELSE 0 END)
							/SUM(s.TotPeakMWH)*100 ELSE 0 END END,
			SortOrder = n.Num

		FROM (

			SELECT t.Refnum, 
				BoilerMaintKUS = MIN(m.MajorBoiler), 
				TurbineMaintKUS = MIN(m.MajorTurbine), 
				CTGMaintKUS = MIN(m.MajorCTG), 
				OtherMaintKUS = MIN(m.MajorOther - ISNULL(m.WGS, 0) - ISNULL(m.DGS, 0)), 
				BoilerLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				TurbineLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				CTGLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				OtherLostPeakMWH_Tot = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 'Wet Gas Scrubber (WGS)') 
					THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real),
				BoilerLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Boiler' AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				TurbineLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Turbine' AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				CTGLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Combustion Turbine' AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				OtherLostPeakMWH_Unp = CAST(SUM(CASE WHEN c.SAIMajorEquip = 'Balance of Plant' AND c.SAIMinorEquip NOT IN ('Dry Gas Scrubber (DGS)', 'Wet Gas Scrubber (WGS)') 
					AND e.EVNT_Category IN ('F', 'M') THEN lro.PeakHrs * e.LostMW ELSE 0 END) AS real), 
				TotPeakMWH = MIN(cu.TotPeakMWH)

			FROM TSort t
				LEFT JOIN Breaks b ON b.Refnum = t.Refnum
				LEFT JOIN MaintEquipSum m ON m.Refnum = t.Refnum
				LEFT JOIN Events e ON e.Refnum = t.Refnum
				LEFT JOIN CauseCodes c ON c.Cause_Code = e.Cause_Code 
				LEFT JOIN CommUnavail cu ON cu.Refnum = t.Refnum 
				LEFT JOIN EventLRO lro ON lro.Refnum = e.Refnum AND lro.TurbineID = e.TurbineID AND lro.Evnt_No = e.Evnt_No AND lro.Phase = e.Phase AND lro.PricingHub = t.PricingHub
	
				WHERE t.Refnum in (SELECT Refnum FROM _RL WHERE ListName = @ListName)
					AND t.StudyYear = @StudyYear
					AND e.EVNT_Category IN ('F', 'M', 'P') 
					AND e.Cause_Code NOT IN (7777, 0)
					AND (
					(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
					(@FuelGroup = 'Gas' AND b.SteamGasOil = 'Y') OR
					(@FuelGroup = 'CE' AND b.CRVSizeGroup LIKE 'CE%') OR
					(@FuelGroup = 'CG' AND b.CRVSizeGroup LIKE 'CG%') OR
					(@FuelGroup = 'SC' AND b.CRVSizeGroup = 'SC')
					)
		
			GROUP BY t.Refnum
			) s

		LEFT JOIN NERCFactors g on g.Refnum = s.Refnum
		LEFT JOIN GenerationTotCalc gtc on gtc.Refnum = s.Refnum
		LEFT JOIN PowerGlobal.dbo.Numbers n ON 1=1
		
		WHERE n.Num IN (2,5,8) -- 2nd, 5th and 8th columns in the output

		GROUP BY n.Num

	) d

	ORDER BY SortOrder








--select top 1000 unitLabel, ROSMaint, Maint FROM (
--	SELECT UnitLabel = CASE WHEN n.Num = 1 THEN 'Study' ELSE 'Company' END,
--		ROSMaint = CASE WHEN n.Num = 1 THEN SUM(CASE WHEN x.ROS = 'Y' THEN 
--						CASE LEFT(@ProcessType,3)
--							WHEN 'Boi' THEN s.BoilerMaintKUS WHEN 'CTG' THEN s.CTGMaintKUS WHEN 'Oth' THEN s.OtherMaintKUS WHEN 'Tur' THEN s.TurbineMaintKUS 
--						END  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
--					ELSE 0 END)/SUM(CASE WHEN x.ROS = 'Y' THEN CASE WHEN RIGHT(@ProcessType,3) = 'EGC' THEN gtc.EGC ELSE g.NMC2Yr END ELSE 0.001 END)
--					ELSE 0 END,
--		Maint = CASE WHEN n.Num = 2 THEN SUM(CASE WHEN x.ROS = 'N' THEN 
--						CASE LEFT(@ProcessType,3)
--							WHEN 'Boi' THEN s.BoilerMaintKUS WHEN 'CTG' THEN s.CTGMaintKUS WHEN 'Oth' THEN s.OtherMaintKUS WHEN 'Tur' THEN s.TurbineMaintKUS 
--						END  * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
--					ELSE 0 END)/SUM(CASE WHEN x.ROS = 'N' THEN CASE WHEN RIGHT(@ProcessType,3) = 'EGC' THEN gtc.EGC ELSE g.NMC2Yr END ELSE 0.001 END)
--					ELSE 0 END,
--		Sortorder = case when n.Num = 1 then 1 else 2 end

--	FROM PowerGlobal.dbo.Numbers n
--		LEFT JOIN TwoPack s ON 1=1 --replaced SixPack
--		LEFT JOIN TSort t ON t.Refnum = s.Refnum
--		LEFT JOIN NERCFactors g ON g.Refnum = s.Refnum
--		LEFT JOIN Breaks b ON b.Refnum = s.Refnum
--		LEFT JOIN GenerationTotCalc gtc ON gtc.Refnum = s.Refnum
--		LEFT JOIN 
--			(SELECT Refnum, ROS = 'Y' FROM _RL WHERE ListName = @ParentList AND Refnum NOT IN (SELECT Refnum FROM _RL WHERE ListName = @ListName)
--			UNION
--			SELECT Refnum, ROS = 'N' FROM _RL WHERE ListName = @ListName AND Refnum IN (SELECT Refnum FROM TSort WHERE StudyYear = @StudyYear)) x ON x.Refnum = s.Refnum
--	WHERE n.Num <= 2 AND 
--	(
--		(@FuelGroup = 'Coal' AND b.FuelGroup = 'Coal') OR
--		(@FuelGroup = 'Gas' AND b.SteamGasOil = 'Y') OR
--		(@FuelGroup = 'CE' AND b.CRVSizeGroup LIKE 'CE%') OR
--		(@FuelGroup = 'CG' AND b.CRVSizeGroup LIKE 'CG%') OR
--		(@FuelGroup = 'SC' AND b.CRVSizeGroup = 'SC')
--	)

--	GROUP BY n.Num

--	union 
--	(select '',0,0,3)
--	union 
--	(select 'Study',0,0,4)
--	union 
--	(select 'Company',0,0,5)
--	union 
--	(select '',0,0,6)
--	union 
--	(select 'Study',0,0,7)
--	union 
--	(select 'Company',0,0,8)

--	) d
--	order by Sortorder

)




