﻿




CREATE FUNCTION [dbo].[MaintRiskLinesAll]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)


)
RETURNS @output TABLE (LineNumber INT, xAxis REAL, yAxis REAL) 
AS
--RETURN 
BEGIN
--call MaintRisk, get the appropriate values, pass it to MaintRiskLines

	DECLARE @MaintRisk MaintRiskTable

	INSERT @MaintRisk SELECT * FROM MaintRisk(@ListName, @StudyYear, @CRVGroup, @ShortListName, @CurrencyCode)

	--DECLARE @output TABLE (LineNumber INT, xAxis REAL, yAxis REAL)

	--a clunky way of doing this, but it works
	--there must be a more imaginitive way of doing it, but I'm pressed for time right now and will fix this later
	--(this written on 11/6/15, if you're checking on how much later is)

	--and the update which collects the MaintRiskTable here and passes it on, that was written 9/20/16
	--what that does is speeds up MaintRiskLines, because it doesnt have to call MaintRisk 5 times for each line number, so we save 45 calls to the db ((5 * 10) - 5)

	INSERT @output (LineNumber, xAxis, yAxis)
	select top 100000 * FROM (
		select LineNumber = 1, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,1,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 2, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,2,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 3, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,3,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 4, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,4,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 5, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,5,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 6, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,6,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 7, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,7,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 8, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,8,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 9, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,9,@CurrencyCode, @MaintRisk)
		union
		select LineNumber = 10, * from MaintRiskLines(@ListName,@StudyYear,@CRVGroup,@ShortListName,10,@CurrencyCode, @MaintRisk)
	) b
	order by LineNumber, XAxis, YAxis

	RETURN

END

