﻿



CREATE FUNCTION [dbo].[zzzP6All]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@ProcessType VARCHAR(30), -- should be Boiler, CTG, Other, or Turbine
	@ParentList VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

	--SELECT TOP 1000 UnitLabel, ROSMaint, Maint, ROSPeakUnavail_Tot, PeakUnavail_Tot, ROSPeakUnavail_Unp, PeakUnavail_Unp
	SELECT TOP 1000 UnitLabel = '', ROSMaint, Maint, ROSPeakUnavail_Tot, PeakUnavail_Tot, ROSPeakUnavail_Unp, PeakUnavail_Unp
	FROM (
		SELECT UnitLabel, ROSMaint, Maint, ROSPeakUnavail_Tot = 0, PeakUnavail_Tot = 0, ROSPeakUnavail_Unp = 0, PeakUnavail_Unp = 0, RefListNo from P6 (@ListName,@StudyYear,@FuelGroup,@ProcessType,@ParentList)
		union
		select * from P6b (@ListName,@StudyYear,@FuelGroup,@ProcessType,@ParentList)
	) b 
	ORDER BY RefListNo

)




