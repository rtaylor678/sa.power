﻿



CREATE FUNCTION [dbo].[P6Labels]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@FuelGroup VARCHAR(30),
	@ProcessType VARCHAR(30), -- should be Boiler, CTG, Other, or Turbine	//	or BoilerEGC, CTGEGC, OtherEGC, TurbineEGC
	@ShortListName VARCHAR(30),
	@CurrencyCode VARCHAR(4)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT * 
	FROM
		(SELECT Label = 'MI', xValue = 2.25 UNION SELECT 'Unavailability', 4.41) a --need two rows in the x column, magic numbers 2.25 and 4.41 align the words MI and Unavailability correctly
	LEFT JOIN (
		SELECT yValue = 0.975 * CASE
				--WHEN CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END < 0.1 THEN CEILING(((CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END)/0.01))*0.01
				--WHEN CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END >= 0.1 AND CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END < 0.5 then CEILING(((CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END)/0.05))*0.05
				--WHEN CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END >= 0.5 AND CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END < 1 then CEILING(((CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END)/0.1))*0.1
				--WHEN CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END >= 1 AND CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END < 2 then CEILING(((CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END)/0.2))*0.2
				--WHEN CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END >= 2 AND CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END < 5 then CEILING(((CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END)/0.5))*0.5
				--WHEN CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END >= 5 THEN CEILING(((CASE WHEN ROSMaint > Maint THEN ROSMaint ELSE Maint/0.95 END)/1))*1

			WHEN MaxValue/0.95 < 0.1 THEN CEILING((MaxValue/0.95 /0.01))*0.01
			WHEN MaxValue/0.95 >= 0.1 AND MaxValue/0.95 < 0.5 then CEILING((MaxValue/0.95/0.05))*0.05
			WHEN MaxValue/0.95 >= 0.5 AND MaxValue/0.95 < 1 then CEILING((MaxValue/0.95/0.1))*0.1
			WHEN MaxValue/0.95 >= 1 AND MaxValue/0.95 < 2 then CEILING((MaxValue/0.95/0.2))*0.2
			WHEN MaxValue/0.95 >= 2 AND MaxValue/0.95 < 5 then CEILING((MaxValue/0.95/0.5))*0.5
			WHEN MaxValue/0.95 >= 5 THEN CEILING((MaxValue/0.95/1))*1

				END
		FROM (
			--SELECT ROSMaint = MAX(ROSMaint), Maint = MAX(Maint)
			--FROM P6 (@ListName, @StudyYear, @FuelGroup, @ProcessType, @ShortListName, @CurrencyCode)

			SELECT MaxValue = MAX(MaxValue) FROM (
				SELECT (SELECT Max(v) 
				FROM (VALUES (ROSMaint), (Maint)) AS value(v)) as MaxValue
				FROM P6 (@ListName, @StudyYear, @FuelGroup, @ProcessType, @ShortListName, @CurrencyCode)
				)d

			) b
		) c on 1=1
)
