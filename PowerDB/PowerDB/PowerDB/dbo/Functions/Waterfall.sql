﻿


CREATE FUNCTION [dbo].[Waterfall]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ShortListName VARCHAR(30),
	@DataType VARCHAR(10),
	@BySite BIT, -- 0 if by unit, 1 if by site
	@CurrencyCode VARCHAR(4),
	@IsPacesetter BIT --if 1 then add " Pacesetters" to the end of the ParentList

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT TOP 1000 
		Title, 
		Title2, 
		BeginCell = CAST(ROUND(BeginCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Gap1 = CAST(ROUND(Gap1 * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Label = CAST(ROUND(Label * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Down = CAST(ROUND(Down * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		Up = CAST(ROUND(Up * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money), 
		EndCell = CAST(ROUND(EndCell * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear),2) AS money) 
	FROM (
		SELECT * FROM WaterfallTop (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)

		UNION ALL

		SELECT * FROM WaterfallBot (@ListName, @StudyYear, @ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)

		UNION ALL

		SELECT title = wes.unitname, Title2 = wes.title, null,wlj.val4, ABS(Value),case when Value < 0 then ABS(value) else null end,case when Value >= 0 then ABS(value) else null end, null, wes.rank + 1 
			from WaterfallEventSummary(@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite) wes
			left join (SELECT * FROM Waterfallleftjoin (@ListName,@StudyYear,@ShortListName + CASE WHEN @IsPacesetter = 1 THEN ' Pacesetters' ELSE '' END, @DataType, @BySite)) wlj on wlj.title2 = wes.unitname and wlj.title = wes.title
		) b
	ORDER BY title, Sort



)



