﻿


CREATE FUNCTION [dbo].[LROByTechPercent]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@AllLRO BIT, -- if 1 then function returns LRO_Tot, else it returns Unplanned LRO
	@CurrencyCode VARCHAR(4)
)
RETURNS TABLE 
AS
RETURN 
(

	--this is terrible code, but it works. change it, how to do a running total for each part?

	SELECT TOP 1000 LROType, Pct FROM (

	SELECT LROType = 'Rankine (Coal)', 
		Pct = CASE WHEN SumLROTotal = 0 THEN 0 ELSE LROTotal/SUMLROTotal * 100 END,
		sortorder = 1
	FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(LROTotal) FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode)) b ON 1 = 1
	WHERE LROType = 'Rankine (Coal)'

	UNION

	SELECT LROType = 'Rankine (Gas & Oil)', 
		Pct = CASE WHEN SumLROTotal = 0 THEN 0 ELSE SUM(LROTotal)/SUMLROTotal * 100 END,
		sortorder = 2
	FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(LROTotal) FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode)) b ON 1 = 1
	WHERE LROType IN ('Rankine (Coal)', 'Rankine (Gas & Oil)')
	GROUP BY SumLROTotal

	UNION

	SELECT LROType = 'Simple & Combined Cycle', 
		Pct = CASE WHEN SumLROTotal = 0 THEN 0 ELSE SUM(LROTotal)/SUMLROTotal * 100 END,
		sortorder = 3
	FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(LROTotal) FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode)) b ON 1 = 1
	WHERE LROType IN ('Rankine (Coal)', 'Rankine (Gas & Oil)','Simple & Combined Cycle')
	GROUP BY SumLROTotal

	UNION

	SELECT LROType = 'Cogeneration', 
		Pct = CASE WHEN SumLROTotal = 0 THEN 0 ELSE SUM(LROTotal)/SUMLROTotal * 100 END,
		sortorder = 4
	FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode) 
		LEFT JOIN ( SELECT SumLROTotal = SUM(LROTotal) FROM LROByTech (@ListName, @StudyYear, @AllLRO, @CurrencyCode)) b ON 1 = 1
	--WHERE LROType IN ('Rankine (Coal)', 'Rankine (Gas & Oil)','Simple & Combined Cycle') --don't need the where, we're pulling all the data at this point
	GROUP BY SumLROTotal
	) b

	ORDER BY sortorder

)



