﻿




CREATE FUNCTION [dbo].[RepEventsData]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@ByUnit BIT, -- all units (0) or each unit individually (1)
	@Filter VARCHAR(10), -- Coal, Gas & Oil, or ''
	@ResultType INT, -- total LRO (0) or LRO/MWh (1) or Count (2)
	@RepetitiveEvents BIT, -- all events (0) or only repetitive (1) (repetitive is defined as >3 of the same cause code)
	@CurrencyCode VARCHAR(4)
)
RETURNS TABLE 
AS
RETURN 
(


	SELECT SiteLabel2, SiteLabel3 = '',--SiteLabel2,
		LRO = CASE @ResultType
			WHEN 0 THEN LRO * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
			WHEN 1 THEN LROMWh * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear)
			WHEN 2 THEN LROMWh * GlobalDB.dbo.CurrencyPerUSD(@CurrencyCode, @StudyYear) -- is sorted by count of events, but chart displays LRO/MWh
			END
	FROM RepEventsParent(@ListName,@StudyYear,@ByUnit,@Filter,@ResultType,@RepetitiveEvents)


)



