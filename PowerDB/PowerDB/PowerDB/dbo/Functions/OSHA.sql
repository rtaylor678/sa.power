﻿
CREATE FUNCTION [dbo].[OSHA]
(	
	@ListName VARCHAR(30),
	@StudyYear INT,
	@CRVGroup VARCHAR(30),
	@ShortListName VARCHAR(30)

)
RETURNS TABLE 
AS
RETURN 
(

	SELECT UnitLabel = 'Study Avg',
		OSHARate = CASE WHEN SUM(p.SiteEffPers) = 0 THEN 0 ELSE SUM(g.OSHARate * p.SiteEffPers)/SUM(p.SiteEffPers) END
		FROM TSort t
			LEFT JOIN GenSum g ON g.Refnum = t.Refnum
			LEFT JOIN PersSTCalc p ON p.Refnum = t.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ShortListName)

	UNION ALL

	SELECT UnitLabel, OSHARate FROM (
		SELECT UnitLabel = 'Co Avg', 
			OSHARate = CASE WHEN SUM(p.SiteEffPers) = 0 THEN 0 ELSE SUM(g.OSHARate * p.SiteEffPers)/SUM(p.SiteEffPers) END,
			COUNT(DISTINCT SiteID) AS cnt
		FROM TSort t
			LEFT JOIN Breaks b ON b.Refnum = t.Refnum
			LEFT JOIN GenSum g ON g.Refnum = t.Refnum
			LEFT JOIN PersSTCalc p ON p.Refnum = t.Refnum
		WHERE t.StudyYear = @StudyYear 
			AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
			AND t.Refnum NOT LIKE '%G'
		)b 
	WHERE cnt > 1

	UNION ALL

	SELECT SiteLabel, OSHARate FROM (
	SELECT TOP 1000 SiteLabel = rtrim(ss.SiteLabel), 
		OSHARate = CASE WHEN SUM(p.SiteEffPers) = 0 THEN 0 ELSE SUM(g.OSHARate * p.SiteEffPers)/SUM(p.SiteEffPers) END,
		SortOrder = CASE WHEN t.Refnum LIKE '%G' THEN 1 ELSE 0 END
	FROM TSort t 
		INNER JOIN PersSTCalc p ON p.Refnum = t.Refnum AND p.SectionID = 'TP' 
		INNER JOIN StudySites ss on t.SiteID = ss.SiteID
		INNER JOIN Gensum g ON g.Refnum = t.Refnum
	WHERE t.studyyear = @StudyYear
		AND t.Refnum IN (SELECT Refnum FROM _RL WHERE Listname = @ListName)
	GROUP BY ss.SiteLabel, CASE WHEN t.Refnum LIKE '%G' THEN 1 ELSE 0 END
	ORDER BY SortOrder, OSHARate DESC) c

)

