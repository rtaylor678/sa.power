﻿CREATE TABLE [dbo].[PresSlides] (
    [PresID]      INT          NOT NULL,
    [SlideID]     INT          NOT NULL,
    [SortOrder]   INT          NULL,
    [Active]      BIT          NULL,
    [UserFilter1] VARCHAR (20) NULL,
    [UserFilter2] VARCHAR (20) NULL
);

