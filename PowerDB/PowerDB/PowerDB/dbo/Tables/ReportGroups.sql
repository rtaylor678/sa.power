﻿CREATE TABLE [dbo].[ReportGroups] (
    [RefNum]             CHAR (20)      NOT NULL,
    [ReportTitle]        CHAR (50)      NOT NULL,
    [ListName]           CHAR (20)      NOT NULL,
    [Field1Name]         VARCHAR (20)   NULL,
    [Field1Value]        VARCHAR (20)   NULL,
    [Field2Name]         VARCHAR (20)   NULL,
    [Field2Value]        VARCHAR (20)   NULL,
    [TileDescription]    VARCHAR (40)   NULL,
    [TileListName]       VARCHAR (30)   NULL,
    [TileBreakValue]     VARCHAR (12)   NULL,
    [TileBreakCondition] VARCHAR (30)   NULL,
    [TileTile]           TINYINT        NULL,
    [SpecialCriteria]    NVARCHAR (100) NULL,
    CONSTRAINT [PK_ReportGroups2] PRIMARY KEY CLUSTERED ([RefNum] ASC)
);

