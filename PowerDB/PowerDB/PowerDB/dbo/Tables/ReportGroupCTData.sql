﻿CREATE TABLE [dbo].[ReportGroupCTData] (
    [RefNum]                      CHAR (20) NOT NULL,
    [ReportTitle]                 CHAR (50) NOT NULL,
    [ListName]                    CHAR (20) NOT NULL,
    [NMC]                         REAL      NULL,
    [PriorNetGWH]                 REAL      NULL,
    [NetGWH]                      REAL      NULL,
    [ServiceHours]                REAL      NULL,
    [NCF2Yr]                      REAL      NULL,
    [NOF2Yr]                      REAL      NULL,
    [FiringTemp]                  REAL      NULL,
    [MaintCostPerStart]           REAL      NULL,
    [AvgStartsPerCT]              REAL      NULL,
    [EquivStarts]                 REAL      NULL,
    [EquivRunHrs]                 REAL      NULL,
    [StartSuccessPct]             REAL      NULL,
    [ForcedOutagePct]             REAL      NULL,
    [MaintenancePct]              REAL      NULL,
    [StartupFailurePct]           REAL      NULL,
    [DeratedPct]                  REAL      NULL,
    [HHVHeatRateKWH]              REAL      NULL,
    [LHVHeatRateKWH]              REAL      NULL,
    [HrsBetweenCombInspection]    REAL      NULL,
    [HrsBetweenHGPInspection]     REAL      NULL,
    [HrsBetweenMajorOverhaul]     REAL      NULL,
    [StartsBetweenCombInspection] REAL      NULL,
    [StartsBetweenHGPInspection]  REAL      NULL,
    [StartsBetweenMajorOverhaul]  REAL      NULL
);

