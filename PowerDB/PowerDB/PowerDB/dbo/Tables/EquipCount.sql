﻿CREATE TABLE [dbo].[EquipCount] (
    [Refnum]           [dbo].[Refnum]    NOT NULL,
    [TurbineID]        [dbo].[TurbineID] NOT NULL,
    [NumBoilers]       SMALLINT          NULL,
    [NumSTGs]          SMALLINT          NULL,
    [BoilerPressure]   SMALLINT          NULL,
    [BoilerTemp]       SMALLINT          NULL,
    [ReheatPressure]   SMALLINT          NULL,
    [ReheatTemp]       SMALLINT          NULL,
    [CondensateStages] SMALLINT          NULL,
    CONSTRAINT [PK_EquipCount_1__10] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC) WITH (FILLFACTOR = 90)
);

