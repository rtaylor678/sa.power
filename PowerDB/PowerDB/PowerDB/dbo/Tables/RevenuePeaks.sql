﻿CREATE TABLE [dbo].[RevenuePeaks] (
    [Refnum]          [dbo].[Refnum] NOT NULL,
    [PricingHub]      CHAR (15)      NOT NULL,
    [StartTime]       DATETIME       NOT NULL,
    [EndTime]         DATETIME       NOT NULL,
    [Price]           REAL           NULL,
    [VarCost]         REAL           NULL,
    [Penalty]         REAL           NULL,
    [MRO]             REAL           NULL,
    [PotentialNetMWH] REAL           NULL,
    CONSTRAINT [PK_RevenuePeaks2] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PricingHub] ASC, [StartTime] ASC) WITH (FILLFACTOR = 90)
);

