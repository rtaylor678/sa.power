﻿CREATE TABLE [dbo].[EventLRO] (
    [Refnum]     [dbo].[Refnum]    NOT NULL,
    [TurbineID]  [dbo].[TurbineID] NOT NULL,
    [PricingHub] CHAR (15)         NOT NULL,
    [EVNT_NO]    SMALLINT          NOT NULL,
    [PHASE]      TINYINT           NOT NULL,
    [PeakHrs]    REAL              NULL,
    [LRO]        REAL              NULL,
    CONSTRAINT [PK_EventLRO] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [PricingHub] ASC, [EVNT_NO] ASC, [PHASE] ASC)
);

