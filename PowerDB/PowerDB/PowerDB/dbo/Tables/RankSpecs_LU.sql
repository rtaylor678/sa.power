﻿CREATE TABLE [dbo].[RankSpecs_LU] (
    [RankSpecsID]    INT               IDENTITY (1, 1) NOT NULL,
    [RefListNo]      [dbo].[RefListNo] NOT NULL,
    [BreakID]        INT               NOT NULL,
    [BreakValue]     CHAR (12)         NOT NULL,
    [RankVariableID] SMALLINT          NOT NULL,
    [LastTime]       SMALLDATETIME     NOT NULL,
    [NumTiles]       TINYINT           NULL,
    [DataCount]      SMALLINT          NULL
);

