﻿CREATE TABLE [dbo].[ReportQueue] (
    [ReportSetID]   INT         NOT NULL,
    [TimeSubmitted] DATETIME    NOT NULL,
    [WhoSubmitted]  VARCHAR (5) NULL
);

