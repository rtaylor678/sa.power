﻿CREATE TABLE [dbo].[PowerGeneration] (
    [Refnum]          [dbo].[Refnum]    NOT NULL,
    [TurbineID]       [dbo].[TurbineID] NOT NULL,
    [Period]          [dbo].[GenPeriod] NOT NULL,
    [GrossMWH]        REAL              NULL,
    [StationSvcElec]  REAL              NULL,
    [NetMWH]          REAL              NULL,
    [ColdStarts]      INT               NULL,
    [HotStarts]       INT               NULL,
    [SROperHrs]       REAL              NULL,
    [HSOperHrs]       REAL              NULL,
    [AutoGenCtrlPcnt] REAL              NULL,
    [CTCombInletTemp] REAL              NULL,
    [ElecMBTU]        REAL              NULL,
    CONSTRAINT [PK_PowerGen] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [Period] ASC) WITH (FILLFACTOR = 90)
);

