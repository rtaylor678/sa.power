﻿CREATE TABLE [dbo].[StudySites] (
    [SiteID]        [dbo].[SiteID] NOT NULL,
    [CompanyID]     CHAR (15)      NOT NULL,
    [SiteName]      VARCHAR (50)   NOT NULL,
    [SiteLabel]     CHAR (5)       NULL,
    [SiteNo]        CHAR (5)       NOT NULL,
    [StudyYear]     INT            NOT NULL,
    [SiteDirectory] VARCHAR (100)  NULL,
    [Consultant]    CHAR (4)       NULL,
    CONSTRAINT [PK_StudySites] PRIMARY KEY CLUSTERED ([SiteID] ASC) WITH (FILLFACTOR = 90)
);

