﻿CREATE TABLE [dbo].[ClientInfo] (
    [SiteID]         [dbo].[SiteID] NOT NULL,
    [CorpName]       CHAR (50)      NULL,
    [AffName]        CHAR (50)      NULL,
    [PlantName]      CHAR (50)      NOT NULL,
    [City]           CHAR (30)      NULL,
    [State]          CHAR (20)      NULL,
    [CoordName]      CHAR (40)      NULL,
    [CoordTitle]     CHAR (50)      NULL,
    [CoordAddr1]     CHAR (50)      NULL,
    [CoordAddr2]     CHAR (50)      NULL,
    [CoordCity]      CHAR (30)      NULL,
    [CoordState]     CHAR (20)      NULL,
    [CoordZip]       CHAR (15)      NULL,
    [CoordPhone]     CHAR (30)      NULL,
    [CoordFax]       CHAR (20)      NULL,
    [CoordEMail]     CHAR (40)      NULL,
    [RptUOM]         VARCHAR (3)    NULL,
    [RptHV]          VARCHAR (3)    NULL,
    [RptSteamMethod] VARCHAR (3)    NULL,
    CONSTRAINT [PK_ClientInfo_2__19] PRIMARY KEY CLUSTERED ([SiteID] ASC) WITH (FILLFACTOR = 90)
);

