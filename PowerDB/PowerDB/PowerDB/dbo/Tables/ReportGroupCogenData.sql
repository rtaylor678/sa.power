﻿CREATE TABLE [dbo].[ReportGroupCogenData] (
    [RefNum]                 CHAR (20) NOT NULL,
    [ReportTitle]            CHAR (50) NOT NULL,
    [ListName]               CHAR (20) NOT NULL,
    [PercentThermal]         REAL      NULL,
    [SteamRedundancyFactor]  REAL      NULL,
    [ProcCycleMassReturns]   REAL      NULL,
    [TotEnergyOutput]        REAL      NULL,
    [ThermalEffPowerBlock]   REAL      NULL,
    [ThermEffCTG]            REAL      NULL,
    [HeatRateElectricOnly]   REAL      NULL,
    [NCFThermalPct]          REAL      NULL,
    [EUFThermalPct]          REAL      NULL,
    [NCFElectricPct]         REAL      NULL,
    [NOFElectricPct]         REAL      NULL,
    [EFORElectricPct]        REAL      NULL,
    [TotCashExpendMBTU]      REAL      NULL,
    [TotCashLessFuelMBTU]    REAL      NULL,
    [PlantManageableExpMBTU] REAL      NULL,
    [MaintenanceIndexMBTU]   REAL      NULL
);

