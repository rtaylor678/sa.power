﻿CREATE TABLE [dbo].[CTGData] (
    [Refnum]       [dbo].[Refnum] NOT NULL,
    [TurbineID]    CHAR (6)       NOT NULL,
    [TurbineName]  CHAR (35)      NOT NULL,
    [Manufacturer] CHAR (35)      NULL,
    [FrameModel]   CHAR (35)      NULL,
    [FiringTemp]   REAL           NULL,
    [ThermEff]     REAL           NULL,
    [ElecMBTU]     REAL           NULL,
    [FuelMBTU]     REAL           NULL,
    [NameplateOEM] CHAR (35)      NULL,
    [Frequency]    REAL           NULL,
    [Class]        CHAR (4)       NULL,
    [SingleShaft]  [dbo].[YorN]   NULL,
    CONSTRAINT [PK_CTGData] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC)
);

