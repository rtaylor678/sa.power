﻿CREATE TABLE [dbo].[ComponentGaps] (
    [Refnum]             [dbo].[Refnum] NOT NULL,
    [EquipGroup]         CHAR (8)       NOT NULL,
    [TargetGroup]        CHAR (12)      NOT NULL,
    [AnnMaintCostMWHGap] REAL           NULL,
    [AnnMaintCostMWH]    REAL           NULL,
    [Target]             REAL           NULL,
    CONSTRAINT [PK_ComponentGaps_1__15] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EquipGroup] ASC) WITH (FILLFACTOR = 90)
);

