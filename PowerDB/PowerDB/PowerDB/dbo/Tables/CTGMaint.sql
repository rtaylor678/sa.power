﻿CREATE TABLE [dbo].[CTGMaint] (
    [Refnum]           [dbo].[Refnum] NOT NULL,
    [TurbineID]        CHAR (6)       NOT NULL,
    [ProjectID]        CHAR (15)      NOT NULL,
    [InspDate]         SMALLDATETIME  NULL,
    [PrevInspDate]     SMALLDATETIME  NULL,
    [PartialLoadTrips] SMALLINT       NULL,
    [FullLoadTrips]    SMALLINT       NULL,
    [HotStarts]        SMALLINT       NULL,
    [ColdStarts]       SMALLINT       NULL,
    [EquivStarts]      SMALLINT       NULL,
    [RunHrs]           INT            NULL,
    [EquivRunHrs]      INT            NULL,
    [TotStarts]        SMALLINT       NULL,
    CONSTRAINT [PK_CTGMaintCC] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [ProjectID] ASC) WITH (FILLFACTOR = 90)
);

