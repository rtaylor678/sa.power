﻿CREATE TABLE [dbo].[WaterfallData] (
    [Refnum]    CHAR (12)    NULL,
    [Title]     VARCHAR (30) NULL,
    [Title2]    VARCHAR (30) NULL,
    [BeginCell] REAL         NULL,
    [Gap1]      REAL         NULL,
    [Label]     REAL         NULL,
    [Down]      REAL         NULL,
    [Up]        REAL         NULL,
    [EndCell]   REAL         NULL,
    [sort]      INT          NULL,
    [DataType]  VARCHAR (10) NULL
);

