﻿CREATE TABLE [dbo].[SlidesToBuild] (
    [SlideID]    INT NULL,
    [SortOrder]  INT NULL,
    [Active]     BIT NULL,
    [TempActive] BIT NULL
);

