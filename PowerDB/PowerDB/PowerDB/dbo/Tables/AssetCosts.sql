﻿CREATE TABLE [dbo].[AssetCosts] (
    [Refnum]        [dbo].[Refnum]    NOT NULL,
    [TurbineID]     [dbo].[TurbineID] NOT NULL,
    [Equipment]     CHAR (40)         NOT NULL,
    [InitialAdd]    CHAR (1)          NOT NULL,
    [Cost]          REAL              NULL,
    [CostLocal]     REAL              NULL,
    [YearInService] INT               NULL,
    [CurrencyID]    INT               NULL,
    CONSTRAINT [PK_AssetCosts] PRIMARY KEY CLUSTERED ([Refnum] ASC, [TurbineID] ASC, [Equipment] ASC, [InitialAdd] ASC)
);

