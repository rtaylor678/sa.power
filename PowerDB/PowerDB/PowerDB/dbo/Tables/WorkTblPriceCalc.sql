﻿CREATE TABLE [dbo].[WorkTblPriceCalc] (
    [UTIL_CODE] CHAR (3)   NOT NULL,
    [UNIT_CODE] CHAR (3)   NOT NULL,
    [EVNT_YEAR] SMALLINT   NOT NULL,
    [EVNT_NO]   SMALLINT   NOT NULL,
    [PHASE]     TINYINT    NOT NULL,
    [PeakHrs]   FLOAT (53) NULL,
    [LRO]       FLOAT (53) NULL
);

