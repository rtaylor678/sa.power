﻿CREATE TABLE [dbo].[AlternateHubs] (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [PricingHub] CHAR (15)      NOT NULL,
    CONSTRAINT [PK_AlternateHubs] PRIMARY KEY CLUSTERED ([Refnum] ASC, [PricingHub] ASC)
);

