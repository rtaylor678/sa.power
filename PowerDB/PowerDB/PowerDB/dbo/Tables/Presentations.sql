﻿CREATE TABLE [dbo].[Presentations] (
    [PresID]   INT          IDENTITY (1, 1) NOT NULL,
    [PresName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PresName] UNIQUE NONCLUSTERED ([PresName] ASC)
);

