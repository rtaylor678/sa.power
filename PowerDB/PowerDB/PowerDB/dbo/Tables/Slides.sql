﻿CREATE TABLE [dbo].[Slides] (
    [SlideID]     INT            IDENTITY (1, 1) NOT NULL,
    [Name]        VARCHAR (50)   NOT NULL,
    [Title]       VARCHAR (50)   NOT NULL,
    [SubTitle]    VARCHAR (100)  NULL,
    [Multi]       REAL           NULL,
    [Sub]         NVARCHAR (100) NULL,
    [SortOrder]   INT            NULL,
    [Active]      BIT            NULL,
    [SlideLayout] VARCHAR (50)   NULL,
    CONSTRAINT [SlideName] UNIQUE NONCLUSTERED ([Name] ASC)
);

