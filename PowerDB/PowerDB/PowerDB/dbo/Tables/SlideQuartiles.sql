﻿CREATE TABLE [dbo].[SlideQuartiles] (
    [ListName]       VARCHAR (50) NOT NULL,
    [Year]           VARCHAR (50) NOT NULL,
    [PeerGroup]      VARCHAR (50) NULL,
    [RankVariableID] INT          NULL,
    [Q12]            REAL         NULL,
    [Q34]            REAL         NULL
);

