﻿CREATE TABLE [dbo].[CRVCalc] (
    [Refnum]        [dbo].[Refnum] NOT NULL,
    [CRVGroup]      CHAR (5)       NOT NULL,
    [CRVActual]     REAL           NULL,
    [CRVPredicited] REAL           NULL,
    [CRVVariance]   REAL           NULL,
    [CRVFuel]       REAL           NULL,
    [CRVTotOpex]    REAL           NULL,
    [CRVUnplanned]  REAL           NULL,
    [CRVRevenue]    REAL           NULL,
    CONSTRAINT [PK_CRVCalcs] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

