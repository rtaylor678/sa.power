﻿CREATE TABLE [dbo].[DistLists] (
    [DistID]   INT          IDENTITY (1, 1) NOT NULL,
    [ListName] VARCHAR (50) NOT NULL,
    [Group1]   VARCHAR (50) NULL,
    [Group2]   VARCHAR (50) NULL,
    [Percent]  INT          NULL,
    [Value]    REAL         NULL,
    [DistName] VARCHAR (50) NULL
);

