﻿CREATE TABLE [dbo].[SiteSafety] (
    [SiteID]          [dbo].[SiteID] NOT NULL,
    [CompOSHAIncRate] REAL           NULL,
    [ContOSHAIncRate] REAL           NULL,
    [OSHAIncRate]     REAL           NULL,
    [LostTimeInjNum]  SMALLINT       NULL,
    [FatalNum]        SMALLINT       NULL,
    CONSTRAINT [PK_Safety] PRIMARY KEY CLUSTERED ([SiteID] ASC)
);

