﻿CREATE TABLE [dbo].[CoalTotCalc] (
    [Refnum]            [dbo].[Refnum] NOT NULL,
    [TotTons]           REAL           NULL,
    [TotTonMiles]       REAL           NULL,
    [TransCostTM]       REAL           NULL,
    [MineCostTon]       REAL           NULL,
    [TransCostTon]      REAL           NULL,
    [OnSitePrepCostTon] REAL           NULL,
    [TotCostTon]        REAL           NULL,
    [MBTU]              REAL           NULL,
    [HeatValue]         REAL           NULL,
    [AshPcnt]           REAL           NULL,
    [SulfurPcnt]        REAL           NULL,
    [MoistPcnt]         REAL           NULL,
    [HardgroveGrind]    REAL           NULL,
    [LHV]               REAL           NULL,
    [MBTULHV]           REAL           NULL,
    [DeliveredCostTon]  REAL           NULL,
    CONSTRAINT [PK___3__13] PRIMARY KEY CLUSTERED ([Refnum] ASC) WITH (FILLFACTOR = 90)
);

