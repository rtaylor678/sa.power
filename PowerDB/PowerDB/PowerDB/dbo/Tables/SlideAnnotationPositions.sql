﻿CREATE TABLE [dbo].[SlideAnnotationPositions] (
    [SlideAnnotationPositionID] INT  IDENTITY (1, 1) NOT NULL,
    [AnnotationPosition]        INT  NOT NULL,
    [Left]                      REAL NULL,
    [Top]                       REAL NULL,
    [Width]                     REAL NULL,
    [Height]                    REAL NULL,
    [FontHeight]                INT  NULL,
    [SlideColorGroup]           INT  NULL,
    [Wrap]                      BIT  NULL,
    [Alignment]                 INT  NULL,
    [BackColor]                 BIT  NULL
);

