﻿CREATE DEFAULT [dbo].[defBitFalse]
    AS 0;


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitFalse]', @objname = N'[dbo].[TSort].[EGCManualTech]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defBitFalse]', @objname = N'[dbo].[CalcQueue_dont_use].[Compare]';

