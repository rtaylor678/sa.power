﻿
CREATE VIEW [Report].[OHInterval]  AS 
SELECT Refnum, Boilers = [BOIL], STG_HPS = [STG-HPS], STG_IPS = [STG-IPS], STG_LPS = [STG-LPS], Generators = [ALL-GEN], CTG_TURB = [CTG-TURB], HRSG = [ALL-HRSG]
FROM (SELECT i.Refnum, i.Component, ds.IntervalPP 
	FROM Report.EquipOHIntervalRaw i LEFT JOIN Report.ReportGroups rg ON rg.Refnum = i.Refnum
	CROSS APPLY (VALUES (CASE WHEN ISNULL(rg.PostProcess,'N') = 'N' OR (i.UnitCount >= 1 AND i.CompanyCount >= 1) THEN i.IntervalMo END)) ds(IntervalPP)) src
PIVOT (AVG(IntervalPP) FOR Component IN ([BOIL],[STG-HPS],[STG-IPS],[STG-LPS],[ALL-GEN],[CTG-TURB],[ALL-HRSG])) pvt

