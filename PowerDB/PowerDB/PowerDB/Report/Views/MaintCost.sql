﻿CREATE VIEW Report.MaintCost
AS
SELECT m.Refnum, x.DataType, x.AnnMaintCost, x.AnnMaintCost_Min2, x.AnnMaintCost_Max2, x.AnnNonOHCost, x.AnnOHCost, x.AnnOHProjCost, x.AnnLTSACost, x.AnnLTSACost_NZ, m.tsUpdate
FROM Report.MaintTotCalc m
CROSS APPLY (VALUES 
	 ('$/MWH', AnnMaintCostMWH, AnnMaintCostMWH_Min2, AnnMaintCostMWH_Max2, AnnNonOHCostMWH, AnnOHCostMWH, AnnOHProjCostMWH, AnnLTSACostMWH, AnnLTSACostMWH_NZ)
	,('K$/MW', AnnMaintCostMW, AnnMaintCostMW_Min2, AnnMaintCostMW_Max2, AnnNonOHCostMW, AnnOHCostMW, AnnOHProjCostMW, AnnLTSACostMW, AnnLTSACostMW_NZ)
	,('$/MBTU', AnnMaintCostMBTU, AnnMaintCostMBTU_Min2, AnnMaintCostMBTU_Max2, AnnNonOHCostMBTU, AnnOHCostMBTU, AnnOHProjCostMBTU, AnnLTSACostMBTU, AnnLTSACostMBTU_NZ)
	,('$/EGC', AnnMaintCostEGC, AnnMaintCostEGC_Min2, AnnMaintCostEGC_Max2, AnnNonOHCostEGC, AnnOHCostEGC, AnnOHProjCostEGC, AnnLTSACostEGC, AnnLTSACostEGC_NZ)
) x(DataType, AnnMaintCost, AnnMaintCost_Min2, AnnMaintCost_Max2, AnnNonOHCost, AnnOHCost, AnnOHProjCost, AnnLTSACost, AnnLTSACost_NZ) 

