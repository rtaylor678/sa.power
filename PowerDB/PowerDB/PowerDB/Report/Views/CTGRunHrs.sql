﻿
CREATE VIEW [Report].[CTGRunHrs]
AS
SELECT Refnum, CombInsp = [COMBINSP], HGPInsp = [HGPINSP], Overhaul = [OVHL]
FROM (SELECT i.Refnum, i.ProjectID, ds.RunHrsPP 
	FROM Report.CTGRunHrsRaw i LEFT JOIN Report.ReportGroups rg ON rg.Refnum = i.Refnum
	CROSS APPLY (VALUES (CASE WHEN ISNULL(rg.PostProcess,'N') = 'N' OR (i.UnitCount >= 1 AND i.CompanyCount >= 1) THEN i.RunHrs END)) ds(RunHrsPP)) src
PIVOT (AVG(RunHrsPP) FOR ProjectID IN ([COMBINSP],[HGPINSP],[OVHL])) pvt
