﻿
CREATE VIEW [Report].[CTGTotStarts]
AS
SELECT Refnum, CombInsp = [COMBINSP], HGPInsp = [HGPINSP], Overhaul = [OVHL]
FROM (SELECT i.Refnum, i.ProjectID, ds.TotStartsPP 
	FROM Report.CTGTotStartsRaw i LEFT JOIN Report.ReportGroups rg ON rg.Refnum = i.Refnum
	CROSS APPLY (VALUES (CASE WHEN ISNULL(rg.PostProcess,'N') = 'N' OR (i.UnitCount >= 1 AND i.CompanyCount >= 1) THEN i.TotStarts END)) ds(TotStartsPP)) src
PIVOT (AVG(TotStartsPP) FOR ProjectID IN ([COMBINSP],[HGPINSP],[OVHL])) pvt
