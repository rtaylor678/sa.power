﻿
CREATE VIEW [Report].[PlantActionExp]  AS 
SELECT Refnum, DataType, 
	SiteWagesBen = TotCompensation,
	CentralContract = (CentralOCCWagesBen + CentralMPSWagesBen + ContMaintLabor +
			   ContMaintMatl + ContMaintLumpSum + OthContSvc),
	MaintMatl,
	OverhaulAdj,
	LTSAAdj,
	OthVar = (ISNULL(OthVar,0)+ISNULL(Chemicals,0)+ISNULL(Water,0)),
	Misc = (Envir + OthFixed),
	ActCashLessFuel
FROM Report.Opex

