﻿CREATE VIEW Report.EquipMaintCost
AS
SELECT Refnum, DataType, [CPHM], Boilers = [BOIL], STG_TURB = [TURB], STG_GEN = [GEN], STG_VC = [VC], STG_CA = [CA], 
[BAG],[PREC],[WGS],[DGS],[SCR],
CTG_TURB = [CTTURB], CTG_GEN = [CTGEN], CTG_TRANS = [CTTRANS], CTG_HRSG = [CTHRSG], CTG_OTH = [CTOTH], CTG_COMB = [CTCOMB], CTG_COMP = [CTCOMP],
[FUELDEL],[CWF],[ASH],[WSS],[TRANS],[WASTEH2],[OTHER]
FROM (SELECT e.Refnum, rd.DataType, e.EquipGroup, rd.AnnMaintCost
	FROM Report.EquipMaintCostRaw e LEFT JOIN Report.ReportGroups rg ON rg.Refnum = e.Refnum
	CROSS APPLY (VALUES (CASE WHEN ISNULL(rg.PostProcess,'N') = 'N' OR (e.UnitCount >= 1 AND e.CompanyCount >= 1) THEN 1.0 END)) ds(ReportData)
	CROSS APPLY (VALUES ('$/MWH', e.AnnMaintCostMWH*ds.ReportData),('K$/MW', e.AnnMaintCostMW*ds.ReportData),('$/MBTU', e.AnnMaintCostMBTU*ds.ReportData)
		) rd(DataType, AnnMaintCost)
	) src
PIVOT (AVG(AnnMaintCost) FOR EquipGroup IN ([CPHM],[BOIL],[TURB],[GEN],[VC],[CA],[BAG],[PREC],[WGS],[DGS],[SCR],
[CTTURB],[CTGEN],[CTTRANS],[CTHRSG],[CTOTH],[CTCOMB],[CTCOMP],
[FUELDEL],[CWF], [ASH],[WSS],[TRANS],[WASTEH2],[OTHER])) pvt









