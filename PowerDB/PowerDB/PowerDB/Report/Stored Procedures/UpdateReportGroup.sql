﻿CREATE PROC [Report].[UpdateReportGroup](@ReportRefnum dbo.ReportRefnum)
AS

SET NOCOUNT ON 

DECLARE @GroupNotDefined bit = 0

IF NOT EXISTS (SELECT 1 FROM Report.ReportGroups WHERE Refnum = @ReportRefnum AND DefinedInReportGroupRefs = 1)
BEGIN
	IF EXISTS (SELECT 1 FROM dbo.TSort WHERE Refnum = @ReportRefnum)
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM Report.ReportGroupRefs WHERE ReportRefnum = @ReportRefnum)
			INSERT Report.ReportGroupRefs(ReportRefnum, Refnum, Weighting)
			VALUES (@ReportRefnum, @ReportRefnum, 1)
	END
	ELSE BEGIN
		DELETE FROM Report.ReportGroupRefs WHERE ReportRefnum = @ReportRefnum 
		IF NOT EXISTS (SELECT 1  FROM Report.ReportGroups WHERE Refnum = @ReportRefnum)
			SET @GroupNotDefined = 1
		ELSE BEGIN
			INSERT Report.ReportGroupRefs (ReportRefnum, Refnum, Weighting)
			SELECT @ReportRefnum, Refnum, 1
			FROM Report.GetReportGroupRefnums(@ReportRefnum)

			UPDATE rgr
			SET Weighting = w.Weighting
			FROM Report.ReportGroupRefs rgr INNER JOIN Report.ReportGroupRefWeighting w ON w.ReportRefnum = rgr.ReportRefnum AND w.Refnum = rgr.Refnum
			WHERE rgr.ReportRefnum = @ReportRefnum
		END
	END
END

DECLARE @Refnums Report.GroupedRefnumList
INSERT @Refnums SELECT 'ReportGroup', @ReportRefnum, Refnum, Weighting FROM Report.ReportGroupRefs WHERE ReportRefnum = @ReportRefnum

PRINT 'Deleting tables for ' + @ReportRefnum
DELETE FROM Report.AbsencePcnt WHERE Refnum = @ReportRefnum
DELETE FROM Report.AshHandlingCosts WHERE Refnum = @ReportRefnum
DELETE FROM Report.Breaks WHERE Refnum = @ReportRefnum
DELETE FROM Report.Coal WHERE Refnum = @ReportRefnum
DELETE FROM Report.Cogen WHERE Refnum = @ReportRefnum
DELETE FROM Report.CommUnavail WHERE Refnum = @ReportRefnum
DELETE FROM Report.Counts WHERE Refnum = @ReportRefnum
DELETE FROM Report.CostPerRunHr WHERE Refnum = @ReportRefnum
DELETE FROM Report.CptlMaintExp WHERE Refnum = @ReportRefnum
DELETE FROM Report.CTGData WHERE Refnum = @ReportRefnum
DELETE FROM Report.CTGHeatRate WHERE Refnum = @ReportRefnum
DELETE FROM Report.CTGMaintCostPerStart WHERE Refnum = @ReportRefnum
DELETE FROM Report.CTGStartsAnalysis WHERE Refnum = @ReportRefnum
DELETE FROM Report.CTGRunHrsRaw WHERE Refnum = @ReportRefnum
DELETE FROM Report.CTGTotStartsRaw WHERE Refnum = @ReportRefnum
DELETE FROM Report.EffPersMW WHERE Refnum = @ReportRefnum
DELETE FROM Report.EffPersPerUnit WHERE Refnum = @ReportRefnum
DELETE FROM Report.EGC WHERE Refnum = @ReportRefnum
DELETE FROM Report.EmpCompensation WHERE Refnum = @ReportRefnum
DELETE FROM Report.Emissions WHERE Refnum = @ReportRefnum
DELETE FROM Report.EquipMaintCostRaw WHERE Refnum = @ReportRefnum
DELETE FROM Report.EquipNonOHCostRaw WHERE Refnum = @ReportRefnum
DELETE FROM Report.EquipOHCostRaw WHERE Refnum = @ReportRefnum
DELETE FROM Report.EquipOHIntervalRaw WHERE Refnum = @ReportRefnum
DELETE FROM Report.EquipOutageFactors WHERE Refnum = @ReportRefnum
DELETE FROM Report.FuelCost WHERE Refnum = @ReportRefnum
DELETE FROM Report.FuelSlate WHERE Refnum = @ReportRefnum
DELETE FROM Report.Generation WHERE Refnum = @ReportRefnum
DELETE FROM Report.LTSA WHERE Refnum = @ReportRefnum
DELETE FROM Report.MaintTotCalc WHERE Refnum = @ReportRefnum
DELETE FROM Report.MiscRevenue WHERE Refnum = @ReportRefnum
DELETE FROM Report.NERCFactors WHERE Refnum = @ReportRefnum
DELETE FROM Report.Opex WHERE Refnum = @ReportRefnum
DELETE FROM Report.OpexRanges WHERE Refnum = @ReportRefnum
DELETE FROM Report.OSHARate WHERE Refnum = @ReportRefnum
DELETE FROM Report.PersOvertimePcnt WHERE Refnum = @ReportRefnum
DELETE FROM Report.ScrubberNonMaintCosts WHERE Refnum = @ReportRefnum
DELETE FROM Report.ScrubbingIndex WHERE Refnum = @ReportRefnum
DELETE FROM Report.SiteUnits WHERE Refnum = @ReportRefnum
DELETE FROM Report.StartsAnalysis WHERE Refnum = @ReportRefnum
DELETE FROM Report.TotEffPers WHERE Refnum = @ReportRefnum

IF @GroupNotDefined = 1
	RETURN 1

PRINT 'Inserting Report.Counts for ' + @ReportRefnum
INSERT INTO Report.Counts(Refnum, UnitCount, CTGCount, SiteCount, CompanyCount)
SELECT SubGroupID, UnitCount, CTGCount, SiteCount, CompanyCount
FROM Report.AggCounts(@Refnums)

PRINT 'Inserting Report.EGC for ' + @ReportRefnum
INSERT INTO Report.EGC (Refnum, kEGC)
SELECT SubGroupID, kEGC
FROM Report.AggEGC(@Refnums)

PRINT 'Inserting Report.AbsencePcnt for ' + @ReportRefnum
INSERT INTO Report.AbsencePcnt (Refnum, EmpType, TotAbsPcnt, HOL, VAC, HOLVAC, ONJOBC, ONJOB, SICK, LEGAL, OthExc, Unexc, Other) 
SELECT SubGroupID, 'OCC', TotOCCAbsPcnt, OCCAbsPcnt_HOL, OCCAbsPcnt_VAC, OCCAbsPcnt_HOLVAC, OCCAbsPcnt_ONJOBC, OCCAbsPcnt_ONJOB, OCCAbsPcnt_SICK, OCCAbsPcnt_LEGAL, OCCAbsPcnt_OthExc, OCCAbsPcnt_Unexc, OCCAbsPcnt_Other 
FROM Report.AggAbsenceOCC(@Refnums)

INSERT INTO Report.AbsencePcnt (Refnum, EmpType, TotAbsPcnt, HOL, VAC, HOLVAC, ONJOBC, ONJOB, SICK, LEGAL, OthExc, Unexc, Other) 
SELECT SubGroupID, 'MPS', TotMPSAbsPcnt, MPSAbsPcnt_HOL, MPSAbsPcnt_VAC, MPSAbsPcnt_HOLVAC, MPSAbsPcnt_ONJOBC, MPSAbsPcnt_ONJOB, MPSAbsPcnt_SICK, MPSAbsPcnt_LEGAL, MPSAbsPcnt_OthExc, MPSAbsPcnt_Unexc, MPSAbsPcnt_Other 
FROM Report.AggAbsenceMPS(@Refnums)

PRINT 'Inserting Report.AshHandlingCosts for ' + @ReportRefnum
INSERT INTO Report.AshHandlingCosts (Refnum, DataType, AshCostOp, AshCostDisp, TotAshHandling) 
SELECT SubGroupID, DataType, AshCostOp, AshCostDisp, TotAshHandling 
FROM Report.AggAshHandlingCosts(@Refnums)

PRINT 'Inserting Report.Breaks for ' + @ReportRefnum
INSERT INTO Report.Breaks (Refnum, StudyYears, Regions, Continents, Regulated, FuelTypes, FuelGroups, LoadTypes, LoadTypes2
	, CoalNMCGroups, CoalNMCGroups2, CoalNMCOverlap, CoalNMCOverlap2, CoalHVGroups, CoalSulfurGroups, CoalAshGroups
	, Scrubber, ScrubberSize, BaseCoal, GasOilNMCGroups, CombinedCycle, CCNMCGroups, SteamGasOil, CRVGroups, CRVSizeGroups, CRVSizeGroups2, LTSA, CogenElec) 
SELECT SubGroupID, StudyYears, Regions, Continents, Regulated, FuelTypes, FuelGroups, LoadTypes, LoadTypes2
	, CoalNMCGroups, CoalNMCGroups2, CoalNMCOverlap, CoalNMCOverlap2, CoalHVGroups, CoalSulfurGroups, CoalAshGroups
	, Scrubber, ScrubberSize, BaseCoal, GasOilNMCGroups, CombinedCycle, CCNMCGroups, SteamGasOil, CRVGroups, CRVSizeGroups, CRVSizeGroups2, LTSA, CogenElec
FROM Report.AggBreaks(@Refnums)

PRINT 'Inserting Report.Coal for ' + @ReportRefnum
INSERT INTO Report.Coal (Refnum, HeatValue, LHV, AshPcnt, SulfurPcnt, MoistPcnt, DeliveredCostTon, OnsitePrepCostTon, TotCostTon) 
SELECT SubGroupID, HeatValue, LHV, AshPcnt, SulfurPcnt, MoistPcnt, DeliveredCostTon, OnsitePrepCostTon, TotCostTon
FROM Report.AggCoal(@Refnums)

PRINT 'Inserting Report.Cogen for ' + @ReportRefnum
INSERT INTO Report.Cogen (Refnum, ThermPcnt, StmReturnPcnt, TotExportMBTU, ThermEff, CTGThermEff, ElecHeatRate, ThermNCF, ThermEUF, ElecNCF, ElecNOF, ElecEFOR, TotCashOpexMBTU, TotCashLessFuelEmmMBTU, PlantMngMBTU, AnnMaintCostMBTU) 
SELECT SubGroupID, ThermPcnt, StmReturnPcnt, TotExportMBTU, ThermEff, CTGThermEff, ElecHeatRate, ThermNCF, ThermEUF, ElecNCF, ElecNOF, ElecEFOR, TotCashOpexMBTU, TotCashLessFuelEmmMBTU, PlantMngMBTU, AnnMaintCostMBTU
FROM Report.AggCogen(@Refnums)

PRINT 'Inserting Report.CommUnavail for ' + @ReportRefnum
INSERT INTO Report.CommUnavail (Refnum, CommUnavail, CUTI_Tot, CommUnavail_Unp, CUTI_Unp, CommUnavail_F, CommUnavail_M, CommUnavail_P, CUTI_F, CUTI_M, CUTI_P
	, CommUnavail_Tot_Win, CUTI_Tot_Win, CommUnavail_F_Win, CUTI_F_Win, CommUnavail_Tot_Sum, CUTI_Tot_Sum, CommUnavail_F_Sum, CUTI_F_Sum) 
SELECT SubGroupID, CommUnavail, CUTI_Tot, CommUnavail_Unp, CUTI_Unp, CommUnavail_F, CommUnavail_M, CommUnavail_P, CUTI_F, CUTI_M, CUTI_P
	, CommUnavail_Tot_Win, CUTI_Tot_Win, CommUnavail_F_Win, CUTI_F_Win, CommUnavail_Tot_Sum, CUTI_Tot_Sum, CommUnavail_F_Sum, CUTI_F_Sum
FROM Report.AggCommUnavail(@Refnums)

PRINT 'Inserting Report.CostPerRunHr for ' + @ReportRefnum
INSERT INTO Report.CostPerRunHr (Refnum, TotalCostPerRunHr, FixedCostPerRunHr, VarLessFuelPerRunHr, TotMaintPerRunHr, NonOHMaintPerRunHr, OHMaintPerRunHr, LTSAPerRunHr) 
SELECT SubGroupID, TotalCostPerRunHr, FixedCostPerRunHr, VarLessFuelPerRunHr, TotMaintPerRunHr, NonOHMaintPerRunHr, OHMaintPerRunHr, LTSAPerRunHr
FROM Report.AggCostPerRunHr(@Refnums)

PRINT 'Inserting Report.CptlMaintExp for ' + @ReportRefnum
INSERT INTO Report.CptlMaintExp (Refnum, DataType, STMaintCptl, STMaintExp, STMMO, Energy, RegEnv, Admin, ConstrRmvl, InvestCptl) 
SELECT SubGroupID, DataType, STMaintCptl, STMaintExp, STMMO, Energy, RegEnv, Admin, ConstrRmvl, InvestCptl
FROM Report.AggCptlMaintExp(@Refnums)

PRINT 'Inserting Report.CTGData for ' + @ReportRefnum
INSERT INTO Report.CTGData (Refnum, NMC, PriorNetGWH, NetGWH, ServiceHrs, NCF2Yr, NOF2Yr, SuccessPcnt, ForcedPcnt, MaintPcnt, StartupPcnt, DeratePcnt, HeatRate, HeatRateLHV, FiringTemp) 
SELECT SubGroupID, NMC, PriorNetGWH, NetGWH, ServiceHrs, NCF2Yr, NOF2Yr, SuccessPcnt, ForcedPcnt, MaintPcnt, StartupPcnt, DeratePcnt, HeatRate, HeatRateLHV, FiringTemp
FROM Report.AggCTGData(@Refnums)

PRINT 'Inserting Report.CTGHeatRate for ' + @ReportRefnum
INSERT INTO Report.CTGHeatRate (Refnum, HeatRate, HeatRateLHV) 
SELECT SubGroupID, HeatRate, HeatRateLHV
FROM Report.AggCTGHeatRate(@Refnums)

PRINT 'Inserting Report.CTGMaintCostPerStart for ' + @ReportRefnum
INSERT INTO Report.CTGMaintCostPerStart(Refnum, AvgTotStarts, LTSAPerStart, AnnOHPerStart, NonOHPerStart, MaintCostPerStart)
SELECT SubGroupID, AvgTotStarts, LTSAPerStart, AnnOHPerStart, NonOHPerStart, MaintCostPerStart
FROM Report.AggCTGMaintCostPerStart(@Refnums)

PRINT 'Inserting Report.CTGStartsAnalysis for ' + @ReportRefnum
INSERT INTO Report.CTGStartsAnalysis (Refnum, SuccessPcnt, ForcedPcnt, MaintPcnt, StartupPcnt, DeratePcnt) 
SELECT SubGroupID, SuccessPcnt, ForcedPcnt, MaintPcnt, StartupPcnt, DeratePcnt
FROM Report.AggCTGStartsAnalysis(@Refnums)

PRINT 'Inserting Report.CTGRunHrsRaw for ' + @ReportRefnum
INSERT INTO Report.CTGRunHrsRaw (Refnum, ProjectID, RunHrs, UnitCount, SiteCount, CompanyCount)
SELECT SubGroupID, ProjectID, RunHrs, UnitCount, SiteCount, CompanyCount
FROM Report.AggCTGRunHrs(@Refnums)

PRINT 'Inserting Report.CTGTotStartsRaw for ' + @ReportRefnum
INSERT INTO Report.CTGTotStartsRaw (Refnum, ProjectID, TotStarts, UnitCount, SiteCount, CompanyCount)
SELECT SubGroupID, ProjectID, TotStarts, UnitCount, SiteCount, CompanyCount
FROM Report.AggCTGTotStarts(@Refnums)

PRINT 'Inserting Report.TotEffPers for ' + @ReportRefnum
INSERT INTO Report.TotEffPers (Refnum, DataType, TotEffPers, TotEffPers_Min2, TotEffPers_Max2)
SELECT SubGroupID, DataType, TotEffPers, TotEffPers_Min2, TotEffPers_Max2
FROM Report.AggTotEffPers(@Refnums)

PRINT 'Inserting Report.EffPersMW for ' + @ReportRefnum
INSERT INTO Report.EffPersMW (Refnum, PersType, OCCOps, OCCSTNOHMaint, OCCNOHMaint, OCCInsp, OCCLumpSum, OCCLTSA, OCCOverhaul, OCCSTMaint, OCCTech, OCCAdmin, OCCNonMaint, OCCTotal
	, MPSOps, MPSSTNOHMaint, MPSNOHMaint, MPSInsp, MPSLumpSum, MPSLTSA, MPSOverhaul, MPSSTMaint, MPSTech, MPSAdmin, MPSNonMaint, MPSTotal, TotalPers) 
SELECT SubGroupID, PersType, OCCOps, OCCSTNOHMaint, OCCNOHMaint, OCCInsp, OCCLumpSum, OCCLTSA, OCCOverhaul, OCCSTMaint, OCCTech, OCCAdmin, OCCNonMaint, OCCTotal
	, MPSOps, MPSSTNOHMaint, MPSNOHMaint, MPSInsp, MPSLumpSum, MPSLTSA, MPSOverhaul, MPSSTMaint, MPSTech, MPSAdmin, MPSNonMaint, MPSTotal, TotalPers
FROM Report.AggEffPersMW(@Refnums)

PRINT 'Inserting Report.EffPersPerUnit for ' + @ReportRefnum
INSERT INTO Report.EffPersPerUnit (Refnum, SiteEffPers, CentralEffPers, AGEffPers, ContractEffPers, TotEffPers) 
SELECT SubGroupID, SiteEffPers, CentralEffPers, AGEffPers, ContractEffPers, TotEffPers
FROM Report.AggEffPersPerUnit(@Refnums)

PRINT 'Inserting Report.EmpCompensation for ' + @ReportRefnum
INSERT INTO Report.EmpCompensation (Refnum, OCCAnnComp, OCCBenPcnt, MPSAnnComp, MPSBenPcnt)
SELECT SubGroupID, OCCAnnComp, OCCBenPcnt, MPSAnnComp, MPSBenPcnt
FROM Report.AggEmpCompensation(@Refnums)

PRINT 'Inserting Report.Emissions for ' + @ReportRefnum
INSERT INTO Report.Emissions (Refnum, EmissionType, LbsPerMWH, TonsPerMWH, TonsEmitted, TonsAllow) 
SELECT SubGroupID, EmissionType, LbsPerMWH, TonsPerMWH, TonsEmitted, TonsAllow
FROM Report.AggEmissions(@Refnums)

PRINT 'Inserting Report.EquipMaintCostRaw for ' + @ReportRefnum
INSERT INTO Report.EquipMaintCostRaw (Refnum, EquipGroup, AnnMaintCostMWH, AnnMaintCostMW, AnnMaintCostMBTU, UnitCount, SiteCount, CompanyCount) 
SELECT SubGroupID, EquipGroup, AnnMaintCostMWH, AnnMaintCostMW, AnnMaintCostMBTU, UnitCount, SiteCount, CompanyCount
FROM Report.AggEquipMaintCost(@Refnums)

PRINT 'Inserting Report.EquipNonOHCostRaw for ' + @ReportRefnum
INSERT INTO Report.EquipNonOHCostRaw (Refnum, Component, AnnNonOHCostMWH, AnnNonOHCostMW, AnnNonOHCostMBTU, UnitCount, SiteCount, CompanyCount) 
SELECT SubGroupID, Component, AnnNonOHCostMWH, AnnNonOHCostMW, AnnNonOHCostMBTU, UnitCount, SiteCount, CompanyCount
FROM Report.AggEquipNonOHCost(@Refnums)

PRINT 'Inserting Report.EquipOHCostRaw for ' + @ReportRefnum
INSERT INTO Report.EquipOHCostRaw (Refnum, Component, ProjectType, AnnOHCostMWH, AnnOHCostMW, AnnOHCostMBTU, UnitCount, SiteCount, CompanyCount) 
SELECT SubGroupID, Component, ProjectType, AnnOHCostMWH, AnnOHCostMW, AnnOHCostMBTU, UnitCount, SiteCount, CompanyCount
FROM Report.AggEquipOHCost(@Refnums)

PRINT 'Inserting Report.EquipOHIntervalRaw for ' + @ReportRefnum
INSERT INTO Report.EquipOHIntervalRaw (Refnum, Component, IntervalMo, UnitCount, SiteCount, CompanyCount) 
SELECT SubGroupID, Component, IntervalMo, UnitCount, SiteCount, CompanyCount
FROM Report.AggOHInterval(@Refnums)


PRINT 'Inserting Report.EquipOutageFactors for ' + @ReportRefnum
INSERT INTO Report.EquipOutageFactors (Refnum, BoilerEPOF, STGEPOF, PollutionEPOF, CTGEPOF, OtherEPOF, CPHM_EUOF, BOIL_EUOF, TURB_EUOF, GEN_EUOF, VC_EUOF, CA_EUOF, BAG_EUOF, PREC_EUOF, WGS_EUOF, DGS_EUOF, SCR_EUOF
	, CTTURB_EUOF, CTGEN_EUOF, CTTRANS_EUOF, CTHRSG_EUOF, CTOTH_EUOF, FHF_EUOF, CWF_EUOF, ASH_EUOF, DQWS_EUOF, TRANS_EUOF, WASTEH2O_EUOF, OTHER_EUOF) 
SELECT SubGroupID, BoilerEPOF, STGEPOF, PollutionEPOF, CTGEPOF, OtherEPOF, CPHM_EUOF, BOIL_EUOF, TURB_EUOF, GEN_EUOF, VC_EUOF, CA_EUOF, BAG_EUOF, PREC_EUOF, WGS_EUOF, DGS_EUOF, SCR_EUOF
	, CTTURB_EUOF, CTGEN_EUOF, CTTRANS_EUOF, CTHRSG_EUOF, CTOTH_EUOF, FHF_EUOF, CWF_EUOF, ASH_EUOF, DQWS_EUOF, TRANS_EUOF, WASTEH2O_EUOF, OTHER_EUOF
FROM Report.AggEquipOutageFactors(@Refnums)

PRINT 'Inserting Report.FuelCost for ' + @ReportRefnum
INSERT INTO Report.FuelCost (Refnum, HVBasis, Gas, Liquid, Coal, Total)
SELECT SubGroupID, HVBasis, Gas, Liquid, Coal, Total
FROM Report.AggFuelCost(@Refnums)

PRINT 'Inserting Report.FuelSlate for ' + @ReportRefnum
INSERT INTO Report.FuelSlate (Refnum, TotGasPcntHHV, NatGasPcntHHV, OffGasPcntHHV, H2GasPcntHHV, JetTurbPcntHHV, DieselPcntHHV, FuelOilPcntHHV, CoalPcntHHV
	, TotGasPcntLHV, NatGasPcntLHV, OffGasPcntLHV, H2GasPcntLHV, JetTurbPcntLHV, DieselPcntLHV, FuelOilPcntLHV, CoalPcntLHV, FuelOilHeatValue, FuelOilLHV) 
SELECT SubGroupID, TotGasPcntHHV, NatGasPcntHHV, OffGasPcntHHV, H2GasPcntHHV, JetTurbPcntHHV, DieselPcntHHV, FuelOilPcntHHV, CoalPcntHHV
	, TotGasPcntLHV, NatGasPcntLHV, OffGasPcntLHV, H2GasPcntLHV, JetTurbPcntLHV, DieselPcntLHV, FuelOilPcntLHV, CoalPcntLHV, FuelOilHeatValue, FuelOilLHV
FROM Report.AggFuelSlate(@Refnums)

PRINT 'Inserting Report.Generation for ' + @ReportRefnum
INSERT INTO Report.Generation (Refnum, SumPriorAdjNetGWH, SumAdjNetGWH, AvgPriorAdjNetGWH, AvgAdjNetGWH, TotStarts, MeanRunTime, AuxiliaryPcnt, ScrubberPcnt, StationSvcPcnt, InternalUsagePcnt
	, HeatRate, HeatRate_Min2, HeatRate_Max2, HeatRateLHV, HeatRateLHV_Min2, HeatRateLHV_Max2) 
SELECT SubGroupID, SumPriorAdjNetGWH, SumAdjNetGWH, AvgPriorAdjNetGWH, AvgAdjNetGWH, TotStarts, MeanRunTime, AuxiliaryPcnt, ScrubberPcnt, StationSvcPcnt, InternalUsagePcnt
	, HeatRate, HeatRate_Min2, HeatRate_Max2, HeatRateLHV, HeatRateLHV_Min2, HeatRateLHV_Max2
FROM Report.AggGeneration(@Refnums)

PRINT 'Inserting Report.LTSA for ' + @ReportRefnum
INSERT INTO Report.LTSA (Refnum, EquivStartsFactor, CurrEquivRunHrsFactor) 
SELECT SubGroupID , EquivStartsFactor, CurrEquivRunHrsFactor
FROM Report.AggLTSA(@Refnums)

PRINT 'Inserting Report.MaintTotCalc for ' + @ReportRefnum
INSERT INTO Report.MaintTotCalc (Refnum, AnnMaintCostMWH, AnnMaintCostMWH_Min2, AnnMaintCostMWH_Max2, AnnNonOHCostMWH, AnnOHCostMWH, AnnOHProjCostMWH, AnnLTSACostMWH, AnnLTSACostMWH_NZ
	, AnnMaintCostMW, AnnMaintCostMW_Min2, AnnMaintCostMW_Max2, AnnNonOHCostMW, AnnOHCostMW, AnnOHProjCostMW, AnnLTSACostMW, AnnLTSACostMW_NZ
	, AnnMaintCostMBTU, AnnMaintCostMBTU_Min2, AnnMaintCostMBTU_Max2, AnnNonOHCostMBTU, AnnOHCostMBTU, AnnOHProjCostMBTU, AnnLTSACostMBTU, AnnLTSACostMBTU_NZ
	, AnnMaintCostEGC, AnnMaintCostEGC_Min2, AnnMaintCostEGC_Max2, AnnNonOHCostEGC, AnnOHCostEGC, AnnOHProjCostEGC, AnnLTSACostEGC) 
SELECT SubGroupID, AnnMaintCostMWH, AnnMaintCostMWH_Min2, AnnMaintCostMWH_Max2, AnnNonOHCostMWH, AnnOHCostMWH, AnnOHProjCostMWH, AnnLTSACostMWH, AnnLTSACostMWH_NZ
	, AnnMaintCostMW, AnnMaintCostMW_Min2, AnnMaintCostMW_Max2, AnnNonOHCostMW, AnnOHCostMW, AnnOHProjCostMW, AnnLTSACostMW, AnnLTSACostMW_NZ
	, AnnMaintCostMBTU, AnnMaintCostMBTU_Min2, AnnMaintCostMBTU_Max2, AnnNonOHCostMBTU, AnnOHCostMBTU, AnnOHProjCostMBTU, AnnLTSACostMBTU, AnnLTSACostMBTU_NZ
	, AnnMaintCostEGC, AnnMaintCostEGC_Min2, AnnMaintCostEGC_Max2, AnnNonOHCostEGC, AnnOHCostEGC, AnnOHProjCostEGC, AnnLTSACostEGC
FROM Report.AggMaintTotCalc(@Refnums)

PRINT 'Inserting Report.MiscRevenue for ' + @ReportRefnum
INSERT INTO Report.MiscRevenue (Refnum, DataType, RevAsh, RevOth, OthNonT7Revenue, OthRevenue) 
SELECT SubGroupID, DataType, RevAsh, RevOth, OthNonT7Revenue, OthRevenue
FROM Report.AggMiscRevenue(@Refnums)

PRINT 'Inserting Report.NERCFactors for ' + @ReportRefnum
INSERT INTO Report.NERCFactors (Refnum, SumNMC, AvgNMC, ServiceHrs, NCF, NOF, EOR, EUOR, EFOR, EPOR, EUF, EUOF, EPOF, MSTUO, MSTFO, MSTMO
	, ServiceHrs2YrAvg, NCF2Yr, NOF2Yr, EOR2Yr, EUOR2Yr, EFOR2Yr, EPOR2Yr, EUF2Yr, EUOF2Yr, EPOF2Yr, Age) 
SELECT SubGroupID, SumNMC, AvgNMC, ServiceHrs, NCF, NOF, EOR, EUOR, EFOR, EPOR, EUF, EUOF, EPOF, MSTUO, MSTFO, MSTMO
	, ServiceHrs2YrAvg, NCF2Yr, NOF2Yr, EOR2Yr, EUOR2Yr, EFOR2Yr, EPOR2Yr, EUF2Yr, EUOF2Yr, EPOF2Yr, Age
FROM Report.AggNERCFactors(@Refnums)

PRINT 'Inserting Report.Opex for ' + @ReportRefnum
INSERT INTO Report.Opex (Refnum, DataType, OCCWages, MPSWages, STWages, OCCBenefits, MPSBenefits, STBenefits, CentralOCCWagesBen, CentralMPSWagesBen, MaintMatl, ContMaintLabor, ContMaintMatl, ContMaintLumpSum, OthContSvc
	, OverhaulAdj, LTSAAdj, Envir, PropTax, OthTax, Insurance, AGPers, AGNonPers, OthFixed, STFixed, Chemicals, Water, PurGas, PurLiquid, PurSolid, STFuelCost
	, SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj, OthVar, STVar, TotCash, Supply, FuelInven, STNonCash, GrandTot
	, ActCashLessFuel, TotCashLessFuel, TotCashLessFuelEm, STFuelCostPlusSO2, AllocSparesInven, OCCCompensation, MPSCompensation, TotCompensation) 
SELECT SubGroupID, DataType, OCCWages, MPSWages, STWages, OCCBenefits, MPSBenefits, STBenefits, CentralOCCWagesBen, CentralMPSWagesBen, MaintMatl, ContMaintLabor, ContMaintMatl, ContMaintLumpSum, OthContSvc
	, OverhaulAdj, LTSAAdj, Envir, PropTax, OthTax, Insurance, AGPers, AGNonPers, OthFixed, STFixed, Chemicals, Water, PurGas, PurLiquid, PurSolid, STFuelCost
	, SO2EmissionAdj, NOxEmissionAdj, CO2EmissionAdj, OthVar, STVar, TotCash, Supply, FuelInven, STNonCash, GrandTot
	, ActCashLessFuel, TotCashLessFuel, TotCashLessFuelEm, STFuelCostPlusSO2, AllocSparesInven, OCCCompensation, MPSCompensation, TotCompensation
FROM Report.AggOpex(@Refnums)

PRINT 'Inserting Report.OpexRanges for ' + @ReportRefnum
INSERT INTO Report.OpexRanges (Refnum, DataType, TotCash_Min2, TotCash_Max2, TotCashLessFuel_Min2, TotCashLessFuel_Max2, TotCashLessFuelEm_Min2, TotCashLessFuelEm_Max2, ActCashLessFuel_Min2, ActCashLessFuel_Max2) 
SELECT SubGroupID, DataType, TotCash_Min2, TotCash_Max2, TotCashLessFuel_Min2, TotCashLessFuel_Max2, TotCashLessFuelEm_Min2, TotCashLessFuelEm_Max2, ActCashLessFuel_Min2, ActCashLessFuel_Max2
FROM Report.AggOpexRanges(@Refnums)

PRINT 'Inserting Report.OSHARate for ' + @ReportRefnum
INSERT INTO Report.OSHARate (Refnum, OSHARate)
SELECT SubGroupID, OSHARate
FROM Report.AggOSHARate(@Refnums)

PRINT 'Inserting Report.PersOvertimePcnt for ' + @ReportRefnum
INSERT INTO Report.PersOvertimePcnt (Refnum, OCCOps, OCCSTNOHMaint, OCCNOHMaint, OCCInsp, OCCOverhaul, OCCSTMaint, OCCTech, OCCAdmin, OCCNonMaint, OCCTotal
	, MPSOps, MPSSTNOHMaint, MPSNOHMaint, MPSInsp, MPSOverhaul, MPSSTMaint, MPSTech, MPSAdmin, MPSNonMaint, MPSTotal, TotalPers) 
SELECT SubGroupID, OCCOps, OCCSTNOHMaint, OCCNOHMaint, OCCInsp, OCCOverhaul, OCCSTMaint, OCCTech, OCCAdmin, OCCNonMaint, OCCTotal
	, MPSOps, MPSSTNOHMaint, MPSNOHMaint, MPSInsp, MPSOverhaul, MPSSTMaint, MPSTech, MPSAdmin, MPSNonMaint, MPSTotal, TotalPers
FROM Report.AggPersOvertimePcnt(@Refnums)

PRINT 'Inserting Report.ScrubberNonMaintCosts for ' + @ReportRefnum
INSERT INTO Report.ScrubberNonMaintCosts (Refnum, DataType, ScrCostLime, ScrCostChem, ScrubberPowerCost, ScrCostNMaint, TotScrubberNonMaint) 
SELECT SubGroupID, DataType, ScrCostLime, ScrCostChem, ScrubberPowerCost, ScrCostNMaint, TotScrubberNonMaint
FROM Report.AggScrubberNonMaintCosts(@Refnums)

PRINT 'Inserting Report.ScrubbingIndex for ' + @ReportRefnum
INSERT INTO Report.ScrubbingIndex (Refnum, ScrubbingIndex) 
SELECT SubGroupID, ScrubbingIndex
FROM Report.AggScrubbingIndex(@Refnums)

PRINT 'Inserting Report.SiteUnits for ' + @ReportRefnum
INSERT INTO Report.SiteUnits (Refnum, TotExclSCNum, TotExclSCCap, TotUnitNum, TotCap, TotExclCTNum, TotExclCTCap) 
SELECT SubGroupID, TotExclSCNum, TotExclSCCap, TotUnitNum, TotCap, TotExclCTNum, TotExclCTCap
FROM Report.AggSiteUnits(@Refnums)

PRINT 'Inserting Report.StartsAnalysis for ' + @ReportRefnum
INSERT INTO Report.StartsAnalysis (Refnum, SuccessPcnt, ForcedPcnt, MaintPcnt, StartupPcnt, DeratePcnt) 
SELECT SubGroupID, SuccessPcnt, ForcedPcnt, MaintPcnt, StartupPcnt, DeratePcnt
FROM Report.AggStartsAnalysis(@Refnums)

PRINT 'Updating of report tables complete for ' + @ReportRefnum

SET NOCOUNT OFF
