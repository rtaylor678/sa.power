﻿CREATE TABLE [Report].[EffPersPerUnit] (
    [Refnum]          [dbo].[ReportRefnum] NOT NULL,
    [SiteEffPers]     REAL                 NULL,
    [CentralEffPers]  REAL                 NULL,
    [AGEffPers]       REAL                 NULL,
    [ContractEffPers] REAL                 NULL,
    [TotEffPers]      REAL                 NULL,
    [tsUpdate]        DATETIME             CONSTRAINT [DF__EffPersPe__tsUpd__050FA50E] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EffPersPerUnit] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

