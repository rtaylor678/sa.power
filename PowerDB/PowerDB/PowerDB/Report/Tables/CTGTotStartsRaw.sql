﻿CREATE TABLE [Report].[CTGTotStartsRaw] (
    [Refnum]       [dbo].[ReportRefnum] NOT NULL,
    [ProjectID]    VARCHAR (50)         NOT NULL,
    [TotStarts]    REAL                 NULL,
    [UnitCount]    SMALLINT             NULL,
    [SiteCount]    SMALLINT             NULL,
    [CompanyCount] SMALLINT             NULL,
    [tsUpdated]    DATETIME             CONSTRAINT [DF_CTGTotStartsRaw_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTGTotStartsRaw] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ProjectID] ASC)
);

