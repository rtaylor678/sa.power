﻿CREATE TABLE [Report].[CTGStartsAnalysis] (
    [Refnum]      [dbo].[ReportRefnum] NOT NULL,
    [SuccessPcnt] NUMERIC (5, 2)       NULL,
    [ForcedPcnt]  NUMERIC (5, 2)       NULL,
    [MaintPcnt]   NUMERIC (5, 2)       NULL,
    [StartupPcnt] NUMERIC (5, 2)       NULL,
    [DeratePcnt]  NUMERIC (5, 2)       NULL,
    [tsUpdate]    DATETIME             CONSTRAINT [DF__CTGStarts__tsUpd__7F56CBB8] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTGStartsAnalysis] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

