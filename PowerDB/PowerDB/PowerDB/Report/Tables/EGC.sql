﻿CREATE TABLE [Report].[EGC] (
    [Refnum]    VARCHAR (25)   NOT NULL,
    [kEGC]      NUMERIC (7, 3) NULL,
    [tsUpdated] DATETIME       CONSTRAINT [DF_EGC_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EGC] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

