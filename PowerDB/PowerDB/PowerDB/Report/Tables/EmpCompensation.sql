﻿CREATE TABLE [Report].[EmpCompensation] (
    [Refnum]     [dbo].[ReportRefnum] NOT NULL,
    [OCCAnnComp] REAL                 NULL,
    [OCCBenPcnt] NUMERIC (5, 2)       NULL,
    [MPSAnnComp] REAL                 NULL,
    [MPSBenPcnt] NUMERIC (5, 2)       NULL,
    [tsUpdated]  DATETIME             CONSTRAINT [DF_EmpCompensation_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EmpCompensation] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

