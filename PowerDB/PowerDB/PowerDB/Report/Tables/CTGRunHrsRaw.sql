﻿CREATE TABLE [Report].[CTGRunHrsRaw] (
    [Refnum]       [dbo].[ReportRefnum] NOT NULL,
    [ProjectID]    VARCHAR (15)         NOT NULL,
    [RunHrs]       REAL                 NULL,
    [UnitCount]    SMALLINT             NULL,
    [SiteCount]    SMALLINT             NULL,
    [CompanyCount] SMALLINT             NULL,
    [tsUpdated]    DATETIME             CONSTRAINT [DF_CTGRunHrsRaw_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTGRunHrsRaw] PRIMARY KEY CLUSTERED ([Refnum] ASC, [ProjectID] ASC)
);

