﻿CREATE TABLE [Report].[CTGHeatRate] (
    [Refnum]      [dbo].[ReportRefnum] NOT NULL,
    [HeatRate]    NUMERIC (6, 1)       NULL,
    [HeatRateLHV] NUMERIC (6, 1)       NULL,
    [tsUpdate]    DATETIME             CONSTRAINT [DF__CTGHeatRa__tsUpd__004AEFF1] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CTGHeatRate] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

