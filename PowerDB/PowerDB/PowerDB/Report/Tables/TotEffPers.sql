﻿CREATE TABLE [Report].[TotEffPers] (
    [Refnum]          [dbo].[ReportRefnum] NOT NULL,
    [DataType]        VARCHAR (3)          NOT NULL,
    [TotEffPers]      REAL                 NULL,
    [TotEffPers_Min2] REAL                 NULL,
    [TotEffPers_Max2] REAL                 NULL,
    [tsUpdated]       DATETIME             CONSTRAINT [DF_TotEffPers_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TotEffPers] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC)
);

