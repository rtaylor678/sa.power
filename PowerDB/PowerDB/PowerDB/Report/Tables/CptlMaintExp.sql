﻿CREATE TABLE [Report].[CptlMaintExp] (
    [Refnum]      [dbo].[ReportRefnum] NOT NULL,
    [DataType]    VARCHAR (6)          NOT NULL,
    [STMaintCptl] REAL                 NULL,
    [STMaintExp]  REAL                 NULL,
    [STMMO]       REAL                 NULL,
    [Energy]      REAL                 NULL,
    [RegEnv]      REAL                 NULL,
    [Admin]       REAL                 NULL,
    [ConstrRmvl]  REAL                 NULL,
    [InvestCptl]  REAL                 NULL,
    [tsUpdate]    DATETIME             CONSTRAINT [DF__CptlMaint__tsUpd__77B5A9F0] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CptlMaintExp_1] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC)
);

