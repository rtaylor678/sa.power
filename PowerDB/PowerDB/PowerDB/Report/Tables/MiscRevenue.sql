﻿CREATE TABLE [Report].[MiscRevenue] (
    [Refnum]          [dbo].[ReportRefnum] NOT NULL,
    [DataType]        VARCHAR (6)          NOT NULL,
    [RevAsh]          REAL                 NULL,
    [RevOth]          REAL                 NULL,
    [OthNonT7Revenue] REAL                 NULL,
    [OthRevenue]      REAL                 NULL,
    [tsUpdate]        DATETIME             CONSTRAINT [DF__MiscReven__tsUpd__7C7A5F0D] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MiscRevenue] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC)
);

