﻿CREATE TABLE [Report].[Counts] (
    [Refnum]       [dbo].[ReportRefnum] NOT NULL,
    [UnitCount]    SMALLINT             NULL,
    [CTGCount]     SMALLINT             NULL,
    [SiteCount]    SMALLINT             NULL,
    [CompanyCount] SMALLINT             NULL,
    [tsUpdated]    DATETIME             CONSTRAINT [DF_Counts_tsUpdated] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Counts] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

