﻿CREATE TABLE [Report].[Coal] (
    [Refnum]            [dbo].[ReportRefnum] NOT NULL,
    [HeatValue]         NUMERIC (6, 1)       NULL,
    [LHV]               NUMERIC (6, 1)       NULL,
    [AshPcnt]           NUMERIC (5, 3)       NULL,
    [SulfurPcnt]        NUMERIC (5, 3)       NULL,
    [MoistPcnt]         NUMERIC (5, 3)       NULL,
    [DeliveredCostTon]  NUMERIC (6, 3)       NULL,
    [OnsitePrepCostTon] NUMERIC (6, 3)       NULL,
    [TotCostTon]        NUMERIC (6, 3)       NULL,
    [tsUpdate]          DATETIME             CONSTRAINT [DF__Coal__tsUpdate__74D93D45] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Coal_1] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

