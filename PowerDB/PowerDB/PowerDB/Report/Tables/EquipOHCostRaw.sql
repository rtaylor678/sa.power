﻿CREATE TABLE [Report].[EquipOHCostRaw] (
    [Refnum]        [dbo].[ReportRefnum] NOT NULL,
    [Component]     VARCHAR (8)          NOT NULL,
    [ProjectType]   VARCHAR (15)         NOT NULL,
    [AnnOHCostMWH]  REAL                 NULL,
    [AnnOHCostMW]   REAL                 NULL,
    [AnnOHCostMBTU] REAL                 NULL,
    [UnitCount]     SMALLINT             NOT NULL,
    [SiteCount]     SMALLINT             NOT NULL,
    [CompanyCount]  SMALLINT             NOT NULL,
    [tsUpdate]      DATETIME             CONSTRAINT [DF__EquipOHCo__tsUpd__06F7ED80] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EquipOHCost] PRIMARY KEY CLUSTERED ([Refnum] ASC, [Component] ASC, [ProjectType] ASC)
);

