﻿CREATE TABLE [Report].[SiteUnits] (
    [Refnum]       [dbo].[ReportRefnum] NOT NULL,
    [TotExclSCNum] SMALLINT             NULL,
    [TotExclSCCap] REAL                 NULL,
    [TotUnitNum]   SMALLINT             NULL,
    [TotCap]       REAL                 NULL,
    [TotExclCTNum] SMALLINT             NULL,
    [TotExclCTCap] REAL                 NULL,
    [tsUpdate]     DATETIME             CONSTRAINT [DF__SiteUnits__tsUpd__013F142A] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SiteUnits] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

