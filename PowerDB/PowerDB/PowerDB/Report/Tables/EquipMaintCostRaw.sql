﻿CREATE TABLE [Report].[EquipMaintCostRaw] (
    [Refnum]           [dbo].[ReportRefnum] NOT NULL,
    [EquipGroup]       VARCHAR (7)          NOT NULL,
    [AnnMaintCostMWH]  REAL                 NULL,
    [AnnMaintCostMW]   REAL                 NULL,
    [AnnMaintCostMBTU] REAL                 NULL,
    [UnitCount]        SMALLINT             NOT NULL,
    [SiteCount]        SMALLINT             NOT NULL,
    [CompanyCount]     SMALLINT             NOT NULL,
    [tsUpdate]         DATETIME             CONSTRAINT [DF__EquipMain__tsUpd__08E035F2] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_EquipMaintCost] PRIMARY KEY CLUSTERED ([Refnum] ASC, [EquipGroup] ASC)
);

