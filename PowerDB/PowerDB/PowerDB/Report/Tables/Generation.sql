﻿CREATE TABLE [Report].[Generation] (
    [Refnum]            [dbo].[ReportRefnum] NOT NULL,
    [SumPriorAdjNetGWH] REAL                 NULL,
    [SumAdjNetGWH]      REAL                 NULL,
    [AvgPriorAdjNetGWH] REAL                 NULL,
    [AvgAdjNetGWH]      REAL                 NULL,
    [TotStarts]         REAL                 NULL,
    [MeanRunTime]       REAL                 NULL,
    [AuxiliaryPcnt]     REAL                 NULL,
    [ScrubberPcnt]      REAL                 NULL,
    [StationSvcPcnt]    REAL                 NULL,
    [InternalUsagePcnt] REAL                 NULL,
    [HeatRate]          REAL                 NULL,
    [HeatRate_Min2]     REAL                 NULL,
    [HeatRate_Max2]     REAL                 NULL,
    [HeatRateLHV]       REAL                 NULL,
    [HeatRateLHV_Min2]  REAL                 NULL,
    [HeatRateLHV_Max2]  REAL                 NULL,
    [tsUpdate]          DATETIME             CONSTRAINT [DF__Generatio__tsUpd__6D381B7D] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Generation] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

