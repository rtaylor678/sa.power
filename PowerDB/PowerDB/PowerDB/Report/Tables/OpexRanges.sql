﻿CREATE TABLE [Report].[OpexRanges] (
    [Refnum]                 [dbo].[ReportRefnum] NOT NULL,
    [DataType]               VARCHAR (6)          NOT NULL,
    [TotCash_Min2]           REAL                 NULL,
    [TotCash_Max2]           REAL                 NULL,
    [TotCashLessFuel_Min2]   REAL                 NULL,
    [TotCashLessFuel_Max2]   REAL                 NULL,
    [TotCashLessFuelEm_Min2] REAL                 NULL,
    [TotCashLessFuelEm_Max2] REAL                 NULL,
    [ActCashLessFuel_Min2]   REAL                 NULL,
    [ActCashLessFuel_Max2]   REAL                 NULL,
    [tsUpdate]               DATETIME             CONSTRAINT [DF__OpexRange__tsUpd__72F0F4D3] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OpexRanges] PRIMARY KEY CLUSTERED ([Refnum] ASC, [DataType] ASC)
);

