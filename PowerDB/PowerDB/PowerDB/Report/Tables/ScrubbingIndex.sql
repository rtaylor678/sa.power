﻿CREATE TABLE [Report].[ScrubbingIndex] (
    [Refnum]         [dbo].[ReportRefnum] NOT NULL,
    [ScrubbingIndex] REAL                 NULL,
    [tsUpdate]       DATETIME             CONSTRAINT [DF__Scrubbing__tsUpd__02333863] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ScrubbingIndex] PRIMARY KEY CLUSTERED ([Refnum] ASC)
);

