﻿CREATE TYPE [Report].[GroupedRefnumList] AS TABLE (
    [SubGroupType] VARCHAR (20)   NOT NULL,
    [SubGroupID]   VARCHAR (25)   NOT NULL,
    [Refnum]       [dbo].[Refnum] NOT NULL,
    [Weighting]    NUMERIC (3, 2) NOT NULL,
    PRIMARY KEY CLUSTERED ([SubGroupType] ASC, [SubGroupID] ASC, [Refnum] ASC));

