﻿CREATE FUNCTION [Report].[AggEGC](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, kEGC = GlobalDB.dbo.WtAvgNZ(g.EGC, r.Weighting)/1000
	FROM @refnums r INNER JOIN dbo.Gensum g ON g.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

