﻿CREATE FUNCTION [Report].[AggEffPersPerUnit](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, SiteEffPers = GlobalDB.dbo.WtAvg(p.SiteEffPers, r.Weighting)
		, CentralEffPers = GlobalDB.dbo.WtAvg(p.CentralEffPers, r.Weighting)
		, AGEffPers = GlobalDB.dbo.WtAvg(p.AGEffPers, r.Weighting)
		, ContractEffPers = GlobalDB.dbo.WtAvg(p.ContractEffPers, r.Weighting)
		, TotEffPers = GlobalDB.dbo.WtAvg(p.TotEffPers, r.Weighting)
	FROM @Refnums r INNER JOIN dbo.PersSTCalc p ON p.Refnum = r.Refnum AND p.SectionID = 'TP'
	WHERE p.TotEffPers > 0
	GROUP BY r.SubGroupType, r.SubGroupID
)

