﻿CREATE FUNCTION [Report].[AggCogen](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, ThermPcnt = GlobalDB.dbo.WtAvgNN(cc.ThermPcnt, cc.TotExportMBTU*r.Weighting)
		, StmReturnPcnt = GlobalDB.dbo.WtAvgNZ(cc.StmReturnPcnt, cc.NetStmExportKLb*r.Weighting)
		, TotExportMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotExportMBTU, r.Weighting)/1e6
		, ThermEff = GlobalDB.dbo.WtAvgNZ(cc.ThermEff, cc.TotInputMBTU*r.Weighting)
		, CTGThermEff = GlobalDB.dbo.WtAvgNZ(cc.CTGThermEff, cc.CTGFuelMBTU*r.Weighting)
		, ElecHeatRate = GlobalDB.dbo.WtAvgNZ(cc.ElecHeatRate, gtc.NetMWH*r.Weighting)
		, ThermNCF = GlobalDB.dbo.WtAvgNZ(CASE WHEN cc.ThermNCF<101 THEN cc.ThermNCF END, cc.MaxStmMakeMBTU*r.Weighting)
		, ThermEUF = GlobalDB.dbo.WtAvgNN(cc.ThermEUF, cc.MaxMBTUExport*r.Weighting)
		, ElecNCF = GlobalDB.dbo.WtAvgNN(cc.ElecNCF, cc.MaxElecMWH*r.Weighting)
		, ElecNOF = GlobalDB.dbo.WtAvgNN(cc.ElecNOF, cc.MaxElecMWH*r.Weighting)
		, ElecEFOR = GlobalDB.dbo.WtAvgNN(cc.ElecEFOR, cc.MaxElecMWH*r.Weighting)
		, TotCashOpexMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotCashOpexMBTU, cc.TotExportMBTU*r.Weighting)
		, TotCashLessFuelEmmMBTU = GlobalDB.dbo.WtAvgNZ(cc.TotCashLessFuelEmmMBTU, cc.TotExportMBTU*r.Weighting)
		, PlantMngMBTU = GlobalDB.dbo.WtAvgNZ(cc.PlantMngMBTU, cc.TotExportMBTU*r.Weighting)
		, AnnMaintCostMBTU = GlobalDB.dbo.WtAvgNZ(cc.AnnMaintCostMBTU, cc.TotExportMBTU*r.Weighting)
	FROM @refnums r INNER JOIN dbo.CogenCalc cc ON cc.Refnum = r.Refnum
	INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

