﻿CREATE FUNCTION [Report].[AggMiscRevenue](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT d.SubGroupType, d.SubGroupID, d.DataType, RevAsh = rev.RevAsh/d.DivValue, RevOth = rev.RevOth/d.DivValue, OthNonT7Revenue = rev.OthNonT7Revenue/d.DivValue, OthRevenue = rev.OthRevenue/d.DivValue
	FROM (
		SELECT r.SubGroupType, r.SubGroupID, dx.DataType, DivValue = SUM(dx.DivValue)
		FROM @Refnums r
		INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = r.Refnum
		INNER JOIN dbo.NERCFactors nf ON nf.Refnum = r.Refnum
		CROSS APPLY (VALUES 
			('$/MWH', gtc.AdjNetMWH/1000),
			('K$/MW', nf.NMC),
			('$/MBTU', gtc.TotalOutputMBTU/1000)
			) dx(DataType, DivValue)
		GROUP BY r.SubGroupType, r.SubGroupID, dx.DataType) d
	LEFT JOIN (
		SELECT r.SubGroupType, r.SubGroupID, RevAsh = SUM(mc.RevAsh), RevOth = SUM(mc.RevOth), OthNonT7Revenue = SUM(mc.OthNonT7Revenue), OthRevenue = SUM(mc.OthRevenue), TotAshHandling = SUM(mc.TotAshHandling)
		FROM @refnums r INNER JOIN dbo.MiscCalc mc ON mc.Refnum = r.Refnum
		GROUP BY r.SubGroupType, r.SubGroupID) rev ON rev.SubGroupType = d.SubGroupType AND rev.SubGroupID = d.SubGroupID
	WHERE d.DivValue > 0
)

