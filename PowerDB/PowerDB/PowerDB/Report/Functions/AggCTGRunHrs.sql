﻿CREATE FUNCTION [Report].[AggCTGRunHrs](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID, m.ProjectID
		, RunHrs = GlobalDB.dbo.WtAvgNZ(m.RunHrs, r.Weighting)
		, UnitCount = COUNT(DISTINCT m.Refnum)
		, SiteCount = COUNT(DISTINCT t.SiteID)
		, CompanyCount = COUNT(DISTINCT t.CompanyID)
	FROM @refnums r INNER JOIN dbo.CTGMaint m ON m.Refnum = r.Refnum
	INNER JOIN dbo.Tsort t ON t.Refnum = r.Refnum
	WHERE m.RunHrs > 0
	GROUP BY r.SubGroupType, r.SubGroupID, m.ProjectID
)

