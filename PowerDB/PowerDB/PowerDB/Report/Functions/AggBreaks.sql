﻿CREATE FUNCTION [Report].[AggBreaks](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, StudyYears = GlobalDB.dbo.WhatsThere(t.StudyYear)
		, Regions = GlobalDB.dbo.WhatsThere(t.Region)
		, Continents = GlobalDB.dbo.WhatsThere(t.Continent)
		, Regulated = GlobalDB.dbo.WhatsThere(t.Regulated)
		, FuelTypes = GlobalDB.dbo.WhatsThere(t.FuelType)
		, FuelGroups = GlobalDB.dbo.WhatsThere(b.FuelGroup)
		, LoadTypes = GlobalDB.dbo.WhatsThere(b.LoadType)
		, LoadTypes2 = GlobalDB.dbo.WhatsThere(b.LoadType2)
		, CoalNMCGroups = GlobalDB.dbo.WhatsThere(b.CoalNMCGroup)
		, CoalNMCGroups2 = GlobalDB.dbo.WhatsThere(b.CoalNMCGroup2)
		, CoalNMCOverlap = GlobalDB.dbo.WhatsThere(b.CoalNMCOverlap)
		, CoalNMCOverlap2 = GlobalDB.dbo.WhatsThere(b.CoalNMCOverlap2)
		, CoalHVGroups = GlobalDB.dbo.WhatsThere(b.CoalHVGroup)
		, CoalSulfurGroups = GlobalDB.dbo.WhatsThere(b.CoalSulfurGroup)
		, CoalAshGroups = GlobalDB.dbo.WhatsThere(b.CoalAshGroup)
		, Scrubber = GlobalDB.dbo.WhatsThere(b.Scrubbers)
		, ScrubberSize = GlobalDB.dbo.WhatsThere(b.ScrubberSize)
		, BaseCoal = GlobalDB.dbo.WhatsThere(b.BaseCoal)
		, GasOilNMCGroups = GlobalDB.dbo.WhatsThere(b.GasOilNMCGroup)
		, CombinedCycle = GlobalDB.dbo.WhatsThere(b.CombinedCycle)
		, CCNMCGroups = GlobalDB.dbo.WhatsThere(b.CCNMCGroup)
		, SteamGasOil = GlobalDB.dbo.WhatsThere(b.SteamGasOil)
		, CRVGroups = GlobalDB.dbo.WhatsThere(b.CRVGroup)
		, CRVSizeGroups = GlobalDB.dbo.WhatsThere(b.CRVSizeGroup)
		, CRVSizeGroups2 = GlobalDB.dbo.WhatsThere(b.CRVSizeGroup2)
		, LTSA = GlobalDB.dbo.WhatsThere(b.LTSA)
		, CogenElec = GlobalDB.dbo.WhatsThere(b.CogenElec)
	FROM @Refnums r INNER JOIN dbo.TSort t ON t.Refnum = r.Refnum
	INNER JOIN dbo.Breaks b ON b.Refnum = r.Refnum 
	GROUP BY r.SubGroupType, r.SubGroupID
)

