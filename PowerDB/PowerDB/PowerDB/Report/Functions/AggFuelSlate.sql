﻿CREATE FUNCTION [Report].[AggFuelSlate](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, TotGasPcntHHV = SUM(ftc.TotGasMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, NatGasPcntHHV = SUM(ftc.NatGasMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, OffGasPcntHHV = SUM(ftc.OffGasMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, H2GasPcntHHV = SUM(ftc.H2GasMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, JetTurbPcntHHV = SUM(ftc.JetTurbMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, DieselPcntHHV = SUM(ftc.DieselMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, FuelOilPcntHHV = SUM(ftc.FuelOilMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, CoalPcntHHV = SUM(ftc.CoalMBTU*r.Weighting)/SUM(ftc.TotMBTU*r.Weighting)*100
		, TotGasPcntLHV = SUM(ftc.TotGasMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, NatGasPcntLHV = SUM(ftc.NatGasMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, OffGasPcntLHV = SUM(ftc.OffGasMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, H2GasPcntLHV = SUM(ftc.H2GasMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, JetTurbPcntLHV = SUM(ftc.JetTurbMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, DieselPcntLHV = SUM(ftc.DieselMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, FuelOilPcntLHV = SUM(ftc.FuelOilMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, CoalPcntLHV = SUM(ftc.CoalMBTULHV*r.Weighting)/SUM(ftc.TotMBTULHV*r.Weighting)*100
		, FuelOilHeatValue = GlobalDB.dbo.WtAvgNZ(ftc.FuelOilHeatValue, ftc.FuelOilKGal*r.Weighting)
		, FuelOilLHV = GlobalDB.dbo.WtAvgNZ(ftc.FuelOilLHV, ftc.FuelOilKGal*r.Weighting)
	FROM @refnums r INNER JOIN dbo.FuelTotCalc ftc ON ftc.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

