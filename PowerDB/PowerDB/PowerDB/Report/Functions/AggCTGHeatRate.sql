﻿CREATE FUNCTION [Report].[AggCTGHeatRate](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, HeatRate = GlobalDB.dbo.WtAvgNZ(sa.HeatRate, sa.NetMWH*r.Weighting)
		, HeatRateLHV = GlobalDB.dbo.WtAvgNZ(sa.HeatRateLHV, sa.NetMWH*r.Weighting)
	FROM @Refnums r INNER JOIN dbo.CTGSummary sa ON sa.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

