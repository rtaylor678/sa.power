﻿CREATE FUNCTION [Report].[AggAbsences](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT occ.SubGroupType, occ.SubGroupID, occ.TotOCCAbsPcnt, occ.OCCAbsPcnt_HOL, occ.OCCAbsPcnt_VAC, occ.OCCAbsPcnt_HOLVAC, occ.OCCAbsPcnt_ONJOBC, occ.OCCAbsPcnt_ONJOB, occ.OCCAbsPcnt_SICK, occ.OCCAbsPcnt_LEGAL, occ.OCCAbsPcnt_OthExc, occ.OCCAbsPcnt_Unexc, occ.OCCAbsPcnt_Other
	, mps.TotMPSAbsPcnt, mps.MPSAbsPcnt_HOL, mps.MPSAbsPcnt_VAC, mps.MPSAbsPcnt_HOLVAC, mps.MPSAbsPcnt_ONJOBC, mps.MPSAbsPcnt_ONJOB, mps.MPSAbsPcnt_SICK, mps.MPSAbsPcnt_LEGAL, mps.MPSAbsPcnt_OthExc, mps.MPSAbsPcnt_Unexc, mps.MPSAbsPcnt_Other
	FROM Report.AggAbsenceOCC(@Refnums) occ INNER JOIN Report.AggAbsenceMPS(@Refnums) mps ON mps.SubGroupType = occ.SubGroupType AND mps.SubGroupID = occ.SubGroupID
)

