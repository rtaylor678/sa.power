﻿
CREATE FUNCTION [Report].[AggCTGTotStarts](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID, m.ProjectID
		, TotStarts = GlobalDB.dbo.WtAvgNZ(m.TotStarts, r.Weighting)
		, UnitCount = COUNT(DISTINCT m.Refnum)
		, SiteCount = COUNT(DISTINCT t.SiteID)
		, CompanyCount = COUNT(DISTINCT t.CompanyID)
	FROM @refnums r INNER JOIN dbo.CTGMaint m ON m.Refnum = r.Refnum
	INNER JOIN dbo.Tsort t ON t.Refnum = r.Refnum
	WHERE m.TotStarts > 0
	GROUP BY r.SubGroupType, r.SubGroupID, m.ProjectID
)

