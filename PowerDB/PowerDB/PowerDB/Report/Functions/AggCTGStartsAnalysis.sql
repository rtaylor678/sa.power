﻿CREATE FUNCTION [Report].[AggCTGStartsAnalysis](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, SuccessPcnt = GlobalDB.dbo.WtAvg(sa.SuccessPcnt, sa.TotalOpps*r.Weighting)
		, ForcedPcnt = GlobalDB.dbo.WtAvg(sa.ForcedPcnt, sa.TotalOpps*r.Weighting)
		, MaintPcnt = GlobalDB.dbo.WtAvg(sa.MaintPcnt, sa.TotalOpps*r.Weighting)
		, StartupPcnt = GlobalDB.dbo.WtAvg(sa.StartupPcnt, sa.TotalOpps*r.Weighting)
		, DeratePcnt = GlobalDB.dbo.WtAvg(sa.DeratePcnt, sa.TotalOpps*r.Weighting)
	FROM @Refnums r INNER JOIN dbo.CTGSummary sa ON sa.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

