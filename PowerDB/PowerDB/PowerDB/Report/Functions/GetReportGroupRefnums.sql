﻿CREATE FUNCTION Report.GetReportGroupRefnums(@ReportRefnum dbo.ReportRefnum)
RETURNS TABLE
AS RETURN (
	SELECT r.Refnum 
	FROM Report.ReportGroups rg
	CROSS APPLY Report.GetMatchingRefnums(rg.ListName, rg.UserGroup, rg.StudyYear
	, rg.TileRankVariable, rg.TileRankBreak, rg.TileBreakValue, rg.TileTile
	, rg.CompanyID, rg.SiteID, rg.Region, rg.Continent, rg.Country, rg.State
	, rg.FuelGroup, rg.FuelType, rg.LoadType, rg.LoadType2, rg.CRVGroup, rg.CRVSizeGroup, rg.CRVSizeGroup2
	, rg.CoalNMCGroup, rg.CoalNMCGroup2, rg.CoalNMCOverlap, rg.CoalNMCOverlap2, rg.CoalAshGroup, rg.CoalHVGroup, rg.CoalSulfurGroup
	, rg.Scrubbers, rg.ScrubberSize, rg.BaseCoal, rg.SuperCritical, rg.SubCritical
	, rg.SteamGasOil, rg.CombinedCycle, rg.CogenElec, rg.GasOilNMCGroup, rg.CCNMCGroup
	, rg.NCF2YrGroup, rg.FiringTempGroup, rg.CTGS, rg.LTSA, rg.Regulated, rg.StartsGroup
	, rg.MinNMC, rg.MaxNMC) r
	WHERE rg.Refnum = @ReportRefnum
)
