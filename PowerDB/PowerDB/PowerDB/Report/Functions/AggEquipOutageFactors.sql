﻿CREATE FUNCTION [Report].[AggEquipOutageFactors](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, BoilerEPOF = GlobalDB.dbo.WtAvgNN(e.BoilerEPOF, e.E_PH*r.Weighting)
		, STGEPOF = GlobalDB.dbo.WtAvgNN(e.STGEPOF, e.E_PH*r.Weighting)
		, PollutionEPOF = GlobalDB.dbo.WtAvgNN(e.PollutionEPOF, e.E_PH*r.Weighting)
		, CTGEPOF = GlobalDB.dbo.WtAvgNN(e.CTGEPOF, e.E_PH*r.Weighting)
		, OtherEPOF = GlobalDB.dbo.WtAvgNN(e.OtherEPOF, e.E_PH*r.Weighting)
		, CPHM_EUOF = GlobalDB.dbo.WtAvgNN(e.CPHM_EUOF, e.E_PH*r.Weighting)
		, BOIL_EUOF = GlobalDB.dbo.WtAvgNN(e.BOIL_EUOF, e.E_PH*r.Weighting)
		, TURB_EUOF = GlobalDB.dbo.WtAvgNN(e.TURB_EUOF, e.E_PH*r.Weighting)
		, GEN_EUOF = GlobalDB.dbo.WtAvgNN(e.GEN_EUOF, e.E_PH*r.Weighting)
		, VC_EUOF = GlobalDB.dbo.WtAvgNN(e.VC_EUOF, e.E_PH*r.Weighting)
		, CA_EUOF = GlobalDB.dbo.WtAvgNN(e.CA_EUOF, e.E_PH*r.Weighting)
		, BAG_EUOF = GlobalDB.dbo.WtAvgNN(e.BAG_EUOF, e.E_PH*r.Weighting)
		, PREC_EUOF = GlobalDB.dbo.WtAvgNN(e.PREC_EUOF, e.E_PH*r.Weighting)
		, WGS_EUOF = GlobalDB.dbo.WtAvgNN(e.WGS_EUOF, e.E_PH*r.Weighting)
		, DGS_EUOF = GlobalDB.dbo.WtAvgNN(e.DGS_EUOF, e.E_PH*r.Weighting)
		, SCR_EUOF = GlobalDB.dbo.WtAvgNN(e.SCR_EUOF, e.E_PH*r.Weighting)
		, CTTURB_EUOF = GlobalDB.dbo.WtAvgNN(e.CTTURB_EUOF, e.E_PH*r.Weighting)
		, CTGEN_EUOF = GlobalDB.dbo.WtAvgNN(e.CTGEN_EUOF, e.E_PH*r.Weighting)
		, CTTRANS_EUOF = GlobalDB.dbo.WtAvgNN(e.CTTRANS_EUOF, e.E_PH*r.Weighting)
		, CTHRSG_EUOF = GlobalDB.dbo.WtAvgNN(e.CTHRSG_EUOF, e.E_PH*r.Weighting)
		, CTOTH_EUOF = GlobalDB.dbo.WtAvgNN(e.CTOTH_EUOF, e.E_PH*r.Weighting)
		, FHF_EUOF = GlobalDB.dbo.WtAvgNN(e.FHF_EUOF, e.E_PH*r.Weighting)
		, CWF_EUOF = GlobalDB.dbo.WtAvgNN(e.CWF_EUOF, e.E_PH*r.Weighting)
		, ASH_EUOF = GlobalDB.dbo.WtAvgNN(e.ASH_EUOF, e.E_PH*r.Weighting)
		, DQWS_EUOF = GlobalDB.dbo.WtAvgNN(e.DQWS_EUOF, e.E_PH*r.Weighting)
		, TRANS_EUOF = GlobalDB.dbo.WtAvgNN(e.TRANS_EUOF, e.E_PH*r.Weighting)
		, WASTEH2O_EUOF = GlobalDB.dbo.WtAvgNN(e.WASTEH2O_EUOF, e.E_PH*r.Weighting)
		, OTHER_EUOF = GlobalDB.dbo.WtAvgNN(e.OTHER_EUOF, e.E_PH*r.Weighting)
	FROM @refnums r INNER JOIN dbo.EUFTotals e ON e.Refnum = r.Refnum
	GROUP BY r.SubGroupType, r.SubGroupID
)

