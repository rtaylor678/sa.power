﻿CREATE FUNCTION [Report].[AggLTSA](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, EquivStartsFactor = SUM(CASE WHEN LTSA.CurrEquivStarts > 0 THEN LTSA.CurrEquivStarts*r.Weighting END)/SUM(CASE WHEN LTSA.CurrEquivStarts > 0 THEN LTSA.CurrTotStarts*r.Weighting END)
		, CurrEquivRunHrsFactor = SUM(CASE WHEN LTSA.CurrEquivRunHrs > 0 THEN LTSA.CurrEquivRunHrs*r.Weighting END)/SUM(CASE WHEN LTSA.CurrEquivRunHrs > 0 THEN LTSA.CurrTotRunHrs*r.Weighting END)
	FROM @Refnums r INNER JOIN dbo.LTSA LTSA ON LTSA.Refnum = r.Refnum
	WHERE r.Weighting > 0
	GROUP BY r.SubGroupType, r.SubGroupID
)

