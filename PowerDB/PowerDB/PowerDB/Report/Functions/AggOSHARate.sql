﻿CREATE FUNCTION [Report].[AggOSHARate](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, OSHARate = GlobalDB.dbo.WtAvg(g.OSHARate, p.SiteEffPers*r.Weighting)
	FROM @refnums r INNER JOIN dbo.GenSum g ON g.Refnum = r.Refnum
	INNER JOIN dbo.PersSTCalc p ON p.Refnum = r.Refnum AND p.SectionID = 'TP'
	GROUP BY r.SubGroupType, r.SubGroupID
)

