﻿CREATE FUNCTION [Report].[AggAbsenceMPS](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT SubGroupType, SubGroupID, TotMPSAbsPcnt = GlobalDB.dbo.WtAvg(TotMPSAbsPcnt, SiteSTH), MPSAbsPcnt_HOL = GlobalDB.dbo.WtAvg(HOL, SiteSTH), MPSAbsPcnt_VAC = GlobalDB.dbo.WtAvg(VAC, SiteSTH), MPSAbsPcnt_HOLVAC = GlobalDB.dbo.WtAvg(HOLVAC, SiteSTH)
		, MPSAbsPcnt_ONJOBC = GlobalDB.dbo.WtAvg(ONJOBC, SiteSTH), MPSAbsPcnt_ONJOB = GlobalDB.dbo.WtAvg(ONJOB, SiteSTH), MPSAbsPcnt_SICK = GlobalDB.dbo.WtAvg(SICK, SiteSTH), MPSAbsPcnt_LEGAL = GlobalDB.dbo.WtAvg(LEGAL, SiteSTH)
		, MPSAbsPcnt_OthExc = GlobalDB.dbo.WtAvg(OTHEXC, SiteSTH), MPSAbsPcnt_Unexc = GlobalDB.dbo.WtAvg(UNEXC, SiteSTH), MPSAbsPcnt_Other = GlobalDB.dbo.WtAvg(OTHER, SiteSTH)
	FROM (
	SELECT pvt.SubGroupType, pvt.SubGroupID, pvt.Refnum, pvt.TotMPSAbsPcnt, [HOL],[VAC],[HOLVAC],[ONJOBC],[ONJOB],[SICK],[LEGAL],[OTHEXC],[UNEXC],[OTHER],p.SiteSTH
	FROM (SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, TotMPSAbsPcnt = t.MPSAbsPcnt, a.AbsCategory, a.MPSAbsPcnt FROM @Refnums r INNER JOIN dbo.AbsenceTotCalc t ON t.Refnum = r.Refnum INNER JOIN dbo.AbsenceCalc a ON a.Refnum = t.Refnum) src
	PIVOT (SUM(MPSAbsPcnt) FOR AbsCategory IN ([HOLVAC],[ONJOB],[OTHER],[UNEXC],[ONJOBC],[HOL],[LEGAL],[VAC],[SICK],[OTHEXC])) pvt
	INNER JOIN dbo.PersSTCalc p ON p.Refnum = pvt.Refnum AND p.SectionID = 'TMPS'
	) MPS GROUP BY SubGroupType, SubGroupID
)

