﻿CREATE FUNCTION [Report].[AggFuelCost](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID, bx.HVBasis
		, Gas = CASE WHEN SUM(bx.GasMBTU*r.Weighting) > 0 THEN SUM(o.PurGas*r.Weighting)/SUM(bx.GasMBTU*r.Weighting)*1000 END
		, Liquid = CASE WHEN SUM(bx.LiquidsMBTU*r.Weighting) > 0 THEN SUM(o.PurLiquid*r.Weighting)/SUM(bx.LiquidsMBTU*r.Weighting)*1000 END
		, Coal = CASE WHEN SUM(bx.CoalMBTU*r.Weighting) > 0 THEN SUM(o.PurSolid*r.Weighting)/SUM(bx.CoalMBTU*r.Weighting)*1000 END 
		, Total = CASE WHEN SUM(bx.TotMBTU*r.Weighting) > 0 THEN SUM(o.STFuelCost*r.Weighting)/SUM(bx.TotMBTU*r.Weighting)*1000 END
	FROM @refnums r INNER JOIN dbo.OpExCalc o ON o.Refnum = r.Refnum AND o.DataType = 'ADJ'
	INNER JOIN dbo.FuelTotCalc f ON f.Refnum = r.Refnum
	CROSS APPLY (VALUES ('HHV', f.TotGasMBTU, f.LiquidsMBTU, f.CoalMBTU, f.TotMBTU)
					, ('LHV', f.TotGasMBTULHV, f.LiquidsMBTULHV, f.CoalMBTULHV, f.TotMBTULHV)
			) bx(HVBasis, GasMBTU, LiquidsMBTU, CoalMBTU, TotMBTU)
	GROUP BY r.SubGroupType, r.SubGroupID, bx.HVBasis
)

