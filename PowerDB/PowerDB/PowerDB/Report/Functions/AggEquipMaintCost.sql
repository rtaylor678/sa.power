﻿CREATE FUNCTION [Report].[AggEquipMaintCost](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT ac.SubGroupType, ac.SubGroupID, ac.EquipGroup
		, AnnMaintCostMWH = GlobalDB.dbo.WtAvg(ac.AnnMaintCostMWH, gtc.AdjNetMWH2Yr*ac.Weighting)
		, AnnMaintCostMW = GlobalDB.dbo.WtAvg(ac.AnnMaintCostMW, nf.NMC2Yr*ac.Weighting)
		, AnnMaintCostMBTU = GlobalDB.dbo.WtAvg(ac.AnnMaintCostMBTU, gtc.TotalOutputMBTU2Yr*ac.Weighting)
		, COUNT(DISTINCT t.Refnum) AS UnitCount
		, COUNT(DISTINCT t.SiteID) AS SiteCount
		, COUNT(DISTINCT t.CompanyID) AS CompanyCount
	FROM (
		SELECT r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, EquipGroup = ISNULL(cc.MapToEquipGroup,m.EquipGroup)
			, AnnMaintCostMWH = SUM(m.AnnMaintCostMWH)
			, AnnMaintCostMW = SUM(m.AnnMaintCostMW)
			, AnnMaintCostMBTU = SUM(m.AnnMaintCostMBTU)
		FROM @Refnums r INNER JOIN dbo.MaintEquipCalc m ON m.Refnum = r.Refnum
		LEFT JOIN (VALUES ('STG', 'STG-GEN')
			, ('BLR-AIR','BOIL'), ('BLR-BOIL','BOIL'), ('BLR-COND','BOIL')
			, ('CTSCR','SCR')
			, ('HRSG','CTHRSG')
			, ('DQWS','WSS'), ('BOILH2O','WSS')
			, ('FHF','FUELDEL')
		) cc(EquipGroup, MapToEquipGroup) ON cc.EquipGroup = m.EquipGroup
		WHERE m.AnnMaintCostKUS > 0
		GROUP BY r.SubGroupType, r.SubGroupID, r.Refnum, r.Weighting, ISNULL(cc.MapToEquipGroup,m.EquipGroup)
	) ac
	INNER JOIN dbo.TSort t ON t.Refnum = ac.Refnum
	INNER JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
	INNER JOIN dbo.NERCFactors nf ON nf.Refnum = t.Refnum
	GROUP BY ac.SubGroupType, ac.SubGroupID, ac.EquipGroup
)

