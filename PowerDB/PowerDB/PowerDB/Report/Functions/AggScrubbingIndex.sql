﻿CREATE FUNCTION [Report].[AggScrubbingIndex](@Refnums Report.GroupedRefnumList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.SubGroupType, r.SubGroupID
		, ScrubbingIndex = GlobalDB.dbo.WtAvgNZ(s.ScrubbingIndex, s.TonsSO2Removed*r.Weighting)
	FROM @refnums r INNER JOIN dbo.SO2Removal s ON s.Refnum = r.Refnum
	WHERE s.ScrubbingIndex > 0
	GROUP BY r.SubGroupType, r.SubGroupID
)

