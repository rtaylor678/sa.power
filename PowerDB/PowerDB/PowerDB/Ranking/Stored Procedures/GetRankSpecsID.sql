﻿CREATE PROC [Ranking].[GetRankSpecsID](@RefListName varchar(30), @RankBreak sysname, @BreakValue varchar(12), @RankVariable sysname, @RankSpecsID int OUTPUT)
AS
BEGIN
	SET @RankSpecsID = NULL
	IF @RefListName IS NULL OR @RankBreak IS NULL OR @BreakValue IS NULL OR @RankVariable IS NULL
		RETURN 1
	ELSE BEGIN
		DECLARE @RefListNo int
		SELECT @RefListNo = RefListNo FROM dbo._RL WHERE ListName = @RefListName
		IF @RefListNo IS NULL
			RETURN 2
		ELSE BEGIN
			SELECT @RankSpecsID = RankSpecsID 
			FROM Ranking.RankSpecs_LU WHERE ListName = @RefListName AND RankBreak = @RankBreak AND BreakValue = @BreakValue AND RankVariable = @RankVariable
		
			IF @RankSpecsID IS NULL
			BEGIN
				INSERT Ranking.RankSpecs_LU(ListName, RankBreak, BreakValue, RankVariable, RefListNo)
				SELECT @RefListName, @RankBreak, @BreakValue, @RankVariable, @RefListNo

				SELECT @RankSpecsID = @@IDENTITY
			END
		END
	END
END

