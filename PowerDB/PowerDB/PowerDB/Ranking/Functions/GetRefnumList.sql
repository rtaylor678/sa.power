﻿CREATE FUNCTION [Ranking].[GetRefnumList](@RefListName varchar(30), @RankBreak sysname, @BreakValue varchar(12))
RETURNS @refnums TABLE (Refnum varchar(18) NOT NULL)
AS
BEGIN
	IF @RankBreak = 'TotalList'
		INSERT @refnums
		SELECT DISTINCT Refnum
		FROM dbo._RL
		WHERE ListName = @RefListName
	ELSE IF @RankBreak = 'UserGroup'
		INSERT @refnums
		SELECT Refnum
		FROM dbo._RL
		WHERE ListName = @RefListName AND UserGroup = @BreakValue
	ELSE
		INSERT @refnums
		SELECT Refnum 
		FROM Ranking.UnitBreakValues
		WHERE RankBreak = @RankBreak AND BreakValue = @BreakValue AND Refnum IN (SELECT Refnum FROM dbo._RL WHERE ListName = @RefListName)

	RETURN
END

