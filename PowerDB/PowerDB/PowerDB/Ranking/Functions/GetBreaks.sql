﻿CREATE FUNCTION [Ranking].[GetBreaks](@RefListName varchar(30) = NULL, @OnlyActiveBreaks bit = 1)
RETURNS @Breaks TABLE (RankBreak varchar(128) NOT NULL)
AS
BEGIN
	IF @RefListName IS NULL OR NOT EXISTS (SELECT * FROM Ranking.RankSpecs_LU WHERE ListName = @RefListName)
		INSERT @Breaks (RankBreak)
		SELECT RankBreak FROM Ranking.RankBreaks WHERE Active = 'Y' OR @OnlyActiveBreaks = 1
	ELSE
		INSERT @Breaks (RankBreak)
		SELECT DISTINCT RankBreak FROM Ranking.RankSpecs_LU WHERE ListName = @RefListName
		
	RETURN
END

