﻿
CREATE FUNCTION [Ranking].[GetBreakValues](@RefListName varchar(30), @RankBreak sysname)
RETURNS @BreakValues TABLE (BreakValue varchar(12) NOT NULL)
AS
BEGIN
	IF EXISTS (SELECT * FROM Ranking.RankBreaks WHERE RankBreak = @RankBreak)
	BEGIN
		IF @RankBreak = 'TotalList'
			INSERT @BreakValues (BreakValue) VALUES ('ALL')
		ELSE BEGIN 
			IF @RankBreak = 'UserGroup'
				INSERT @BreakValues (BreakValue)
				SELECT DISTINCT UserGroup
				FROM dbo._RL WHERE ListName = @RefListName
			ELSE 
				INSERT @BreakValues (BreakValue)
				SELECT DISTINCT BreakValue
				FROM Ranking.UnitBreakValues
				WHERE RankBreak = @RankBreak AND BreakValue IS NOT NULL
				AND Refnum IN (SELECT Refnum FROM dbo._RL WHERE ListName = @RefListName)
			
			INSERT @BreakValues(BreakValue)
			SELECT BreakValue FROM Ranking.RankSpecs_LU rs
			WHERE ListName = @RefListName AND RankBreak = @RankBreak
			AND NOT EXISTS (SELECT * FROM @BreakValues x WHERE x.BreakValue = rs.BreakValue)
		END
	END
	RETURN
END


