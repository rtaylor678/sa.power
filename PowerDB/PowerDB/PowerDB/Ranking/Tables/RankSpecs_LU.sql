﻿CREATE TABLE [Ranking].[RankSpecs_LU] (
    [RankSpecsID]  INT               IDENTITY (1, 1) NOT NULL,
    [ListName]     VARCHAR (30)      NOT NULL,
    [RankBreak]    VARCHAR (128)     NOT NULL,
    [BreakValue]   CHAR (12)         NOT NULL,
    [RankVariable] VARCHAR (128)     NOT NULL,
    [RefListNo]    [dbo].[RefListNo] NULL,
    CONSTRAINT [PK_RankSpecs_LU_1] PRIMARY KEY CLUSTERED ([RankSpecsID] ASC) WITH (FILLFACTOR = 70),
    CONSTRAINT [UniqRankSpec_1] UNIQUE NONCLUSTERED ([ListName] ASC, [RankBreak] ASC, [BreakValue] ASC, [RankVariable] ASC) WITH (FILLFACTOR = 70)
);

