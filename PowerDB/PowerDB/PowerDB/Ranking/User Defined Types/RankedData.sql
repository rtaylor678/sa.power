﻿CREATE TYPE [Ranking].[RankedData] AS TABLE (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [Rank]       SMALLINT       NULL,
    [Percentile] REAL           NULL,
    [Tile]       TINYINT        NULL,
    [Value]      FLOAT (53)     NULL,
    PRIMARY KEY CLUSTERED ([Refnum] ASC));

