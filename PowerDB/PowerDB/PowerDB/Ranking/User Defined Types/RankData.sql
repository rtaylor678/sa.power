﻿CREATE TYPE [Ranking].[RankData] AS TABLE (
    [Refnum]     [dbo].[Refnum] NOT NULL,
    [RankValue]  REAL           NOT NULL,
    [PcntFactor] REAL           NOT NULL,
    PRIMARY KEY CLUSTERED ([Refnum] ASC));

