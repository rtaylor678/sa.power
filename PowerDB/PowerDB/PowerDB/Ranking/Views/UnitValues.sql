﻿
CREATE VIEW [Ranking].[UnitValues]  AS 
SELECT t.Refnum, v.RankVariable, v.Value, v.PcntFactor
FROM dbo.TSort t LEFT JOIN dbo.AbsenceCalc sick ON sick.Refnum = t.Refnum AND sick.AbsCategory = 'SICK'
LEFT JOIN dbo.CoalCostMBTU coalcost ON coalcost.Refnum = t.Refnum
LEFT JOIN dbo.CogenCalc cc ON cc.Refnum = t.Refnum
LEFT JOIN dbo.CommUnavail cu ON cu.Refnum = t.Refnum
LEFT JOIN dbo.CRVCalc crv ON crv.Refnum = t.Refnum
LEFT JOIN dbo.GapFactors gf ON gf.Refnum = t.Refnum
LEFT JOIN dbo.GenerationTotCalc gtc ON gtc.Refnum = t.Refnum
LEFT JOIN dbo.GenSum gs ON gs.Refnum = t.Refnum
LEFT JOIN dbo.MaintTotCalc mtc ON mtc.Refnum = t.Refnum
LEFT JOIN dbo.MaintIndexDetails blrm ON blrm.Refnum = t.Refnum AND blrm.EquipGroup2 = 'BOILER'
LEFT JOIN dbo.MaintIndexDetails turbm ON turbm.Refnum = t.Refnum AND turbm.EquipGroup2 = 'TURBINE'
LEFT JOIN dbo.MaintIndexDetails othm ON othm.Refnum = t.Refnum AND othm.EquipGroup2 = 'OTHER'
LEFT JOIN dbo.NERCFactors nf ON nf.Refnum = t.Refnum
LEFT JOIN dbo.OpExCalc oegc ON oegc.Refnum = t.Refnum AND oegc.DataType = 'EGC'
LEFT JOIN dbo.OpExCalc okw ON okw.Refnum = t.Refnum AND okw.DataType = 'KW'
LEFT JOIN dbo.OpExCalc omwh ON omwh.Refnum = t.Refnum AND omwh.DataType = 'MWH'
LEFT JOIN dbo.PlantActionExp pmkw ON pmkw.Refnum = t.Refnum AND pmkw.DataType = 'KW'
LEFT JOIN dbo.PlantActionExp pmmwh ON pmmwh.Refnum = t.Refnum AND pmmwh.DataType = 'MWH'
LEFT JOIN dbo.SO2Removal si ON si.Refnum = t.Refnum
CROSS APPLY (VALUES 
	('MPSSickPcnt', CAST(sick.MPSAbsPcnt as real), gtc.AdjNetMWH),
	('OCCSickPcnt', CAST(sick.OCCAbsPcnt as real), gtc.AdjNetMWH),
	('CoalCostMBTU', CAST(coalcost.TotCostMBTU as real), gtc.AdjNetMWH),
	('AnnMaintCostMBTU', CAST(cc.AnnMaintCostMBTU as real), gtc.AdjNetMWH),
	('ThermEff', CAST(cc.ThermEff as real), gtc.AdjNetMWH),
	('TotCashLessFuelMBTU', CASE WHEN TotCashLessFuelMBTU > 0 THEN CAST(cc.TotCashLessFuelMBTU as real) END, gtc.AdjNetMWH),
	('CUTI_P', CAST(cu.CUTI_P as real), gtc.AdjNetMWH),
	('CUTI_Tot', CAST(cu.CUTI_Tot as real), gtc.AdjNetMWH),
	('CUTI_Tot_Sum', CAST(cu.CUTI_Tot_Sum as real), gtc.AdjNetMWH),
	('CUTI_Unp', CAST(cu.CUTI_Unp as real), gtc.AdjNetMWH),
	('LRO_Unp', CAST(cu.LRO_Unp as real), gtc.AdjNetMWH),
	('LROP_Tot', CAST(cu.LROP_Tot as real), gtc.AdjNetMWH),
	('PeakUnavail_F', CAST(cu.PeakUnavail_F as real), gtc.AdjNetMWH),
	('PeakUnavail_P', CAST(cu.PeakUnavail_P as real), gtc.AdjNetMWH),
	('PeakUnavail_Tot', CAST(cu.PeakUnavail_Tot as real), gtc.AdjNetMWH),
	('PeakUnavail_Unp', CAST(cu.PeakUnavail_Unp as real), gtc.AdjNetMWH),
	('CRV', CAST(crv.CRVVariance as real), gtc.AdjNetMWH),
	('FixedLessWagesBenMWH', CAST(gf.FixedLessWagesBen as real), gtc.AdjNetMWH),
	('FixedLessWagesBenMW', CAST(gf.FixedLessWagesBenMW as real), gtc.AdjNetMWH),
	('SiteWagesBenMWH', CAST(gf.SiteWagesBen as real), gtc.AdjNetMWH),
	('SiteWagesBenMW', CAST(gf.SiteWagesBenMW as real), gtc.AdjNetMWH),
	('TotLessAGEffPersMW', CAST(gf.TotLessAGEffPersMW as real), gtc.AdjNetMWH),
	('VarLessFuelMWH', CAST(gf.VarLessFuel as real), gtc.AdjNetMWH),
	('VarLessFuelMW', CAST(gf.VarLessFuelMW as real), gtc.AdjNetMWH),
	('HeatRateLHV', CAST(gtc.HeatRateLHV as real), gtc.AdjNetMWH),
	('InternalLessScrubberPcnt', CAST(gtc.InternalLessScrubberPcnt as real), gtc.AdjNetMWH),
	('InternalUsagePcnt', CAST(gtc.InternalUsagePcnt as real), gtc.AdjNetMWH),
	('TotStarts', CAST(gtc.TotStarts as real), gtc.AdjNetMWH),
	('AvgFuelCostMBTU', CAST(gs.AvgFuelCostMBTU as real), gtc.AdjNetMWH),
	('AvgFuelCostMBTULHV', CAST(gs.AvgFuelCostMBTULHV as real), gtc.AdjNetMWH),
	('EAF2Yr', CAST(gs.EAF2Yr as real), gtc.AdjNetMWH),
	('EFOR2Yr', CAST(gs.EFOR2Yr as real), gtc.AdjNetMWH),
	('EUF2Yr', CAST(gs.EUF2Yr as real), gtc.AdjNetMWH),
	('HeatRate', CAST(gs.HeatRate as real), gtc.AdjNetMWH),
	('NCF2Yr', CAST(gs.NCF2Yr as real), gtc.AdjNetMWH),
	('NOF2Yr', CAST(gs.NOF2Yr as real), gtc.AdjNetMWH),
	('OSHARate', CAST(gs.OSHARate as real), gtc.AdjNetMWH),
	('TotEffPersMW', CAST(gs.TotEffPersMW as real), gtc.AdjNetMWH),
	('TotEffPersMWH', CAST(gs.TotEffPersMWH as real), gtc.AdjNetMWH),
	('AnnMaintCostMW_Boiler', CAST(blrm.AnnMaintCostMW as real), gtc.AdjNetMWH),
	('AnnMaintCostMWH_Boiler', CAST(blrm.AnnMaintCostMWH as real), gtc.AdjNetMWH),
	('AnnMaintCostMW_Other', CAST(othm.AnnMaintCostMW as real), gtc.AdjNetMWH),
	('AnnMaintCostMWH_Other', CAST(othm.AnnMaintCostMWH as real), gtc.AdjNetMWH),
	('AnnMaintCostMW_Turbine', CAST(turbm.AnnMaintCostMW as real), gtc.AdjNetMWH),
	('AnnMaintCostMWH_Turbine', CAST(turbm.AnnMaintCostMWH as real), gtc.AdjNetMWH),
	('AnnMaintCostEGC', CAST(mtc.AnnMaintCostEGC as real), gtc.AdjNetMWH),
	('AnnMaintCostMW', CAST(mtc.AnnMaintCostMW as real), gtc.AdjNetMWH),
	('AnnNonOHCostMW', CAST(mtc.AnnNonOHCostMW as real), gtc.AdjNetMWH),
	('AnnOHCostMW', CAST(mtc.AnnOHCostMW as real), gtc.AdjNetMWH),
	('AnnLTSACostMW', CASE WHEN mtc.AnnLTSACostMW > 0  THEN CAST(mtc.AnnLTSACostMW as real) END, gtc.AdjNetMWH),
	('AnnMaintCostMWH', CAST(mtc.AnnMaintCostMWH as real), gtc.AdjNetMWH),
	('AnnNonOHCostMWH', CAST(mtc.AnnNonOHCostMWH as real), gtc.AdjNetMWH),
	('AnnOHCostMWH', CAST(mtc.AnnOHCostMWH as real), gtc.AdjNetMWH),
	('AnnLTSACostMWH', CASE WHEN mtc.AnnLTSACostMWH > 0 THEN CAST(mtc.AnnLTSACostMWH as real) END, gtc.AdjNetMWH),
	('EFOR', CAST(nf.EFOR as real), gtc.AdjNetMWH),
	('EPOF', CAST(nf.EPOF as real), gtc.AdjNetMWH),
	('EUF', CAST(nf.EUF as real), gtc.AdjNetMWH),
	('EUOF', CAST(nf.EUOF as real), gtc.AdjNetMWH),
	('EUOF2Yr', CAST(nf.EUOF2Yr as real), gtc.AdjNetMWH),
	('EUOR', CAST(nf.EUOR as real), gtc.AdjNetMWH),
	('MSTFO', CAST(nf.MSTFO as real), gtc.AdjNetMWH),
	('MSTUO', CAST(nf.MSTUO as real), gtc.AdjNetMWH),
	('POF2Yr', CAST(nf.POF2Yr as real), gtc.AdjNetMWH),
	('ActCashLessFuelEGC', CAST(oegc.ActCashLessFuel as real), gtc.AdjNetMWH),
	('TotCashEGC', CAST(oegc.TotCash as real), gtc.AdjNetMWH),
	('TotCashLessFuelEmEGC', CAST(oegc.TotCashLessFuelEm as real), gtc.AdjNetMWH),
	('ActCashLessFuelMW', CAST(okw.ActCashLessFuel as real), gtc.AdjNetMWH),
	('TotCashMW', CAST(okw.TotCash as real), gtc.AdjNetMWH),
	('TotCashLessFuelMW', CAST(okw.TotCashLessFuel as real), gtc.AdjNetMWH),
	('TotCashLessFuelEmMW', CAST(okw.TotCashLessFuelEm as real), gtc.AdjNetMWH),
	('ActCashLessFuelMWH', CAST(omwh.ActCashLessFuel as real), gtc.AdjNetMWH),
	('TotCashMWH', CAST(omwh.TotCash as real), gtc.AdjNetMWH),
	('TotCashLessFuelMWH', CAST(omwh.TotCashLessFuel as real), gtc.AdjNetMWH),
	('TotCashLessFuelEmMWH', CAST(omwh.TotCashLessFuelEm as real), gtc.AdjNetMWH),
	('GrandTotOpexMWH', CAST(omwh.GrandTot as real), gtc.AdjNetMWH),
	('FERCApproxExpMW', CAST(pmkw.FERCApproxExp as real), gtc.AdjNetMWH),
	('FERCApproxExpMWH', CAST(pmmwh.FERCApproxExp as real), gtc.AdjNetMWH),
	('ScrubbingIndex', CAST(si.ScrubbingIndex as real), gtc.AdjNetMWH)
) v(RankVariable, Value, PcntFactor)
WHERE v.Value IS NOT NULL


