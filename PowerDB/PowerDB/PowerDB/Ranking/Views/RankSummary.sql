﻿
CREATE VIEW [Ranking].[RankSummary]  AS 
SELECT lu.RefListNo, RefListName = lu.ListName, lu.RankBreak, lu.BreakValue, lu.RankVariable, 
	s.LastTime, s.DataCount, s.NumTiles, s.Top2, s.Tile1Break, s.Tile2Break, s.Tile3Break, s.Btm2, lu.RankSpecsID
FROM Ranking.RankSpecs_LU lu INNER JOIN Ranking.RankSpecsStats s ON s.RankSpecsID = lu.RankSpecsID

